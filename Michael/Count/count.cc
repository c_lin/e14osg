#include <iostream>
#include <fstream>
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TClonesArray.h"
#include "GsimData/GsimGenParticleData.h"

using namespace std;

int main(int argc, char** argv)
{
	TFile* ifile = new TFile(argv[1], "READ", "InputFile");
	TTree* itree = (TTree*) ifile->Get("eventTree00");

	int i;
	int j;
	int NDalitz = 0;
	int N6Gamma = 0;
	int NDDalitz = 0;

	GsimGenParticleData* GsimParticle = new GsimGenParticleData();
	itree->SetBranchAddress("GenParticle.", &GsimParticle);

	ofstream ofile;
	ofile.open(argv[2]);


	for(i = 0; i < itree->GetEntries(); i++)
	{
		itree->GetEntry(i);

		TClonesArray* GPArray = GsimParticle->briefTracks;
		TVector3 DecayVertex;
		int e = 0;
		int ep = 0;
		int Gamma = 0;
		int Pion = 0;
		int KLong = 0;

		for(j = 0; j < GPArray->GetEntries(); j++)
		{
			GsimTrackData* jparticle = (GsimTrackData*) GPArray->At(j);

			if(jparticle->pid == 11)
				e++;

			if(jparticle->pid == -11)
				ep++;

			if(jparticle->pid == 22)
				Gamma++;

			if(jparticle->pid == 111)
				Pion++;

			if(jparticle->pid == 130)
			{
				DecayVertex = jparticle->end_v;
				KLong++;
			}
		}

		if(GPArray->GetEntries() == 11 && e == 1 && ep == 1 && Gamma == 5 && Pion == 3 && KLong == 1 && DecayVertex.Z() > 2000 && DecayVertex.Z() < 6000)
		{
			NDalitz++;
		}

		if(GPArray->GetEntries() == 10 && Gamma == 6 && Pion == 3 && KLong == 1 && DecayVertex.Z() > 2000 && DecayVertex.Z() < 6000)
		{
			N6Gamma++;
		}

		if(GPArray->GetEntries() == 12 && e == 2 && ep == 2 && Gamma == 4 && Pion == 3 && KLong == 1 && DecayVertex.Z() > 2000 && DecayVertex.Z() < 6000)
		{
			NDDalitz++;
		}
	}

	
	ofile << "Number of Dalitz Generated: " << NDalitz << "\n";
	ofile << "Number of Six Gamma Generated: " << N6Gamma << "\n";
	ofile << "Number of Double Dalitz Generated: " << NDDalitz << "\n";
	
        cout << "Number of Dalitz Generated: " << NDalitz << "\n";
        cout << "Number of Six Gamma Generated: " << N6Gamma << "\n";
        cout << "Number of Double Dalitz Generated: " << NDDalitz << "\n";	
	
	ofile.close();

	ifile->Close();


	return 0;
}
