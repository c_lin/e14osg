#include <numeric>
#include <vector>
#include <bitset>
#include <cmath>

#include "TFile.h"
#include "TChain.h"
#include "TMath.h"
#include "gnana/E14GNAnaFunction.h"
#include "gnana/E14GNAnaDataContainer.h"
#include "gamma/GammaFinder.h"
#include "rec2g/Rec2g.h"
#include "CLHEP/Vector/ThreeVector.h"

#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "MTAnalysisLibrary/MTPositionHandler.h"
#include "MTAnalysisLibrary/MTFunction.h"
#include "MTAnalysisLibrary/MTVetoFBAR.h"
#include "MTAnalysisLibrary/MTVetoNCC.h"
#include "MTAnalysisLibrary/MTVetoCBAR.h"
#include "MTAnalysisLibrary/MTVetoBCV.h"
#include "MTAnalysisLibrary/MTVetoCV.h"
#include "MTAnalysisLibrary/MTVetoOEV.h"
#include "MTAnalysisLibrary/MTVetoCC03.h"
#include "MTAnalysisLibrary/MTVetoCSI.h"
#include "MTAnalysisLibrary/MTVetoLCV.h"
#include "MTAnalysisLibrary/MTVetoCC04.h"
#include "MTAnalysisLibrary/MTVetoCC05.h"
#include "MTAnalysisLibrary/MTVetoCC06.h"
#include "MTAnalysisLibrary/MTVetoBHCV.h"
#include "MTAnalysisLibrary/MTVetoBHPV.h"
#include "MTAnalysisLibrary/MTVetoBPCV.h"
#include "MTAnalysisLibrary/MTVetoBHGC.h"
#include "MTAnalysisLibrary/MTVetonewBHCV.h"
#include "MTAnalysisLibrary/MTVetoIB.h"
#include "MTAnalysisLibrary/MTVetoIBCV.h"
#include "MTAnalysisLibrary/MTVetoMBCV.h"
#include "MTAnalysisLibrary/MTVetoDCV.h"
#include "MTAnalysisLibrary/MTVetoUCV.h"
#include "MTAnalysisLibrary/MTVetoUCVLG.h"
#include "MTAnalysisLibrary/MTVetoDetector.h"
#include "MTAnalysisLibrary/MTGenParticle.h"

#include "SshinoSuppConfInfoManager/SuppConfInfoManager.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
//#include "E14ProdInfo/E14ProdInfoWriter.h"
#include "LcFTT/LcFTTAnaDataHandler.h"
#include <UTCconfig/UTCconfig.h>

#include "./header/cvdetector.h"
#include "./header/cvclass.h"

// functions in src
bool user_rec(std::list<Gamma> const &glist, std::vector<Klong> &klVec, int userFlag);
void user_cut(E14GNAnaDataContainer &data, std::vector<Klong> const &klVec);
int recVtxBisection(double Zmax, double Zmin, std::list<Gamma> glist, Klong &klong, Double_t COEXY[2]);
double recVtxMFunction(std::list<Gamma> glist, double VtxZ, Double_t COEXY[2]);
bool recpi04e(std::list<Gamma> glist, std::vector<Klong> &klVec, int userFlag, Double_t COEXY[2]);
void FindGammaMom( std::list<Gamma> &glist, Double_t VertexZ, Double_t COEXY[2]);
Double_t GetEventStartTimeNew( std::vector<Klong> &klVec, Double_t* vertexTime, const double CSIZPosition, Double_t COEXY[2] );
void CVMapping( std::list<Gamma> glist, CV_Class *CV, Double_t EventStartZ, CVDetector MyCVCoordinates, Double_t COEXY[2] );

bool CVModCheck( std::list<Gamma> glist, CV_Class *CV, Double_t EventStartZ, CVDetector MyCVCoordinates, Double_t COEXY[2] );

int CheckDoubleCount(CV_Class *CV);

Double_t RecPi0Mass(Double_t Gamma1E, Double_t Gamma2E, Double_t Gamma1Pos[3], Double_t Gamma2Pos[3], Double_t VertexZ, Double_t COEXY[2]);

Double_t recVtxPi0(Double_t Zmax, Double_t Zmin, Double_t Gamma1E, Double_t Gamma2E, Double_t Gamma1Pos[3], Double_t Gamma2Pos[3], Double_t COEXY[2]);

Double_t Distance( Double_t a[3], Double_t b[3] );

void FindCOEXY( Double_t COEXY[2], std::list<Gamma> glist);

Double_t Rec3Mass( Double_t Pos[3][3], Double_t Ene[3], Double_t Vtx[3] );

Double_t ThreeParticlesMass( Double_t ChargedE[4], Double_t ChargedPos[4][3], Double_t GammaE[2], Double_t GammaPos[2][3], Double_t Vtx[3] );

Double_t CalculateCVResidualEne( Double_t CVFrontHitsEne[4], Double_t CVRearHitsEne[4], CV_Class *CV, Double_t &CVTotalModuleEne, Double_t &CVTotalHitsEne );

void FindChargedPt(std::list<Gamma> glist, CV_Class *CV, Double_t &PtFourCharged, Double_t *PtTwoCharged);
void FindFusionParameters(std::list<Gamma> glist, Double_t &GamClusRMS_Max, Double_t &GammaChi2_Max);


//
const Double_t CVFrontZ = 5842 + 20;
const Double_t CVRearZ = 6093 + 20;

enum{ DELTA_VERTEX_TIME=0,  DELTA_KL_MASS,    CsIEt,          KL_PT,
      KL_CHISQZ,            DELTA_PI0_MASS,   DELTA_PI0_Z,    MIN_GAMMAE, 
      MAX_GAMMAE,           MIN_FIDUCIAL_XY,  MAX_FIDUCIALR,  MIN_CLUSTER_DISTANCE,
      KL_Z,                 KL_BEAMEXIT_XY,   MAX_SHAPECHISQ  };


// cut line ******************************************************

int g6ana( char* inputFile, char* outputFile, int userFlag, int argc, char** argv );


int main( int argc, char** argv )
{
  std::cout << "g6ana starts." << std::endl;
  // read argument
  if( argc != 3 && argc!=4 ){
    std::cout << "Argument Error ! :" << std::endl
	      << "\t usage : " << argv[0] << " InputFileName OutputFileName userFlag" << std::endl
	      << "\t input file should be the output of clusteringFromDst."  << std::endl;
    std::cout << "If there is no userFlag, default userFlag (20151101) will be set." << std::endl;
    return -1;
  }
  int UserFlag=20151101;
  if( argc==4 )UserFlag = atoi(argv[3]);
  std::cout<<"userFlag: "<<UserFlag<<std::endl;
  if( UserFlag<20150101 || 21120903<UserFlag ){
    std::cout<<"Unexpected UserFlag is used! The UserFlag format is yyyymmdd. Used userFlag: "<<UserFlag<<std::endl;
    return -1;
  }

  int result = g6ana( argv[1], argv[2], UserFlag, argc, argv );
  std::cout << "g6ana finished." << std::endl;
  std::cout << std::endl;
  return result;
}


int g6ana( char* inputFile, char* outputFile, int userFlag, int argc, char** argv )
{
  bool isRealData = true; // Run:true, Sim:false

  // set input file
  TChain *inputTree = new TChain( "clusteringTree" );
  inputTree->Add( inputFile );
  const long nEntries = inputTree->GetEntries();
  if( nEntries==0 ){
    std::cout << "This file has no events." << std::endl;
    return -1;
  }

  // E14BasicParam
  E14BasicParamManager::E14BasicParamManager *m_E14BP
    = new E14BasicParamManager::E14BasicParamManager();
  m_E14BP->SetDetBasicParam(userFlag,"CSI");
  const double CSIZPosition = m_E14BP->GetDetZPosition();

  NsUTC::UTCconfig* utcconf;
  
  //CSIEt
  Double_t CSIEt      = 0.0;
  Double_t CSIHalfEtR = 0.0;
  Double_t CSIHalfEtL = 0.0;
  inputTree->SetBranchAddress( "CSIEt",      &CSIEt );
  inputTree->SetBranchAddress( "CSIHalfEtR", &CSIHalfEtR );
  inputTree->SetBranchAddress( "CSIHalfEtL", &CSIHalfEtL );

  // Run information
  Int_t RunID = 0;
  Int_t NodeID = 0;
  Int_t FileID = 0;
  Int_t DstEntryID = 0;
  Int_t ClusteringEntryID = 0;// additional branch

  Short_t SpillID = 0;
  Int_t   EventID = 0;
  Int_t   TimeStamp;
  UInt_t  L2TimeStamp;
  Short_t Error;
  Short_t TrigTagMismatch;
  Short_t L2AR;
  Short_t COE_Et_overflow;
  Short_t COE_L2_override;
  Int_t   COE_Esum;
  Int_t   COE_Ex;
  Int_t   COE_Ey;
  Bool_t  isGoodRun;
  Bool_t  isGoodSpill;
  Int_t    DetectorWFMCorruptBit;
  UInt_t  DetectorBit,ScaledTrigBit,RawTrigBit;
  Int_t   CDTNum;
  Short_t CDTData[3][64];
  Short_t CDTBit[38][38];
  Short_t CDTVetoData[4][64];
  Short_t CDTVetoBitSum[13];

  
  inputTree->SetBranchAddress( "RunID",     &RunID );
  inputTree->SetBranchAddress( "NodeID",    &NodeID );
  inputTree->SetBranchAddress( "FileID",    &FileID );
  inputTree->SetBranchAddress( "DstEntryID",&DstEntryID );

  if(inputTree->GetBranch("DetectorBit")){
    inputTree->SetBranchAddress("SpillID",        &SpillID );
    inputTree->SetBranchAddress("EventID",        &EventID );
    inputTree->SetBranchAddress("TimeStamp",      &TimeStamp);
    inputTree->SetBranchAddress("L2TimeStamp",    &L2TimeStamp);
    inputTree->SetBranchAddress("Error",          &Error);
    inputTree->SetBranchAddress("TrigTagMismatch",&TrigTagMismatch);
    inputTree->SetBranchAddress("L2AR",           &L2AR);
    inputTree->SetBranchAddress("COE_Et_overflow",&COE_Et_overflow);
    inputTree->SetBranchAddress("COE_L2_override",&COE_L2_override);
    inputTree->SetBranchAddress("COE_Esum",       &COE_Esum);
    inputTree->SetBranchAddress("COE_Ex",         &COE_Ex);
    inputTree->SetBranchAddress("COE_Ey",         &COE_Ey);
    inputTree->SetBranchAddress("DetectorBit",    &DetectorBit);
    inputTree->SetBranchAddress("ScaledTrigBit",  &ScaledTrigBit);
    inputTree->SetBranchAddress("RawTrigBit",     &RawTrigBit);
    inputTree->SetBranchAddress("isGoodRun",      &isGoodRun);
    inputTree->SetBranchAddress("isGoodSpill",    &isGoodSpill);
    if(inputTree->GetBranch("CDTVetoData")){
      inputTree->SetBranchAddress("CDTVetoData",  CDTVetoData);    
      inputTree->SetBranchAddress("CDTVetoBitSum",CDTVetoBitSum);    
    }
	if(inputTree->GetBranch("DetectorWFMCorruptBit")){
	  inputTree->SetBranchAddress("DetectorWFMCorruptBit",  &DetectorWFMCorruptBit);
	}
  }  

  if(inputTree->GetBranch("CDTNum")){
     inputTree->SetBranchAddress("CDTNum",       &CDTNum);
     if( inputTree->GetBranch("CDTData")){
        inputTree->SetBranchAddress("CDTData",      CDTData);
        inputTree->SetBranchAddress("CDTBit",       CDTBit);
     }
  }

  // check sim or data
  Int_t    TruePID = 0; // mother particle ID
  Int_t    TrueDecayMode = 0;
  Int_t    GsimGenEventID = 0;
  Int_t    GsimEntryID = 0;
  Int_t    MCEventID = 0;//obsolete
  Double_t TrueVertexZ;
  Double_t TrueVertexTime;
  Double_t TrueMom[3];
  Double_t TruePos[3];
  MTGenParticle* genP = new MTGenParticle();
  std::string *AccidentalFileName = new std::string;
  Int_t        AccidentalFileID;
  Long64_t     AccidentalEntryID;
  Bool_t       AccidentalOverlayFlag;
  if( inputTree->GetBranch( "TruePID" ) ){
    isRealData = false;
    inputTree->SetBranchAddress("TruePID",       &TruePID );
    inputTree->SetBranchAddress("TrueDecayMode", &TrueDecayMode );
    inputTree->SetBranchAddress("TrueVertexZ",   &TrueVertexZ );
    inputTree->SetBranchAddress("TrueVertexTime",&TrueVertexTime );
    inputTree->SetBranchAddress("TrueMom",        TrueMom );
    inputTree->SetBranchAddress("TruePos",        TruePos );
    inputTree->SetBranchAddress("GsimGenEventID",    &GsimGenEventID );
    inputTree->SetBranchAddress("GsimEntryID",       &GsimEntryID );
    inputTree->SetBranchAddress("MCEventID",         &MCEventID );
    inputTree->SetBranchAddress("AccidentalFileName",   &AccidentalFileName );
    inputTree->SetBranchAddress("AccidentalFileID",     &AccidentalFileID );
    inputTree->SetBranchAddress("AccidentalEntryID",    &AccidentalEntryID );
    inputTree->SetBranchAddress("AccidentalOverlayFlag",&AccidentalOverlayFlag );
    genP->SetBranchAddress( inputTree );
	utcconf = NsUTC::UTCconfig::GetInstance(NsUTC::RUN81);
  }else{
	inputTree->GetEntry(0);
	utcconf = NsUTC::UTCconfig::GetInstance(RunID);
  }

  E14GNAnaDataContainer data;
  data.setBranchAddress( inputTree );

  SshinoLib::SuppConfInfoManager *SuppConfInfoMan = NULL; 
  SuppConfInfoMan = new SshinoLib::SuppConfInfoManager( inputFile );
  
  // set output file
  TFile *otf = new TFile( outputFile, "RECREATE");

  // add version information of E14 Analysis Suite
  //E14ProdLibrary::E14ProdInfoWriter::GetInstance()->Write(inputTree->GetFile(), otf, argc, argv);
  
  // add peakfind and zero-suppression information
  SuppConfInfoMan -> WriteSuppConfInfo( otf );

  TTree *otr = new TTree( "RecTree", "output from g6ana");  
  TTree *logtr = new TTree( "LogTree", "Log information");  

  otr->Branch( "RunID",             &RunID,             "RunID/I" );
  otr->Branch( "NodeID",            &NodeID,            "NodeID/I" );
  otr->Branch( "FileID",            &FileID,            "FileID/I" );
  otr->Branch( "DstEntryID",        &DstEntryID,        "DstEntryID/I" );
  otr->Branch( "ClusteringEntryID", &ClusteringEntryID, "ClusteringEntryID/I" );  
  otr->Branch( "userFlag",          &userFlag,          "userFlag/I");

  if(isRealData){
    otr->Branch("SpillID",        &SpillID,        "SpillID/S" );
    otr->Branch("EventID",        &EventID,        "EventID/I" );
    otr->Branch("DetectorBit",    &DetectorBit,    "DetectorBit/i");
    otr->Branch("ScaledTrigBit",  &ScaledTrigBit,  "ScaledTrigBit/i");  
    otr->Branch("RawTrigBit",     &RawTrigBit,     "RawTrigBit/i");
    otr->Branch("TimeStamp",      &TimeStamp,      "TimeStamp/I");
    otr->Branch("L2TimeStamp",    &L2TimeStamp,    "L2TimeStamp/i");
    otr->Branch("Error",          &Error,          "Error/S");
    otr->Branch("TrigTagMismatch",&TrigTagMismatch,"TrigTagMismatch/S");
    otr->Branch("L2AR",           &L2AR,           "L2AR/S");
    otr->Branch("COE_Et_overflow",&COE_Et_overflow,"COE_Et_overflow/S");
    otr->Branch("COE_L2_override",&COE_L2_override,"COE_L2_override/S");
    otr->Branch("COE_Esum",       &COE_Esum,       "COE_Esum/I");
    otr->Branch("COE_Ex",         &COE_Ex,         "COE_Ex/I");
    otr->Branch("COE_Ey",         &COE_Ey,         "COE_Ey/I");
    otr->Branch("isGoodRun",      &isGoodRun,      "isGoodRun/O");
    otr->Branch("isGoodSpill",    &isGoodSpill,    "isGoodSpill/O");
    if(inputTree->GetBranch("CDTVetoData")){
      otr->Branch("CDTVetoData",  CDTVetoData,     "CDTVetoData[4][64]/S");
      otr->Branch("CDTVetoBitSum",CDTVetoBitSum,   "CDTVetoBitSum[13]/S");
    }
    if(inputTree->GetBranch("DetectorWFMCorruptBit")){
		otr->Branch("DetectorWFMCorruptBit", &DetectorWFMCorruptBit,  "DetectorWFMCorruptBit/I");
	}
  }

  if(inputTree->GetBranch("CDTNum")){
      otr->Branch("CDTNum",       &CDTNum,         "CDTNum/I");
      if(inputTree->GetBranch("CDTData")){
         otr->Branch("CDTData",      CDTData,         "CDTData[3][64]/S");
         otr->Branch("CDTBit",       CDTBit,          "CDTBit[38][38]/S");
     }
  } 

 
  data.branchOfKlong( otr );
  
  // UTC branches
  float GammaUTCEne[120];
  float GammaUTCTime[120];
  float GammaUTCWeightedTime[120];
 
  float GammaUTCCorrectedTime[120];
  float GammaUTCCorrectedWeightedTime[120];

	
  if(userFlag>=20190101){  
	  otr->Branch("GammaUTCEne", &GammaUTCEne, "GammaUTCEne[120]");
	  otr->Branch("GammaUTCTime", &GammaUTCTime, "GammaUTCTime[120]");
	  otr->Branch("GammaUTCWeightedTime", &GammaUTCWeightedTime, "GammaUTCWeightedTime[120]");
  }

  
  Double_t EventStartTime = 0.0;
  Double_t EventStartZ = 0.0;
  Double_t VertexTime[6] = {0.0};
  // Original cluster information
  Int_t OriginalClusterNumber = 0;
  Double_t* OriginalClusterE = new Double_t [256]();
  Double_t* OriginalClusterT = new Double_t [256]();
  Double_t (*OriginalClusterPos)[3] = new Double_t [256][3]();
  Double_t* OriginalClusterVertexT = new Double_t [256]();
  
  const int NclsMAX = 120;
  const int NUTCmodMAX = 20;
  int ClusterUTCModNumber[NclsMAX];
  int ClusterUTCModID[NclsMAX][NUTCmodMAX];
  float ClusterUTCModTime[NclsMAX][NUTCmodMAX];
  float ClusterUTCModEne[NclsMAX][NUTCmodMAX];

  for(int i=0;i<NclsMAX;i++){
	  ClusterUTCModNumber[i] = 0;
	  for(int j=0;j<NUTCmodMAX;j++){
		  ClusterUTCModID[i][j] = 0;
		  ClusterUTCModTime[i][j] = 0;
		  ClusterUTCModEne[i][j] = 0;
	  }
  }



  Int_t    UTCNumber;
  Int_t    UTCModID[256];
  Float_t  UTCEne[256];
  Float_t  UTCTime[256];
  Short_t  UTCPSBit[256];

  if(userFlag>=20190101){  
  	inputTree->SetBranchAddress("UTCNumber",&UTCNumber);
  	inputTree->SetBranchAddress("UTCModID",  UTCModID);
  	inputTree->SetBranchAddress("UTCEne",    UTCEne);
  	inputTree->SetBranchAddress("UTCTime",   UTCTime);
  	inputTree->SetBranchAddress("UTCPSBit",  UTCPSBit);
  	
  	otr->Branch("UTCNumber",&UTCNumber,"UTCNumber/I");
  	otr->Branch("UTCModID",  UTCModID, "UTCModID[UTCNumber]/I");
  	otr->Branch("UTCEne",    UTCEne,   "UTCEne[UTCNumber]/F");
  	otr->Branch("UTCTime",   UTCTime,  "UTCTime[UTCNumber]/F");
  	otr->Branch("UTCPSBit",   UTCPSBit,  "UTCPSBit[UTCNumber]/S");

  	inputTree->SetBranchAddress("ClusterUTCModNumber", ClusterUTCModNumber);
  	inputTree->SetBranchAddress("ClusterUTCModID", ClusterUTCModID);
  	inputTree->SetBranchAddress("ClusterUTCModTime", ClusterUTCModTime);
  	inputTree->SetBranchAddress("ClusterUTCModEne", ClusterUTCModEne);

  	otr->Branch("OriginalClusterUTCModNumber", ClusterUTCModNumber, "OriginalClusterUTCModNumber[120]/I");
  	otr->Branch("OriginalClusterUTCModID", ClusterUTCModID, "OriginalClusterUTCModID[120][20]/I");
  	otr->Branch("OriginalClusterUTCModTime", ClusterUTCModTime, "OriginalClusterUTCModTime[120][20]" );
  	otr->Branch("OriginalClusterUTCModEne", ClusterUTCModEne, "OriginalClusterUTCModEne[120][20]" );
  }


  
  // My cut set and veto set
  Int_t MyCutCondition = 0;
  Int_t MyVetoCondition = 0;
  // Cut parameters
  Double_t DeltaVertexTime = 0;
  Double_t MaxDeltaPi0Mass = 0;
  Double_t MinHalfEt = 0;
  Double_t TotalEt = 0;
  Double_t MinGammaE = 0;
  Double_t MaxGammaE = 0;
  Double_t MaxFiducialR = 0;
  Double_t MinClusterDistance = 0;
  Double_t MinFiducialXY = 0;
  Double_t KLDeltaChisqZ = 0;
  Double_t MaxShapeChisq = 0;
  Double_t ExtraClusterDeltaVertexTime = 0;
  Double_t ExtraClusterEne = 0;
  Double_t KLBeamExitX = 0;
  Double_t KLBeamExitY = 0;
  Double_t AverageClusterTime = 0;
  otr->Branch( "CSIEt", &CSIEt, "CSIEt/D" );
  otr->Branch( "CSIHalfEtR", &CSIHalfEtR, "CSIHalfEtR/D" );
  otr->Branch( "CSIHalfEtL", &CSIHalfEtL, "CSIHalfEtL/D" );
  otr->Branch( "EventStartTime", &EventStartTime, "EventStartTime/D");
  otr->Branch( "EventStartZ", &EventStartZ, "EventStartZ/D");
  otr->Branch( "VertexTime", VertexTime, "VertexTime[6]/D");
  otr->Branch( "OriginalClusterNumber", &OriginalClusterNumber, "OriginalClusterNumber/I" );
  otr->Branch( "OriginalClusterE", OriginalClusterE, "OriginalClusterE[OriginalClusterNumber]/D" );
  otr->Branch( "OriginalClusterT", OriginalClusterT, "OriginalClusterT[OriginalClusterNumber]/D" );
  otr->Branch( "OriginalClusterVertexT", OriginalClusterVertexT, "OriginalClusterVertexT[OriginalClusterNumber]/D" );
  otr->Branch( "MyCutCondition", &MyCutCondition, "MyCutCondition/I" );
  otr->Branch( "MyVetoCondition", &MyVetoCondition, "MyVetoCondition/I" );
  otr->Branch( "DeltaVertexTime", &DeltaVertexTime, "DeltaVertexTime/D" );
  otr->Branch( "MaxDeltaPi0Mass", &MaxDeltaPi0Mass, "MaxDeltaPi0Mass/D" );
  otr->Branch( "MinHalfEt", &MinHalfEt, "MinHalfEt/D" );
  otr->Branch( "TotalEt", &TotalEt, "TotalEt/D");
  otr->Branch( "MinGammaE", &MinGammaE, "MinGammaE/D");
  otr->Branch( "MaxGammaE", &MaxGammaE, "MaxGammaE/D");
  otr->Branch( "MaxFiducialR", &MaxFiducialR, "MaxFiducialR/D");
  otr->Branch( "MinFiducialXY", &MinFiducialXY, "MinFiducialXY/D");
  otr->Branch( "MinClusterDistance", &MinClusterDistance, "MinClusterDistance/D");
  otr->Branch( "KLDeltaChisqZ", &KLDeltaChisqZ, "KLDeltaChisqZ/D" );
  otr->Branch( "MaxShapeChisq", &MaxShapeChisq, "MaxShapeChisq/D" );
  otr->Branch( "ExtraClusterDeltaVertexTime", &ExtraClusterDeltaVertexTime, "ExtraClusterDeltaVertexTime/D" );
  otr->Branch( "ExtraClusterEne", &ExtraClusterEne, "ExtraClusterEne/D" );
  otr->Branch( "KLBeamExitX", &KLBeamExitX, "KLBeamExitX/D" );
  otr->Branch( "KLBeamExitY", &KLBeamExitY, "KLBeamExitY/D" );
  otr->Branch( "AverageClusterTime", &AverageClusterTime, "AverageClusterTime/D" );
  if( !isRealData ){
    otr->Branch( "TruePID",          &TruePID,           "TruePID/I" );
    otr->Branch( "TrueDecayMode",    &TrueDecayMode,     "TrueDecayMode/I" );
    otr->Branch( "TrueVertexZ",      &TrueVertexZ,       "TrueVertexZ/D" );
    otr->Branch( "TrueVertexTime",   &TrueVertexTime,    "TrueVertexTime/D" );
    otr->Branch( "TrueMom",           TrueMom,           "TrueMom[3]/D" );
    otr->Branch( "TruePos",           TruePos,           "TruePos[3]/D" );
    otr->Branch( "GsimGenEventID",   &GsimGenEventID,    "GsimGenEventID/I" );
    otr->Branch( "GsimEntryID",      &GsimEntryID,       "GsimEntryID/I" );
    otr->Branch( "MCEventID",        &MCEventID,         "MCEventID/I" );
    otr->Branch( "AccidentalFileName",   "std::string",          &AccidentalFileName );
    otr->Branch( "AccidentalFileID",     &AccidentalFileID,      "AccidentalFileID/I" );
    otr->Branch( "AccidentalEntryID",    &AccidentalEntryID,     "AccidentalEntryID/L" );
    otr->Branch( "AccidentalOverlayFlag",&AccidentalOverlayFlag, "AccidentalOverlayFlag/O" );
    genP->Branch( otr );
  }

  std::cout<<"Finish Set Branch"<<std::endl;
  
  GammaFinder gFinder;

  // Veto detectors
  Bool_t  DetectorExistFlag[MTBP::nDetectors];
  MTVeto* vetoHandler[MTBP::nDetectors];
  vetoHandler[MTBP::FBAR] = new MTVetoFBAR( userFlag );
  vetoHandler[MTBP::NCC]  = new MTVetoNCC( userFlag );
  vetoHandler[MTBP::CBAR] = new MTVetoCBAR( userFlag );
  vetoHandler[MTBP::BCV]  = new MTVetoBCV( userFlag );
  vetoHandler[MTBP::CV]   = new MTVetoCV( userFlag );
  vetoHandler[MTBP::LCV]  = new MTVetoLCV( userFlag );
  vetoHandler[MTBP::CC03] = new MTVetoCC03( userFlag );
  vetoHandler[MTBP::OEV]  = new MTVetoOEV( userFlag );
  vetoHandler[MTBP::CSI]  = new MTVetoCSI( userFlag );
  vetoHandler[MTBP::CC04] = new MTVetoCC04( userFlag );
  vetoHandler[MTBP::CC05] = new MTVetoCC05( userFlag );
  vetoHandler[MTBP::CC06] = new MTVetoCC06( userFlag );
  vetoHandler[MTBP::BPCV] = new MTVetoBPCV( userFlag );    
  vetoHandler[MTBP::IBCV] = new MTVetoIBCV( userFlag );
  vetoHandler[MTBP::MBCV] = new MTVetoMBCV( userFlag );
  vetoHandler[MTBP::DCV]  = new MTVetoDCV( userFlag);
  vetoHandler[MTBP::UCVLG]= new MTVetoUCVLG(userFlag);

  std::cout<<"Finish making veto Handler"<<std::endl;
  
  MTBHVeto* bhvetoHandler[6];
  bhvetoHandler[0] = new MTVetoBHCV( userFlag );
  bhvetoHandler[1] = new MTVetoBHPV( userFlag );
  bhvetoHandler[2] = new MTVetonewBHCV( userFlag );
  bhvetoHandler[3] = new MTVetoBHGC( userFlag );
  bhvetoHandler[4] = new MTVetoIB( userFlag );
  bhvetoHandler[5] = new MTVetoUCV(userFlag);

  /// prepare FTT container 
  std::vector<LcLib::LcFTTAnaDataHandler*> fttDataVec; 

  for( int idet=0; idet<MTBP::nDetectors; idet++ ){
    ///// Check the existence of the detector.
    ///// If it doesn't exist, DetectorHandler of the detector is not generated.
    if( inputTree->GetBranch(Form("%sNumber",MTBP::detectorName[idet].c_str())) 
	|| inputTree->GetBranch(Form("%sModuleNumber",MTBP::detectorName[idet].c_str())) ){
      DetectorExistFlag[idet] = true;
      std::cout << "Veto Detector : " << MTBP::detectorName[idet].c_str() << " exists." << std::endl;
    }else{
      DetectorExistFlag[idet] = false;
      std::cout<< "Veto Detector : " << MTBP::detectorName[idet].c_str() <<" doesn't exist." << std::endl;
      continue;
    }

    switch( idet ){
    case MTBP::FBAR:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      if( 20180101<=userFlag && userFlag<20180301 ) vetoHandler[idet]->BranchOriginalData( otr );              
      break;
    case MTBP::NCC:
    case MTBP::BCV:
    case MTBP::LCV:
    case MTBP::CSI:
    case MTBP::OEV:
    case MTBP::CC03:
    case MTBP::CC04:
    case MTBP::CC05:
    case MTBP::CC06:
    case MTBP::BPCV:
    case MTBP::IBCV:
    case MTBP::MBCV:
	case MTBP::DCV:
	case MTBP::UCVLG:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      break;
    case MTBP::CV:    
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      vetoHandler[idet]->BranchOriginalData( otr );              
      break;
    case MTBP::CBAR:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      vetoHandler[idet]->BranchOriginalData( otr );              
      break;
    case MTBP::BHCV:
      bhvetoHandler[0]->SetBranchAddress( inputTree );
      bhvetoHandler[0]->Branch( otr );
      break;        
    case MTBP::BHPV:
      bhvetoHandler[1]->SetBranchAddress( inputTree );
      bhvetoHandler[1]->Branch( otr );
      break;                    
    case MTBP::newBHCV:
      bhvetoHandler[2]->SetBranchAddress( inputTree );
      bhvetoHandler[2]->Branch( otr );
      break;                      
    case MTBP::BHGC:
      bhvetoHandler[3]->SetBranchAddress( inputTree );
      bhvetoHandler[3]->Branch( otr );
      break;
    case MTBP::IB:
      bhvetoHandler[4]->SetBranchAddress( inputTree );
      bhvetoHandler[4]->Branch( otr );
      break;                                       
	case MTBP::UCV:
      bhvetoHandler[5]->SetBranchAddress( inputTree );
      bhvetoHandler[5]->Branch( otr );
	  break;
    default:
      break;
    }

    /// FTT 
    if( inputTree->GetBranch(Form("%sFTTChisq", MTBP::detectorName[idet].c_str() )) ){
       LcLib::LcFTTAnaDataHandler *ftt_data = 
                            new LcLib::LcFTTAnaDataHandler( MTBP::detectorName[idet] ); 
       ftt_data->SetBranchAddresses( inputTree );
       ftt_data->AddBranches( otr );
       fttDataVec.push_back( ftt_data );
    }
  }
  std::cout<<"Finish Set Veto Detector"<<std::endl;
  
  vetoHandler[MTBP::CSI]->SetDataContainer( &data );

  for( Int_t iDet=0 ; iDet<MTBP::nDetectors ; iDet++ ){
    if( !DetectorExistFlag[iDet] )
      continue;

    if( !MTBP::is500MHzDetector[iDet] && MTBP::VetoWidth[iDet]>0 ){
      vetoHandler[iDet]->SetVetoThreshold( MTBP::VetoEneThre[iDet] );
      vetoHandler[iDet]->SetVetoWindow( MTBP::VetoWidth[iDet], MTBP::VetoTiming[iDet]  );
    }
  }
  bhvetoHandler[0]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::BHCV] );
  bhvetoHandler[0]->SetVetoWindow( MTBP::VetoWidth[MTBP::BHCV], MTBP::VetoTiming[MTBP::BHCV] );

  bhvetoHandler[1]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::BHPV] );
  bhvetoHandler[1]->SetVetoWindow( MTBP::VetoWidth[MTBP::BHPV], MTBP::VetoTiming[MTBP::BHPV] );

  bhvetoHandler[2]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::newBHCV] );
  bhvetoHandler[2]->SetVetoWindow( MTBP::VetoWidth[MTBP::newBHCV], MTBP::VetoTiming[MTBP::newBHCV] );

  bhvetoHandler[3]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::BHGC] );
  bhvetoHandler[3]->SetVetoWindow( MTBP::VetoWidth[MTBP::BHGC], MTBP::VetoTiming[MTBP::BHGC] );

  bhvetoHandler[4]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::IB] );
  bhvetoHandler[4]->SetVetoWindow( MTBP::VetoWidth[MTBP::IB], MTBP::VetoTiming[MTBP::IB] );
  
  bhvetoHandler[5]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::UCV] );
  bhvetoHandler[5]->SetVetoWindow( MTBP::VetoWidth[MTBP::UCV], MTBP::VetoTiming[MTBP::UCV] );


  // Read data for CV
  Int_t TempCVModuleNumber = 0;
  inputTree->SetBranchAddress( "CVModuleNumber",      &TempCVModuleNumber );

  const double MKL = 497.611;
  // Customized New Output Variables
  Double_t ChargedClusterPos[4][3];
  Double_t GammaClusterPos[2][3];
  Double_t ChargedClusterEne[4];
  Double_t GammaClusterEne[2];

  Double_t RecMassPi0;
  Double_t RecMassKlong;	//For new vertex
  Double_t GamClusRMS_Max;
  Double_t GammaChi2_Max;

  Double_t CVFrontMinDist;
  Double_t CVRearMinDist;  
  Double_t CVFrontMaxDist;
  Double_t CVRearMaxDist;  

  Int_t    NumCVHits;
  Double_t CVFrontHitsEne[4];
  Double_t CVRearHitsEne[4];
  Double_t CVFrontHitsTime[4];
  Double_t CVRearHitsTime[4];
  Double_t CVFrontEstHitsTime[4];
  Double_t CVRearEstHitsTime[4];
  Double_t CVTotalModuleEne;
  Double_t CVTotalHitsEne;
  Int_t    CVClusterModCheck;
  Int_t    FlagFor4CV;

  Double_t COEXY[2];
  Double_t ThreeMass;
  Double_t CVResidualEne;
  Double_t NewVtxZ;
  Double_t RecMassTwoCharged[6];
  Double_t RecMassTwoCharged_Pi0;
  Double_t PtTwoCharged[6];
  Double_t PtFourCharged;

  //write these variables into output tree
  otr->Branch("ChargedClusterPos", ChargedClusterPos, "ChargedClusterPos[4][3]/D");
  otr->Branch("GammaClusterPos", GammaClusterPos, "GammaClusterPos[2][3]/D");
  otr->Branch("ChargedClusterEne", ChargedClusterEne, "ChargedClusterEne[4]/D");
  otr->Branch("GammaClusterEne", GammaClusterEne, "GammaClusterEne[2]/D");
  otr->Branch("RecMassPi0", &RecMassPi0,"RecMassPi0/D");
  otr->Branch("RecMassKlong", &RecMassKlong,"RecMassKlong/D");
  otr->Branch("GamClusRMS_Max", &GamClusRMS_Max,"GamClusRMS_Max/D");
  otr->Branch("GammaChi2_Max", &GammaChi2_Max,"GammaChi2_Max/D");

  otr->Branch("CVFrontMinDist", &CVFrontMinDist,"CVFrontMinDist/D");
  otr->Branch("CVRearMinDist", &CVRearMinDist,"CVRearMinDist/D");
  otr->Branch("CVFrontMaxDist", &CVFrontMaxDist,"CVFrontMaxDist/D");
  otr->Branch("CVRearMaxDist", &CVRearMaxDist,"CVRearMaxDist/D");

  otr->Branch("NumCVHits", &NumCVHits,"NumCVHits/I");
  otr->Branch("CVFrontHitsEne", CVFrontHitsEne,"CVFrontHitsEne[4]/D");
  otr->Branch("CVRearHitsEne", CVRearHitsEne,"CVRearHitsEne[4]/D");
  otr->Branch("CVFrontHitsTime", CVFrontHitsTime,"CVFrontHitsTime[4]/D");
  otr->Branch("CVRearHitsTime", CVRearHitsTime,"CVRearHitsTime[4]/D");
  otr->Branch("CVFrontEstHitsTime", CVFrontEstHitsTime,"CVFrontEstHitsTime[4]/D");
  otr->Branch("CVRearEstHitsTime", CVRearEstHitsTime,"CVRearEstHitsTime[4]/D");
  otr->Branch("CVTotalModuleEne",&CVTotalModuleEne,"CVTotalModuleEne/D");
  otr->Branch("CVTotalHitsEne",&CVTotalHitsEne,"CVTotalHitsEne/D");
  otr->Branch("CVClusterModCheck",&CVClusterModCheck,"CVClusterModCheck/I");
  otr->Branch("FlagFor4CV", &FlagFor4CV,"FlagFor4CV/I");

  otr->Branch("COEXY", COEXY,"COEXY[2]/D");  
  otr->Branch("ThreeMass", &ThreeMass,"ThreeMass/D");
  otr->Branch("CVResidualEne", &CVResidualEne,"CVResidualEne/D");
  otr->Branch("NewVtxZ", &NewVtxZ,"NewVtxZ/D");
  otr->Branch("RecMassTwoCharged", RecMassTwoCharged,"RecMassTwoCharged[6]/D");  
  otr->Branch("RecMassTwoCharged_Pi0", &RecMassTwoCharged_Pi0,"RecMassTwoCharged_Pi0/D");  
  otr->Branch("PtTwoCharged", PtTwoCharged,"PtTwoCharged[6]/D");  
  otr->Branch("PtFourCharged", &PtFourCharged,"PtFourCharged/D");  

  //logtree variables
  Int_t NumberEventFourCV = 0;
  Int_t NumberEvent6Clusters = 0;
  Int_t NumberEventStdCut = 0;
  Int_t Num4CVCut = 0;		//Events that got cut for below 4 CV hits
  Int_t NumVetoCut = 0;		//Events that got cut from Veto

  logtr->Branch("NumberEvent6Clusters",&NumberEvent6Clusters,"NumberEvent6Clusters/I");
  logtr->Branch("NumberEventStdCut",&NumberEventStdCut,"NumberEventStdCut/I");
  logtr->Branch("NumberEventFourCV",&NumberEventFourCV,"NumberEventFourCV/I");
  logtr->Branch("Num4CVCut",&Num4CVCut,"Num4CVCut/I");
  logtr->Branch("NumVetoCut",&NumVetoCut,"NumVetoCut/I");

  // loop analysis------------------------------------------------
  for( int entry=0; entry<nEntries; entry++ ){
    if( nEntries>100 )
      if( entry%(nEntries/10)==0 )
	std::cout << entry/(nEntries/100) << "%" << std::endl;

    // Initialize
    MyCutCondition = 0;
    MyVetoCondition = 0;
    OriginalClusterNumber = 0;
    DeltaVertexTime = 0;
    MaxDeltaPi0Mass = 0;
    MinHalfEt = 0;
    MinGammaE = 9999;
    MaxGammaE = 0;
    MaxFiducialR = 0;
    MinFiducialXY = 9999;
    MinClusterDistance = 1E15;
    KLDeltaChisqZ = 0;
    MaxShapeChisq = 0;
    ExtraClusterDeltaVertexTime = 9999;
    ExtraClusterEne = 0;
    KLBeamExitX = 0;
    KLBeamExitY = 0;
    AverageClusterTime = -9999.;
    TotalEt=0;
    
    // read data
    inputTree->GetEntry( entry );

    ClusteringEntryID = entry;

    std::list<Cluster> clist;
    data.getData(clist);

    // Store original clusters information
    for( std::list<Cluster>::iterator it=clist.begin(); it!=clist.end(); it++ ){
      OriginalClusterE[OriginalClusterNumber] = it->e();
      OriginalClusterT[OriginalClusterNumber] = it->t();
      OriginalClusterVertexT[OriginalClusterNumber] = it->t();
      OriginalClusterPos[OriginalClusterNumber][0] = it->x();
      OriginalClusterPos[OriginalClusterNumber][1] = it->y();
      OriginalClusterPos[OriginalClusterNumber][2] = it->z();
      
      OriginalClusterNumber++;
    }
    bool* clusterUsageFlag = new bool[OriginalClusterNumber]();
    for( int icluster=0; icluster<OriginalClusterNumber; icluster++ ){
      clusterUsageFlag[icluster] = false;
    }


    // gamma finding
    std::list<Gamma> glist;
    gFinder.findGamma( clist, glist );
    // energy is first time corrected in findGamma function
    // check number of gammas
    if( glist.size() < 6 ){
      data.KlongNumber   = 0;
      data.Pi0Number     = 0;
      data.GammaNumber   = 0;
      data.GamClusNumber = 0;
      //otr->Fill();
      data.eventID++;
      continue; 
    }


    // If there are more than 6 clusters, I select most near time 6 clusters
    std::vector< double > clusterTimeVec;
    for( std::list<Gamma>::iterator it=glist.begin(); it!=glist.end(); it++ ){
      clusterTimeVec.push_back( it->t() );
    }

    while( glist.size() != 6 ){
      std::sort( clusterTimeVec.begin(), clusterTimeVec.end() );
      while( clusterTimeVec.size() != 6 ){
	double averageTime = std::accumulate( clusterTimeVec.begin(), clusterTimeVec.end(), 0.0);
	averageTime /= clusterTimeVec.size();
	if( averageTime-clusterTimeVec[0] > clusterTimeVec[clusterTimeVec.size()-1]-averageTime ){
	  clusterTimeVec.erase( clusterTimeVec.begin() );
	}else{
	  clusterTimeVec.pop_back();
	}
      }

      for( std::list<Gamma>::iterator it=glist.begin(); it!=glist.end(); ){
	bool rejectFlag = true;
	for( unsigned int index=0; index<clusterTimeVec.size(); index++ ){
	  if( it->t() == clusterTimeVec[index] ){
	    rejectFlag = false;
	    break;
	  }
	}
	if( rejectFlag ){
	  it = glist.erase( it );
	  continue;
	}
	it++;
      }
    }

    std::vector<Klong> klVec;
    //calculate COE here
    FindCOEXY( COEXY, glist ); 

    if( !recpi04e( glist, klVec, userFlag, COEXY) ){
      data.KlongNumber   = 0;
      data.Pi0Number     = 0;
      data.GammaNumber   = 0;
      data.GamClusNumber = 0;
      //otr->Fill();
      data.eventID++;
      continue; 
    }

    // this function finds the Gamma momentum/direction
    FindGammaMom(glist, klVec[0].vz(), COEXY);
    // do the shape chi 2
    std::list<Gamma> NewGlist;
    for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      E14GNAnaFunction::getFunction()->shapeChi2(g);
      NewGlist.push_back( g );
    }
    glist = NewGlist;

// Correct the gamma pos and ene with initial vertex
    std::list<Gamma> NewGlist1;
    int indexgamma = 0;
    for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      indexgamma ++;
      E14GNAnaFunction::getFunction()->correctEnergyWithAngle(g);
      E14GNAnaFunction::getFunction()->correctPosition(g);
      NewGlist1.push_back( g );
    }
    glist = NewGlist1;

    klVec.clear();
// re-reconstruction with corrected ene and pos
    if( !recpi04e( glist, klVec, userFlag, COEXY) ){
      data.KlongNumber   = 0;
      data.Pi0Number     = 0;
      data.GammaNumber   = 0;
      data.GamClusNumber = 0;
      //otr->Fill();
      data.eventID++;
      continue; 
    }
// re-find the gamma momentum
    FindGammaMom(glist, klVec[0].vz(), COEXY);

// find the klong momentum
    Double_t TmpKLMom[3] = {0};
    for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      TmpKLMom[0] += g.p3().x(); 
      TmpKLMom[1] += g.p3().y(); 
      TmpKLMom[2] += g.p3().z(); 
    }
    klVec[0].setP3( TmpKLMom[0], TmpKLMom[1], TmpKLMom[2] ); 

// add gamma into klVec
    for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      klVec[0].addGamma( g );
    }

    // Vertex timing reconstruction
    EventStartTime = GetEventStartTimeNew( klVec, VertexTime, CSIZPosition , COEXY);
    EventStartZ = klVec[0].vz();

    data.setData(klVec);

    // find which clusters are included
    for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      clusterUsageFlag[g.id()] = true;
    }

    bool Only6gFlag = true;
    for( int icluster=0; icluster<OriginalClusterNumber; icluster++ ){
      OriginalClusterVertexT[icluster] = OriginalClusterT[icluster] - sqrt( pow( CSIZPosition-EventStartZ, 2) + pow( OriginalClusterPos[icluster][0], 2) + pow( OriginalClusterPos[icluster][1], 2) ) / (TMath::C()/1E6);
      if( clusterUsageFlag[icluster] ) continue; // This cluster is included in KL
      if(fabs(OriginalClusterVertexT[icluster]-EventStartTime)<10 && OriginalClusterE[icluster]>20){
	Only6gFlag=false;
      }
    }

    if(!Only6gFlag){
      data.eventID++;
      continue;
    }
    NumberEvent6Clusters ++;
    
    //std::cout << "\n entry is "<< entry;
// Fiducial Cut Condition-----------------------------------
// ----------------------------------------------------------
    MinHalfEt = std::min( CSIHalfEtR, CSIHalfEtL );
    double clusterPos[6][2]; // ( x, y)
    AverageClusterTime = 0;

    for( int igamma=0; igamma<6; igamma++ ){
      if( DeltaVertexTime < fabs( EventStartTime-VertexTime[igamma] ) ){
	DeltaVertexTime = fabs( EventStartTime-VertexTime[igamma] );
      }
    }

    KLBeamExitX = klVec[0].vx() / ( klVec[0].vz()-MTBP::T1TargetZ) * ( MTBP::BeamExitZ-MTBP::T1TargetZ);
    KLBeamExitY = klVec[0].vy() / ( klVec[0].vz()-MTBP::T1TargetZ) * ( MTBP::BeamExitZ-MTBP::T1TargetZ);

    if( klVec.size() == 1 ){
      KLDeltaChisqZ = 0;
    }else{
      KLDeltaChisqZ = klVec[1].chisqZ() - klVec[0].chisqZ();
    }

    int iglist = 0;	
    for( std::list<Gamma>::iterator g1=glist.begin();
       g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      if( MinGammaE > g.e() ) MinGammaE = g.e();
      if( MaxGammaE < g.e() ) MaxGammaE = g.e();
      if( MaxFiducialR < g.pos().perp() )           MaxFiducialR = g.pos().perp();
      if( MinFiducialXY > std::max( fabs(g.x()), fabs(g.y())) ) 
	MinFiducialXY = std::max( fabs(g.x()), fabs(g.y()) );
      if( MaxShapeChisq < g.chisq() ) 
	MaxShapeChisq = g.chisq();
      clusterPos[iglist][0] = g.x();
      clusterPos[iglist][1] = g.y();
      AverageClusterTime += g.t();
      TotalEt+=g.e();
      iglist++;
    } 

    for( int igamma=0; igamma<6; igamma++ ){
      for( int jgamma=igamma+1; jgamma<6; jgamma++ ){
	double tempDistance = pow( clusterPos[igamma][0]-clusterPos[jgamma][0], 2) + pow( clusterPos[igamma][1]-clusterPos[jgamma][1], 2);
	if( MinClusterDistance > tempDistance ){
	  MinClusterDistance = tempDistance;
	}
      }
    }
    MinClusterDistance = sqrt( MinClusterDistance );

    AverageClusterTime /= 6;

    if( true ){
      if( DeltaVertexTime > 3. ){
	MyCutCondition |= (1<<DELTA_VERTEX_TIME);
      }
      if( fabs( klVec[0].m() - MTBP::KL_MASS ) > 15. ){
	MyCutCondition |= (1<<DELTA_KL_MASS);
      }
      if( TotalEt < 650. ){
	MyCutCondition |= (1<<CsIEt);
      }
      if( klVec[0].p3().perp() > 50. ){
	MyCutCondition |= (1<<KL_PT);
      }
      if( klVec[0].chisqZ() > 20. ){
	MyCutCondition |= (1<<KL_CHISQZ);
      }
      if( MaxDeltaPi0Mass > 10. ){
	MyCutCondition |= (1<<DELTA_PI0_MASS);
      }
      if( klVec[0].deltaZ() > 400. ){
	MyCutCondition |= (1<<DELTA_PI0_Z);
      }
      if( MinGammaE < 50. ){
	MyCutCondition |= (1<<MIN_GAMMAE);
      }
      if( MinFiducialXY < 150. ){
	MyCutCondition |= (1<<MIN_FIDUCIAL_XY);
      }
      if( MaxFiducialR > 850 ){
	MyCutCondition |= (1<<MAX_FIDUCIALR);
      }
      if( MinClusterDistance < 150. ){
	MyCutCondition |= (1<<MIN_CLUSTER_DISTANCE);
      }
      if( EventStartZ<3000 || 5000<EventStartZ ){
	MyCutCondition |= (1<<KL_Z);
      }
    }
//MyCutCondition end ----------------------------------------------

//MyVetoCondition -------------------------------------------------
    if( OriginalClusterNumber > 6 ){
      for( int icluster=0; icluster<OriginalClusterNumber; icluster++ ){
	if( clusterUsageFlag[icluster] ) continue; // This cluster is included in KL
	if( fabs( ExtraClusterDeltaVertexTime ) > fabs( OriginalClusterVertexT[icluster] - EventStartTime ) ){
	  ExtraClusterDeltaVertexTime = OriginalClusterVertexT[icluster] - EventStartTime;
	  ExtraClusterEne = OriginalClusterE[icluster];
	}
      }
    }

    // veto detector check
    vetoHandler[MTBP::CSI]->SetKlInfo( &(klVec[0]) );


    for( int idet=0; idet<MTBP::nDetectors; idet++ ){
      if( !DetectorExistFlag[idet] )
	continue;
      switch( idet ){
      case MTBP::FBAR:
      case MTBP::OEV:
      case MTBP::CBAR:
      case MTBP::CC03:
      case MTBP::CC04:
      case MTBP::CC05:
      case MTBP::CC06:
      case MTBP::BCV:
      case MTBP::LCV:
      case MTBP::NCC:
      case MTBP::CV:	
      case MTBP::DCV:
      case MTBP::UCVLG:
	vetoHandler[idet]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( vetoHandler[idet]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;
      case MTBP::IBCV:
      case MTBP::MBCV:
      case MTBP::BPCV:
	vetoHandler[idet]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( vetoHandler[idet]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;
      case MTBP::CSI:
	vetoHandler[idet]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( vetoHandler[idet]->Process( 6 ) ){
	  MyVetoCondition |= (1<<idet);
	}	
	break;
	
      case MTBP::BHCV:
	bhvetoHandler[0]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[0]->Process( 2 ) ){
	  if(userFlag<20160101)MyVetoCondition |= (1<<idet);
	}
	break;	
      case MTBP::BHPV:
	bhvetoHandler[1]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[1]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;
	
      case MTBP::newBHCV:
	bhvetoHandler[2]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[2]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;
	
      case MTBP::BHGC:
	bhvetoHandler[3]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[3]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;

      case MTBP::IB:
	bhvetoHandler[4]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[4]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;
	case MTBP::UCV:
	bhvetoHandler[5]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[5]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;

      default:
	break;
      }
    }
    
    
    if( fabs(ExtraClusterDeltaVertexTime) < 10. ){
      MyVetoCondition |= (1<<31);
    }
//MyVetoCondition  ----------------------------------
//For Log Tree
    if( MyVetoCondition & (255<<0) || MyVetoCondition & (1<<9) ||
	MyVetoCondition & (2047<<11) ){
	NumVetoCut++;
    }
    //Veto Cut
    if( MyVetoCondition & (1023<<0) || MyVetoCondition & (2047<<11) ){
	//otr->Fill();
	data.eventID++;
      	continue; 
    }
    //Fiducial Cut
    if( MyCutCondition & (1<<7) || MyCutCondition & (7<<9) ||
	MyCutCondition & (1<<12) ){
	//otr->Fill();
      	data.eventID++;
      	continue;
    }      

    NumberEventStdCut ++;
//  Standard Cuts end -----------------------------------------

//  initiallize the CV Coordinates
    CVDetector MyCVCoordinates;
    MyCVCoordinates.initialize();

    CV_Class* CV;
    CV = new CV_Class(TempCVModuleNumber,inputTree,entry);
    CV->HitExists = 0;

//  check if all cluster occupy different module
    bool CVModCheckbit = CVModCheck(glist, CV, EventStartZ, MyCVCoordinates, COEXY);
    std::cout<<"\n CV Module check is "<< CVModCheckbit;

    if( CVModCheckbit ){
    	CVClusterModCheck = 1;
    }else{
    	CVClusterModCheck = 0;
    }

//  CV Mapping

    CVMapping(glist, CV, EventStartZ, MyCVCoordinates, COEXY);
    RemoveTreeAddress(inputTree);

    NumCVHits = CV->HitExists;
//  check if there are four hits on CV
    if( CV->HitExists != 4 || CheckDoubleCount(CV) >=1){
      	Num4CVCut ++;
	FlagFor4CV = 0; 
	otr->Fill();
      	continue;
    }
    if( CV->HitExists == 4){
        NumberEventFourCV++;
	FlagFor4CV = 1; 
    }
//  write Cluster info into charged and gamma separately
    for(int i=0; i<4; i++){
      int pos = 0;
      pos = CV->ChargeEntry[i];
      std::list<Gamma>::iterator git = glist.begin();
      std::advance(git, pos );
      Gamma tmpG = *git;
      ChargedClusterEne[i] = tmpG.e();
      ChargedClusterPos[i][0] = tmpG.pos().x();
      ChargedClusterPos[i][1] = tmpG.pos().y();
      ChargedClusterPos[i][2] = tmpG.pos().z();
    }
    for(int i=0; i<2; i++){
      int pos = 0;
      pos = CV->GammaEntry[i];
      std::list<Gamma>::iterator git = glist.begin();
      std::advance(git, pos );
      Gamma tmpG = *git;
      GammaClusterEne[i] = tmpG.e();
      GammaClusterPos[i][0] = tmpG.pos().x();
      GammaClusterPos[i][1] = tmpG.pos().y();
      GammaClusterPos[i][2] = tmpG.pos().z();
    }
    Double_t Gamma1Pos[3];
    Double_t Gamma2Pos[3];
    for(int i=0;i<3;i++){
      Gamma1Pos[i] = GammaClusterPos[0][i];
      Gamma2Pos[i] = GammaClusterPos[1][i];
    }

// compute the closest invariant mass of combination of
// 2 charged and 1 gamma
   Double_t Vtx[3];
   Vtx[2] = EventStartZ;
   Vtx[0] = COEXY[0];
   Vtx[1] = COEXY[1];
   ThreeMass = ThreeParticlesMass( ChargedClusterEne, ChargedClusterPos, GammaClusterEne, GammaClusterPos, Vtx );

// calculate some variables for cut
   RecMassPi0 = sqrt( RecPi0Mass( GammaClusterEne[0],GammaClusterEne[1],
		 Gamma1Pos,Gamma2Pos,EventStartZ,COEXY) + pow(134.98,2));
   //std::cout<<"\n Rec Pi0 Mass is" << RecMassPi0;

// write CV Variables into output tree
   CV->CVMinDist();
   CV->CVMaxDist();
   CVFrontMinDist = CV->MinFrontDist;
   CVRearMinDist = CV->MinRearDist;
   CVFrontMaxDist = CV->MaxFrontDist;
   CVRearMaxDist = CV->MaxRearDist;

   for(int i=0; i<4; i++){
     CVFrontHitsEne[i] = CV->HitsFrontEne[i]; 
     CVRearHitsEne[i] = CV->HitsRearEne[i]; 
     CVFrontHitsTime[i] = CV->HitsFrontTime[i]; 
     CVRearHitsTime[i] = CV->HitsRearTime[i];
     CVFrontEstHitsTime[i] = CV->HitsFrontEstTime[i]; 
     CVRearEstHitsTime[i] = CV->HitsRearEstTime[i]; 
   } 

   CVResidualEne = CalculateCVResidualEne(  CVFrontHitsEne, CVRearHitsEne, CV , CVTotalModuleEne, CVTotalHitsEne);  

   //reconstruct new vertex based on pi0 mass
   NewVtxZ = recVtxPi0(5500, 2500, GammaClusterEne[0], GammaClusterEne[1], Gamma1Pos, Gamma2Pos, COEXY);

   //correct momentum based on new vertex
   FindGammaMom(glist, NewVtxZ, COEXY);

   //correct momentum for electron positron
   const Double_t e_mass = 0.511;
   std::list<Gamma> tmpGlist;
   int e_index = 0;
   for(int i=0; i<6; i++){
      std::list<Gamma>::iterator git = glist.begin();
      std::advance(git, i );
      Gamma tmpG = *git;
      if( i == CV->ChargeEntry[e_index]){
      	e_index++;
      	Double_t e_Ene = tmpG.e();
      	CLHEP::Hep3Vector e_Mom = tmpG.p3();
      	Double_t ratio = e_Ene/sqrt(pow(e_Ene,2) - pow(e_mass,2) );
      	tmpG.setP3( e_Mom.x()*ratio, e_Mom.y()*ratio, e_Mom.z()*ratio );
      }
      tmpGlist.push_back( tmpG );
   }
   glist = tmpGlist;
 
   //correct shower leakage based on new momentum
   tmpGlist.clear();
   indexgamma = 0;
   for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      indexgamma ++;
      E14GNAnaFunction::getFunction()->correctEnergyWithAngle(g);
      E14GNAnaFunction::getFunction()->correctPosition(g);
      tmpGlist.push_back( g );
   }
   glist = tmpGlist;
   //find new klong momentum
    TmpKLMom[3] = {0};
    for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      TmpKLMom[0] += g.p3().x();
      TmpKLMom[1] += g.p3().y();
      TmpKLMom[2] += g.p3().z();
    }
    klVec[0].setP3( TmpKLMom[0], TmpKLMom[1], TmpKLMom[2] );
   // add updated gamma into klVec
    klVec[0].gamma().clear(); //clear gammas first
    for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      klVec[0].addGamma( g );
    }
    data.setData(klVec);

// RecMass of Klong
   RecMassKlong = sqrt( recVtxMFunction( glist, NewVtxZ, COEXY) + pow( MKL, 2) ); 

// RecMass of two charged particles (6 combinations);
   int tmpindex = 0;
   for(int i=0; i<3; i++){
	for(int j=i+1; j<4; j++){
		RecMassTwoCharged[tmpindex] = sqrt(
		RecPi0Mass( ChargedClusterEne[i], ChargedClusterEne[j],
		ChargedClusterPos[i], ChargedClusterPos[j], NewVtxZ,
		COEXY) + pow(134.98,2) );
		tmpindex++;
	}
   }
// Finding the closest to pi0 mass
   Double_t tmpDiff = 10000;
   const Double_t Pi0Mass = 135;
   for(int i=0; i<6; i++){
 	if(std::abs(RecMassTwoCharged[i] - Pi0Mass) < tmpDiff){
        	tmpDiff = std::abs(RecMassTwoCharged[i] - Pi0Mass);
               	RecMassTwoCharged_Pi0 = RecMassTwoCharged[i];
     	}
   }
// Find 2e pt[6] and 4e pt
   FindChargedPt(glist, CV, PtFourCharged, PtTwoCharged);

// Find GamClusRMS_Max and GammaChi2_Max
   FindFusionParameters(glist, GamClusRMS_Max, GammaChi2_Max);


// All Kinematic Cuts and Fusion Cuts applied here
/*   if( 	RecMassPi0 < 126 	|| RecMassPi0 > 148.4 	||
 	CVFrontMinDist < 250 	|| CVRearMinDist < 300	||
	GammaChi2_Max  > 6	|| GamClusRMS_Max > 40	||
	CVClusterModCheck == 0	|| CVResidualEne > 0.05 ){
	otr->Fill(); 
	continue;
	data.eventID++;
   }
*/
//UTC variables
   for( int j=0;j<120;j++){
        GammaUTCEne[j] = 0;
        GammaUTCTime[j] = 0;
        GammaUTCWeightedTime[j] = 0;
        GammaUTCCorrectedTime[j] = 0;
        GammaUTCCorrectedWeightedTime[j] = 0;
   }
   int isbadUTCevent = 0;
   for( int j=0;j<data.GammaNumber;j++){

       	int id = data.GammaId[j];

   	double UTCEne = 0;
   	double UTCTime = ClusterUTCModTime[id][0];
   	double UTCWeightedTime = 0;

   	for( int k=0;k<ClusterUTCModNumber[id];k++){

        	int modid = ClusterUTCModID[id][k];
        	if( utcconf->CheckHVcondition(modid)<0){
            	    isbadUTCevent = 1;
            	    break;
        	}
        	double e = ClusterUTCModEne[id][k];
        	if( e>20.0 || k==0){
            	    UTCEne += e;
            	    UTCWeightedTime += ClusterUTCModTime[id][k]*e;
        	}
   	}
        UTCWeightedTime /= UTCEne;

        GammaUTCEne[j] = UTCEne;
        GammaUTCTime[j] = UTCTime;
        GammaUTCWeightedTime[j] = UTCWeightedTime;

   }


   otr->Fill();
   data.eventID++;

   delete[] clusterUsageFlag;
  }

  otf->cd();
  logtr->Fill();
  logtr->Write();
  otr->Write();
  otf->Close();

  std::cout<<"\nEvents with 6 clusters is"<<NumberEvent6Clusters;
  std::cout<<"\nEvents pass Veto and Fiducial cut is"<<NumberEventStdCut;
  std::cout<<"\nEvents with four cv hits is"<<NumberEventFourCV;

  return 0;
}

