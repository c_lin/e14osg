#include "../header/cvclass.h"

Double_t Distance( const array<Double_t, 3>& a, const array<Double_t, 3>& b);

CV_Class::CV_Class(int TempCVModuleNumber, TTree* itree, int EntryNum)
{
	ModuleNumber = TempCVModuleNumber;

	ModuleEnergy.resize(ModuleNumber);
	ModuleID.resize(ModuleNumber);
	ModuleTime.resize(ModuleNumber);

	itree->SetBranchAddress("CVModuleEne", ModuleEnergy.data());
	itree->SetBranchAddress("CVModuleModID", ModuleID.data());
	itree->SetBranchAddress("CVModuleHitTime", ModuleTime.data());
	itree->GetEntry(EntryNum);
	
	return;
}

void RemoveTreeAddress(TTree* itree)
{
        itree->SetBranchAddress("CVModuleEne", nullptr);
        itree->SetBranchAddress("CVModuleModID", nullptr);
        itree->SetBranchAddress("CVModuleHitTime", nullptr);
	
	return;
}

bool RadOverlap(Double_t R, Double_t* C, Double_t* X1, Double_t* X2)
{
	Double_t Xn[2];

        Xn[0] = max(X1[0], min(C[0], X2[0]));
        Xn[1] = max(X1[1], min(C[1], X2[1]));

        Double_t Dx = Xn[0] - C[0];
        Double_t Dy = Xn[1] - C[1];

        return (Dx * Dx + Dy * Dy) <= R * R;
}


void CV_Class::CVMinDist()
{
	int n = HitExists;
	MinFrontDist = Distance( HitsFrontPos[0], HitsFrontPos[1] );
	MinRearDist = Distance( HitsRearPos[0], HitsRearPos[1] );

	for(int i=0; i<n; i++)
	{
	  for(int j=i+1; j<n; j++)
	  {	
	    Double_t FrontDist = Distance(HitsFrontPos[i], HitsFrontPos[j]);
	    Double_t RearDist = Distance(HitsRearPos[i], HitsRearPos[j]);
	    if(MinFrontDist > FrontDist)
	      MinFrontDist = FrontDist;
	    if(MinRearDist > RearDist)
	      MinRearDist = RearDist;
	  }
	}
	return;
}

void CV_Class::CVMaxDist()
{
	int n = HitExists;
	MaxFrontDist = Distance( HitsFrontPos[0], HitsFrontPos[1] );
	MaxRearDist = Distance( HitsRearPos[0], HitsRearPos[1] );

	for(int i=0; i<n; i++)
	{
	  for(int j=i+1; j<n; j++)
	  {	
	    Double_t FrontDist = Distance(HitsFrontPos[i], HitsFrontPos[j]);
	    Double_t RearDist = Distance(HitsRearPos[i], HitsRearPos[j]);
	    if(MaxFrontDist < FrontDist)
	      MaxFrontDist = FrontDist;
	    if(MaxRearDist < RearDist)
	      MaxRearDist = RearDist;
	  }
	}
	return;
}

