#include <iostream>
#include <list>
#include "klong/RecKlong.h"
#include "gnana/E14GNAnaFunction.h"
#include "ShapeChi2/ShapeChi2New.h"

bool user_rec(std::list<Gamma> const &glist, std::vector<Klong> &klVec, int userFlag)
{
  ///// reconstruction
  static RecKlong recKl;      
  //  klVec = recKl.recK3pi0(glist,VERTEX_FIX_XYZERO) ;
  klVec = recKl.recK3pi0(glist, userFlag) ;  
  if(klVec.size()==0)
    return false;
  
  ///// gamma position & energy correction for angle dependency
  E14GNAnaFunction::getFunction()->correctPosition(klVec[0]);  
  E14GNAnaFunction::getFunction()->correctEnergyWithAngle(klVec[0]);  

  ///// re-reconstruction with corrected gamma
  std::list<Gamma> glist2;
  for( std::vector<Pi0>::iterator it=klVec[0].pi0().begin();
       it!=klVec[0].pi0().end(); it++){
    glist2.push_back(it->g1());
    glist2.push_back(it->g2());
  }
  klVec = recKl.recK3pi0(glist2, userFlag) ;
  if(klVec.size()==0)
    return false;
  
  ///// shape chi2 evaluation
  //E14GNAnaFunction::getFunction()->shapeChi2(klVec[0]);
  static ShapeChi2New chi2Newcalc;
  for( Int_t iPi0 = 0 ; iPi0 < 3 ; ++iPi0 ){
    for( Int_t iGamma = 0 ; iGamma < 2 ; ++iGamma ){
      Gamma& gam = ( (iGamma==0) ? 
		     klVec[0].pi0()[iPi0].g1() :
		     klVec[0].pi0()[iPi0].g2() );
      chi2Newcalc.shapeChi2(gam);
    }
  }

  return true;
}
