#include <iostream>
#include <list>
#include "klong/RecKlong.h"
#include "gnana/E14GNAnaFunction.h"
#include "ShapeChi2/ShapeChi2New.h"
#include <cmath>
#include "TMath.h"
#include "MTAnalysisLibrary/MTFunction.h"
#include <set>
#include "../header/cvdetector.h"
#include "../header/cvclass.h"

const Double_t CVFrontZ = 5842 + 20;
const Double_t CVRearZ = 6093 + 20;
const Double_t CSIZ = 6168;

int recVtxBisection(double Zmax, double Zmin, std::list<Gamma> glist, Klong &klong, Double_t COEXY[2]);

bool recpi04e(std::list<Gamma> glist, std::vector<Klong> &klVec, int userFlag, Double_t COEXY[2])
{
	Klong klong;
	klong.setUserFlag( userFlag );
	//reconstruct vertex
	if(recVtxBisection(5500, 2500, glist, klong, COEXY) == 1)
		klVec.push_back( klong );

	if(klVec.size() != 1)
		return false;
	return true;
}

Double_t recVtxMFunction(std::list<Gamma> glist, Double_t VtxZ, Double_t COEXY[2])
{
	const double MKL = 497.611;
	int id=0;
	Double_t x[6], y[6], z[6];      //cluster Pos on CSI
        Double_t E[6];                  //cluster energy
        Double_t DistCluVtx[6];         //distance between vertex and cluster
        Double_t DistBtwClu[6][6];      //distance between two clusters
        Double_t CosAngle[6][6];        //cos of the opening angle between any two cluster to vertex
        Double_t Function = 0;          //the function we want to find the root which is the invariant mass of Klong = sum of E

	for( std::list<Gamma>::iterator g1=glist.begin();
             g1!=glist.end(); g1++ )
	{
		Gamma g = *g1;
		x[id] = g.pos().x() - COEXY[0];
		y[id] = g.pos().y() - COEXY[1];
		z[id] = 6168;
		E[id] = g.e();
		DistCluVtx[id] = sqrt( pow(z[id]-VtxZ,2) + pow(x[id],2) + pow(y[id],2) );
		id++;
	}

        for(int i=0;i<6;i++)
        {
                for(int j=0;j<6;j++)
                {
                        DistBtwClu[i][j] = sqrt( pow(x[i]-x[j],2) + pow(y[i]-y[j],2) );
                        CosAngle[i][j] = ( pow(DistCluVtx[i],2) + pow(DistCluVtx[j],2) - pow(DistBtwClu[i][j],2) ) / (2*DistCluVtx[i]*DistCluVtx[j]);
                        Function += E[i]*E[j]*(1-CosAngle[i][j]);
                }
        }
        Function -= pow(MKL,2);          //rest mass of Klong
	return Function;
}		

int recVtxBisection(double Zmax, double Zmin, std::list<Gamma> glist, Klong &klong, Double_t COEXY[2])	
//find solution for the MFunction
{
	double Z[1000];
	int n = 1000;
        int NumSolution = 0;
        Double_t a, b, c, VtxZ = 0;    // c is mid point of a and b
        Double_t ErrZ = 0.1;
        for(int i=0; i<n; i++)
        {
                Z[i] = Zmin + (Zmax-Zmin)*i/n;
        }

        for(int i=0; i<n-1; i++)
        {
                if(recVtxMFunction(glist, Z[i], COEXY)*recVtxMFunction(glist, Z[i+1], COEXY) < 0)
                {
                        NumSolution ++;
                        a = Z[i];
                        b = Z[i+1];
                        while(recVtxMFunction(glist, a, COEXY)*recVtxMFunction(glist, b, COEXY) < 0 && b-a >= ErrZ)
                        {
                                c = (a+b)/2;
                                if(recVtxMFunction(glist, a, COEXY)*recVtxMFunction(glist, c, COEXY) < 0)
                                        b = c;
                                else
                                        a = c;
                                if(recVtxMFunction(glist, c, COEXY) == 0)
                                        break;
                        }
                        VtxZ = c;
                }
        }
	if(NumSolution == 1)
		klong.setVtx( COEXY[0], COEXY[1], VtxZ);

	return NumSolution;
}

void FindGammaMom( std::list<Gamma> &glist, Double_t VertexZ, Double_t COEXY[2])
{
	double x,y,z,magn,E,px,py,pz;
	int id = 0;
	std::list<Gamma> Newlist;
	for( std::list<Gamma>::iterator g1=glist.begin();
             g1!=glist.end(); g1++ )
	{
		Gamma g = *g1;
		x = g.pos().x() - COEXY[0];
		y = g.pos().y() - COEXY[1];
		z = 6168 - VertexZ;
		E = g.e();
		magn = sqrt( pow(x,2)+pow(y,2)+pow(z,2) );
		px = E*(x/magn);
		py = E*(y/magn);
		pz = E*(z/magn);
		g.setP3( px, py, pz);
		Newlist.push_back(g);
		id++;
	}
	glist = Newlist;
	return;
}


void CVMapping( std::list<Gamma> glist, CV_Class *CV, Double_t EventStartZ, CVDetector MyCVCoordinates, Double_t COEXY[2])
{

    CV->HitExists = 0;    
    int clusterid=0;
// 1) Loop over all gamma
    for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      //initiallize the input variables
      Double_t ClusterPos[3], GammaTrajectory[3];
      Double_t CVFrontPos[3] = {0};
      Double_t CVRearPos[3] = {0};
      //read the cluster pos and direction
      ClusterPos[0] = g.pos().x();
      ClusterPos[1] = g.pos().y();
      ClusterPos[2] = g.pos().z();
      GammaTrajectory[0] = g.p3().x();
      GammaTrajectory[1] = g.p3().y();
      GammaTrajectory[2] = g.p3().z();
      //find the intersection point (CV hit pos)
      Double_t Step = (CVFrontZ - EventStartZ)/GammaTrajectory[2];
      CVFrontPos[0]=GammaTrajectory[0]*Step + COEXY[0];
      CVFrontPos[1]=GammaTrajectory[1]*Step + COEXY[1];
      CVFrontPos[2]=CVFrontZ;
      Step = (CVRearZ - EventStartZ)/GammaTrajectory[2];
      CVRearPos[0]=GammaTrajectory[0]*Step + COEXY[0];
      CVRearPos[1]=GammaTrajectory[1]*Step + COEXY[1];
      CVRearPos[2]=CVRearZ;
      //define temporary variables for potential hit within R circle
      std::vector <Int_t> FrontModules;
      std::vector <Float_t> FrontEnergies;
      std::vector <Float_t> FrontTimes;
      std::vector <Int_t> RearModules;
      std::vector <Float_t> RearEnergies;
      std::vector <Float_t> RearTimes;
      //2 loops over all modules
      for(int n = 0; n < 96; n++)
      {
        Double_t X1[2] = {MyCVCoordinates.Pos[n][0], MyCVCoordinates.Pos[n][2]};
        Double_t X2[2] = {MyCVCoordinates.Pos[n][1], MyCVCoordinates.Pos[n][3]};
        Double_t R = 60;

        if(RadOverlap(R, CVFrontPos, X1, X2)){
          for(int j = 0; j < CV->ModuleNumber; j++){
            if(n == CV->ModuleID[j]){
              FrontModules.push_back(n);
              FrontEnergies.push_back(CV->ModuleEnergy[j]);
              FrontTimes.push_back(CV->ModuleTime[j]);
            }
          }
        } // end of if statement
      } // end loop over all front modules
      for(int n = 100; n < 187; n++)
      {
        Double_t X1[2] = {MyCVCoordinates.Pos[n][0], MyCVCoordinates.Pos[n][2]};
        Double_t X2[2] = {MyCVCoordinates.Pos[n][1], MyCVCoordinates.Pos[n][3]};
        Double_t R = 60;
        if(RadOverlap(R, CVRearPos, X1, X2)){
          for(int j = 0; j < CV->ModuleNumber; j++){
            if(n == CV->ModuleID[j]){
              RearModules.push_back(n);
              RearEnergies.push_back(CV->ModuleEnergy[j]);
              RearTimes.push_back(CV->ModuleTime[j]); 
            }
          }
        } // end of if statement
      } // end loop over all rear modules
      Double_t MaxEneFront = 0;
      Int_t MaxModFront;
      Double_t MaxTimeFront = 0;

      Double_t MaxEneRear = 0;
      Int_t MaxModRear;
      Double_t MaxTimeRear = 0;

      for(int n = 0; n < FrontEnergies.size(); n++){
         if(MaxEneFront < FrontEnergies[n]){
           MaxEneFront = FrontEnergies[n];
           MaxModFront = FrontModules[n];
           MaxTimeFront = FrontTimes[n];
         }
      }

      for(int n = 0; n < RearEnergies.size(); n++){
         if(MaxEneRear < RearEnergies[n]){
           MaxEneRear = RearEnergies[n];
           MaxModRear = RearModules[n];
           MaxTimeRear = RearTimes[n];
         }
      }

// Here find the estimated hit time based on cluster hit time
      Double_t FrontEstTime = 0;      
      Double_t RearEstTime = 0;      
      Double_t SpeedZ = 0;
      SpeedZ = 300*g.p3().z()/g.e();
      FrontEstTime = g.t() - (CSIZ-CVFrontZ)/SpeedZ;
      RearEstTime = g.t() - (CSIZ-CVRearZ)/SpeedZ;

// Here load hits info
      if(  MaxEneFront >= 0.1 && MaxEneRear >= 0.1 
	//&& MaxTimeFront >= 264.24 && MaxTimeFront <= 271.96
	//&& MaxTimeRear >= 265.14 && MaxTimeRear <= 272.86  ){
	){
        CV->HitExists ++;
        CV->HitsFrontEne.push_back(MaxEneFront);
        CV->HitsRearEne.push_back(MaxEneRear);
        CV->HitsFrontModuleID.push_back(MaxModFront);
        CV->HitsRearModuleID.push_back(MaxModRear);
        CV->HitsFrontTime.push_back(MaxTimeFront);
        CV->HitsRearTime.push_back(MaxTimeRear);
	CV->HitsFrontPos.push_back(std::array<Double_t ,3>{CVFrontPos[0],CVFrontPos[1],CVFrontPos[2]});
	CV->HitsRearPos.push_back(std::array<Double_t ,3>{CVRearPos[0],CVRearPos[1],CVRearPos[2]});
	CV->ChargeEntry.push_back(clusterid);
        CV->HitsFrontEstTime.push_back(FrontEstTime);
        CV->HitsRearEstTime.push_back(RearEstTime);
	CV->NumCharge ++;
      }
      else
      {
        CV->GammaEntry.push_back(clusterid);
	CV->NumGamma ++;
      }
      clusterid++;
    } //end of loop over all clusters

    return;
}

int CheckDoubleCount(CV_Class *CV)
{
        int NumDoubleCount = 0;
        for(int i=0; i < CV->HitExists ; i++)
        {
          for(int j=i+1; j < CV->HitExists ; j++)
          {
                if(CV->HitsFrontModuleID[i] == CV->HitsFrontModuleID[j] && CV->HitsRearModuleID[i] == CV->HitsRearModuleID[j] )
                        NumDoubleCount++;
          }
        }

	return NumDoubleCount;

}

Double_t RecPi0Mass(Double_t Gamma1E, Double_t Gamma2E, Double_t Gamma1Pos[3], Double_t Gamma2Pos[3], Double_t VertexZ, Double_t COEXY[2])
{
	const Double_t MPi0 = 134.98;
	Double_t MassPi0 = 0;
	Double_t DistBtwGammas = 0;
	Double_t DistGamma1Vtx = 0;
	Double_t DistGamma2Vtx = 0;
	Double_t CosAngle = 0;

	DistGamma1Vtx = sqrt( pow(Gamma1Pos[2]-VertexZ,2) 
			+ pow(Gamma1Pos[0] - COEXY[0],2) 
			+ pow(Gamma1Pos[1] - COEXY[1],2) );
	DistGamma2Vtx = sqrt( pow(Gamma2Pos[2]-VertexZ,2) 
			+ pow(Gamma2Pos[0] - COEXY[0],2) 
			+ pow(Gamma2Pos[1] - COEXY[1],2) );
	DistBtwGammas = sqrt( pow(Gamma1Pos[0]-Gamma2Pos[0],2) 
			+ pow(Gamma1Pos[1]-Gamma2Pos[1],2) );
	CosAngle = ( - pow(DistBtwGammas,2) + pow(DistGamma1Vtx,2) + pow(DistGamma2Vtx,2) )/ (2*DistGamma1Vtx*DistGamma2Vtx );

	MassPi0 = 2 * Gamma1E * Gamma2E * (1-CosAngle);
	Double_t Difference = MassPi0 - pow(MPi0,2);
	return Difference;
}

Double_t recVtxPi0(Double_t Zmax, Double_t Zmin, Double_t Gamma1E, Double_t Gamma2E, Double_t Gamma1Pos[3], Double_t Gamma2Pos[3], Double_t COEXY[2])
{
	Double_t Z[1000];
	int n = 1000;
	int NumSolution = 0;
	Double_t a, b, c, VtxZ = 0;
	Double_t ErrZ = 0.1;
        for(int i=0; i<n; i++)
        {
           Z[i] = Zmin + (Zmax-Zmin)*i/n;
        }
        for(int i=0; i<n-1; i++)
        {
	   if(RecPi0Mass(Gamma1E, Gamma2E, Gamma1Pos, Gamma2Pos,
	      Z[i], COEXY)
	     *RecPi0Mass(Gamma1E, Gamma2E, Gamma1Pos, Gamma2Pos,
	      Z[i+1], COEXY) < 0)
	   {
	     NumSolution++;
	     a = Z[i];
             b = Z[i+1];
	     while(RecPi0Mass(Gamma1E, Gamma2E, Gamma1Pos, Gamma2Pos, 
		   a, COEXY)*
		   RecPi0Mass(Gamma1E, Gamma2E, Gamma1Pos, Gamma2Pos, 
		   b, COEXY) < 0 && b-a >= ErrZ )
	     {
		   c = (a+b)/2;
		   if(RecPi0Mass(Gamma1E, Gamma2E, Gamma1Pos, Gamma2Pos, 
		      a, COEXY)*
		      RecPi0Mass(Gamma1E, Gamma2E, Gamma1Pos, Gamma2Pos, 
		      c, COEXY) < 0)
		      b = c;
		   else
		      a = c;
		   if(RecPi0Mass(Gamma1E, Gamma2E, Gamma1Pos, Gamma2Pos, 
		      c, COEXY) == 0)
		      break;
	     }
	     VtxZ = c;
	  }
	}
	if(NumSolution >= 1)
	   return VtxZ;
	else
	   return 0.0;
}


Double_t Distance( const array<Double_t, 3>& a, const array<Double_t, 3>& b)
{
	Double_t dx = a[0] - b[0];
	Double_t dy = a[1] - b[1];
	Double_t dz = a[2] - b[2];
	
	return sqrt( dx*dx + dy*dy + dz*dz );
}

Double_t GetEventStartTimeNew( std::vector<Klong> &klVec, Double_t* vertexTime, const double CSIZPosition, Double_t COEXY[2] )
{
  std::list<Gamma> glist = klVec[0].gamma();
  Double_t gammaHitTime[6]  = {0.0};
  Double_t gammaHitX[6]     = {0.0};
  Double_t gammaHitY[6]     = {0.0};
  Double_t gammaE[6]        = {0.0};
  Double_t gammaTWeight[6]  = {0.0};
  int id = 0;
  for( std::list<Gamma>::iterator g1=glist.begin();
       g1!=glist.end(); g1++ )
  {
   Gamma g = *g1;
   gammaHitTime[id]     = g.t();
   gammaHitX[id]        = g.pos().x();
   gammaHitY[id]        = g.pos().y();
   gammaE[id]           = g.e();
   gammaTWeight[id]     = pow( MTFUNC::TResolutionOfCluster( g.e() ), -2 );
   id++;
  }
  double vertexZ = klVec[0].v().z();
  for( int i=0; i<6; i++)
  {
   vertexTime[i] = 	gammaHitTime[i] 
			- sqrt( pow( CSIZPosition-vertexZ, 2) 
			+ pow( gammaHitX[i] - COEXY[0], 2) 
			+ pow( gammaHitY[i] - COEXY[1], 2) ) 
			/ (TMath::C()/1E6);
  }
  return MTFUNC::WeightedAverage( 6, vertexTime, gammaTWeight );
}

void FindCOEXY( Double_t COEXY[2], std::list<Gamma> glist)
{
	COEXY[0] = 0;
	COEXY[1] = 0;
	Double_t x[6], y[6], E[6];
	Double_t TotalE = 0;
	int id = 0;
        for( std::list<Gamma>::iterator g1=glist.begin();
             g1!=glist.end(); g1++ ){
                Gamma g = *g1;
                x[id] = g.pos().x();
                y[id] = g.pos().y();
                E[id] = g.e();
                id++;
        }
	for( int n=0; n<6; n++){
		COEXY[0] += x[n]*E[n];
		COEXY[1] += y[n]*E[n];
		TotalE += E[n];
	}
	COEXY[0] /= TotalE;
	COEXY[1] /= TotalE;
	return;
}

Double_t Rec3Mass( Double_t Pos[3][3], Double_t Ene[3], Double_t Vtx[3] ){
	Double_t Mass = 0;
	Double_t DistToVtx[3];
	Double_t DistBtw[3][3];
	Double_t CosAngle[3][3];
	for(int i=0; i<3; i++){
	  DistToVtx[i] = sqrt( 	  pow(Pos[i][0]-Vtx[0],2)
				+ pow(Pos[i][1]-Vtx[1],2)
				+ pow(Pos[i][2]-Vtx[2],2) );
	  for(int j=0; j<3; j++){
	    DistBtw[i][j] = sqrt( pow(Pos[i][0]-Pos[j][0],2)
				+ pow(Pos[i][1]-Pos[j][1],2) );
	    CosAngle[i][j] = (	  pow(DistToVtx[i],2)
				+ pow(DistToVtx[j],2)
				- pow(DistBtw[i][j],2) )/
				(2*DistToVtx[i]*DistToVtx[j]);	
	   Mass += Ene[i]*Ene[j]*(1-CosAngle[i][j]);
	  }
	}
	Mass = sqrt( Mass );	
	return Mass;
}

Double_t ThreeParticlesMass( Double_t ChargedE[4], Double_t ChargedPos[4][3], Double_t GammaE[2], Double_t GammaPos[2][3], Double_t Vtx[3] ){
	std::vector <Double_t> ThreeMass;
	Double_t Pos[3][3] = {0};
	Double_t Ene[3] = {0};
	for(int n=0; n<2; n++){
	  for(int i=0; i<4; i++){
	    for(int j=0; j<4; j++){
		Ene[0] = ChargedE[i];
		Ene[1] = ChargedE[j];
		Ene[2] = GammaE[n];
		for(int k=0;k<3;k++){
		    Pos[0][k] = ChargedPos[i][k];
		    Pos[1][k] = ChargedPos[j][k];
		    Pos[2][k] = GammaPos[n][k];	
		}
		ThreeMass.push_back( Rec3Mass( Pos,Ene,Vtx ) );    
	    }
	  }
	}
	Double_t Mass;
	Double_t Diff = 10000;
	const Double_t Pi0Mass = 135;
	for(int i=0; i<ThreeMass.size(); i++){
		if(std::abs(ThreeMass[i] - Pi0Mass) < Diff){
			Diff = std::abs(ThreeMass[i] - Pi0Mass);
			Mass = ThreeMass[i];
		}
	}

	return Mass;
}

Double_t CalculateCVResidualEne( Double_t CVFrontHitsEne[4], Double_t CVRearHitsEne[4], CV_Class *CV, Double_t &CVTotalModuleEne, Double_t &CVTotalHitsEne )
{
	Double_t CVResidualEne = 0;
	CVTotalModuleEne = 0;
	CVTotalHitsEne = 0;
	for(int i=0; i<CV->ModuleNumber; i++){
		if(CV->ModuleTime[i] >= 260 &&
		   CV->ModuleTime[i] <= 280 ){
		   //&& CV->ModuleEnergy[i] >= 0.01){
			CVTotalModuleEne += CV->ModuleEnergy[i];
		}
	}
        std::set<Double_t> uniqueHitsEnergy;
        for (int i = 0; i < 4; i++) {
                uniqueHitsEnergy.insert(CVFrontHitsEne[i]);
                uniqueHitsEnergy.insert(CVRearHitsEne[i]);
        }
        for (const auto& element : uniqueHitsEnergy) {
                CVTotalHitsEne += element;
        }
	CVResidualEne = CVTotalModuleEne - CVTotalHitsEne;

	return CVResidualEne;
}

void FindChargedPt(std::list<Gamma> glist, CV_Class *CV, Double_t &PtFourCharged, Double_t *PtTwoCharged){

	PtFourCharged = 0;
	Double_t MomFourCharged[3] = {0};
	Double_t MomTwoCharged[6][3] = {0};
	CLHEP::Hep3Vector e_Mom[4];
	int e_index = 0;
	for(int i=0;i<6;i++){
      		std::list<Gamma>::iterator git = glist.begin();
      		std::advance(git, i );
      		Gamma tmpG = *git;
      		if( i == CV->ChargeEntry[e_index]){
		  e_Mom[e_index] = tmpG.p3();
		  e_index++;
		}
	}

	for(int i=0; i<4; i++){
		MomFourCharged[0] += e_Mom[i].x();
		MomFourCharged[1] += e_Mom[i].y();
	}
	PtFourCharged = sqrt( pow(MomFourCharged[0],2) + 
			      pow(MomFourCharged[1],2) );
	int tmpindex = 0;
   	for(int i=0; i<3; i++){
           for(int j=i+1; j<4; j++){
		MomTwoCharged[tmpindex][0] = e_Mom[i].x()+e_Mom[j].x();
		MomTwoCharged[tmpindex][1] = e_Mom[i].y()+e_Mom[j].y();
		tmpindex ++;	
	   }
	}
	for(int i=0;i<6;i++){
		PtTwoCharged[i] = sqrt( pow(MomTwoCharged[i][0],2) +
					pow(MomTwoCharged[i][1],2) );
	}

return;
}


bool CVModCheck( std::list<Gamma> glist, CV_Class *CV, Double_t EventStartZ, CVDetector MyCVCoordinates, Double_t COEXY[2])
{
    const Double_t R = 0.0001;
    int clusterid=0;

    int FrontHitsModID[6];
    int RearHitsModID[6];

// 1) Loop over all gamma
    for( std::list<Gamma>::iterator g1=glist.begin();
         g1!=glist.end(); g1++ ){
      Gamma g = *g1;
      //initiallize the input variables
      Double_t ClusterPos[3], GammaTrajectory[3];
      Double_t CVFrontPos[3] = {0};
      Double_t CVRearPos[3] = {0};
      //read the cluster pos and direction
      ClusterPos[0] = g.pos().x();
      ClusterPos[1] = g.pos().y();
      ClusterPos[2] = g.pos().z();
      GammaTrajectory[0] = g.p3().x();
      GammaTrajectory[1] = g.p3().y();
      GammaTrajectory[2] = g.p3().z();
      //find the intersection point (CV hit pos)
      Double_t Step = (CVFrontZ - EventStartZ)/GammaTrajectory[2];
      CVFrontPos[0]=GammaTrajectory[0]*Step + COEXY[0];
      CVFrontPos[1]=GammaTrajectory[1]*Step + COEXY[1];
      CVFrontPos[2]=CVFrontZ;
      Step = (CVRearZ - EventStartZ)/GammaTrajectory[2];
      CVRearPos[0]=GammaTrajectory[0]*Step + COEXY[0];
      CVRearPos[1]=GammaTrajectory[1]*Step + COEXY[1];
      CVRearPos[2]=CVRearZ;

      //2 loops
      //loop over front modules
      for(int n = 0; n < 96; n++)
      {
        Double_t X1[2] = {MyCVCoordinates.Pos[n][0], MyCVCoordinates.Pos[n][2]};
        Double_t X2[2] = {MyCVCoordinates.Pos[n][1], MyCVCoordinates.Pos[n][3]};

        if(RadOverlap(R, CVFrontPos, X1, X2)){
	   FrontHitsModID[clusterid] = n;
        } // end of if statement
      } // end front modules loop
      //loop over rear modules
      for(int n = 100; n < 187; n++)
      {
        Double_t X1[2] = {MyCVCoordinates.Pos[n][0], MyCVCoordinates.Pos[n][2]};
        Double_t X2[2] = {MyCVCoordinates.Pos[n][1], MyCVCoordinates.Pos[n][3]};
        if(RadOverlap(R, CVRearPos, X1, X2)){
	   RearHitsModID[clusterid] = n;
        } // end of if statement
      } // end rear modules loop

      clusterid++;
    } //end of loop over all clusters
    int checkbit = 0;
    for(int i=0; i<5; i++){
	for(int j=i+1; j<6; j++){
	    if( FrontHitsModID[i] == FrontHitsModID[j] || 
	        RearHitsModID[i] == RearHitsModID[j] ){
		checkbit = 1;
	    }
	}
    }

    if(checkbit == 1){
       return false;
    }
    else{
       return true;
    }
}

void FindFusionParameters(std::list<Gamma> glist, Double_t &GamClusRMS_Max, Double_t &GammaChi2_Max){

	Double_t GammaChi2[6];
	Double_t GamClusRMS[6];

	int g_index = 0;
	for(int i=0;i<6;i++){
      		std::list<Gamma>::iterator git = glist.begin();
      		std::advance(git, i );
      		Gamma tmpG = *git;
		GammaChi2[g_index] = tmpG.chisq();
		GamClusRMS[g_index] = tmpG.rms();
		g_index++;
	}

	GammaChi2_Max = GammaChi2[0];
	GamClusRMS_Max = GamClusRMS[0];
	for(int i=1; i<6; i++){
		if(GammaChi2[i]>GammaChi2_Max)
			GammaChi2_Max = GammaChi2[i];
		if(GamClusRMS[i]>GamClusRMS_Max)
			GamClusRMS_Max = GamClusRMS[i];
	}

return;
}



