#include "TTree.h"
#include <vector>

using namespace std;

class CV_Class
{       public:

//input variables from clusteringTree
        int ModuleNumber;
        vector <float> 	ModuleEnergy;
        vector <int> 	ModuleID;
        vector <float> 	ModuleTime;
//output variables for cv hits found
	int HitExists;
        vector <Float_t>        	HitsFrontEne;
        vector <Int_t>          	HitsFrontModuleID;
        vector <Float_t>        	HitsFrontTime;
        vector <Float_t>        	HitsRearEne;
        vector <Int_t>          	HitsRearModuleID;
        vector <Float_t>        	HitsRearTime;
	vector <array<Double_t, 3>>	HitsFrontPos;
	vector <array<Double_t, 3>>	HitsRearPos;
        vector <Double_t>        	HitsFrontEstTime;
        vector <Double_t>        	HitsRearEstTime;

	Double_t			MinFrontDist;
	Double_t			MinRearDist;			
	Double_t			MaxFrontDist;
	Double_t			MaxRearDist;			


	int			NumCharge = 0;
	vector <Int_t>		ChargeEntry;
	int 			NumGamma = 0;
	vector <Int_t>		GammaEntry;

	CV_Class(int TempCVModuleNumber, TTree* itree, int EntryNum);
	void CVMinDist();
	void CVMaxDist();
};

void RemoveTreeAddress(TTree* itree);

bool RadOverlap(Double_t R, Double_t* C, Double_t* X1, Double_t* X2);
