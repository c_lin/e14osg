#include <iostream>
#include <list>
#include "klong/RecKlong.h"
#include "gnana/E14GNAnaFunction.h"
#include "ShapeChi2/ShapeChi2New.h"

bool user_rec(std::list<Gamma> const &glist, std::vector<Klong> &klVec, int userFlag, double &chisq_2pi0gg, double &chisq_3pi0)
{
  ///// reconstruction
  static RecKlong recKl;      
  std::vector<Klong> klVec2;
  //  klVec = recKl.recK3pi0(glist,VERTEX_FIX_XYZERO) ;
  //  set userflag = 0
  klVec = recKl.recK2pi0gg(glist, VERTEX_FIX_XYZERO);
  klVec2 = recKl.recK3pi0_wcfit(glist, 0, VERTEX_FIX_XYZERO);   //simultaneously do KL3pi0 reconstruction
  if(klVec.size()==0){return false;}
  if(klVec2.size()==0){return false;}
  ///// gamma position & energy correction for angle dependency
  E14GNAnaFunction::getFunction()->correctPosition(klVec[0]);  
  E14GNAnaFunction::getFunction()->correctEnergyWithAngle(klVec[0]);  
  E14GNAnaFunction::getFunction()->correctPosition(klVec2[0]);
  E14GNAnaFunction::getFunction()->correctEnergyWithAngle(klVec2[0]);
  int c = 0;
  ///// re-reconstruction with corrected gamma
  std::list<Gamma> glist2;
  std::list<Gamma> glist3;
  for( std::vector<Pi0>::iterator it=klVec[0].pi0().begin();
       it!=klVec[0].pi0().end(); it++){
    glist2.push_back(it->g1());
    glist2.push_back(it->g2());
 //   std::cout << *it << std::endl;
    c = c+2;
  }
  for( std::vector<Pi0>::iterator it=klVec2[0].pi0().begin(); it!=klVec2[0].pi0().end(); it++){
       glist3.push_back(it->g1());
       glist3.push_back(it->g2());
  //     std::cout << *it << std::endl;
       }
  klVec = recKl.recK2pi0gg(glist2);
  klVec2 = recKl.recK3pi0_wcfit(glist3);
  if(klVec.size()==0){return false;}
  if(klVec2.size()==0){return false;} 
  for( std::vector<Pi0>::iterator it=klVec[0].pi0().begin(); it!=klVec[0].pi0().end(); it++){
       //std::cout << it->g1().clusterIdVec().size() << std::endl << it->g2().clusterIdVec().size() << std::endl;
       //std::cout << it->g1() << std::endl << it->g2() << std::endl;
       if(std::isnan(it->g1().p3().x()) || std::isnan(it->g1().e()) || it->g1().clusterIdVec().size() == 0 || std::isnan(it->g2().p3().x()) || std::isnan(it->g2().e()) || it->g2().clusterIdVec().size() == 0){
          return false;
       }
  }
  ///// shape chi2 evaluation
  E14GNAnaFunction::getFunction()->shapeChi2(klVec[0]);

  static ShapeChi2New chi2Newcalc;
  chisq_2pi0gg = klVec[0].chisqZ(); /// klVec.chisqZ() actually set to chisq_2pi0gg is reconstruction 
//  chisq_2pi0gg = E14GNAnaFunction::getFunction()->cfit_klpi0pi0X(klVec[0]);
  chisq_3pi0   = klVec2[0].chisqZ();

//  std::cout << "good" << std::endl;
  return true;
}
