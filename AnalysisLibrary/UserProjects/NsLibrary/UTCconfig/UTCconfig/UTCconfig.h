#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

namespace NsUTC{

		
	typedef enum {RUN81=-1, RUN82=-2, RUN85=-3} RunFlag;
	
	class UTCconfig{

	
	public:
		int GetSumID(int csimodID);
		int GetSubID(int csimodID);
		int GetSumXY(int sumID, double&x, double& y);
	
		static UTCconfig* GetInstance(int runID=0);
	
		int CheckHVcondition(int SumID);


	private:
	
		UTCconfig(int runID);
		~UTCconfig();
		
		int m_runID;
		static const int Ncry = 2716;
		static const int Nhybrid = 1024;
		static const int Nsum = 256;
		static int sumIDaslocID[Ncry];
		static UTCconfig* theInstance;
		static double xsum[Nsum];
		static double ysum[Nsum];
		static double xhybrid[Nhybrid];
		static double yhybrid[Nhybrid];
		static int    nhybrid[Nsum];
		
		int isBd0(int SumID);
		
	};

}
