#include "UTCconfig/UTCconfig.h"
#include "math.h"
#include <iostream>
#include <cstdlib>

namespace NsUTC{

	UTCconfig* UTCconfig::theInstance = NULL;
	int UTCconfig::sumIDaslocID[Ncry];
	
	double UTCconfig::xhybrid[Nhybrid];
	double UTCconfig::yhybrid[Nhybrid];
	int UTCconfig::nhybrid[Nsum];
	double UTCconfig::xsum[Nsum];
	double UTCconfig::ysum[Nsum];
	
	UTCconfig* UTCconfig::GetInstance(int runID){
		if(theInstance==NULL){
			theInstance = new UTCconfig(runID);
		}
		return theInstance;
	}
	
	UTCconfig::UTCconfig(int runID){		

		std::string str_externaldata = std::getenv("E14ANA_EXTERNAL_DATA");
		std::string sumidaslocid_file;
	
		m_runID = runID;

	  	if(runID==0){	
			sumidaslocid_file = str_externaldata + "/UTCsumData/config/sumIDaslocID.txt";
		}else if((29997<=runID && runID<=36000) || (runID==RUN81 || runID==RUN82 || runID==RUN85)){
			sumidaslocid_file = str_externaldata + "/UTCsumData/config/sumIDaslocID_run79.txt";
		}else{
			std::cout << "[UTCconfig]: Warning! invalid runID is specified. Default run79 settings is used\n";
			sumidaslocid_file = str_externaldata + "/UTCsumData/config/sumIDaslocID_run79.txt";
		}
		std::ifstream fin(sumidaslocid_file.c_str());
		for(int i=0;i<Ncry;i++){
			int modID, sumID;
			fin >> modID >> sumID;
			sumIDaslocID[modID] = sumID;
		}
		fin.close();
	
				
		for(int i=0;i<Nsum;i++){
		   	nhybrid[i] = 0;
		   	xsum[i] = 0;
		   	ysum[i] = 0;
		}
		
		
		std::string sumlocation_file;
		if(runID==0){	
			sumlocation_file = str_externaldata + "/UTCsumData/config/sumlocation.txt";
		}else if(29997<=runID){
			sumlocation_file = str_externaldata + "/UTCsumData/config/sumlocation_run79.txt";
		}
		
		fin.open(sumlocation_file.c_str());
		for(int i=0;i<Nhybrid;i++){
			int sumID;
			double x, y;
			fin >> sumID >> x >> y;
			if(fabs(x)>1e-4 && fabs(y)>1e-4){
				xhybrid[i] = x;
				yhybrid[i] = y;
			}else{
			xhybrid[i] = 0;
				yhybrid[i] = 0;
				continue;
			}
			sumID /= 4;
			
			xsum[sumID]+= x;
			ysum[sumID]+= y;
			nhybrid[sumID]++;
		}
		fin.close();
		for(int i=0;i<Nsum;i++){
			xsum[i] /= nhybrid[i];
			ysum[i] /= nhybrid[i];
		}
	
	}
	
		   	
	int UTCconfig::isBd0(int SumID){
		if(39<=SumID && SumID<=41) return 1;
		else if(51<=SumID && SumID<=53) return 1;
		else if(63<=SumID && SumID<=64) return 1;
		else return 0;
	}
	int UTCconfig::CheckHVcondition(int SumID){

		if(m_runID<0) return 0;

		int BD0down = 0;
		if(30770<=m_runID && m_runID<=30773) return -1;
		else if(31044<=m_runID && m_runID<=31049) BD0down = 1;
		else if(31200<=m_runID && m_runID<=31209) BD0down = 1;
		else if(31695<=m_runID && m_runID<=31696) BD0down = 1;
		else if(32045<=m_runID && m_runID<=32054) BD0down = 1;
		else if(32067==m_runID) BD0down = 1;

		if(BD0down){
			if(isBd0(SumID)) return -1;
			else return 0;
		}else{
			return 0;
		}
	}
	
	UTCconfig::~UTCconfig(){}
	
	int UTCconfig::GetSumID(int csimodID){
		if(csimodID<0 || csimodID>=Ncry) return -1;
		int sumid = sumIDaslocID[csimodID];
		if(sumid<0) return -1;
		return sumid/4;
	}
	int UTCconfig::GetSubID(int csimodID){
		if(csimodID<0 || csimodID>=Ncry) return -1;
		int sumid = sumIDaslocID[csimodID];
		if(sumid<0) return -1;
		return sumid%4;
	}
	
	int UTCconfig::GetSumXY(int sumID, double&x, double& y){
		if(sumID<0 || sumID>=Nsum) return -1;
		x = xsum[sumID];
		y = ysum[sumID];
		
		return 0;	
	}
	

}

