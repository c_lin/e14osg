#include "IndividualityCompensator/IndividualityCompensator.h"
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TRandom3.h>
#include <sstream>
#include <UTCconfig/UTCconfig.h>

namespace NsUTC{

	
	
	IndividualityCompensator::IndividualityCompensator(){
	
		utcconfig = UTCconfig::GetInstance(30000);
		
		std::string str_externaldata = std::getenv("E14ANA_EXTERNAL_DATA");
		std::string str_dir_individuality = str_externaldata + "/UTCsumData/IndividualityCompensator/";

		std::string config = str_dir_individuality + "/t0_hybrid_list.txt";
		std::ifstream fin(config.c_str());
		
		while(1){
			int Runid_ini, Runid_end, fileID = -1;
			fin >> Runid_ini >> Runid_end >> fileID;
			if(fileID>=0){
				v_runID_ini.push_back(Runid_ini);
				v_runID_end.push_back(Runid_end);
				v_calibfileID.push_back(fileID);
			}
			if(fin.eof())break;
		}

		fin.close();
		if(v_runID_ini.size()==0){
			std::cout << "Error! t0 hybrid file was not found!\n";
		}
		Nconfigfile = v_runID_ini.size();
		hybrid_calibt0 = new double*[v_runID_ini.size()];
		for(int i=0;i<v_runID_ini.size();i++){
			int id = v_calibfileID[i];
			hybrid_calibt0[i] = new double[Nhybrid];
			
			std::string t0file = str_dir_individuality + Form("/t0/t0_hybrid_%d.txt", id);
			fin.open(t0file.c_str());
	
			int nch = -1;
			fin >> nch;
			if(nch!=Nhybrid){
				std::cout << "Strange T0 calib file : " + t0file + " Confirm it!\n";
				continue;
			}

			for(int j=0;j<Nhybrid;j++){
				int ich;
				double t0, t0err;	
				fin >> ich >> t0 >> t0err;
				hybrid_calibt0[i][j] = t0*8;
			}
			fin.close();
		}

		ind_config = -1;
	}

		
	int IndividualityCompensator::setRunID(int runID){
		ind_config = -1;
		for(int i=0;i<Nconfigfile;i++){	
			if(v_runID_ini[i]<=runID && runID<=v_runID_end[i]){
				ind_config = v_calibfileID[i];
				break;
			}
		}
		if(ind_config<0){
			std::cout << "Warning! hybrid t0 file was not found\n";
		}
		return ind_config;
	}

	IndividualityCompensator::~IndividualityCompensator(){
	
		for(int i=0;i<Nconfigfile;i++){
			delete [] hybrid_calibt0[i];
		}	

	}
	
	
	double IndividualityCompensator::getOffset(int NmodID, const int* pModIDs, const double* pEnergies){

		if(ind_config<0) return -9999;

		double T = 0;
		double E = 0;
		double Tp = 0;
		int    N = 0;
		for(int i=0;i<NmodID;i++){
			int sumid = utcconfig->GetSumID(pModIDs[i]);
			int subid = utcconfig->GetSubID(pModIDs[i]);

			if(sumid<0) continue;

			int indsum = sumid*4+subid;
			double t0 = hybrid_calibt0[ind_config][indsum];
			Tp += t0;
			T  += t0*pEnergies[i];
			E  += pEnergies[i];
			N++;
		}

		if(N==0) return 0;
		
		T /= E;
		Tp /= N;

		return T-Tp;
	}
	
	
}
