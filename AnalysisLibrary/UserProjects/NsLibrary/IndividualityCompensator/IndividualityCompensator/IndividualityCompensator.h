#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <TF1.h>
#include <vector>
#include <UTCconfig/UTCconfig.h>

namespace NsUTC{ 

	class IndividualityCompensator{
	
	public:
		IndividualityCompensator();
		~IndividualityCompensator();
	
	
		double getOffset(int NmodID, const int* pModIDs, const double* pEnergies);
		int setRunID(int runID);

	private:
	

		UTCconfig* utcconfig;
		double** hybrid_calibt0;

		std::vector<int> v_runID_ini;
		std::vector<int> v_runID_end;
		std::vector<int> v_calibfileID;

		int Nconfigfile;	
		int ind_config;
		static const int Nhybrid = 1024;
	
			
	
	};

}
