#ifndef LCLIB_LCWFM_LCWFMDATAHANDLER_H
#define LCLIB_LCWFM_LCWFMDATAHANDLER_H

#include "TObject.h"
#include "TTree.h"
#include "TClonesArray.h"

#include "LcWfm/LcWfm.h"
#include "LcWfmData/LcWfmData.h"

namespace LcLib
{

   class LcWfmDataHandler : public TObject
   {
    public : 
      /// constructor
      LcWfmDataHandler( const std::string detname );

      /// destructor
      virtual ~LcWfmDataHandler();

      /// method
      void Clear(Option_t* = "" );

      bool SetBranchAddresses(TTree *tr);
      bool AddBranches(TTree* tr);

      void AddWfmData();
      bool LoadWfmData( const Int_t idx );

      /// setter
      void SetModId( const Int_t modId );
      void SetEnergy( const Double_t energy );
      void SetTwfm( const LcWfm &twfm );
      void SetFwfm( const LcWfm &fwfm );
      
      /// getter
      std::string GetDetectorName() const { return m_detname; }
      Int_t       GetSize() const;
      Int_t       GetModId() const;
      Double_t    GetEnergy() const;
      LcWfm       GetTwfm() const;
      LcWfm       GetFwfm() const;

    private :
      const std::string      m_detname;
      TClonesArray          *m_arrWfmData;
      LcWfmData             *m_wfmData;
      
      Int_t                 *m_modId;
      Double_t              *m_energy;
      std::vector<Double_t> *m_twfmvec;
      std::vector<Double_t> *m_fwfmvec;

      void ResetWfmDataPointer();
      void SetWfmDataPointer();

      ClassDef(LcWfmDataHandler,1);
   };

}

#endif
