#ifndef LCLIB_LCWFM_LCWFMDATA_H
#define LCLIB_LCWFM_LCWFMDATA_H

#include <vector>

#include "TObject.h"

namespace LcLib
{

   class LcWfmData : public TObject
   {
    public : 
      /// constructor
      LcWfmData();

      /// destructor
      virtual ~LcWfmData();

      ///
      virtual void Clear( Option_t* = "");

      ///
      Int_t                 ModId;
      Double_t              Energy;
      std::vector<Double_t> Twfm; 
      std::vector<Double_t> Fwfm;

      ClassDef(LcWfmData,1);
   };

}

#endif /* LcWfmData.h guard */
