#include "LcWfmData/LcWfmData.h"

namespace LcLib
{

   LcWfmData::LcWfmData()
   {
      Clear();
   }

   LcWfmData::~LcWfmData()
   {
      ;
   }

   void LcWfmData::Clear(Option_t* )
   {
      ModId = -1;
      std::vector<Double_t>().swap(Twfm);
      std::vector<Double_t>().swap(Fwfm);
   }

}
