#include "LcWfmData/LcWfmDataHandler.h"

namespace LcLib
{

   /// constructor
   LcWfmDataHandler::LcWfmDataHandler( const std::string detname )
      : m_detname(detname)
   {
      m_arrWfmData = new TClonesArray("LcLib::LcWfmData");
   }

   /// destructor
   LcWfmDataHandler::~LcWfmDataHandler()
   {
      delete m_arrWfmData;
   }

   /// 
   void LcWfmDataHandler::Clear(Option_t* )
   {
      m_arrWfmData->Clear("C");
      ResetWfmDataPointer();
   }
 
   bool LcWfmDataHandler::SetBranchAddresses(TTree *tr)
   {
      if( tr==NULL ){
         Error("SetBranchAddresses","NULL pointer is given.");
         return false;
      }

      tr->SetBranchAddress( m_detname.c_str(), &m_arrWfmData );

      return true;
   }

   bool LcWfmDataHandler::AddBranches(TTree* tr)
   {
      if( tr==NULL ){
         Error("AddBranches","NULL pointer is given.");
         return false;
      }

      tr->Branch( m_detname.c_str(), &m_arrWfmData );
      
      return true;
   }

   ///
   void LcWfmDataHandler::AddWfmData()
   {
      const Int_t nentry = m_arrWfmData->GetEntriesFast();
      m_wfmData = new ( (*m_arrWfmData)[nentry] ) LcWfmData();
      SetWfmDataPointer();
   }

   bool LcWfmDataHandler::LoadWfmData( const Int_t idx )
   {
      if( idx<0 || idx>m_arrWfmData->GetEntriesFast() ){
         Error("LoadWfmData", " invalid index (0-%d)",m_arrWfmData->GetEntriesFast()-1);
         return false;
      }

      m_wfmData = dynamic_cast<LcWfmData*>(m_arrWfmData->At(idx));
      SetWfmDataPointer();
      return true;

   }

   /// setter
   void LcWfmDataHandler::SetModId( const Int_t modId )
   {
      if( m_modId ) *m_modId = modId;
   }

   void LcWfmDataHandler::SetEnergy( const Double_t energy )
   {
      if( m_energy ) *m_energy = energy;
   }

   void LcWfmDataHandler::SetTwfm( const LcWfm &twfm )
   {
      if( m_twfmvec ) *m_twfmvec = twfm.GetWfmVec();
   }

   void LcWfmDataHandler::SetFwfm( const LcWfm &fwfm )
   {
      if( m_fwfmvec ) *m_fwfmvec = fwfm.GetWfmVec();
   }

   /// getter
   Int_t LcWfmDataHandler::GetSize() const
   {
      return ( m_arrWfmData ) ? m_arrWfmData->GetEntriesFast() : 0;
   }

   Int_t LcWfmDataHandler::GetModId() const
   {
      return ( m_modId ) ? *m_modId : -1;
   }

   Double_t LcWfmDataHandler::GetEnergy() const
   {
      return ( m_energy ) ? *m_energy : 0.;
   }

   LcWfm LcWfmDataHandler::GetTwfm() const
   {
      if( m_twfmvec ){
         const LcWfm twfm( *m_twfmvec );
         return twfm;
      }else{
         return LcWfm();
      }
   }

   LcWfm LcWfmDataHandler::GetFwfm() const
   {
      if( m_fwfmvec ){
         const LcWfm fwfm( *m_fwfmvec );
         return fwfm;
      }else{
         return LcWfm();
      }
   }

   /// private ///
   void LcWfmDataHandler::ResetWfmDataPointer()
   {
      m_modId = NULL;
      m_energy = NULL;
      m_twfmvec = NULL;
      m_fwfmvec = NULL;
   }

   void LcWfmDataHandler::SetWfmDataPointer()
   {
      m_modId = &(m_wfmData->ModId);
      m_energy = &(m_wfmData->Energy);
      m_twfmvec = &(m_wfmData->Twfm);
      m_fwfmvec = &(m_wfmData->Fwfm);
   }

}
