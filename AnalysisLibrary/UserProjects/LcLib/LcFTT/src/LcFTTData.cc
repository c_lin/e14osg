#include "LcFTT/LcFTTData.h"

namespace LcLib
{

   LcFTTData::LcFTTData()
   {
      Clear();
   }

   LcFTTData::~LcFTTData()
   {
      ;
   }

   void LcFTTData::Clear(Option_t* )
   {
      ModId = -1;
      FTTMean.clear();
      FTTSigma.clear();
   }

}
