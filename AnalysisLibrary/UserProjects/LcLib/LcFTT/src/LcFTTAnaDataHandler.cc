#include "LcFTT/LcFTTAnaDataHandler.h"

#include "LcDetPar/LcDetPar.h"

namespace LcLib
{
   /// constructor
   LcFTTAnaDataHandler::LcFTTAnaDataHandler( const std::string detname )
      : m_detname(detname)
   {
      const LcDetPar detpar(detname);
      m_nchannel = detpar.GetNchannel();
      m_number = 0;
      m_modId = new Int_t   [ m_nchannel ];
      m_chisq = new Float_t [ m_nchannel ];
      m_time  = new Float_t [ m_nchannel ];
      
      Init();
   }

   /// destuctor
   LcFTTAnaDataHandler::~LcFTTAnaDataHandler()
   {
      delete [] m_modId;
      delete [] m_chisq;
      delete [] m_time;
   }

   ///
   void LcFTTAnaDataHandler::Init()
   {
      m_number = 0;
      for( Int_t idx=0; idx<m_nchannel; ++idx )
      {
         m_modId[idx] = -1;
         m_chisq[idx] = -5.;
         m_time[idx] = -1.;
      }
   }

   ///
   bool LcFTTAnaDataHandler::SetBranchAddresses( TTree *tr )
   {
      if(tr==NULL){
         Error("SetBranchAddresses","NULL pointer is given.");
         return false;
      }

      tr->SetBranchAddress(Form("%sFTTNumber", m_detname.c_str()), &m_number );
      tr->SetBranchAddress(Form("%sFTTModID" , m_detname.c_str()),  m_modId  );
      tr->SetBranchAddress(Form("%sFTTChisq" , m_detname.c_str()),  m_chisq  );
      tr->SetBranchAddress(Form("%sFTTTime"  , m_detname.c_str()),  m_time   );
      return true;
   }
      
   bool LcFTTAnaDataHandler::AddBranches( TTree *tr )
   {
      if(tr==NULL){
         Error("AddBranches","NULL pointer is given");
         return false;
      }

      tr->Branch( Form("%sFTTNumber",m_detname.c_str()), &m_number, 
                  Form("%sFTTNumber/I",m_detname.c_str()) );

      tr->Branch( Form("%sFTTModID" ,m_detname.c_str()),  m_modId,
                  Form("%sFTTModID[%sFTTNumber]/I",m_detname.c_str(),m_detname.c_str() ) );

      tr->Branch( Form("%sFTTChisq" ,m_detname.c_str()),  m_chisq,
                  Form("%sFTTChisq[%sFTTNumber]/F",m_detname.c_str(),m_detname.c_str() ) );

      tr->Branch( Form("%sFTTTime" ,m_detname.c_str()),  m_time,
                  Form("%sFTTTime[%sFTTNumber]/F",m_detname.c_str(),m_detname.c_str() ) );

      return true;
   }
 

}
