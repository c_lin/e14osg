#include "LcFTT/LcFTTDataHandler.h"

namespace LcLib
{

   /// constructor
   LcFTTDataHandler::LcFTTDataHandler( const std::string detname )
      : m_detname(detname)
   {
      m_arrFTTData = new TClonesArray("LcLib::LcFTTData");
   }

   /// destructor
   LcFTTDataHandler::~LcFTTDataHandler()
   {
      delete m_arrFTTData;
   }

   /// 
   void LcFTTDataHandler::Clear(Option_t* )
   {
      m_arrFTTData->Clear("C");
      ResetFTTDataPointer();
   }
 
   bool LcFTTDataHandler::SetBranchAddresses(TTree *tr)
   {
      if( tr==NULL ){
         Error("SetBranchAddresses","NULL pointer is given.");
         return false;
      }

      tr->SetBranchAddress( m_detname.c_str(), &m_arrFTTData );

      return true;
   }

   bool LcFTTDataHandler::AddBranches(TTree* tr)
   {
      if( tr==NULL ){
         Error("AddBranches","NULL pointer is given.");
         return false;
      }

      tr->Branch( m_detname.c_str(), &m_arrFTTData );
      
      return true;
   }

   ///
   void LcFTTDataHandler::AddFTTData()
   {
      const Int_t nentry = m_arrFTTData->GetEntriesFast();
      m_fttData = new ( (*m_arrFTTData)[nentry] ) LcFTTData();
      SetFTTDataPointer();
   }

   bool LcFTTDataHandler::LoadFTTData( const Int_t idx )
   {
      if( idx<0 || idx>m_arrFTTData->GetEntriesFast() ){
         Error("LoadWfmData", " invalid index (0-%d)",m_arrFTTData->GetEntriesFast()-1);
         return false;
      }

      m_fttData = dynamic_cast<LcFTTData*>(m_arrFTTData->At(idx));
      SetFTTDataPointer();
      return true;

   }

   void LcFTTDataHandler::LoadIndexMap()
   {
      std::map<Int_t, Int_t> indexMap;
      for( Int_t idx=0; idx<GetSize(); ++idx )
      {
         LoadFTTData(idx);
         indexMap.insert( std::make_pair( GetModId(), idx ) );
      }
      m_indexMap.swap( indexMap );
   }

   /// setter
   void LcFTTDataHandler::SetModId( const Int_t modId )
   {
      if( m_modId ) *m_modId = modId;
   }

   void LcFTTDataHandler::SetFTTMean( const LcWfm &fttmean )
   {
      if( m_fttmean_vec ) *m_fttmean_vec = fttmean.GetWfmVec();
   }

   void LcFTTDataHandler::SetFTTSigma( const LcWfm &fttsigma )
   {
      if( m_fttsigma_vec ) *m_fttsigma_vec = fttsigma.GetWfmVec();
   }

   /// getter
   Int_t LcFTTDataHandler::GetSize() const
   {
      return ( m_arrFTTData ) ? m_arrFTTData->GetEntriesFast() : 0;
   }

   Int_t LcFTTDataHandler::GetIndex( const Int_t modId ) const
   {
      std::map<Int_t, Int_t>::const_iterator it = m_indexMap.find(modId);
      return ( it!=m_indexMap.end() ) ? it->second : -1;
   }

   Int_t LcFTTDataHandler::GetModId() const
   {
      return ( m_modId ) ? *m_modId : -1;
   }

   LcWfm LcFTTDataHandler::GetFTTMean() const
   {
      if( m_fttmean_vec ){
         const LcWfm fttMean( *m_fttmean_vec );
         return fttMean;
      }else{
         return LcWfm();
      }
   }

   LcWfm LcFTTDataHandler::GetFTTSigma() const
   {
      if( m_fttsigma_vec ){
         const LcWfm fttSigma( *m_fttsigma_vec );
         return fttSigma;
      }else{
         return LcWfm();
      }
   }

   /// private ///
   void LcFTTDataHandler::ResetFTTDataPointer()
   {
      m_modId = NULL;
      m_fttmean_vec = NULL;
      m_fttsigma_vec = NULL;
   }

   void LcFTTDataHandler::SetFTTDataPointer()
   {
      m_modId = &(m_fttData->ModId);
      m_fttmean_vec = &(m_fttData->FTTMean);
      m_fttsigma_vec = &(m_fttData->FTTSigma);
   }

}
