#include "LcFTT/LcFTT.h"

#include "TMath.h"

#include "LcWfm/LcWfmParameter.h"

namespace LcLib
{

   /// constructor
   LcFTT::LcFTT( const std::string detname )
      : m_detname(detname),
        m_detpar(detname)
   {
      m_file = NULL;
      m_tree = NULL;
      m_ftt_handler = NULL;
      m_nentry = 0;
      m_properMaxSample = -1;
      SetDefaultPeakThreshold();
   }

   /// destructor
   LcFTT::~LcFTT()
   {
      delete m_file; 
      delete m_tree;
      delete m_ftt_handler;
   }

   /// 
   bool LcFTT::SetFTTFile( const std::string fttFileName )
   {
      if( m_file ){
         Warning("SetFTTFile"," The FTT file was set before.");
         return false;
      }
      else{
         m_file = new TFile( fttFileName.c_str() );
         if( m_file->IsZombie() ){
            Error("SetFTTFile"," Fail to open the FTT file %s",fttFileName.c_str());
            return false;
         }
         m_tree = dynamic_cast<TTree*>( m_file->Get("fttTree") );
         m_nentry = m_tree->GetEntries();

         if( m_nentry==0 ){
            Error("SetFTTFile"," Content is empty.");
            return false;
         }
      }

      m_ftt_handler = new LcFTTDataHandler(m_detname);
      m_ftt_handler->SetBranchAddresses(m_tree);
 
      // p.s. To speed up, NO UPDATE while calling GenerateWfm, it requires the indexMap is the
      //      same for all entries. The file should be inspected in advance!
      m_tree->GetEntry(0);
      LoadBasicParameter();
      m_ftt_handler->LoadIndexMap();
      return true;
   } 

   /// 
   Double_t LcFTT::EvalFTTChisq( const Int_t modId, const LcWfm &wfm )
   {
      /// if modId does not exist or energy is too low, -3. is returned
      if( !LoadTemplate( modId, wfm.Integral() ) ) return -3.;

      /// if no local max. is obtained, -2. is returned (peak < threshold)
      m_properMaxSample = GetProperMaxSample( wfm );
      if( m_properMaxSample<0 ) return -2.;

      /// if the waveform cannot be converted into frequency domain completely, -1. is returned
      const Int_t sam_beg = m_properMaxSample - m_wfmLeftRange;
      const Int_t sam_end = m_properMaxSample + m_wfmRightRange;
      if( sam_beg<0 || sam_end>wfm.GetNsample() ) return -1.;

      /// get the waveform to be compared with and normalize it
      LcWfm fwfm = wfm.GetFTWfm( sam_beg, sam_end );
      fwfm *= 1./fwfm.Integral(0,m_nFTTNormSample);      

      /// evaluate the chisq
      return CalcChisq(m_nFTTNormSample, fwfm, m_fttMean, m_fttSigma);
   } 

   Double_t LcFTT::GetTimeToEvalFTT( const LcWfm &wfm ) const
   {
      return ( m_properMaxSample < 1 ) ? -1. : wfm.GetParabolicTime(m_properMaxSample);
   }

   /// private ///
   void LcFTT::LoadBasicParameter()
   {
      m_ftt_handler->LoadFTTData(0);
      LcWfm ftt = m_ftt_handler->GetFTTMean();
      if( ftt.GetNsample() == LcWfmParameter::k_125MHzFTTNsample ){
         m_nFTTNormSample = LcWfmParameter::k_125MHzFwfmNormRange;
         m_wfmLeftRange   = LcWfmParameter::k_125MHzWfmLeftRange;
         m_wfmRightRange  = LcWfmParameter::k_125MHzWfmRightRange;
         m_nominalTime    = LcWfmParameter::k_125MHzNominalSample; 
      }else if( ftt.GetNsample() == LcWfmParameter::k_500MHzFTTNsample ){
         m_nFTTNormSample = LcWfmParameter::k_500MHzFwfmNormRange;
         m_wfmLeftRange   = LcWfmParameter::k_500MHzWfmLeftRange;
         m_wfmRightRange  = LcWfmParameter::k_500MHzWfmRightRange;
         m_nominalTime    = LcWfmParameter::k_500MHzNominalSample;
      }else{
         Error("LoadBasicParameter","unknown template data with #sample = %d",ftt.GetNsample());
      }
   }

   bool LcFTT::LoadTemplate( const Int_t modId, const Double_t energy )
   {
      if( m_ftt_handler==NULL ) return false;
      
      const Int_t index = m_ftt_handler->GetIndex( modId );
      Int_t entry = m_detpar.GetCategoryId( energy );

      if( entry<0 || index<0 ) return false;

      m_tree->GetEntry(entry);
      m_ftt_handler->LoadFTTData(index);
      m_fttMean  = m_ftt_handler->GetFTTMean();
      m_fttSigma = m_ftt_handler->GetFTTSigma();
      return true; 
   }

   void LcFTT::SetDefaultPeakThreshold()
   {
      if     ( m_detname=="FBAR" ) SetPeakThreshold(0.28);
      else if( m_detname=="NCC"  ) SetPeakThreshold(0.25);
      else if( m_detname=="CBAR" ) SetPeakThreshold(0.3);
      else if( m_detname=="IB"   ) SetPeakThreshold(0.35);
      else if( m_detname=="CV"   ) SetPeakThreshold(0.008);
      else if( m_detname=="CC06" ) SetPeakThreshold(0.25);  
      else                         SetPeakThreshold(0.1);
   }

   Int_t LcFTT::GetProperMaxSample( const LcWfm &wfm ) const 
   {
      return wfm.GetProperMaxSample( m_nominalTime, m_peakThreshold );
   }

   Double_t LcFTT::CalcChisq( const Int_t nsample,
                              const LcWfm &test_wfm,
                              const LcWfm &template_mean,
                              const LcWfm &template_sigma ) const
   {
      Int_t calc_nsam = nsample;
      if( nsample > test_wfm.GetNsample() || nsample > template_mean.GetNsample() )
      {
         Warning("CalcChisq"," user input #sample = %d larger than existed waveform.",nsample);  
         calc_nsam = (test_wfm.GetNsample() > template_mean.GetNsample() ) ? 
                      template_mean.GetNsample() : test_wfm.GetNsample();
      }

      Double_t chisq = 0.;
      for( Int_t is=0; is<calc_nsam; ++is )
      {
         Double_t residual = (test_wfm[is] - template_mean[is]) / template_sigma[is];
         chisq += residual * residual;
      } 

      return chisq;
   }

   std::ostream& operator << ( std::ostream& os, const LcFTT& ftt )
   {
      os <<" template mean : " << std::endl;
      os << ftt.m_fttMean << std::endl;
      os <<" template sigma : " << std::endl;
      os << ftt.m_fttSigma << std::endl;

      return os;
   }

}
