#ifndef LCLIB_LCFTT_LCFTTDATA_H
#define LCLIB_LCFTT_LCFTTDATA_H

#include <vector>

#include "TObject.h"

namespace LcLib
{

   class LcFTTData : public TObject
   {
    public : 
      /// constructor
      LcFTTData();

      /// destructor
      virtual ~LcFTTData();

      ///
      virtual void Clear( Option_t* = "");

      ///
      Int_t                 ModId;
      std::vector<Double_t> FTTMean;
      std::vector<Double_t> FTTSigma; 

      ClassDef(LcFTTData,1);
   };

}

#endif /* LcFTTData.h guard */
