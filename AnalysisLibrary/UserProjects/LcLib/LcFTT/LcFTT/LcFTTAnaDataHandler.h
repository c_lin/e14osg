#ifndef LCLIB_LCFTT_LCFTTANADATAHANDLER_H
#define LCLIB_LCFTT_LCFTTANADATAHANDLER_H

#include <vector>

#include "TObject.h"
#include "TTree.h"

namespace LcLib
{

   class LcFTTAnaDataHandler : public TObject
   {
    public : 
      /// constructor
      LcFTTAnaDataHandler( const std::string detname );

      /// destructor
      virtual ~LcFTTAnaDataHandler();

      ///
      void Init();

      bool SetBranchAddresses( TTree *tr );
      bool AddBranches( TTree *tr );

      /// getter
      Int_t GetArraySize() const { return m_nchannel; }

    private :
      const std::string m_detname;

      Int_t    m_nchannel;

      Int_t    m_number;
      Int_t   *m_modId;
      Float_t *m_chisq;
      Float_t *m_time;

      ClassDef(LcFTTAnaDataHandler,1);
   };

}

#endif /* LcFTTAnaDataHandler.h guard */
