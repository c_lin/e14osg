#ifndef LCLIB_LCFTT_LCFTT_H
#define LCLIB_LCFTT_LCFTT_H

#include <string>
#include <map>

#include "TObject.h"
#include "TFile.h"
#include "TTree.h"

#include "LcWfm/LcWfm.h"
#include "LcFTT/LcFTTDataHandler.h"
#include "LcDetPar/LcDetParEneCategory.h"

namespace LcLib
{

   class LcFTT : public TObject
   {
    public :
      /// constructor
      LcFTT( const std::string detname );

      /// destructor
      ~LcFTT();

      ///
      bool SetFTTFile( const std::string fttFileName );

      ///
      void SetPeakThreshold( const Double_t peakThreshold ){ m_peakThreshold = peakThreshold; }
      void SetNominalTime( const Double_t nominalTime ){ m_nominalTime = nominalTime; }

      /// getter
      std::string GetDetectorName() const { return m_detname; }
      std::string GetFileName() const { return m_fname; }

      Double_t GetPeakThreshold() const { return m_peakThreshold; }
      Double_t GetNominalTime() const { return m_nominalTime; }

      /// method
      Double_t EvalFTTChisq( const Int_t modId, const LcWfm &wfm );
      Double_t GetTimeToEvalFTT( const LcWfm &wfm ) const;

    private :
      const std::string m_detname;
      const LcDetParEneCategory m_detpar;

      std::string m_fname;

      TFile *m_file;
      TTree *m_tree;

      LcWfm m_fttMean;
      LcWfm m_fttSigma;

      Int_t m_nentry;

      Int_t m_properMaxSample;

      Double_t m_nominalTime;
      Double_t m_peakThreshold;

      Int_t m_nFTTNormSample;
      Int_t m_wfmLeftRange;
      Int_t m_wfmRightRange; 

      void LoadBasicParameter();
      bool LoadTemplate( const Int_t modId, const Double_t energy );
      void SetDefaultPeakThreshold();
 
      Int_t GetProperMaxSample( const LcWfm &wfm ) const;
     
      Double_t CalcChisq( const Int_t nsample, 
                          const LcWfm &test_wfm,
                          const LcWfm &template_mean,
                          const LcWfm &template_sigma ) const;

      LcFTTDataHandler *m_ftt_handler;


    friend std::ostream& operator<< ( std::ostream& os, const LcFTT& ftt);

      ClassDef(LcFTT,1);
   };

}

#endif /* LcFTT.h guard */
