#ifndef LCLIB_LCFTT_LCFTTDATAHANDLER_H
#define LCLIB_LCFTT_LCFTTDATAHANDLER_H

#include <map>

#include "TObject.h"
#include "TTree.h"
#include "TClonesArray.h"

#include "LcWfm/LcWfm.h"
#include "LcFTT/LcFTTData.h"

namespace LcLib
{

   class LcFTTDataHandler : public TObject
   {
    public : 
      /// constructor
      LcFTTDataHandler( const std::string detname );

      /// destructor
      virtual ~LcFTTDataHandler();

      /// method
      void Clear(Option_t* = "" );

      bool SetBranchAddresses(TTree *tr);
      bool AddBranches(TTree* tr);

      void AddFTTData();
      bool LoadFTTData( const Int_t idx );

      /// setter
      void SetModId( const Int_t modId );
      void SetFTTMean( const LcWfm &fttmean );
      void SetFTTSigma( const LcWfm &fttsigma );
     
      void LoadIndexMap();
 
      /// getter
      std::string GetDetectorName() const { return m_detname; }
      Int_t       GetIndex( const Int_t modId ) const;
      Int_t       GetSize() const;
      Int_t       GetModId() const;
      LcWfm       GetFTTMean() const;
      LcWfm       GetFTTSigma() const;

    private :
      const std::string      m_detname;
      TClonesArray          *m_arrFTTData;
      LcFTTData             *m_fttData;
      
      Int_t                 *m_modId;
      std::vector<Double_t> *m_fttmean_vec;
      std::vector<Double_t> *m_fttsigma_vec;

      std::map<Int_t, Int_t> m_indexMap;

      void ResetFTTDataPointer();
      void SetFTTDataPointer();

      ClassDef(LcFTTDataHandler,1);
   };

}

#endif
