#ifndef LCLIB_LCDBWFM_LCDBWFMDATAHANDLER_H
#define LCLIB_LCDBWFM_LCDBWFMDATAHANDLER_H

#include <string>
#include <map>

#include "TObject.h"
#include "TClonesArray.h"
#include "TSpline.h"
#include "TTree.h"

#include "LcWfm/LcWfmSpline.h"
#include "LcDbWfm/LcDbWfmData.h"

namespace LcLib
{
 
   class LcDbWfmDataHandler : public TObject
   {
    public :
      LcDbWfmDataHandler( const std::string name );
      ~LcDbWfmDataHandler();

      void Clear(Option_t* = "");
 
      bool AddBranches(TTree *tr);
      bool SetBranchAddresses(TTree* tr);

      void SetModId( const Int_t modId );
      void SetNfilled( const Int_t nFilled );
      void SetSplineTime( const Double_t val );
      void SetWfmSpline( const LcWfmSpline &spline );

      std::string GetDetectorName() const { return m_branchName; }
      Int_t GetIndex( const Int_t modId ) const;
      Int_t GetModId() const;
      Int_t GetNfilled() const;
      Double_t GetSplineTime() const;
      LcWfmSpline GetWfmSpline() const;

      Int_t GetSize() const;

      void AddDbWfmData();
      bool LoadDbWfmData( const Int_t index = 0);

      void LoadIndexMap();

    private : 
      const std::string  m_branchName;
      TClonesArray      *m_arrDbWfmData;
      LcDbWfmData       *m_dbWfmData;
    
      Int_t             *m_modId;
      Int_t             *m_nFilled; 
      Double_t          *m_splineTime;
      TSpline3          *m_wfmSpline;

      std::map<Int_t, Int_t> m_indexMap;

      void ResetDbWfmPointer();
      void SetDbWfmPointer();

      ClassDef(LcDbWfmDataHandler,1);

   };
}

#endif
