#ifndef LCLIB_LCDBWFM_LCDBWFMDATA_H
#define LCLIB_LCDBWFM_LCDBWFMDATA_H

#include "TObject.h"
#include "TSpline.h"

namespace LcLib
{

   class LcDbWfmData : public TObject
   {
    public :
      LcDbWfmData();
      virtual ~LcDbWfmData();

      virtual void Clear(Option_t* ="");

      Int_t    ModId;
      Int_t    nFilled;
      Double_t SplineTime;
      TSpline3 WfmSpline;      

      ClassDef(LcDbWfmData,1);
   };

}

#endif /* LcDbWfmData.h guard */
