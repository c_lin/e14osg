#ifndef LCLIB_LCDBWFM_LCDBWFMGENERATOR_H
#define LCLIB_LCDBWFM_LCDBWFMGENERATOR_H

#include <string>
#include <map>

#include "TObject.h"
#include "TFile.h"
#include "TTree.h"

#include "LcDetPar/LcDetParEneCategory.h"
#include "LcDbWfm/LcDbWfmDataHandler.h"
#include "LcWfm/LcWfmSpline.h"

namespace LcLib
{
   class LcDbWfmGenerator : public TObject 
   {
    public :
      /// constructor
      LcDbWfmGenerator( const std::string detname );

      /// destructor
      ~LcDbWfmGenerator();

      ///
      void SetFastMode();
      bool SetDbWfmFile( const std::string dbWfmFileName );

      ///
      Int_t GetNsample() const { return m_nsample; }
      std::string GetDetectorName() const { return m_detname; }

      LcWfm GenerateWfm( const Int_t modId, 
                         const Double_t energy, 
                         const Double_t time,
                         const Double_t wfmFluc = -1. );

    private :
      const std::string m_detname;
      const LcDetParEneCategory m_detpar;

      TFile *m_file;
      TTree *m_tree;
  
      LcDbWfmDataHandler *m_db_handler;

      Int_t m_nentry;
      Int_t m_nsample;
      
      void SetNsampleFromDbWfmData();

      Double_t EvalTimeShift( const Double_t peak_time ) const;
      bool     LoadDbWfmData( const Int_t modId, const Double_t energy );

      LcWfmSpline GetWfmSpline();

      bool     m_isFastMode;

      Int_t    m_entryFast;
      Int_t    m_indexFast;

      typedef std::vector<std::vector<LcWfmSpline*> > WfmSplineData_t;
      typedef std::vector<std::vector<Double_t*>    > WfmTimeData_t;

      WfmSplineData_t m_wfmSplineData;
      WfmTimeData_t   m_wfmTimeData;

      ClassDef(LcDbWfmGenerator,1);
   };
}

#endif /* LcDbWfmGenerator.h guard */
