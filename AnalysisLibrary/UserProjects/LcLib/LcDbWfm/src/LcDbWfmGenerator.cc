#include "LcDbWfm/LcDbWfmGenerator.h"

#include "LcWfm/LcWfmParameter.h"

#include "TMath.h"
#include "TRandom3.h"

namespace LcLib
{

   /// constructor
   LcDbWfmGenerator::LcDbWfmGenerator( const std::string detname )
      : m_detname(detname),
        m_detpar(m_detname)
   {
      m_file = NULL;
      m_tree = NULL; 
      m_db_handler = NULL;
      m_nentry = 0;
      m_nsample = 0;
      m_isFastMode = false;

      ///
      m_entryFast = 0;
      m_indexFast = 0;
   }

   
   LcDbWfmGenerator::~LcDbWfmGenerator()
   {
      delete m_file;
      delete m_tree;
      delete m_db_handler;
   }

   void LcDbWfmGenerator::SetFastMode()
   {
      if( m_file==NULL ){
         Warning("SetFastMode"," The dbWfm file was not set.");
         return;
      }

      if( m_isFastMode ) return;

      m_isFastMode = true;
      m_tree->GetEntry(0);
      const Int_t ncategory = m_tree->GetEntries();
      Info("SetFastMode","[%s] Load %d categories for %d channels",
                          m_db_handler->GetDetectorName().c_str(), 
                          ncategory, m_db_handler->GetSize() );

      for( Int_t icat=0; icat<m_tree->GetEntries(); ++icat )
      {
         m_tree->GetEntry(icat);
         std::vector<LcWfmSpline*> wfmvec;
         std::vector<Double_t*>    timevec;
         for( Int_t imod=0; imod<m_db_handler->GetSize(); ++imod )
         {
            m_db_handler->LoadDbWfmData(imod);
 
            wfmvec.push_back( new LcWfmSpline( m_db_handler->GetWfmSpline() ) );
            timevec.push_back( new Double_t( m_db_handler->GetSplineTime() ) );
         } 
         m_wfmSplineData.push_back(wfmvec);
         m_wfmTimeData.push_back(timevec);
      }
   }

   bool LcDbWfmGenerator::SetDbWfmFile( const std::string dbWfmFileName )
   {
      if( m_file ){
         Warning("SetDbWfmFile"," The dbWfm file was set before.");
         return false;
      }
      else{
          m_file = new TFile( dbWfmFileName.c_str() );
          if( m_file->IsZombie() ){
             Error("SetDbWfmFile"," Fail to open the dbWfm file %s",dbWfmFileName.c_str() );
             return false;
          }
          m_tree = dynamic_cast<TTree*>( m_file->Get("dbWfmTree") );
          m_nentry = m_tree->GetEntries();

          if( m_nentry==0 ){
             Error("SetDbWfmFile"," Content is empty.");
             return false;
          }
      }

      m_db_handler = new LcDbWfmDataHandler(m_detname);
      m_db_handler->SetBranchAddresses(m_tree); 
      SetNsampleFromDbWfmData();

      /// p.s. To speed up, NO UPDATE while calling GenerateWfm, it requires the indexMap is the 
      //       same for all entries. The file should be inspected in advance!
      m_tree->GetEntry(0);
      m_db_handler->LoadIndexMap(); 
 
      return true;
   }

   LcWfm LcDbWfmGenerator::GenerateWfm( const Int_t modId, 
                                        const Double_t energy, 
                                        const Double_t time,
                                        const Double_t wfmFluc )
   {
      /// if timing out of window, empty waveform is returned 
      if( time<0. || time>m_nsample ) return LcWfm(m_nsample);

      /// if no associated waveform in data base, empty waveform is returned 
      if( !LoadDbWfmData( modId, energy ) ) return LcWfm(m_nsample);

      /// evaluate the waveform parameter
      const LcWfmSpline wfm_sp = GetWfmSpline();
      const Double_t delta_time = EvalTimeShift( time );
      const Int_t sam_beg = TMath::CeilNint(delta_time);
      const Int_t sam_end = sam_beg + wfm_sp.GetNsample() - 1; 
      const Double_t time_offset = sam_beg - delta_time;
      
      LcWfm wfm( m_nsample );     
      Double_t norm_area = 0.;
      for( Int_t is = sam_beg; is<sam_end; ++is )
      {
         const Int_t sam_sp = is - sam_beg;
         const Double_t fluc = (wfmFluc>0.) ? gRandom->Gaus(0.,wfmFluc) : 0.;
         const Double_t val = wfm_sp.Eval(sam_sp+time_offset) + fluc;
         norm_area += val;
         if( is < 0 || is >= wfm.GetNsample() ) continue;
         wfm.SetData(is, val );
      }
     
      wfm *= energy / norm_area;
 
      return wfm;
   }


   /// private ///
   void LcDbWfmGenerator::SetNsampleFromDbWfmData() 
   {
      m_tree->GetEntry(0);
      m_db_handler->LoadDbWfmData(0);
      LcWfmSpline wfm_sp = m_db_handler->GetWfmSpline();

      if(       wfm_sp.GetNsample() == LcWfmParameter::k_125MHzWfmLeftRange + 
                                       LcWfmParameter::k_125MHzWfmRightRange ){
         m_nsample = LcWfmParameter::k_125MHzWfmNsample;
      }else if( wfm_sp.GetNsample() == LcWfmParameter::k_500MHzWfmLeftRange + 
                                       LcWfmParameter::k_500MHzWfmRightRange ){
         m_nsample = LcWfmParameter::k_500MHzWfmNsample;
      }else{
         Error("SetNsampleFromDbWfmData","unknown db wfm #sample : %d", wfm_sp.GetNsample() );
         m_nsample = 0;
      }
   }   

   Double_t LcDbWfmGenerator::EvalTimeShift( const Double_t peak_time ) const
   {
      Double_t db_time = -1;
      if( m_isFastMode ){
         db_time = *( (m_wfmTimeData[m_entryFast])[m_indexFast] );
      }else{
         db_time = m_db_handler->GetSplineTime();
      }

      if( db_time < 0 ){
         Warning("EvalTimeShift"," dbWfm is not correctly loaded.");
         return 0.;
      }else{
         return peak_time - db_time;
      }
   }

   LcWfmSpline LcDbWfmGenerator::GetWfmSpline()
   {
      if( m_isFastMode ){
         return *( (m_wfmSplineData[m_entryFast])[m_indexFast] );
      }else{
         return m_db_handler->GetWfmSpline();
      }
   }

   bool LcDbWfmGenerator::LoadDbWfmData( const Int_t modId, const Double_t energy )
   {
      if( m_db_handler==NULL ) return false;
      const Int_t index = m_db_handler->GetIndex( modId );
      Int_t entry = m_detpar.GetCategoryId( energy );
      entry = ( entry < 0 ) ? 0 : entry;

      if( index<0 ) return false;

      if( m_isFastMode ){
         m_entryFast = entry;
         m_indexFast = index;
      }else{
         m_tree->GetEntry(entry);
         m_db_handler->LoadDbWfmData(index);
      }
      return true; 
   }
}
