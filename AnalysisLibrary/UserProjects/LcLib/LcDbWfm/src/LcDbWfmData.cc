#include "LcDbWfm/LcDbWfmData.h"

namespace LcLib
{

   LcDbWfmData::LcDbWfmData()
   {
      Clear();
   }

   LcDbWfmData::~LcDbWfmData()
   {
      ;
   }

   void LcDbWfmData::Clear(Option_t*)
   {
      ModId = -1;
      nFilled = 0;
      SplineTime = 0.;
      TSpline3 spline;
      WfmSpline = spline;
   }

}
