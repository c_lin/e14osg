#include "LcDbWfm/LcDbWfmDataHandler.h"

namespace LcLib
{

   LcDbWfmDataHandler::LcDbWfmDataHandler( const std::string name )
      : m_branchName(name)
   {
      m_arrDbWfmData = new TClonesArray("LcLib::LcDbWfmData");
   }

   LcDbWfmDataHandler::~LcDbWfmDataHandler()
   {
      ;
   }

   void LcDbWfmDataHandler::Clear(Option_t* )
   {
      m_arrDbWfmData->Clear("C");
      ResetDbWfmPointer();
   }

   bool LcDbWfmDataHandler::AddBranches( TTree* tr )
   {
      if( tr==NULL ){
         Error("AddBranches","NULL pointer is given.");
         return false;
      }
     
      tr->Branch( m_branchName.c_str(), &m_arrDbWfmData );
 
      return true;
   }

   bool LcDbWfmDataHandler::SetBranchAddresses( TTree* tr )
   {
      if( tr==NULL ){
         Error("SetBranchAddresses","NULL pointer is given.");
         return false;
      }
      
      tr->SetBranchAddress( m_branchName.c_str(), &m_arrDbWfmData );

      return true;
   }

   void LcDbWfmDataHandler::AddDbWfmData()
   {
      const Int_t n = m_arrDbWfmData->GetEntriesFast();
      m_dbWfmData = new ( (*m_arrDbWfmData)[n] ) LcDbWfmData();
      SetDbWfmPointer();
   } 

   bool LcDbWfmDataHandler::LoadDbWfmData( const Int_t index )
   {
      if( index<0 || index>m_arrDbWfmData->GetEntriesFast() ){
         Error("LoadSplineData","invalid index (0-%d)",m_arrDbWfmData->GetEntriesFast()-1);
         return false;
      }

      m_dbWfmData = dynamic_cast<LcDbWfmData*>(m_arrDbWfmData->At(index));
      SetDbWfmPointer();
      return true;
   }

   void LcDbWfmDataHandler::LoadIndexMap()
   {
      std::map<Int_t, Int_t> indexMap;
      for( Int_t idx=0; idx<GetSize(); ++idx )
      {
         LoadDbWfmData(idx);
         indexMap.insert( std::make_pair(GetModId(),idx) );
      }
      m_indexMap.swap(indexMap);
   }

   void LcDbWfmDataHandler::SetModId( const Int_t modId )
   {
      if( m_modId ) *m_modId = modId;
   }

   void LcDbWfmDataHandler::SetNfilled( const Int_t nFilled )
   {
      if( m_nFilled ) *m_nFilled = nFilled;
   }

   void LcDbWfmDataHandler::SetSplineTime( const Double_t val )
   {
      if( m_splineTime ) *m_splineTime = val;
   }

   void LcDbWfmDataHandler::SetWfmSpline( const LcWfmSpline &spline )
   {
      if( m_wfmSpline ) *m_wfmSpline = spline.GetWfmSpline();
   }

   Int_t LcDbWfmDataHandler::GetIndex( const Int_t modId ) const
   {
      std::map<Int_t, Int_t>::const_iterator it = m_indexMap.find(modId);
      return (it!=m_indexMap.end() ) ? it->second : -1 ;
   }

   Int_t LcDbWfmDataHandler::GetModId() const
   {
      return (m_modId) ? (*m_modId) : -1;
   }

   Int_t LcDbWfmDataHandler::GetNfilled() const
   {
      return (m_nFilled) ? (*m_nFilled) : 0;
   }

   Double_t LcDbWfmDataHandler::GetSplineTime() const
   {
      return (m_splineTime) ? (*m_splineTime) : -1.; 
   }

   LcWfmSpline LcDbWfmDataHandler::GetWfmSpline() const
   {
      if( m_wfmSpline ){
         LcWfmSpline wfm_sp( *m_wfmSpline );
         return wfm_sp;
      }else{
         return LcWfmSpline();
      }
   }

   Int_t LcDbWfmDataHandler::GetSize() const
   {
      return (m_arrDbWfmData) ? m_arrDbWfmData->GetEntriesFast() : 0;
   }

   void LcDbWfmDataHandler::ResetDbWfmPointer()
   {
      m_modId = NULL;
      m_nFilled = NULL;
      m_splineTime = NULL;
      m_wfmSpline = NULL;
   }

   void LcDbWfmDataHandler::SetDbWfmPointer()
   {
      m_modId = &m_dbWfmData->ModId;
      m_nFilled = &m_dbWfmData->nFilled;
      m_splineTime = &m_dbWfmData->SplineTime;
      m_wfmSpline  = &m_dbWfmData->WfmSpline;
   }
  

}
