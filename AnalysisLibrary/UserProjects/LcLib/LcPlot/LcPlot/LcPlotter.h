#ifndef LCLIB_LCPLOT_LCPLOTTER_H
#define LCLIB_LCPLOT_LCPLOTTER_H

#include <string>

#include "TObject.h"
#include "TList.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TGaxis.h"
#include "TH1.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "THStack.h"

#include "LcPlot/LcPlotParameter.h"

namespace LcLib
{

   class LcPlotter : public TObject
   {
      public :
         LcPlotter();
         virtual ~LcPlotter();

      public :
         TCanvas    *E14Canvas();
         TPad       *E14SinglePad( const bool IsWithPalette = false );
         TPad       *E14TopPad();
         TPad       *E14BottomPad();
         TLegend    *E14Legend(  const float x_min, const float y_min, 
                                 const float x_max, const float y_max ); 
         TPaveText  *E14TextBox( const float x_min, const float y_min,
                                 const float x_max, const float y_max );

         void        DrawKOTOLabel( const int tag = LcPlotParameter::k_Publish );
         void        DrawDataLabel( const std::string &txt );

         void        SetTopPlotAxis( TPad* pad );
         void        SetButtomPlotAxis( TPad* pad );
         void        SetSinglePlotAxis( TPad* pad );
         void        SetAxis( TPad* pad, std::string type );

         void        SetTopPlotAxis( TH1* plot );
         void        SetBottomPlotAxis( TH1* plot );
         void        SetAxis( TH1* plot ); 

      private :
         TH1        *FindHistObj( TObject* obj );

      ///
      ClassDef(LcPlotter, 1);  

   };

}
#endif /* End of LcPlotter.h guard*/
