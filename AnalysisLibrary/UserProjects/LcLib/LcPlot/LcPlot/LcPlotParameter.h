#ifndef LCLIB_LCPLOT_LCPLOTPARAMETER_H
#define LCLIB_LCPLOT_LCPLOTPARAMETER_H

namespace LcLib
{
   namespace LcPlotParameter
   {
      /// default canvas settings
      const double k_Default_Canvas_Width  = 650.;
      const double k_Default_Canvas_Height = 500.;

      /// default pad settings
      const double k_Plot_X_Min = 0.13 ;
      const double k_Plot_Y_Min = 0.105;
      const double k_Plot_X_Max = 0.95 ;
      const double k_Plot_Y_Max = 0.9  ;

      /// default font settings
      const int    k_Font_Type            = 43; // SANS + absolute font size
      const int    k_Axis_Title_Font_Size = 20;
      const int    k_Axis_Label_Font_Size = 16;
      const int    k_Text_Font_Size       = 16;

      /// text alignment settings
      const int    k_Bottom_Left     = 11;
      const int    k_Bottom_Right    = 31;
      const int    k_Top_Left        = 13;
      const int    k_Top_Right       = 33;

      const double k_Text_Margin     = 0.04;
      const double k_Text_Height     = k_Text_Font_Size / k_Default_Canvas_Height;
      const double k_Rel_Line_Height = 1.5 ;
      const double k_Line_Height     = k_Text_Height * k_Rel_Line_Height;
      const double k_Plot_X_Text_Min = k_Plot_X_Min + k_Text_Margin;
      const double k_Plot_X_Text_Max = k_Plot_X_Max - k_Text_Margin;
      const double k_Plot_Y_Text_Min = k_Plot_Y_Min + k_Text_Margin;
      const double k_Plot_Y_Text_Max = k_Plot_Y_Max - k_Text_Margin;

      const double k_X_Title_Offset  = 3.0;
      const double k_Y_Title_Offset  = 1.2;
      const int    k_Max_Digits      = 4  ;

      /// label plotting/
      enum
      {
         k_Preliminary = 0 ,
         k_Simulation      ,
         k_Publish
      };

      /// divide
      const double k_Top_Bottom_Sep  = 0.3;
   }
}

#endif /* End of guard for LcPlotParameter.h*/
