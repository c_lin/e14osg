#include "LcPlot/LcPlotter.h"

namespace LcLib
{

   /// contructor ///
   LcPlotter::LcPlotter()
   {
      ;
   }

   /// destructor ///
   LcPlotter::~LcPlotter()
   {
      ;
   } 

   /// plot settings ///
   TCanvas* LcPlotter::E14Canvas()
   {
      TCanvas* c = new TCanvas( "c", "c", LcPlotParameter::k_Default_Canvas_Width, 
                                          LcPlotParameter::k_Default_Canvas_Height );
      c->SetTicks( 1, 1 );
      return c;
   }

   TPad* LcPlotter::E14SinglePad( const bool IsWithPalette )
   {
      TPad* pad = new TPad( "pad", "", 0, 0.00, 1, 1.0 );
      pad->SetLeftMargin( LcPlotParameter::k_Plot_X_Min );
      pad->SetBottomMargin( LcPlotParameter::k_Plot_Y_Min );
      pad->SetTopMargin( 1 - LcPlotParameter::k_Plot_Y_Max );

      if( !IsWithPalette )
      {
         pad->SetTicks( 1, 1 );
         pad->SetRightMargin( 1 - LcPlotParameter::k_Plot_X_Max );
      }
      return pad;
   }

   TPad* LcPlotter::E14TopPad()
   {
      TPad* pad = new TPad( "toppad", "", 0, LcPlotParameter::k_Top_Bottom_Sep, 1., 1.0 );
      pad->SetTicks( 1, 1 );
      pad->SetBottomMargin( 0.05 );
      pad->SetLeftMargin( LcPlotParameter::k_Plot_X_Min );
      pad->SetRightMargin( 1 - LcPlotParameter::k_Plot_X_Max );
      pad->SetTopMargin(   ( 1 - LcPlotParameter::k_Plot_Y_Max ) 
                         / ( 1 - LcPlotParameter::k_Top_Bottom_Sep ) );
      return pad;
   } 

   TPad* LcPlotter::E14BottomPad()
   {
      TPad* pad = new TPad( "botpad", "", 0, 0.00, 1, LcPlotParameter::k_Top_Bottom_Sep );
      pad->SetTicks( 1, 1 );
      pad->SetTopMargin( 0.025 );
      pad->SetLeftMargin( LcPlotParameter::k_Plot_X_Min );
      pad->SetRightMargin( 1 - LcPlotParameter::k_Plot_X_Max );
      pad->SetBottomMargin(   ( LcPlotParameter::k_Plot_Y_Min ) 
                            / ( LcPlotParameter::k_Top_Bottom_Sep ) );
      return pad; 
   }

   TLegend *LcPlotter::E14Legend( const float x_min, 
                                  const float y_min,
                                  const float x_max, 
                                  const float y_max )
   {
      TLegend* ans = new TLegend( x_min, y_min, x_max, y_max );
      // Setting up default values
      ans->SetBorderSize( 0 );
      ans->SetTextFont( LcPlotParameter::k_Font_Type );
      ans->SetTextSize( LcPlotParameter::k_Text_Font_Size );
      ans->SetFillColorAlpha( 0, 0 );
      return ans;
   }

   TPaveText *LcPlotter::E14TextBox( const float x_min, 
                                     const float y_min,
                                     const float x_max, 
                                     const float y_max )
   {
      TPaveText* ans = new TPaveText( x_min, y_min, x_max, y_max, "NDC" );
      ans->SetTextFont( LcPlotParameter::k_Font_Type );
      ans->SetTextSize( LcPlotParameter::k_Text_Font_Size );
      ans->SetFillColor( kWhite );
      return ans;
   }

   /// draw labels ///
   void  LcPlotter::DrawKOTOLabel( const int tag )
   {
      TLatex tl;
      tl.SetNDC( kTRUE );
      tl.SetTextFont( LcPlotParameter::k_Font_Type );
      tl.SetTextSize( LcPlotParameter::k_Axis_Title_Font_Size );
      tl.SetTextAlign( LcPlotParameter::k_Bottom_Left );
      std::string text = "#bf{KOTO} ";

      if( tag == LcPlotParameter::k_Preliminary ){
         text += "#it{Preliminary}";
      }else if( tag == LcPlotParameter::k_Simulation ){
         text += "#it{Simulation}";
      }

      tl.DrawLatex(   LcPlotParameter::k_Plot_X_Min
                    , LcPlotParameter::k_Plot_Y_Max + LcPlotParameter::k_Text_Margin / 4
                    , text.c_str() );
   }

   void  LcPlotter::DrawDataLabel( const std::string &txt )
   {
      TLatex tl;
      tl.SetNDC( kTRUE );
      tl.SetTextFont( LcPlotParameter::k_Font_Type );
      tl.SetTextSize( LcPlotParameter::k_Axis_Title_Font_Size );
      tl.SetTextAlign( LcPlotParameter::k_Bottom_Right );
      tl.DrawLatex(   LcPlotParameter::k_Plot_X_Max
                    , LcPlotParameter::k_Plot_Y_Max + ( LcPlotParameter::k_Text_Margin / 4 )
                    , txt.c_str() );
   }

   /// Axis settings ///
   void LcPlotter::SetTopPlotAxis( TPad* pad )
   {
      SetAxis(pad,"top");
   }

   void LcPlotter::SetButtomPlotAxis( TPad* pad )
   {
      SetAxis(pad,"bottom");
   }

   void LcPlotter::SetSinglePlotAxis( TPad* pad )
   {
      SetAxis(pad,"single");
   }

   void LcPlotter::SetAxis( TPad* pad, std::string type )
   {
      TIter iter( pad->GetListOfPrimitives() );
      TObject *obj  = NULL;
      while( (obj = iter() ) )
      {
         TH1* plot = FindHistObj(obj);
         if(plot && type=="top")    SetTopPlotAxis( plot );
         if(plot && type=="bottom") SetBottomPlotAxis( plot );
         if(plot && type=="single") SetAxis( plot );
      }
   }

   void LcPlotter::SetTopPlotAxis( TH1* plot )
   {
      SetAxis( plot );
      plot->GetXaxis()->SetLabelSize( 0 );
      plot->GetXaxis()->SetTitleSize( 0 );
   }

   void LcPlotter::SetBottomPlotAxis( TH1* plot )
   {
      SetAxis( plot );
      plot->GetYaxis()->SetNdivisions( 503 );
      plot->GetXaxis()->SetTitleOffset( LcPlotParameter::k_X_Title_Offset );
   }

   void LcPlotter::SetAxis( TH1* plot )
   {
      // Global settings from TGaxis
      TGaxis::SetMaxDigits( LcPlotParameter::k_Max_Digits );
      TGaxis::SetExponentOffset( -1000, -1000 );

      plot->GetXaxis()->SetLabelFont( LcPlotParameter::k_Font_Type );
      plot->GetXaxis()->SetTitleFont( LcPlotParameter::k_Font_Type );
      plot->GetYaxis()->SetLabelFont( LcPlotParameter::k_Font_Type );
      plot->GetYaxis()->SetTitleFont( LcPlotParameter::k_Font_Type );
      plot->GetXaxis()->SetLabelSize( LcPlotParameter::k_Axis_Label_Font_Size );
      plot->GetXaxis()->SetTitleSize( LcPlotParameter::k_Axis_Title_Font_Size );
      plot->GetYaxis()->SetLabelSize( LcPlotParameter::k_Axis_Label_Font_Size );
      plot->GetYaxis()->SetTitleSize( LcPlotParameter::k_Axis_Title_Font_Size );
      plot->GetYaxis()->SetTitleOffset( LcPlotParameter::k_Y_Title_Offset );
   }

   ///// private /////
   TH1* LcPlotter::FindHistObj( TObject* obj )
   {
       TH1 *plot = NULL;

       if( obj->InheritsFrom("TH1") ){
          plot = dynamic_cast<TH1*>( obj );
       }else if( obj->InheritsFrom("TGraph") ){
          plot = ( dynamic_cast<TGraph*>( obj ) )->GetHistogram();
       }else if( obj->InheritsFrom("TGraphErrors") ){
          plot = ( dynamic_cast<TGraphErrors*>( obj ) )->GetHistogram();
       }else if( obj->InheritsFrom("TF1") ){
          plot = ( dynamic_cast<TF1*>( obj ) )->GetHistogram();
       }else if( obj->InheritsFrom("TMultiGraph") ){
          plot = ( dynamic_cast<TMultiGraph*>( obj ) )->GetHistogram();
       }else if( obj->InheritsFrom("THStack")){
          plot = ( dynamic_cast<THStack*>( obj ) )->GetHistogram();
       }

      return plot;      
   }

}
