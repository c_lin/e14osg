#ifndef LCLIB_LCANA_LCANAVETOCV_H
#define LCLIB_LCANA_LCANAVETOCV_H

#include "TObject.h"
#include "TTree.h"

#include "LcAna/LcAnaVeto.h"
#include "LcRecData/LcRecData.h"

namespace LcLib
{
   class LcAnaVetoCV : public LcAnaVeto
   {
    public:
      /// constructor
      LcAnaVetoCV();

      /// destructor
      virtual ~LcAnaVetoCV();

      /// 
      void         SetDefaultParameter();
      virtual bool SetBranchAddresses( TTree *tr );
      virtual bool AddBranches( TTree *tr );

    protected:
      virtual void SetModuleData( const LcRecData *data,
                                  IndexMap_t &indexMap,
                                  std::vector<Float_t> &eneVec,
                                  std::vector<Float_t> &timeVec );

      virtual void SetFTTData( const LcRecData *data,
                               IndexMap_t &indexMap,
                               std::vector<Float_t> &fttVec );

    private:
   
      ClassDef(LcAnaVetoCV,1);
   };

}

#endif /* LcAnaVetoCV.h guard */
