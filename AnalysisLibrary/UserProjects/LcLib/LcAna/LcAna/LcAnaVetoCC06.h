#ifndef LCLIB_LCANA_LCANAVETOCC06_H
#define LCLIB_LCANA_LCANAVETOCC06_H

#include "TObject.h"
#include "TTree.h"

#include "LcAna/LcAnaVeto.h"
#include "LcRecData/LcRecData.h"

namespace LcLib
{
   class LcAnaVetoCC06 : public LcAnaVeto
   {
    public:
      /// constructor
      LcAnaVetoCC06();

      /// destructor
      virtual ~LcAnaVetoCC06();

      /// 
      void         SetDefaultParameter();
      virtual bool SetBranchAddresses( TTree *tr );
      virtual bool AddBranches( TTree *tr );

    protected:
      virtual void SetModuleData( const LcRecData *data,
                                  IndexMap_t &indexMap,
                                  std::vector<Float_t> &eneVec,
                                  std::vector<Float_t> &timeVec );

      virtual void SetFTTData( const LcRecData *data,
                               IndexMap_t &indexMap,
                               std::vector<Float_t> &fttVec );

    private:
      Float_t GetDeltaTime( const Float_t hitTime, const Float_t averageClusterTime );
   
      ClassDef(LcAnaVetoCC06,1);
   };

}

#endif /* LcAnaVetoCC06.h guard */
