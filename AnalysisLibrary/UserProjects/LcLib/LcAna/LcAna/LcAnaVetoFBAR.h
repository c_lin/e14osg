#ifndef LCLIB_LCANA_LCANAVETOFBAR_H
#define LCLIB_LCANA_LCANAVETOFBAR_H

#include "TObject.h"
#include "TTree.h"

#include "LcAna/LcAnaVeto.h"
#include "LcRecData/LcRecData.h"

namespace LcLib
{
   class LcAnaVetoFBAR : public LcAnaVeto
   {
    public:
      /// constructor
      LcAnaVetoFBAR();

      /// destructor
      virtual ~LcAnaVetoFBAR();

      /// 
      void         SetDefaultParameter();
      virtual bool SetBranchAddresses( TTree *tr );
      virtual bool AddBranches( TTree *tr );

    protected:
      virtual void SetModuleData( const LcRecData *data,
                                  IndexMap_t &indexMap,
                                  std::vector<Float_t> &eneVec,
                                  std::vector<Float_t> &timeVec );

      virtual void SetFTTData( const LcRecData *data,
                               IndexMap_t &indexMap,
                               std::vector<Float_t> &fttVec );

    private:
      Float_t GetDeltaTime( const Float_t hitTime, const Float_t averageClusterTime );
   
      ClassDef(LcAnaVetoFBAR,1);
   };

}

#endif /* LcAnaVetoFBAR.h guard */
