#ifndef LCLIB_LCANA_LCANAVETOIB_H
#define LCLIB_LCANA_LCANAVETOIB_H

#include "TObject.h"
#include "TTree.h"

#include "LcAna/LcAnaVeto.h"
#include "LcRecData/LcRecData.h"

namespace LcLib
{
   class LcAnaVetoIB : public LcAnaVeto
   {
    public:
      /// constructor
      LcAnaVetoIB();

      /// destructor
      virtual ~LcAnaVetoIB();

      /// 
      void         SetDefaultParameter();
      virtual bool SetBranchAddresses( TTree *tr );
      virtual bool AddBranches( TTree *tr );

    protected:
      virtual void SetModuleData( const LcRecData *data,
                                  IndexMap_t &indexMap,
                                  std::vector<Float_t> &eneVec,
                                  std::vector<Float_t> &timeVec );

      virtual void SetFTTData( const LcRecData *data,
                               IndexMap_t &indexMap,
                               std::vector<Float_t> &fttVec );

    private:
      Float_t GetDeltaTime( const Float_t hitTime, 
                            const Float_t hitZ, 
                            const Float_t averageClusterTime );
   
      ClassDef(LcAnaVetoIB,1);
   };

}

#endif /* LcAnaVetoIB.h guard */
