#ifndef LCLIB_LCANA_LCANAVETO_H
#define LCLIB_LCANA_LCANAVETO_H

#include <map>

#include "TObject.h"
#include "TTree.h"

#include "LcRecData/LcRecData.h"

namespace LcLib
{
   class LcAnaVeto : public TObject
   {
    public:
      /// constructor
      LcAnaVeto();

      /// destructor
      virtual ~LcAnaVeto();

      /// methods
      void            Reset();
      virtual bool    SetBranchAddresses( TTree *tr ) = 0;
      virtual bool    AddBranches( TTree *tr ) = 0;
      void            UpdateVars( const LcRecData *data );

      /// setter ///
      void    SetNominalTime      ( const Float_t nominalTime )
                                                         { m_nominalTime = nominalTime; }
      void    SetEnergyThreshold  ( const Float_t energyThreshold )
                                                 { m_energyThreshold = energyThreshold; }
      void    SetFTTChisqThreshold( const Float_t fttChisqThreshold )
                                             { m_fttChisqThreshold = fttChisqThreshold; }
      void    SetNarrowVetoWindow( const Float_t minTime, const Float_t maxTime );
      void    SetWideVetoWindow( const Float_t minTime, const Float_t maxTime );
      void    SetDeadModuleVec( const std::vector<Int_t> deadIdVec ){ m_deadIdVec = deadIdVec; }

      /// getter ///
      std::string GetDetectorName()      const { return m_detname; }
      Float_t     GetNominalTime ()      const { return m_nominalTime;       }
      Float_t     GetEnergyThreshold()   const { return m_energyThreshold;   }
      Float_t     GetFTTChisqThreshold() const { return m_fttChisqThreshold; }

      Int_t    GetNhit() const { return m_indexMap.size(); }

      Int_t    GetIndex    ( const Int_t modId ) const;
      Int_t    GetModId    ( const Int_t index ) const;
      Float_t  GetEnergy   ( const Int_t index ) const;
      Float_t  GetDeltaTime( const Int_t index ) const;
      Float_t  GetFTTChisq ( const Int_t index ) const;

      Int_t    GetProperModId()    const { return m_properModId;    }
      Float_t  GetProperTime()     const { return m_properTime;     }
      Float_t  GetProperEnergy()   const { return m_properEnergy;   }
      Float_t  GetProperFTTChisq() const { return m_properFTTChisq; }

      Int_t    GetMaskingProperModId()    const { return m_maskingProperModId;    }
      Float_t  GetMaskingProperTime()     const { return m_maskingProperTime;     }
      Float_t  GetMaskingProperEnergy()   const { return m_maskingProperEnergy;   }
      Float_t  GetMaskingProperFTTChisq() const { return m_maskingProperFTTChisq; }

      /// algorithm ///
      Int_t   FindProperIndex( const Float_t nominalTime, 
                               const Float_t energyThreshold,
                               const Float_t fttChisqThreshold = -9999. ) const;

    protected:
      std::string m_detname;

      typedef std::map<Int_t, Int_t> IndexMap_t;
      IndexMap_t            m_indexMap;
      std::vector<Float_t>  m_eneVec;
      std::vector<Float_t>  m_timeVec;
      std::vector<Float_t>  m_fttVec;

      virtual void SetModuleData( const LcRecData *data,
                                  IndexMap_t &indexMap,
                                  std::vector<Float_t> &eneVec,
                                  std::vector<Float_t> &timeVec ) = 0;

      virtual void SetFTTData( const LcRecData *data,
                               IndexMap_t &indexMap,
                               std::vector<Float_t> &fttVec ) = 0;

      void InitializeFTTVecByIndexMap( const IndexMap_t &indexMap, 
                                       std::vector<Float_t> &fttVec );
     
      Int_t GetIndexFromMap( const IndexMap_t &indexMap, const Int_t modId ) const;

      void UpdateInputVars( const LcRecData *data );
      void UpdateProperVars();
      void UpdateVetoCondition();
      void UpdateDeadModule();

      Float_t m_defaultModId;
      Float_t m_defaultEnergy;
      Float_t m_defaultDeltaTime;
      Float_t m_defaultFTTChisq;

      Float_t m_nominalTime;
      Float_t m_energyThreshold;
      Float_t m_fttChisqThreshold;

      Float_t m_narrowWindowMinTime;
      Float_t m_narrowWindowMaxTime;
      Float_t m_wideWindowMinTime;
      Float_t m_wideWindowMaxTime;

      std::vector<Int_t> m_deadIdVec;

      /// branch variables ///
      Int_t   m_properModId;
      Float_t m_properTime;
      Float_t m_properEnergy;
      Float_t m_properFTTChisq;

      Int_t   m_maskingProperModId;
      Float_t m_maskingProperTime;
      Float_t m_maskingProperEnergy;
      Float_t m_maskingProperFTTChisq;

      Bool_t  m_isVeto;

      ClassDef(LcAnaVeto,1);
   };

}

#endif /* LcAnaVeto.h guard */
