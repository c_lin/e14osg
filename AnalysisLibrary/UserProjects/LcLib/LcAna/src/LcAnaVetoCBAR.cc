#include "LcAna/LcAnaVetoCBAR.h"

#include "MTAnalysisLibrary/MTBasicParameters.h"

#include "TMath.h"

namespace LcLib
{
   /// constructor
   LcAnaVetoCBAR::LcAnaVetoCBAR()
   {
      SetDefaultParameter();
      m_detname = "CBAR";
   }

   /// destructor
   LcAnaVetoCBAR::~LcAnaVetoCBAR()
   {
      ;
   }

   /// 
   void LcAnaVetoCBAR::SetDefaultParameter()
   {
      m_nominalTime = 42.6;
      m_energyThreshold = 1.;
      m_fttChisqThreshold = 20.;
      SetNarrowVetoWindow(34.6,54.6);
      SetWideVetoWindow(12.6,72.6);
   }

   bool LcAnaVetoCBAR::SetBranchAddresses( TTree *tr )
   {
      if( tr==NULL ){
         Error("SetBranchAddresses","null pointer is given.");
         return false;
      }

      tr->SetBranchAddress("CBARProperModID",&m_properModId);
      tr->SetBranchAddress("CBARProperTime",&m_properTime);
      tr->SetBranchAddress("CBARProperEne",&m_properEnergy);
      tr->SetBranchAddress("CBARProperFTTChisq",&m_properFTTChisq);

      tr->SetBranchAddress("CBARMaskingProperIndex",&m_maskingProperModId);
      tr->SetBranchAddress("CBARMaskingProperTime",&m_maskingProperTime);
      tr->SetBranchAddress("CBARMaskingProperEne",&m_maskingProperEnergy);
      tr->SetBranchAddress("CBARMaskingProperFTTChisq",&m_maskingProperFTTChisq);

      //tr->SetBranchAddress("IsCBARVeto",&m_isVeto);
      return true;
   }

   bool LcAnaVetoCBAR::AddBranches( TTree *tr )
   {
      if( tr==NULL ){
         Error("AddBranches","null pointer is given.");
         return false;
      }

      tr->Branch("CBARProperModID",&m_properModId,"CBARProperModID/I");
      tr->Branch("CBARProperTime",&m_properTime,"CBARProperTime/F");
      tr->Branch("CBARProperEne",&m_properEnergy,"CBARProperEne/F");
      tr->Branch("CBARProperFTTChisq",&m_properFTTChisq,"CBARProperFTTChisq/F");

      tr->Branch("CBARMaskingProperModID",&m_maskingProperModId,"CBARMaskingProperModID/I");
      tr->Branch("CBARMaskingProperTime",&m_maskingProperTime,"CBARMaskingProperTime/F");
      tr->Branch("CBARMaskingProperEne",&m_maskingProperEnergy,"CBARMaskingProperEne/F");
      tr->Branch("CBARMaskingProperFTTChisq",&m_maskingProperFTTChisq,"CBARMaskingProperFTTChisq/F");

      //tr->Branch("IsCBARVeto",&m_isVeto,"IsCBARVeto/O");
      return true;
   }

   Float_t LcAnaVetoCBAR::GetDeltaTime( const Float_t hitTime,
                                        const Float_t hitZ,
                                        const Float_t averageClusterTime,
                                        const Bool_t  isInnerModule )
   {
      const Float_t k_BadTimeThreshold = -1000.;
      if(hitTime < k_BadTimeThreshold ) return m_defaultDeltaTime;

      const Double_t k_BarrelRadius = (isInnerModule) ? MTBP::CBARInnerInnerR
                                                      : MTBP::CBAROuterInnerR;
      const Double_t k_BarrelCenter = MTBP::CBARZPosition + MTBP::CBARLength / 2.;
      const Double_t k_C = 300.; //[mm/ns]

      const Double_t deltaZ = MTBP::CSIZPosition - (hitZ + k_BarrelCenter);
      const Double_t tBack = TMath::Sqrt( k_BarrelRadius*k_BarrelRadius + deltaZ*deltaZ) / k_C;
      return ( hitTime - averageClusterTime + tBack );
   }

   /// private ///
   void LcAnaVetoCBAR::SetModuleData( const LcRecData *data,
                                      IndexMap_t &indexMap,
                                      std::vector<Float_t> &eneVec,
                                      std::vector<Float_t> &timeVec )
   {
      for( Int_t imod=0; imod<data->CBARModuleNumber; ++imod )
      {
         indexMap.insert( std::make_pair(data->CBARModuleModID[imod], imod) );
         const Bool_t isInnerModule = data->CBARModuleModID[imod]<32;
         const Float_t deltaTime = GetDeltaTime( data->CBARModuleHitTime[imod], 
                                                 data->CBARModuleHitZ[imod],
                                                 data->AverageClusterTime,
                                                 isInnerModule );
         eneVec.push_back( data->CBARModuleEne[imod] );
         timeVec.push_back( deltaTime );
      }
   }

   void LcAnaVetoCBAR::SetFTTData( const LcRecData *data,
                                   IndexMap_t &indexMap,
                                   std::vector<Float_t> &fttVec )
   {
      InitializeFTTVecByIndexMap(indexMap, fttVec);
      const Int_t vecSize = fttVec.size();

      for( Int_t ich=0; ich<data->CBARFTTNumber; ++ich )
      {
         const Int_t channelId = data->CBARFTTModID[ich];
         const Int_t modId     = ( channelId < 100 ) ? channelId : (channelId - 100 );
         const Int_t index = GetIndexFromMap(indexMap, modId); 
         if( index<0 || index>=vecSize ) continue; 
         if( data->CBARFTTChisq[ich] > fttVec[index] ) fttVec[index] = data->CBARFTTChisq[ich];
      }
   }

}
