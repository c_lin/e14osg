#include "LcAna/LcAnaVetoNCC.h"

namespace LcLib
{
   /// constructor
   LcAnaVetoNCC::LcAnaVetoNCC()
   {
      SetDefaultParameter();
      m_detname = "NCC";
   }

   /// destructor
   LcAnaVetoNCC::~LcAnaVetoNCC()
   {
      ;
   }

   /// 
   void LcAnaVetoNCC::SetDefaultParameter()
   {
      m_nominalTime = 9.1;
      m_energyThreshold = 1.;
      m_fttChisqThreshold = 20.;
      SetNarrowVetoWindow(m_nominalTime-20.,m_nominalTime+20.);
      SetWideVetoWindow(m_nominalTime-75.,m_nominalTime+75.);
   }

   bool LcAnaVetoNCC::SetBranchAddresses( TTree *tr )
   {
      if( tr==NULL ){
         Error("SetBranchAddresses","null pointer is given.");
         return false;
      }

      tr->SetBranchAddress("NCCProperModID",&m_properModId);
      tr->SetBranchAddress("NCCProperTime",&m_properTime);
      tr->SetBranchAddress("NCCProperEne",&m_properEnergy);
      tr->SetBranchAddress("NCCProperFTTChisq",&m_properFTTChisq);

      tr->SetBranchAddress("NCCMaskingProperIndex",&m_maskingProperModId);
      tr->SetBranchAddress("NCCMaskingProperTime",&m_maskingProperTime);
      tr->SetBranchAddress("NCCMaskingProperEne",&m_maskingProperEnergy);
      tr->SetBranchAddress("NCCMaskingProperFTTChisq",&m_maskingProperFTTChisq);

      //tr->SetBranchAddress("IsNCCVeto",&m_isVeto);
      return true;
   }

   bool LcAnaVetoNCC::AddBranches( TTree *tr )
   {
      if( tr==NULL ){
         Error("AddBranches","null pointer is given.");
         return false;
      }

      tr->Branch("NCCProperModID",&m_properModId,"NCCProperModID/I");
      tr->Branch("NCCProperTime",&m_properTime,"NCCProperTime/F");
      tr->Branch("NCCProperEne",&m_properEnergy,"NCCProperEne/F");
      tr->Branch("NCCProperFTTChisq",&m_properFTTChisq,"NCCProperFTTChisq/F");

      tr->Branch("NCCMaskingProperModID",&m_maskingProperModId,"NCCMaskingProperModID/I");
      tr->Branch("NCCMaskingProperTime",&m_maskingProperTime,"NCCMaskingProperTime/F");
      tr->Branch("NCCMaskingProperEne",&m_maskingProperEnergy,"NCCMaskingProperEne/F");
      tr->Branch("NCCMaskingProperFTTChisq",&m_maskingProperFTTChisq,"NCCMaskingProperFTTChisq/F");

      //tr->Branch("IsNCCVeto",&m_isVeto,"IsNCCVeto/O");
      return true;
   }

   Float_t LcAnaVetoNCC::GetDeltaTime( const Float_t hitTime, const Float_t averageClusterTime )
   {
      const Float_t k_BadTimeThreshold = -1000.;
      return (hitTime < k_BadTimeThreshold) ? m_defaultDeltaTime 
                                            : (hitTime - averageClusterTime); 
   }

   /// private ///
   void LcAnaVetoNCC::SetModuleData( const LcRecData *data,
                                     IndexMap_t &indexMap,
                                     std::vector<Float_t> &eneVec,
                                     std::vector<Float_t> &timeVec )
   {
      for( Int_t imod=0; imod<data->NCCModuleNumber; ++imod )
      {
         indexMap.insert( std::make_pair(data->NCCModuleModID[imod], imod) );
         const Float_t deltaTime = GetDeltaTime( data->NCCModuleTime[imod], 
                                                 data->AverageClusterTime);
         eneVec.push_back( data->NCCModuleEne[imod] );
         timeVec.push_back( deltaTime );
      }
   }

   void LcAnaVetoNCC::SetFTTData( const LcRecData *data,
                                  IndexMap_t &indexMap,
                                  std::vector<Float_t> &fttVec )
   {
      InitializeFTTVecByIndexMap(indexMap, fttVec);
      const Int_t vecSize = fttVec.size();

      for( Int_t imod=0; imod<data->NCCFTTNumber; ++imod )
      {
         const Int_t index = GetIndexFromMap(indexMap, data->NCCFTTModID[imod]); 
         if( index<0 || index>=vecSize ) continue; 
         fttVec[index] = data->NCCFTTChisq[imod];
      }
   }

}
