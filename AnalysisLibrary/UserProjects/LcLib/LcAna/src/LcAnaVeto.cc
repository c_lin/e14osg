#include "LcAna/LcAnaVeto.h"

#include "TMath.h"

namespace LcLib
{
   /// constructor
   LcAnaVeto::LcAnaVeto()
      : m_defaultModId(-1),
        m_defaultEnergy(-1.),
        m_defaultDeltaTime(-9999.),
        m_defaultFTTChisq(-10.)
   {
      ;
   }

   /// destructor
   LcAnaVeto::~LcAnaVeto()
   {
      ;
   }

   ///
   void LcAnaVeto::Reset()
   {
      m_properModId = m_defaultModId;
      m_properTime = m_defaultDeltaTime;
      m_properEnergy = m_defaultEnergy;
      m_properFTTChisq = m_defaultFTTChisq;

      m_maskingProperModId = m_defaultModId;
      m_maskingProperTime = m_defaultDeltaTime;
      m_maskingProperEnergy = m_defaultEnergy;
      m_maskingProperFTTChisq = m_defaultFTTChisq;

      m_isVeto = false;
   }

   void LcAnaVeto::UpdateVars( const LcRecData *data )
   {
      UpdateInputVars(data);
      UpdateDeadModule();
      UpdateProperVars();
      //UpdateVetoCondition();
   }

   ///
   Int_t LcAnaVeto::GetIndex( const Int_t modId ) const
   {
       return GetIndexFromMap( m_indexMap, modId );
   }

   Int_t LcAnaVeto::GetModId( const Int_t index ) const
   {
      if( index<0 || index>=GetNhit() ) return -1;
      IndexMap_t::const_iterator it = m_indexMap.begin();
      std::advance(it,index);
      return it->first;
   }
   
   Float_t LcAnaVeto::GetEnergy( const Int_t index ) const
   {
      return (index<0 || index>=GetNhit() ) ? m_defaultEnergy : m_eneVec[index];
   }
  
   Float_t LcAnaVeto::GetDeltaTime( const Int_t index ) const
   {
      return (index<0 || index>=GetNhit() ) ? m_defaultDeltaTime : m_timeVec[index]; 
   }
      
   Float_t LcAnaVeto::GetFTTChisq( const Int_t index ) const
   {
      return (index<0 || index>=GetNhit() ) ? m_defaultFTTChisq : m_fttVec[index];
   }

   /// algorithm ///
   Int_t LcAnaVeto::FindProperIndex( const Float_t nominalTime,
                                     const Float_t energyThreshold,
                                     const Float_t fttChisqThreshold ) const
   {
      Float_t timeDiff = 9999.;
      Int_t   properIndex = -1;
      for( Int_t imod=0; imod<GetNhit(); ++imod )
      {
         if(    GetEnergy(imod) > energyThreshold 
             && GetFTTChisq(imod) > fttChisqThreshold
             && TMath::Abs(GetDeltaTime(imod) - nominalTime) < timeDiff )
         {
            properIndex = imod;
            timeDiff = TMath::Abs(GetDeltaTime(imod) - nominalTime);
         }
      }
      return properIndex;
   }

   /// protected ///
   void LcAnaVeto::InitializeFTTVecByIndexMap( const IndexMap_t &indexMap,
                                               std::vector<Float_t> &fttVec )
   {
      std::vector<Float_t>( indexMap.size(), m_defaultFTTChisq ).swap( fttVec );
   }

   /// protected ///
   Int_t LcAnaVeto::GetIndexFromMap( const IndexMap_t &indexMap, const Int_t modId ) const
   {
      IndexMap_t::const_iterator it = indexMap.find( modId );
      return ( it!=indexMap.end() ) ? it->second : -1;
   }

   void LcAnaVeto::SetNarrowVetoWindow( const Float_t minTime, const Float_t maxTime )
   {
      m_narrowWindowMinTime = minTime;
      m_narrowWindowMaxTime = maxTime;
   }
      
   void LcAnaVeto::SetWideVetoWindow( const Float_t minTime, const Float_t maxTime )
   {
      m_wideWindowMinTime = minTime;
      m_wideWindowMaxTime = maxTime;
   }

   void LcAnaVeto::UpdateInputVars( const LcRecData *data )
   {
      IndexMap_t indexMap;
      std::vector<Float_t> eneVec;
      std::vector<Float_t> timeVec;
      std::vector<Float_t> fttVec;

      /// Load data ///
      SetModuleData( data, indexMap, eneVec, timeVec );
      SetFTTData( data, indexMap, fttVec );

      /// set variables ///
      m_indexMap.swap( indexMap );
      m_eneVec.swap( eneVec );
      m_timeVec.swap( timeVec );
      m_fttVec.swap( fttVec );
   }

   void LcAnaVeto::UpdateDeadModule()
   {
      for(std::vector<Int_t>::const_iterator dead_it = m_deadIdVec.begin(); 
                                             dead_it!= m_deadIdVec.end(); ++dead_it )
      {
         IndexMap_t::const_iterator mod_it = m_indexMap.find( *dead_it );
         if( mod_it!=m_indexMap.end() ){
            const Int_t index = mod_it->second;
            m_eneVec[index] = -1.;
         }
      }
   }

   void LcAnaVeto::UpdateProperVars()
   {
      /// set single-hit ///
      const Int_t properIndex = FindProperIndex( m_nominalTime, 
                                                 m_energyThreshold );
      m_properModId    = (properIndex < 0 ) ? m_defaultModId
                                            : GetModId(properIndex);
      m_properTime     = (properIndex < 0 ) ? m_defaultDeltaTime 
                                            : GetDeltaTime(properIndex);
      m_properEnergy   = (properIndex < 0 ) ? m_defaultEnergy 
                                            : GetEnergy(properIndex);
      m_properFTTChisq = (properIndex < 0 ) ? m_defaultFTTChisq
                                            : GetFTTChisq(properIndex);

      /// set double pulse ///
      const Int_t maskingProperIndex = FindProperIndex( m_nominalTime, 
                                                        m_energyThreshold, 
                                                        m_fttChisqThreshold );
      m_maskingProperModId    = (maskingProperIndex < 0 ) ? m_defaultModId 
                                                          : GetModId(maskingProperIndex);
      m_maskingProperTime     = (maskingProperIndex < 0 ) ? m_defaultDeltaTime
                                                          : GetDeltaTime(maskingProperIndex);
      m_maskingProperEnergy   = (maskingProperIndex < 0 ) ? m_defaultEnergy
                                                          : GetEnergy(maskingProperIndex);
      m_maskingProperFTTChisq = (maskingProperIndex < 0 ) ? m_defaultFTTChisq
                                                          : GetFTTChisq(maskingProperIndex);

   }

   void LcAnaVeto::UpdateVetoCondition()
   {
      m_isVeto = false;

      /// narrow window veto ///
      if( m_properTime>m_narrowWindowMinTime && m_properTime<m_narrowWindowMaxTime )
         m_isVeto = true;

      /// wide window veto if FTT > threshold ///
      if( m_maskingProperTime>m_wideWindowMinTime && m_maskingProperTime<m_wideWindowMaxTime )
         m_isVeto = true;
   }


}
