#include "LcAna/LcAnaVetoCV.h"

namespace LcLib
{
   /// constructor
   LcAnaVetoCV::LcAnaVetoCV()
   {
      SetDefaultParameter();
      m_detname = "CV";
   }

   /// destructor
   LcAnaVetoCV::~LcAnaVetoCV()
   {
      ;
   }

   /// 
   void LcAnaVetoCV::SetDefaultParameter()
   {
      m_nominalTime = 54.1;
      m_energyThreshold = .2;
      m_fttChisqThreshold = 20.;
      SetNarrowVetoWindow(44.1,64.1);
      SetWideVetoWindow(14.1,94.1);
   }

   bool LcAnaVetoCV::SetBranchAddresses( TTree *tr )
   {
      if( tr==NULL ){
         Error("SetBranchAddresses","null pointer is given.");
         return false;
      }

      tr->SetBranchAddress("CVProperModID",&m_properModId);
      tr->SetBranchAddress("CVProperTime",&m_properTime);
      tr->SetBranchAddress("CVProperEne",&m_properEnergy);
      tr->SetBranchAddress("CVProperFTTChisq",&m_properFTTChisq);

      tr->SetBranchAddress("CVMaskingProperIndex",&m_maskingProperModId);
      tr->SetBranchAddress("CVMaskingProperTime",&m_maskingProperTime);
      tr->SetBranchAddress("CVMaskingProperEne",&m_maskingProperEnergy);
      tr->SetBranchAddress("CVMaskingProperFTTChisq",&m_maskingProperFTTChisq);

      //tr->SetBranchAddress("IsCVVeto",&m_isVeto);
      return true;
   }

   bool LcAnaVetoCV::AddBranches( TTree *tr )
   {
      if( tr==NULL ){
         Error("AddBranches","null pointer is given.");
         return false;
      }

      tr->Branch("CVProperModID",&m_properModId,"CVProperModID/I");
      tr->Branch("CVProperTime",&m_properTime,"CVProperTime/F");
      tr->Branch("CVProperEne",&m_properEnergy,"CVProperEne/F");
      tr->Branch("CVProperFTTChisq",&m_properFTTChisq,"CVProperFTTChisq/F");

      tr->Branch("CVMaskingProperModID",&m_maskingProperModId,"CVMaskingProperModID/I");
      tr->Branch("CVMaskingProperTime",&m_maskingProperTime,"CVMaskingProperTime/F");
      tr->Branch("CVMaskingProperEne",&m_maskingProperEnergy,"CVMaskingProperEne/F");
      tr->Branch("CVMaskingProperFTTChisq",&m_maskingProperFTTChisq,"CVMaskingProperFTTChisq/F");

      //tr->Branch("IsCVVeto",&m_isVeto,"IsCVVeto/O");
      return true;
   }

   /// private ///
   void LcAnaVetoCV::SetModuleData( const LcRecData *data,
                                    IndexMap_t &indexMap,
                                    std::vector<Float_t> &eneVec,
                                    std::vector<Float_t> &timeVec )
   {
      for( Int_t imod=0; imod<data->CVModuleNumber; ++imod )
      {
         indexMap.insert( std::make_pair(data->CVModuleModID[imod], imod) );
         const Float_t deltaTime = data->CVModuleDeltaTime[imod];
         eneVec.push_back( data->CVModuleEne[imod] );
         timeVec.push_back( deltaTime );
      }
   }

   void LcAnaVetoCV::SetFTTData( const LcRecData *data,
                                 IndexMap_t &indexMap,
                                 std::vector<Float_t> &fttVec )
   {
      InitializeFTTVecByIndexMap(indexMap, fttVec);
      const Int_t vecSize = fttVec.size();

      for( Int_t imod=0; imod<data->CVFTTNumber; ++imod )
      {
         const Int_t index = GetIndexFromMap(indexMap, data->CVFTTModID[imod]); 
         if( index<0 || index>=vecSize ) continue; 
         fttVec[index] = data->CVFTTChisq[imod];
      }
   }

}
