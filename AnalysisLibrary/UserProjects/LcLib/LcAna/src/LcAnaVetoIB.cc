#include "LcAna/LcAnaVetoIB.h"

#include "TMath.h"

#include "MTAnalysisLibrary/MTBasicParameters.h"

namespace LcLib
{
   /// constructor
   LcAnaVetoIB::LcAnaVetoIB()
   {
      SetDefaultParameter();
      m_detname = "IB";
   }

   /// destructor
   LcAnaVetoIB::~LcAnaVetoIB()
   {
      ;
   }

   /// 
   void LcAnaVetoIB::SetDefaultParameter()
   {
      m_nominalTime = -40.5;
      m_energyThreshold = 1.;
      m_fttChisqThreshold = 20.;
      SetNarrowVetoWindow(-48.5,-28.5);
      SetWideVetoWindow(-70.5,-10.5);
   }

   bool LcAnaVetoIB::SetBranchAddresses( TTree *tr )
   {
      if( tr==NULL ){
         Error("SetBranchAddresses","null pointer is given.");
         return false;
      }

      tr->SetBranchAddress("IBProperModID",&m_properModId);
      tr->SetBranchAddress("IBProperTime",&m_properTime);
      tr->SetBranchAddress("IBProperEne",&m_properEnergy);
      tr->SetBranchAddress("IBProperFTTChisq",&m_properFTTChisq);

      tr->SetBranchAddress("IBMaskingProperIndex",&m_maskingProperModId);
      tr->SetBranchAddress("IBMaskingProperTime",&m_maskingProperTime);
      tr->SetBranchAddress("IBMaskingProperEne",&m_maskingProperEnergy);
      tr->SetBranchAddress("IBMaskingProperFTTChisq",&m_maskingProperFTTChisq);

      //tr->SetBranchAddress("IsIBVeto",&m_isVeto);
      return true;
   }

   bool LcAnaVetoIB::AddBranches( TTree *tr )
   {
      if( tr==NULL ){
         Error("AddBranches","null pointer is given.");
         return false;
      }

      tr->Branch("IBProperModID",&m_properModId,"IBProperModID/I");
      tr->Branch("IBProperTime",&m_properTime,"IBProperTime/F");
      tr->Branch("IBProperEne",&m_properEnergy,"IBProperEne/F");
      tr->Branch("IBProperFTTChisq",&m_properFTTChisq,"IBProperFTTChisq/F");

      tr->Branch("IBMaskingProperModID",&m_maskingProperModId,"IBMaskingProperModID/I");
      tr->Branch("IBMaskingProperTime",&m_maskingProperTime,"IBMaskingProperTime/F");
      tr->Branch("IBMaskingProperEne",&m_maskingProperEnergy,"IBMaskingProperEne/F");
      tr->Branch("IBMaskingProperFTTChisq",&m_maskingProperFTTChisq,"IBMaskingProperFTTChisq/F");

      //tr->Branch("IsIBVeto",&m_isVeto,"IsIBVeto/O");
      return true;
   }

   Float_t LcAnaVetoIB::GetDeltaTime( const Float_t hitTime, 
                                      const Float_t hitZ,
                                      const Float_t averageClusterTime )
   {
      const Float_t k_BadTimeThreshold = -1000.;
      if(hitTime < k_BadTimeThreshold ) return m_defaultDeltaTime;

      const Double_t k_BarrelRadius = MTBP::IBInnerInnerR;
      const Double_t k_BarrelCenter = MTBP::IBZPosition + MTBP::IBLength / 2.;
      const Double_t k_C = 300.; //[mm/ns]
 
      const Double_t deltaZ = MTBP::CSIZPosition - (hitZ + k_BarrelCenter);
      const Double_t tBack = TMath::Sqrt( k_BarrelRadius*k_BarrelRadius + deltaZ*deltaZ) / k_C;
      return ( hitTime - averageClusterTime + tBack );
   }

   /// private ///
   void LcAnaVetoIB::SetModuleData( const LcRecData *data,
                                    IndexMap_t &indexMap,
                                    std::vector<Float_t> &eneVec,
                                    std::vector<Float_t> &timeVec )
   {
      for( Int_t imod=0; imod<data->IBModuleNumber; ++imod )
      {
         indexMap.insert( std::make_pair(data->IBModuleModID[imod], imod) );
         const Float_t deltaTime = GetDeltaTime( data->IBModuleHitTime[imod],
                                                 data->IBModuleHitZ[imod], 
                                                 data->AverageClusterTime);
         eneVec.push_back( data->IBModuleEne[imod] );
         timeVec.push_back( deltaTime );
      }
   }

   void LcAnaVetoIB::SetFTTData( const LcRecData *data,
                                 IndexMap_t &indexMap,
                                 std::vector<Float_t> &fttVec )
   {
      InitializeFTTVecByIndexMap(indexMap, fttVec);
      const Int_t vecSize = fttVec.size();

      for( Int_t ich=0; ich<data->IBFTTNumber; ++ich )
      {
         const Int_t channelId = data->IBFTTModID[ich];
         const Int_t modId     = ( channelId < 100 ) ? channelId : (channelId - 100 );
         const Int_t index = GetIndexFromMap(indexMap, modId);
         if( index<0 || index>=vecSize ) continue;
         if( data->IBFTTChisq[ich] > fttVec[index] ) fttVec[index] = data->IBFTTChisq[ich];
      }
   }

}
