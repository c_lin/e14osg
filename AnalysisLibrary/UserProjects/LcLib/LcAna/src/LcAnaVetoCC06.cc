#include "LcAna/LcAnaVetoCC06.h"

namespace LcLib
{
   /// constructor
   LcAnaVetoCC06::LcAnaVetoCC06()
   {
      SetDefaultParameter();
      m_detname = "CC06";
   }

   /// destructor
   LcAnaVetoCC06::~LcAnaVetoCC06()
   {
      ;
   }

   /// 
   void LcAnaVetoCC06::SetDefaultParameter()
   {
      m_nominalTime = -8.5;
      m_energyThreshold = 3.;
      m_fttChisqThreshold = 20.;
      SetNarrowVetoWindow(-18.5,1.5);
      SetWideVetoWindow(-23.5,5.6);
   }

   bool LcAnaVetoCC06::SetBranchAddresses( TTree *tr )
   {
      if( tr==NULL ){
         Error("SetBranchAddresses","null pointer is given.");
         return false;
      }

      tr->SetBranchAddress("CC06ProperModID",&m_properModId);
      tr->SetBranchAddress("CC06ProperTime",&m_properTime);
      tr->SetBranchAddress("CC06ProperEne",&m_properEnergy);
      tr->SetBranchAddress("CC06ProperFTTChisq",&m_properFTTChisq);

      tr->SetBranchAddress("CC06MaskingProperIndex",&m_maskingProperModId);
      tr->SetBranchAddress("CC06MaskingProperTime",&m_maskingProperTime);
      tr->SetBranchAddress("CC06MaskingProperEne",&m_maskingProperEnergy);
      tr->SetBranchAddress("CC06MaskingProperFTTChisq",&m_maskingProperFTTChisq);

      //tr->SetBranchAddress("IsCC06Veto",&m_isVeto);
      return true;
   }

   bool LcAnaVetoCC06::AddBranches( TTree *tr )
   {
      if( tr==NULL ){
         Error("AddBranches","null pointer is given.");
         return false;
      }

      tr->Branch("CC06ProperModID",&m_properModId,"CC06ProperModID/I");
      tr->Branch("CC06ProperTime",&m_properTime,"CC06ProperTime/F");
      tr->Branch("CC06ProperEne",&m_properEnergy,"CC06ProperEne/F");
      tr->Branch("CC06ProperFTTChisq",&m_properFTTChisq,"CC06ProperFTTChisq/F");

      tr->Branch("CC06MaskingProperModID",&m_maskingProperModId,"CC06MaskingProperModID/I");
      tr->Branch("CC06MaskingProperTime",&m_maskingProperTime,"CC06MaskingProperTime/F");
      tr->Branch("CC06MaskingProperEne",&m_maskingProperEnergy,"CC06MaskingProperEne/F");
      tr->Branch("CC06MaskingProperFTTChisq",&m_maskingProperFTTChisq,"CC06MaskingProperFTTChisq/F");

      //tr->Branch("IsCC06Veto",&m_isVeto,"IsCC06Veto/O");
      return true;
   }

   Float_t LcAnaVetoCC06::GetDeltaTime( const Float_t hitTime, const Float_t averageClusterTime )
   {
      const Float_t k_BadTimeThreshold = -1000.;
      return (hitTime < k_BadTimeThreshold) ? m_defaultDeltaTime 
                                            : (hitTime - averageClusterTime); 
   }

   /// private ///
   void LcAnaVetoCC06::SetModuleData( const LcRecData *data,
                                      IndexMap_t &indexMap,
                                      std::vector<Float_t> &eneVec,
                                      std::vector<Float_t> &timeVec )
   {
      for( Int_t imod=0; imod<data->CC06E391ModuleNumber; ++imod )
      {
         indexMap.insert( std::make_pair(data->CC06E391ModuleModID[imod], imod) );
         const Float_t deltaTime = GetDeltaTime( data->CC06E391ModuleTime[imod], 
                                                 data->AverageClusterTime);
         eneVec.push_back( data->CC06E391ModuleEne[imod] );
         timeVec.push_back( deltaTime );
      }
   }

   void LcAnaVetoCC06::SetFTTData( const LcRecData *data,
                                   IndexMap_t &indexMap,
                                   std::vector<Float_t> &fttVec )
   {
      InitializeFTTVecByIndexMap(indexMap, fttVec);
      const Int_t vecSize = fttVec.size();

      for( Int_t imod=0; imod<data->CC06FTTNumber; ++imod )
      {
         const Int_t index = GetIndexFromMap(indexMap, data->CC06FTTModID[imod]); 
         if( index<0 || index>=vecSize ) continue; 
         fttVec[index] = data->CC06FTTChisq[imod];
      }
   }

}
