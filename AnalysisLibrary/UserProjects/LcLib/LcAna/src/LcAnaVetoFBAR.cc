#include "LcAna/LcAnaVetoFBAR.h"

namespace LcLib
{
   /// constructor
   LcAnaVetoFBAR::LcAnaVetoFBAR()
   {
      SetDefaultParameter();
      m_detname = "FBAR";
   }

   /// destructor
   LcAnaVetoFBAR::~LcAnaVetoFBAR()
   {
      ;
   }

   /// 
   void LcAnaVetoFBAR::SetDefaultParameter()
   {
      m_nominalTime = 38.5;
      m_energyThreshold = 1.;
      m_fttChisqThreshold = 33.;
      SetNarrowVetoWindow(15.1,66.);
      SetWideVetoWindow(-40.4,110.4);
   }

   bool LcAnaVetoFBAR::SetBranchAddresses( TTree *tr )
   {
      if( tr==NULL ){
         Error("SetBranchAddresses","null pointer is given.");
         return false;
      }

      tr->SetBranchAddress("FBARProperModID",&m_properModId);
      tr->SetBranchAddress("FBARProperTime",&m_properTime);
      tr->SetBranchAddress("FBARProperEne",&m_properEnergy);
      tr->SetBranchAddress("FBARProperFTTChisq",&m_properFTTChisq);

      tr->SetBranchAddress("FBARMaskingProperIndex",&m_maskingProperModId);
      tr->SetBranchAddress("FBARMaskingProperTime",&m_maskingProperTime);
      tr->SetBranchAddress("FBARMaskingProperEne",&m_maskingProperEnergy);
      tr->SetBranchAddress("FBARMaskingProperFTTChisq",&m_maskingProperFTTChisq);

      //tr->SetBranchAddress("IsFBARVeto",&m_isVeto);
      return true;
   }

   bool LcAnaVetoFBAR::AddBranches( TTree *tr )
   {
      if( tr==NULL ){
         Error("AddBranches","null pointer is given.");
         return false;
      }

      tr->Branch("FBARProperModID",&m_properModId,"FBARProperModID/I");
      tr->Branch("FBARProperTime",&m_properTime,"FBARProperTime/F");
      tr->Branch("FBARProperEne",&m_properEnergy,"FBARProperEne/F");
      tr->Branch("FBARProperFTTChisq",&m_properFTTChisq,"FBARProperFTTChisq/F");

      tr->Branch("FBARMaskingProperModID",&m_maskingProperModId,"FBARMaskingProperModID/I");
      tr->Branch("FBARMaskingProperTime",&m_maskingProperTime,"FBARMaskingProperTime/F");
      tr->Branch("FBARMaskingProperEne",&m_maskingProperEnergy,"FBARMaskingProperEne/F");
      tr->Branch("FBARMaskingProperFTTChisq",&m_maskingProperFTTChisq,"FBARMaskingProperFTTChisq/F");

      //tr->Branch("IsFBARVeto",&m_isVeto,"IsFBARVeto/O");
      return true;
   }

   Float_t LcAnaVetoFBAR::GetDeltaTime( const Float_t hitTime, const Float_t averageClusterTime )
   {
      const Float_t k_BadTimeThreshold = -1000.;
      return (hitTime < k_BadTimeThreshold) ? m_defaultDeltaTime 
                                            : (hitTime - averageClusterTime); 
   }

   /// private ///
   void LcAnaVetoFBAR::SetModuleData( const LcRecData *data,
                                      IndexMap_t &indexMap,
                                      std::vector<Float_t> &eneVec,
                                      std::vector<Float_t> &timeVec )
   {
      for( Int_t imod=0; imod<data->FBARModuleNumber; ++imod )
      {
         indexMap.insert( std::make_pair(data->FBARModuleModID[imod], imod) );
         const Float_t deltaTime = GetDeltaTime( data->FBARModuleTime[imod], 
                                                 data->AverageClusterTime);
         eneVec.push_back( data->FBARModuleEne[imod] );
         timeVec.push_back( deltaTime );
      }
   }

   void LcAnaVetoFBAR::SetFTTData( const LcRecData *data,
                                   IndexMap_t &indexMap,
                                   std::vector<Float_t> &fttVec )
   {
      InitializeFTTVecByIndexMap(indexMap, fttVec);
      const Int_t vecSize = fttVec.size();

      for( Int_t imod=0; imod<data->FBARFTTNumber; ++imod )
      {
         const Int_t index = GetIndexFromMap(indexMap, data->FBARFTTModID[imod]); 
         if( index<0 || index>=vecSize ) continue; 
         fttVec[index] = data->FBARFTTChisq[imod];
      }
   }

}
