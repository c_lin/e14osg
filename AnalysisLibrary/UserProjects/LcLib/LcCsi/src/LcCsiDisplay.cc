#include "LcCsi/LcCsiDisplay.h"

#include <limits>

#include "TStyle.h"
#include "TMath.h"

namespace LcLib
{
   /// constructor
   LcCsiDisplay::LcCsiDisplay()
      : m_title("")
   {
      m_csiMap = new LcCsiMap;
      m_nBox = m_csiMap->GetNchannel();
      m_box = new TBox [m_nBox];
      for( Int_t ibox=0; ibox<m_nBox; ++ibox ) m_box[ibox].SetFillStyle(0);

      m_canv = new TCanvas("canv","",750,700);
      m_canv->SetRightMargin(0.15);
      m_histSmall = new TH2D("hs","",80,-1000,1000,80,-1000,1000);
      m_histLarge = new TH2D("hl","",40,-1000,1000,40,-1000,1000);
      m_histSmall->SetStats(0);
      m_histLarge->SetStats(0);

      gStyle->SetPalette(1);

      m_userZRangeMin = 0.;
      m_userZRangeMax = 0.;

      Reset();
   }

   /// destructor
   LcCsiDisplay::~LcCsiDisplay()
   {
      delete [] m_box;
      delete m_canv;
      delete m_histSmall;
      delete m_histLarge;
      delete m_csiMap;
   }

   ///
   void LcCsiDisplay::Reset()
   {
      m_isUserSetZRange = false;
      m_histSmall->Reset();
      m_histLarge->Reset();
      std::vector<Double_t> valvec(m_csiMap->GetNchannel(),0.);
      m_valvec.swap(valvec);
   }

   ///
   void LcCsiDisplay::Fill( const Int_t modId, const Double_t weight )
   {
      if( modId<0 || modId>=m_nBox ){
         Warning("Fill","Input ID %d is invalid",modId);
      }

      m_valvec[modId] += weight;

      Double_t x = m_csiMap->GetX(modId);
      Double_t y = m_csiMap->GetY(modId);
      Double_t w = m_csiMap->GetW(modId);

      if( w < 30. ) m_histSmall->Fill(x,y,weight);
      else          m_histLarge->Fill(x,y,weight);   
   }

   void LcCsiDisplay::DrawDisplay()
   {
      if( !m_isUserSetZRange )
         SetZRange();

      m_histSmall->SetTitle( m_title.c_str() );
      m_histSmall->Draw("colz");
      m_histLarge->Draw("colz same");
      DrawCrystal();
      m_canv->Update();
   }

   void LcCsiDisplay::SetLogz( const bool isLogz )
   {
      if( isLogz && GetMinimum() < 0. ) return;
      m_canv->SetLogz(isLogz);
   }

   void LcCsiDisplay::SetUserZRange( const Double_t user_min, const Double_t user_max )
   {
      m_isUserSetZRange = true;
      m_histSmall->GetZaxis()->SetRangeUser(user_min , user_max);
      m_histLarge->GetZaxis()->SetRangeUser(user_min , user_max);
   }

   Double_t LcCsiDisplay::GetContent( const Int_t modId ) const
   {
      if( modId<0 || modId>=m_nBox ){
         Warning("GetContent","Input ID %d is invalid",modId);
      }
      return m_valvec[modId];
   }

   Int_t LcCsiDisplay::GetMinimumId() const
   {
      Double_t min_val = std::numeric_limits<Double_t>::max(); 
      Double_t min_id = -1;
      for( Int_t id=0; id<m_csiMap->GetNchannel(); ++id ){
         if( m_valvec[id]<=min_val ){
            min_val = m_valvec[id];
            min_id = id;
         }
      }
      return min_id;
   }

   Int_t LcCsiDisplay::GetMaximumId() const
   {
      Double_t max_val = std::numeric_limits<Double_t>::min();
      Double_t max_id = -1;
      for( Int_t id=0; id<m_csiMap->GetNchannel(); ++id ){
         if( m_valvec[id]>=max_val ){
            max_val = m_valvec[id];
            max_id = id;
         }
      }
      return max_id;
   }

   Double_t LcCsiDisplay::GetMaximum() const
   {
      Int_t max_id = GetMaximumId();
      return (max_id < 0) ? std::numeric_limits<Double_t>::max() : GetContent(max_id);
   }

   Double_t LcCsiDisplay::GetMinimum() const
   {
      Int_t min_id = GetMinimumId();
      return (min_id < 0) ? std::numeric_limits<Double_t>::min() : GetContent(min_id);
   }

   void LcCsiDisplay::DrawCrystal()
   {
      for(int id=0;id<m_nBox;id++)
      {
         Double_t x = m_csiMap->GetX(id);
         Double_t y = m_csiMap->GetY(id);
         Double_t w = m_csiMap->GetW(id);

         Double_t box_x = (TMath::Floor(x/w)+0.5)*w;
         Double_t box_y = (TMath::Floor(y/w)+0.5)*w;
         m_box[id].DrawBox(box_x-w/2,box_y-w/2,box_x+w/2,box_y+w/2);
      }
      m_canv->Update();
   }

   void LcCsiDisplay::SetZRange()
   {
      Double_t min = GetMinimum();
      Double_t max = GetMaximum();
      
      m_histSmall->GetZaxis()->SetRangeUser(min * 1.1 , max * 1.1 );
      m_histLarge->GetZaxis()->SetRangeUser(min * 1.1 , max * 1.1 );
   }

}
