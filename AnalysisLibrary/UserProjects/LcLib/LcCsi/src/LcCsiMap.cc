#include "LcCsi/LcCsiMap.h"

#include <string>
#include <cstdlib>
#include <fstream>

#include "TString.h"

namespace LcLib
{
   /// constructor ///
   LcCsiMap::LcCsiMap()
   {
      LoadMetric();
   }

   /// destructor ///
   LcCsiMap::~LcCsiMap()
   {
      ;
   }

   ///
   Double_t LcCsiMap::GetX( const Int_t modId ) const
   {
      const Int_t size = m_xvec.size();
      if( modId<0 || modId>=size ) return -9999.;
      return m_xvec[modId];
   }

   Double_t LcCsiMap::GetY( const Int_t modId ) const
   {
      const Int_t size = m_yvec.size();
      if( modId<0 || modId>=size ) return -9999.;
      return m_yvec[modId];
   }

   Double_t LcCsiMap::GetW( const Int_t modId ) const
   {
      const Int_t size = m_wvec.size();
      if( modId<0 || modId>=size ) return -1.;
      return m_wvec[modId];
   }

   /// method ///
   void LcCsiMap::LoadMetric()
   {
      const std::string fname = Form("%s/AnalysisLibrary/UserProjects/LcLib/LcCsi/data/"
                                     "csi_metric.txt", std::getenv("MY_TOP_DIR") );
      std::ifstream ifile( fname.c_str() );
      Int_t r_id;
      Double_t r_x, r_y, r_w;
      while( ifile >> r_id >> r_x >> r_y >> r_w )
      {
         m_xvec.push_back( r_x );
         m_yvec.push_back( r_y );
         m_wvec.push_back( r_w );
      }

   }


}
