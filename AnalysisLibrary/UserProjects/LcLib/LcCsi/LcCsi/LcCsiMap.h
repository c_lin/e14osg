#ifndef LCLIB_LCCSI_LCCSIMAP_H
#define LCLIB_LCCSI_LCCSIMAP_H

#include "TObject.h"

#include <vector>

namespace LcLib
{

   class LcCsiMap : public TObject
   {
    public:
      /// constructor
      LcCsiMap();

      /// destructor
      ~LcCsiMap();

      ///
      Double_t GetX( const Int_t modId ) const; 
      Double_t GetY( const Int_t modId ) const;
      Double_t GetW( const Int_t modId ) const;

      ///
      Int_t GetNchannel() const { return s_nCsi; }

    private:
      static const Int_t s_nCsi = 2716;
      std::vector<Double_t> m_xvec;
      std::vector<Double_t> m_yvec;
      std::vector<Double_t> m_wvec;

      void LoadMetric();

      ClassDef(LcCsiMap,1);
   };

}

#endif /* LcCsiMap.h guard */
