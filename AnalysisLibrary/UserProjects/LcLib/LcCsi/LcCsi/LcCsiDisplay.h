#ifndef LCIB_LCCSI_LCCSIDISPLAY_H
#define LCIB_LCCSI_LCCSIDISPLAY_H

#include <string>
#include <vector>

#include "TObject.h"
#include "TCanvas.h"
#include "TBox.h"
#include "TH2D.h"

#include "LcCsi/LcCsiMap.h"

namespace LcLib
{
   class LcCsiDisplay : public TObject
   {
    public:
      /// constructor
      LcCsiDisplay();

      /// destructor
      ~LcCsiDisplay();

      /// method
      void Reset();
      void Fill( const Int_t modId, const Double_t weight = 1. );
      void DrawDisplay();

      /// setter 
      void SetTitle( const std::string title ){ m_title = title; }
      void SetLogz( const bool isLogz = true );
      void SetUserZRange( const Double_t user_min, const Double_t user_max );

      /// getter
      Double_t GetContent( const Int_t modId ) const;
      Int_t GetMinimumId() const;
      Int_t GetMaximumId() const;

      Double_t GetMaximum() const;
      Double_t GetMinimum() const;

    private:
       std::string  m_title;
       Int_t        m_nBox;
       TCanvas     *m_canv;
       TBox        *m_box;
       TH2D        *m_histSmall;
       TH2D        *m_histLarge;

       LcCsiMap    *m_csiMap;

       std::vector<Double_t> m_valvec;

       void         LoadCsiMetric();
       void         DrawCrystal();
       void         SetZRange();

       bool         m_isUserSetZRange;
       Double_t     m_userZRangeMin;
       Double_t     m_userZRangeMax;
 
      ClassDef(LcCsiDisplay,1);
   }; 

}

#endif /* LcCsiDisplay.h guard */
