#include "LcWfm/LcWfm.h"

#include <iomanip> 

#include "TMath.h"
#include "TH1D.h"

namespace LcLib
{
   /// constructor
   LcWfm::LcWfm()
   {
      ;
   }

   LcWfm::LcWfm( const std::vector<Double_t>::size_type nsample ) 
      : m_wfmvec(nsample)
   {
      Init();
   } 

   LcWfm::LcWfm( const std::vector<Double_t>::size_type nsample, Double_t val ) 
      : m_wfmvec(nsample,val)
   {
      Init();
   }

   LcWfm::LcWfm( const std::vector<Double_t> &wfmvec ) 
      : m_wfmvec(wfmvec)
   {
      Init();
   }

   LcWfm::LcWfm( std::vector<Double_t>::const_iterator it_beg,
                 std::vector<Double_t>::const_iterator it_end )
      : m_wfmvec(it_beg,it_end)
   {
      Init();
   }
  

   LcWfm::LcWfm( const Double_t *arr_beg, const Double_t *arr_end) 
      : m_wfmvec(arr_beg,arr_end)
   {
      Init();
   }

   LcWfm::LcWfm( const std::vector<Short_t> &wfmvec )
   {
      std::vector<Short_t>::const_iterator it = wfmvec.begin();
      while( it!=wfmvec.end() )
         m_wfmvec.push_back(*it++);
      Init();
   }

   LcWfm::LcWfm( const Int_t nsample, const Short_t *arr )
   {
      for( Int_t is=0; is<nsample; ++is )  
         m_wfmvec.push_back( arr[is] );
   }

   LcWfm::LcWfm( const Int_t nsample, const Float_t *arr )
   {
      for( Int_t is=0; is<nsample; ++is )
         m_wfmvec.push_back( arr[is] );
   }

   /// destructor
   LcWfm::~LcWfm()
   {
      ;
   }

   /// 
   void LcWfm::Init()
   {
      SetSkipFirstSampleToEvalPed();
   }

   Int_t LcWfm::GetMaxSample() const
   {
      Int_t   max_sample = GetNsample();
      Double_t peak_val = 0.;

      for(std::vector<Double_t>::const_iterator it=m_wfmvec.begin(); it!=m_wfmvec.end(); ++it )
      {
         if( *it > peak_val ){
            peak_val = *it;
            max_sample = it-m_wfmvec.begin();
         }
      }
      return max_sample;
   }

   std::vector<Int_t> LcWfm::GetLocalMaximum( const Double_t peak_threshold ) const
   {
      std::vector<Int_t> maxvec;
      if( GetNsample()<3 ) return maxvec;

      for(std::vector<Double_t>::size_type is=1; is!=m_wfmvec.size()-1; ++is )
      {
         if(    m_wfmvec[is] >= m_wfmvec[is-1] 
             && m_wfmvec[is] >= m_wfmvec[is+1] 
             && m_wfmvec[is] > peak_threshold
            )
         {
            maxvec.push_back(is);   
         }
      }
      return maxvec;
   }

   Int_t LcWfm::GetProperMaxSample( const Double_t nominalTime,
                                    const Double_t peakThreshold ) const
   {
      const std::vector<Int_t> max_vec = GetLocalMaximum( peakThreshold ); 
      if( max_vec.empty() ) return -1;

      Double_t delta_time = GetNsample();
      Int_t proper_sample = -1;

      for( std::vector<Int_t>::size_type idx=0; idx!=max_vec.size(); ++idx ){
         if( TMath::Abs( max_vec[idx] - nominalTime ) < delta_time ){
            delta_time = TMath::Abs( max_vec[idx] - nominalTime );
            proper_sample = max_vec[idx];
         }
      }

      return proper_sample;
   }

   Double_t LcWfm::GetProperParabolicTime( const Double_t nominalTime,
                                           const Double_t peakThreshold ) const
   {
      const Int_t properMaxSample = GetProperMaxSample( nominalTime, peakThreshold );
      return ( properMaxSample < 0 ) ? -1. : GetParabolicTime(properMaxSample);
   }

   Double_t LcWfm::GetParabolicTime( const Int_t maxSample ) const
   {
      if( maxSample <= 1 || maxSample>=GetNsample()-2 ) return -1.;

      const Double_t yL = GetData( maxSample - 1 );
      const Double_t yC = GetData( maxSample     );
      const Double_t yR = GetData( maxSample + 1 );

      /// protection ///
      if( yL > yC || yR > yC ) return -2.;
      if( yC - yR < 1e-4 ) return ( maxSample + 0.5);

      ///
      const Double_t delta_ratio = (yC - yL) / (yC - yR);
      const Double_t time = 0.5 - 1./( delta_ratio + 1.);
      return ( maxSample + time );
   }

   std::vector<Double_t> LcWfm::GetWfmVec( const Int_t sam_beg, const Int_t sam_end ) const
   {
      if( sam_end < sam_beg || sam_end > GetNsample() ){
         Error("GetData","sam_end < sam_beg || sam_end > GetNsample()");
      }

      std::vector<Double_t>::const_iterator it_beg = m_wfmvec.begin() + sam_beg;
      std::vector<Double_t>::const_iterator it_end = m_wfmvec.begin() + sam_end;
      const std::vector<Double_t> user_wfmvec(it_beg,it_end);
      return user_wfmvec;
   }

   LcWfm LcWfm::GetWfm(const Int_t sam_beg, const Int_t sam_end) const
   {
      LcWfm wfm( GetWfmVec(sam_beg,sam_end) );
      return wfm;
   }

   LcWfm LcWfm::GetFTWfm(const Int_t sam_beg, const Int_t sam_end) const 
   {
      if( sam_end < sam_beg || sam_end > GetNsample() ){
         Error("GetFTWfm","sam_end < sam_beg || sam_end > GetNsample()");
         const LcWfm fwfm(0);
         return fwfm;
      }

      const Int_t nsam = sam_end - sam_beg;
      TH1D *ht = new TH1D("ht","",nsam,0,sam_end);

      for( Int_t isam=sam_beg; isam<sam_end; ++isam )
      {
         ht->SetBinContent( isam-sam_beg+1, GetData(isam) ); 
      }

      TH1* hf = NULL;
      /// R2C : real to complex, MAG : magnitude, p : patient flag 
      hf = ht->FFT(hf,"R2C MAG P"); 
      std::vector<Double_t> fwfm_vec;
      for( Int_t isam=0; isam<nsam/2; ++isam )
      {
         fwfm_vec.push_back(hf->GetBinContent(isam+1));
      }

      const LcWfm fwfm(fwfm_vec);

      delete ht;
      delete hf;

      return fwfm;
   }

   LcWfm LcWfm::GetMovingAveragedWfm( const Int_t nSampleMovingAverage )
   {
      const Int_t nMovingAveragedWfmSample = GetNsample() - nSampleMovingAverage + 1;
      if( nMovingAveragedWfmSample<=0 ) return LcWfm();
      LcWfm wfm(nMovingAveragedWfmSample);
      for( Int_t is=0; is<nMovingAveragedWfmSample; ++is )
      {
         Double_t val = 0.;
         for( Int_t js=0; js<nSampleMovingAverage; ++js  )
            val += GetData(is+js)/nSampleMovingAverage;
         wfm[is] = val;
      }
      return wfm;
   }

   void LcWfm::SetData( const Int_t isample, const Double_t val )
   {
      if( isample>0 && isample<GetNsample() ) m_wfmvec[isample] = val;
   }


   Double_t LcWfm::EvalPedestal( const Int_t nEvalSamples ) const
   {
      /// protection
      if( nEvalSamples > GetNsample() || nEvalSamples==0 ){
         Error("EvalPedestal","nEvalSamples > m_wfmvec.size() or 0 is not allowed.");
         return 0.;
      }else if( nEvalSamples==1 && m_IsSkipFirstSampleToEvalPed ){
         Error("EvalPedestal","nEvalSamples==1 && m_IsSkipFirstSampleToEvalPed==true.");
         return 0.;
      }

      const Int_t offset = (m_IsSkipFirstSampleToEvalPed) ? 1 : 0;

      const Double_t left_mean  = TMath::Mean(  nEvalSamples-offset,
                                               &m_wfmvec[offset]                       );
      const Double_t right_mean = TMath::Mean(  nEvalSamples-offset,
                                               &m_wfmvec[m_wfmvec.size()-nEvalSamples] );

      const Double_t left_rms   = TMath::RMS (  nEvalSamples-offset,
                                               &m_wfmvec[offset]                       );
      const Double_t right_rms  = TMath::RMS (  nEvalSamples-offset,
                                               &m_wfmvec[m_wfmvec.size()-nEvalSamples] );

      ///
      return (left_rms > right_rms) ? right_mean : left_mean;
   }

   Double_t LcWfm::Integral( const Int_t sam_beg, const Int_t sam_end ) const
   {
      if( sam_beg > sam_end || sam_end>GetNsample() ){
         Error("Integral","sam_beg > sam_end or sam_end > Nsample.");
         return 0;
      } 

      Double_t sum = 0.;
      std::vector<Double_t>::const_iterator it     = m_wfmvec.begin()+sam_beg;
      std::vector<Double_t>::const_iterator it_end = m_wfmvec.begin()+sam_end;
      while( it!=it_end ) sum += *it++;  
      return sum;
   }

   void LcWfm::Square()
   {
      for( std::vector<Double_t>::iterator it = m_wfmvec.begin(); it!=m_wfmvec.end(); ++it )
         (*it) *= (*it);
   }

   void LcWfm::Sqrt()
   {
      for( std::vector<Double_t>::iterator it = m_wfmvec.begin(); it!=m_wfmvec.end(); ++it )
         (*it) = TMath::Sqrt( *it );
   }

   TGraph* LcWfm::GetWfmGraph()
   {
      std::vector<Double_t> xvec;
      for( Int_t ix=0; ix<GetNsample(); ++ix ) xvec.push_back(ix);
      TGraph *gr = new TGraph(GetNsample(),&xvec[0],&m_wfmvec[0]);
      return gr;
   }

   /// arithmetic
   void LcWfm::Add( const LcWfm& wfm )
   {
      if( wfm.m_wfmvec.size()!=m_wfmvec.size() ){
         Error("Add","#samples is different!");
         return;
      }
      for( std::vector<Double_t>::size_type is=0; is!=wfm.m_wfmvec.size(); ++is )
         m_wfmvec[is] += wfm.m_wfmvec[is];
   }

   void LcWfm::Add( const Double_t val )
   {
      for( std::vector<Double_t>::size_type is=0; is!=m_wfmvec.size(); ++is )
         m_wfmvec[is] += val;
   }

   void LcWfm::Minus( const LcWfm& wfm )
   {
      if( wfm.m_wfmvec.size()!=m_wfmvec.size() ){
         Error("Minus","#samples is different!");
         return;
      }
      for( std::vector<Double_t>::size_type is=0; is!=wfm.m_wfmvec.size(); ++is )
         m_wfmvec[is] -= wfm.m_wfmvec[is];
   }

   void LcWfm::Minus( const Double_t val )
   {
      for( std::vector<Double_t>::size_type is=0; is!=m_wfmvec.size(); ++is )
         m_wfmvec[is] -= val;
   }

   void LcWfm::Scale( const Double_t val )
   {
      for( std::vector<Double_t>::size_type is=0; is!=m_wfmvec.size(); ++is )
         m_wfmvec[is] *= val;
   }

   /// operator overloading
   LcWfm& LcWfm::operator+= (const LcWfm& wfm)
   {
      this->Add(wfm);
      return (*this);
   }

   LcWfm& LcWfm::operator+= (const Double_t val)
   {
      this->Add(val);
      return (*this);
   }

   LcWfm& LcWfm::operator-= (const LcWfm& wfm)
   {
      this->Minus(wfm);
      return (*this);
   }

   LcWfm& LcWfm::operator-= (const Double_t val)
   {
      this->Minus(val);
      return (*this);
   }

   LcWfm& LcWfm::operator*= (const Double_t val)
   {
      this->Scale(val);
      return (*this);
   }

   Double_t &LcWfm::operator[] (const size_t isample)
   {
      return m_wfmvec[isample];
   }
   
   const Double_t &LcWfm::operator[] (const size_t isample) const
   {
      return m_wfmvec[isample];
   }

   /// friend
   std::ostream& operator << ( std::ostream& os, const LcWfm& wfm )
   {
      const std::vector<Double_t>::size_type nsam_line = 16;
      os << std::setprecision(2) << std::fixed;
      os << "LcWfm::m_wfmvec : " << std::endl;

      for( std::vector<Double_t>::size_type is=0; is!=wfm.m_wfmvec.size(); ++is )
      {
         os << wfm.m_wfmvec[is] << "\t" ;
         if( (is % nsam_line)==nsam_line-1 ) os << std::endl;
      }

      return os;
   }

}
