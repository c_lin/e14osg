#include "LcWfm/LcWfmProfile.h"

namespace LcLib
{

   LcWfmProfile::LcWfmProfile( const Int_t nsample, const bool isEvalStdev )
      : m_nsample(nsample),
        m_isEvalStdev(isEvalStdev),
        m_nwfm(0),
        m_wfmProfile(nsample,0.),
        m_wfmProfileStdev(nsample,0.)
   {
      ;   
   }

   LcWfmProfile::~LcWfmProfile()
   {
      ;
   }   

   void LcWfmProfile::Reset()
   {
      m_nwfm = 0;
      LcWfm wfmProfile(m_nsample,0.);
      LcWfm wfmProfileStdev(m_nsample,0.);

      m_wfmProfile = wfmProfile;
      m_wfmProfileStdev = wfmProfileStdev;
   }

   void LcWfmProfile::Fill( const LcWfm &wfm )
   {
      m_nwfm++;
      m_wfmProfile += wfm;

      if( m_isEvalStdev ){
         LcWfm wfm_sq = wfm;
         wfm_sq.Square();
         m_wfmProfileStdev += wfm_sq;
      }
   }

   /// getter
   LcWfm LcWfmProfile::GetWfmProfile() const
   {
      LcWfm wfmProfile = m_wfmProfile;
      wfmProfile *= 1./m_nwfm;
      return wfmProfile;
   }

   LcWfm LcWfmProfile::GetWfmProfileStdev() const
   {
      if( !m_isEvalStdev ){
         Warning("GetWfmProfileStdev","isEvalStdev is disabled.");
         return m_wfmProfileStdev;
      }

      /// <x^2>      
      LcWfm wfmProfileStdev = m_wfmProfileStdev;
      wfmProfileStdev *= 1./m_nwfm;
 
      /// <x>^2
      LcWfm wfm_sq = m_wfmProfile;
      wfm_sq *= 1./m_nwfm;
      wfm_sq.Square(); 
 
      /// var = <x^2> - <x>^2
      /// stdev = sqrt(var); 
      wfmProfileStdev -= wfm_sq;
      wfmProfileStdev.Sqrt();

      return wfmProfileStdev;     
   }
}
