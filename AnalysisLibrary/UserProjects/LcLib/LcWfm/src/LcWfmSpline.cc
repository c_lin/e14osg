#include "LcWfm/LcWfmSpline.h"

#include "TMath.h"

namespace LcLib
{

   ///
   LcWfmSpline::LcWfmSpline()
   {
      ;
   }

   LcWfmSpline::LcWfmSpline( const LcWfm &wfm )
      : LcWfm(wfm)     
   {
      UpdateSpline();
   }

   LcWfmSpline::LcWfmSpline( const TSpline3 &spline )
      : LcWfm(0)
   {
      m_spline = spline;

      Double_t xval, yval; 
      Int_t iknot = 0;
      const Int_t nknot = spline.GetNp();

      while( iknot!=nknot ){
         spline.GetKnot(iknot,xval,yval);
         m_wfmvec.push_back(yval);
         iknot++;  
      }        
   }

   LcWfmSpline::~LcWfmSpline()
   {
      ;
   }

   void LcWfmSpline::UpdateSpline()
   {
      const Int_t nsample = GetNsample();
      if( nsample==0 ){
         return;
      }

      Int_t isample = 0;
      std::vector<Double_t> xvec;
      std::vector<Double_t> yvec( m_wfmvec );     
      while( isample < nsample )
         xvec.push_back(isample++);
      
      TSpline3 spline("sp",&xvec[0],&yvec[0],nsample,"blel",0.,nsample);

      m_spline = spline;
   }

   ///
   Double_t LcWfmSpline::Eval( const Double_t x_val ) const
   { 
      return (m_spline.GetNp() ) ? m_spline.Eval(x_val) : 0.; 
   }
      
   TSpline3 LcWfmSpline::GetWfmSpline() const 
   { 
      return (m_spline.GetNp() ) ? m_spline : TSpline3(); 
   }

   Double_t LcWfmSpline::GetSplineTime() const
   {
      TSpline3 spline = m_spline;
      Int_t peak_interval = GetMaxSample();
      if( spline.Derivative(peak_interval) < 0. ) peak_interval--;

      // y  = p1 x   +   p2 x^2 +   p3 x^3
      // y' = p1     + 2*p2 x   + 3*p3 x^2
      //--------------------------------------
      //       C     +    B x   +    A x^2 

      Double_t dummy_x, dummy_y, p1, p2, p3;
      spline.GetCoeff(peak_interval, dummy_x, dummy_y, p1, p2, p3 );

      Double_t  A = 3 * p3;
      Double_t  B = 2 * p2;
      Double_t  C =     p1;      
      Double_t  D = B * B - 4 * A * C;

      if( D < 0 ){
         Error("GetSplineTime","D < 0");
         return -1.;
      }

      Double_t root1 = ( -B - TMath::Sqrt(D) ) / ( 2 * A );
      Double_t root2 = ( -B + TMath::Sqrt(D) ) / ( 2 * A ); 

      Double_t peak_time = ( root1 > 0. && root1 < 1. ) ? root1 : root2;
      return (peak_time + peak_interval);

   }

}
