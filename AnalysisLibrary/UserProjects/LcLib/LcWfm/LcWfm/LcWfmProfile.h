#ifndef LCLIB_LCWFM_LCWFMPROFILE_H
#define LCLIB_LCWFM_LCWFMPROFILE_H

#include "TObject.h"

#include "LcWfm/LcWfm.h"

namespace LcLib
{

   class LcWfmProfile : public TObject
   {
    public :
      /// constructor
      LcWfmProfile( const Int_t nsample, const bool isEvalStdev = true );

      /// destructor
      virtual ~LcWfmProfile();

      ///
      void   Reset();
      void   Fill( const LcWfm &wfm );
     
      /// getter
      Int_t  GetNwaveform() const { return m_nwfm; }
      LcWfm  GetWfmProfile() const;
      LcWfm  GetWfmProfileStdev() const; 

    private :
      const Int_t  m_nsample;
      const bool   m_isEvalStdev;
      Int_t        m_nwfm;
      LcWfm        m_wfmProfile;
      LcWfm        m_wfmProfileStdev;

      ClassDef(LcWfmProfile,1);

   };

}

#endif /* LcWfmProfile.h guard */
