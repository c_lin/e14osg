#ifndef LCLIB_LCWFM_LCWFMSPLINE_H
#define LCLIB_LCWFM_LCWFMSPLINE_H

#include "TObject.h"
#include "TSpline.h"

#include "LcWfm/LcWfm.h"

namespace LcLib
{

   class LcWfmSpline : public LcWfm
   {
    public :
      /// constructor
      LcWfmSpline();
      LcWfmSpline( const LcWfm &wfm );
      LcWfmSpline( const TSpline3 &spline );

      /// destructor
      virtual ~LcWfmSpline();
      
      /// 
      void UpdateSpline();
      
      /// getter
      Double_t Eval( const Double_t x_val ) const; 
      TSpline3 GetWfmSpline() const; 
      Double_t GetSplineTime() const;

    private : 
      TSpline3 m_spline;

      ClassDef(LcWfmSpline,1);
   };

}

#endif /* LcWfmSpline.h guard */
