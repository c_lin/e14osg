#ifndef LCLIB_LCWFM_LCWFMPARAMETER_H
#define LCLIB_LCWFM_LCWFMPARAMETER_H

#include "TObject.h"

namespace LcLib
{
   namespace LcWfmParameter
   {
      const Int_t k_125MHzWfmNsample = 64;
      const Int_t k_500MHzWfmNsample = 256;

      const Int_t k_125MHzWfmLeftRange  = 12;
      const Int_t k_125MHzWfmRightRange = 24;

      const Int_t k_500MHzWfmLeftRange  = 12;
      const Int_t k_500MHzWfmRightRange = 40;

      const Int_t k_125MHzFTTNsample = 18;
      const Int_t k_500MHzFTTNsample = 26;

      const Int_t k_125MHzFwfmNormRange = 5;
      const Int_t k_500MHzFwfmNormRange = 8;

      const Double_t k_WfmLocalMaxThreshold = 0.04;

      const Int_t k_125MHzNominalSample = 29;
      const Int_t k_500MHzNominalSample = 120; 

   }
}

#endif /* LcWfmParameter.h guard */
