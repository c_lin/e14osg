#ifndef LCLIB_LCWFM_LCWFM_H
#define LCLIB_LCWFM_LCWFM_H

#include <iostream>
#include <vector>

#include "TObject.h"
#include "TGraph.h"

namespace LcLib
{

   class LcWfm : public TObject
   {

    public :
      /// constructor
      LcWfm();
      LcWfm( const std::vector<Double_t>::size_type nsample );
      LcWfm( const std::vector<Double_t>::size_type nsample, const Double_t val );
      LcWfm( const std::vector<Double_t> &wfmvec );
      LcWfm( std::vector<Double_t>::const_iterator it_beg, 
             std::vector<Double_t>::const_iterator it_end );
      LcWfm( const Double_t *arr_beg, const Double_t *arr_end );
      LcWfm( const std::vector<Short_t> &wfmvec );      
      LcWfm( const Int_t nsample, const Short_t *arr );
      LcWfm( const Int_t nsample, const Float_t *arr );

      /// destructor
      virtual ~LcWfm();

      ///
      void     Init();

      /// static constants
      static const Int_t s_DefaultNsampleToEvalPed = 10;
      static const Int_t s_nSampleMovingAverage = 5;

      /// getter 
      Int_t    GetNsample()                    const { return m_wfmvec.size();      }
      Double_t GetData( const Int_t isample )  const { return m_wfmvec.at(isample); }
      Int_t    GetMaxSample()                  const; 
      
      std::vector<Int_t>    GetLocalMaximum( const Double_t peak_threshold ) const;
      std::vector<Double_t> GetWfmVec( const Int_t sam_beg, const Int_t sam_end ) const;
      std::vector<Double_t> GetWfmVec() const { return GetWfmVec(0,GetNsample()); }

      Int_t    GetProperMaxSample( const Double_t nominalTime, 
                                   const Double_t peakThreshold ) const;
      Double_t GetProperParabolicTime( const Double_t nominalTime, 
                                       const Double_t peakThreshold ) const;
      Double_t GetParabolicTime( const Int_t maxSample ) const;

      LcWfm GetWfm( const Int_t sam_beg, const Int_t sam_end ) const;
      LcWfm GetWfm() const { return GetWfm(0,GetNsample()); }

      LcWfm GetFTWfm( const Int_t sam_beg, const Int_t sam_end ) const;
      LcWfm GetFTWfm() const { return GetFTWfm(0,GetNsample()); }

      LcWfm GetMovingAveragedWfm( const Int_t nSampleMovingAverage = s_nSampleMovingAverage );

      TGraph*  GetWfmGraph();

      /// setter
      void SetData( const Int_t isample, const Double_t val ); 
      void SetSkipFirstSampleToEvalPed( const bool IsSkipFirstSampleToEvalPed = true )
                      { m_IsSkipFirstSampleToEvalPed = IsSkipFirstSampleToEvalPed;  }

      /// method
      Double_t EvalPedestal( const Int_t nEvalSamples = s_DefaultNsampleToEvalPed ) const;
      Double_t Integral( const Int_t sam_beg, const Int_t sam_end ) const;
      Double_t Integral() const { return Integral( 0 , GetNsample() ); }
      void     Square();
      void     Sqrt();

      /// arithmetic
      void Add  ( const LcWfm&   wfm );
      void Add  ( const Double_t val );
      void Minus( const LcWfm&   wfm );
      void Minus( const Double_t val );
      void Scale( const Double_t val );

      /// operator overloading
      LcWfm& operator+= (const LcWfm&   wfm);
      LcWfm& operator+= (const Double_t val);
      LcWfm& operator-= (const LcWfm&   wfm);
      LcWfm& operator-= (const Double_t val);
      LcWfm& operator*= (const Double_t val);

      Double_t &operator[] (const size_t isample);
      const Double_t &operator[] (const size_t isample) const;

    protected :
      std::vector<Double_t> m_wfmvec;

    private :
      bool   m_IsSkipFirstSampleToEvalPed;

    friend std::ostream& operator<< ( std::ostream& os, const LcWfm& wfm);

      ClassDef(LcWfm,1);

   };
}

#endif /* LcWfm.h guard */
