#ifndef LCLIB_LCTRIGSIM_LCTRIGSIMCLUSTER_H
#define LCLIB_LCTRIGSIM_LCTRIGSIMCLUSTER_H

#include <map>
#include <vector>
#include <cstdlib>

#include "TObject.h"
#include "TH2D.h"

#include "LcDetPar/LcDetPar.h"
#include "LcWfm/LcWfm.h"

namespace LcLib
{
   class LcTrigSimCluster : public TObject
   {
    public :
      /// constructor
      LcTrigSimCluster();

      /// destructor
      ~LcTrigSimCluster();

      /// typedef / enum
      typedef std::pair<Int_t, Int_t> BitXY_t;
      enum EBlockPattern{ kConvexity=0, kConcavity, kDiagonal, kOthers};

      /// 
      void Reset();
      void Fill( const Int_t modId );
      void FillByWfm( const Int_t modId, const LcWfm &wfm, 
                      const Double_t peak_threshold, const Double_t pedestal = 0. );
      bool IsGetClusterBit( const LcWfm &wfm, const Double_t peak_threshold, 
                            const Double_t pedestal = 0.) const;
      void JointNearbyCrystal();

      ///
      void SetNominalSample( const Int_t nominalSample )
                                  { m_nominalSample = nominalSample; }
      void SetGlobalLeftRange( const Int_t globalLeftRange )
                                  { m_globalLeftRange = globalLeftRange; }
      void SetGlobalRightRange( const Int_t globalRightRange )
                                  { m_globalRightRange = globalRightRange; }
      void SetPeakSensingLeftRange( const Int_t peakSensingLeftRange )
                                  { m_peakSensingLeftRange = peakSensingLeftRange; }
      void SetPeakSensingRightRange( const Int_t peakSensingRightRange )
                                  { m_peakSensingRightRange = peakSensingRightRange; }
      void SetMaskingModId( const Int_t modId );
      void SetMaskingByUserflag( const Int_t userflag );
 
      /// getter
      Int_t   GetCSIModId( const Int_t bitx, const Int_t bity ) const;
      BitXY_t GetBitXY( const Int_t modId ) const; 
      Int_t   GetNcluster() const;

      const static Int_t s_nBinXY = 38;
      EBlockPattern GetBlockPattern( const bool quad1, const bool quad2,
                                     const bool quad3, const bool quad4 ) const;
     
      TH2D* GetClusterBitMap() const;
 
    private :
      const LcDetPar m_detpar;

      std::map<BitXY_t, Int_t> m_bitMap;
      std::vector<Int_t> m_bitX;
      std::vector<Int_t> m_bitY;

      std::vector<bool> m_isMasked;

      bool m_isBitOn[s_nBinXY][s_nBinXY];
      Int_t m_nominalSample;
      Int_t m_globalLeftRange;
      Int_t m_globalRightRange;
      Int_t m_peakSensingLeftRange;
      Int_t m_peakSensingRightRange;

      bool m_isJointNearbyCrystal;

      void ReadCDTMap();

      /// 
      ClassDef(LcTrigSimCluster,1);
   };
}

#endif /* LcTrigSimCluster.h guard */
