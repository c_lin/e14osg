#include "LcTrigSim/LcTrigSimCluster.h"

#include <fstream>

#include "LcWfm/LcWfmParameter.h"

namespace LcLib
{
   /// constructor
   LcTrigSimCluster::LcTrigSimCluster()
      : m_detpar("CSI"),
        m_bitX( m_detpar.GetNchannel() ,-1),
        m_bitY( m_detpar.GetNchannel() ,-1),
        m_isMasked( m_detpar.GetNchannel(), false ),
        m_nominalSample(30),
        m_globalLeftRange(2),
        m_globalRightRange(3),
        m_peakSensingLeftRange(1),
        m_peakSensingRightRange(2),
        m_isJointNearbyCrystal(false)
   {
      ReadCDTMap();
   }

   /// destructor
   LcTrigSimCluster::~LcTrigSimCluster()
   {
      ;
   }

   ///
   void LcTrigSimCluster::Reset()
   {
      m_isJointNearbyCrystal = false;
      for( Int_t ix=0; ix<s_nBinXY; ++ix )
         for( Int_t iy=0; iy<s_nBinXY; ++iy )
            m_isBitOn[ix][iy] = false;
   }

   void LcTrigSimCluster::Fill( const Int_t modId )
   {
      if( modId<0 || modId>=m_detpar.GetNchannel() ) return;
      if( m_isMasked[modId] ) return;
      BitXY_t bitxy = GetBitXY( modId );
      m_isBitOn[ bitxy.first ][ bitxy.second ] = true;
   }

   void LcTrigSimCluster::FillByWfm( const Int_t modId, const LcWfm &wfm,
                                     const Double_t peak_threshold, const Double_t pedestal )
   {
      if( IsGetClusterBit(wfm, peak_threshold, pedestal) ) Fill(modId);
   }

   bool LcTrigSimCluster::IsGetClusterBit( const LcWfm &wfm, const Double_t peak_threshold,
                                           const Double_t pedestal ) const
   {
      if( wfm.GetNsample()!=LcWfmParameter::k_125MHzWfmNsample ){
         Error("IsGetClusterBit","Input waveform #sample = %d",wfm.GetNsample());
         return false;
      }
      
      Int_t peakSample = -1;
      for( Int_t isam=m_nominalSample-m_globalLeftRange; 
                 isam<= m_nominalSample+m_globalRightRange; ++isam )
      {
         if( wfm[isam] >= peak_threshold + pedestal )
            peakSample = isam;
      }

      /// false if no any sample within global range exceeds the threshold 
      if( peakSample < 0 ) return false;

      /// false if the peak is not within the peak sensing range
      if(    peakSample < m_nominalSample - m_peakSensingLeftRange 
          || peakSample < m_nominalSample - m_peakSensingRightRange ) return false;

      /// 
      return true;
    
   }

   void LcTrigSimCluster::JointNearbyCrystal()
   {
      m_isJointNearbyCrystal = true;
      bool isFakeBitOn[s_nBinXY][s_nBinXY] = {};

      /// get extra fake bit map
      for( Int_t ix=1; ix<s_nBinXY-1; ++ix )
      {
         for( Int_t iy=1; iy<s_nBinXY-1; ++iy )
         {
            EBlockPattern pattern = GetBlockPattern( m_isBitOn[ix+1][iy+1], m_isBitOn[ix  ][iy+1],
                                                     m_isBitOn[ix  ][iy  ], m_isBitOn[ix+1][iy  ] ); 
            if( pattern==kDiagonal ){
               isFakeBitOn[ix  ][iy  ] = true;
               isFakeBitOn[ix+1][iy  ] = true;
               isFakeBitOn[ix  ][iy+1] = true;
               isFakeBitOn[ix+1][iy+1] = true;
            }
         } // END iy loop
      } // END ix loop

      /// add fake bit on original map
     for( Int_t ix=1; ix<s_nBinXY-1; ++ix )
     {
        for( Int_t iy=1; iy<s_nBinXY-1; ++iy )
        {
           if(isFakeBitOn[ix][iy]) m_isBitOn[ix][iy] = true;
        } // END iy loop
     } // END ix loop

   }
 
   void LcTrigSimCluster::SetMaskingModId( const Int_t modId )
   {
      if( modId<0 || modId>=m_detpar.GetNchannel() ) return;
      m_isMasked[modId] = true;
   }

   void LcTrigSimCluster::SetMaskingByUserflag( const Int_t userflag )
   {
      /// get the masking list file
      const Int_t accRunNo = m_detpar.GetAccRunNo(userflag);
      const std::string fmask_name =   m_detpar.GetE14ExtDir() 
                                    +  "/TriggerSimulation/cluster/MaskingList"
                                    + Form("/Masking_run%d.txt",accRunNo); 
      std::ifstream fmask( fmask_name.c_str() );
      if( fmask.fail() ){
         Warning("SetMaskingByUserflag","The masking list for run%d is not found.",accRunNo);
         return;
      }

      /// read masing list
      Int_t rmask;
      while( fmask >> rmask ){
         SetMaskingModId(rmask);
      } 
   }

   Int_t LcTrigSimCluster::GetCSIModId( const Int_t bitx, const Int_t bity ) const
   {
      std::map<BitXY_t, Int_t>::const_iterator it = m_bitMap.find( BitXY_t(bitx,bity) );
      return (it!=m_bitMap.end()) ? it->second : -1;
   }
      
   LcTrigSimCluster::BitXY_t LcTrigSimCluster::GetBitXY( const Int_t modId ) const
   {
      return ( modId<0 || modId>=m_detpar.GetNchannel() ) ? BitXY_t() 
                                                          : BitXY_t(m_bitX[modId],m_bitY[modId]);  
   }

   Int_t LcTrigSimCluster::GetNcluster() const
   {
      Int_t nturn = 0;
      for( Int_t ix=0; ix<s_nBinXY-1; ++ix )
      {
         for( Int_t iy=0; iy<s_nBinXY-1; ++iy )
         {
            /////////////////////
            /// Block quadrant
            /// quad2 quad1
            /// quad3 quad4
            EBlockPattern pattern = GetBlockPattern( m_isBitOn[ix+1][iy+1], m_isBitOn[ix][iy+1],
                                                     m_isBitOn[ix][iy]    , m_isBitOn[ix+1][iy] ); 
            switch(pattern)
            {
               case kConvexity : nturn++ ;                                   break;
               case kConcavity : nturn-- ;                                   break;
               case kDiagonal  : nturn+= (m_isJointNearbyCrystal) ? 2 : -2 ; break;
               default         : ; 
            }

         } // END iy loop
      } // END ix loop

      return nturn/4;

   }

   LcTrigSimCluster::EBlockPattern 
   LcTrigSimCluster::GetBlockPattern( const bool quad1, const bool quad2,
                                      const bool quad3, const bool quad4 ) const
   {
      /// quad2, quad1
      /// quad3, quad4
            
      ///////////////////////////////////////////////////////////////////////////
      /// convexity group
      /// 1 0
      ///.0 0
      if(         ( quad1==1 && quad2==0 && quad3==0 && quad4==0 )  
               || ( quad1==0 && quad2==1 && quad3==0 && quad4==0 )
               || ( quad1==0 && quad2==0 && quad3==1 && quad4==0 )
               || ( quad1==0 && quad2==0 && quad3==0 && quad4==1 )
        )
      {
         return kConvexity;
      }
      ////////////////////////////////////////////////////////////////////////////
      /// concavity group
      /// 1 1
      /// 1 0
      else if(    ( quad1==1 && quad2==1 && quad3==1 && quad4==0 )
               || ( quad1==1 && quad2==1 && quad3==0 && quad4==1 )
               || ( quad1==1 && quad2==0 && quad3==1 && quad4==1 )
               || ( quad1==0 && quad2==1 && quad3==1 && quad4==1 )
             )
      {
         return kConcavity;
      }
      //////////////////////////////////////////////////////////////////////////////
      /// diagonal group
      /// 1 0  
      /// 0 1
      else if(    ( quad1==1 && quad2==0 && quad3==1 && quad4==0 )
               || ( quad1==0 && quad2==1 && quad3==0 && quad4==1 )
             )
      {
         return kDiagonal;
      }
      //////////////////////////////////////////////////////////////////////////////
      /// others
      ///.1 1  1 1  0 0
      /// 1 1  0 0  0 0
      else
      {
         return kOthers;
      }
   }

   TH2D* LcTrigSimCluster::GetClusterBitMap() const
   {
      TH2D *h2_cb = new TH2D("h2_cb","",s_nBinXY,0,s_nBinXY,s_nBinXY,0,s_nBinXY);
      for( Int_t ix=0; ix<s_nBinXY; ++ix )
         for( Int_t iy=0; iy<s_nBinXY; ++iy )
            h2_cb->SetBinContent(ix+1, iy+1, m_isBitOn[ix][iy]);

      return h2_cb;
   }

   /// private ///
   void LcTrigSimCluster::ReadCDTMap()
   {
      std::string bitfname =   std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
                             + "/TriggerSimulation/cluster/CDT_csimap.txt";

      std::ifstream bitfile( bitfname.c_str() );
      if( bitfile.fail() ){
         Error("ReadCDTMap","Fail to get CDT map.");
         return;
      }

      Int_t rindex, rbitx, rbity;
      while( bitfile >> rindex >> rbitx >> rbity )
      {
         m_bitX[ rindex ] = rbitx;
         m_bitY[ rindex ] = rbity;
         m_bitMap.insert( std::make_pair( BitXY_t(rbitx, rbity), rindex ) );
      }
   
   }
}
