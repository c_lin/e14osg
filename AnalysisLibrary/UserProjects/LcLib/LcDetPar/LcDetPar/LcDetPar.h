#ifndef LCLIB_LCDETPAR_LCDETPAR_H
#define LCLIB_LCDETPAR_LCDETPAR_H

#include <cstdlib>
#include <string>
#include <map>

#include "TObject.h"

#include "MTAnalysisLibrary/MTBasicParameters.h"

namespace LcLib
{
   class LcDetPar : public TObject
   {
    public :
      /// constructor
      LcDetPar( const std::string detname );

      /// destructor
      virtual ~LcDetPar();

      /// 
      bool    IsZombie() const { return ( m_MTBPindex < 0 ); }

      /// getter
      bool    Is125MHzDetector() const;
      bool    Is500MHzDetector() const;
      Int_t   GetNchannel() const;
      Int_t   GetNmodule() const;

      std::string GetGsimDetectorName() const;

      Int_t   GetModId( const Int_t idx ) const;
      Int_t   GetIndex( const Int_t modId ) const;

      std::string GetDetectorName() const { return m_detname; }
      std::string GetE14ExtDir() const { return m_e14ext_dir; }
      Int_t       GetAccRunNo( const Int_t userflag ) const;

    protected :
      const std::string m_detname;
      const std::string m_e14ext_dir;

    private :
      Int_t m_MTBPindex;
      std::map<Int_t, Int_t> m_indexMap;

      void LoadIndexFile();
      Int_t GetMTBPIndex();

      ClassDef(LcDetPar,1);
   };
}

#endif /* LcDetPar.h guard */
