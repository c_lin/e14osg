#ifndef LCLIB_LCDETPAR_LCDETPARECALIB_H
#define LCLIB_LCDETPAR_LCDETPARECALIB_H

#include <map>

#include "TObject.h"

#include "LcDetPar/LcDetPar.h"

namespace LcLib
{
   class LcDetParECalib : public LcDetPar
   {
    public : 
      /// constructor
      LcDetParECalib( const std::string detname, const Int_t userFlag = 20180601 );

      /// destructor 
      virtual ~LcDetParECalib();

      /// getter 
      Double_t GetECalibConst( const Int_t modId ) const;     
 
    private :
      const Int_t               m_accRunNo;
      std::map<Int_t, Double_t> m_eCalibMap;     

      void ReadECalibFile();      

      ClassDef(LcDetParECalib,1);
   };

}

#endif /* LcDetParECalib.h guard */
