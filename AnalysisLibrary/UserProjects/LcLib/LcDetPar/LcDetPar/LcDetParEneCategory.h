#ifndef LCLIB_LCDETPAR_LCDETPARENECATEGORY_H
#define LCLIB_LCDETPAR_LCDETPARENECATEGORY_H

#include <string>

#include "TObject.h"

#include "LcDetPar/LcDetPar.h"

namespace LcLib
{
   class LcDetParEneCategory : public LcDetPar
   {
    public :
      /// constructor
      LcDetParEneCategory( const std::string detname );

      /// destructor
      virtual ~LcDetParEneCategory();

      ///
      Int_t GetCategoryId( const Double_t ene ) const; 
      Int_t GetNcategory() const { return m_nCategory; }

    private :
      Int_t m_nCategory;
      void SetNcategory();

      ClassDef(LcDetParEneCategory,1);
   }; 
}

#endif /* LcDetParEneCategory.h guard */
