#ifndef LCLIB_LCDETPAR_LCDETPARDATAWFMT0_H
#define LCLIB_LCDETPAR_LCDETPARDATAWFMT0_H

#include <string>
#include <vector>

#include "TObject.h"

#include "LcDetPar/LcDetPar.h"

namespace LcLib
{
   class LcDetParDataWfmT0 : public LcDetPar
   {
    public :
      /// constructor
      LcDetParDataWfmT0( const std::string detname, const Int_t userflag );

      /// destructor
      virtual ~LcDetParDataWfmT0();

      ///
      Int_t GetDataWfmT0( const Int_t modId = 0 ) const;

    private :
      std::vector<Int_t> m_wfmT0Vec;
      Int_t m_accRunNo;

      void  SetDataWfmT0();
      void  SetDataWfmT0Run79();
      void  SetDataWfmT0Run78();
      void  SetDataWfmT0Run74();
      void  SetDataWfmT0Run69();

      ClassDef(LcDetParDataWfmT0,1);
   }; 
}

#endif /* LcDetParDataWfmT0.h guard */
