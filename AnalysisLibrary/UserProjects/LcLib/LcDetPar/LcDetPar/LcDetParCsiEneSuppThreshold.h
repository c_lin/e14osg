#ifndef LCLIB_LCDETPAR_LCDETPARCSIENESUPPTHRESHOLD_H
#define LCLIB_LCDETPAR_LCDETPARCSIENESUPPTHRESHOLD_H

#include <map>

#include "TObject.h"

#include "LcDetPar/LcDetPar.h"

namespace LcLib
{
   class LcDetParCsiEneSuppThreshold : public LcDetPar
   {
    public : 
      /// constructor
      LcDetParCsiEneSuppThreshold( const Int_t userFlag = 20180601 );

      /// destructor 
      virtual ~LcDetParCsiEneSuppThreshold();

      /// getter 
      Double_t GetEneSuppThreshold( const Int_t modId ) const;     
 
    private :
      const Int_t               m_accRunNo;
      std::map<Int_t, Double_t> m_eneSuppMap;     

      void ReadCsiEneSuppFile();      

      ClassDef(LcDetParCsiEneSuppThreshold,1);
   };

}

#endif /* LcDetParCsiEneSuppThreshold.h guard */
