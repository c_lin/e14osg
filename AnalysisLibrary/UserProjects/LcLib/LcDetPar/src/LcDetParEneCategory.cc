#include "LcDetPar/LcDetParEneCategory.h"

#include "TMath.h"

namespace LcLib
{
   /// constructor
   LcDetParEneCategory::LcDetParEneCategory( const std::string detname )
      : LcDetPar( detname )
   {
      SetNcategory();
   }

   /// destructor
   LcDetParEneCategory::~LcDetParEneCategory()
   {
      ;
   }

   ///
   Int_t LcDetParEneCategory::GetCategoryId( const Double_t ene ) const
   {
       /// binning methood   
       /// CV     : log_2 (5*E) with bin width = 1
       /// others : log_10(E  ) with bin width = 0.5

      const Double_t bin_width = (m_detname=="CV") ? 1. : 0.5;
      const Double_t val       = (m_detname=="CV") ? TMath::Log2(5*ene) : TMath::Log10(ene) ;
 
      const Int_t ibin = TMath::FloorNint( val / bin_width ) - 1;
      const Int_t k_MaxNbin = GetNcategory();

      if( ibin < 0 ){
         return -1;
      }else if( ibin >= k_MaxNbin ){
         return k_MaxNbin - 1;
      }else{
         return ibin;
      }

   }

   void LcDetParEneCategory::SetNcategory()
   {
      if     ( m_detname == "CSI"  ) m_nCategory = 5;
      else if( m_detname == "CBAR" ) m_nCategory = 3;
      else if( m_detname == "CV"   ) m_nCategory = 3;
      else if( m_detname == "NCC"  ) m_nCategory = 4;
      else if( m_detname == "FBAR" ) m_nCategory = 4;
      else if( m_detname == "IB"   ) m_nCategory = 4;
      else if( m_detname == "CC06" ) m_nCategory = 4;
      else                           m_nCategory = 0;
   }

}
