#include "LcDetPar/LcDetParCsiEneSuppThreshold.h"

#include <fstream>

#include "TString.h"

namespace LcLib
{

   /// constructor
   LcDetParCsiEneSuppThreshold::LcDetParCsiEneSuppThreshold( const Int_t userFlag )
      : LcDetPar( "CSI" ),
        m_accRunNo( GetAccRunNo(userFlag) )
   {
      Info("LcDetParCsiEneSuppThreshold", "Run %d configuration is set. ", m_accRunNo );
      ReadCsiEneSuppFile();
   }

   /// destructor
   LcDetParCsiEneSuppThreshold::~LcDetParCsiEneSuppThreshold()
   {
      ;
   }

   /// getter
   Double_t LcDetParCsiEneSuppThreshold::GetEneSuppThreshold( const Int_t modId ) const
   {
      std::map<Int_t, Double_t>::const_iterator it = m_eneSuppMap.find(modId);
      if( it!=m_eneSuppMap.end() ){
         return it->second;      
      }else{
         Warning("GetEneSuppThreshold", "User input mod Id : %d is not found.",modId);
         return 0.;
      }
   }

   /// private ///
   void LcDetParCsiEneSuppThreshold::ReadCsiEneSuppFile()
   {
      std::string fname = m_e14ext_dir + "/DetectorParameter/CsiEneSuppThreshold" 
                                       + Form("/RUN%d/",m_accRunNo)
                                       + "CsiEneSuppThreshold.txt";

      std::ifstream ifile( fname.c_str() );

      if( ifile.fail() ){
         Error( "ReadCsiEneSuppFile", 
                "Fail to read the Csi Energy Suppression File");
         return;
      }

      Int_t r_id;
      Double_t r_val;
      while( ifile >> r_id >> r_val ){
         m_eneSuppMap[r_id] = r_val;
      }

   }


}
