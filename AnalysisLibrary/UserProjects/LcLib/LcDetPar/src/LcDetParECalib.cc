#include "LcDetPar/LcDetParECalib.h"

#include <fstream>

#include "TString.h"

namespace LcLib
{

   /// constructor
   LcDetParECalib::LcDetParECalib( const std::string detname, const Int_t userFlag )
      : LcDetPar(detname),
        m_accRunNo( GetAccRunNo(userFlag) )
   {
      Info("LcDetParECalib", "Run %d configuration is set. ", m_accRunNo );
      ReadECalibFile();
   }

   /// destructor
   LcDetParECalib::~LcDetParECalib()
   {
      ;
   }

   /// getter
   Double_t LcDetParECalib::GetECalibConst( const Int_t modId ) const
   {
      std::map<Int_t, Double_t>::const_iterator it = m_eCalibMap.find(modId);
      if( it!=m_eCalibMap.end() ){
         return it->second;      
      }else{
         Warning("GetECalibConst", "User input mod Id : %d is not found.",modId);
         return 0.;
      }
   }

   /// private ///
   void LcDetParECalib::ReadECalibFile()
   {
      std::string fname = m_e14ext_dir + "/DetectorParameter/ECalibConst" 
                                       + Form("/RUN%d/",m_accRunNo)
                                       + m_detname + ".txt";

      std::ifstream ifile( fname.c_str() );

      if( ifile.fail() ){
         Error( "ReadECalibFile", "Fail to read the ECalibConst files for %s", m_detname.c_str() );
         return;
      }

      Int_t r_idx, r_modId;
      Double_t r_val;
      while( ifile >> r_idx >> r_modId >> r_val ){
         m_eCalibMap[r_modId] = r_val;
      }

   }


}
