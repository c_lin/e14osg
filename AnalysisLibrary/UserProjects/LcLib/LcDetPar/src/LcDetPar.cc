#include "LcDetPar/LcDetPar.h"

#include <fstream>

namespace LcLib
{

   /// constructor
   LcDetPar::LcDetPar( const std::string detname )
      : m_detname(detname),
        m_e14ext_dir( std::string( std::getenv("E14ANA_EXTERNAL_DATA") )  )
   {
      m_MTBPindex = GetMTBPIndex();
      LoadIndexFile();
   }

   /// destructor
   LcDetPar::~LcDetPar()
   {
      ;
   }

   ///
   bool LcDetPar::Is125MHzDetector() const
   {
      return ( !IsZombie() ) ? MTBP::is125MHzDetector[m_MTBPindex] : false; 
   }

   bool LcDetPar::Is500MHzDetector() const
   {
      return ( !IsZombie() ) ? MTBP::is500MHzDetector[m_MTBPindex] : false;
   }

   Int_t LcDetPar::GetNchannel() const
   {
      return ( !IsZombie() ) ? MTBP::DetNChannels[m_MTBPindex] : 0 ;
   }

   Int_t LcDetPar::GetNmodule() const
   {
      return ( !IsZombie() ) ? MTBP::DetNModules[m_MTBPindex] : 0 ;
   }

   std::string LcDetPar::GetGsimDetectorName() const
   {
      return ( !IsZombie() ) ? MTBP::GsimE14DetName[m_MTBPindex] : "";
   }

   ///
   Int_t LcDetPar::GetModId( const Int_t idx ) const
   {
      if( idx < 0 || idx > GetNchannel() ){
         Warning( "GetModId", "User input index %d out of range" ,idx);
         return -1;
      }

      std::map<Int_t, Int_t>::const_iterator it = m_indexMap.begin();
      std::advance(it, idx);
      return it->first;
   }

   Int_t LcDetPar::GetIndex( const Int_t modId ) const
   {
      std::map<Int_t, Int_t>::const_iterator it = m_indexMap.find(modId);
      if( it!=m_indexMap.end() ) return it->second;
      else                       return -1;
   }   

   Int_t LcDetPar::GetAccRunNo( const Int_t userFlag ) const
   {
      if( userFlag<20160501 || userFlag>=20300000 ){
         /// p.s. runs before 2016 are not supported.
         Warning("GetAccRunNo", "Invalid input userFlag : %d",userFlag);
         return -1;
      }else if( userFlag<20170401 ){
         return 69;
      }else if( userFlag<20180101 ){
         return 74;
      }else if( userFlag<20180601 ){
         return 78;
      }else if( userFlag<20181231 ){
         return 79;
      }else if( userFlag<20191231 ){
         return 81;
      }else if( userFlag<20201231 ){
         return 85;
      }else if( userFlag<20210430 ){
         return 86;
      }else if( userFlag<20211231 ){
         return 87;
      }else{
         return 87;
      }
   }

   /// private ///
   void LcDetPar::LoadIndexFile()
   {
      std::string fname = m_e14ext_dir + "/DetectorParameter/IndexMap/" + m_detname + ".txt";
      std::ifstream ifile( fname.c_str() );

      if( ifile.fail() ){
         Error( "LoadIndexFile", "Fail to read the index map for %s", m_detname.c_str() );
         return;
      }

      Int_t r_idx, r_modId;
      while( ifile >> r_idx >> r_modId ){
         m_indexMap[r_modId] = r_idx;
      }

   }

   Int_t LcDetPar::GetMTBPIndex()
   {
      Int_t det_index = -1;
      for( Int_t idx=0; idx<MTBP::nDetectors; ++idx )
      {
         if( m_detname==MTBP::detectorName[idx] ){
            det_index = idx;
            break;
         }
      }
      if( det_index<0 ) Error("GetMTBPIndex","unknown input detector : %s",m_detname.c_str() );
      return det_index;     
   }

}
