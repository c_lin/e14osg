#include "LcDetPar/LcDetParDataWfmT0.h"

namespace LcLib
{
   /// constructor
   LcDetParDataWfmT0::LcDetParDataWfmT0( const std::string detname, const Int_t userflag )
      : LcDetPar( detname ),
        m_accRunNo( GetAccRunNo(userflag) ) 
   {
      SetDataWfmT0();
   }

   /// destructor
   LcDetParDataWfmT0::~LcDetParDataWfmT0()
   {
      ;
   }

   ///
   Int_t LcDetParDataWfmT0::GetDataWfmT0( const Int_t modId ) const
   {
      if( m_wfmT0Vec.size()==0 ) return -1;                  // unknown 
      else if( m_wfmT0Vec.size()==1 ) return m_wfmT0Vec[0];  // same value for all mod
      else if( m_wfmT0Vec.size()==2 ){                       // for barrel counter only
         if( modId<100 ) return m_wfmT0Vec[0];
         else            return m_wfmT0Vec[1];
      }
      return -2;
   }

   /// private ///
   void LcDetParDataWfmT0::SetDataWfmT0()
   {
      if      ( m_accRunNo==79 ) SetDataWfmT0Run79();
      else if ( m_accRunNo==78 ) SetDataWfmT0Run78();
      else if ( m_accRunNo==74 ) SetDataWfmT0Run74();
      else if ( m_accRunNo==69 ) SetDataWfmT0Run69();
      else                       SetDataWfmT0Run79();
   }

   void LcDetParDataWfmT0::SetDataWfmT0Run79()
   {
      if     ( m_detname=="FBAR" ){
         m_wfmT0Vec.push_back(31);
      }else if( m_detname=="NCC" ){
         m_wfmT0Vec.push_back(30);
      }else if( m_detname=="CV"  ){
         m_wfmT0Vec.push_back(28);
      }else if( m_detname=="CC06"){ 
         m_wfmT0Vec.push_back(29);
      }else if( m_detname=="CBAR"){
         m_wfmT0Vec.push_back(31);  // upstream
         m_wfmT0Vec.push_back(28);  // downstream
      }else if( m_detname=="IB" ){
         m_wfmT0Vec.push_back(126); // upstream
         m_wfmT0Vec.push_back(118); // downstream
      } 
   }

   void LcDetParDataWfmT0::SetDataWfmT0Run78()
   {
      if     ( m_detname=="FBAR" ){
         m_wfmT0Vec.push_back(31);
      }else if( m_detname=="NCC" ){
         m_wfmT0Vec.push_back(27);
      }else if( m_detname=="CV"  ){
         m_wfmT0Vec.push_back(32);
      }else if( m_detname=="CC06"){
         m_wfmT0Vec.push_back(25);
      }else if( m_detname=="CBAR"){
         m_wfmT0Vec.push_back(32);  // upstream
         m_wfmT0Vec.push_back(30);  // downstream
      }else if( m_detname=="IB" ){
         m_wfmT0Vec.push_back(86); // upstream
         m_wfmT0Vec.push_back(78); // downstream
      }
   }

   void LcDetParDataWfmT0::SetDataWfmT0Run74()
   {
      if     ( m_detname=="FBAR" ){
         m_wfmT0Vec.push_back(31);
      }else if( m_detname=="NCC" ){
         m_wfmT0Vec.push_back(28);
      }else if( m_detname=="CV"  ){
         m_wfmT0Vec.push_back(33);
      }else if( m_detname=="CC06"){
         m_wfmT0Vec.push_back(26);
      }else if( m_detname=="CBAR"){
         m_wfmT0Vec.push_back(31);  // upstream
         m_wfmT0Vec.push_back(31);  // downstream
      }else if( m_detname=="IB" ){
         m_wfmT0Vec.push_back(86); // upstream
         m_wfmT0Vec.push_back(82); // downstream
      }
   }

   void LcDetParDataWfmT0::SetDataWfmT0Run69()
   {
      if     ( m_detname=="FBAR" ){
         m_wfmT0Vec.push_back(31);
      }else if( m_detname=="NCC" ){
         m_wfmT0Vec.push_back(28);
      }else if( m_detname=="CV"  ){
         m_wfmT0Vec.push_back(33);
      }else if( m_detname=="CC06"){
         m_wfmT0Vec.push_back(26);
      }else if( m_detname=="CBAR"){
         m_wfmT0Vec.push_back(31);  // upstream
         m_wfmT0Vec.push_back(31);  // downstream
      }else if( m_detname=="IB" ){
         m_wfmT0Vec.push_back(110); // upstream
         m_wfmT0Vec.push_back(106); // downstream
      }
   }

}
