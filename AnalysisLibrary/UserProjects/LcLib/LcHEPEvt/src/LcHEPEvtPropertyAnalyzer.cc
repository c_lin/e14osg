#include "LcHEPEvt/LcHEPEvtPropertyAnalyzer.h"

#include "TMath.h"

namespace LcLib
{
   //
   //
   //
   LcHEPEvtPropertyAnalyzer::LcHEPEvtPropertyAnalyzer()
      : m_csiZsurface(6168.),
        m_csiRadius(900.),
        m_csiHoleHalfWidth(100.),
        m_scanRadius(70.)
   {
      ;
   }

   //
   //
   //
   LcHEPEvtPropertyAnalyzer::~LcHEPEvtPropertyAnalyzer()
   {
      ;
   }

   void LcHEPEvtPropertyAnalyzer::Reset()
   {
      m_pCsiList.clear();
      m_pVetoDecayVolumeList.clear();
      m_pVetoBeamHoleList.clear();
   }

   //
   //
   //
   void LcHEPEvtPropertyAnalyzer::SetParticleList( std::list<LcHEPEvtParticle> const& plist ) 
   {
      std::list<LcHEPEvtParticle> pCsiList, pVetoBeamHoleList, pVetoDecayVolumeList;
      for(std::list<LcHEPEvtParticle>::const_iterator ip=plist.begin(); ip!=plist.end(); ++ip )
      {
         LcHEPEvtParticle const& p = *ip;
         switch( GetHitFlag( p.GetVertex(), p.GetEndPos() ) )
         {
            case k_CsiHit:
               pCsiList.push_back( p );
               break;
            case k_BeamHoleHit:
               pVetoBeamHoleList.push_back( p );
               break;
            case k_DecayVolumeHit:
               pVetoDecayVolumeList.push_back( p );
               break;
            default:
               ;
         }
      }
      m_pCsiList.swap( pCsiList );
      m_pVetoBeamHoleList.swap( pVetoBeamHoleList );
      m_pVetoDecayVolumeList.swap( pVetoDecayVolumeList );
   }

   //
   //
   //
   std::vector<LcHEPEvtPropertyAnalyzer::CsiTrueHit> 
   LcHEPEvtPropertyAnalyzer::GetCsiClusterMatchTrueHitVec( TVector3 const& rec_pos ) const
   {
      std::vector<CsiTrueHit> trueHitVec;
      for(std::list<LcHEPEvtParticle>::const_iterator ip = m_pCsiList.begin();
                                                      ip!= m_pCsiList.end()  ; ++ip )
      {
         LcHEPEvtParticle const& p = *ip;
         TVector3 true_pos = ProjectCsi( p.GetVertex(), p.GetEndPos() );
         TVector3 disp = true_pos - rec_pos;
         if( disp.Perp() < m_scanRadius ){
            CsiTrueHit hit;
            hit.id = p.GetUserId();
            hit.pos = true_pos;
            hit.e = p.GetEnergy();
            trueHitVec.push_back( hit );
         }
      }

      return trueHitVec;
   }

   //
   //
   //
   LcHEPEvtPropertyAnalyzer::E_HitFlag 
   LcHEPEvtPropertyAnalyzer::GetHitFlag(TVector3 const& beg_pos, TVector3 const& end_pos ) const
   {
      TVector3 v_csi = ProjectCsi(beg_pos, end_pos);
      if( v_csi.z() > m_csiZsurface ) return k_BadDecay;

      /// goes into the beam hole
      if(    TMath::Abs(v_csi.x())<m_csiHoleHalfWidth 
          && TMath::Abs(v_csi.y())<m_csiHoleHalfWidth ) return k_BeamHoleHit;

      /// hit on the barrel
      if( v_csi.Perp() > m_csiRadius ) return k_DecayVolumeHit;

      return k_CsiHit;
   }

   //
   //
   //
   TVector3 LcHEPEvtPropertyAnalyzer::ProjectCsi(TVector3 const& beg_pos, 
                                                 TVector3 const& end_pos) const
   {
      if( beg_pos.z()>m_csiZsurface ) return TVector3(0., 0., HUGE_VAL);
      TVector3 disp = end_pos - beg_pos;
      if( disp.z()<1.e-6 ) return TVector3( m_csiRadius, m_csiRadius, m_csiZsurface );
      Double_t scale = (m_csiZsurface - beg_pos.z() ) / disp.z();
      return beg_pos + disp * scale;
   }

}
