#include "LcHEPEvt/LcHEPEvtTrueDataContainer.h"

namespace LcLib
{

   LcHEPEvtTrueDataContainer::LcHEPEvtTrueDataContainer()
   {
      ;
   }

   LcHEPEvtTrueDataContainer::~LcHEPEvtTrueDataContainer()
   {
      ;
   }

   bool LcHEPEvtTrueDataContainer::SetBranchAddresses( TTree *tr )
   {
      if( tr==NULL ) return false;
      tr->SetBranchAddress("GenParticleNumber"  ,&m_num     );
      tr->SetBranchAddress("GenParticleTrack"   , m_track   );
      tr->SetBranchAddress("GenParticleMother"  , m_mother  );
      tr->SetBranchAddress("GenParticlePid"     , m_pid     );
      tr->SetBranchAddress("GenParticleMom"     , m_mom     );
      tr->SetBranchAddress("GenParticleEk"      , m_ek      );
      tr->SetBranchAddress("GenParticleMass"    , m_mass    );
      tr->SetBranchAddress("GenParticleTime"    , m_time    );
      tr->SetBranchAddress("GenParticlePos"     , m_pos     );
      tr->SetBranchAddress("GenParticleEndTime" , m_endTime );
      tr->SetBranchAddress("GenParticleEndPos"  , m_endPos  );
      return true;
   }

   bool LcHEPEvtTrueDataContainer::AddBranches( TTree *tr )
   {
      if( tr==NULL ) return false;
      tr->Branch("GenParticleNumber"  ,&m_num    ,"GenParticleNumber/I"                       );
      tr->Branch("GenParticleTrack"   , m_track  ,"GenParticleTrack[GenParticleNumber]/I"     );
      tr->Branch("GenParticleMother"  , m_mother ,"GenParticleMother[GenParticleNumber]/I"    );
      tr->Branch("GenParticlePid"     , m_pid    ,"GenParticlePid[GenParticleNumber]/I"       );
      tr->Branch("GenParticleMom"     , m_mom    ,"GenParticleMom[GenParticleNumber][3]/D"    );
      tr->Branch("GenParticleEk"      , m_ek     ,"GenParticleEk[GenParticleNumber]/D"        );
      tr->Branch("GenParticleMass"    , m_mass   ,"GenParticleMass[GenParticleNumber]/D"      );
      tr->Branch("GenParticleTime"    , m_time   ,"GenParticleTime[GenParticleNumber]/D"      );
      tr->Branch("GenParticlePos"     , m_pos    ,"GenParticlePos[GenParticleNumber][3]/D"    );
      tr->Branch("GenParticleEndTime" , m_endTime,"GenParticleEndTime[GenParticleNumber]/D"   );
      tr->Branch("GenParticleEndPos"  , m_endPos ,"GenParticleEndPos[GenParticleNumber][3]/D" );
      return true;
   }

   //
   //
   //
   std::list<LcHEPEvtParticle> LcHEPEvtTrueDataContainer::GetParticleList()
   {
      std::list<LcHEPEvtParticle> plist;

      for( Int_t ip=0; ip<m_num; ++ip )
      {
         LcHEPEvtParticle particle;

         particle.SetUserId( ip );
         particle.SetPid( m_pid[ip] );
         particle.SetTrack( m_track[ip] );
         particle.SetMother( m_mother[ip] );
         particle.SetP3( m_mom[ip][0], m_mom[ip][1], m_mom[ip][2] );
         particle.SetMass( m_mass[ip] );
         particle.SetVertex( m_pos[ip][0], m_pos[ip][1], m_pos[ip][2] );
         particle.SetEndPos( m_endPos[ip][0], m_endPos[ip][1], m_endPos[ip][2] );
         particle.SetTime( m_time[ip] );
         particle.SetEndTime( m_endTime[ip] );
         particle.SetEnergy( m_ek[ip] );

         plist.push_back(particle);
      }

      return plist;
   }

}
