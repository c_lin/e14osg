#include "LcHEPEvt/LcHEPEvtParticle.h"

namespace LcLib
{
   //
   //! @brief Default constructor.
   //
   LcHEPEvtParticle::LcHEPEvtParticle()
   {
      ;
   }

   LcHEPEvtParticle::LcHEPEvtParticle( GsimTrackData* gtrack )
   {
      SetParticleByGsimTrack( gtrack );
   }

   //
   //! @brief Default destructor.
   //
   LcHEPEvtParticle::~LcHEPEvtParticle()
   {
      ;
   }

   //
   //
   //
   bool LcHEPEvtParticle::SetParticleByGsimTrack( GsimTrackData* gtrack )
   {
      if( !gtrack ) return false;
      m_track  = gtrack->track;
      m_pid    = gtrack->pid;
      m_mother = gtrack->mother;
      m_p3     = gtrack->p;
      m_v      = gtrack->v;
      m_mass   = gtrack->mass;
      m_time   = gtrack->time;
      m_energy = gtrack->ek;
      return true;
   }

   void LcHEPEvtParticle::SetP3( Double_t px, Double_t py, Double_t pz )
   {
      TVector3 p3(px, py, pz);
      m_p3 = p3;
   }

   void LcHEPEvtParticle::SetVertex( Double_t vx, Double_t vy, Double_t vz )
   {
      TVector3 v(vx, vy, vz);
      m_v = v;
   }

   void LcHEPEvtParticle::SetEndPos( Double_t endx, Double_t endy, Double_t endz )
   {
      TVector3 endPos( endx, endy, endz);
      m_endPos = endPos;
   }
}
