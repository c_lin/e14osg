#include "LcHEPEvt/LcHEPEvtHandler.h"

namespace LcLib
{
   //
   //! @brief Default constructor.
   //
   LcHEPEvtHandler::LcHEPEvtHandler()
   {
      ;
   }

   //
   //! @brief Default destructor
   //
   LcHEPEvtHandler::~LcHEPEvtHandler()
   {
      ;
   }

   //
   //! @brief Initializination of all parameters.
   //
   void LcHEPEvtHandler::Init()
   {
      m_NHEP = 0;
      for( Int_t i=0; i<s_arrSize; ++i )
      {
         m_ISTHEP[i] = 1;
         m_IDHEP[i] = 0; 
         m_JDAHEP1[i] = m_JDAHEP2[i] = 0;   
         m_PHEP1[i] = m_PHEP2[i] = m_PHEP3[i] = m_PHEP5[i] = 0.;
         m_VHEP1[i] = m_VHEP2[i] = m_VHEP3[i] = m_VHEP4[i] = 0.;
      }
   }

   //
   //! @brief Construction of TTree object for HEPEvt.
   //
   TTree* LcHEPEvtHandler::HEPEvtTree()
   {
      TTree* tr = new TTree("HEPEvt","HEPEvt");
      return tr;
   }

   //
   //! @brief 
   //
   bool LcHEPEvtHandler::AddBranches( TTree* tr )
   {
      if( tr==NULL ){
         Error("AddBranches","null pointer is given.");
         return false;
      }

      tr->Branch("NHEP"   ,&m_NHEP,   "NHEP/I");
      tr->Branch("ISTHEP" , m_ISTHEP, "ISTHEP[NHEP]/I");
      tr->Branch("IDHEP",   m_IDHEP,  "IDHEP[NHEP]/I");
      tr->Branch("JDAHEP1", m_JDAHEP1,"JDAHEP1[NHEP]/I");
      tr->Branch("JDAHEP2", m_JDAHEP2,"JDAHEP2[NHEP]/I");
      tr->Branch("PHEP1",   m_PHEP1,  "PHEP1[NHEP]/D");
      tr->Branch("PHEP2",   m_PHEP2,  "PHEP2[NHEP]/D");
      tr->Branch("PHEP3",   m_PHEP3,  "PHEP3[NHEP]/D");
      tr->Branch("PHEP5",   m_PHEP5,  "PHEP5[NHEP]/D");
      tr->Branch("VHEP1",   m_VHEP1,  "VHEP1[NHEP]/D");
      tr->Branch("VHEP2",   m_VHEP2,  "VHEP2[NHEP]/D");
      tr->Branch("VHEP3",   m_VHEP3,  "VHEP3[NHEP]/D");
      tr->Branch("VHEP4",   m_VHEP4,  "VHEP4[NHEP]/D");

      return true;
   }

   //
   //! @brief
   //
   bool LcHEPEvtHandler::SetPid( Int_t pid )
   {
      if( m_NHEP >= s_arrSize ) return false;
      m_IDHEP[ m_NHEP ] = pid;
      return true;
   }
    
   //
   //! @param[in] x, y, and z are in the unit of mm.
   //
   bool LcHEPEvtHandler::SetVertex( Double_t x, Double_t y, Double_t z)
   {
      if( m_NHEP >= s_arrSize ) return false;
      m_VHEP1[ m_NHEP ] = x;
      m_VHEP2[ m_NHEP ] = y;
      m_VHEP3[ m_NHEP ] = z; 
      return true;  
   }
    
   //
   //! @param[in] px, py, and pz are in the unit of MeV/c.
   //
   bool LcHEPEvtHandler::SetMomentum( Double_t px, Double_t py, Double_t pz )
   {
      if( m_NHEP >= s_arrSize ) return false;
      const Double_t k_MeV2GeV = 1.e-3;
      m_PHEP1[ m_NHEP ] = px * k_MeV2GeV;
      m_PHEP2[ m_NHEP ] = py * k_MeV2GeV;
      m_PHEP3[ m_NHEP ] = pz * k_MeV2GeV;
      return true;
   }
    
   //
   //! @param[in] time is in the unit of ns.
   //
   bool LcHEPEvtHandler::SetTiming( Double_t time )
   {
      if( m_NHEP >= s_arrSize ) return false;
      const Double_t k_ns2mm_c = 300.;
      m_VHEP4[ m_NHEP ] = time * k_ns2mm_c;
      return true;     
   }
    
   //
   //! @param[in] mass is in the unit of MeV/c^2
   //
   bool LcHEPEvtHandler::SetMass( Double_t mass )
   {
      if( m_NHEP >= s_arrSize ) return false;
      const Double_t k_MeV2GeV = 1.e-3;
      m_PHEP5[ m_NHEP ] = mass * k_MeV2GeV;
      return true;
   }

   //
   //! @brief
   //
   bool LcHEPEvtHandler::AddParticle( LcHEPEvtParticle const& particle )
   {
      SetPid( particle.GetPid() );
      SetVertex( particle.GetVx(), particle.GetVy(), particle.GetVz() );
      SetMomentum( particle.GetPx(), particle.GetPy(), particle.GetPz() );
      SetTiming( particle.GetTime() );
      SetMass( particle.GetMass() );
      return AddParticle();
   }

   //
   //! @brief
   //
   bool LcHEPEvtHandler::AddParticle()
   {
      if( m_NHEP >= s_arrSize ){
         Warning("AddParticle","Number of particles in this entry is more than the maximum size");
         return false;
      }
      
      m_NHEP++;
      return true;
   }

}
