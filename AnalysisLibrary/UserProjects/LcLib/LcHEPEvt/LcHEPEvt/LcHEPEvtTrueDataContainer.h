#ifndef LCLIB_LCHEPEVT_LCHEPEVTTRUEDATACONTAINER_H
#define LCLIB_LCHEPEVT_LCHEPEVTTRUEDATACONTAINER_H

#include <list>

#include "TObject.h"
#include "TTree.h"

#include "LcHEPEvt/LcHEPEvtParticle.h"

namespace LcLib
{
   class LcHEPEvtTrueDataContainer 
   {
    public:
      LcHEPEvtTrueDataContainer();
      ~LcHEPEvtTrueDataContainer();

      bool SetBranchAddresses( TTree *tr );
      bool AddBranches( TTree *Tr );

      std::list<LcHEPEvtParticle> GetParticleList();

    private:
      static const Int_t s_arrSize = 100;
      Int_t    m_num;
      Int_t    m_track[s_arrSize];
      Int_t    m_mother[s_arrSize];
      Int_t    m_pid[s_arrSize];
      Double_t m_mom[s_arrSize][3];
      Double_t m_ek[s_arrSize];
      Double_t m_mass[s_arrSize];
      Double_t m_time[s_arrSize];
      Double_t m_pos[s_arrSize][3];
      Double_t m_endTime[s_arrSize];
      Double_t m_endPos[s_arrSize][3];
   };
}


#endif /* LcHEPEvtTrueDataContainer.h guard. */
