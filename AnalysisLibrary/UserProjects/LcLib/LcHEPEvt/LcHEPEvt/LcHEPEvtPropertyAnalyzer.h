#ifndef LCLIB_LCHEPEVT_LCHEPEVTPROPERTYANALYZER_H
#define LCLIB_LCHEPEVT_LCHEPEVTPROPERTYANALYZER_H

#include <vector>
#include <list>

#include "TObject.h"
#include "TVector3.h"

#include "LcHEPEvt/LcHEPEvtParticle.h"

namespace LcLib
{
   class LcHEPEvtPropertyAnalyzer
   {
      enum E_HitFlag{ k_CsiHit=0, k_BeamHoleHit, k_DecayVolumeHit, k_BadDecay };

    public:
      LcHEPEvtPropertyAnalyzer();
      ~LcHEPEvtPropertyAnalyzer();

      void Reset();

      void SetCsiZsurface( Double_t csi_z ){ m_csiZsurface = csi_z; }
      void SetCsiRadius( Double_t csi_r ){ m_csiRadius = csi_r; }
      void SetCsiHoleHalfWidth( Double_t halfWidth ){ m_csiHoleHalfWidth = halfWidth; }
      void SetScanRadius( Double_t scan_r ){ m_scanRadius = scan_r; }

      void SetParticleList( std::list<LcHEPEvtParticle> const& plist ); 

      Int_t GetNTrueHitInCsi() const { return m_pCsiList.size(); }
      Int_t GetNTrueHitInDecayVolume() const { return m_pVetoDecayVolumeList.size(); }
      Int_t GetNTrueHitInBeamHole() const { return m_pVetoBeamHoleList.size(); }

      struct CsiTrueHit
      {
         Int_t id;
         TVector3 pos;
         Double_t e;
      };

      std::vector<CsiTrueHit> GetCsiClusterMatchTrueHitVec( TVector3 const& rec_pos ) const;

      std::list<LcHEPEvtParticle> const& GetCsiHitParticleList()          const 
                                         { return m_pCsiList;             }
      std::list<LcHEPEvtParticle> const& GetVetoDecayVoluneParticleList() const 
                                         { return m_pVetoDecayVolumeList; }
      std::list<LcHEPEvtParticle> const& GetVetoBeamHoleParticleList()    const
                                         { return m_pVetoDecayVolumeList; }

    private:
      Double_t m_csiZsurface;
      Double_t m_csiRadius;
      Double_t m_csiHoleHalfWidth;
      Double_t m_scanRadius;

      std::list<LcHEPEvtParticle> m_pCsiList;
      std::list<LcHEPEvtParticle> m_pVetoDecayVolumeList;
      std::list<LcHEPEvtParticle> m_pVetoBeamHoleList;

      E_HitFlag GetHitFlag(TVector3 const& beg_pos, TVector3 const& end_pos) const;
      TVector3 ProjectCsi(TVector3 const& beg_pos, TVector3 const& end_pos) const;

   };
}

#endif
