#ifndef LCLIB_LCHEPEVT_LCHEPEVTHANDLER_H
#define LCLIB_LCHEPEVT_LCHEPEVTHANDLER_H

#include "TObject.h"
#include "TTree.h"

#include "LcHEPEvt/LcHEPEvtParticle.h"

namespace LcLib
{
   class LcHEPEvtHandler
   {
    public:
      // constructor
      LcHEPEvtHandler();
   
      // destructor
      ~LcHEPEvtHandler();

      void Init();
      TTree* HEPEvtTree();
      bool AddBranches( TTree* tr );

      bool SetPid( Int_t pid );
      bool SetVertex( Double_t x, Double_t y, Double_t z);
      bool SetMomentum( Double_t px, Double_t py, Double_t pz );
      bool SetTiming( Double_t time );
      bool SetMass( Double_t mass );

      bool AddParticle( LcHEPEvtParticle const& particle );
      bool AddParticle();

    private:
      static const int s_arrSize = 100;
      Int_t     m_NHEP;
      Int_t     m_ISTHEP[s_arrSize];     // status
      Int_t     m_IDHEP[s_arrSize];      // PDG code
      Int_t     m_JDAHEP1[s_arrSize];    // 1st daughter
      Int_t     m_JDAHEP2[s_arrSize];    // 2nd daughter
      Double_t  m_PHEP1[s_arrSize];
      Double_t  m_PHEP2[s_arrSize];
      Double_t  m_PHEP3[s_arrSize];      // momentum GeV
      Double_t  m_PHEP5[s_arrSize];      // mass GeV
      Double_t  m_VHEP1[s_arrSize];
      Double_t  m_VHEP2[s_arrSize];
      Double_t  m_VHEP3[s_arrSize];      // vertex position mm
      Double_t  m_VHEP4[s_arrSize];      // time

   };
}

#endif /* LcHepEvtHandler.h guard */
