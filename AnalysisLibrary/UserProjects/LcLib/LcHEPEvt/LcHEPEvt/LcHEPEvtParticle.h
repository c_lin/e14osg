#ifndef LCLIB_LCHEPEVT_LCHEPEVTPARTICLE_H
#define LCLIB_LCHEPEVT_LCHEPEVTPARTICLE_H

#include "TObject.h"
#include "TVector3.h"

#include "GsimData/GsimTrackData.h"

namespace LcLib
{
   class LcHEPEvtParticle
   {
    public:
      LcHEPEvtParticle();
      LcHEPEvtParticle( GsimTrackData* gtrack );
      ~LcHEPEvtParticle();

      bool SetParticleByGsimTrack( GsimTrackData* gtrack );

      void SetUserId( Int_t userId )              { m_userId = userId;   }
      void SetPid( Int_t pid )                    { m_pid = pid;         }
      void SetTrack( Int_t track )                { m_track = track;     }
      void SetMother( Int_t mother )              { m_mother = mother;   }
      void SetP3( Double_t px, Double_t py, Double_t pz );
      void SetP3( TVector3 const& p3 )            { m_p3 = p3;           }
      void SetMass( Double_t mass )               { m_mass = mass;       }
      void SetVertex( Double_t vx, Double_t vy, Double_t vz );
      void SetVertex( TVector3 const& v )         { m_v = v;             }
      void SetEndPos( TVector3 const& endPos )    { m_endPos = endPos;   }
      void SetEndPos( Double_t endx, Double_t endy, Double_t endz );
      void SetTime( Double_t time )               { m_time = time;       }
      void SetEndTime( Double_t endTime )         { m_endTime = endTime; }
      void SetEnergy( Double_t energy )           { m_energy = energy;   }

      inline Int_t             GetUserId()  const { return m_userId;     }
      inline Int_t             GetPid()     const { return m_pid;        }
      inline Int_t             GetTrack()   const { return m_track;      }
      inline Int_t             GetMother()  const { return m_mother;     }
      inline TVector3 const&   GetP3()      const { return m_p3;         }
      inline Double_t          GetPx()      const { return m_p3.x();     }
      inline Double_t          GetPy()      const { return m_p3.y();     }
      inline Double_t          GetPz()      const { return m_p3.z();     }
      inline Double_t          GetMass()    const { return m_mass;       }
      inline TVector3 const&   GetVertex()  const { return m_v;          }
      inline Double_t          GetVx()      const { return m_v.x();      }
      inline Double_t          GetVy()      const { return m_v.y();      }
      inline Double_t          GetVz()      const { return m_v.z();      }
      inline TVector3 const&   GetEndPos()  const { return m_endPos;     }
      inline Double_t          GetEndPosX() const { return m_endPos.x(); }
      inline Double_t          GetEndPosY() const { return m_endPos.y(); }
      inline Double_t          GetEndPosZ() const { return m_endPos.z(); }
      inline Double_t          GetTime()    const { return m_time;       }
      inline Double_t          GetEnergy()  const { return m_energy;     }
      inline Double_t          GetEndTime() const { return m_endTime;    }

    private:
      Int_t    m_userId;
      Int_t    m_pid;
      Int_t    m_track;
      Int_t    m_mother;
      TVector3 m_v;
      TVector3 m_p3;
      TVector3 m_endPos;
      Double_t m_mass;
      Double_t m_time;
      Double_t m_energy;
      Double_t m_endTime;

   };
}
#endif /* LcHEPEvtParticle.h guard. */
