#ifndef LCLIB_LCRECDATA_LCRECDATA_H
#define LCLIB_LCRECDATA_LCRECDATA_H

#include <string>

#include "TObject.h"
#include "TROOT.h"
#include "TChain.h"

namespace LcLib
{

   class LcRecData : public TObject 
   {
    public :
      /// constructor ///
      LcRecData( TTree* tr = NULL );

      /// destructor ///
      virtual ~LcRecData();

      /// method ///
      void SetBranchAddresses( TTree *tr );
      
      /// getter ///
      bool IsMC() const { return m_isMC; }

      /// tree ///
    private :
      TTree          *m_chain;  
  
    public :
      // Declaration of leaf types
      Int_t           RunID;
      Int_t           NodeID;
      Int_t           FileID;
      Int_t           DstEntryID;
      Int_t           ClusteringEntryID;
      Int_t           userFlag;

      /// only for data ///
      Short_t         SpillID;
      Int_t           EventID;
      UInt_t          DetectorBit;
      UInt_t          ScaledTrigBit;
      UInt_t          RawTrigBit;
      Int_t           TimeStamp;
      UInt_t          L2TimeStamp;
      Short_t         Error;
      Short_t         TrigTagMismatch;
      Short_t         L2AR;
      Short_t         COE_Et_overflow;
      Short_t         COE_L2_override;
      Int_t           COE_Esum;
      Int_t           COE_Ex;
      Int_t           COE_Ey;
      Bool_t          isGoodRun;
      Bool_t          isGoodSpill;

      /// only for MC ///
      Int_t           TruePID;
      Int_t           TrueDecayMode;
      Double_t        TrueVertexZ;
      Double_t        TrueVertexTime;
      Double_t        TrueMom[3];
      Double_t        TruePos[3];
      Int_t           GsimGenEventID;
      Int_t           GsimEntryID;
      Int_t           MCEventID;
      std::string    *AccidentalFileName;
      Int_t           AccidentalFileID;
      Long64_t        AccidentalEntryID;
      Bool_t          AccidentalOverlayFlag;
      Int_t           GenParticleNumber;
      Int_t           GenParticleTrack[100];   //[GenParticleNumber]
      Int_t           GenParticleMother[100];   //[GenParticleNumber]
      Int_t           GenParticlePid[100];   //[GenParticleNumber]
      Double_t        GenParticleMom[100][3];   //[GenParticleNumber]
      Double_t        GenParticleEk[100];   //[GenParticleNumber]
      Double_t        GenParticleMass[100];   //[GenParticleNumber]
      Double_t        GenParticleTime[100];   //[GenParticleNumber]
      Double_t        GenParticlePos[100][3];   //[GenParticleNumber]
      Double_t        GenParticleEndMom[100][3];   //[GenParticleNumber]
      Double_t        GenParticleEndEk[100];   //[GenParticleNumber]
      Double_t        GenParticleEndTime[100];   //[GenParticleNumber]
      Double_t        GenParticleEndPos[100][3];   //[GenParticleNumber]

      /// common ///
      Int_t           CDTNum;
      Int_t           eventID;
      Int_t           OrigEventID;
      Int_t           CutCondition;
      Int_t           VetoCondition;
      Int_t           GamClusNumber;
      Int_t           GamClusId[6];   //[GamClusNumber]
      Int_t           GamClusStatus[6];   //[GamClusNumber]
      Double_t        GamClusThreshold[6];   //[GamClusNumber]
      Double_t        GamClusDepE[6];   //[GamClusNumber]
      Double_t        GamClusCoePos[6][3];   //[GamClusNumber]
      Double_t        GamClusTime[6];   //[GamClusNumber]
      Double_t        GamClusRMS[6];   //[GamClusNumber]
      Int_t           GamClusSize[6];   //[GamClusNumber]
      Int_t           GamClusCsiId[6][120];   //[GamClusNumber]
      Double_t        GamClusCsiE[6][120];   //[GamClusNumber]
      Double_t        GamClusCsiTime[6][120];   //[GamClusNumber]
      Int_t           GammaNumber;
      Int_t           GammaId[18];   //[GammaNumber]
      Int_t           GammaStatus[18];   //[GammaNumber]
      Double_t        GammaE[18];   //[GammaNumber]
      Double_t        GammaPos[18][3];   //[GammaNumber]
      Double_t        GammaTime[18];   //[GammaNumber]
      Double_t        GammaMom[18][3];   //[GammaNumber]
      Double_t        GammaSigmaE[18];   //[GammaNumber]
      Double_t        GammaSigmaPos[18][3];   //[GammaNumber]
      Double_t        GammaChi2[18];   //[GammaNumber]
      Double_t        GammaAnn[18];   //[GammaNumber]
      Int_t           Gamma_clusIndex[18];   //[GammaNumber]
      Int_t           Pi0Number;
      Int_t           Pi0Id[9];   //[Pi0Number]
      Int_t           Pi0Status[9];   //[Pi0Number]
      Double_t        Pi0E[9];   //[Pi0Number]
      Double_t        Pi0Pos[9][3];   //[Pi0Number]
      Double_t        Pi0Mom[9][3];   //[Pi0Number]
      Double_t        Pi0Pt[9];   //[Pi0Number]
      Double_t        Pi0Mass[9];   //[Pi0Number]
      Double_t        Pi0RecZ[9];   //[Pi0Number]
      Double_t        Pi0RecZsig2[9];   //[Pi0Number]
      Int_t           Pi0_gamIndex[9][2];   //[Pi0Number]
      Int_t           KlongNumber;
      Int_t           KlongId[3];   //[KlongNumber]
      Int_t           KlongStatus[3];   //[KlongNumber]
      Double_t        KlongE[3];   //[KlongNumber]
      Double_t        KlongPos[3][3];   //[KlongNumber]
      Double_t        KlongMom[3][3];   //[KlongNumber]
      Double_t        KlongPt[3];   //[KlongNumber]
      Double_t        KlongMass[3];   //[KlongNumber]
      Double_t        KlongDeltaZ[3];   //[KlongNumber]
      Double_t        KlongChisqZ[3];   //[KlongNumber]
      Int_t           KlongVertFlag[3];   //[KlongNumber]
      Int_t           KlongSortFlag[3];   //[KlongNumber]
      Int_t           Klong_piIndex[3][3];   //[KlongNumber]
      Int_t           Klong_gamNumber;
      Int_t           Klong_gamIndex[3][100];   //[KlongNumber]
      Double_t        CSIEt;
      Double_t        EventStartTime;
      Double_t        EventStartZ;
      Double_t        VertexTime[6];
      Int_t           OriginalClusterNumber;
      Double_t        OriginalClusterE[20];   //[OriginalClusterNumber]
      Double_t        OriginalClusterT[20];   //[OriginalClusterNumber]
      Double_t        OriginalClusterVertexT[20];   //[OriginalClusterNumber]
      Int_t           MyCutCondition;
      Int_t           MyVetoCondition;
      Double_t        DeltaVertexTime;
      Double_t        MaxDeltaPi0Mass;
      Double_t        TotalEt;
      Double_t        MinGammaE;
      Double_t        MaxGammaE;
      Double_t        MaxFiducialR;
      Double_t        MinFiducialXY;
      Double_t        MinClusterDistance;
      Double_t        KLDeltaChisqZ;
      Double_t        MaxShapeChisq;

      /// only for g2ana ///
      Int_t           AddCutCondition;
      Double_t        ERatio;
      Double_t        EAsymmetry;
      Double_t        ProjectionAngle;
      Double_t        MinETheta;
      Double_t        CSICoEX;
      Double_t        CSICoEY;
      Double_t        InterpolatedDistance;
      Double_t        IntersectedSquare;
      Double_t        MinClusRMS;
      Double_t        MinDeadChR;

      /// common ///
      Double_t        ExtraClusterDeltaVertexTime;
      Double_t        ExtraClusterEne;
      Double_t        KLBeamExitX;
      Double_t        KLBeamExitY;
      Double_t        AverageClusterTime;

      /// veto ///
      Int_t           CC03ModuleNumber;
      Int_t           CC03ModuleModID[32];   //[CC03ModuleNumber]
      Float_t         CC03ModuleEne[32];   //[CC03ModuleNumber]
      Float_t         CC03ModuleTime[32];   //[CC03ModuleNumber]
      Int_t           CC03VetoModID;
      Float_t         CC03VetoEne;
      Float_t         CC03VetoTime;
      Float_t         CC03TotalVetoEne;
      Int_t           CC04E391ModuleNumber;
      Int_t           CC04E391ModuleModID[42];   //[CC04E391ModuleNumber]
      Float_t         CC04E391ModuleEne[42];   //[CC04E391ModuleNumber]
      Float_t         CC04E391ModuleTime[42];   //[CC04E391ModuleNumber]
      Int_t           CC04KTeVModuleNumber;
      Int_t           CC04KTeVModuleModID[16];   //[CC04KTeVModuleNumber]
      Float_t         CC04KTeVModuleEne[16];   //[CC04KTeVModuleNumber]
      Float_t         CC04KTeVModuleTime[16];   //[CC04KTeVModuleNumber]
      Int_t           CC04ScintiModuleNumber;
      Int_t           CC04ScintiModuleModID[6];   //[CC04ScintiModuleNumber]
      Float_t         CC04ScintiModuleEne[6];   //[CC04ScintiModuleNumber]
      Float_t         CC04ScintiModuleTime[6];   //[CC04ScintiModuleNumber]
      Int_t           CC04E391VetoModID;
      Float_t         CC04E391VetoEne;
      Float_t         CC04E391VetoTime;
      Int_t           CC04KTeVVetoModID;
      Float_t         CC04KTeVVetoEne;
      Float_t         CC04KTeVVetoTime;
      Int_t           CC04ScintiVetoModID;
      Float_t         CC04ScintiVetoEne;
      Float_t         CC04ScintiVetoTime;
      Int_t           CC05E391ModuleNumber;
      Int_t           CC05E391ModuleModID[64];   //[CC05E391ModuleNumber]
      Float_t         CC05E391ModuleEne[64];   //[CC05E391ModuleNumber]
      Float_t         CC05E391ModuleTime[64];   //[CC05E391ModuleNumber]
      Int_t           CC05ScintiModuleNumber;
      Int_t           CC05ScintiModuleModID[6];   //[CC05ScintiModuleNumber]
      Float_t         CC05ScintiModuleEne[6];   //[CC05ScintiModuleNumber]
      Float_t         CC05ScintiModuleTime[6];   //[CC05ScintiModuleNumber]
      Int_t           CC05E391VetoModID;
      Float_t         CC05E391VetoEne;
      Float_t         CC05E391VetoTime;
      Int_t           CC05ScintiVetoModID;
      Float_t         CC05ScintiVetoEne;
      Float_t         CC05ScintiVetoTime;
      Int_t           CC06E391ModuleNumber;
      Int_t           CC06E391ModuleModID[64];   //[CC06E391ModuleNumber]
      Float_t         CC06E391ModuleEne[64];   //[CC06E391ModuleNumber]
      Float_t         CC06E391ModuleTime[64];   //[CC06E391ModuleNumber]
      Int_t           CC06ScintiModuleNumber;
      Int_t           CC06ScintiModuleModID[6];   //[CC06ScintiModuleNumber]
      Float_t         CC06ScintiModuleEne[6];   //[CC06ScintiModuleNumber]
      Float_t         CC06ScintiModuleTime[6];   //[CC06ScintiModuleNumber]
      Int_t           CC06E391VetoModID;
      Float_t         CC06E391VetoEne;
      Float_t         CC06E391VetoTime;
      Int_t           CC06ScintiVetoModID;
      Float_t         CC06ScintiVetoEne;
      Float_t         CC06ScintiVetoTime;
      Int_t           CC06FTTNumber;
      Int_t           CC06FTTModID[64];   //[CC06FTTNumber]
      Float_t         CC06FTTChisq[64];   //[CC06FTTNumber]
      Float_t         CC06FTTTime[64];   //[CC06FTTNumber]
      Int_t           CBARVetoModID;
      Float_t         CBARVetoEne;
      Float_t         CBARVetoTime;
      Float_t         CBARVetoHitZ;
      Float_t         CBARTotalVetoEne;
      Int_t           CBARInnerVetoModID;
      Float_t         CBARInnerVetoEne;
      Float_t         CBARInnerVetoTime;
      Float_t         CBARInnerVetoHitZ;
      Int_t           CBAROuterVetoModID;
      Float_t         CBAROuterVetoEne;
      Float_t         CBAROuterVetoTime;
      Float_t         CBAROuterVetoHitZ;
      Int_t           CBARModuleNumber;
      Int_t           CBARModuleModID[64];   //[CBARModuleNumber]
      Float_t         CBARModuleHitTime[64];   //[CBARModuleNumber]
      Float_t         CBARModuleEne[64];   //[CBARModuleNumber]
      Float_t         CBARModuleHitZ[64];   //[CBARModuleNumber]
      Float_t         CBARModuleDeltaTime[64];   //[CBARModuleNumber]
      Int_t           CBARFTTNumber;
      Int_t           CBARFTTModID[128];   //[CBARFTTNumber]
      Float_t         CBARFTTChisq[128];   //[CBARFTTNumber]
      Float_t         CBARFTTTime[128];   //[CBARFTTNumber]
      Int_t           FBARModuleNumber;
      Int_t           FBARModuleModID[32];   //[FBARModuleNumber]
      Float_t         FBARModuleEne[32];   //[FBARModuleNumber]
      Float_t         FBARModuleTime[32];   //[FBARModuleNumber]
      Int_t           FBARVetoModID;
      Float_t         FBARVetoEne;
      Float_t         FBARVetoTime;
      Int_t           FBARFTTNumber;
      Int_t           FBARFTTModID[32];   //[FBARFTTNumber]
      Float_t         FBARFTTChisq[32];   //[FBARFTTNumber]
      Float_t         FBARFTTTime[32];   //[FBARFTTNumber]
      Int_t           CSIVetoNumber;
      Int_t           CSIVetoModID[2716];   //[CSIVetoNumber]
      Double_t        CSIVetoEne[2716];   //[CSIVetoNumber]
      Double_t        CSIVetoDistance[2716];   //[CSIVetoNumber]
      Double_t        CSIVetoDeltaTime[2716];   //[CSIVetoNumber]
      Int_t           BHPVNumber;
      Int_t           BHPVModID[34];   //[BHPVNumber]
      Short_t         BHPVnHits[34];   //[BHPVNumber]
      Float_t         BHPVEne[34][20];   //[BHPVNumber]
      Float_t         BHPVTime[34][20];   //[BHPVNumber]
      Int_t           BHPVVetoModID;
      Int_t           BHPVVetonHitMod;
      Float_t         BHPVVetoEne;
      Float_t         BHPVVetoTime;
      Int_t           BHPVCoinNumber;
      Int_t           BHPVCoinnHitMod[32];   //[BHPVCoinNumber]
      Int_t           BHPVCoinModID[32];   //[BHPVCoinNumber]
      Float_t         BHPVCoinEne[32];   //[BHPVCoinNumber]
      Float_t         BHPVCoinTime[32];   //[BHPVCoinNumber]
      Float_t         BHPVCoinTimeSpread[32];   //[BHPVCoinNumber]
      Int_t           CVVetoModID;
      Float_t         CVVetoEne;
      Float_t         CVVetoTime;
      Float_t         CVTotalVetoEne;
      Int_t           CVFrontVetoModID;
      Float_t         CVFrontVetoEne;
      Float_t         CVFrontVetoTime;
      Float_t         CVFrontTotalVetoEne;
      Int_t           CVRearVetoModID;
      Float_t         CVRearVetoEne;
      Float_t         CVRearVetoTime;
      Float_t         CVRearTotalVetoEne;
      Float_t         CVMaxPulseWidth;
      Int_t           CVModuleNumber;
      Int_t           CVModuleModID[88];   //[CVModuleNumber]
      Float_t         CVModuleHitTime[88];   //[CVModuleNumber]
      Float_t         CVModuleEne[88];   //[CVModuleNumber]
      Float_t         CVModuleDeltaTime[88];   //[CVModuleNumber]
      Float_t         CVModuleRiseTime[88][2];   //[CVModuleNumber]
      Int_t           CVFTTNumber;
      Int_t           CVFTTModID[188];   //[CVFTTNumber]
      Float_t         CVFTTChisq[188];   //[CVFTTNumber]
      Float_t         CVFTTTime[188];   //[CVFTTNumber]
      Int_t           NCCModuleNumber;
      Int_t           NCCModuleModID[240];   //[NCCModuleNumber]
      Float_t         NCCModuleEne[240];   //[NCCModuleNumber]
      Float_t         NCCModuleTime[240];   //[NCCModuleNumber]
      Int_t           NCCScintiModuleNumber;
      Int_t           NCCScintiModuleModID[4];   //[NCCScintiModuleNumber]
      Float_t         NCCScintiModuleEne[4];   //[NCCScintiModuleNumber]
      Float_t         NCCScintiModuleTime[4];   //[NCCScintiModuleNumber]
      Int_t           NCCVetoModID;
      Float_t         NCCVetoEne;
      Float_t         NCCVetoTime;
      Float_t         NCCTotalVetoEne;
      Int_t           NCCScintiVetoModID;
      Float_t         NCCScintiVetoEne;
      Float_t         NCCScintiVetoTime;
      Int_t           NCCFTTNumber;
      Int_t           NCCFTTModID[64];   //[NCCFTTNumber]
      Float_t         NCCFTTChisq[64];   //[NCCFTTNumber]
      Float_t         NCCFTTTime[64];   //[NCCFTTNumber]
      Int_t           OEVModuleNumber;
      Int_t           OEVModuleModID[50];   //[OEVModuleNumber]
      Float_t         OEVModuleEne[50];   //[OEVModuleNumber]
      Float_t         OEVModuleTime[50];   //[OEVModuleNumber]
      Int_t           OEVVetoModID;
      Float_t         OEVVetoEne;
      Float_t         OEVVetoTime;
      Int_t           LCVModuleNumber;
      Int_t           LCVModuleModID[4];   //[LCVModuleNumber]
      Float_t         LCVModuleEne[4];   //[LCVModuleNumber]
      Float_t         LCVModuleTime[4];   //[LCVModuleNumber]
      Int_t           LCVVetoModID;
      Float_t         LCVVetoEne;
      Float_t         LCVVetoTime;
      Int_t           BPCVModuleNumber;
      Int_t           BPCVModuleModID[4];   //[BPCVModuleNumber]
      Float_t         BPCVModuleEne[4];   //[BPCVModuleNumber]
      Float_t         BPCVModuleTime[4];   //[BPCVModuleNumber]
      Int_t           BPCVVetoModID;
      Float_t         BPCVVetoEne;
      Float_t         BPCVVetoTime;
      Int_t           newBHCVNumber;
      Int_t           newBHCVModID[48];   //[newBHCVNumber]
      Short_t         newBHCVnHits[48];   //[newBHCVNumber]
      Float_t         newBHCVEne[48][20];   //[newBHCVNumber]
      Float_t         newBHCVTime[48][20];   //[newBHCVNumber]
      Int_t           newBHCVVetoModID;
      Float_t         newBHCVVetoEne;
      Float_t         newBHCVVetoTime;
      Int_t           newBHCVHitCount;
      Int_t           newBHCVModHitCount;
      Int_t           BHGCNumber;
      Int_t           BHGCModID[8];   //[BHGCNumber]
      Short_t         BHGCnHits[8];   //[BHGCNumber]
      Float_t         BHGCEne[8][20];   //[BHGCNumber]
      Float_t         BHGCTime[8][20];   //[BHGCNumber]
      Int_t           BHGCVetoModID;
      Float_t         BHGCVetoEne;
      Float_t         BHGCVetoTime;
      Int_t           IBNumber;
      Int_t           IBModID[64];   //[IBNumber]
      Short_t         IBnHits[64];   //[IBNumber]
      Float_t         IBEne[64][20];   //[IBNumber]
      Float_t         IBTime[64][20];   //[IBNumber]
      Int_t           IBModuleNumber;
      Int_t           IBModuleModID[32];   //[IBModuleNumber]
      Float_t         IBModuleHitTime[32];   //[IBModuleNumber]
      Float_t         IBModuleEne[32];   //[IBModuleNumber]
      Float_t         IBModuleHitZ[32];   //[IBModuleNumber]
      Float_t         IBModuleDeltaTime[32];   //[IBModuleNumber]
      Int_t           IBVetoModID;
      Float_t         IBVetoEne;
      Float_t         IBVetoTime;
      Int_t           IBWideVetoModID;
      Float_t         IBWideVetoEne;
      Float_t         IBWideVetoTime;
      Float_t         IBCH55VetoEne;
      Float_t         IBCH55VetoTime;
      Int_t           IBFTTNumber;
      Int_t           IBFTTModID[64];   //[IBFTTNumber]
      Float_t         IBFTTChisq[64];   //[IBFTTNumber]
      Float_t         IBFTTTime[64];   //[IBFTTNumber]
      Int_t           IBCVModuleNumber;
      Int_t           IBCVModuleModID[32];   //[IBCVModuleNumber]
      Float_t         IBCVModuleEne[32];   //[IBCVModuleNumber]
      Float_t         IBCVModuleHitTime[32];   //[IBCVModuleNumber]
      Float_t         IBCVModuleDeltaTime[32];   //[IBCVModuleNumber]
      Int_t           IBCVVetoModID;
      Float_t         IBCVVetoEne;
      Float_t         IBCVVetoTime;
      Float_t         IBCVCH27VetoEne;
      Float_t         IBCVCH27VetoTime;
      Int_t           MBCVModuleNumber;
      Int_t           MBCVModuleModID[16];   //[MBCVModuleNumber]
      Float_t         MBCVModuleEne[16];   //[MBCVModuleNumber]
      Float_t         MBCVModuleTime[16];   //[MBCVModuleNumber]
      Int_t           MBCVVetoModID;
      Float_t         MBCVVetoEne;
      Float_t         MBCVVetoTime;
      Float_t         MBCVCH2VetoEne;
      Float_t         MBCVCH2VetoTime;

      // NewVeto //
      Bool_t          isFitSigmaCSIVeto;
      Bool_t          isTightFitSigmaCSIVeto;
      Int_t           TightCSIVetoMinDistModID;
      Int_t           TightCSIVetoMaxEneModID;
      Int_t           FBARProperModID;
      Float_t         FBARProperTime;
      Float_t         FBARProperEne;
      Float_t         FBARProperFTTChisq;
      Int_t           FBARMaskingProperModID;
      Float_t         FBARMaskingProperTime;
      Float_t         FBARMaskingProperEne;
      Float_t         FBARMaskingProperFTTChisq;
      Int_t           NCCProperModID;
      Float_t         NCCProperTime;
      Float_t         NCCProperEne;
      Float_t         NCCProperFTTChisq;
      Int_t           NCCMaskingProperModID;
      Float_t         NCCMaskingProperTime;
      Float_t         NCCMaskingProperEne;
      Float_t         NCCMaskingProperFTTChisq;
      Int_t           CBARProperModID;
      Float_t         CBARProperTime;
      Float_t         CBARProperEne;
      Float_t         CBARProperFTTChisq;
      Int_t           CBARMaskingProperModID;
      Float_t         CBARMaskingProperTime;
      Float_t         CBARMaskingProperEne;
      Float_t         CBARMaskingProperFTTChisq;
      Int_t           IBProperModID;
      Float_t         IBProperTime;
      Float_t         IBProperEne;
      Float_t         IBProperFTTChisq;
      Int_t           IBMaskingProperModID;
      Float_t         IBMaskingProperTime;
      Float_t         IBMaskingProperEne;
      Float_t         IBMaskingProperFTTChisq;
      Int_t           CVProperModID;
      Float_t         CVProperTime;
      Float_t         CVProperEne;
      Float_t         CVProperFTTChisq;
      Int_t           CVMaskingProperModID;
      Float_t         CVMaskingProperTime;
      Float_t         CVMaskingProperEne;
      Float_t         CVMaskingProperFTTChisq;
      Int_t           CC06ProperModID;
      Float_t         CC06ProperTime;
      Float_t         CC06ProperEne;
      Float_t         CC06ProperFTTChisq;
      Int_t           CC06MaskingProperModID;
      Float_t         CC06MaskingProperTime;
      Float_t         CC06MaskingProperEne;
      Float_t         CC06MaskingProperFTTChisq;
      UInt_t          NewVetoCondition;

      // List of branches
      TBranch        *b_RunID;   //!
      TBranch        *b_NodeID;   //!
      TBranch        *b_FileID;   //!
      TBranch        *b_DstEntryID;   //!
      TBranch        *b_ClusteringEntryID;   //!
      TBranch        *b_userFlag;   //!
      TBranch        *b_SpillID;   //!
      TBranch        *b_EventID;   //!
      TBranch        *b_DetectorBit;   //!
      TBranch        *b_ScaledTrigBit;   //!
      TBranch        *b_RawTrigBit;   //!
      TBranch        *b_TimeStamp;   //!
      TBranch        *b_L2TimeStamp;   //!
      TBranch        *b_Error;   //!
      TBranch        *b_TrigTagMismatch;   //!
      TBranch        *b_L2AR;   //!
      TBranch        *b_COE_Et_overflow;   //!
      TBranch        *b_COE_L2_override;   //!
      TBranch        *b_COE_Esum;   //!
      TBranch        *b_COE_Ex;   //!
      TBranch        *b_COE_Ey;   //!
      TBranch        *b_isGoodRun;   //!
      TBranch        *b_isGoodSpill;   //!
      TBranch        *b_CDTNum;   //!
      TBranch        *b_eventID;   //!
      TBranch        *b_OrigEventID;   //!
      TBranch        *b_CutCondition;   //!
      TBranch        *b_VetoCondition;   //!
      TBranch        *b_GamClusNumber;   //!
      TBranch        *b_GamClusId;   //!
      TBranch        *b_GamClusStatus;   //!
      TBranch        *b_GamClusThreshold;   //!
      TBranch        *b_GamClusDepE;   //!
      TBranch        *b_GamClusCoePos;   //!
      TBranch        *b_GamClusTime;   //!
      TBranch        *b_GamClusRMS;   //!
      TBranch        *b_GamClusSize;   //!
      TBranch        *b_GamClusCsiId;   //!
      TBranch        *b_GamClusCsiE;   //!
      TBranch        *b_GamClusCsiTime;   //!
      TBranch        *b_GammaNumber;   //!
      TBranch        *b_GammaId;   //!
      TBranch        *b_GammaStatus;   //!
      TBranch        *b_GammaE;   //!
      TBranch        *b_GammaPos;   //!
      TBranch        *b_GammaTime;   //!
      TBranch        *b_GammaMom;   //!
      TBranch        *b_GammaSigmaE;   //!
      TBranch        *b_GammaSigmaPos;   //!
      TBranch        *b_GammaChi2;   //!
      TBranch        *b_GammaAnn;   //!
      TBranch        *b_Gamma_clusIndex;   //!
      TBranch        *b_Pi0Number;   //!
      TBranch        *b_Pi0Id;   //!
      TBranch        *b_Pi0Status;   //!
      TBranch        *b_Pi0E;   //!
      TBranch        *b_Pi0Pos;   //!
      TBranch        *b_Pi0Mom;   //!
      TBranch        *b_Pi0Pt;   //!
      TBranch        *b_Pi0Mass;   //!
      TBranch        *b_Pi0RecZ;   //!
      TBranch        *b_Pi0RecZsig2;   //!
      TBranch        *b_Pi0_gamIndex;   //!
      TBranch        *b_KlongNumber;   //!
      TBranch        *b_KlongId;   //!
      TBranch        *b_KlongStatus;   //!
      TBranch        *b_KlongE;   //!
      TBranch        *b_KlongPos;   //!
      TBranch        *b_KlongMom;   //!
      TBranch        *b_KlongPt;   //!
      TBranch        *b_KlongMass;   //!
      TBranch        *b_KlongDeltaZ;   //!
      TBranch        *b_KlongChisqZ;   //!
      TBranch        *b_KlongVertFlag;   //!
      TBranch        *b_KlongSortFlag;   //!
      TBranch        *b_Klong_piIndex;   //!
      TBranch        *b_Klong_gamNumber;   //!
      TBranch        *b_Klong_gamIndex;   //!
      TBranch        *b_CSIEt;   //!
      TBranch        *b_CSIHalfEtR;   //!
      TBranch        *b_CSIHalfEtL;   //!
      TBranch        *b_EventStartTime;   //!
      TBranch        *b_EventStartZ;   //!
      TBranch        *b_VertexTime;   //!
      TBranch        *b_OriginalClusterNumber;   //!
      TBranch        *b_OriginalClusterE;   //!
      TBranch        *b_OriginalClusterT;   //!
      TBranch        *b_OriginalClusterVertexT;   //!
      TBranch        *b_MyCutCondition;   //!
      TBranch        *b_MyVetoCondition;   //!
      TBranch        *b_DeltaVertexTime;   //!
      TBranch        *b_MaxDeltaPi0Mass;   //!
      TBranch        *b_MinHalfEt;   //!
      TBranch        *b_TotalEt;   //!
      TBranch        *b_MinGammaE;   //!
      TBranch        *b_MaxGammaE;   //!
      TBranch        *b_MaxFiducialR;   //!
      TBranch        *b_MinFiducialXY;   //!
      TBranch        *b_MinClusterDistance;   //!
      TBranch        *b_KLDeltaChisqZ;   //!
      TBranch        *b_MaxShapeChisq;   //!
      TBranch        *b_ERatio;   //!
      TBranch        *b_EAsymmetry;   //!
      TBranch        *b_ProjectionAngle;   //!
      TBranch        *b_MinETheta;   //!
      TBranch        *b_CSICoEX;   //!
      TBranch        *b_CSICoEY;   //!
      TBranch        *b_InterpolatedDistance;   //!
      TBranch        *b_IntersectedSquare;   //!
      TBranch        *b_MinClusRMS;   //!
      TBranch        *b_MinDeadChR;   //!
      TBranch        *b_ExtraClusterDeltaVertexTime;   //!
      TBranch        *b_ExtraClusterEne;   //!
      TBranch        *b_KLBeamExitX;   //!
      TBranch        *b_KLBeamExitY;   //!
      TBranch        *b_AverageClusterTime;   //!
      TBranch        *b_TruePID;   //!
      TBranch        *b_TrueDecayMode;   //!
      TBranch        *b_TrueVertexZ;   //!
      TBranch        *b_TrueVertexTime;   //!
      TBranch        *b_TrueMom;   //!
      TBranch        *b_TruePos;   //!
      TBranch        *b_GsimGenEventID;   //!
      TBranch        *b_GsimEntryID;   //!
      TBranch        *b_MCEventID;   //!
      TBranch        *b_AccidentalFileName;   //!
      TBranch        *b_AccidentalFileID;   //!
      TBranch        *b_AccidentalEntryID;   //!
      TBranch        *b_AccidentalOverlayFlag;   //!
      TBranch        *b_GenParticleNumber;   //!
      TBranch        *b_GenParticleTrack;   //!
      TBranch        *b_GenParticleMother;   //!
      TBranch        *b_GenParticlePid;   //!
      TBranch        *b_GenParticleMom;   //!
      TBranch        *b_GenParticleEk;   //!
      TBranch        *b_GenParticleMass;   //!
      TBranch        *b_GenParticleTime;   //!
      TBranch        *b_GenParticlePos;   //!
      TBranch        *b_GenParticleEndMom;   //!
      TBranch        *b_GenParticleEndEk;   //!
      TBranch        *b_GenParticleEndTime;   //!
      TBranch        *b_GenParticleEndPos;   //!
      TBranch        *b_CC03ModuleNumber;   //!
      TBranch        *b_CC03ModuleModID;   //!
      TBranch        *b_CC03ModuleEne;   //!
      TBranch        *b_CC03ModuleTime;   //!
      TBranch        *b_CC03VetoModID;   //!
      TBranch        *b_CC03VetoEne;   //!
      TBranch        *b_CC03VetoTime;   //!
      TBranch        *b_CC03TotalVetoEne;   //!
      TBranch        *b_CC04E391ModuleNumber;   //!
      TBranch        *b_CC04E391ModuleModID;   //!
      TBranch        *b_CC04E391ModuleEne;   //!
      TBranch        *b_CC04E391ModuleTime;   //!
      TBranch        *b_CC04KTeVModuleNumber;   //!
      TBranch        *b_CC04KTeVModuleModID;   //!
      TBranch        *b_CC04KTeVModuleEne;   //!
      TBranch        *b_CC04KTeVModuleTime;   //!
      TBranch        *b_CC04ScintiModuleNumber;   //!
      TBranch        *b_CC04ScintiModuleModID;   //!
      TBranch        *b_CC04ScintiModuleEne;   //!
      TBranch        *b_CC04ScintiModuleTime;   //!
      TBranch        *b_CC04E391VetoModID;   //!
      TBranch        *b_CC04E391VetoEne;   //!
      TBranch        *b_CC04E391VetoTime;   //!
      TBranch        *b_CC04KTeVVetoModID;   //!
      TBranch        *b_CC04KTeVVetoEne;   //!
      TBranch        *b_CC04KTeVVetoTime;   //!
      TBranch        *b_CC04ScintiVetoModID;   //!
      TBranch        *b_CC04ScintiVetoEne;   //!
      TBranch        *b_CC04ScintiVetoTime;   //!
      TBranch        *b_CC05E391ModuleNumber;   //!
      TBranch        *b_CC05E391ModuleModID;   //!
      TBranch        *b_CC05E391ModuleEne;   //!
      TBranch        *b_CC05E391ModuleTime;   //!
      TBranch        *b_CC05ScintiModuleNumber;   //!
      TBranch        *b_CC05ScintiModuleModID;   //!
      TBranch        *b_CC05ScintiModuleEne;   //!
      TBranch        *b_CC05ScintiModuleTime;   //!
      TBranch        *b_CC05E391VetoModID;   //!
      TBranch        *b_CC05E391VetoEne;   //!
      TBranch        *b_CC05E391VetoTime;   //!
      TBranch        *b_CC05ScintiVetoModID;   //!
      TBranch        *b_CC05ScintiVetoEne;   //!
      TBranch        *b_CC05ScintiVetoTime;   //!
      TBranch        *b_CC06E391ModuleNumber;   //!
      TBranch        *b_CC06E391ModuleModID;   //!
      TBranch        *b_CC06E391ModuleEne;   //!
      TBranch        *b_CC06E391ModuleTime;   //!
      TBranch        *b_CC06ScintiModuleNumber;   //!
      TBranch        *b_CC06ScintiModuleModID;   //!
      TBranch        *b_CC06ScintiModuleEne;   //!
      TBranch        *b_CC06ScintiModuleTime;   //!
      TBranch        *b_CC06E391VetoModID;   //!
      TBranch        *b_CC06E391VetoEne;   //!
      TBranch        *b_CC06E391VetoTime;   //!
      TBranch        *b_CC06ScintiVetoModID;   //!
      TBranch        *b_CC06ScintiVetoEne;   //!
      TBranch        *b_CC06ScintiVetoTime;   //!
      TBranch        *b_CC06FTTNumber;   //!
      TBranch        *b_CC06FTTModID;   //!
      TBranch        *b_CC06FTTChisq;   //!
      TBranch        *b_CC06FTTTime;   //!
      TBranch        *b_CBARVetoModID;   //!
      TBranch        *b_CBARVetoEne;   //!
      TBranch        *b_CBARVetoTime;   //!
      TBranch        *b_CBARVetoHitZ;   //!
      TBranch        *b_CBARTotalVetoEne;   //!
      TBranch        *b_CBARInnerVetoModID;   //!
      TBranch        *b_CBARInnerVetoEne;   //!
      TBranch        *b_CBARInnerVetoTime;   //!
      TBranch        *b_CBARInnerVetoHitZ;   //!
      TBranch        *b_CBAROuterVetoModID;   //!
      TBranch        *b_CBAROuterVetoEne;   //!
      TBranch        *b_CBAROuterVetoTime;   //!
      TBranch        *b_CBAROuterVetoHitZ;   //!
      TBranch        *b_CBARFTTNumber;   //!
      TBranch        *b_CBARFTTModID;   //!
      TBranch        *b_CBARFTTChisq;   //!
      TBranch        *b_CBARFTTTime;   //!
      TBranch        *b_FBARModuleNumber;   //!
      TBranch        *b_FBARModuleModID;   //!
      TBranch        *b_FBARModuleEne;   //!
      TBranch        *b_FBARModuleTime;   //!
      TBranch        *b_FBARVetoModID;   //!
      TBranch        *b_FBARVetoEne;   //!
      TBranch        *b_FBARVetoTime;   //!
      TBranch        *b_FBARFTTNumber;   //!
      TBranch        *b_FBARFTTModID;   //!
      TBranch        *b_FBARFTTChisq;   //!
      TBranch        *b_FBARFTTTime;   //!
      TBranch        *b_CSIVetoNumber;   //!
      TBranch        *b_CSIVetoModID;   //!
      TBranch        *b_CSIVetoEne;   //!
      TBranch        *b_CSIVetoDistance;   //!
      TBranch        *b_CSIVetoDeltaTime;   //!
      TBranch        *b_BHPVNumber;   //!
      TBranch        *b_BHPVModID;   //!
      TBranch        *b_BHPVnHits;   //!
      TBranch        *b_BHPVEne;   //!
      TBranch        *b_BHPVTime;   //!
      TBranch        *b_BHPVVetoModID;   //!
      TBranch        *b_BHPVVetonHitMod;   //!
      TBranch        *b_BHPVVetoEne;   //!
      TBranch        *b_BHPVVetoTime;   //!
      TBranch        *b_BHPVCoinNumber;   //!
      TBranch        *b_BHPVCoinnHitMod;   //!
      TBranch        *b_BHPVCoinModID;   //!
      TBranch        *b_BHPVCoinEne;   //!
      TBranch        *b_BHPVCoinTime;   //!
      TBranch        *b_BHPVCoinTimeSpread;   //!
      TBranch        *b_CVVetoModID;   //!
      TBranch        *b_CVVetoEne;   //!
      TBranch        *b_CVVetoTime;   //!
      TBranch        *b_CVTotalVetoEne;   //!
      TBranch        *b_CVFrontVetoModID;   //!
      TBranch        *b_CVFrontVetoEne;   //!
      TBranch        *b_CVFrontVetoTime;   //!
      TBranch        *b_CVFrontTotalVetoEne;   //!
      TBranch        *b_CVRearVetoModID;   //!
      TBranch        *b_CVRearVetoEne;   //!
      TBranch        *b_CVRearVetoTime;   //!
      TBranch        *b_CVRearTotalVetoEne;   //!
      TBranch        *b_CVMaxPulseWidth;   //!
      TBranch        *b_CVModuleNumber;   //!
      TBranch        *b_CVModuleModID;   //!
      TBranch        *b_CVModuleHitTime;   //!
      TBranch        *b_CVModuleEne;   //!
      TBranch        *b_CVModuleDeltaTime;   //!
      TBranch        *b_CVModuleRiseTime;   //!
      TBranch        *b_CVFTTNumber;   //!
      TBranch        *b_CVFTTModID;   //!
      TBranch        *b_CVFTTChisq;   //!
      TBranch        *b_CVFTTTime;   //!
      TBranch        *b_NCCModuleNumber;   //!
      TBranch        *b_NCCModuleModID;   //!
      TBranch        *b_NCCModuleEne;   //!
      TBranch        *b_NCCModuleTime;   //!
      TBranch        *b_NCCScintiModuleNumber;   //!
      TBranch        *b_NCCScintiModuleModID;   //!
      TBranch        *b_NCCScintiModuleEne;   //!
      TBranch        *b_NCCScintiModuleTime;   //!
      TBranch        *b_NCCVetoModID;   //!
      TBranch        *b_NCCVetoEne;   //!
      TBranch        *b_NCCVetoTime;   //!
      TBranch        *b_NCCTotalVetoEne;   //!
      TBranch        *b_NCCScintiVetoModID;   //!
      TBranch        *b_NCCScintiVetoEne;   //!
      TBranch        *b_NCCScintiVetoTime;   //!
      TBranch        *b_NCCFTTNumber;   //!
      TBranch        *b_NCCFTTModID;   //!
      TBranch        *b_NCCFTTChisq;   //!
      TBranch        *b_NCCFTTTime;   //!
      TBranch        *b_OEVModuleNumber;   //!
      TBranch        *b_OEVModuleModID;   //!
      TBranch        *b_OEVModuleEne;   //!
      TBranch        *b_OEVModuleTime;   //!
      TBranch        *b_OEVVetoModID;   //!
      TBranch        *b_OEVVetoEne;   //!
      TBranch        *b_OEVVetoTime;   //!
      TBranch        *b_LCVModuleNumber;   //!
      TBranch        *b_LCVModuleModID;   //!
      TBranch        *b_LCVModuleEne;   //!
      TBranch        *b_LCVModuleTime;   //!
      TBranch        *b_LCVVetoModID;   //!
      TBranch        *b_LCVVetoEne;   //!
      TBranch        *b_LCVVetoTime;   //!
      TBranch        *b_BPCVModuleNumber;   //!
      TBranch        *b_BPCVModuleModID;   //!
      TBranch        *b_BPCVModuleEne;   //!
      TBranch        *b_BPCVModuleTime;   //!
      TBranch        *b_BPCVVetoModID;   //!
      TBranch        *b_BPCVVetoEne;   //!
      TBranch        *b_BPCVVetoTime;   //!
      TBranch        *b_newBHCVNumber;   //!
      TBranch        *b_newBHCVModID;   //!
      TBranch        *b_newBHCVnHits;   //!
      TBranch        *b_newBHCVEne;   //!
      TBranch        *b_newBHCVTime;   //!
      TBranch        *b_newBHCVVetoModID;   //!
      TBranch        *b_newBHCVVetoEne;   //!
      TBranch        *b_newBHCVVetoTime;   //!
      TBranch        *b_newBHCVHitCount;   //!
      TBranch        *b_newBHCVModHitCount;   //!
      TBranch        *b_BHGCNumber;   //!
      TBranch        *b_BHGCModID;   //!
      TBranch        *b_BHGCnHits;   //!
      TBranch        *b_BHGCEne;   //!
      TBranch        *b_BHGCTime;   //!
      TBranch        *b_BHGCVetoModID;   //!
      TBranch        *b_BHGCVetoEne;   //!
      TBranch        *b_BHGCVetoTime;   //!
      TBranch        *b_IBNumber;   //!
      TBranch        *b_IBModID;   //!
      TBranch        *b_IBnHits;   //!
      TBranch        *b_IBEne;   //!
      TBranch        *b_IBTime;   //!
      TBranch        *b_IBModuleNumber;   //!
      TBranch        *b_IBModuleModID;   //!
      TBranch        *b_IBModuleHitTime;   //!
      TBranch        *b_IBModuleEne;   //!
      TBranch        *b_IBModuleHitZ;   //!
      TBranch        *b_IBModuleDeltaTime;   //!
      TBranch        *b_IBVetoModID;   //!
      TBranch        *b_IBVetoEne;   //!
      TBranch        *b_IBVetoTime;   //!
      TBranch        *b_IBWideVetoModID;   //!
      TBranch        *b_IBWideVetoEne;   //!
      TBranch        *b_IBWideVetoTime;   //!
      TBranch        *b_IBCH55VetoEne;   //!
      TBranch        *b_IBCH55VetoTime;   //!
      TBranch        *b_IBFTTNumber;   //!
      TBranch        *b_IBFTTModID;   //!
      TBranch        *b_IBFTTChisq;   //!
      TBranch        *b_IBFTTTime;   //!
      TBranch        *b_IBCVModuleNumber;   //!
      TBranch        *b_IBCVModuleModID;   //!
      TBranch        *b_IBCVModuleEne;   //!
      TBranch        *b_IBCVModuleHitTime;   //!
      TBranch        *b_IBCVModuleDeltaTime;   //!
      TBranch        *b_IBCVVetoModID;   //!
      TBranch        *b_IBCVVetoEne;   //!
      TBranch        *b_IBCVVetoTime;   //!
      TBranch        *b_IBCVCH27VetoEne;   //!
      TBranch        *b_IBCVCH27VetoTime;   //!
      TBranch        *b_MBCVModuleNumber;   //!
      TBranch        *b_MBCVModuleModID;   //!
      TBranch        *b_MBCVModuleEne;   //!
      TBranch        *b_MBCVModuleTime;   //!
      TBranch        *b_MBCVVetoModID;   //!
      TBranch        *b_MBCVVetoEne;   //!
      TBranch        *b_MBCVVetoTime;   //!
      TBranch        *b_MBCVCH2VetoEne;   //!
      TBranch        *b_MBCVCH2VetoTime;   //!   
      TBranch        *b_isFitSigmaCSIVeto;   //!
      TBranch        *b_isTightFitSigmaCSIVeto;   //!
      TBranch        *b_TightCSIVetoMinDistModID;   //!
      TBranch        *b_TightCSIVetoMaxEneModID;   //!
      TBranch        *b_FBARProperModID;   //!
      TBranch        *b_FBARProperTime;   //!
      TBranch        *b_FBARProperEne;   //!
      TBranch        *b_FBARProperFTTChisq;   //!
      TBranch        *b_FBARMaskingProperModID;   //!
      TBranch        *b_FBARMaskingProperTime;   //!
      TBranch        *b_FBARMaskingProperEne;   //!
      TBranch        *b_FBARMaskingProperFTTChisq;   //!
      TBranch        *b_NCCProperModID;   //!
      TBranch        *b_NCCProperTime;   //!
      TBranch        *b_NCCProperEne;   //!
      TBranch        *b_NCCProperFTTChisq;   //!
      TBranch        *b_NCCMaskingProperModID;   //!
      TBranch        *b_NCCMaskingProperTime;   //!
      TBranch        *b_NCCMaskingProperEne;   //!
      TBranch        *b_NCCMaskingProperFTTChisq;   //!
      TBranch        *b_CBARProperModID;   //!
      TBranch        *b_CBARProperTime;   //!
      TBranch        *b_CBARProperEne;   //!
      TBranch        *b_CBARProperFTTChisq;   //!
      TBranch        *b_CBARMaskingProperModID;   //!
      TBranch        *b_CBARMaskingProperTime;   //!
      TBranch        *b_CBARMaskingProperEne;   //!
      TBranch        *b_CBARMaskingProperFTTChisq;   //!
      TBranch        *b_IBProperModID;   //!
      TBranch        *b_IBProperTime;   //!
      TBranch        *b_IBProperEne;   //!
      TBranch        *b_IBProperFTTChisq;   //!
      TBranch        *b_IBMaskingProperModID;   //!
      TBranch        *b_IBMaskingProperTime;   //!
      TBranch        *b_IBMaskingProperEne;   //!
      TBranch        *b_IBMaskingProperFTTChisq;   //!
      TBranch        *b_CVProperModID;   //!
      TBranch        *b_CVProperTime;   //!
      TBranch        *b_CVProperEne;   //!
      TBranch        *b_CVProperFTTChisq;   //!
      TBranch        *b_CVMaskingProperModID;   //!
      TBranch        *b_CVMaskingProperTime;   //!
      TBranch        *b_CVMaskingProperEne;   //!
      TBranch        *b_CVMaskingProperFTTChisq;   //!
      TBranch        *b_CC06ProperModID;   //!
      TBranch        *b_CC06ProperTime;   //!
      TBranch        *b_CC06ProperEne;   //!
      TBranch        *b_CC06ProperFTTChisq;   //!
      TBranch        *b_CC06MaskingProperModID;   //!
      TBranch        *b_CC06MaskingProperTime;   //!
      TBranch        *b_CC06MaskingProperEne;   //!
      TBranch        *b_CC06MaskingProperFTTChisq;   //!
      TBranch        *b_NewVetoCondition;   //!

    private:
      bool m_isMC;

      ClassDef(LcRecData,1);
   };

} 
#endif /* guard of LcRecDataG6ana.h */ 
