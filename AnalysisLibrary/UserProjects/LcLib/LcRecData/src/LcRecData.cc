#include "LcRecData/LcRecData.h"

namespace LcLib
{
   LcRecData::LcRecData( TTree* tr )
      : m_chain(NULL),
        AccidentalFileName(NULL),
        m_isMC(false)
   {
      m_chain = tr;
      if( tr ) SetBranchAddresses(tr);
   }


   LcRecData::~LcRecData()
   {
      ;
   }
 
   void LcRecData::SetBranchAddresses( TTree *tr )
   {
      if ( !tr ) return;
      m_chain = tr;
   
      m_chain->SetBranchAddress("RunID", &RunID, &b_RunID);
      m_chain->SetBranchAddress("NodeID", &NodeID, &b_NodeID);
      m_chain->SetBranchAddress("FileID", &FileID, &b_FileID);
      m_chain->SetBranchAddress("DstEntryID", &DstEntryID, &b_DstEntryID);
      m_chain->SetBranchAddress("ClusteringEntryID", &ClusteringEntryID, &b_ClusteringEntryID);
      if( m_chain->GetBranch("userFlag") ){
	m_chain->SetBranchAddress("userFlag", &userFlag, &b_userFlag);
      }

      if( m_chain->GetBranch("SpillID") ){
         m_chain->SetBranchAddress("SpillID", &SpillID, &b_SpillID);
         m_chain->SetBranchAddress("EventID", &EventID, &b_EventID);
         m_chain->SetBranchAddress("DetectorBit", &DetectorBit, &b_DetectorBit);
         m_chain->SetBranchAddress("ScaledTrigBit", &ScaledTrigBit, &b_ScaledTrigBit);
         m_chain->SetBranchAddress("RawTrigBit", &RawTrigBit, &b_RawTrigBit);
         m_chain->SetBranchAddress("TimeStamp", &TimeStamp, &b_TimeStamp);
         m_chain->SetBranchAddress("L2TimeStamp", &L2TimeStamp, &b_L2TimeStamp);
         m_chain->SetBranchAddress("Error", &Error, &b_Error);
         m_chain->SetBranchAddress("TrigTagMismatch", &TrigTagMismatch, &b_TrigTagMismatch);
         m_chain->SetBranchAddress("L2AR", &L2AR, &b_L2AR);
         m_chain->SetBranchAddress("COE_Et_overflow", &COE_Et_overflow, &b_COE_Et_overflow);
         m_chain->SetBranchAddress("COE_L2_override", &COE_L2_override, &b_COE_L2_override);
         m_chain->SetBranchAddress("COE_Esum", &COE_Esum, &b_COE_Esum);
         m_chain->SetBranchAddress("COE_Ex", &COE_Ex, &b_COE_Ex);
         m_chain->SetBranchAddress("COE_Ey", &COE_Ey, &b_COE_Ey);
         m_chain->SetBranchAddress("isGoodRun", &isGoodRun, &b_isGoodRun);
         m_chain->SetBranchAddress("isGoodSpill", &isGoodSpill, &b_isGoodSpill);
      }

      if( m_chain->GetBranch("TruePID") ){
         m_isMC = true;

         m_chain->SetBranchAddress("TruePID", &TruePID, &b_TruePID);
         m_chain->SetBranchAddress("TrueDecayMode", &TrueDecayMode, &b_TrueDecayMode);
         m_chain->SetBranchAddress("TrueVertexZ", &TrueVertexZ, &b_TrueVertexZ);
         m_chain->SetBranchAddress("TrueVertexTime", &TrueVertexTime, &b_TrueVertexTime);
         m_chain->SetBranchAddress("TrueMom", TrueMom, &b_TrueMom);
         m_chain->SetBranchAddress("TruePos", TruePos, &b_TruePos);
         m_chain->SetBranchAddress("GsimGenEventID", &GsimGenEventID, &b_GsimGenEventID);
         m_chain->SetBranchAddress("GsimEntryID", &GsimEntryID, &b_GsimEntryID);
         m_chain->SetBranchAddress("MCEventID", &MCEventID, &b_MCEventID);
         m_chain->SetBranchAddress("AccidentalFileName", &AccidentalFileName, &b_AccidentalFileName);
         m_chain->SetBranchAddress("AccidentalFileID", &AccidentalFileID, &b_AccidentalFileID);
         m_chain->SetBranchAddress("AccidentalEntryID", &AccidentalEntryID, &b_AccidentalEntryID);
         m_chain->SetBranchAddress("AccidentalOverlayFlag", &AccidentalOverlayFlag, &b_AccidentalOverlayFlag);
         m_chain->SetBranchAddress("GenParticleNumber", &GenParticleNumber, &b_GenParticleNumber);
         m_chain->SetBranchAddress("GenParticleTrack", GenParticleTrack, &b_GenParticleTrack);
         m_chain->SetBranchAddress("GenParticleMother", GenParticleMother, &b_GenParticleMother);
         m_chain->SetBranchAddress("GenParticlePid", GenParticlePid, &b_GenParticlePid);
         m_chain->SetBranchAddress("GenParticleMom", GenParticleMom, &b_GenParticleMom);
         m_chain->SetBranchAddress("GenParticleEk", GenParticleEk, &b_GenParticleEk);
         m_chain->SetBranchAddress("GenParticleMass", GenParticleMass, &b_GenParticleMass);
         m_chain->SetBranchAddress("GenParticleTime", GenParticleTime, &b_GenParticleTime);
         m_chain->SetBranchAddress("GenParticlePos", GenParticlePos, &b_GenParticlePos);
         m_chain->SetBranchAddress("GenParticleEndMom", GenParticleEndMom, &b_GenParticleEndMom);
         m_chain->SetBranchAddress("GenParticleEndEk", GenParticleEndEk, &b_GenParticleEndEk);
         m_chain->SetBranchAddress("GenParticleEndTime", GenParticleEndTime, &b_GenParticleEndTime);
         m_chain->SetBranchAddress("GenParticleEndPos", GenParticleEndPos, &b_GenParticleEndPos);
      }

      if( m_chain->GetBranch("CDTNum") ){
	m_chain->SetBranchAddress("CDTNum", &CDTNum, &b_CDTNum);
      }

      m_chain->SetBranchAddress("eventID", &eventID, &b_eventID);
      m_chain->SetBranchAddress("OrigEventID", &OrigEventID, &b_OrigEventID);
      m_chain->SetBranchAddress("CutCondition", &CutCondition, &b_CutCondition);
      m_chain->SetBranchAddress("VetoCondition", &VetoCondition, &b_VetoCondition);
      m_chain->SetBranchAddress("GamClusNumber", &GamClusNumber, &b_GamClusNumber);
      m_chain->SetBranchAddress("GamClusId", GamClusId, &b_GamClusId);
      m_chain->SetBranchAddress("GamClusStatus", GamClusStatus, &b_GamClusStatus);
      m_chain->SetBranchAddress("GamClusThreshold", GamClusThreshold, &b_GamClusThreshold);
      m_chain->SetBranchAddress("GamClusDepE", GamClusDepE, &b_GamClusDepE);
      m_chain->SetBranchAddress("GamClusCoePos", GamClusCoePos, &b_GamClusCoePos);
      m_chain->SetBranchAddress("GamClusTime", GamClusTime, &b_GamClusTime);
      m_chain->SetBranchAddress("GamClusRMS", GamClusRMS, &b_GamClusRMS);
      m_chain->SetBranchAddress("GamClusSize", GamClusSize, &b_GamClusSize);
      m_chain->SetBranchAddress("GamClusCsiId", GamClusCsiId, &b_GamClusCsiId);
      m_chain->SetBranchAddress("GamClusCsiE", GamClusCsiE, &b_GamClusCsiE);
      m_chain->SetBranchAddress("GamClusCsiTime", GamClusCsiTime, &b_GamClusCsiTime);
      m_chain->SetBranchAddress("GammaNumber", &GammaNumber, &b_GammaNumber);
      m_chain->SetBranchAddress("GammaId", GammaId, &b_GammaId);
      m_chain->SetBranchAddress("GammaStatus", GammaStatus, &b_GammaStatus);
      m_chain->SetBranchAddress("GammaE", GammaE, &b_GammaE);
      m_chain->SetBranchAddress("GammaPos", GammaPos, &b_GammaPos);
      m_chain->SetBranchAddress("GammaTime", GammaTime, &b_GammaTime);
      m_chain->SetBranchAddress("GammaMom", GammaMom, &b_GammaMom);
      m_chain->SetBranchAddress("GammaSigmaE", GammaSigmaE, &b_GammaSigmaE);
      m_chain->SetBranchAddress("GammaSigmaPos", GammaSigmaPos, &b_GammaSigmaPos);
      m_chain->SetBranchAddress("GammaChi2", GammaChi2, &b_GammaChi2);
      m_chain->SetBranchAddress("GammaAnn", GammaAnn, &b_GammaAnn);
      m_chain->SetBranchAddress("Gamma_clusIndex", Gamma_clusIndex, &b_Gamma_clusIndex);
      m_chain->SetBranchAddress("Pi0Number", &Pi0Number, &b_Pi0Number);
      m_chain->SetBranchAddress("Pi0Id", Pi0Id, &b_Pi0Id);
      m_chain->SetBranchAddress("Pi0Status", Pi0Status, &b_Pi0Status);
      m_chain->SetBranchAddress("Pi0E", Pi0E, &b_Pi0E);
      m_chain->SetBranchAddress("Pi0Pos", Pi0Pos, &b_Pi0Pos);
      m_chain->SetBranchAddress("Pi0Mom", Pi0Mom, &b_Pi0Mom);
      m_chain->SetBranchAddress("Pi0Pt", Pi0Pt, &b_Pi0Pt);
      m_chain->SetBranchAddress("Pi0Mass", Pi0Mass, &b_Pi0Mass);
      m_chain->SetBranchAddress("Pi0RecZ", Pi0RecZ, &b_Pi0RecZ);
      m_chain->SetBranchAddress("Pi0RecZsig2", Pi0RecZsig2, &b_Pi0RecZsig2);
      m_chain->SetBranchAddress("Pi0_gamIndex", Pi0_gamIndex, &b_Pi0_gamIndex);
     
      /// g4ana, g6ana /// 
      if( m_chain->GetBranch("KlongNumber") ){
         m_chain->SetBranchAddress("KlongNumber", &KlongNumber, &b_KlongNumber);
         m_chain->SetBranchAddress("KlongId", KlongId, &b_KlongId);
         m_chain->SetBranchAddress("KlongStatus", KlongStatus, &b_KlongStatus);
         m_chain->SetBranchAddress("KlongE", KlongE, &b_KlongE);
         m_chain->SetBranchAddress("KlongPos", KlongPos, &b_KlongPos);
         m_chain->SetBranchAddress("KlongMom", KlongMom, &b_KlongMom);
         m_chain->SetBranchAddress("KlongPt", KlongPt, &b_KlongPt);
         m_chain->SetBranchAddress("KlongMass", KlongMass, &b_KlongMass);
         m_chain->SetBranchAddress("KlongDeltaZ", KlongDeltaZ, &b_KlongDeltaZ);
         m_chain->SetBranchAddress("KlongChisqZ", KlongChisqZ, &b_KlongChisqZ);
         m_chain->SetBranchAddress("KlongVertFlag", KlongVertFlag, &b_KlongVertFlag);
         m_chain->SetBranchAddress("KlongSortFlag", KlongSortFlag, &b_KlongSortFlag);
         m_chain->SetBranchAddress("Klong_piIndex", Klong_piIndex, &b_Klong_piIndex);
         m_chain->SetBranchAddress("Klong_gamNumber", &Klong_gamNumber, &b_Klong_gamNumber);
         m_chain->SetBranchAddress("Klong_gamIndex", Klong_gamIndex, &b_Klong_gamIndex);
      }

      if( m_chain->GetBranch("CSIEt") )
         m_chain->SetBranchAddress("CSIEt", &CSIEt, &b_CSIEt);

      /// common ///
      m_chain->SetBranchAddress("EventStartTime", &EventStartTime, &b_EventStartTime);
      m_chain->SetBranchAddress("EventStartZ", &EventStartZ, &b_EventStartZ);
      m_chain->SetBranchAddress("VertexTime", VertexTime, &b_VertexTime);
      m_chain->SetBranchAddress("OriginalClusterNumber", &OriginalClusterNumber, &b_OriginalClusterNumber);
      m_chain->SetBranchAddress("OriginalClusterE", OriginalClusterE, &b_OriginalClusterE);
      m_chain->SetBranchAddress("OriginalClusterT", OriginalClusterT, &b_OriginalClusterT);
      m_chain->SetBranchAddress("OriginalClusterVertexT", OriginalClusterVertexT, &b_OriginalClusterVertexT);
      if( m_chain->GetBranch("MyCutCondition") )
         m_chain->SetBranchAddress("MyCutCondition", &MyCutCondition, &b_MyCutCondition);
  
      m_chain->SetBranchAddress("MyVetoCondition", &MyVetoCondition, &b_MyVetoCondition);
      m_chain->SetBranchAddress("DeltaVertexTime", &DeltaVertexTime, &b_DeltaVertexTime);

      if( m_chain->GetBranch("MaxDeltaPi0Mass") )
         m_chain->SetBranchAddress("MaxDeltaPi0Mass", &MaxDeltaPi0Mass, &b_MaxDeltaPi0Mass);

      m_chain->SetBranchAddress("TotalEt", &TotalEt, &b_TotalEt);
      m_chain->SetBranchAddress("MinGammaE", &MinGammaE, &b_MinGammaE);
      m_chain->SetBranchAddress("MaxGammaE", &MaxGammaE, &b_MaxGammaE);
      m_chain->SetBranchAddress("MaxFiducialR", &MaxFiducialR, &b_MaxFiducialR);
      m_chain->SetBranchAddress("MinFiducialXY", &MinFiducialXY, &b_MinFiducialXY);

      
      /// Use MinClusterDistance as variable for all gXana ///
      if( m_chain->GetBranch("MinClusterDistance") )
         m_chain->SetBranchAddress("MinClusterDistance", &MinClusterDistance, &b_MinClusterDistance);
      else if( m_chain->GetBranch("ClusterDistance") )
         m_chain->SetBranchAddress("ClusterDistance", &MinClusterDistance, &b_MinClusterDistance);

      /// g4ana, g6ana ///
      if( m_chain->GetBranch("KLDeltChisqZ") )
         m_chain->SetBranchAddress("KLDeltaChisqZ", &KLDeltaChisqZ, &b_KLDeltaChisqZ);

      /// g2ana, g2anaKL ///
      if( m_chain->GetBranch("ERatio") ){
         m_chain->SetBranchAddress("ERatio", &ERatio, &b_ERatio);
         m_chain->SetBranchAddress("EAsymmetry", &EAsymmetry, &b_EAsymmetry);
         m_chain->SetBranchAddress("ProjectionAngle", &ProjectionAngle, &b_ProjectionAngle);
         m_chain->SetBranchAddress("MinETheta", &MinETheta, &b_MinETheta);
         m_chain->SetBranchAddress("CSICoEX", &CSICoEX, &b_CSICoEX);
         m_chain->SetBranchAddress("CSICoEY", &CSICoEY, &b_CSICoEY);
         m_chain->SetBranchAddress("InterpolatedDistance", &InterpolatedDistance, &b_InterpolatedDistance);
         m_chain->SetBranchAddress("IntersectedSquare", &IntersectedSquare, &b_IntersectedSquare);
      }

      /// g2ana only ///
      if( m_chain->GetBranch("AddCutCondition") ){
         m_chain->SetBranchAddress("AddCutCondition", &AddCutCondition);
         m_chain->SetBranchAddress("MinClusRMS", &MinClusRMS, &b_MinClusRMS);
         m_chain->SetBranchAddress("MinDeadChR", &MinDeadChR, &b_MinDeadChR);
      }

      m_chain->SetBranchAddress("MaxShapeChisq", &MaxShapeChisq, &b_MaxShapeChisq);
      m_chain->SetBranchAddress("ExtraClusterDeltaVertexTime", &ExtraClusterDeltaVertexTime, &b_ExtraClusterDeltaVertexTime);
      m_chain->SetBranchAddress("ExtraClusterEne", &ExtraClusterEne, &b_ExtraClusterEne);

      if( m_chain->GetBranch("KLBeamExitX") ){
         m_chain->SetBranchAddress("KLBeamExitX", &KLBeamExitX, &b_KLBeamExitX);
         m_chain->SetBranchAddress("KLBeamExitY", &KLBeamExitY, &b_KLBeamExitY);
      }

      m_chain->SetBranchAddress("AverageClusterTime", &AverageClusterTime, &b_AverageClusterTime);
      m_chain->SetBranchAddress("CC03ModuleNumber", &CC03ModuleNumber, &b_CC03ModuleNumber);
      m_chain->SetBranchAddress("CC03ModuleModID", CC03ModuleModID, &b_CC03ModuleModID);
      m_chain->SetBranchAddress("CC03ModuleEne", CC03ModuleEne, &b_CC03ModuleEne);
      m_chain->SetBranchAddress("CC03ModuleTime", CC03ModuleTime, &b_CC03ModuleTime);
      m_chain->SetBranchAddress("CC03VetoModID", &CC03VetoModID, &b_CC03VetoModID);
      m_chain->SetBranchAddress("CC03VetoEne", &CC03VetoEne, &b_CC03VetoEne);
      m_chain->SetBranchAddress("CC03VetoTime", &CC03VetoTime, &b_CC03VetoTime);
      m_chain->SetBranchAddress("CC03TotalVetoEne", &CC03TotalVetoEne, &b_CC03TotalVetoEne);
      m_chain->SetBranchAddress("CC04E391ModuleNumber", &CC04E391ModuleNumber, &b_CC04E391ModuleNumber);
      m_chain->SetBranchAddress("CC04E391ModuleModID", CC04E391ModuleModID, &b_CC04E391ModuleModID);
      m_chain->SetBranchAddress("CC04E391ModuleEne", CC04E391ModuleEne, &b_CC04E391ModuleEne);
      m_chain->SetBranchAddress("CC04E391ModuleTime", CC04E391ModuleTime, &b_CC04E391ModuleTime);
      m_chain->SetBranchAddress("CC04KTeVModuleNumber", &CC04KTeVModuleNumber, &b_CC04KTeVModuleNumber);
      m_chain->SetBranchAddress("CC04KTeVModuleModID", CC04KTeVModuleModID, &b_CC04KTeVModuleModID);
      m_chain->SetBranchAddress("CC04KTeVModuleEne", CC04KTeVModuleEne, &b_CC04KTeVModuleEne);
      m_chain->SetBranchAddress("CC04KTeVModuleTime", CC04KTeVModuleTime, &b_CC04KTeVModuleTime);
      m_chain->SetBranchAddress("CC04ScintiModuleNumber", &CC04ScintiModuleNumber, &b_CC04ScintiModuleNumber);
      m_chain->SetBranchAddress("CC04ScintiModuleModID", CC04ScintiModuleModID, &b_CC04ScintiModuleModID);
      m_chain->SetBranchAddress("CC04ScintiModuleEne", CC04ScintiModuleEne, &b_CC04ScintiModuleEne);
      m_chain->SetBranchAddress("CC04ScintiModuleTime", CC04ScintiModuleTime, &b_CC04ScintiModuleTime);
      m_chain->SetBranchAddress("CC04E391VetoModID", &CC04E391VetoModID, &b_CC04E391VetoModID);
      m_chain->SetBranchAddress("CC04E391VetoEne", &CC04E391VetoEne, &b_CC04E391VetoEne);
      m_chain->SetBranchAddress("CC04E391VetoTime", &CC04E391VetoTime, &b_CC04E391VetoTime);
      m_chain->SetBranchAddress("CC04KTeVVetoModID", &CC04KTeVVetoModID, &b_CC04KTeVVetoModID);
      m_chain->SetBranchAddress("CC04KTeVVetoEne", &CC04KTeVVetoEne, &b_CC04KTeVVetoEne);
      m_chain->SetBranchAddress("CC04KTeVVetoTime", &CC04KTeVVetoTime, &b_CC04KTeVVetoTime);
      m_chain->SetBranchAddress("CC04ScintiVetoModID", &CC04ScintiVetoModID, &b_CC04ScintiVetoModID);
      m_chain->SetBranchAddress("CC04ScintiVetoEne", &CC04ScintiVetoEne, &b_CC04ScintiVetoEne);
      m_chain->SetBranchAddress("CC04ScintiVetoTime", &CC04ScintiVetoTime, &b_CC04ScintiVetoTime);
      m_chain->SetBranchAddress("CC05E391ModuleNumber", &CC05E391ModuleNumber, &b_CC05E391ModuleNumber);
      m_chain->SetBranchAddress("CC05E391ModuleModID", CC05E391ModuleModID, &b_CC05E391ModuleModID);
      m_chain->SetBranchAddress("CC05E391ModuleEne", CC05E391ModuleEne, &b_CC05E391ModuleEne);
      m_chain->SetBranchAddress("CC05E391ModuleTime", CC05E391ModuleTime, &b_CC05E391ModuleTime);
      m_chain->SetBranchAddress("CC05ScintiModuleNumber", &CC05ScintiModuleNumber, &b_CC05ScintiModuleNumber);
      m_chain->SetBranchAddress("CC05ScintiModuleModID", CC05ScintiModuleModID, &b_CC05ScintiModuleModID);
      m_chain->SetBranchAddress("CC05ScintiModuleEne", CC05ScintiModuleEne, &b_CC05ScintiModuleEne);
      m_chain->SetBranchAddress("CC05ScintiModuleTime", CC05ScintiModuleTime, &b_CC05ScintiModuleTime);
      m_chain->SetBranchAddress("CC05E391VetoModID", &CC05E391VetoModID, &b_CC05E391VetoModID);
      m_chain->SetBranchAddress("CC05E391VetoEne", &CC05E391VetoEne, &b_CC05E391VetoEne);
      m_chain->SetBranchAddress("CC05E391VetoTime", &CC05E391VetoTime, &b_CC05E391VetoTime);
      m_chain->SetBranchAddress("CC05ScintiVetoModID", &CC05ScintiVetoModID, &b_CC05ScintiVetoModID);
      m_chain->SetBranchAddress("CC05ScintiVetoEne", &CC05ScintiVetoEne, &b_CC05ScintiVetoEne);
      m_chain->SetBranchAddress("CC05ScintiVetoTime", &CC05ScintiVetoTime, &b_CC05ScintiVetoTime);
      m_chain->SetBranchAddress("CC06E391ModuleNumber", &CC06E391ModuleNumber, &b_CC06E391ModuleNumber);
      m_chain->SetBranchAddress("CC06E391ModuleModID", CC06E391ModuleModID, &b_CC06E391ModuleModID);
      m_chain->SetBranchAddress("CC06E391ModuleEne", CC06E391ModuleEne, &b_CC06E391ModuleEne);
      m_chain->SetBranchAddress("CC06E391ModuleTime", CC06E391ModuleTime, &b_CC06E391ModuleTime);
      m_chain->SetBranchAddress("CC06ScintiModuleNumber", &CC06ScintiModuleNumber, &b_CC06ScintiModuleNumber);
      m_chain->SetBranchAddress("CC06ScintiModuleModID", CC06ScintiModuleModID, &b_CC06ScintiModuleModID);
      m_chain->SetBranchAddress("CC06ScintiModuleEne", CC06ScintiModuleEne, &b_CC06ScintiModuleEne);
      m_chain->SetBranchAddress("CC06ScintiModuleTime", CC06ScintiModuleTime, &b_CC06ScintiModuleTime);
      m_chain->SetBranchAddress("CC06E391VetoModID", &CC06E391VetoModID, &b_CC06E391VetoModID);
      m_chain->SetBranchAddress("CC06E391VetoEne", &CC06E391VetoEne, &b_CC06E391VetoEne);
      m_chain->SetBranchAddress("CC06E391VetoTime", &CC06E391VetoTime, &b_CC06E391VetoTime);
      m_chain->SetBranchAddress("CC06ScintiVetoModID", &CC06ScintiVetoModID, &b_CC06ScintiVetoModID);
      m_chain->SetBranchAddress("CC06ScintiVetoEne", &CC06ScintiVetoEne, &b_CC06ScintiVetoEne);
      m_chain->SetBranchAddress("CC06ScintiVetoTime", &CC06ScintiVetoTime, &b_CC06ScintiVetoTime);
      m_chain->SetBranchAddress("CBARVetoModID", &CBARVetoModID, &b_CBARVetoModID);
      m_chain->SetBranchAddress("CBARVetoEne", &CBARVetoEne, &b_CBARVetoEne);
      m_chain->SetBranchAddress("CBARVetoTime", &CBARVetoTime, &b_CBARVetoTime);
      m_chain->SetBranchAddress("CBARVetoHitZ", &CBARVetoHitZ, &b_CBARVetoHitZ);
      m_chain->SetBranchAddress("CBARTotalVetoEne", &CBARTotalVetoEne, &b_CBARTotalVetoEne);
      m_chain->SetBranchAddress("CBARInnerVetoModID", &CBARInnerVetoModID, &b_CBARInnerVetoModID);
      m_chain->SetBranchAddress("CBARInnerVetoEne", &CBARInnerVetoEne, &b_CBARInnerVetoEne);
      m_chain->SetBranchAddress("CBARInnerVetoTime", &CBARInnerVetoTime, &b_CBARInnerVetoTime);
      m_chain->SetBranchAddress("CBARInnerVetoHitZ", &CBARInnerVetoHitZ, &b_CBARInnerVetoHitZ);
      m_chain->SetBranchAddress("CBAROuterVetoModID", &CBAROuterVetoModID, &b_CBAROuterVetoModID);
      m_chain->SetBranchAddress("CBAROuterVetoEne", &CBAROuterVetoEne, &b_CBAROuterVetoEne);
      m_chain->SetBranchAddress("CBAROuterVetoTime", &CBAROuterVetoTime, &b_CBAROuterVetoTime);
      m_chain->SetBranchAddress("CBAROuterVetoHitZ", &CBAROuterVetoHitZ, &b_CBAROuterVetoHitZ);
      m_chain->SetBranchAddress("CBARModuleNumber",&CBARModuleNumber);
      m_chain->SetBranchAddress("CBARModuleModID" , CBARModuleModID);
      m_chain->SetBranchAddress("CBARModuleHitTime",CBARModuleHitTime);
      m_chain->SetBranchAddress("CBARModuleEne",CBARModuleEne);
      m_chain->SetBranchAddress("CBARModuleHitTime",CBARModuleHitTime);
      m_chain->SetBranchAddress("CBARModuleHitZ",CBARModuleHitZ);
      m_chain->SetBranchAddress("CBARModuleDeltaTime",CBARModuleDeltaTime);
      m_chain->SetBranchAddress("FBARModuleNumber", &FBARModuleNumber, &b_FBARModuleNumber);
      m_chain->SetBranchAddress("FBARModuleModID", FBARModuleModID, &b_FBARModuleModID);
      m_chain->SetBranchAddress("FBARModuleEne", FBARModuleEne, &b_FBARModuleEne);
      m_chain->SetBranchAddress("FBARModuleTime", FBARModuleTime, &b_FBARModuleTime);
      m_chain->SetBranchAddress("FBARVetoModID", &FBARVetoModID, &b_FBARVetoModID);
      m_chain->SetBranchAddress("FBARVetoEne", &FBARVetoEne, &b_FBARVetoEne);
      m_chain->SetBranchAddress("FBARVetoTime", &FBARVetoTime, &b_FBARVetoTime);
      m_chain->SetBranchAddress("CSIVetoNumber", &CSIVetoNumber, &b_CSIVetoNumber);
      m_chain->SetBranchAddress("CSIVetoModID", CSIVetoModID, &b_CSIVetoModID);
      m_chain->SetBranchAddress("CSIVetoEne", CSIVetoEne, &b_CSIVetoEne);
      m_chain->SetBranchAddress("CSIVetoDistance", CSIVetoDistance, &b_CSIVetoDistance);
      m_chain->SetBranchAddress("CSIVetoDeltaTime", CSIVetoDeltaTime, &b_CSIVetoDeltaTime);
      m_chain->SetBranchAddress("BHPVNumber", &BHPVNumber, &b_BHPVNumber);
      m_chain->SetBranchAddress("BHPVModID", BHPVModID, &b_BHPVModID);
      m_chain->SetBranchAddress("BHPVnHits", BHPVnHits, &b_BHPVnHits);
      m_chain->SetBranchAddress("BHPVEne", BHPVEne, &b_BHPVEne);
      m_chain->SetBranchAddress("BHPVTime", BHPVTime, &b_BHPVTime);
      m_chain->SetBranchAddress("BHPVVetoModID", &BHPVVetoModID, &b_BHPVVetoModID);
      m_chain->SetBranchAddress("BHPVVetonHitMod", &BHPVVetonHitMod, &b_BHPVVetonHitMod);
      m_chain->SetBranchAddress("BHPVVetoEne", &BHPVVetoEne, &b_BHPVVetoEne);
      m_chain->SetBranchAddress("BHPVVetoTime", &BHPVVetoTime, &b_BHPVVetoTime);
      m_chain->SetBranchAddress("BHPVCoinNumber", &BHPVCoinNumber, &b_BHPVCoinNumber);
      m_chain->SetBranchAddress("BHPVCoinnHitMod", BHPVCoinnHitMod, &b_BHPVCoinnHitMod);
      m_chain->SetBranchAddress("BHPVCoinModID", BHPVCoinModID, &b_BHPVCoinModID);
      m_chain->SetBranchAddress("BHPVCoinEne", BHPVCoinEne, &b_BHPVCoinEne);
      m_chain->SetBranchAddress("BHPVCoinTime", BHPVCoinTime, &b_BHPVCoinTime);
      m_chain->SetBranchAddress("BHPVCoinTimeSpread", BHPVCoinTimeSpread, &b_BHPVCoinTimeSpread);
      m_chain->SetBranchAddress("CVVetoModID", &CVVetoModID, &b_CVVetoModID);
      m_chain->SetBranchAddress("CVVetoEne", &CVVetoEne, &b_CVVetoEne);
      m_chain->SetBranchAddress("CVVetoTime", &CVVetoTime, &b_CVVetoTime);
      m_chain->SetBranchAddress("CVTotalVetoEne", &CVTotalVetoEne, &b_CVTotalVetoEne);
      m_chain->SetBranchAddress("CVFrontVetoModID", &CVFrontVetoModID, &b_CVFrontVetoModID);
      m_chain->SetBranchAddress("CVFrontVetoEne", &CVFrontVetoEne, &b_CVFrontVetoEne);
      m_chain->SetBranchAddress("CVFrontVetoTime", &CVFrontVetoTime, &b_CVFrontVetoTime);
      m_chain->SetBranchAddress("CVFrontTotalVetoEne", &CVFrontTotalVetoEne, &b_CVFrontTotalVetoEne);
      m_chain->SetBranchAddress("CVRearVetoModID", &CVRearVetoModID, &b_CVRearVetoModID);
      m_chain->SetBranchAddress("CVRearVetoEne", &CVRearVetoEne, &b_CVRearVetoEne);
      m_chain->SetBranchAddress("CVRearVetoTime", &CVRearVetoTime, &b_CVRearVetoTime);
      m_chain->SetBranchAddress("CVRearTotalVetoEne", &CVRearTotalVetoEne, &b_CVRearTotalVetoEne);
      m_chain->SetBranchAddress("CVMaxPulseWidth", &CVMaxPulseWidth, &b_CVMaxPulseWidth);
      m_chain->SetBranchAddress("CVModuleNumber", &CVModuleNumber, &b_CVModuleNumber);
      m_chain->SetBranchAddress("CVModuleModID", CVModuleModID, &b_CVModuleModID);
      m_chain->SetBranchAddress("CVModuleHitTime", CVModuleHitTime, &b_CVModuleHitTime);
      m_chain->SetBranchAddress("CVModuleEne", CVModuleEne, &b_CVModuleEne);
      m_chain->SetBranchAddress("CVModuleDeltaTime", CVModuleDeltaTime, &b_CVModuleDeltaTime);
      m_chain->SetBranchAddress("CVModuleRiseTime", CVModuleRiseTime, &b_CVModuleRiseTime);
      m_chain->SetBranchAddress("NCCModuleNumber", &NCCModuleNumber, &b_NCCModuleNumber);
      m_chain->SetBranchAddress("NCCModuleModID", NCCModuleModID, &b_NCCModuleModID);
      m_chain->SetBranchAddress("NCCModuleEne", NCCModuleEne, &b_NCCModuleEne);
      m_chain->SetBranchAddress("NCCModuleTime", NCCModuleTime, &b_NCCModuleTime);
      m_chain->SetBranchAddress("NCCScintiModuleNumber", &NCCScintiModuleNumber, &b_NCCScintiModuleNumber);
      m_chain->SetBranchAddress("NCCScintiModuleModID", NCCScintiModuleModID, &b_NCCScintiModuleModID);
      m_chain->SetBranchAddress("NCCScintiModuleEne", NCCScintiModuleEne, &b_NCCScintiModuleEne);
      m_chain->SetBranchAddress("NCCScintiModuleTime", NCCScintiModuleTime, &b_NCCScintiModuleTime);
      m_chain->SetBranchAddress("NCCVetoModID", &NCCVetoModID, &b_NCCVetoModID);
      m_chain->SetBranchAddress("NCCVetoEne", &NCCVetoEne, &b_NCCVetoEne);
      m_chain->SetBranchAddress("NCCVetoTime", &NCCVetoTime, &b_NCCVetoTime);
      m_chain->SetBranchAddress("NCCTotalVetoEne", &NCCTotalVetoEne, &b_NCCTotalVetoEne);
      m_chain->SetBranchAddress("NCCScintiVetoModID", &NCCScintiVetoModID, &b_NCCScintiVetoModID);
      m_chain->SetBranchAddress("NCCScintiVetoEne", &NCCScintiVetoEne, &b_NCCScintiVetoEne);
      m_chain->SetBranchAddress("NCCScintiVetoTime", &NCCScintiVetoTime, &b_NCCScintiVetoTime);
      m_chain->SetBranchAddress("OEVModuleNumber", &OEVModuleNumber, &b_OEVModuleNumber);
      m_chain->SetBranchAddress("OEVModuleModID", OEVModuleModID, &b_OEVModuleModID);
      m_chain->SetBranchAddress("OEVModuleEne", OEVModuleEne, &b_OEVModuleEne);
      m_chain->SetBranchAddress("OEVModuleTime", OEVModuleTime, &b_OEVModuleTime);
      m_chain->SetBranchAddress("OEVVetoModID", &OEVVetoModID, &b_OEVVetoModID);
      m_chain->SetBranchAddress("OEVVetoEne", &OEVVetoEne, &b_OEVVetoEne);
      m_chain->SetBranchAddress("OEVVetoTime", &OEVVetoTime, &b_OEVVetoTime);
      m_chain->SetBranchAddress("LCVModuleNumber", &LCVModuleNumber, &b_LCVModuleNumber);
      m_chain->SetBranchAddress("LCVModuleModID", LCVModuleModID, &b_LCVModuleModID);
      m_chain->SetBranchAddress("LCVModuleEne", LCVModuleEne, &b_LCVModuleEne);
      m_chain->SetBranchAddress("LCVModuleTime", LCVModuleTime, &b_LCVModuleTime);
      m_chain->SetBranchAddress("LCVVetoModID", &LCVVetoModID, &b_LCVVetoModID);
      m_chain->SetBranchAddress("LCVVetoEne", &LCVVetoEne, &b_LCVVetoEne);
      m_chain->SetBranchAddress("LCVVetoTime", &LCVVetoTime, &b_LCVVetoTime);

      if( m_chain->GetBranch("BPCVModuleNumber") ){
         m_chain->SetBranchAddress("BPCVModuleNumber", &BPCVModuleNumber, &b_BPCVModuleNumber);
         m_chain->SetBranchAddress("BPCVModuleModID", BPCVModuleModID, &b_BPCVModuleModID);
         m_chain->SetBranchAddress("BPCVModuleEne", BPCVModuleEne, &b_BPCVModuleEne);
         m_chain->SetBranchAddress("BPCVModuleTime", BPCVModuleTime, &b_BPCVModuleTime);
         m_chain->SetBranchAddress("BPCVVetoModID", &BPCVVetoModID, &b_BPCVVetoModID);
         m_chain->SetBranchAddress("BPCVVetoEne", &BPCVVetoEne, &b_BPCVVetoEne);
         m_chain->SetBranchAddress("BPCVVetoTime", &BPCVVetoTime, &b_BPCVVetoTime);
      }
      m_chain->SetBranchAddress("newBHCVNumber", &newBHCVNumber, &b_newBHCVNumber);
      m_chain->SetBranchAddress("newBHCVModID", newBHCVModID, &b_newBHCVModID);
      m_chain->SetBranchAddress("newBHCVnHits", newBHCVnHits, &b_newBHCVnHits);
      m_chain->SetBranchAddress("newBHCVEne", newBHCVEne, &b_newBHCVEne);
      m_chain->SetBranchAddress("newBHCVTime", newBHCVTime, &b_newBHCVTime);
      m_chain->SetBranchAddress("newBHCVVetoModID", &newBHCVVetoModID, &b_newBHCVVetoModID);
      m_chain->SetBranchAddress("newBHCVVetoEne", &newBHCVVetoEne, &b_newBHCVVetoEne);
      m_chain->SetBranchAddress("newBHCVVetoTime", &newBHCVVetoTime, &b_newBHCVVetoTime);
      m_chain->SetBranchAddress("newBHCVHitCount", &newBHCVHitCount, &b_newBHCVHitCount);
      m_chain->SetBranchAddress("newBHCVModHitCount", &newBHCVModHitCount, &b_newBHCVModHitCount);
      m_chain->SetBranchAddress("BHGCNumber", &BHGCNumber, &b_BHGCNumber);
      m_chain->SetBranchAddress("BHGCModID", BHGCModID, &b_BHGCModID);
      m_chain->SetBranchAddress("BHGCnHits", BHGCnHits, &b_BHGCnHits);
      m_chain->SetBranchAddress("BHGCEne", BHGCEne, &b_BHGCEne);
      m_chain->SetBranchAddress("BHGCTime", BHGCTime, &b_BHGCTime);
      m_chain->SetBranchAddress("BHGCVetoModID", &BHGCVetoModID, &b_BHGCVetoModID);
      m_chain->SetBranchAddress("BHGCVetoEne", &BHGCVetoEne, &b_BHGCVetoEne);
      m_chain->SetBranchAddress("BHGCVetoTime", &BHGCVetoTime, &b_BHGCVetoTime);

      if( m_chain->GetBranch("CC06FTTNumber") ){
	m_chain->SetBranchAddress("CC06FTTNumber", &CC06FTTNumber, &b_CC06FTTNumber);
	m_chain->SetBranchAddress("CC06FTTModID", CC06FTTModID, &b_CC06FTTModID);
	m_chain->SetBranchAddress("CC06FTTChisq", CC06FTTChisq, &b_CC06FTTChisq);
	m_chain->SetBranchAddress("CC06FTTTime", CC06FTTTime, &b_CC06FTTTime);
	m_chain->SetBranchAddress("CBARFTTNumber", &CBARFTTNumber, &b_CBARFTTNumber);
	m_chain->SetBranchAddress("CBARFTTModID", CBARFTTModID, &b_CBARFTTModID);
	m_chain->SetBranchAddress("CBARFTTChisq", CBARFTTChisq, &b_CBARFTTChisq);
	m_chain->SetBranchAddress("CBARFTTTime", CBARFTTTime, &b_CBARFTTTime);
	m_chain->SetBranchAddress("FBARFTTNumber", &FBARFTTNumber, &b_FBARFTTNumber);
	m_chain->SetBranchAddress("FBARFTTModID", FBARFTTModID, &b_FBARFTTModID);
	m_chain->SetBranchAddress("FBARFTTChisq", FBARFTTChisq, &b_FBARFTTChisq);
	m_chain->SetBranchAddress("FBARFTTTime", FBARFTTTime, &b_FBARFTTTime);
	m_chain->SetBranchAddress("CVFTTNumber", &CVFTTNumber, &b_CVFTTNumber);
	m_chain->SetBranchAddress("CVFTTModID", CVFTTModID, &b_CVFTTModID);
	m_chain->SetBranchAddress("CVFTTChisq", CVFTTChisq, &b_CVFTTChisq);
	m_chain->SetBranchAddress("CVFTTTime", CVFTTTime, &b_CVFTTTime);
	m_chain->SetBranchAddress("NCCFTTNumber", &NCCFTTNumber, &b_NCCFTTNumber);
	m_chain->SetBranchAddress("NCCFTTModID", NCCFTTModID, &b_NCCFTTModID);
	m_chain->SetBranchAddress("NCCFTTChisq", NCCFTTChisq, &b_NCCFTTChisq);
	m_chain->SetBranchAddress("NCCFTTTime", NCCFTTTime, &b_NCCFTTTime);
	m_chain->SetBranchAddress("IBFTTNumber", &IBFTTNumber, &b_IBFTTNumber);
	m_chain->SetBranchAddress("IBFTTModID", IBFTTModID, &b_IBFTTModID);
	m_chain->SetBranchAddress("IBFTTChisq", IBFTTChisq, &b_IBFTTChisq);
	m_chain->SetBranchAddress("IBFTTTime", IBFTTTime, &b_IBFTTTime);
      }

      if( m_chain->GetBranch("IBNumber") ){
	m_chain->SetBranchAddress("IBNumber", &IBNumber, &b_IBNumber);
	m_chain->SetBranchAddress("IBModID", IBModID, &b_IBModID);
	m_chain->SetBranchAddress("IBnHits", IBnHits, &b_IBnHits);
	m_chain->SetBranchAddress("IBEne", IBEne, &b_IBEne);
	m_chain->SetBranchAddress("IBTime", IBTime, &b_IBTime);
	m_chain->SetBranchAddress("IBModuleNumber", &IBModuleNumber, &b_IBModuleNumber);
	m_chain->SetBranchAddress("IBModuleModID", IBModuleModID, &b_IBModuleModID);
	m_chain->SetBranchAddress("IBModuleHitTime", IBModuleHitTime, &b_IBModuleHitTime);
	m_chain->SetBranchAddress("IBModuleEne", IBModuleEne, &b_IBModuleEne);
	m_chain->SetBranchAddress("IBModuleHitZ", IBModuleHitZ, &b_IBModuleHitZ);
	m_chain->SetBranchAddress("IBModuleDeltaTime", IBModuleDeltaTime, &b_IBModuleDeltaTime);
	m_chain->SetBranchAddress("IBVetoModID", &IBVetoModID, &b_IBVetoModID);
	m_chain->SetBranchAddress("IBVetoEne", &IBVetoEne, &b_IBVetoEne);
	m_chain->SetBranchAddress("IBVetoTime", &IBVetoTime, &b_IBVetoTime);
	m_chain->SetBranchAddress("IBWideVetoModID", &IBWideVetoModID, &b_IBWideVetoModID);
	m_chain->SetBranchAddress("IBWideVetoEne", &IBWideVetoEne, &b_IBWideVetoEne);
	m_chain->SetBranchAddress("IBWideVetoTime", &IBWideVetoTime, &b_IBWideVetoTime);
	m_chain->SetBranchAddress("IBCH55VetoEne", &IBCH55VetoEne, &b_IBCH55VetoEne);
	m_chain->SetBranchAddress("IBCH55VetoTime", &IBCH55VetoTime, &b_IBCH55VetoTime);
      }
      if( m_chain->GetBranch("IBCVModuleNumber") ){
	m_chain->SetBranchAddress("IBCVModuleNumber", &IBCVModuleNumber, &b_IBCVModuleNumber);
	m_chain->SetBranchAddress("IBCVModuleModID", IBCVModuleModID, &b_IBCVModuleModID);
	m_chain->SetBranchAddress("IBCVModuleEne", IBCVModuleEne, &b_IBCVModuleEne);
	m_chain->SetBranchAddress("IBCVModuleHitTime", IBCVModuleHitTime, &b_IBCVModuleHitTime);
	m_chain->SetBranchAddress("IBCVModuleDeltaTime", IBCVModuleDeltaTime, &b_IBCVModuleDeltaTime);
	m_chain->SetBranchAddress("IBCVVetoModID", &IBCVVetoModID, &b_IBCVVetoModID);
	m_chain->SetBranchAddress("IBCVVetoEne", &IBCVVetoEne, &b_IBCVVetoEne);
	m_chain->SetBranchAddress("IBCVVetoTime", &IBCVVetoTime, &b_IBCVVetoTime);
	m_chain->SetBranchAddress("IBCVCH27VetoEne", &IBCVCH27VetoEne, &b_IBCVCH27VetoEne);
	m_chain->SetBranchAddress("IBCVCH27VetoTime", &IBCVCH27VetoTime, &b_IBCVCH27VetoTime);
      }
      if( m_chain->GetBranch("MBCVModuleNumber") ){
	m_chain->SetBranchAddress("MBCVModuleNumber", &MBCVModuleNumber, &b_MBCVModuleNumber);
	m_chain->SetBranchAddress("MBCVModuleModID", MBCVModuleModID, &b_MBCVModuleModID);
	m_chain->SetBranchAddress("MBCVModuleEne", MBCVModuleEne, &b_MBCVModuleEne);
	m_chain->SetBranchAddress("MBCVModuleTime", MBCVModuleTime, &b_MBCVModuleTime);
	m_chain->SetBranchAddress("MBCVVetoModID", &MBCVVetoModID, &b_MBCVVetoModID);
	m_chain->SetBranchAddress("MBCVVetoEne", &MBCVVetoEne, &b_MBCVVetoEne);
	m_chain->SetBranchAddress("MBCVVetoTime", &MBCVVetoTime, &b_MBCVVetoTime);
	m_chain->SetBranchAddress("MBCVCH2VetoEne", &MBCVCH2VetoEne, &b_MBCVCH2VetoEne);
	m_chain->SetBranchAddress("MBCVCH2VetoTime", &MBCVCH2VetoTime, &b_MBCVCH2VetoTime);
      }

      if( m_chain->GetBranch("NewVetoCondition") ){
	
	m_chain->SetBranchAddress("isFitSigmaCSIVeto", &isFitSigmaCSIVeto, &b_isFitSigmaCSIVeto);
	m_chain->SetBranchAddress("isTightFitSigmaCSIVeto", &isTightFitSigmaCSIVeto, &b_isTightFitSigmaCSIVeto);
	m_chain->SetBranchAddress("TightCSIVetoMinDistModID", &TightCSIVetoMinDistModID, &b_TightCSIVetoMinDistModID);
	m_chain->SetBranchAddress("TightCSIVetoMaxEneModID", &TightCSIVetoMaxEneModID, &b_TightCSIVetoMaxEneModID);
	m_chain->SetBranchAddress("FBARProperModID", &FBARProperModID, &b_FBARProperModID);
	m_chain->SetBranchAddress("FBARProperTime", &FBARProperTime, &b_FBARProperTime);
	m_chain->SetBranchAddress("FBARProperEne", &FBARProperEne, &b_FBARProperEne);
	m_chain->SetBranchAddress("FBARProperFTTChisq", &FBARProperFTTChisq, &b_FBARProperFTTChisq);
	m_chain->SetBranchAddress("FBARMaskingProperModID", &FBARMaskingProperModID, &b_FBARMaskingProperModID);
	m_chain->SetBranchAddress("FBARMaskingProperTime", &FBARMaskingProperTime, &b_FBARMaskingProperTime);
	m_chain->SetBranchAddress("FBARMaskingProperEne", &FBARMaskingProperEne, &b_FBARMaskingProperEne);
	m_chain->SetBranchAddress("FBARMaskingProperFTTChisq", &FBARMaskingProperFTTChisq, &b_FBARMaskingProperFTTChisq);
	m_chain->SetBranchAddress("NCCProperModID", &NCCProperModID, &b_NCCProperModID);
	m_chain->SetBranchAddress("NCCProperTime", &NCCProperTime, &b_NCCProperTime);
	m_chain->SetBranchAddress("NCCProperEne", &NCCProperEne, &b_NCCProperEne);
	m_chain->SetBranchAddress("NCCProperFTTChisq", &NCCProperFTTChisq, &b_NCCProperFTTChisq);
	m_chain->SetBranchAddress("NCCMaskingProperModID", &NCCMaskingProperModID, &b_NCCMaskingProperModID);
	m_chain->SetBranchAddress("NCCMaskingProperTime", &NCCMaskingProperTime, &b_NCCMaskingProperTime);
	m_chain->SetBranchAddress("NCCMaskingProperEne", &NCCMaskingProperEne, &b_NCCMaskingProperEne);
	m_chain->SetBranchAddress("NCCMaskingProperFTTChisq", &NCCMaskingProperFTTChisq, &b_NCCMaskingProperFTTChisq);
	m_chain->SetBranchAddress("CBARProperModID", &CBARProperModID, &b_CBARProperModID);
	m_chain->SetBranchAddress("CBARProperTime", &CBARProperTime, &b_CBARProperTime);
	m_chain->SetBranchAddress("CBARProperEne", &CBARProperEne, &b_CBARProperEne);
	m_chain->SetBranchAddress("CBARProperFTTChisq", &CBARProperFTTChisq, &b_CBARProperFTTChisq);
	m_chain->SetBranchAddress("CBARMaskingProperModID", &CBARMaskingProperModID, &b_CBARMaskingProperModID);
	m_chain->SetBranchAddress("CBARMaskingProperTime", &CBARMaskingProperTime, &b_CBARMaskingProperTime);
	m_chain->SetBranchAddress("CBARMaskingProperEne", &CBARMaskingProperEne, &b_CBARMaskingProperEne);
	m_chain->SetBranchAddress("CBARMaskingProperFTTChisq", &CBARMaskingProperFTTChisq, &b_CBARMaskingProperFTTChisq);
	m_chain->SetBranchAddress("IBProperModID", &IBProperModID, &b_IBProperModID);
	m_chain->SetBranchAddress("IBProperTime", &IBProperTime, &b_IBProperTime);
	m_chain->SetBranchAddress("IBProperEne", &IBProperEne, &b_IBProperEne);
	m_chain->SetBranchAddress("IBProperFTTChisq", &IBProperFTTChisq, &b_IBProperFTTChisq);
	m_chain->SetBranchAddress("IBMaskingProperModID", &IBMaskingProperModID, &b_IBMaskingProperModID);
	m_chain->SetBranchAddress("IBMaskingProperTime", &IBMaskingProperTime, &b_IBMaskingProperTime);
	m_chain->SetBranchAddress("IBMaskingProperEne", &IBMaskingProperEne, &b_IBMaskingProperEne);
	m_chain->SetBranchAddress("IBMaskingProperFTTChisq", &IBMaskingProperFTTChisq, &b_IBMaskingProperFTTChisq);
	m_chain->SetBranchAddress("CVProperModID", &CVProperModID, &b_CVProperModID);
	m_chain->SetBranchAddress("CVProperTime", &CVProperTime, &b_CVProperTime);
	m_chain->SetBranchAddress("CVProperEne", &CVProperEne, &b_CVProperEne);
	m_chain->SetBranchAddress("CVProperFTTChisq", &CVProperFTTChisq, &b_CVProperFTTChisq);
	m_chain->SetBranchAddress("CVMaskingProperModID", &CVMaskingProperModID, &b_CVMaskingProperModID);
	m_chain->SetBranchAddress("CVMaskingProperTime", &CVMaskingProperTime, &b_CVMaskingProperTime);
	m_chain->SetBranchAddress("CVMaskingProperEne", &CVMaskingProperEne, &b_CVMaskingProperEne);
	m_chain->SetBranchAddress("CVMaskingProperFTTChisq", &CVMaskingProperFTTChisq, &b_CVMaskingProperFTTChisq);
	m_chain->SetBranchAddress("CC06ProperModID", &CC06ProperModID, &b_CC06ProperModID);
	m_chain->SetBranchAddress("CC06ProperTime", &CC06ProperTime, &b_CC06ProperTime);
	m_chain->SetBranchAddress("CC06ProperEne", &CC06ProperEne, &b_CC06ProperEne);
	m_chain->SetBranchAddress("CC06ProperFTTChisq", &CC06ProperFTTChisq, &b_CC06ProperFTTChisq);
	m_chain->SetBranchAddress("CC06MaskingProperModID", &CC06MaskingProperModID, &b_CC06MaskingProperModID);
	m_chain->SetBranchAddress("CC06MaskingProperTime", &CC06MaskingProperTime, &b_CC06MaskingProperTime);
	m_chain->SetBranchAddress("CC06MaskingProperEne", &CC06MaskingProperEne, &b_CC06MaskingProperEne);
	m_chain->SetBranchAddress("CC06MaskingProperFTTChisq", &CC06MaskingProperFTTChisq, &b_CC06MaskingProperFTTChisq);
	m_chain->SetBranchAddress("NewVetoCondition", &NewVetoCondition, &b_NewVetoCondition);

      }

   }

}

