#ifndef E14ANA_GIRILIBREC_SPECIALREC2G_H
#define E14ANA_GIRILIBREC_SPECIALREC2G_H

#include <TMath.h>
#include "rec2g/Rec2g.h"

namespace GiriLib
{
  class SpecialRec2g : public Rec2g
  {
  public:
    Pi0 RecX2gWithXYZM( const std::list<Cluster> &ClusterList, 
			const Double_t VtxX,
			const Double_t VtxY,
			const Double_t VtxZ,
			const Double_t XMass,
			Double_t &dCosTheta,
			Double_t &dMass
			);
    void UpdateMomentum( Gamma &gamma, const CLHEP::Hep3Vector Vertex );
  };
}

#endif
