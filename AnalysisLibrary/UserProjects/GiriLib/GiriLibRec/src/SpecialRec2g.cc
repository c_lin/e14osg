#include "GiriLibRec/SpecialRec2g.h"

#include "E14Fsim/E14FsimFunction.h"
#include "gnana/E14GNAnaFunction.h"

namespace GiriLib
{
  Pi0 SpecialRec2g::RecX2gWithXYZM( const std::list<Cluster> &ClusterList,
				    const Double_t VtxX,
				    const Double_t VtxY,
				    const Double_t VtxZ,
				    const Double_t XMass,
				    Double_t &dCosTheta,
				    Double_t &dMass
				    )
  {
    std::list<Gamma> GammaList;
    if( ClusterList.size()!=2 )
      std::cout << "Bud N Cluster" << std::endl;
    
    const CLHEP::Hep3Vector Vtx(VtxX,VtxY,VtxZ);
    
    Int_t id = 0;
    for( std::list<Cluster>::const_iterator clus = ClusterList.begin() ; clus != ClusterList.end() ; ++clus ){
      ///// Set gamma from cluster
      ///// following GammaFinder::findGammaDefault
      Gamma gam(*clus);
      E14GNAnaFunction::getFunction()->correctEnergy( gam );
      gam.setId(id++);
      Double_t sigmaE  = E14FsimFunction::getFunction()->csiEnergyRes(gam.e()/1000.)*1000.;
      gam.setSigmaE(sigmaE);
      Double_t sigmaXY = E14FsimFunction::getFunction()->csiPosRes(gam.e()/1000.); //mm
      gam.setSigmaPos(sigmaXY,sigmaXY,0);
      
      ///// set gamma momentum
      UpdateMomentum( gam, Vtx );
      
      ///// gamma position correction
      E14GNAnaFunction::getFunction()->correctPosition(gam);
      //UpdateMomentum( gam, Vtx );

      ///// gamma energy correction
      E14GNAnaFunction::getFunction()->correctEnergyWithAngle(gam);
      UpdateMomentum( gam, Vtx );
      
      GammaList.push_back( gam );
    }
    
    
    Pi0 pi0;
    for(   std::list<Gamma>::iterator g1 = GammaList.begin() ; g1 != GammaList.end() ; ++g1 ){
      for( std::list<Gamma>::iterator g2 = GammaList.begin() ; g2 != GammaList.end() ; ++g2 ){
	if( g1->id() >= g2->id() )
	  continue;  // skip this combination (actually only one combination is allowed)
	Double_t CosThetaRec = g1->p3().dot( g2->p3() ) / ( g1->p3().mag() * g2->p3().mag() );
	Double_t CosThetaKin = 1. - XMass*XMass / ( 2. * g1->e() * g2->e() );
	dCosTheta = CosThetaKin - CosThetaRec;

	Double_t RecMass = sqrt( 2.* g1->e() * g2->e() * (1. - CosThetaRec) );
	dMass = RecMass - XMass;

	///// input Pi0 information
	pi0.setId(0);
	pi0.setRecZ( Vtx.z() );
	pi0.setRecZsig2( 0 );
	pi0.setVtx( Vtx );
	pi0.setGamma( *g1, *g2 );
	pi0.setStatus( 1 );

	pi0.updateVars();
      }
    }
    
    return pi0;
  }
  
  
  void SpecialRec2g::UpdateMomentum( Gamma &gamma, const CLHEP::Hep3Vector Vertex )
  {
    CLHEP::Hep3Vector Mom3 = gamma.pos() - Vertex;
    Mom3.setMag( gamma.e() );
    gamma.setP3( Mom3 );
  }
  
}
