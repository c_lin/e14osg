#include "GiriLibCSI/ThetaChi2.h"

namespace GiriLib
{
  ThetaChi2::ThetaChi2()
  {
    ///// Load Angle Resolution
    TString my_top_dir = std::getenv("MY_TOP_DIR");
    fresol  = new TFile(my_top_dir+"/AnalysisLibrary/UserProjects/GiriLib/GiriLibCSI/data/ThetaChi2/weights/AngleResol.root","");
    if( !fresol->IsOpen() ){
      std::cout << "file open error !! : " << fresol->GetName() << std::endl;
      exit(1);
    }
    hresol1 = (TH2F*)fresol->Get("hresol1");
    hresol2 = (TH2F*)fresol->Get("hresol2");
    hmshift = (TH2F*)fresol->Get("hmshift");
    
    ///// Load CSI map
    std::ifstream mapfile;
    TString mapfilename = my_top_dir+"/AnalysisLibrary/UserProjects/GiriLib/GiriLibCSI/data/ThetaChi2/csimap";
    mapfile.open( mapfilename);
    if( mapfile.fail() ){
      std::cout<< "file open error !! : " << mapfilename.Data() << std::endl;
      exit(1);
    }
    Int_t icsi = 0;
    Double_t x_tmp, y_tmp, w_tmp;
    while( mapfile >> icsi >> x_tmp >> y_tmp >> w_tmp ) {
      csix[icsi] = x_tmp*10.;// [cm] --> [mm]
      csiy[icsi] = y_tmp*10.;// [cm] --> [mm]
      csiw[icsi] = w_tmp*10.;// [cm] --> [mm]
    }
    mapfile.close();
    
    TMVA::Tools::Instance();
    // --- Create the Reader object
    reader = new TMVA::Reader( "!Color:!Silent" );
    // Declare MLP input variables to the reader
    reader->AddVariable( "xpeak", &xpeak );
    reader->AddVariable( "sigma", &sigma );
    reader->AddVariable( "alpha", &alpha );
    reader->AddVariable( "r1f", &r1f );
    reader->AddVariable( "r2f", &r2f );
    reader->AddVariable( "r3f", &r3f );
    reader->AddVariable( "r4f", &r4f );
    reader->AddVariable( "r1b", &r1b );
    reader->AddVariable( "r2b", &r2b );
    reader->AddVariable( "r3b", &r3b );
    reader->AddVariable( "r4b", &r4b );  
    reader->AddVariable( "r1f1", &r1f1 );
    reader->AddVariable( "r1f2", &r1f2 );
    reader->AddVariable( "r1f3", &r1f3 );
    reader->AddVariable( "r2f1", &r2f1 );
    reader->AddVariable( "r2f2", &r2f2 );
    reader->AddVariable( "r2f3", &r2f3 );
    reader->AddVariable( "r1b1", &r1b1 );
    reader->AddVariable( "r1b2", &r1b2 );
    reader->AddVariable( "r1b3", &r1b3 );
    reader->AddVariable( "r2b1", &r2b1 );
    reader->AddVariable( "r2b2", &r2b2 );
    reader->AddVariable( "r2b3", &r2b3 );
    reader->AddVariable( "ClusterDepE", &ClusterDepE );
    reader->AddVariable( "recphi", &recphi );
    reader->AddVariable( "recr", &recr );
    // --- Book the MVA methods
    TString methodName = "MLP method";
    TString weightfilename = my_top_dir + "/AnalysisLibrary/UserProjects/GiriLib/GiriLibCSI/data/ThetaChi2/weights/TMVARegression_MLP.weights.xml";
    struct stat st;
    if( stat(weightfilename.Data(),&st) != 0 ){
      std::cout<< "file open error !! : " << weightfilename.Data() << std::endl;
      exit(1);
    }
    
    reader->BookMVA( methodName, weightfilename );
    
    hx = new TH1F("hx","",160,-300,500);
    fx = new TF1("fx",AsymGaus,-100,100,4);
  }
  
  ThetaChi2::~ThetaChi2()
  {
    delete fresol;
    delete reader;
    //delete hx;
    delete fx;
  }
  
  void ThetaChi2::Eval( const Gamma &gamma, const Double_t VtxX, const Double_t VtxY, const Double_t VtxZ,
			Float_t &NNTheta, Float_t &ThetaChisq, Bool_t SpecialFlag, Int_t userFlag )
  {
    Initialize();
    
    ClusterDepE   = gamma.edep();
    Double_t coex = gamma.coex();
    Double_t coey = gamma.coey();

    Double_t CSIZPosition = 6148.; // mm
    if( userFlag>20160101 ) CSIZPosition = 6148. +20.;//mm
    
    //Double_t phi = gamma.p3().phi()*TMath::RadToDeg();
    Double_t phi = atan2(coey-VtxY,coex-VtxX);
    Double_t CosPhi = cos(phi);
    Double_t SinPhi = sin(phi);
    recphi = atan2( fabs(coey-VtxY), fabs(coex-VtxX) )*TMath::RadToDeg(); 
    if( recphi > 45. )
      recphi = 90. - recphi;
    recr = sqrt( pow(coex-VtxX,2) + pow(coey-VtxY,2) );
    
    ///// set the "hx" histogram bin-contents
    for( Int_t ic = 0 ; ic < (Int_t)gamma.clusterIdVec().size() ; ++ic ){
      Int_t    id = gamma.clusterIdVec()[ic];
      Double_t ie = gamma.clusterEVec()[ic];
      Double_t ix = csix[id] - coex;
      Double_t iy = csiy[id] - coey;
      Double_t rx =  ix*CosPhi + iy*SinPhi;// the scalar projection of the 'COE-->crystal' vector onto gamma-direction vector
      Int_t xbin = hx->FindBin(rx);
      for( Int_t i = -3 ; i <= 3 ; ++i ) {
	if( xbin+i < 1 || xbin +i > 160 ) continue;
	hx->SetBinContent(xbin+i,hx->GetBinContent(xbin+i)+ie);
      }
    }
    ///// fit hx with fx to get xpeak/sigma/alpha
    hx->Smooth(1);
    fx->SetParameters(100.,hx->GetMean(),50.,0);
    for(Int_t i = 0 ; i < 4 ; ++i)
      fx->SetParError(i,0);
    fx->FixParameter(0,100.);
    fx->SetParLimits(1,-100.,100.); // sigma
    fx->SetParLimits(2,5.,150.); // sigma
    fx->SetParLimits(3,0.,2.); // alpha
    hx->Fit("fx","QLN");
    xpeak = fx->GetParameter(1);
    sigma = fx->GetParameter(2);
    alpha = fx->GetParameter(3);
    
    //std::cout << "(hx_mean, hx_RSM) = ( " << hx->GetMean() <<", "<< hx->GetRMS() <<" ) "<< std::endl;
    //std::cout << "(xpeak, sigma, alpha) = ( " << xpeak <<", "<< sigma <<", "<< alpha <<" ) "<< std::endl;
    
    ///// calculate the other MLP input parameters  
    for( Int_t ic = 0 ; ic < (Int_t)gamma.clusterIdVec().size() ; ++ic ){
      Int_t    id = gamma.clusterIdVec()[ic];
      Double_t ie = gamma.clusterEVec()[ic];
      Double_t ix = csix[id] - coex;
      Double_t iy = csiy[id] - coey;
      Double_t rx =  ix*CosPhi + iy*SinPhi - xpeak;
      
      const Double_t interval = 20;
      if( rx > 0 ) {
	r1f += rx/10.        * ie/ClusterDepE;
	r2f += pow(rx/10.,2) * ie/ClusterDepE;
	r3f += pow(rx/10.,3) * ie/ClusterDepE;
	r4f += pow(rx/10.,4) * ie/ClusterDepE;
	
	if( fabs(rx) < interval ){
	  r1f1 += rx/10.        * ie/ClusterDepE;
	  r2f1 += pow(rx/10.,2) * ie/ClusterDepE;
	}
	if( fabs(rx) < interval*2. ){
	  r1f2 += rx/10.        * ie/ClusterDepE;
	  r2f2 += pow(rx/10.,2) * ie/ClusterDepE;
	}
	if( fabs(rx) < interval*3. ){  
	  r1f3 += rx/10.        * ie/ClusterDepE;
	  r2f3 += pow(rx/10.,2) * ie/ClusterDepE;
	}
      }
      if( rx < 0 ) {
	r1b += rx/10.        * ie/ClusterDepE;
	r2b += pow(rx/10.,2) * ie/ClusterDepE;
	r3b += pow(rx/10.,3) * ie/ClusterDepE;
	r4b += pow(rx/10.,4) * ie/ClusterDepE;
	if( fabs(rx) < interval ){
	  r1b1 += rx/10.        * ie/ClusterDepE;
	  r2b1 += pow(rx/10.,2) * ie/ClusterDepE;
	}
	if( fabs(rx) < interval*2. ){
	  r1b2 += rx/10.        * ie/ClusterDepE;
	  r2b2 += pow(rx/10.,2) * ie/ClusterDepE;
	}
	if( fabs(rx) < interval*3. ){
	  r1b3 += rx/10.        * ie/ClusterDepE;
	  r2b3 += pow(rx/10.,2) * ie/ClusterDepE;
	}
      }
    }
    
    ///// Evaluate NNTheta
    NNTheta = (reader->EvaluateRegression("MLP method"))[0];
    if( NNTheta < 0.  ) NNTheta = 0.;
    if( NNTheta > 90. ) NNTheta = 89.;
    
    ///// Evaluate ThetaChisq
    Double_t RecTheta;
    if( SpecialFlag )
      RecTheta = gamma.p3().theta()*TMath::RadToDeg();// not correct
    else
      RecTheta = atan2( sqrt( pow(coex-VtxX,2) + pow(coey-VtxY,2) ), CSIZPosition - VtxZ) * TMath::RadToDeg();
    Double_t ThetaErr;// =  hresol->Interpolate(ClusterDepE,NNTheta[ig]);
    Double_t mshift =  hmshift->Interpolate(ClusterDepE,NNTheta);
    if( RecTheta - NNTheta < mshift )
      ThetaErr = hresol1->Interpolate(ClusterDepE,NNTheta);
    else
      ThetaErr = hresol2->Interpolate(ClusterDepE,NNTheta);
    
    NNTheta += mshift;
    ThetaChisq = pow(NNTheta-RecTheta,2) / pow(ThetaErr,2);

  }
  
  void ThetaChi2::Initialize()
  {
    hx->Reset();
    
    r1f = 0.; r2f = 0.; r3f = 0.; r4f = 0.;
    r1b = 0.; r2b = 0.; r3b = 0.; r4b = 0.;
    r1f1 = 0.; r1f2 = 0.; r1f3 = 0.; 
    r2f1 = 0.; r2f2 = 0.; r2f3 = 0.; 
    r1b1 = 0.; r1b2 = 0.; r1b3 = 0.; 
    r2b1 = 0.; r2b2 = 0.; r2b3 = 0.; 
  }
  
  Double_t ThetaChi2::AsymGaus(Double_t *x, Double_t *par)
  {
    Double_t area   = par[0];
    Double_t mean   = par[1];
    Double_t sigma0 = par[2];
    Double_t alpha  = par[3];
    Double_t X      = x[0] - mean;
    Double_t sigma  = alpha*X + sigma0;
    
    return area*exp(-(X*X)/(2.*sigma*sigma));
  }
  
  Double_t ThetaChi2::GetThetaResol( Double_t ene )
  {
    //  Double_t par[4] = {3.04034e+00,-6.90769e-04,2.43612e+00,-1.99144e-03};
    static const Double_t par[4] = {3.11270e+00,-7.28931e-04,2.18122e+00,-2.84356e-03};
    return par[0]+par[1]*ene+exp(par[2]+par[3]*ene);
  }
  
}
