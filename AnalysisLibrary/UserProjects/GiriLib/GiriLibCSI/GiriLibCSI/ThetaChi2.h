#ifndef E14ANA_GIRILIBREC_THETACHI2_H
#define E14ANA_GIRILIBREC_THETACHI2_H

#include <cstdlib>
#include <vector>
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <sys/stat.h>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"

#include "TMVA/Tools.h"
#include "TMVA/Reader.h"

#include "gamma/Gamma.h"

namespace GiriLib
{
  class ThetaChi2
  {
  public:
    ThetaChi2();
    ~ThetaChi2();
    
    void Eval( const Gamma &gamma, const Double_t VtxX, const Double_t VtxY, const Double_t VtxZ,
    	       Float_t &NNTheta, Float_t &ThetaChisq,
	       const Bool_t SpecialFlag = false, const Int_t userFlag = 20151101);
    void Eval( const Gamma &gamma, const Double_t VtxZ,
	       Float_t &NNTheta, Float_t &ThetaChisq,
	       const Bool_t SpecialFlag = false, const Int_t userFlag = 20151101){ Eval( gamma, 0, 0, VtxZ,
											 NNTheta, ThetaChisq,
											 SpecialFlag, userFlag ); }
    
  private:
    void            Initialize();
    static Double_t AsymGaus(Double_t *x,Double_t *par);
    Double_t        GetThetaResol( Double_t ene );
    
    static const Int_t NCSI = 2716;
    Double_t csix[NCSI];
    Double_t csiy[NCSI];
    Double_t csiw[NCSI];
    
    TFile *fresol;
    TH2F  *hresol1;
    TH2F  *hresol2;
    TH2F  *hmshift;
    
    
    TMVA::Reader *reader;
    
    ///// a set of the MLP input variables
    Float_t ClusterDepE;
    Float_t xpeak, sigma, alpha;
    Float_t r1f, r2f, r3f, r4f;
    Float_t r1b, r2b, r3b, r4b;
    Float_t r1f1, r1f2, r1f3;
    Float_t r2f1, r2f2, r2f3;
    Float_t r1b1, r1b2, r1b3;
    Float_t r2b1, r2b2, r2b3;
    Float_t recphi;
    Float_t recr;
    
    TH1F *hx;
    TF1  *fx;
  };
}
#endif
