#include <iostream>
#include <fstream>

#include <TSystem.h>
#include <TChain.h>
#include <TString.h>

#include "GiriLibDst/GoodRunSpillManager.h"

namespace GiriLib
{
  GoodRunSpillManager::GoodRunSpillManager( const Char_t *DAQFileName )
  {
    tree = new TChain("tree");
    SetDAQFile(DAQFileName);
    CurrentRunID = -1;
    RunFindFlag  = false;
  }

  GoodRunSpillManager::GoodRunSpillManager( const Int_t RUN, const Int_t RunNo,
					    const Char_t *RunType, const Int_t Version )
  {
    TString FileName = Form( "%s/GoodRunSpill/%s/RUN%d/v%d/RunByRun/DAQSummary_%d.root",
			     std::getenv("E14ANA_EXTERNAL_DATA"), RunType,
			     RUN, Version, RunNo );
    tree = new TChain("tree");
    SetDAQFile(FileName.Data());
    CurrentRunID = -1;
    RunFindFlag  = false;
  }
  
  GoodRunSpillManager::~GoodRunSpillManager()
  {
    delete tree;
  }
  
  void GoodRunSpillManager::SetDAQFile( const Char_t *DAQFileName ) 
  { 
    tree->Add(DAQFileName);
    if(tree->GetBranch("runID")){ 
		tree->SetBranchAddress("runID",      &runID);
	}
	if(tree->GetBranch("RunID")){ 
		tree->SetBranchAddress("RunID",      &runID);
	}
    tree->SetBranchAddress("isGoodRun",  &isGoodRun);
    tree->SetBranchAddress("TotalnSpill",&TotalnSpill);
    tree->SetBranchAddress("SpillID",     SpillID);
    tree->SetBranchAddress("isGoodSpill", isGoodSpill);
    if( tree->GetEntries()==0 ){
      std::cout << "ERROR in GoodRunSpillManager::SetDAQFile() : DAQ summary file is not found." << std::endl;
    }
  }
  
  void GoodRunSpillManager::SetRunID( const Int_t RunNo )
  {
    if( RunNo==CurrentRunID )
      return;

    RunFindFlag = false;
    
    for (Long64_t ientry = 0 ; ientry < tree->GetEntries() ; ++ientry){
      tree->GetEntry(ientry);
      if( runID==RunNo ){
	RunFindFlag = true;
	CurrentRunID = RunNo;
	break;
      }
    }
  }
  
  Bool_t GoodRunSpillManager::IsGoodRun()
  {
    if( !RunFindFlag ) return false;
    return (isGoodRun==1)? true : false ;
  }
  
  
  Bool_t GoodRunSpillManager::IsGoodSpill( const Short_t SpillNo )
  {
    if( !RunFindFlag ) return false;

    if( SpillNo < 0 ){
      std::cout << "ERROR in GoodRunSpillManager::GetIsGoodSpill() : SpillNo is negative." << std::endl;
      return 0;
    }
    UInt_t isGood = 0;
    for(UInt_t ispill = 0 ; ispill < TotalnSpill ; ++ispill){
      if( static_cast<Short_t>(SpillID[ispill]) == SpillNo ){
	isGood = isGoodSpill[ispill];
	break;
      }
    }
    return (isGood==1)? true : false;
  }
  
}
