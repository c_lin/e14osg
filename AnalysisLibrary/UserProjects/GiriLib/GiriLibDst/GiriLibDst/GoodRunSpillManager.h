#ifndef E14ANA_GIRILIBDST_GOODRUNSPILLMANAGER_H
#define E14ANA_GIRILIBDST_GOODRUNSPILLMANAGER_H

#include <TChain.h>
#include <TString.h>
#include <TFile.h>

namespace GiriLib
{
  
  class GoodRunSpillManager
  {
  public:
    GoodRunSpillManager( const Char_t *DAQFileName );
    GoodRunSpillManager( const Int_t RUN, const Int_t RunNo, 
			 const Char_t *RunType, const Int_t Version );// RunType : { Physics, Z0Al, DVUAl }, Version : Version of DAQ summary file
    ~GoodRunSpillManager();
    void     SetRunID( const Int_t RunNo );
    Bool_t   IsGoodRun();
    Bool_t   IsGoodSpill( const Short_t SpillNo );
    
  private:
    void   SetDAQFile( const Char_t *DAQFileName );

    TChain *tree;
    Int_t  runID;  
    UInt_t isGoodRun;
    UInt_t TotalnSpill;  
    UInt_t SpillID[1000];
    UInt_t isGoodSpill[1000];
  
    Bool_t RunFindFlag;
    Int_t  CurrentRunID;
    
  };
}

#endif
