/* -*- C++ -*- */
#ifndef E14ANA_E14BASICPARAMMANAGER_E14BASICPARAMMANAGER_H
#define E14ANA_E14BASICPARAMMANAGER_E14BASICPARAMMANAGER_H

#include <TObject.h>
#include "TChain.h"

namespace E14BasicParamManager
{
  class E14BasicParamManager/* : public TObject*/
  {
  public:
    E14BasicParamManager();
    ~E14BasicParamManager();

    void initializeDataValues();

    void   SetDetBasicParam(const int userFlag, const std::string name, int chID=-1);
    void   SetE14BasicParamFromText(const std::string FileName);
    void   SetE14BasicParamFromRootFile(const std::string FileName, const int userFlag, const std::string detName, int chID=-1);
    void   SetUserFlag(const int RunID);

    double GetNominalAverageClusterTime_Data() const { return m_NominalAverageClusterTime_Data; }
    double GetNominalAverageClusterTime_MC()   const { return m_NominalAverageClusterTime_MC; }
    double GetDetZPosition()  const { return m_DetZPosition; }
    double GetG2DPMinPeakHeight() const { return m_G2DPMinPeakHeight; }
    double GetG2DPNominalPeakTime() const { return m_G2DPNominalPeakTime; }
    int    GetNDeadChannels() const { return m_NDeadChannels; }
    std::vector<int> GetDeadChannelIDVec() const { return m_DeadChannelIDVec; }
    int    GetNDeadModules() const { return m_NDeadModules; }
    std::vector<int> GetDeadModuleIDVec() const { return m_DeadModuleIDVec; }
    double GetG2DPSmearingSigma() const { return m_G2DPSmearingSigma; }

    int    GetUserFlag() const { return m_userFlag; }

    void   GetArrayNominalPeakTime( float Input_NPT[4096] );
    void   GetArrayMinPeakHeight(   float Input_MPH[4096] );
    void   GetArrayRangeMin( int Input_RangeMin[4096] );
    void   GetArrayRangeMax( int Input_RangeMax[4096] );
    void   GetArraynSampleIntegral( int Input_nSampleIntegral[4096] );
    void   GetArraynSampleIntegralBeforePeak( int Input_nSampleIntegralBeforePeak[4096] );

  private:

    TChain *tri;
    
    double m_NominalAverageClusterTime_Data; //[ns], average cluster time from gXana in data
    double m_NominalAverageClusterTime_MC;   //[ns], average cluster time from gXana in MC
    double m_DetZPosition;  // [mm], detector Z position
    double m_G2DPMinPeakHeight; // [FADC count], gsim2dst parameter, for minimum peak height threshold
    double m_G2DPNominalPeakTime; // [clock], gsim2dst parameter, for parabola time calculation
    int    m_NDeadChannels; // number of dead channels
    std::vector<int> m_DeadChannelIDVec; // [e14ID], dead channel ID
    int    m_NDeadModules; // number of dead moduless
    std::vector<int> m_DeadModuleIDVec; // [e14ID], dead module ID
    double m_G2DPSmearingSigma; // [ns], smearing for timing calculation

    float m_NPT[4096];
    float m_MPH[4096];
    int   m_RangeMin[4096];
    int   m_RangeMax[4096];
    int   m_nSampleIntegral[4096];
    int   m_nSampleIntegralBeforePeak[4096];

    int   m_userFlag; // userFlag based on runID

    void SetDataFBAR(const int userFlag, int chID=-1);
    void SetDataNCC(const int userFlag,  int chID=-1);
    void SetDataCBAR(const int userFlag, int chID=-1);
    void SetDataBCV(const int userFlag,  int chID=-1);
    void SetDataIB(const int userFlag,   int chID=-1);
    void SetDataIBCV(const int userFlag, int chID=-1);
    void SetDataMBCV(const int userFlag, int chID=-1);
    void SetDataOEV(const int userFlag,  int chID=-1);
    void SetDataCSI(const int userFlag,  int chID=-1);
    void SetDataCV(const int userFlag,   int chID=-1);
    void SetDataCVF(const int userFlag,  int chID=-1);
    void SetDataCVR(const int userFlag,  int chID=-1);
    void SetDataLCV(const int userFlag,  int chID=-1);
    void SetDataCC03(const int userFlag, int chID=-1);
    void SetDataCC04(const int userFlag, int chID=-1);
    void SetDataCC05(const int userFlag, int chID=-1);
    void SetDataCC06(const int userFlag, int chID=-1);
    void SetDataBPCV(const int userFlag, int chID=-1);
    void SetDataBHCV(const int userFlag, int chID=-1);
    void SetDatanewBHCV(const int userFlag, int chID=-1);
    void SetDataBHPV(const int userFlag, int chID=-1);
    void SetDataBHGC(const int userFlag, int chID=-1);
    void SetDataDCV(const int userFlag,  int chID=-1);
    void SetDataDCV1(const int userFlag, int chID=-1);
    void SetDataDCV2(const int userFlag, int chID=-1);
    void SetDataUCV(const int userFlag,  int chID=-1);
    void SetDataUCVLG(const int userFlag,int chID=-1);

  };

}

#endif
