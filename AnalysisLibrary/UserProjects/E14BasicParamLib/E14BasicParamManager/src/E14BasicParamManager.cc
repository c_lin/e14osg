#include "E14BasicParamManager/E14BasicParamManager.h"

#include <iostream>

namespace E14BasicParamManager
{
  
  E14BasicParamManager::E14BasicParamManager()
  {

    tri = new TChain("conf_E14BP");
    initializeDataValues();

  }

  E14BasicParamManager::~E14BasicParamManager()
  {
    delete tri;
  }
  
  void E14BasicParamManager::initializeDataValues()
  {

    m_DetZPosition  = -9999;
    m_NDeadChannels = 0;
    m_NDeadModules = 0;
    m_G2DPMinPeakHeight   = 9999;
    m_G2DPNominalPeakTime = 9999;

    m_DeadChannelIDVec.clear();
    m_DeadModuleIDVec.clear();

    m_G2DPSmearingSigma = 0;

    m_NominalAverageClusterTime_Data = 215;
    m_NominalAverageClusterTime_MC   = 215;

    m_userFlag = 20151101;

    for(int i=0;i<4096;i++){
      m_NPT[i] = 9999;
      m_MPH[i] = 9999;
      m_RangeMin[i] = 9999;
      m_RangeMax[i] = 9999;
      m_nSampleIntegral[i] = 9999;
      m_nSampleIntegralBeforePeak[i] = 9999;
    }

  }

  void E14BasicParamManager::SetDetBasicParam(const int userFlag, const std::string name, int chID)
  {

    initializeDataValues();
    
    if (name=="FBAR") {
      SetDataFBAR(userFlag, chID);
    } else if (name=="NCC") {
      SetDataNCC(userFlag, chID);
    } else if (name=="CBAR") {
      SetDataCBAR(userFlag, chID);
    } else if (name=="BCV") {
      SetDataBCV(userFlag, chID);
    } else if (name=="IB") {
      SetDataIB(userFlag, chID);
    } else if (name=="IBCV") {
      SetDataIBCV(userFlag, chID);
    } else if (name=="MBCV") {
      SetDataMBCV(userFlag, chID);
    } else if (name=="OEV") {
      SetDataOEV(userFlag, chID);
    } else if (name=="CSI") {
      SetDataCSI(userFlag, chID);
    } else if (name=="CV") {
      SetDataCV(userFlag, chID);
    } else if (name=="CVF") {
      SetDataCVF(userFlag, chID);
    } else if (name=="CVR") {
      SetDataCVR(userFlag, chID);
    } else if (name=="LCV") {
      SetDataLCV(userFlag, chID);
    } else if (name=="CC03") {
      SetDataCC03(userFlag, chID);
    } else if (name=="CC04") {
      SetDataCC04(userFlag, chID);
    } else if (name=="CC05") {
      SetDataCC05(userFlag, chID);
    } else if (name=="CC06") {
      SetDataCC06(userFlag, chID);
    } else if (name=="BPCV") {
      SetDataBPCV(userFlag, chID);
    } else if (name=="BHCV") {
      SetDataBHCV(userFlag, chID);
    } else if (name=="newBHCV") {
      SetDatanewBHCV(userFlag, chID);
    } else if (name=="BHPV") {
      SetDataBHPV(userFlag, chID);
    } else if (name=="BHGC") {
      SetDataBHGC(userFlag, chID);
    } else if (name=="DCV") {
      SetDataDCV(userFlag, chID);
    } else if (name=="DCV1") {
      SetDataDCV1(userFlag, chID);
    } else if (name=="DCV2") {
      SetDataDCV2(userFlag, chID);
    } else if (name=="UCV") {
      SetDataUCV(userFlag, chID);
    } else if (name=="UCVLG") {
      SetDataUCVLG(userFlag, chID);
    } else {
      std::cout<<"Unexpected detector was asigned to E14BasicParamManager"<<std::endl;
      initializeDataValues();
    }

  }

  void E14BasicParamManager::SetE14BasicParamFromText(const std::string FileName)
  {
    initializeDataValues();

    FILE *fp;
    fp = fopen( FileName.c_str() ,"rt");
    if (fp == NULL) {
      std::cout<<"SetE14BasicParamFromText : E14BP file "
	       <<FileName.c_str()
	       <<" is not found."<<std::endl;
      return;
    }
    std::cout<<"SetE14BasicParamFromText : E14BP file "
	     <<FileName.c_str()
	     <<" is opened."<<std::endl;

    fscanf(fp,"%lf\n",&m_DetZPosition);
    fscanf(fp,"%lf\n",&m_G2DPMinPeakHeight);
    fscanf(fp,"%lf\n",&m_G2DPNominalPeakTime);
    fscanf(fp,"%d\n",&m_NDeadChannels);
    for(int i=0;i<m_NDeadChannels;i++){
      int tmp_deadchid=9999;
      fscanf(fp,"%d\n",&tmp_deadchid);
      m_DeadChannelIDVec.push_back(tmp_deadchid);
    }
    fscanf(fp,"%d\n",&m_NDeadModules);
    for(int i=0;i<m_NDeadModules;i++){
      int tmp_deadmodid=9999;
      fscanf(fp,"%d\n",&tmp_deadmodid);
      m_DeadModuleIDVec.push_back(tmp_deadmodid);
    }
    fclose(fp);


  }
  
  void E14BasicParamManager::SetE14BasicParamFromRootFile(const std::string FileName, const int userFlag, const std::string detName, int chID)
  {
    initializeDataValues();

    tri->Reset();
    tri->Add( FileName.c_str() );
    if ( tri->GetEntries()==0 ) {
      std::cout<<"SetE14BasicParamFromRootFile : E14BP file "
	       <<FileName.c_str()
	       <<" is not found."<<std::endl;
      return;
    }

    Int_t userFlag_ini = 20151101;
    Int_t userFlag_end = 20151101;
    Double_t DetZPosition[4096];
    Double_t MinPeakHeight[4096];
    Double_t NominalPeakTime[4096];
    Int_t NDeadChannels;
    Int_t DeadChannelID[4096];
    Int_t NDeadModules;
    Int_t DeadModuleID[4096];
    Double_t SmearingSigma[4096];
    Int_t RangeMin[4096];
    Int_t RangeMax[4096];
    Int_t nSampleIntegral[4096];
    Int_t nSampleIntegralBeforePeak[4096];
    Double_t NominalAverageClusterTime_Data = 215;
    Double_t NominalAverageClusterTime_MC   = 215;


    tri->SetBranchAddress("userFlag_ini", &userFlag_ini);
    tri->SetBranchAddress("userFlag_end", &userFlag_end);
    tri->SetBranchAddress(Form( "%sZPosition",detName.c_str() ),        DetZPosition);
    tri->SetBranchAddress(Form( "%sMinPeakHeight",detName.c_str() ),    MinPeakHeight);
    tri->SetBranchAddress(Form( "%sNominalPeakTime",detName.c_str() ),  NominalPeakTime);
    tri->SetBranchAddress(Form( "%sNDeadChannels",detName.c_str() ),   &NDeadChannels);
    tri->SetBranchAddress(Form( "%sDeadChannelID",detName.c_str() ),    DeadChannelID);
    tri->SetBranchAddress(Form( "%sNDeadModules",detName.c_str() ),    &NDeadModules);
    tri->SetBranchAddress(Form( "%sDeadModuleID",detName.c_str() ),     DeadModuleID);
    tri->SetBranchAddress(Form( "%sSmearingSigma",detName.c_str() ),    SmearingSigma);
    tri->SetBranchAddress(Form( "%sRangeMin",detName.c_str() ),         RangeMin);
    tri->SetBranchAddress(Form( "%sRangeMax",detName.c_str() ),         RangeMax);
    tri->SetBranchAddress(Form( "%snSampleIntegral",detName.c_str() ),  nSampleIntegral);
    tri->SetBranchAddress(Form( "%snSampleIntegralBeforePeak",detName.c_str() ), nSampleIntegralBeforePeak);
    tri->SetBranchAddress("NominalAverageClusterTime_Data", &NominalAverageClusterTime_Data);
    tri->SetBranchAddress("NominalAverageClusterTime_MC",   &NominalAverageClusterTime_MC);

    bool isSetE14BPParamFromRootFile = false;
    const int nevt = tri->GetEntries();
    for(int ievt=0;ievt<nevt;ievt++){
      tri->GetEntry(ievt);
      if( userFlag_ini<=userFlag && userFlag<=userFlag_end){
	isSetE14BPParamFromRootFile = true;
	break;
      }
    }

    if( !isSetE14BPParamFromRootFile ){
      std::cout<<"SetE14BasicParamFromRootFile : E14BP file "
	       <<FileName.c_str()
	       <<" was not set correctly due to wrong userFlag range."<<std::endl;
      return;
    }
    
    m_DetZPosition = DetZPosition[chID];

    m_G2DPMinPeakHeight   = MinPeakHeight[chID];
    m_G2DPNominalPeakTime = NominalPeakTime[chID];

    m_G2DPSmearingSigma = SmearingSigma[chID];

    m_NominalAverageClusterTime_Data = NominalAverageClusterTime_Data;
    m_NominalAverageClusterTime_MC   = NominalAverageClusterTime_MC;

    m_NDeadChannels = NDeadChannels;
    for(int idead=0;idead<NDeadChannels;idead++) m_DeadChannelIDVec.push_back(DeadChannelID[idead]);
    m_NDeadModules = NDeadModules;
    for(int idead=0;idead<NDeadModules;idead++) m_DeadModuleIDVec.push_back(DeadModuleID[idead]);

    for(int ich=0;ich<4096;ich++){
      m_NPT[ich] = NominalPeakTime[ich];
      m_MPH[ich] = MinPeakHeight[ich];
      m_RangeMin[ich] = RangeMin[ich];
      m_RangeMax[ich] = RangeMax[ich];
      m_nSampleIntegral[ich] = nSampleIntegral[ich];
      m_nSampleIntegralBeforePeak[ich] = nSampleIntegralBeforePeak[ich];
    }

  }

  void E14BasicParamManager::SetDataFBAR(const int userFlag, int chID)
  {
    if(chID<-1 || 32<=chID)std::cout<<"Warning! E14BasicParamManager FBAR wrong chID settings\n";

    m_DetZPosition = 0;
    m_G2DPSmearingSigma = 0.;

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015 run
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 30.9;
    } else if( 20160101<=userFlag && userFlag<20160615) { //RUN69 before amp
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 34.8;
    } else if( 20160615<=userFlag && userFlag<20170101) { //RUN69 after amp
      m_G2DPMinPeakHeight = 15;
      m_G2DPNominalPeakTime = 31.3;
    } else if( 20170101<=userFlag && userFlag<20170701) { //RUN75
      m_G2DPMinPeakHeight = 25;
      m_G2DPNominalPeakTime = 29.4;
    } else if( 20170701<=userFlag && userFlag<20180401) { //RUN78
      m_G2DPMinPeakHeight = 25;
      m_G2DPNominalPeakTime = 29.2;
      m_NDeadChannels = 1;
      m_DeadChannelIDVec.push_back(5);
      m_NDeadModules = 1;
      m_DeadModuleIDVec.push_back(5);
    } else if( 20180401<=userFlag && userFlag<20180701) { //RUN79
      m_G2DPMinPeakHeight = 25;
      m_G2DPNominalPeakTime = 29.2;
    } else if( 20190101<=userFlag && userFlag<=20190301 ){ // RUN81
      m_G2DPMinPeakHeight  = 25.;
      m_G2DPNominalPeakTime = 26.2;
    } else if( 20190301<=userFlag && userFlag<=20191201 ){ // RUN82
      m_G2DPMinPeakHeight  = 25.;
      m_G2DPNominalPeakTime = 27.2;
    } else if( 20200101<=userFlag ){ // RUN85~
      m_G2DPMinPeakHeight  = 25.;
      m_G2DPNominalPeakTime = 27.2;
    }

  }


  void E14BasicParamManager::SetDataNCC(const int userFlag, int chID)
  {

    if(chID<-1)std::cout<<"Error! E14BasicParamManager NCC wrong chID settings\n";

    bool isNCCCommon = false;
    bool isNCCIndi   = false;
    bool isNCCOuter  = false;
    bool isNCCScinti = false;
    if( chID<480 && chID%10==0 )                   isNCCCommon = true;
    else if( chID<480 && chID%10<4  )              isNCCIndi   = true;
    else if( 500<=chID && chID<600 && chID%10==0 ) isNCCOuter  = true;
    else if( 600<=chID && chID<640 && chID%10==0 ) isNCCScinti = true;
    else std::cout<<"Warning! E14BasicParamManager NCC wrong chID settings\n";

    // default value
    m_DetZPosition = 1990; // upstream surface
    m_G2DPSmearingSigma = 0.;
    m_G2DPMinPeakHeight = 25;
    m_G2DPNominalPeakTime = 30.0;

    if( isNCCCommon ){
      if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
	m_G2DPMinPeakHeight = 50;
	m_G2DPNominalPeakTime = 28.3;
      } else if( 20160101<=userFlag && userFlag<20160615) { //RUN69 before amp
	m_G2DPMinPeakHeight = 25;
	m_G2DPNominalPeakTime = 31.8;
	m_G2DPSmearingSigma = 71.98;
      } else if ( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPMinPeakHeight = 25;
	m_G2DPNominalPeakTime = 28.4;
	m_G2DPSmearingSigma = 71.98;
      } else if ( 20170101<=userFlag && userFlag<20170701) { // RUN75
	m_G2DPMinPeakHeight = 25;
	m_G2DPNominalPeakTime = 27.7;
	m_G2DPSmearingSigma = 71.98;
      } else if ( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPMinPeakHeight = 25;
	m_G2DPNominalPeakTime = 26.7;
	m_G2DPSmearingSigma = 71.98;
      } else if ( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPMinPeakHeight = 25;
	m_G2DPNominalPeakTime = 26.2;
	m_G2DPSmearingSigma = 71.98;
      } else if ( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
	m_G2DPMinPeakHeight  = 50.0;
	m_G2DPNominalPeakTime = 28.92;
	m_G2DPSmearingSigma = 71.98;
      } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
	m_G2DPMinPeakHeight  = 50.0;
	m_G2DPNominalPeakTime = 30.0;
	m_G2DPSmearingSigma = 71.98;
      } else if( 20200101<=userFlag ){ // RUN85~
	m_G2DPMinPeakHeight  = 50.0;
	m_G2DPNominalPeakTime = 30.0;
	m_G2DPSmearingSigma = 71.98;
      } 
    }else if( isNCCIndi ){
      if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
	m_G2DPMinPeakHeight = 50;
	m_G2DPNominalPeakTime = 28.3;
      } else if ( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPMinPeakHeight = 10;
	m_G2DPNominalPeakTime = 31.4;
      } else if ( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPMinPeakHeight = 10;
	m_G2DPNominalPeakTime = 28.1;
      } else if ( 20170101<=userFlag && userFlag<20170701) { // RUN75
	m_G2DPMinPeakHeight = 10;
	m_G2DPNominalPeakTime = 26.4;
      } else if ( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPMinPeakHeight = 10;
	m_G2DPNominalPeakTime = 26.6;
      } else if ( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPMinPeakHeight = 10;
	m_G2DPNominalPeakTime = 26.0;
      } else if ( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
	m_G2DPMinPeakHeight = 10;
	m_G2DPNominalPeakTime = 28.84;
      } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
	m_G2DPMinPeakHeight = 10;
	m_G2DPNominalPeakTime = 29.9;
      } else if( 20200101<=userFlag ){ // RUN85~
	m_G2DPMinPeakHeight = 10;
	m_G2DPNominalPeakTime = 29.9;
      }
    }else if( isNCCOuter ){
      m_G2DPMinPeakHeight = 10;

      if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
	m_G2DPNominalPeakTime = 28.3;
      } else if ( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 31.8;
      } else if ( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 28.4;
      } else if ( 20170101<=userFlag && userFlag<20170701) { // RUN75
	m_G2DPNominalPeakTime = 27.7;
      } else if ( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 26.7;
      } else if ( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 26.2;
      } else if ( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
	m_G2DPNominalPeakTime = 28.92;
      } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
	m_G2DPNominalPeakTime = 30.0;
      } else if( 20200101<=userFlag ){ // RUN85~
	m_G2DPNominalPeakTime = 30.0;
      }
    }else if( isNCCScinti ){
      m_G2DPMinPeakHeight = 50;

      if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
	m_G2DPNominalPeakTime = 28.3;
      } else if ( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 31.8;
      } else if ( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 28.4;
      } else if ( 20170101<=userFlag && userFlag<20170701) { // RUN75
	m_G2DPNominalPeakTime = 27.7;
      } else if ( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 26.7;
      } else if ( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 26.2;
      } else if ( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
	m_G2DPNominalPeakTime = 28.92;
      } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
	m_G2DPNominalPeakTime = 30.0;
      } else if( 20200101<=userFlag ){ // RUN85~
	m_G2DPNominalPeakTime = 30.0;
      }
    }
   
  }

  void E14BasicParamManager::SetDataCBAR(const int userFlag, int chID)
  {
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager CBAR wrong chID settings\n";

    bool isCBARUpstreamInner    = false;
    bool isCBARUpstreamOuter    = false;
    bool isCBARDownstreamInner  = false;
    bool isCBARDownstreamOuter  = false;
    if( chID<32 )                    isCBARUpstreamInner = true;
    else if( 32<=chID && chID<64 )   isCBARUpstreamOuter = true;
    else if( 100<=chID && chID<132 ) isCBARDownstreamInner  = true;
    else if( 132<=chID && chID<164 ) isCBARDownstreamOuter  = true;
    else std::cout<<"Warning! E14BasicParamManager CBAR wrong chID settings\n";

    m_DetZPosition = 1348 + 7; // upstream end
    m_G2DPSmearingSigma = 0.;
    m_G2DPNominalPeakTime = 30.75;

    // CBAR common parameter
    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_G2DPMinPeakHeight = 10;
    } else if( 20160101<=userFlag && userFlag<20170101) { // RUN69
      m_G2DPMinPeakHeight = 10;
      m_G2DPSmearingSigma = 3.96;
    } else if( 20170101<=userFlag && userFlag<20180701) { // from RUN74 to RUN79
      m_G2DPMinPeakHeight = 50;
      m_G2DPSmearingSigma = 3.96;
    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_G2DPMinPeakHeight = 50;
      m_G2DPSmearingSigma = 3.96;
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_G2DPMinPeakHeight = 50;
      m_G2DPSmearingSigma = 3.96;
    } else if( 20200101<=userFlag ){ // RUN85~
      m_G2DPMinPeakHeight = 50;
      m_G2DPSmearingSigma = 3.96;
    }

    if( isCBARUpstreamInner ){
      if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
	m_G2DPNominalPeakTime = 30.75;
      } else if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 37.1;
      } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 33.8;
      } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
	m_G2DPNominalPeakTime = 32.0;
      } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 32.0;
      } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 31.5;
      } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
	m_G2DPNominalPeakTime = 30.9;
      } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
	m_G2DPNominalPeakTime = 32.0;
      } else if( 20200101<=userFlag ){ // RUN85~
	m_G2DPNominalPeakTime = 32.0;
      }
    }else if( isCBARUpstreamOuter ){
      if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
	m_G2DPNominalPeakTime = 30.75;
      } else if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 37.8;
      } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 34.4;
      } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
	m_G2DPNominalPeakTime = 32.7;
      } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 32.7;
      } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 32.2;
      } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
	m_G2DPNominalPeakTime = 31.5;
      } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
	m_G2DPNominalPeakTime = 32.5;
      } else if( 20200101<=userFlag ){ // RUN85~
	m_G2DPNominalPeakTime = 32.5;
      }
    }else if( isCBARDownstreamInner ){
      if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
	m_G2DPNominalPeakTime = 30.75;
      } else if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 34.7;
      } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 31.2;
      } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
	m_G2DPNominalPeakTime = 29.3;
      } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 29.4;
      } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 28.9;
      } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
	m_G2DPNominalPeakTime = 28.2;
      } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
	m_G2DPNominalPeakTime = 29.2;
      } else if( 20200101<=userFlag ){ // RUN85~
	m_G2DPNominalPeakTime = 29.2;
      }
    }else if( isCBARDownstreamOuter ){
      if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
	m_G2DPNominalPeakTime = 30.75;
      } else if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 35.0;
      } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 31.7;
      } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
	m_G2DPNominalPeakTime = 29.8;
      } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 29.8;
      } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 29.3;
      } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
	m_G2DPNominalPeakTime = 28.6;
      } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
	m_G2DPNominalPeakTime = 29.7;
      } else if( 20200101<=userFlag ){ // RUN85~
	m_G2DPNominalPeakTime = 29.7;
      }
    }
  }

  void E14BasicParamManager::SetDataBCV(const int userFlag, int chID)
  {
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager BCV wrong chID settings\n";

    m_DetZPosition = 1348 + 7; // upstream end, same as CBAR

    if ( 20150401<=userFlag && userFlag<20160101) {
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 30.25;
    } else if( 20180701<=userFlag ) { //RUN8X~
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 30.25;
    }
  }

  void E14BasicParamManager::SetDataIB(const int userFlag, int chID)
  {
    if(chID<-1 || (32<=chID&&chID<100) || 132<=chID )
      std::cout<<"Warning! E14BasicParamManager IB wrong chID settings\n";

    m_DetZPosition = 1348 + 7 + 1639.5; // upstream end

    if( 20160101<=userFlag && userFlag<20170401 ){ // RUN69
      m_G2DPMinPeakHeight = 6; // not used;
      m_G2DPNominalPeakTime = 30.75;
    } else if( 20170401<=userFlag && userFlag<20180701 ){ // RUN74~RUN79
      m_G2DPMinPeakHeight = 6; // not used;
      m_G2DPNominalPeakTime = 30.75;
      m_NDeadChannels = 1;
      m_DeadChannelIDVec.push_back(23);
      m_NDeadModules = 1;
      m_DeadModuleIDVec.push_back(23);
    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_G2DPMinPeakHeight = 6; // not used;
      m_G2DPNominalPeakTime = 30.75;
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_G2DPMinPeakHeight = 6; // not used;
      m_G2DPNominalPeakTime = 30.75;
    } else if( 20200101<=userFlag ){ // RUN85~
      m_G2DPMinPeakHeight = 6; // not used;
      m_G2DPNominalPeakTime = 30.75;
    }

  }  

  void E14BasicParamManager::SetDataIBCV(const int userFlag, int chID)
  {
    if(chID<-1 || (32<=chID&&chID<100) || 132<=chID )
      std::cout<<"Warning! E14BasicParamManager IBCV wrong chID settings\n";

    m_DetZPosition = 1348 + 7 + 1639.5; // upstream end, same as IB
    m_G2DPMinPeakHeight = 10;

    if( 20160101<=userFlag && userFlag<20180701 ){ // RUN69~RUN79
      m_NDeadChannels = 1;
      m_DeadChannelIDVec.push_back(127);
      m_NDeadModules = 1;
      m_DeadModuleIDVec.push_back(27);
      if ( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 31.5;
      } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 before amp
	m_G2DPNominalPeakTime = 28.1;
      } else if ( 20170101<=userFlag && userFlag<20170701) { // RUN75
	m_G2DPNominalPeakTime = 26.3;
      } else if ( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 26.7;
      } else if ( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 25.6;
      }
    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      if(chID<0){
	std::cout << "Warning! MTBasicParamManager: IBCV chID setting is wrong!\n";
      }else if(chID<32){
	m_G2DPNominalPeakTime = 25.6;  
      }else if(100<=chID&&chID<132){
	m_G2DPNominalPeakTime = 26.6;  
      }
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      if(chID<0){
	std::cout << "Warning! MTBasicParamManager: IBCV chID setting is wrong!\n";
      }else if(chID<32){
	m_G2DPNominalPeakTime = 26.6;  
      }else if(100<=chID&&chID<132){
	m_G2DPNominalPeakTime = 26.6;  
      }    
    } else if( 20200101<=userFlag ){ // RUN85~
      if(chID<0){
	std::cout << "Warning! MTBasicParamManager: IBCV chID setting is wrong!\n";
      }else if(chID<32){
	m_G2DPNominalPeakTime = 26.6;  
      }else if(100<=chID&&chID<132){
	m_G2DPNominalPeakTime = 26.6;  
      }
    }
  }  

  void E14BasicParamManager::SetDataMBCV(const int userFlag, int chID)
  {
    if(chID<-1 || 16<=chID)std::cout<<"Warning! E14BasicParamManager MBCV wrong chID settings\n";

    m_DetZPosition = 6343;
      
    if( 20160101<=userFlag && userFlag<20160615 ){ // RUN69 before amp
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 33.4;
    } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 30.0;
    } else if( 20170101<=userFlag && userFlag<20170701 ){ // RUN74
      m_G2DPMinPeakHeight = 20;
      m_G2DPNominalPeakTime = 28.1;
    } else if( 20170701<=userFlag && userFlag<20180401 ){ // RUN78
      m_G2DPMinPeakHeight = 20;
      m_G2DPNominalPeakTime = 28.1;
    } else if( 20180401<=userFlag && userFlag<20180701 ){ // RUN79
      m_G2DPMinPeakHeight = 20;
      m_G2DPNominalPeakTime = 27.5;
    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_G2DPMinPeakHeight = 20; 
      m_G2DPNominalPeakTime = 24.9;
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_G2DPMinPeakHeight = 20; 
      m_G2DPNominalPeakTime = 25.9;
    } else if( 20200101<=userFlag ){ // RUN85~
      m_G2DPMinPeakHeight = 20; 
      m_G2DPNominalPeakTime = 25.9;
    }
  }  

  void E14BasicParamManager::SetDataOEV(const int userFlag, int chID)
  {
    if(chID<-1||44<=chID)std::cout<<"Warning! E14BasicParamManager OEV wrong chID settings\n";

    m_G2DPMinPeakHeight = 50;

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 6148; // Z front surface
      m_G2DPNominalPeakTime = 31.5;
    } else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = 6148 + 20; // Z front surface

      if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 33.4;
      } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 30.0;
      } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
	m_G2DPNominalPeakTime = 28.1;
      } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 28.1;
      } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 27.4;
      }
    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_DetZPosition = 6148 + 20; // Z front surface
      m_G2DPNominalPeakTime = 24.85;
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_DetZPosition = 6148 + 20; // Z front surface
      m_G2DPNominalPeakTime = 25.22;
    } else if( 20200101<=userFlag ){ // RUN85~
      m_DetZPosition = 6148 + 20; // Z front surface
      m_G2DPNominalPeakTime = 25.22;
    }
  }
  
  void E14BasicParamManager::SetDataCSI(const int userFlag, int chID)
  {
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager CSI wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 6148; // surface
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 31.1;
      m_NDeadChannels = 3;
      m_DeadChannelIDVec.push_back(1554);
      m_DeadChannelIDVec.push_back(2070);
      m_DeadChannelIDVec.push_back(2229);
      m_NDeadModules = 3;
      m_DeadModuleIDVec.push_back(1554);
      m_DeadModuleIDVec.push_back(2070);
      m_DeadModuleIDVec.push_back(2229);
    } else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = 6148 + 20; // surface
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 31.1;
      m_NDeadChannels = 2;
      m_DeadChannelIDVec.push_back(356);
      m_DeadChannelIDVec.push_back(357);
      m_NDeadModules = 2;
      m_DeadModuleIDVec.push_back(356);
      m_DeadModuleIDVec.push_back(357);
    } else if( 20180701<=userFlag ) { //RUN8X~
      m_DetZPosition = 6148 + 20; // surface
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 31.1;
    }
  }
  
  void E14BasicParamManager::SetDataCV(const int userFlag, int chID)
  {
    if(chID<-1 || (96<=chID&&chID<100) || 188<=chID)
      std::cout<<"Warning! E14BasicParamManager CV wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { //2015run
      m_DetZPosition = 5842; // Front CV
      m_G2DPMinPeakHeight = 150;
      m_G2DPNominalPeakTime = 33.5;
      m_NDeadChannels = 2;
      m_DeadChannelIDVec.push_back(115);
      m_DeadChannelIDVec.push_back(172);
      m_NDeadModules = 2;
      m_DeadModuleIDVec.push_back(114);
      m_DeadModuleIDVec.push_back(172);

    }else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = 5842 + 20; // Front CV
      m_G2DPMinPeakHeight = 150;
      if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 37.6;
      } else if ( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 34.2;
      } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
	m_G2DPNominalPeakTime = 32.4;
      } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 32.3;
      } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 31.8;
      }

    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_G2DPMinPeakHeight = 150;
      m_G2DPNominalPeakTime = 29.4;
      m_DetZPosition = 5842 + 20; 
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_G2DPMinPeakHeight = 150;
      m_G2DPNominalPeakTime = 30.4;
      m_DetZPosition = 5842 + 20; 
    } else if( 20200101<=userFlag ){ // RUN85~
      m_G2DPMinPeakHeight = 150;
      m_G2DPNominalPeakTime = 30.4;
      m_DetZPosition = 5842 + 20; 
    }
  }

  void E14BasicParamManager::SetDataCVF(const int userFlag, int chID)
  {
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager CVF wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { //2015run
      m_DetZPosition = 5842; // Front CV
    } else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = 5842 + 20; // Front CV
    } else if( 20180701<=userFlag ) { //RUN8X
      m_DetZPosition = 5842 + 20; // Front CV
    }
  }

  void E14BasicParamManager::SetDataCVR(const int userFlag, int chID)
  {
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager CVR wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { //2015run
      m_DetZPosition = 6093; // Rear CV
    } else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = 6093 + 20; // Rear CV
    } else if( 20180701<=userFlag ) { //RUN8X
      m_DetZPosition = 6093 + 20; // Rear CV
    }
  }
  
  void E14BasicParamManager::SetDataLCV(const int userFlag, int chID)
  {
    if(chID<-1 || 4<=chID)std::cout<<"Warning! E14BasicParamManager LCV wrong chID settings\n";

    m_G2DPMinPeakHeight = 50;

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 6148; // Z front surface
      m_G2DPNominalPeakTime = 28.6;

    } else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = 6148 + 20; // Z front surface
      if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 32.4;
      } else if ( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 29.0;
      } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
	m_G2DPNominalPeakTime = 27.5;
      } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 27.6;
      } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 26.9;
      }

    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_DetZPosition = 6148 + 20; // Z front surface
      m_G2DPNominalPeakTime = 32.5;
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_DetZPosition = 6148 + 20; // Z front surface
      m_G2DPNominalPeakTime = 33.6;
    } else if( 20200101<=userFlag ){ // RUN85~
      m_DetZPosition = 6148 + 20; // Z front surface
      m_G2DPNominalPeakTime = 33.6;
    }
  }
  
  void E14BasicParamManager::SetDataCC03(const int userFlag, int chID)
  {
    if(chID<-1 || 32<=chID)std::cout<<"Warning! E14BasicParamManager CC03 wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 6148; // surface
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 30.3;
      m_NDeadChannels = 1;
      m_DeadChannelIDVec.push_back(17);
      m_NDeadModules = 1;
      m_DeadModuleIDVec.push_back(16);

    } else if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
      m_DetZPosition = 6148 + 20; // surface
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 35.2;
    } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
      m_DetZPosition = 6148 + 20; // surface
      m_G2DPMinPeakHeight = 10;
      m_G2DPNominalPeakTime = 31.6;
    } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
      m_DetZPosition = 6148 + 20; // surface
      m_G2DPMinPeakHeight = 30;
      m_G2DPNominalPeakTime = 29.8;
    } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
      m_DetZPosition = 6148 + 20; // surface
      m_G2DPMinPeakHeight = 30;
      m_G2DPNominalPeakTime = 29.9;
    } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
      m_DetZPosition = 6148 + 20; // surface
      m_G2DPMinPeakHeight = 30;
      m_G2DPNominalPeakTime = 29.0;

    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_DetZPosition = 6148 + 20; // 
      m_G2DPMinPeakHeight = 30;
      m_G2DPNominalPeakTime = 24.9;
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_DetZPosition = 6148 + 20; // surface
      m_G2DPMinPeakHeight = 30;
      m_G2DPNominalPeakTime = 25.5;
    } else if( 20200101<=userFlag ){ // RUN85~
      m_DetZPosition = 6148 + 20; // surface
      m_G2DPMinPeakHeight = 30;
      m_G2DPNominalPeakTime = 25.5;
    }
  }

  void E14BasicParamManager::SetDataCC04(const int userFlag, int chID)
  {
    if(chID<-1 || 66<=chID)std::cout<<"Warning! E14BasicParamManager CC04 wrong chID settings\n";
    
    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 7415; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 10; // common for CsI and scinti
      m_G2DPNominalPeakTime = 28.3;
      m_NDeadChannels = 1;
      m_DeadChannelIDVec.push_back(51);
      m_NDeadModules = 1;
      m_DeadModuleIDVec.push_back(51);

    } else if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
      m_DetZPosition = 7415 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 10; // common for CsI and scinti
      m_G2DPNominalPeakTime = 31.9;
    } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
      m_DetZPosition = 7415 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 10; // common for CsI and scinti
      m_G2DPNominalPeakTime = 28.5;
    } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
      m_DetZPosition = 7415 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 26.7;
      if(chID<42) m_G2DPMinPeakHeight  = 20.;
      else if(chID<58) m_G2DPMinPeakHeight  = 10.;
      else if(chID<66) m_G2DPMinPeakHeight  = 20.;

    } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
      m_DetZPosition = 7415 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 26.9;
      if(chID<42) m_G2DPMinPeakHeight  = 20.;
      else if(chID<58) m_G2DPMinPeakHeight  = 10.;
      else if(chID<66) m_G2DPMinPeakHeight  = 20.;

    } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
      m_DetZPosition = 7415 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 26.1;
      if(chID<42) m_G2DPMinPeakHeight  = 20.;
      else if(chID<58) m_G2DPMinPeakHeight  = 10.;
      else if(chID<66) m_G2DPMinPeakHeight  = 20.;

    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_DetZPosition = 7415 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 28.8; //
      if(chID<0) std::cout << "Warning! E14BasicParamManager CC04 wrong chID settings\n";
      else if(chID<42) m_G2DPMinPeakHeight  = 20.;
      else if(chID<58) m_G2DPMinPeakHeight  = 10.;
      else if(chID<66) m_G2DPMinPeakHeight  = 20.;
	
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_DetZPosition = 7415 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 29.9; //
      if(chID<0) std::cout << "Warning! E14BasicParamManager CC04 wrong chID settings\n";
      else if(chID<42) m_G2DPMinPeakHeight  = 20.;
      else if(chID<58) m_G2DPMinPeakHeight  = 10.;
      else if(chID<66) m_G2DPMinPeakHeight  = 20.;
	
    } else if( 20200101<=userFlag ){ // RUN85~
      m_DetZPosition = 7415 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 29.9; //
      if(chID<0) std::cout << "Warning! E14BasicParamManager CC04 wrong chID settings\n";
      else if(chID<42) m_G2DPMinPeakHeight  = 20.;
      else if(chID<58) m_G2DPMinPeakHeight  = 10.;
      else if(chID<66) m_G2DPMinPeakHeight  = 20.;
	
    }
  }
  
  void E14BasicParamManager::SetDataCC05(const int userFlag, int chID)
  {
    if(chID<-1 || 66<=chID)std::cout<<"Warning! E14BasicParamManager CC05 wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 8793; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 20; // common for CsI and scinti
      m_G2DPNominalPeakTime = 26.0;

    } else if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
      m_DetZPosition = 8793 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 20; // common for CsI and scinti
      m_G2DPNominalPeakTime = 28.9;
    } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
      m_DetZPosition = 8793 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 20; // common for CsI and scinti
      m_G2DPNominalPeakTime = 25.5;
    } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
      m_DetZPosition = 8793 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 17; // common for CsI and scinti
      m_G2DPNominalPeakTime = 23.7;
    } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
      m_DetZPosition = 8793 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 17; // common for CsI and scinti
      m_G2DPNominalPeakTime = 23.7;
    } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
      m_DetZPosition = 8793 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 17; // common for CsI and scinti
      m_G2DPNominalPeakTime = 23.1;

    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_DetZPosition = 8793 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 17; // common for CsI and scinti
      m_G2DPNominalPeakTime = 27.7;
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_DetZPosition = 8793 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 17; // common for CsI and scinti
      m_G2DPNominalPeakTime = 28.7;
    } else if( 20200101<=userFlag ){ // RUN85~
      m_DetZPosition = 8793 + 20; // Z front surface of CsI crystal
      m_G2DPMinPeakHeight = 17; // common for CsI and scinti
      m_G2DPNominalPeakTime = 28.7;
    }
  }
  
  void E14BasicParamManager::SetDataCC06(const int userFlag, int chID)
  {
    if(chID<-1 || 66<=chID)std::cout<<"Warning! E14BasicParamManager CC06 wrong chID settings\n";

    m_G2DPSmearingSigma = 0;
    m_G2DPMinPeakHeight = 20;
    if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10; // scinti

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 10338; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 26.4;
      if( chID<54 ) m_G2DPMinPeakHeight = 15;
      else if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10;

    } else if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
      m_DetZPosition = 10338 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 29.8;
      m_G2DPSmearingSigma = 38.83;
      if( chID<54 ) m_G2DPMinPeakHeight = 15;
      else if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10;

    } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
      m_DetZPosition = 10338 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 26.5;
      m_G2DPSmearingSigma = 38.83;
      if( chID<54 ) m_G2DPMinPeakHeight = 15;
      else if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10;

    } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
      m_DetZPosition = 10338 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 24.6;
      m_G2DPSmearingSigma = 38.83;
      if( chID<54 ) m_G2DPMinPeakHeight = 20;
      else if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10;

    } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
      m_DetZPosition = 10338 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 24.6;
      m_G2DPSmearingSigma = 38.83;
      if( chID<54 ) m_G2DPMinPeakHeight = 20;
      else if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10;

    } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
      m_DetZPosition = 10338 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 24.1;
      m_G2DPSmearingSigma = 38.83;
      if( chID<54 ) m_G2DPMinPeakHeight = 20;
      else if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10;

    } else if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_DetZPosition = 10338 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 28.6;
      m_G2DPSmearingSigma = 38.83;
      if( chID<54 ) m_G2DPMinPeakHeight = 20;
      else if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10;

    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_DetZPosition = 10338 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 29.6;
      m_G2DPSmearingSigma = 38.83;
      if( chID<54 ) m_G2DPMinPeakHeight = 20;
      else if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10;

    } else if( 20200101<=userFlag ){ // RUN85~
      m_DetZPosition = 10338 + 20; // Z front surface of CsI crystal
      m_G2DPNominalPeakTime = 29.6;
      m_G2DPSmearingSigma = 38.83;
      if( chID<54 ) m_G2DPMinPeakHeight = 20;
      else if( 60<=chID && chID<66) m_G2DPMinPeakHeight = 10;

    }

  }  

  void E14BasicParamManager::SetDataBPCV(const int userFlag, int chID)
  {
    if(chID<-1 || 4<=chID)std::cout<<"Warning! E14BasicParamManager BPCV wrong chID settings\n";

    m_G2DPMinPeakHeight = 10;

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 8793;
      m_G2DPNominalPeakTime = 26.08;

    } else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = 8793 + 20;
      if( 20160101<=userFlag && userFlag<20160615) { // RUN69 before amp
	m_G2DPNominalPeakTime = 34.6;
      } else if( 20160615<=userFlag && userFlag<20170101) { // RUN69 after amp
	m_G2DPNominalPeakTime = 31.3;
      } else if( 20170101<=userFlag && userFlag<20170701) { // RUN74
	m_G2DPNominalPeakTime = 29.3;
      } else if( 20170701<=userFlag && userFlag<20180401) { // RUN78
	m_G2DPNominalPeakTime = 29.3;
      } else if( 20180401<=userFlag && userFlag<20180701) { // RUN79
	m_G2DPNominalPeakTime = 28.7;
      }
    }
  }  

  void E14BasicParamManager::SetDataBHCV(const int userFlag, int chID)
  {
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager BHCV wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 10757;
      m_G2DPMinPeakHeight = 30; // not used
    }
  }    
  
  void E14BasicParamManager::SetDatanewBHCV(const int userFlag, int chID)
  {
    if(chID<-1 || 48<=chID)std::cout<<"Warning! E14BasicParamManager newBHCV wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = (614.8+242.5+155.5+2.+19.+(7.006+0.029)*3.+1.125+(1+0.14))*10.;
      m_G2DPMinPeakHeight = 30; // not used
    } else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = (614.8+242.5+155.5+2.+19.+(7.006+0.029)*3.+1.125+(1+0.14))*10. + 20;
      m_G2DPMinPeakHeight = 30; // not used
    } else if( 20180701<=userFlag ) { //RUN8X~
      m_DetZPosition = (614.8+242.5+155.5+2.+19.+(7.006+0.029)*3.+1.125+(1+0.14))*10. + 20;
      m_G2DPMinPeakHeight = 30; // not used
    }
  }  

  void E14BasicParamManager::SetDataBHPV(const int userFlag, int chID)
  {
    if(chID<-1 || 42<=chID)std::cout<<"Warning! E14BasicParamManager BHPV wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 12077; // z front surface of module_0
      m_G2DPMinPeakHeight = 10; // not used
    } else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = 12077 + 20; // z front surface of module_0
      m_G2DPMinPeakHeight = 10; // not used
    } else if( 20180701<=userFlag ) { //RUN8X~
      m_DetZPosition = 12077 + 20; // z front surface of module_0
      m_G2DPMinPeakHeight = 10; // not used
    }
  }
    
  void E14BasicParamManager::SetDataBHGC(const int userFlag, int chID)
  {
    if(chID<-1 || 8<=chID)std::cout<<"Warning! E14BasicParamManager BHGC wrong chID settings\n";

    if ( 20150401<=userFlag && userFlag<20160101) { // 2015run
      m_DetZPosition = 18195 + 40;
      m_G2DPMinPeakHeight = 10; // not used
    } else if( 20160101<=userFlag && userFlag<20180701) { // RUN69~RUN79
      m_DetZPosition = 18195 + 40 + 20;
      m_G2DPMinPeakHeight = 10; // not used
    } else if( 20180701<=userFlag ) { //RUN8X~
      m_DetZPosition = 18195 + 40 + 20;
      m_G2DPMinPeakHeight = 10; // not used
    }
  }    

  void E14BasicParamManager::SetDataDCV(const int userFlag, int chID)
  {
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager DCV wrong chID settings\n";

    if( 20190101<=userFlag && userFlag<20190301 ){ // RUN81
      m_G2DPMinPeakHeight = 100; // 
      m_G2DPNominalPeakTime = 28.3;
    } else if ( 20190301<=userFlag && userFlag<20191201 ){ // RUN82
      m_G2DPMinPeakHeight = 100; // 
      m_G2DPNominalPeakTime = 29.4;
    } else if( 20200101<=userFlag ){ // RUN85~
      m_G2DPMinPeakHeight = 100; // 
      m_G2DPNominalPeakTime = 29.4;
    }
  }    

  void E14BasicParamManager::SetDataDCV1(const int userFlag, int chID){
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager DCV1 wrong chID settings\n";

    if( 20190101<=userFlag ) { //RUN8X~
      m_DetZPosition = 7820.2;
    }
  }

  void E14BasicParamManager::SetDataDCV2(const int userFlag, int chID){
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager DCV2 wrong chID settings\n";

    if( 20190101<=userFlag ) { //RUN8X~
      m_DetZPosition = 9353.2;
    }
  }

  void E14BasicParamManager::SetDataUCV(const int userFlag, int chID)
  {
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager UCV wrong chID settings\n";

    if ( userFlag<20200201) { // before 2019
      m_DetZPosition = -9999;
      m_G2DPMinPeakHeight = -9999;
    } else if( 20200201<=userFlag && userFlag <20210201) {
      m_DetZPosition = -607;
      m_G2DPMinPeakHeight = 0; // not used
    } else {
      m_DetZPosition = -857.5;
      m_G2DPMinPeakHeight = 0; // not used
    }
    
  }

  void E14BasicParamManager::SetDataUCVLG(const int userFlag, int chID){
    if(chID<-1)std::cout<<"Warning! E14BasicParamManager UCVLG wrong chID settings\n";

    SetDataUCV(userFlag);
  }

  void E14BasicParamManager::SetUserFlag( const int RunID )
  {

    initializeDataValues();
    
    if ( RunID>=17532 && RunID<22563 ) { // from RUN62 to RUN65
      m_userFlag = 20151101;
    } else if( RunID>=22563 && RunID<23645 ){ // RUN69 before amp
      m_userFlag = 20160601;
    } else if( RunID>=23645 && RunID<24446 ){ // RUN69 after amp
      m_userFlag = 20160630;
    } else if( RunID>=24446 && RunID<26712 ){ // RUN74,75
      m_userFlag = 20170601;
    } else if( RunID>=26712 && RunID<28404 ){ // RUN78
      m_userFlag = 20180201;
    } else if( RunID>=28404 && RunID<29827 ){ // RUN79
      m_userFlag = 20180601;
    } else if( RunID>=29827 && RunID<31532){ //  RUN81
      m_userFlag = 20190201;
    } else if( RunID>=31532 && RunID<32221){ // RUN82
      m_userFlag = 20190201;
    } else if( RunID>=32221 && RunID<33820){ // RUN85
      m_userFlag = 20200601;
    } else if( RunID>=33820 && RunID<34510){ // RUN86 Feb
      m_userFlag = 20210201;
    } else if( RunID>=34510 && RunID<34900){ // RUN86 March
      m_userFlag = 20210301;
    } else if( RunID>=34900 ){ // RUN87 -
      m_userFlag = 20210501;
    }
    
    
  }
   
  void E14BasicParamManager::GetArrayNominalPeakTime( float Input_NPT[4096] ){
    for(int i=0;i<4096;i++) Input_NPT[i] = m_NPT[i];
    return;
  }

  void E14BasicParamManager::GetArrayMinPeakHeight( float Input_MPH[4096] ){
    for(int i=0;i<4096;i++) Input_MPH[i] = m_MPH[i];
    return;
  }

  void E14BasicParamManager::GetArrayRangeMin( int Input_RangeMin[4096] ){
    for(int i=0;i<4096;i++) Input_RangeMin[i] = m_RangeMin[i];
    return;
  }

  void E14BasicParamManager::GetArrayRangeMax( int Input_RangeMax[4096] ){
    for(int i=0;i<4096;i++) Input_RangeMax[i] = m_RangeMax[i];
    return;
  }

  void E14BasicParamManager::GetArraynSampleIntegral( int Input_nSampleIntegral[4096] ){
    for(int i=0;i<4096;i++) Input_nSampleIntegral[i] = m_nSampleIntegral[i];
    return;
  }

  void E14BasicParamManager::GetArraynSampleIntegralBeforePeak( int Input_nSampleIntegralBeforePeak[4096] ){
    for(int i=0;i<4096;i++) Input_nSampleIntegralBeforePeak[i] = m_nSampleIntegralBeforePeak[i];
    return;
  }
    
     
}
