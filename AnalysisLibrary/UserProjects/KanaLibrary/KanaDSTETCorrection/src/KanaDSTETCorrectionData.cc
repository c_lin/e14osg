#include "KanaDSTETCorrection/KanaDSTETCorrectionData.h"

#include <algorithm>

ClassImp(KanaLibrary::KanaDSTETCorrectionData)

namespace KanaLibrary
{

  KanaDSTETCorrectionData::

  KanaDSTETCorrectionData::KanaDSTETCorrectionData()
  {
  }

  KanaDSTETCorrectionData::~KanaDSTETCorrectionData()
  {
  }
  
  void KanaDSTETCorrectionData::AddCorrectionData(const Int_t mod_ID, const Float_t correction_value)
  {
    struct CorrectionData data = { mod_ID, correction_value };

    std::vector<CorrectionData>::iterator
      itr = std::find(m_CorrectionDataList.begin(), m_CorrectionDataList.end(), data);
    if (itr==m_CorrectionDataList.end()) {
      m_CorrectionDataList.push_back(data);
    } else {
      (*itr) = data;
    }
  }
  
  Int_t KanaDSTETCorrectionData::FindIndex(const Int_t mod_ID)
  {
    Int_t index = -1;
    for (std::vector<CorrectionData>::iterator
	   itr=m_CorrectionDataList.begin(), end=m_CorrectionDataList.end(); itr!=end; ++itr) {
      if (itr->modID==mod_ID) {
	index = std::distance(m_CorrectionDataList.begin(), itr);
	break;
      }
    }
    return index;
  }
  
  void KanaDSTETCorrectionData::Clear(Option_t*)
  {
    m_CorrectionDataList.clear();
  }
  
  void KanaDSTETCorrectionData::Sort()
  {
    std::sort(m_CorrectionDataList.begin(), m_CorrectionDataList.end());
  }
  
}
