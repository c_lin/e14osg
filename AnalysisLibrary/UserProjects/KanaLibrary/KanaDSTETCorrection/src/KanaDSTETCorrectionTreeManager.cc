#include "KanaDSTETCorrection/KanaDSTETCorrectionTreeManager.h"

ClassImp(KanaLibrary::KanaDSTETCorrectionTreeManager)

namespace KanaLibrary
{

  KanaDSTETCorrectionTreeManager::KanaDSTETCorrectionTreeManager()
    : m_InputTree(NULL), m_InputMode(-1),
      m_SpillID(0), m_L2TrigNo(0), m_Nmod(0), m_modID(NULL), m_CorrectionValue(NULL)
  {
  }
  
  KanaDSTETCorrectionTreeManager::KanaDSTETCorrectionTreeManager(const Int_t n_ch)
    : m_InputTree(NULL), m_InputMode(-1),
      m_SpillID(0), m_L2TrigNo(0), m_Nmod(0), m_modID(NULL), m_CorrectionValue(NULL)
  {
    m_modID           = new Int_t   [n_ch];
    m_CorrectionValue = new Float_t [n_ch];
  }

  void KanaDSTETCorrectionTreeManager::SetRBRCorrectionTree(TDirectory *dir, const std::string tree_name)
  {
    SetCorrectionTree(dir, tree_name, k_RBRMode);
  }
  
  void KanaDSTETCorrectionTreeManager::SetSBSCorrectionTree(TDirectory *dir, const std::string tree_name)
  {
    SetCorrectionTree(dir, tree_name, k_SBSMode);
  }
  
  void KanaDSTETCorrectionTreeManager::SetEBECorrectionTree(TDirectory *dir, const std::string tree_name)
  {
    SetCorrectionTree(dir, tree_name, k_EBEMode);
  }
  
  void KanaDSTETCorrectionTreeManager::
  SetCorrectionTree(TDirectory *dir, const std::string tree_name, const Int_t mode)
  {
    m_InputMode = mode;

    // check input ROOT directory (or file)
    if (dir==NULL) {
      Error("SetCorrectionTree", "input ROOT directory (or file) is NULL");
      return;
    }

    // get input tree
    dir->cd();
    m_InputTree = (TTree*)dir->Get(tree_name.c_str());
    if (m_InputTree==NULL) {
      Error("SetCorrectionTree", "cannot find tree '%s' in %s", tree_name.c_str(), dir->GetName());
      return;
    }

    // set branch addresses
    if (m_InputTree->GetBranch("SpillID")!=NULL) {
      m_InputTree->SetBranchAddress("SpillID", &m_SpillID);
    }
    if (m_InputTree->GetBranch("L2TrigNo")!=NULL) {
      m_InputTree->SetBranchAddress("L2TrigNo", &m_L2TrigNo);
    }

    m_InputTree->SetBranchAddress("Nmod", &m_Nmod );
    m_InputTree->SetBranchAddress("modID", m_modID);
    if (m_InputTree->GetBranch("CorrectionFactor")!=NULL) { // for energy correction
      m_InputTree->SetBranchAddress("CorrectionFactor",  m_CorrectionValue);
    } else if (m_InputTree->GetBranch("CorrectionInClock")!=NULL) { // for timing correction
      m_InputTree->SetBranchAddress("CorrectionInClock", m_CorrectionValue);
    }

    // get list of the pair: (SpillID, L2TrigNo)
    m_EventKeyList.clear();
    for (Long64_t i_entry=0, n_entries=m_InputTree->GetEntries(); i_entry < n_entries; ++i_entry) {
      m_InputTree->GetEntry(i_entry);
      m_EventKeyList.push_back( std::pair<Short_t,Short_t>(m_SpillID, m_L2TrigNo) );
    }
  }
  
  KanaDSTETCorrectionData KanaDSTETCorrectionTreeManager::
  GetCorrectionData(const Short_t spill_ID, const Short_t L2_trig_no)
  {
    KanaDSTETCorrectionData data;

    Long64_t entry_index = -1;
    if (m_InputMode==k_RBRMode) { // run-by-run mode -> only 1 event is filled in tree
      entry_index = 0;
    } else { // find corresponding correction data for spill-by-spill or event-by-event mode
      std::vector< std::pair<Short_t,Short_t> >::iterator begin = m_EventKeyList.begin();
      std::vector< std::pair<Short_t,Short_t> >::iterator end   = m_EventKeyList.end();
      for (std::vector< std::pair<Short_t,Short_t> >::iterator itr=begin; itr!=end; ++itr) {
	const Short_t spill_ID_tmp   = itr->first;
	const Short_t L2_trig_no_tmp = itr->second;
	if ( (m_InputMode==k_SBSMode && spill_ID_tmp==spill_ID) 
	     || (m_InputMode==k_EBEMode && spill_ID_tmp==spill_ID && L2_trig_no_tmp==L2_trig_no) ) {
	  entry_index = std::distance(begin, itr);
	  break;
	}
      }
    }
    
    if (entry_index!=-1) { // found
      m_InputTree->GetEntry(entry_index);
      for (Int_t i_mod=0; i_mod < m_Nmod; ++i_mod) {
	data.AddCorrectionData(m_modID[i_mod], m_CorrectionValue[i_mod]);
      }
    }
    
    return data;
  }
  
  KanaDSTETCorrectionTreeManager::~KanaDSTETCorrectionTreeManager()
  {
    if (m_modID          !=NULL) { delete [] m_modID;           m_modID          =NULL; }
    if (m_CorrectionValue!=NULL) { delete [] m_CorrectionValue; m_CorrectionValue=NULL; }
  }
  
}
