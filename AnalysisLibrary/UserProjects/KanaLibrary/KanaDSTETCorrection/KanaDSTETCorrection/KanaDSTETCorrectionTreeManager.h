/* -*- C++ -*- */
#ifndef E14ANA_KANADSTETCORRECTION_KANADSTETCORRECTIONMANAGER_H
#define E14ANA_KANADSTETCORRECTION_KANADSTETCORRECTIONMANAGER_H

#include <vector>
#include <map>

#include <TFile.h>
#include <TObject.h>
#include <TTree.h>

#include "KanaDSTETCorrectionData.h"

namespace KanaLibrary
{
  
  class KanaDSTETCorrectionTreeManager : public TObject
  {
  public:
    KanaDSTETCorrectionTreeManager();
    KanaDSTETCorrectionTreeManager(const Int_t n_ch);
    ~KanaDSTETCorrectionTreeManager();

    void SetRBRCorrectionTree(TDirectory *dir, const std::string tree_name);
    void SetSBSCorrectionTree(TDirectory *dir, const std::string tree_name);
    void SetEBECorrectionTree(TDirectory *dir, const std::string tree_name);

    enum { // input mode
      k_RBRMode=0, k_SBSMode, k_EBEMode
    };

    Int_t GetCurrentMode() { return m_InputMode; }

    KanaDSTETCorrectionData GetCorrectionData(const Short_t spill_ID, const Short_t L2_trig_no);
    
  private:
    void SetCorrectionTree(TDirectory *dir, const std::string tree_name, const Int_t mode);

    TTree *m_InputTree;
    Int_t m_InputMode;

    Short_t m_SpillID;
    Short_t m_L2TrigNo;

    Int_t   m_Nmod;
    Int_t   *m_modID;
    Float_t *m_CorrectionValue;

    std::vector< std::pair<Short_t,Short_t> > m_EventKeyList;

    ClassDef(KanaDSTETCorrectionTreeManager, 1);
  };
  
}

#endif
