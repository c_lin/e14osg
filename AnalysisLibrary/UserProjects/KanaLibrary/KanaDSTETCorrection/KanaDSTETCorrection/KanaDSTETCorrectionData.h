/* -*- C++ -*- */

#ifndef E14ANA_KANADSTETCORRECTION_KANADSTETCORRECTIONDATA_H
#define E14ANA_KANADSTETCORRECTION_KANADSTETCORRECTIONDATA_H

#include <vector>

#include <TObject.h>
#include <TTree.h>

namespace KanaLibrary
{
  
  class KanaDSTETCorrectionData : public TObject
  {
  public:

    struct CorrectionData {
      Int_t   modID;
      Float_t CorrectionValue;
      bool operator==(const CorrectionData &data) const
      {
	return (modID==data.modID);
      }
      bool operator!=(const CorrectionData &data) const
      {
	return !(*this==data);
      }
      bool operator<(const CorrectionData &data) const
      {
	return (modID<data.modID);
      }
      bool operator>(const CorrectionData &data) const
      {
	return (modID<data.modID);
      }
    };
    
    KanaDSTETCorrectionData();
    ~KanaDSTETCorrectionData();

    void AddCorrectionData(const Int_t mod_ID, const Float_t correction_value);
    
    Int_t   Nmod    () { return m_CorrectionDataList.size(); }
    Int_t   modID          (const Int_t i_mod) { return m_CorrectionDataList.at(i_mod).modID;           }
    Float_t CorrectionValue(const Int_t i_mod) { return m_CorrectionDataList.at(i_mod).CorrectionValue; }

    Int_t FindIndex(const Int_t mod_ID);
    
    virtual void Clear(Option_t* ="");
    void Sort();
    
  private:
    std::vector<CorrectionData> m_CorrectionDataList;
    
    ClassDef(KanaDSTETCorrectionData, 1);
  };
  
}
  
#endif
