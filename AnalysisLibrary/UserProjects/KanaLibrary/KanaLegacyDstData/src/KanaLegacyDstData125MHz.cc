#include "KanaLegacyDstData/KanaLegacyDstData125MHz.h"

#include <algorithm>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaLegacyDstData125MHz)

namespace KanaLibrary
{
  KanaLegacyDstData125MHz::KanaLegacyDstData125MHz()
  {
    InitKanaLegacyDstData125MHz();
  }

  KanaLegacyDstData125MHz::KanaLegacyDstData125MHz(const std::string name, const Int_t n_ch)
    : KanaVirtualLegacyDstData(name, n_ch)
  {
    InitKanaLegacyDstData125MHz();

    Peak          = new Short_t  [n_ch];
    IntegratedADC = new Float_t  [n_ch];
    InitialTime   = new Float_t  [n_ch];
    Ene           = new Float_t  [n_ch];
    Time          = new Float_t  [n_ch];
    FallTime      = new Float_t  [n_ch];
    InitialPTime  = new Float_t  [n_ch];
    PTime         = new Float_t  [n_ch];
    PTimeOrig     = new Float_t  [n_ch];
    EtSum         = new Long64_t [N_SAMPLES];
    MaxDDc        = new Float_t  [n_ch];
    MaxDDcSam     = new Short_t  [n_ch];

    Clear();
  }
  
  KanaLegacyDstData125MHz::~KanaLegacyDstData125MHz()
  {
    if (Peak         !=NULL) { delete [] Peak;          Peak         =NULL; }
    if (IntegratedADC!=NULL) { delete [] IntegratedADC; IntegratedADC=NULL; }
    if (InitialTime  !=NULL) { delete [] InitialTime;   InitialTime  =NULL; }
    if (Ene          !=NULL) { delete [] Ene;           Ene          =NULL; }
    if (Time         !=NULL) { delete [] Time;          Time         =NULL; }
    if (FallTime     !=NULL) { delete [] FallTime;      FallTime     =NULL; }
    if (InitialPTime !=NULL) { delete [] InitialPTime;  InitialPTime =NULL; }
    if (PTime        !=NULL) { delete [] PTime;         PTime        =NULL; }
    if (PTimeOrig    !=NULL) { delete [] PTimeOrig;     PTimeOrig    =NULL; }
    if (EtSum        !=NULL) { delete [] EtSum;         EtSum        =NULL; }
    if (MaxDDc       !=NULL) { delete [] MaxDDc;        MaxDDc       =NULL; }
    if (MaxDDcSam    !=NULL) { delete [] MaxDDcSam;     MaxDDcSam    =NULL; }
  }
  
  
  void KanaLegacyDstData125MHz::InitKanaLegacyDstData125MHz()
  {
    Peak          = NULL;
    IntegratedADC = NULL;
    InitialTime   = NULL;
    Ene           = NULL;
    Time          = NULL;
    FallTime      = NULL;
    InitialPTime  = NULL;
    PTime         = NULL;
    PTimeOrig     = NULL;
    EtSum         = NULL;
    MaxDDc        = NULL;
    MaxDDcSam     = NULL;
  }
  
  
  std::vector<std::string> KanaLegacyDstData125MHz::SetBranchAddresses(TTree *tr, const std::string branch_list_expr)
  {
    const Bool_t use_all = (branch_list_expr=="*");

    const std::vector<std::string> branch_list = GetBranchList(branch_list_expr);
    std::vector<std::string>::const_iterator begin = branch_list.begin();
    std::vector<std::string>::const_iterator end   = branch_list.end();
    
    // setting Number & ModID even if they are not specified in branch_list_expr
    {
      const std::string bname = Name + "Number";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &Number);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    {
      const std::string bname = Name + "ModID";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModID);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }

    // setting other branches
    if ( use_all || std::find(begin, end, "TimeStamp")!=end ) {
      const std::string bname = Name + "TimeStamp";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), TimeStamp);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "Pedestal")!=end ) {
      const std::string bname = Name + "Pedestal";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Pedestal);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "Peak")!=end ) {
      const std::string bname = Name + "Peak";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Peak);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "IntegratedADC")!=end ) {
      const std::string bname = Name + "IntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), IntegratedADC);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "InitialTime")!=end ) {
      const std::string bname = Name + "InitialTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), InitialTime);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "Ene")!=end ) {
      const std::string bname = Name + "Ene";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Ene);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "Time")!=end ) {
      const std::string bname = Name + "Time";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Time);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "FallTime")!=end ) {
      const std::string bname = Name + "FallTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), FallTime);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "InitialPTime")!=end ) {
      const std::string bname = Name + "InitialPTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), InitialPTime);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "PTime")!=end ) {
      const std::string bname = Name + "PTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PTime);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "EtSum")!=end ) {
      const std::string bname = Name + "EtSum";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), EtSum);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "MaxDDc")!=end ) {
      const std::string bname = Name + "MaxDDc";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MaxDDc);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "MaxDDcSam")!=end ) {
      const std::string bname = Name + "MaxDDcSam";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MaxDDcSam);
      Info("SetBranchAddresses", "%s is set", bname.c_str());
    }

    return branch_list;
  }


  std::vector<std::string> KanaLegacyDstData125MHz::AddBranches(TTree *tr, const std::string branch_list_expr)
  {
    const Bool_t use_all = (branch_list_expr=="*");

    const std::vector<std::string> branch_list = GetBranchList(branch_list_expr);
    std::vector<std::string>::const_iterator begin = branch_list.begin();
    std::vector<std::string>::const_iterator end   = branch_list.end();
    
    // adding Number & ModID even if they are not specified in branch_list_expr
    {
      const std::string bname    = Name + "Number";
      const std::string leaflist = bname + "/I";
      tr->Branch(bname.c_str(), &Number, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    {
      const std::string bname    = Name + "ModID";
      const std::string leaflist = bname + "[" + Name + "Number]/I";
      tr->Branch(bname.c_str(), ModID, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }

    // adding other branches
    if ( use_all || std::find(begin, end, "TimeStamp")!=end ) {
      const std::string bname    = Name + "TimeStamp";
      const std::string leaflist = bname + "[" + Name + "Number]/I";
      tr->Branch(bname.c_str(), TimeStamp, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "Pedestal")!=end ) {
      const std::string bname    = Name + "Pedestal";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), Pedestal, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "Peak")!=end ) {
      const std::string bname    = Name + "Peak";
      const std::string leaflist = bname + "[" + Name + "Number]/S";
      tr->Branch(bname.c_str(), Peak, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "IntegratedADC")!=end ) {
      const std::string bname    = Name + "IntegratedADC";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), IntegratedADC, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "InitialTime")!=end ) {
      const std::string bname    = Name + "InitialTime";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), InitialTime, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "Ene")!=end ) {
      const std::string bname    = Name + "Ene";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), Ene, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "Time")!=end ) {
      const std::string bname    = Name + "Time";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), Time, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "FallTime")!=end ) {
      const std::string bname    = Name + "FallTime";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), FallTime, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "InitialPTime")!=end ) {
      const std::string bname    = Name + "InitialPTime";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), InitialPTime, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "PTime")!=end ) {
      const std::string bname    = Name + "PTime";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), PTime, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "PTimeOrig")!=end ) {
      const std::string bname    = Name + "PTimeOrig";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), PTimeOrig, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "EtSum")!=end ) {
      const std::string bname    = Name + "EtSum";
      const std::string leaflist = bname + Form("[%d]/L", N_SAMPLES);
      tr->Branch(bname.c_str(), EtSum, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "MaxDDc")!=end ) {
      const std::string bname    = Name + "MaxDDc";
      const std::string leaflist = bname + "[" + Name + "Number]/F";
      tr->Branch(bname.c_str(), MaxDDc, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    if ( use_all || std::find(begin, end, "MaxDDcSam")!=end ) {
      const std::string bname    = Name + "MaxDDcSam";
      const std::string leaflist = bname + "[" + Name + "Number]/S";
      tr->Branch(bname.c_str(), MaxDDcSam, leaflist.c_str());
      Info("AddBranches", "add %s", bname.c_str());
    }
    
    return branch_list;
  }

  void KanaLegacyDstData125MHz::ReduceData(const std::vector<Int_t> &ch_list)
  {
    std::vector<Int_t> num_list;
    for (Int_t i_ch=0, n_ch=ch_list.size(); i_ch < n_ch; ++i_ch) {
      const Int_t ch_ID = ch_list.at(i_ch);
      const Int_t num   = GetNumber(ch_ID);
      if (num!=-1) num_list.push_back(num);
    }
    std::sort(num_list.begin(), num_list.end());
    num_list.erase( std::unique(num_list.begin(), num_list.end()), num_list.end() );

    Number = 0;
    for (std::vector<Int_t>::iterator iter=num_list.begin(), end=num_list.end(); iter!=end; ++iter) {
      const Int_t num = (*iter);
      ModID        [Number] = ModID        [num];
      TimeStamp    [Number] = TimeStamp    [num];
      Pedestal     [Number] = Pedestal     [num];
      Peak         [Number] = Peak         [num];
      IntegratedADC[Number] = IntegratedADC[num];
      InitialTime  [Number] = InitialTime  [num];
      Ene          [Number] = Ene          [num];
      Time         [Number] = Time         [num];
      FallTime     [Number] = FallTime     [num];
      InitialPTime [Number] = InitialPTime [num];
      PTime        [Number] = PTime        [num];
      MaxDDc       [Number] = MaxDDc       [num];
      MaxDDcSam    [Number] = MaxDDcSam    [num];
      
      ++Number;
    }
  }

  void KanaLegacyDstData125MHz::Clear(Option_t*)
  {
    KanaVirtualLegacyDstData::Clear();
    
    for (Int_t i_ch=0; i_ch<nChannels; ++i_ch) {
      Peak         [i_ch] = 0;
      IntegratedADC[i_ch] = 0;
      InitialTime  [i_ch] = 0;
      Ene          [i_ch] = 0;
      Time         [i_ch] = 0;
      FallTime     [i_ch] = 0;
      InitialPTime [i_ch] = 0;
      PTime        [i_ch] = 0;
      MaxDDc       [i_ch] = 0;
      MaxDDcSam    [i_ch] = 0;
    }
    for (Int_t i_sample=0; i_sample<N_SAMPLES; ++i_sample) {
      EtSum[i_sample] = 0;
    }
  }
  
}
