#include "KanaLegacyDstData/KanaLegacyDstCommon.h"

ClassImp(KanaLibrary::KanaLegacyDstCommon)

namespace KanaLibrary
{
  
  KanaLegacyDstCommon::KanaLegacyDstCommon()
  {
  }

  KanaLegacyDstCommon::~KanaLegacyDstCommon()
  {
  }
  
  std::vector<std::string> KanaLegacyDstCommon::GetBranchList(const std::string branch_list_expr)
  {
    std::vector<std::string> branch_list;
    
    Info("GetBranchList", "Get list of branches");
    
    std::string bname = "";
    for (Int_t i=0, n=branch_list_expr.size(); i < n; ++i) {
      const char next = branch_list_expr.at(i);
      if (next==';') {
  	branch_list.push_back(bname);
  	Info("GetBranchList", "branch: %s", bname.c_str() );
  	bname = "";
      } else {
  	bname += next;
      }
    }
    if (bname.size()!=0) {
      branch_list.push_back(bname);
      Info("GetBranchList", "branch: %s", bname.c_str() );
    }
    
    return branch_list;
  }
  
}
