#include "KanaLegacyDstData/KanaVirtualLegacyDstData.h"

ClassImp(KanaLibrary::KanaVirtualLegacyDstData)

namespace KanaLibrary
{
  KanaVirtualLegacyDstData::KanaVirtualLegacyDstData()
    : nChannels(-1)
  {
    InitKanaVirtualLegacyDstData();
  }
  
  KanaVirtualLegacyDstData::KanaVirtualLegacyDstData(const std::string name, const Int_t n_ch)
    : Name(name), nChannels(n_ch)
  {
    InitKanaVirtualLegacyDstData();
    
    ModID     = new Int_t   [n_ch];
    TimeStamp = new Int_t   [n_ch];
    Pedestal  = new Float_t [n_ch];
  }
  
  KanaVirtualLegacyDstData::~KanaVirtualLegacyDstData()
  {
    if (ModID    !=NULL) { delete [] ModID;     ModID    =NULL; }
    if (TimeStamp!=NULL) { delete [] TimeStamp; TimeStamp=NULL; }
    if (Pedestal !=NULL) { delete [] Pedestal;  Pedestal =NULL; }
  }
  
  void KanaVirtualLegacyDstData::InitKanaVirtualLegacyDstData()
  {
    Number    = 0;
    ModID     = NULL;
    TimeStamp = NULL;
    Pedestal  = NULL;
  }
  
  Int_t KanaVirtualLegacyDstData::GetNumber(const Int_t ch_ID)
  {
    Int_t number = -1;
    for (Int_t num=0; num<Number; ++num) {
      if (ModID[num]==ch_ID) { number = num; break; }
    }
    return number;
  }

  void KanaVirtualLegacyDstData::Clear(Option_t*)
  {
    Number=0;
    for (Int_t i_ch=0; i_ch<nChannels; ++i_ch) {
      ModID    [i_ch] = 0;
      TimeStamp[i_ch] = 0;
      Pedestal [i_ch] = 0;
    }
  }
}
