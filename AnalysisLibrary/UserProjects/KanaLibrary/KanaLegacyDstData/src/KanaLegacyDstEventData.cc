#include "KanaLegacyDstData/KanaLegacyDstEventData.h"

#include <algorithm>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaLegacyDstEventData)

namespace KanaLibrary
{
  KanaLegacyDstEventData::KanaLegacyDstEventData()
  {
    InitKanaLegacyDstEventData();
  }
  
  KanaLegacyDstEventData::~KanaLegacyDstEventData()
  {
  }

  void KanaLegacyDstEventData::InitKanaLegacyDstEventData()
  {
    Clear();
  }
  
  std::vector<std::string> KanaLegacyDstEventData::SetBranchAddresses(TTree *tr, const std::string branch_list_expr)
  {
    const Bool_t use_all = (branch_list_expr=="*");
    
    const std::vector<std::string> branch_list = GetBranchList(branch_list_expr);
    std::vector<std::string>::const_iterator begin = branch_list.begin();
    std::vector<std::string>::const_iterator end   = branch_list.end();

    if ( use_all || std::find(begin, end, "EventNo")!=end ) {
      KanaFunctions::ActivateBranch(tr, "EventNo", &EventNo);
      Info("SetBranchAddresses", "EventNo is set");
    }
    if ( use_all || std::find(begin, end, "L1TrigNo")!=end ) {
      KanaFunctions::ActivateBranch(tr, "L1TrigNo", &L1TrigNo);
      Info("SetBranchAddresses", "L1TrigNo is set");
    }
    if ( use_all || std::find(begin, end, "L2TrigNo")!=end ) {
      KanaFunctions::ActivateBranch(tr, "L2TrigNo", &L2TrigNo);
      Info("SetBranchAddresses", "L2TrigNo is set");
    }
    if ( use_all || std::find(begin, end, "SpillNo")!=end ) {
      KanaFunctions::ActivateBranch(tr, "SpillNo", &SpillNo);
      Info("SetBranchAddresses", "SpillNo is set");
    }
    if ( use_all || std::find(begin, end, "TimeStamp")!=end ) {
      KanaFunctions::ActivateBranch(tr, "TimeStamp", &TimeStamp);
      Info("SetBranchAddresses", "TimeStamp is set");
    }
    if ( use_all || std::find(begin, end, "L2TimeStamp")!=end ) {
      KanaFunctions::ActivateBranch(tr, "L2TimeStamp", &L2TimeStamp);
      Info("SetBranchAddresses", "L2TimeStamp set");
    }
    if ( use_all || std::find(begin, end, "Error")!=end ) {
      KanaFunctions::ActivateBranch(tr, "Error", &Error);
      Info("SetBranchAddresses", "Error is set");
    }
    if ( use_all || std::find(begin, end, "TrigTagMismatch")!=end ) {
      KanaFunctions::ActivateBranch(tr, "TrigTagMismatch", &TrigTagMismatch);
      Info("SetBranchAddresses", "TrigTagMismatch is set");
    }
    if ( use_all || std::find(begin, end, "L2AR")!=end ) {
      KanaFunctions::ActivateBranch(tr, "L2AR", &L2AR);
      Info("SetBranchAddresses", "L2AR is set");
    }
    if ( use_all || std::find(begin, end, "DetectorBit")!=end ) {
      KanaFunctions::ActivateBranch(tr, "DetectorBit", &DetectorBit);
      Info("SetBranchAddresses", "DetectorBit is set");
    }
    if ( use_all || std::find(begin, end, "RawTrigBit")!=end ) {
      KanaFunctions::ActivateBranch(tr, "RawTrigBit", &RawTrigBit);
      Info("SetBranchAddresses", "RawTrigBit is set");
    }
    if ( use_all || std::find(begin, end, "ScaledTrigBit")!=end ) {
      KanaFunctions::ActivateBranch(tr, "ScaledTrigBit", &ScaledTrigBit);
      Info("SetBranchAddresses", "ScaledTrigBit is set");
    }

    if ( use_all || std::find(begin, end, "COE_Et_overflow")!=end ) {
      KanaFunctions::ActivateBranch(tr, "COE_Et_overflow", &COE_Et_overflow);
      Info("SetBranchAddresses", "COE_Et_overflow is set");
    }
    if ( use_all || std::find(begin, end, "COE_L2_override")!=end ) {
      KanaFunctions::ActivateBranch(tr, "COE_L2_override", &COE_L2_override);
      Info("SetBranchAddresses", "COE_L2_override is set");
    }
    if ( use_all || std::find(begin, end, "COE_Esum")!=end ) {
      KanaFunctions::ActivateBranch(tr, "COE_Esum", &COE_Esum);
      Info("SetBranchAddresses", "COE_Esum is set");
    }
    if ( use_all || std::find(begin, end, "COE_Ex")!=end ) {
      KanaFunctions::ActivateBranch(tr, "COE_Ex", &COE_Ex);
      Info("SetBranchAddresses", "COE_Ex is set");
    }
    if ( use_all || std::find(begin, end, "COE_Ey")!=end ) {
      KanaFunctions::ActivateBranch(tr, "COE_Ey", &COE_Ey);
      Info("SetBranchAddresses", "COE_Ey is set");
    }

    if ( use_all || std::find(begin, end, "LeftEt")!=end ) {
      KanaFunctions::ActivateBranch(tr, "LeftEt", &LeftEt);
      Info("SetBranchAddresses", "LeftEt is set");
    }
    if ( use_all || std::find(begin, end, "LeftEt_raw")!=end ) {
      KanaFunctions::ActivateBranch(tr, "LeftEt_raw", &LeftEt_raw);
      Info("SetBranchAddresses", "LeftEt_raw is set");
    }
    if ( use_all || std::find(begin, end, "RightEt")!=end ) {
      KanaFunctions::ActivateBranch(tr, "RightEt", &RightEt);
      Info("SetBranchAddresses", "RightEt is set");
    }
    if ( use_all || std::find(begin, end, "RightEt_raw")!=end ) {
      KanaFunctions::ActivateBranch(tr, "RightEt_raw", &RightEt_raw);
      Info("SetBranchAddresses", "RightEt_raw is set");
    }

    if ( use_all || std::find(begin, end, "MactrisL1TrigNo")!=end ) {
      KanaFunctions::ActivateBranch(tr, "MactrisL1TrigNo", &MactrisL1TrigNo);
      Info("SetBranchAddresses", "MactrisL1TrigNo is set");
    }
    if ( use_all || std::find(begin, end, "MactrisTimeStamp")!=end ) {
      KanaFunctions::ActivateBranch(tr, "MactrisTimeStamp", &MactrisTimeStamp);
      Info("SetBranchAddresses", "MactrisTimeStamp is set");
    }
    if ( use_all || std::find(begin, end, "L3TrigBit")!=end ) {
      KanaFunctions::ActivateBranch(tr, "L3TrigBit", &L3TrigBit);
      Info("SetBranchAddresses", "L3TrigBit is set");
    }
    
    if ( use_all || std::find(begin, end, "EventID")!=end ) {
      KanaFunctions::ActivateBranch(tr, "EventID", &EventID);
      Info("SetBranchAddresses", "EventID is set");
    }
    if ( use_all || std::find(begin, end, "EtRaw")!=end ) {
      KanaFunctions::ActivateBranch(tr, "EtRaw", &EtRaw);
      Info("SetBranchAddresses", "EtRaw is set");
    }
    if ( use_all || std::find(begin, end, "ExtTrigType")!=end ) {
      KanaFunctions::ActivateBranch(tr, "ExtTrigType", &ExtTrigType);
      Info("SetBranchAddresses", "ExtTrigType is set");
    }
    if ( use_all || std::find(begin, end, "DeltaTrig")!=end ) {
      KanaFunctions::ActivateBranch(tr, "DeltaTrig", &DeltaTrig);
      Info("SetBranchAddresses", "DeltaTrig is set");
    }
    if ( use_all || std::find(begin, end, "PSTrig")!=end ) {
      KanaFunctions::ActivateBranch(tr, "PSTrig", &PSTrig);
      Info("SetBranchAddresses", "PSTrig is set");
    }
    
    if ( use_all || std::find(begin, end, "CDTNum")!=end ) {
      KanaFunctions::ActivateBranch(tr, "CDTNum", &CDTNum);
      Info("SetBranchAddresses", "CDTNum is set");
    }
    if ( use_all || std::find(begin, end, "CDTData")!=end ) {
      KanaFunctions::ActivateBranch(tr, "CDTData", CDTData);
      Info("SetBranchAddresses", "CDTData is set");
    }
    if ( use_all || std::find(begin, end, "CDTBit")!=end ) {
      KanaFunctions::ActivateBranch(tr, "CDTBit", CDTBit);
      Info("SetBranchAddresses", "CDTBit is set");
    }
    if ( use_all || std::find(begin, end, "CDTVetoData")!=end ) {
      KanaFunctions::ActivateBranch(tr, "CDTVetoData", CDTVetoData);
      Info("SetBranchAddresses", "CDTVetoData is set");
    }
    if ( use_all || std::find(begin, end, "CDTVetoBitMap")!=end ) {
      KanaFunctions::ActivateBranch(tr, "CDTVetoBitMap", CDTVetoBitMap);
      Info("SetBranchAddresses", "CDTVetoBitMap is set");
    }
    if ( use_all || std::find(begin, end, "CDTVetoBitSum")!=end ) {
      KanaFunctions::ActivateBranch(tr, "CDTVetoBitSum", CDTVetoBitSum);
      Info("SetBranchAddresses", "CDTVetoBitSum is set");
    }

    return branch_list;
  }

  std::vector<std::string> KanaLegacyDstEventData::AddBranches(TTree *tr, const std::string branch_list_expr)
  {
    const Bool_t use_all = (branch_list_expr=="*");
    
    const std::vector<std::string> branch_list = GetBranchList(branch_list_expr);
    std::vector<std::string>::const_iterator begin = branch_list.begin();
    std::vector<std::string>::const_iterator end   = branch_list.end();
    
    if ( use_all || std::find(begin, end, "EventNo")!=end ) {
      tr->Branch("EventNo", &EventNo, "EventNo/I");
      Info("AddBranches", "add EventNo");
    }
    if ( use_all || std::find(begin, end, "L1TrigNo")!=end ) {
      tr->Branch("L1TrigNo", &L1TrigNo, "L1TrigNo/I");
      Info("AddBranches", "add L1TrigNo");
    }
    if ( use_all || std::find(begin, end, "L2TrigNo")!=end ) {
      tr->Branch("L2TrigNo", &L2TrigNo, "L2TrigNo/I");
      Info("AddBranches", "add L2TrigNo");
    }
    if ( use_all || std::find(begin, end, "SpillNo")!=end ) {
      tr->Branch("SpillNo", &SpillNo, "SpillNo/S");
      Info("AddBranches", "add SpillNo");
    }
    if ( use_all || std::find(begin, end, "TimeStamp")!=end ) {
      tr->Branch("TimeStamp", &TimeStamp, "TimeStamp/I");
      Info("AddBranches", "add TimeStamp");
    }
    if ( use_all || std::find(begin, end, "L2TimeStamp")!=end ) {
      tr->Branch("L2TimeStamp", &L2TimeStamp, "L2TimeStamp/i");
      Info("AddBranches", "add L2TimeStamp");
    }
    if ( use_all || std::find(begin, end, "Error")!=end ) {
      tr->Branch("Error", &Error, "Error/S");
      Info("AddBranches", "add Error");
    }
    if ( use_all || std::find(begin, end, "TrigTagMismatch")!=end ) {
      tr->Branch("TrigTagMismatch", &TrigTagMismatch, "TrigTagMismatch/S");
      Info("AddBranches", "add TrigTagMismatch");
    }
    if ( use_all || std::find(begin, end, "L2AR")!=end ) {
      tr->Branch("L2AR", &L2AR, "L2AR/S");
      Info("AddBranches", "add L2AR");
    }
    if ( use_all || std::find(begin, end, "DetectorBit")!=end ) {
      tr->Branch("DetectorBit", &DetectorBit, "DetectorBit/i");
      Info("AddBranches", "add DetectorBit");
    }
    if ( use_all || std::find(begin, end, "RawTrigBit")!=end ) {
      tr->Branch("RawTrigBit", &RawTrigBit, "RawTrigBit/i");
      Info("AddBranches", "add RawTrigBit");
    }
    if ( use_all || std::find(begin, end, "ScaledTrigBit")!=end ) {
      tr->Branch("ScaledTrigBit", &ScaledTrigBit, "ScaledTrigBit/i");
      Info("AddBranches", "add ScaledTrigBit");
    }
    if ( use_all || std::find(begin, end, "COE_Et_overflow")!=end ) {
      tr->Branch("COE_Et_overflow", &COE_Et_overflow, "COE_Et_overflow/S");
      Info("AddBranches", "add COE_Et_overflow");
    }
    if ( use_all || std::find(begin, end, "COE_L2_override")!=end ) {
      tr->Branch("COE_L2_override", &COE_L2_override, "COE_L2_override/S");
      Info("AddBranches", "add COE_L2_override");
    }
    if ( use_all || std::find(begin, end, "COE_Esum")!=end ) {
      tr->Branch("COE_Esum", &COE_Esum, "COE_Esum/I");
      Info("AddBranches", "add COE_Esum");
    }
    if ( use_all || std::find(begin, end, "COE_Ex")!=end ) {
      tr->Branch("COE_Ex", &COE_Ex, "COE_Ex/I");
      Info("AddBranches", "add COE_Ex");
    }
    if ( use_all || std::find(begin, end, "COE_Ey")!=end ) {
      tr->Branch("COE_Ey", &COE_Ey, "COE_Ey/I");
      Info("AddBranches", "add COE_Ey");
    }
    if ( use_all || std::find(begin, end, "LeftEt")!=end ) {
      tr->Branch("LeftEt", &LeftEt, "LeftEt/I");
      Info("AddBranches", "add LeftEt");
    }
    if ( use_all || std::find(begin, end, "LeftEt_raw")!=end ) {
      tr->Branch("LeftEt_raw", &LeftEt_raw, "LeftEt_raw/I");
      Info("AddBranches", "add LeftEt_raw");
    }
    if ( use_all || std::find(begin, end, "RightEt")!=end ) {
      tr->Branch("RightEt", &RightEt, "RightEt/I");
      Info("AddBranches", "add RightEt");
    }
    if ( use_all || std::find(begin, end, "RightEt_raw")!=end ) {
      tr->Branch("RightEt_raw", &RightEt_raw, "RightEt_raw/I");
      Info("AddBranches", "add RightEt_raw");
    }
    
    if ( use_all || std::find(begin, end, "MactrisL1TrigNo")!=end ) {
      tr->Branch("MactrisL1TrigNo", &MactrisL1TrigNo, "MactrisL1TrigNo/I");
      Info("AddBranches", "add MactrisL1TrigNo");
    }
    if ( use_all || std::find(begin, end, "MactrisTimeStamp")!=end ) {
      tr->Branch("MactrisTimeStamp", &MactrisTimeStamp, "MactrisTimeStamp/I");
      Info("AddBranches", "add MactrisTimeStamp");
    }
    if ( use_all || std::find(begin, end, "L3TrigBit")!=end ) {
      tr->Branch("L3TrigBit", &L3TrigBit, "L3TrigBit/i");
      Info("AddBranches", "add L3TrigBit");
    }
    
    if ( use_all || std::find(begin, end, "EventID")!=end ) {
      tr->Branch("EventID", &EventID, "EventID/I");
      Info("AddBranches", "add EventID");
    }
    if ( use_all || std::find(begin, end, "EtRaw")!=end ) {
      tr->Branch("EtRaw", &EtRaw, "EtRaw/I");
      Info("AddBranches", "add EtRaw");
    }
    if ( use_all || std::find(begin, end, "ExtTrigType")!=end ) {
      tr->Branch("ExtTrigType", &ExtTrigType, "ExtTrigType/S");
      Info("AddBranches", "add ExtTrigType");
    }
    if ( use_all || std::find(begin, end, "DeltaTrig")!=end ) {
      tr->Branch("DeltaTrig", &DeltaTrig, "DeltaTrig/S");
      Info("AddBranches", "add DeltaTrig");
    }
    if ( use_all || std::find(begin, end, "PSTrig")!=end ) {
      tr->Branch("PSTrig", &PSTrig, "PSTrig/S");
      Info("AddBranches", "add PSTrig");
    }
    
    if ( use_all || std::find(begin, end, "CDTNum")!=end ) {
      tr->Branch("CDTNum", &CDTNum, "CDTNum/I");
      Info("AddBranches", "add CDTNum");
    }
    if ( use_all || std::find(begin, end, "CDTData")!=end ) {
      tr->Branch("CDTData", CDTData, "CDTData[3][64]/S");
      Info("AddBranches", "add CDTData");
    }
    if ( use_all || std::find(begin, end, "CDTBit")!=end ) {
      tr->Branch("CDTBit", CDTBit, "CDTBit[38][38]/S");
      Info("AddBranches", "add CDTBit");
    }
    if ( use_all || std::find(begin, end, "CDTVetoData")!=end ) {
      tr->Branch("CDTVetoData", CDTVetoData, "CDTVetoData[4][64]/S");
      Info("AddBranches", "add CDTVetoData");
    }
    if ( use_all || std::find(begin, end, "CDTVetoBitMap")!=end ) {
      tr->Branch("CDTVetoBitMap", CDTVetoBitMap, "CDTVetoBitMap[13][300]/S");
      Info("AddBranches", "add CDTVetoBitMap");
    }
    if ( use_all || std::find(begin, end, "CDTVetoBitSum")!=end ) {
      tr->Branch("CDTVetoBitSum", CDTVetoBitSum, "CDTVetoBitSum[13]/S");
      Info("AddBranches", "add CDTVetoBitSum");
    }

    return branch_list;
  }
  
  void KanaLegacyDstEventData::Clear(Option_t*)
  {
    EventNo         = 0;
    L1TrigNo        = 0;
    L2TrigNo        = 0;
    SpillNo         = 0;
    TimeStamp       = 0;
    L2TimeStamp     = 0;
    Error           = 0;
    TrigTagMismatch = 0;
    L2AR            = 0;
    DetectorBit     = 0;
    RawTrigBit      = 0;
    ScaledTrigBit   = 0;

    COE_Et_overflow = 0;
    COE_L2_override = 0;
    COE_Esum = 0;
    COE_Ex   = 0;
    COE_Ey   = 0;

    LeftEt      = 0;
    LeftEt_raw  = 0;
    RightEt     = 0;
    RightEt_raw = 0;

    MactrisL1TrigNo  = 0;
    MactrisTimeStamp = 0;
    
    L3TrigBit = 0;

    EventID     = 0;
    EtRaw       = 0;
    ExtTrigType = 0;
    DeltaTrig   = 0;
    PSTrig      = 0;

    CDTNum = 0;
    for (Int_t i=0;i<3;++i) {
      for (Int_t j=0;j<64;++j) {
	CDTData[i][j]=0;
      }
    }
    for (Int_t i=0;i<38;++i) {
      for (Int_t j=0;j<38;++j) {
	CDTBit[i][j]=0;
      }
    }
    for (Int_t i=0;i<4;++i) {
      for (Int_t j=0;j<64;++j) {
	CDTVetoData[i][j]=0;
      }
    }
    for (Int_t i=0;i<13;++i) {
      for (Int_t j=0;j<300;++j) {
	CDTVetoBitMap[i][j]=0;
      }
      CDTVetoBitSum[i]=0;
    }
  }
  
}
