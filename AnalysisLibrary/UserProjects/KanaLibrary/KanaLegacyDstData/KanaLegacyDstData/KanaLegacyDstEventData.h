/* -*- C++ -*- */
#ifndef E14ANA_KANALEGACYDSTDATA_KANALEGACYDSTEVENTDATA_H
#define E14ANA_KANALEGACYDSTDATA_KANALEGACYDSTEVENTDATA_H

#include <TTree.h>

#include "KanaLegacyDstCommon.h"

namespace KanaLibrary
{
  class KanaLegacyDstEventData : public KanaLegacyDstCommon
  {
  public:
    KanaLegacyDstEventData();
    ~KanaLegacyDstEventData();
    
  private:
    void InitKanaLegacyDstEventData();
    
  public:
    virtual std::vector<std::string> SetBranchAddresses(TTree *tr, const std::string branch_list_expr="*");
    virtual std::vector<std::string> AddBranches(TTree *tr, const std::string branch_list_expr="*");

    virtual void Clear(Option_t* = "");
    
  public:
    Int_t   EventNo;
    Int_t   L1TrigNo;
    Int_t   L2TrigNo;
    Short_t SpillNo;
    Int_t   TimeStamp;
    UInt_t  L2TimeStamp;
    Short_t Error;
    Short_t TrigTagMismatch;
    Short_t L2AR;
    UInt_t  DetectorBit;
    UInt_t  RawTrigBit;
    UInt_t  ScaledTrigBit;
    Short_t COE_Et_overflow;
    Short_t COE_L2_override;
    Int_t   COE_Esum;
    Int_t   COE_Ex;
    Int_t   COE_Ey;
    Int_t   LeftEt;
    Int_t   LeftEt_raw;
    Int_t   RightEt;
    Int_t   RightEt_raw;

    Int_t  MactrisL1TrigNo;
    Int_t  MactrisTimeStamp;
    UInt_t L3TrigBit;
    
    Int_t   EventID;
    Int_t   EtRaw;
    Short_t ExtTrigType;
    Short_t DeltaTrig;
    Short_t PSTrig;

    Int_t   CDTNum;
    Short_t CDTData[3][64];
    Short_t CDTBit[38][38];
    Short_t CDTVetoData[4][64];
    Short_t CDTVetoBitMap[13][300];
    Short_t CDTVetoBitSum[13];

    ClassDef(KanaLegacyDstEventData, 1);
  };
  
}

#endif
