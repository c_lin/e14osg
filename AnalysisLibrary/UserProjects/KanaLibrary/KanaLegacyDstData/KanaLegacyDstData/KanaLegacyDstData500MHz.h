/* -*- C++ -*- */
#ifndef E14ANA_KANALEGACYDSTDATA_KANALEGACYDSTDATA500MHZ_H
#define E14ANA_KANALEGACYDSTDATA_KANALEGACYDSTDATA500MHZ_H

#include "KanaVirtualLegacyDstData.h"

#include <TTree.h>

namespace KanaLibrary
{
  class KanaLegacyDstData500MHz : public KanaVirtualLegacyDstData
  {
  public:
    KanaLegacyDstData500MHz();
    KanaLegacyDstData500MHz(const std::string name, const Int_t n_ch=3000);
    virtual ~KanaLegacyDstData500MHz();
    
  private:
    enum {
      MAX_N_HITS = 20,
      N_SAMPLES  = 256
    };

    void InitKanaLegacyDstData500MHz();

  public:
    Short_t  *nHits;
    Short_t  (*Peak)[MAX_N_HITS];
    Float_t  (*IntegratedADC)[MAX_N_HITS];
    Float_t  (*InitialTime)  [MAX_N_HITS];
    Float_t  (*Ene) [MAX_N_HITS];
    Float_t  (*Time)[MAX_N_HITS];
    Long64_t *EtSum;
    Float_t  *MaxDDc;
    Short_t  *MaxDDcSam;
     
  public:
    virtual std::vector<std::string> SetBranchAddresses(TTree *tr, const std::string branch_list_expr="*");
    virtual std::vector<std::string> AddBranches(TTree *tr, const std::string branch_list_expr="*");

    virtual void ReduceData(const std::vector<Int_t> &ch_list);

    virtual void Clear(Option_t* = "");
    
    ClassDef(KanaLegacyDstData500MHz, 1);
  };
}


#endif
