/* -*- C++ -*- */
#ifndef E14ANA_KANALEGACYDSTDATA_KANALEGACYDSTDATA125MHZ_H
#define E14ANA_KANALEGACYDSTDATA_KANALEGACYDSTDATA125MHZ_H

#include "KanaVirtualLegacyDstData.h"

#include <TTree.h>

namespace KanaLibrary
{
  class KanaLegacyDstData125MHz : public KanaVirtualLegacyDstData
  {
  public:
    KanaLegacyDstData125MHz();
    KanaLegacyDstData125MHz(const std::string name, const Int_t n_ch=3000);
    virtual ~KanaLegacyDstData125MHz();
    
  private:
    enum {
      N_SAMPLES = 64
    };

    void InitKanaLegacyDstData125MHz();

  public:
    Short_t  *Peak;
    Float_t  *IntegratedADC;
    Float_t  *InitialTime;
    Float_t  *Ene;
    Float_t  *Time;
    Float_t  *FallTime;
    Float_t  *InitialPTime;
    Float_t  *PTime;
    Float_t  *PTimeOrig;
    Float_t  *MaxDDc;
    Short_t  *MaxDDcSam;
    Long64_t *EtSum;
    
  public:
    virtual std::vector<std::string> SetBranchAddresses(TTree *tr, const std::string branch_list_expr="*");
    virtual std::vector<std::string> AddBranches(TTree *tr, const std::string branch_list_expr="*");

    virtual void ReduceData(const std::vector<Int_t> &ch_list);

    virtual void Clear(Option_t* = "");
    
    ClassDef(KanaLegacyDstData125MHz, 1);
  };
}


#endif
