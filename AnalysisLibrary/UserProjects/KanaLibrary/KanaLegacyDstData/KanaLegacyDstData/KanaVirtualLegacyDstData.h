/* -*- C++ -*-  */
#ifndef E14ANA_KANALEGACYDSTDATA_KANAVIRTUALLEGACYDSTDATA_H
#define E14ANA_KANALEGACYDSTDATA_KANAVIRTUALLEGACYDSTDATA_H

#include <string>
#include <vector>

#include <TTree.h>

#include "KanaLegacyDstData/KanaLegacyDstCommon.h"

namespace KanaLibrary
{
  class KanaVirtualLegacyDstData : public KanaLegacyDstCommon
  {
  public:
    KanaVirtualLegacyDstData();
    KanaVirtualLegacyDstData(const std::string name, const Int_t n_ch);
    virtual ~KanaVirtualLegacyDstData();
    
  protected:
    const std::string Name;
    const Int_t nChannels;
    
  private:
    void InitKanaVirtualLegacyDstData();

  public:
    virtual std::vector<std::string> SetBranchAddresses(TTree *tr, const std::string branch_list_expr)=0;
    virtual std::vector<std::string> AddBranches(TTree *tr, const std::string branch_list_expr)=0;
    
    Int_t GetNumber(const Int_t ch_ID);

    virtual void ReduceData(const std::vector<Int_t> &ch_list)=0;

    virtual void Clear(Option_t* = "");
    
  public:
    Int_t    Number;
    Int_t    *ModID;
    Int_t    *TimeStamp;
    Float_t  *Pedestal;
    
    ClassDef(KanaVirtualLegacyDstData, 1);
  };
}

#endif
