/* -*- C++ -*- */
#ifndef E14ANA_KANALEGACYDSTDATA_KANALEGACYDSTCOMMON_H
#define E14ANA_KANALEGACYDSTDATA_KANALEGACYDSTCOMMON_H

#include <TObject.h>

namespace KanaLibrary
{
  class KanaLegacyDstCommon : public TObject
  {
  public:
    KanaLegacyDstCommon();
    virtual ~KanaLegacyDstCommon();
    
  protected:
    std::vector<std::string> GetBranchList(const std::string branch_list_expr);

    ClassDef(KanaLegacyDstCommon, 1);
  };
}

#endif
