#include "KanaConvManager/KanaFADCMapManager.h"

#include <iostream>
#include <sstream>

#include <TChain.h>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaFADCMapManager)

namespace KanaLibrary
{
  
  KanaFADCMapManager::KanaFADCMapManager()
    : AccRunID(-1),
      ProVersion(-1),
      DetectorName("")
  {
    InitKanaFADCMapManager();
  }
  
  KanaFADCMapManager::KanaFADCMapManager(const Int_t acc_run_ID, const Int_t pro_version,
					 const std::string detector_name)
    : AccRunID(acc_run_ID),
      ProVersion(pro_version),
      DetectorName(detector_name)
  {
    InitKanaFADCMapManager();
    
    FileReader = new KanaFileReader;
    
    /* get E14ID list */
    E14IDList = KanaFunctions::GetE14IDList(AccRunID, ProVersion, DetectorName);
  }
  
  
  void KanaFADCMapManager::InitKanaFADCMapManager()
  {
    FileReader = NULL;
  }
  
  
  void KanaFADCMapManager::SetTopDir(const std::string top_dir)
  {
    /* set parameters */
    TopDirectory = top_dir;
    
    /* load mpafile list */
    LoadListFile(TopDirectory+"/ch_map_"+DetectorName+"_list.txt");
  }
  
  void KanaFADCMapManager::LoadListFile(const std::string path)
  {
    FileReader->SetFile(path);
    
    std::vector<MapFileListData> list_temp;
    for (Int_t i_line=0, n_lines=FileReader->GetnLines(); i_line < n_lines; ++i_line) {
      FileReader->ReadLine(i_line, "ssv");
      const Int_t first_run_ID = std::atoi(FileReader->GetWord(0).c_str());
      const Int_t last_run_ID  = std::atoi(FileReader->GetWord(1).c_str());
      const Int_t file_ID      = std::atoi(FileReader->GetWord(2).c_str());
      MapFileListData data_temp = { file_ID, first_run_ID, last_run_ID };
      list_temp.push_back(data_temp);
    }
    
    MapFileList = list_temp;
  }
  
  
  void KanaFADCMapManager::LoadFADCMap(const Int_t run_ID)
  {
    /* get mapfile ID */
    Bool_t is_found = kFALSE;
    Int_t  file_ID  = -1;
    for (std::vector<MapFileListData>::iterator
	   itr=MapFileList.begin(), end=MapFileList.end(); itr!=end; ++itr) {
      if (itr->FirstRunID<=run_ID && run_ID<=itr->LastRunID) {
	file_ID  = itr->FileID;
	is_found = kTRUE;
	break;
      }
    }
    
    /* read mapfile */
    FADCMap.clear();
    
    if (!is_found) {
      NoFADCMapData = kTRUE;
    } else {
      NoFADCMapData = kFALSE;
      
      std::stringstream path;
      path << TopDirectory << "/mapfile/ch_map_" << DetectorName << "_" << file_ID << ".txt";
      FileReader->SetFile(path.str());
      for (Int_t i_line=1, n_lines=FileReader->GetnLines(); i_line < n_lines; ++i_line) {
	FileReader->ReadLine(i_line, "ssv");
	if (FileReader->GetnWords()!=4) continue;
	FADCMapData &data_temp = FADCMap[ std::atoi(FileReader->GetWord(0).c_str()) ];
	data_temp.CrateID   = std::atoi(FileReader->GetWord(1).c_str());
	data_temp.ModuleID  = std::atoi(FileReader->GetWord(2).c_str());
	data_temp.ChannelID = std::atoi(FileReader->GetWord(3).c_str());
      }
    }
  }
  
  
  void KanaFADCMapManager::DumpFADCMap(const Int_t n_ch)
  {
    std::cout << "   - FADC map data for " << DetectorName << std::endl;
    std::cout << "     -- top dir    : " << TopDirectory << std::endl;
    std::cout << "     -- acc run ID : " << AccRunID     << std::endl;
    std::cout << "     -- pro version: " << ProVersion   << std::endl;
    
    Int_t n_dump = 0;
    for (std::vector<Int_t>::iterator itr = E14IDList.begin(); itr != E14IDList.end(); ++itr) {
      const Int_t e14_ID = (*itr);
      const FADCMapData &map_data = FADCMap[e14_ID];
      std::cout << "     CH " << e14_ID << ": " << std::endl;
      std::cout << "       CrateID  : " << map_data.CrateID   << std::endl;
      std::cout << "       ModuleID : " << map_data.ModuleID  << std::endl;
      std::cout << "       ChannelID: " << map_data.ChannelID << std::endl;
      ++n_dump;
      
      if (n_dump>=n_ch) break;
    }
  }
  
  
  KanaFADCMapManager::~KanaFADCMapManager()
  {
    if (FileReader !=NULL) delete FileReader;
  }
  
}
