#include "KanaConvManager/KanaFADCCrateManager125MHz.h"

ClassImp(KanaLibrary::KanaFADCCrateManager125MHz)

namespace KanaLibrary
{
  
  KanaFADCCrateManager125MHz::KanaFADCCrateManager125MHz()
  {
    InitKanaFADCCrateManager125MHz();
    
    Data          = new Short_t [FADC_N_MODULES][FADC_N_CHANNELS][FADC_N_SAMPLES];
    Pedestal      = new Float_t [FADC_N_MODULES][FADC_N_CHANNELS];
    PeakHeight    = new Short_t [FADC_N_MODULES][FADC_N_CHANNELS];
    PeakTime      = new Short_t [FADC_N_MODULES][FADC_N_CHANNELS];
    IntegratedADC = new Float_t [FADC_N_MODULES][FADC_N_CHANNELS];
  }
  
  void KanaFADCCrateManager125MHz::InitKanaFADCCrateManager125MHz()
  {
    Data          = NULL;
    Pedestal      = NULL;
    PeakHeight    = NULL;
    PeakTime      = NULL;
    IntegratedADC = NULL;
  }
  
  
  Int_t KanaFADCCrateManager125MHz::GetnSamples()
  {
    return FADC_N_SAMPLES;
  }
  
  
  Int_t KanaFADCCrateManager125MHz::GetResolution()
  {
    return FADC_RESOLUTION;
  }
  
  
  void KanaFADCCrateManager125MHz::SetBranchAddresses(TTree *tr)
  {
    SetBranchAddressesBase(tr);
    SetBranchAddresses125MHz(tr);
  }
  
  void KanaFADCCrateManager125MHz::SetBranchAddresses125MHz(TTree *tr)
  {
    {
      const std::string bname = "Data";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Data);
    }
    {
      const std::string bname = "Pedestal";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Pedestal);
    }
    {
      const std::string bname = "PeakHeight";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PeakHeight);
    }
    {
      const std::string bname = "PeakTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PeakTime);
    }
    {
      const std::string bname = "IntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), IntegratedADC);
    }
  }
  
  
  const Short_t* KanaFADCCrateManager125MHz::GetData(const Int_t mod_ID, const Int_t ch_ID)
  {
    return &Data[mod_ID][ch_ID][0];
  }
  
  Float_t KanaFADCCrateManager125MHz::GetPedestal(const Int_t mod_ID, const Int_t ch_ID)
  {
    return Pedestal[mod_ID][ch_ID];
  }
  
  Short_t KanaFADCCrateManager125MHz::GetPeakHeight(const Int_t mod_ID, const Int_t ch_ID)
  {
    return PeakHeight[mod_ID][ch_ID];
  }
  
  Short_t KanaFADCCrateManager125MHz::GetPeakTime(const Int_t mod_ID, const Int_t ch_ID)
  {
    return PeakTime[mod_ID][ch_ID];
  }
  
  Float_t KanaFADCCrateManager125MHz::GetIntegratedADC(const Int_t mod_ID, const Int_t ch_ID)
  {
    return IntegratedADC[mod_ID][ch_ID];
  }
  
  
  KanaFADCCrateManager125MHz::~KanaFADCCrateManager125MHz()
  {
    if (PeakHeight   != NULL) delete [] PeakHeight;
    if (PeakTime     != NULL) delete [] PeakTime;
    if (IntegratedADC!= NULL) delete [] IntegratedADC;
    if (Pedestal     != NULL) delete [] Pedestal;
    if (Data         != NULL) delete [] Data;
  }
  
}
