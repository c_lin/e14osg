#include "KanaConvManager/KanaFADCCrateManagerBase.h"

ClassImp(KanaLibrary::KanaFADCCrateManagerBase)

namespace KanaLibrary
{
  
  KanaFADCCrateManagerBase::KanaFADCCrateManagerBase()
  {
    InitKanaFADCCrateManagerBase();
    
    TimeStamp = new Int_t [FADC_N_MODULES];
  }
  
  void KanaFADCCrateManagerBase::InitKanaFADCCrateManagerBase()
  {
    ScaledTrigBit = 0;
    TimeStamp     = NULL;
  }
  
  
  void KanaFADCCrateManagerBase::SetBranchAddresses(TTree *tr)
  {
    SetBranchAddressesBase(tr);
  }
  
  
  void KanaFADCCrateManagerBase::SetBranchAddressesBase(TTree *tr)
  {
    {
      const std::string bname = "ScaledTrigBit";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &ScaledTrigBit);
    }
    {
      const std::string bname = "TimeStamp";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), TimeStamp);
    }
  }
  
  
  KanaFADCCrateManagerBase::~KanaFADCCrateManagerBase()
  {
    if (TimeStamp!=NULL) delete [] TimeStamp;
  }
  
}
