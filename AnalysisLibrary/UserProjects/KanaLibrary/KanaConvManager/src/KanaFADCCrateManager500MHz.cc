#include "KanaConvManager/KanaFADCCrateManager500MHz.h"

ClassImp(KanaLibrary::KanaFADCCrateManager500MHz)

namespace KanaLibrary
{
  
  KanaFADCCrateManager500MHz::KanaFADCCrateManager500MHz()
  {
    InitKanaFADCCrateManager500MHz();
    
    Data        = new Short_t [FADC_N_MODULES][FADC_N_CHANNELS][FADC_N_SAMPLES];
    Pedestal    = new Float_t [FADC_N_MODULES][FADC_N_CHANNELS];
    AllIntegral = new Float_t [FADC_N_MODULES][FADC_N_CHANNELS];
    nHit        = new Short_t [FADC_N_MODULES][FADC_N_CHANNELS];
    PeakHeight  = new Float_t [FADC_N_MODULES][FADC_N_CHANNELS][MAX_N_HITS];
    PeakTime    = new Short_t [FADC_N_MODULES][FADC_N_CHANNELS][MAX_N_HITS];
    Integral    = new Float_t [FADC_N_MODULES][FADC_N_CHANNELS][MAX_N_HITS];
  }
  
  void KanaFADCCrateManager500MHz::InitKanaFADCCrateManager500MHz()
  {
    Data        = NULL;
    Pedestal    = NULL;
    AllIntegral = NULL;
    nHit        = NULL;
    PeakHeight  = NULL;
    PeakTime    = NULL;
    Integral    = NULL;
  }
  
  
  Int_t KanaFADCCrateManager500MHz::GetnSamples()
  {
    return FADC_N_SAMPLES;
  }
  
  
  Int_t KanaFADCCrateManager500MHz::GetResolution()
  {
    return FADC_RESOLUTION;
  }
  
  
  void KanaFADCCrateManager500MHz::SetBranchAddresses(TTree *tr)
  {
    SetBranchAddressesBase(tr);
    SetBranchAddresses500MHz(tr);
  }
  
  void KanaFADCCrateManager500MHz::SetBranchAddresses500MHz(TTree *tr)
  {
    {
      const std::string bname = "Data";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Data);
    }
    {
      const std::string bname = "Pedestal";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Pedestal);
    }
    {
      const std::string bname = "AllIntegral";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), AllIntegral);
    }
    {
      const std::string bname = "nHit";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), nHit);
    }
    {
      const std::string bname = "PeakHeight";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PeakHeight);
    }
    {
      const std::string bname = "PeakTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PeakTime);
    }
    {
      const std::string bname = "Integral";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Integral);
    }
  }
  
  
  const Short_t* KanaFADCCrateManager500MHz::GetData(const Int_t mod_ID, const Int_t ch_ID)
  {
    return &Data[mod_ID][ch_ID][0];
  }
  
  Float_t KanaFADCCrateManager500MHz::GetPedestal(const Int_t mod_ID, const Int_t ch_ID)
  {
    return Pedestal[mod_ID][ch_ID];
  }
  
  Float_t KanaFADCCrateManager500MHz::GetAllIntegral(const Int_t mod_ID, const Int_t ch_ID)
  {
    return AllIntegral[mod_ID][ch_ID];
  }
  
  Short_t KanaFADCCrateManager500MHz::GetnHit(const Int_t mod_ID, const Int_t ch_ID)
  {
    return nHit[mod_ID][ch_ID];
  }
  
  Float_t KanaFADCCrateManager500MHz
  ::GetPeakHeight(const Int_t mod_ID, const Int_t ch_ID, const Int_t i_hit)
  {
    return PeakHeight[mod_ID][ch_ID][i_hit];
  }
  
  Short_t KanaFADCCrateManager500MHz
  ::GetPeakTime(const Int_t mod_ID, const Int_t ch_ID, const Int_t i_hit)
  {
    return PeakTime[mod_ID][ch_ID][i_hit];
  }
  
  Float_t KanaFADCCrateManager500MHz
  ::GetIntegral(const Int_t mod_ID, const Int_t ch_ID, const Int_t i_hit)
  {
    return Integral[mod_ID][ch_ID][i_hit];
  }
  
  KanaFADCCrateManager500MHz::~KanaFADCCrateManager500MHz()
  {
    if (Data       != NULL) delete [] Data;
    if (Pedestal   != NULL) delete [] Pedestal;
    if (AllIntegral!= NULL) delete [] AllIntegral;
    if (nHit       != NULL) delete [] nHit;
    if (PeakHeight != NULL) delete [] PeakHeight;
    if (PeakTime   != NULL) delete [] PeakTime;
    if (Integral   != NULL) delete [] Integral;
  }
  
}
