#include "KanaConvManager/KanaConvTreeManager.h"

#include <algorithm>

#include <TROOT.h>

ClassImp(KanaLibrary::KanaConvTreeManager)

namespace KanaLibrary
{
  
  KanaConvTreeManager::KanaConvTreeManager()
  {
    InitKanaConvTreeManager();
  }
  
  void KanaConvTreeManager::InitKanaConvTreeManager()
  {
    RootFile = NULL;
  }
  
  
  Bool_t KanaConvTreeManager::OpenConvFile(std::string path)
  {
    if (RootFile!=NULL) {
      RootFile->Close();
      delete RootFile;
    }
    
    if (CrateManagerList.size()!=0) {
      for (std::map<Int_t,KanaFADCCrateManagerBase*>::iterator
	     itr=CrateManagerList.begin(); itr != CrateManagerList.end(); ++itr) {
	delete itr->second;
      }
    }
    
    CrateIDList.clear();
    CrateTreeList.clear();
    CrateManagerList.clear();
    
    RootFile = new TFile(path.c_str());
    
    return (RootFile->IsZombie() ? kFALSE : kTRUE);
  }
  
  
  Bool_t KanaConvTreeManager::ActivateCrateTree(const Int_t crate_ID, const Bool_t is_500MHz)
  {
    if ( std::find(CrateIDList.begin(), CrateIDList.end(), crate_ID)!=CrateIDList.end() ) {
      // in case already activated
      return kTRUE;
    }
    
    std::stringstream tr_name;
    tr_name << "Crate" << crate_ID;
    TTree *tr = (TTree*)RootFile->Get(tr_name.str().c_str());
    if (tr!=NULL) {
      CrateIDList.push_back(crate_ID);
      CrateTreeList[crate_ID]    = tr;
      
      KanaFADCCrateManagerBase *crate_manager;
      if (is_500MHz) {
	crate_manager = new KanaFADCCrateManager500MHz;
      } else {
	crate_manager = new KanaFADCCrateManager125MHz;
      }
      
      tr->SetBranchStatus("*", 0);
      crate_manager->SetBranchAddresses(tr);
      
      CrateManagerList[crate_ID] = crate_manager;
    }
    
    return (tr!=NULL ? kTRUE : kFALSE);
  }
  
  
  Bool_t KanaConvTreeManager::GetEntry(const Long64_t event_ID)
  {
    Bool_t is_valid = kTRUE;
    for (std::vector<Int_t>::iterator itr = CrateIDList.begin(), end = CrateIDList.end();
	 itr != end; ++itr) {
      const Int_t crate_ID = (*itr);
      const Int_t n_bytes = CrateTreeList[crate_ID]->GetEntry(event_ID);
      if (n_bytes==0) is_valid = kFALSE;
    }
    
    return is_valid;
  }
  
  
  KanaFADCCrateManagerBase* KanaConvTreeManager::GetCrateManager(const Int_t crate_ID)
  {
    return ( CrateManagerList.find(crate_ID)!=CrateManagerList.end() ?
	     CrateManagerList[crate_ID] : NULL );
  }
  
  
  KanaConvTreeManager::~KanaConvTreeManager()
  {
    if (RootFile!=NULL) {
      RootFile->Close();
      delete RootFile;
    }
    
    if (CrateManagerList.size()!=0) {
      for (std::map<Int_t,KanaFADCCrateManagerBase*>::iterator
	     itr=CrateManagerList.begin(); itr != CrateManagerList.end(); ++itr) {
	delete itr->second;
      }
    }
  }
  
}
