/* -*- C++ -*- */
#ifndef E14ANA_KANACONVMANAGER_KANAFADCMAPMANAGER_H
#define E14ANA_KANACONVMANAGER_KANAFADCMAPMANAGER_H

#include <string>
#include <vector>
#include <map>

#include <TObject.h>
#include <TTree.h>

#include "KanaLib/KanaFileReader.h"

namespace KanaLibrary
{
  
  class KanaFADCMapManager : public TObject
  {
  private:
  
    const Int_t AccRunID;
    const Int_t ProVersion;
    const std::string DetectorName;
    
    KanaFileReader *FileReader;
    
  public:

    struct MapFileListData
    {
      Int_t FileID;
      Int_t FirstRunID;
      Int_t LastRunID;
    };
    
    struct FADCMapData
    {
      Int_t CrateID;
      Int_t ModuleID;
      Int_t ChannelID;
    };
    
    KanaFADCMapManager();
    KanaFADCMapManager(const Int_t acc_run_ID, const Int_t pro_version,
		       const std::string detector_name);
    
  private:
    void InitKanaFADCMapManager();
    
    std::vector<Int_t> E14IDList;
    
  public:
    Int_t GetnChannels() const { return E14IDList.size(); }
    Int_t GetE14ID(const Int_t i_ch) const { return E14IDList.at(i_ch); }
    
  public:
    void SetTopDir(const std::string top_dir);
    
  private:
    std::string TopDirectory;
    
    void LoadListFile(const std::string path);
    std::vector<MapFileListData> MapFileList;
    
  public:
    void   LoadFADCMap(const Int_t run_ID);
    Bool_t MapFileReady() { return !NoFADCMapData; }
    
  private:
    Bool_t NoFADCMapData;
    std::map<Int_t,FADCMapData> FADCMap;
    
  public:
    Int_t GetCrateID(const Int_t e14_ID) const
    {
      std::map<Int_t,FADCMapData>::const_iterator itr = FADCMap.find(e14_ID);
      return (itr!=FADCMap.end() ? itr->second.CrateID : -1);
    }
    Int_t GetModuleID(const Int_t e14_ID) const
    {
      std::map<Int_t,FADCMapData>::const_iterator itr = FADCMap.find(e14_ID);
      return (itr!=FADCMap.end() ? itr->second.ModuleID : -1);
    }
    Int_t GetChannelID(const Int_t e14_ID) const
    {
      std::map<Int_t,FADCMapData>::const_iterator itr = FADCMap.find(e14_ID);
      return (itr!=FADCMap.end() ? itr->second.ChannelID : -1);
    }
    
  public:
    void DumpFADCMap(const Int_t n_ch);
    
  public:
    virtual ~KanaFADCMapManager();

    ClassDef(KanaFADCMapManager, 1);
  };
  
}

#endif
