/* -*- C++ -*- */

#ifndef E14ANA_KANACONVMANAGER_KANAFADCCRATEMANAGERBASE_H
#define E14ANA_KANACONVMANAGER_KANAFADCCRATEMANAGERBASE_H

#include <TObject.h>
#include <TTree.h>

#include "KanaLib/KanaFunctions.h"

namespace KanaLibrary
{
  
  class KanaFADCCrateManagerBase : public TObject
  {
  protected:
    enum {
      FADC_N_MODULES = 20
    };
    
  public:
    KanaFADCCrateManagerBase();
    
  private:
    void InitKanaFADCCrateManagerBase();
    
  protected:
    UInt_t  ScaledTrigBit;
    Int_t  *TimeStamp;
    
  public:
    virtual void SetBranchAddresses(TTree *tr);
    
  protected:
    void SetBranchAddressesBase(TTree *tr);
    
  public:
    inline UInt_t GetScaledTrigBit()
    {
      return ScaledTrigBit;
    }
    inline Int_t GetTimeStamp(const Int_t mod_ID)
    {
      return TimeStamp[mod_ID];
    }
    
    virtual Int_t GetnSamples()=0;
    virtual Int_t GetResolution()=0;
    
    virtual const Short_t* GetData(const Int_t mod_ID, const Int_t ch_ID)=0;
    
  public:
    virtual ~KanaFADCCrateManagerBase();

    ClassDef(KanaFADCCrateManagerBase, 1);
  };
  
}

#endif
