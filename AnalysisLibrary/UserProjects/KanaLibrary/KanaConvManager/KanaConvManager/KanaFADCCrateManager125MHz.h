/* -*- C++ -*- */

#ifndef E14ANA_KANACONVMANAGER_KANAFADCCRATEMANAGER125MHZ_H
#define E14ANA_KANACONVMANAGER_KANAFADCCRATEMANAGER125MHZ_H

#include <TTree.h>

#include "KanaLib/KanaFunctions.h"

#include "KanaFADCCrateManagerBase.h"

namespace KanaLibrary
{
  
  class KanaFADCCrateManager125MHz : public KanaFADCCrateManagerBase
  {
  private:
    enum {
      FADC_N_CHANNELS = 16,
      FADC_N_SAMPLES  = 64,
      FADC_RESOLUTION = 14
    };
    
  public:
    KanaFADCCrateManager125MHz();
    
    virtual Int_t GetnSamples();
    virtual Int_t GetResolution();
    
  private:
    void InitKanaFADCCrateManager125MHz();
    
    Short_t (*Data)         [FADC_N_CHANNELS][FADC_N_SAMPLES];
    Float_t (*Pedestal)     [FADC_N_CHANNELS];
    Short_t (*PeakHeight)   [FADC_N_CHANNELS];
    Short_t (*PeakTime)     [FADC_N_CHANNELS];
    Float_t (*IntegratedADC)[FADC_N_CHANNELS];
    
  public:
    virtual void SetBranchAddresses(TTree *tr);
    
  private:
    void SetBranchAddresses125MHz(TTree *tr);
    
  public:
    virtual const Short_t* GetData(const Int_t mod_ID, const Int_t ch_ID);
    
    Float_t GetPedestal     (const Int_t mod_ID, const Int_t ch_ID);
    Short_t GetPeakHeight   (const Int_t mod_ID, const Int_t ch_ID);
    Short_t GetPeakTime     (const Int_t mod_ID, const Int_t ch_ID);
    Float_t GetIntegratedADC(const Int_t mod_ID, const Int_t ch_ID);
    
    
  public:
    virtual ~KanaFADCCrateManager125MHz();

    ClassDef(KanaFADCCrateManager125MHz, 1);
  };
  
}

#endif
