/* -*- C++ -*- */

#ifndef E14ANA_KANACONVMANAGER_KANAFADCCRATEMANAGER500MHZ_H
#define E14ANA_KANACONVMANAGER_KANAFADCCRATEMANAGER500MHZ_H

#include <TTree.h>

#include "KanaLib/KanaFunctions.h"

#include "KanaFADCCrateManagerBase.h"

namespace KanaLibrary
{
  
  class KanaFADCCrateManager500MHz : public KanaFADCCrateManagerBase
  {
  private:
    enum {
      FADC_N_CHANNELS = 4,
      FADC_N_SAMPLES  = 256,
      MAX_N_HITS      = 20,
      FADC_RESOLUTION = 12
    };
    
  public:
    KanaFADCCrateManager500MHz();
    
    virtual Int_t GetnSamples();
    virtual Int_t GetResolution();
    
  private:
    void InitKanaFADCCrateManager500MHz();
    
    Short_t (*Data)       [FADC_N_CHANNELS][FADC_N_SAMPLES];
    Float_t (*Pedestal)   [FADC_N_CHANNELS];
    Float_t (*AllIntegral)[FADC_N_CHANNELS];
    Short_t (*nHit)       [FADC_N_CHANNELS];
    Float_t (*PeakHeight) [FADC_N_CHANNELS][MAX_N_HITS];
    Short_t (*PeakTime)   [FADC_N_CHANNELS][MAX_N_HITS];
    Float_t (*Integral)   [FADC_N_CHANNELS][MAX_N_HITS];
    
  public:
    virtual void SetBranchAddresses(TTree *tr);
    
  private:
    void SetBranchAddresses500MHz(TTree *tr);
    
  public:
    virtual const Short_t* GetData(const Int_t mod_ID, const Int_t ch_ID);
    
    Float_t GetPedestal   (const Int_t mod_ID, const Int_t ch_ID);
    Float_t GetAllIntegral(const Int_t mod_ID, const Int_t ch_ID);
    Short_t GetnHit       (const Int_t mod_ID, const Int_t ch_ID);
    
    Float_t GetPeakHeight (const Int_t mod_ID, const Int_t ch_ID, const Int_t i_hit);
    Short_t GetPeakTime   (const Int_t mod_ID, const Int_t ch_ID, const Int_t i_hit);
    Float_t GetIntegral   (const Int_t mod_ID, const Int_t ch_ID, const Int_t i_hit);
    
    
  public:
    virtual ~KanaFADCCrateManager500MHz();

    ClassDef(KanaFADCCrateManager500MHz, 1);
  };
  
}

#endif
