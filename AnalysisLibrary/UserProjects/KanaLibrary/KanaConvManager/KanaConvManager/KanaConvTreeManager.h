/* -*- C++ -*- */

#ifndef E14ANA_KANACONVMANAGER_KANACONVTREEMANAGER_H
#define E14ANA_KANACONVMANAGER_KANACONVTREEMANAGER_H

#include <sstream>
#include <vector>
#include <map>

#include <TObject.h>
#include <TFile.h>
#include <TTree.h>

#include "KanaFADCCrateManagerBase.h"
#include "KanaFADCCrateManager125MHz.h"
#include "KanaFADCCrateManager500MHz.h"

namespace KanaLibrary
{
  
  class KanaConvTreeManager : public TObject
  {
  public:
    KanaConvTreeManager();
    
  private:
    void InitKanaConvTreeManager();
    
  public:
    Bool_t OpenConvFile(std::string path);
    
  private:
    TFile *RootFile;
    
  public:
    Bool_t ActivateCrateTree(const Int_t crate_ID, const Bool_t is_500MHz);
    
  private:
    std::vector<Int_t> CrateIDList;
    std::map<Int_t,TTree*> CrateTreeList;
    std::map<Int_t,KanaFADCCrateManagerBase*> CrateManagerList;
    
  public:
    Bool_t GetEntry(const Long64_t event_ID);
    KanaFADCCrateManagerBase* GetCrateManager(const Int_t crate_ID);
    
    
  public:
    ~KanaConvTreeManager();

    ClassDef(KanaConvTreeManager, 1);
  };
  
}

#endif
