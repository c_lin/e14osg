#include "KanaLib/KanaTRIGGERTAGManager.h"

#include <string>

#include <TTree.h>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaTRIGGERTAGManager)

namespace KanaLibrary
{
  
  KanaTRIGGERTAGManager::KanaTRIGGERTAGManager()
    : AccRunID(-1), ProVersion(-1)
  {
  }

  KanaTRIGGERTAGManager::KanaTRIGGERTAGManager(const Int_t acc_run_ID, const Int_t pro_version)
    : AccRunID(acc_run_ID),
      ProVersion(pro_version)
  {
  }

  KanaTRIGGERTAGManager::~KanaTRIGGERTAGManager()
  {
  }

  void KanaTRIGGERTAGManager::SetBranchAddresses(TTree *tr)
  {
    const std::string prefix = "TRIGGERTAG";
    {
      const std::string name = prefix + "Number";
      KanaFunctions::ActivateBranch(tr, name.c_str(), &Number);
    }
    {
      const std::string name = prefix + "ModID";
      KanaFunctions::ActivateBranch(tr, name.c_str(), ModID);
    }
    {
      const std::string name = prefix + "IntegratedADC";
      KanaFunctions::ActivateBranch(tr, name.c_str(), IntegratedADC);
    }
  }

  Bool_t KanaTRIGGERTAGManager::IsClockEvent()
  {
    return (IntegratedADC[CLOCK] >= Threshold) ? kTRUE : kFALSE;
  }

  Bool_t KanaTRIGGERTAGManager::IsLaserEvent()
  {
    return (IntegratedADC[LASER] >= Threshold) ? kTRUE : kFALSE;
  }

  Bool_t KanaTRIGGERTAGManager::IsLEDEvent()
  {
    return (IntegratedADC[LED] >= Threshold) ? kTRUE : kFALSE;
  }
  
}
