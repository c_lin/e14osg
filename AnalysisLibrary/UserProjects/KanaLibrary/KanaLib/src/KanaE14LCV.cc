#include "KanaLib/KanaE14LCV.h"

#include <iostream>
#include <iomanip>
#include <sstream>

#include <TMath.h>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaE14LCV)

namespace KanaLibrary
{
  
  KanaE14LCV::KanaE14LCV()
    : KanaE14Detector125MHz(),
      
      FrontEdgeZ(6095.7), // [mm]
      RearEdgeZ(6670.7),  // [mm]
      Width(142.),        // [mm]
      Thickness(3.),      // [mm]
      
      MIPPeakEnergy(0.4663)
  {
    InitKanaE14LCV();
  }

  KanaE14LCV::KanaE14LCV(const Int_t acc_run_ID, const Int_t pro_version)
    : KanaE14Detector125MHz(acc_run_ID, pro_version, "LCV"),
      
      FrontEdgeZ(6095.7), // [mm]
      RearEdgeZ(6670.7),  // [mm]
      Width(142.),        // [mm]
      Thickness(3.),      // [mm]
      
      MIPPeakEnergy(0.4663)
  {
    InitKanaE14LCV();
    
    SetModulePositionMap();
    
    // allocate memories
    CosmicModID                  = new Int_t   [nChannels];
    CosmicPeak                   = new Short_t [nChannels];
    CosmicCorrectedPeak          = new Float_t [nChannels];
    CosmicIntegratedADC          = new Float_t [nChannels];
    CosmicCorrectedIntegratedADC = new Float_t [nChannels];
    CosmicInitialTime            = new Float_t [nChannels];
    CosmicHitX                   = new Float_t [nChannels];
    CosmicHitY                   = new Float_t [nChannels];
    CosmicHitZ                   = new Float_t [nChannels];
  }

  void KanaE14LCV::InitKanaE14LCV()
  {
    CosmicNumber = 0;

    CosmicModID                  = NULL;
    CosmicPeak                   = NULL;
    CosmicCorrectedPeak          = NULL;
    CosmicIntegratedADC          = NULL;
    CosmicCorrectedIntegratedADC = NULL;
    CosmicInitialTime            = NULL;
    CosmicHitX                   = NULL;
    CosmicHitY                   = NULL;
    CosmicHitZ                   = NULL;
  }


  void KanaE14LCV::SetModulePositionMap()
  {
    for (Int_t i_ch=0; i_ch < nChannels; ++i_ch) {
      
      const Int_t e14_ID = E14IDList.at(i_ch);
      
      if (e14_ID==0) {
        PositionMap[e14_ID]  = std::pair<Double_t,Double_t>(76., 0);
        ModuleRangeX[e14_ID]
  	= std::pair<Double_t,Double_t>(76.-Thickness*0.5, 76.+Thickness*0.5);
        ModuleRangeY[e14_ID] = std::pair<Double_t,Double_t>(-76.95, 73.95);
      } else if (e14_ID==1) {
        PositionMap[e14_ID]  = std::pair<Double_t,Double_t>(0, 76.);
        ModuleRangeX[e14_ID] = std::pair<Double_t,Double_t>(-76.7, 76.7);
        ModuleRangeY[e14_ID]
  	= std::pair<Double_t,Double_t>(76.-Thickness*0.5, 76.+Thickness*0.5);
      } else if (e14_ID==2) {
        PositionMap[e14_ID]  = std::pair<Double_t,Double_t>(-76., 0);
        ModuleRangeX[e14_ID]
  	= std::pair<Double_t,Double_t>(-76.-Thickness*0.5, -76.+Thickness*0.5);
        ModuleRangeY[e14_ID] = std::pair<Double_t,Double_t>(-76.95, 73.95);
      } else if (e14_ID==3) {
        PositionMap[e14_ID]  = std::pair<Double_t,Double_t>(0, -76.);
        ModuleRangeX[e14_ID] = std::pair<Double_t,Double_t>(-73.7, 73.7);
        ModuleRangeY[e14_ID]
  	= std::pair<Double_t,Double_t>(-76.-Thickness*0.5, -76.+Thickness*0.5);
      }
    }
  }


  void KanaE14LCV::SetBranchAddresses(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branch addresses: " << Name << std::endl;
    
    // set branch addresses of KanaE14DetectorBase
    SetBranchAddressesBase(tr, b_list);

    // set branch addresses of KanaE14Detector125MHz
    SetBranchAddresses125MHz(tr, b_list);

    // set branch addresses of KanaE14LCV
    SetBranchAddressesLCV(tr, b_list);
  }

  void KanaE14LCV::SetBranchAddressesLCV(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "CosmicNumber") != end) {
      const std::string bname = Name + "CosmicNumber";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicNumber);
      std::cout << "     -- CosmicNumber: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModID") != end) {
      const std::string bname = Name + "CosmicModID";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModID);
      std::cout << "     -- CosmicModID: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPeak") != end) {
      const std::string bname = Name + "CosmicPeak";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicPeak);
      std::cout << "     -- CosmicPeak: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicCorrectedPeak") != end) {
      const std::string bname = Name + "CosmicCorrectedPeak";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicCorrectedPeak);
      std::cout << "     -- CosmicCorrectedPeak: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicIntegratedADC") != end) {
      const std::string bname = Name + "CosmicIntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicIntegratedADC);
      std::cout << "     -- CosmicIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicCorrectedIntegratedADC") != end) {
      const std::string bname = Name + "CosmicCorrectedIntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicCorrectedIntegratedADC);
      std::cout << "     -- CosmicCorrectedIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicInitialTime") != end) {
      const std::string bname = Name + "CosmicInitialTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicInitialTime);
      std::cout << "     -- CosmicInitialTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicHitX") != end) {
      const std::string bname = Name + "CosmicHitX";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicHitX);
      std::cout << "     -- CosmicHitX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicHitY") != end) {
      const std::string bname = Name + "CosmicHitY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicHitY);
      std::cout << "     -- CosmicHitY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicHitZ") != end) {
      const std::string bname = Name + "CosmicHitZ";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicHitZ);
      std::cout << "     -- CosmicHitZ: enabled" << std::endl;
    }
  }


  void KanaE14LCV::AddBranches(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branches: " << Name << std::endl;

    // add branches of KanaE14DetectorBase
    AddBranchesBase(tr, b_list);

    // add branches of KanaE14Detector125MHz
    AddBranches125MHz(tr, b_list);
    
    // add branches of KanaE14LCV
    AddBranchesLCV(tr, b_list);
  }


  void KanaE14LCV::AddBranchesLCV(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "CosmicNumber") != end) {
      const std::string bname = Name + "CosmicNumber";
      std::stringstream leaflist;
      leaflist << bname << "/I";
      tr->Branch(bname.c_str(), &CosmicNumber, leaflist.str().c_str());
      std::cout << "     -- CosmicNumber: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModID") != end) {
      const std::string bname = Name + "CosmicModID";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/I";
      tr->Branch(bname.c_str(), CosmicModID, leaflist.str().c_str());
      std::cout << "     -- CosmicModID: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPeak") != end) {
      const std::string bname = Name + "CosmicPeak";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/S";
      tr->Branch(bname.c_str(), CosmicPeak, leaflist.str().c_str());
      std::cout << "     -- CosmicPeak: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicCorrectedPeak") != end) {
      const std::string bname = Name + "CosmicCorrectedPeak";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicCorrectedPeak, leaflist.str().c_str());
      std::cout << "     -- CosmicCorrectedPeak: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicIntegratedADC") != end) {
      const std::string bname = Name + "CosmicIntegratedADC";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicIntegratedADC, leaflist.str().c_str());
      std::cout << "     -- CosmicIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicCorrectedIntegratedADC") != end) {
      const std::string bname = Name + "CosmicCorrectedIntegratedADC";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicCorrectedIntegratedADC, leaflist.str().c_str());
      std::cout << "     -- CosmicCorrectedIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicInitialTime") != end) {
      const std::string bname = Name + "CosmicInitialTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicInitialTime, leaflist.str().c_str());
      std::cout << "     -- CosmicInitialTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicHitX") != end) {
      const std::string bname = Name + "CosmicHitX";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicHitX, leaflist.str().c_str());
      std::cout << "     -- CosmicHitX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicHitY") != end) {
      const std::string bname = Name + "CosmicHitY";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicHitY, leaflist.str().c_str());
      std::cout << "     -- CosmicHitY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicHitZ") != end) {
      const std::string bname = Name + "CosmicHitZ";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicHitZ, leaflist.str().c_str());
      std::cout << "     -- CosmicHitZ: enabled" << std::endl;
    }
  }


  void KanaE14LCV::GetCosmicHitModules(const Double_t intercept_xy, const Double_t slope_xy,
  				     const Double_t intercept_zy, const Double_t slope_zy)
  {
    // const Double_t half_width = Width*0.5;
    
    CosmicNumber = 0;
    for (Int_t num=0; num < Number; ++num) {
      
      const Int_t e14_ID = ModID[num];
      
      if ((e14_ID==0) || (e14_ID==2)) {
        const Double_t cosmic_hit_x = PositionMap[e14_ID].first;
        const Double_t cosmic_hit_y = slope_xy * cosmic_hit_x + intercept_xy;
        const Double_t cosmic_hit_z = (cosmic_hit_y - intercept_zy) / slope_zy;
        
        if ( (ModuleRangeY[e14_ID].first < cosmic_hit_y)
  	   && (cosmic_hit_y < ModuleRangeY[e14_ID].second) 
  	   && (FrontEdgeZ < cosmic_hit_z) && (cosmic_hit_z < RearEdgeZ) ) {
  	
  	const Double_t cosine
  	  = 1.
  	  / TMath::Sqrt( 1 + 1./(slope_xy*slope_xy) + 1./(slope_zy*slope_zy) )
  	  / TMath::Abs(slope_xy);
  	
  	const Double_t correction
  	  = cosine * MIPPeakEnergy / (MIPPeakEnergy + 0.03751*(1-cosine));
  	
  	CosmicModID[CosmicNumber] = e14_ID;
  	
  	CosmicPeak[CosmicNumber]                   = Peak[num];
  	CosmicCorrectedPeak[CosmicNumber]          = Peak[num] * correction;
  	CosmicIntegratedADC[CosmicNumber]          = IntegratedADC[num];
  	CosmicCorrectedIntegratedADC[CosmicNumber] = IntegratedADC[num] * correction;
  	
  	CosmicInitialTime[CosmicNumber] = InitialTime[num];
  	
  	CosmicHitX[CosmicNumber] = cosmic_hit_x;
  	CosmicHitY[CosmicNumber] = cosmic_hit_y;
  	CosmicHitZ[CosmicNumber] = cosmic_hit_z;
  	
  	++CosmicNumber;
        }
      } else {
        const Double_t cosmic_hit_y = PositionMap[e14_ID].second;
        const Double_t cosmic_hit_x = (cosmic_hit_y - intercept_xy) / slope_xy;
        const Double_t cosmic_hit_z = (cosmic_hit_y - intercept_zy) / slope_zy;

        if ( (ModuleRangeX[e14_ID].first < cosmic_hit_x)
  	   && (cosmic_hit_x < ModuleRangeX[e14_ID].second)
  	   && (FrontEdgeZ < cosmic_hit_z) && (cosmic_hit_z < RearEdgeZ) ) {
  	
  	const Double_t cosine
  	  = 1. / TMath::Sqrt( 1 + 1./(slope_xy*slope_xy) + 1./(slope_zy*slope_zy) );
  	
  	const Double_t correction
  	  = cosine * MIPPeakEnergy / (MIPPeakEnergy + 0.03751*(1-cosine));
  	
  	CosmicModID[CosmicNumber] = e14_ID;
  	
  	CosmicPeak[CosmicNumber]                   = Peak[num];
  	CosmicCorrectedPeak[CosmicNumber]	   = Peak[num] * correction;
  	CosmicIntegratedADC[CosmicNumber]          = IntegratedADC[num];
  	CosmicCorrectedIntegratedADC[CosmicNumber] = IntegratedADC[num] * correction;
  	
  	CosmicInitialTime[CosmicNumber] = InitialTime[num];
  	
  	CosmicHitX[CosmicNumber] = cosmic_hit_x;
  	CosmicHitY[CosmicNumber] = cosmic_hit_y;
  	CosmicHitZ[CosmicNumber] = cosmic_hit_z;
  	
  	++CosmicNumber;
        }
      }
    }
  }


  KanaE14LCV::~KanaE14LCV()
  {
    if (CosmicModID                  != NULL) delete [] CosmicModID;
    if (CosmicPeak                   != NULL) delete [] CosmicPeak;
    if (CosmicCorrectedPeak          != NULL) delete [] CosmicCorrectedPeak;
    if (CosmicIntegratedADC          != NULL) delete [] CosmicIntegratedADC;
    if (CosmicCorrectedIntegratedADC != NULL) delete [] CosmicCorrectedIntegratedADC;
    if (CosmicInitialTime            != NULL) delete [] CosmicInitialTime;

    if (CosmicHitX != NULL) delete [] CosmicHitX;
    if (CosmicHitY != NULL) delete [] CosmicHitY;
    if (CosmicHitZ != NULL) delete [] CosmicHitZ;
  }
  
}
