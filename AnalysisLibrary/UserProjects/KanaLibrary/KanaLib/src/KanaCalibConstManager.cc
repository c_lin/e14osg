#include "KanaLib/KanaCalibConstManager.h"

#include <iostream>
#include <sstream>

#include <TChain.h>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaCalibConstManager)

namespace KanaLibrary
{
  
  KanaCalibConstManager::KanaCalibConstManager()
    : AccRunID(-1),
      ProVersion(-1),
      DetectorName("")
  {
    InitKanaCalibConstManager();
  }
  
  KanaCalibConstManager::KanaCalibConstManager(const Int_t acc_run_ID, const Int_t pro_version,
					       const std::string detector_name)
    : AccRunID(acc_run_ID),
      ProVersion(pro_version),
      DetectorName(detector_name)
  {
    InitKanaCalibConstManager();
    
    FileReader = new KanaFileReader;
    
    /* get E14ID list */
    E14IDList = KanaFunctions::GetE14IDList(AccRunID, ProVersion, DetectorName);
  }
  
  
  void KanaCalibConstManager::InitKanaCalibConstManager()
  {
    FileReader = NULL;
  }
  
  
  void KanaCalibConstManager::SetCalibDir(const std::string top_dir)
  {
    /* set parameters */
    TopDirectory = top_dir;
    
    /* load calib/t0 list file */
    CalibList = LoadListFile(TopDirectory+"/calib_"+DetectorName+"_list.txt");
    T0List    = LoadListFile(TopDirectory+"/t0_"   +DetectorName+"_list.txt");
  }
  
  std::vector<KanaCalibConstManager::CalibFileListData> KanaCalibConstManager::LoadListFile(const std::string path)
  {
    FileReader->SetFile(path);
    
    std::vector<CalibFileListData> list_temp;
    for (Int_t i_line=0, n_lines=FileReader->GetnLines(); i_line < n_lines; ++i_line) {
      FileReader->ReadLine(i_line, "ssv");
      const Int_t first_run_ID = std::atoi(FileReader->GetWord(0).c_str());
      const Int_t last_run_ID  = std::atoi(FileReader->GetWord(1).c_str());
      const Int_t file_ID      = std::atoi(FileReader->GetWord(2).c_str());
      CalibFileListData data_temp = { file_ID, first_run_ID, last_run_ID };
      list_temp.push_back(data_temp);
    }
    
    return list_temp;
  }
  
  
  Int_t KanaCalibConstManager::GetCalibFileID(const Int_t run_ID)
  {
    return GetFileID(CalibList, run_ID);
  }
  
  
  Int_t KanaCalibConstManager::GetT0FileID(const Int_t run_ID)
  {
    return GetFileID(T0List, run_ID);
  }
  
  
  Int_t KanaCalibConstManager::
  GetFileID(const std::vector<CalibFileListData> &list, const Int_t run_ID)
  {
    Int_t file_ID = -1;
    for (std::vector<CalibFileListData>::const_iterator
	   itr=list.begin(), end=list.end(); itr!=end; ++itr) {
      if (itr->FirstRunID<=run_ID && run_ID<=itr->LastRunID) { file_ID = itr->FileID; break; }
    }
    return file_ID;
  }
  
  
  void KanaCalibConstManager::LoadCalibConstFromCalibFile(const Int_t run_ID)
  {
    /* get calib file IDs */
    // calib
    Bool_t calib_is_found = kFALSE;
    Int_t  calib_file_ID  = -1;
    for (std::vector<CalibFileListData>::iterator itr=CalibList.begin(), end=CalibList.end();
	 itr!=end; ++itr) {
      if (itr->FirstRunID<=run_ID && run_ID<=itr->LastRunID) {
	calib_file_ID  = itr->FileID;
	calib_is_found = kTRUE;
	break;
      }
    }
    
    // t0
    Bool_t t0_is_found = kFALSE;
    Int_t  t0_file_ID  = -1;
    for (std::vector<CalibFileListData>::iterator itr=T0List.begin(), end=T0List.end();
	 itr!=end; ++itr) {
      if (itr->FirstRunID<=run_ID && run_ID<=itr->LastRunID) {
	t0_file_ID  = itr->FileID;
	t0_is_found = kTRUE;
	break;
      }
    }
    
    
    /* read calib/t0 file */
    CalibConst.clear();
    
    // calib
    if (!calib_is_found) {
      NoCalibData = kTRUE;
    } else {
      NoCalibData = kFALSE;
      
      std::stringstream path;
      path << TopDirectory << "/calib/calib_" << DetectorName << "_" << calib_file_ID << ".txt";
      FileReader->SetFile(path.str());
      for (Int_t i_line=1, n_lines=FileReader->GetnLines(); i_line < n_lines; ++i_line) {
	FileReader->ReadLine(i_line, "ssv");
	CalibConstData &data_temp
	  = CalibConst[ std::atoi(FileReader->GetWord(0).c_str()) ];
	data_temp.GainMean  = std::atof(FileReader->GetWord(1).c_str());
	data_temp.GainSigma = std::atof(FileReader->GetWord(2).c_str());
	data_temp.MeVCoeff  = std::atof(FileReader->GetWord(3).c_str());
      }
    }
    
    // t0
    if (!t0_is_found) {
      NoT0Data = kTRUE;
    } else {
      NoT0Data = kFALSE;
      
      std::stringstream path;
      path << TopDirectory << "/t0/t0_" << DetectorName << "_" << t0_file_ID << ".txt";
      FileReader->SetFile(path.str());
      for (Int_t i_line=1, n_lines=FileReader->GetnLines(); i_line < n_lines; ++i_line) {
	FileReader->ReadLine(i_line, "ssv");
	CalibConstData &data_temp
	  = CalibConst[ std::atoi(FileReader->GetWord(0).c_str()) ];
	data_temp.T0Mean  = std::atof(FileReader->GetWord(1).c_str());
	data_temp.T0Sigma = std::atof(FileReader->GetWord(2).c_str());
      }
    }
  }
  
  
  void KanaCalibConstManager::LoadCalibConstFromDst(std::string path)
  {
    TChain calib("calib");
    calib.Add(path.c_str());
    if (calib.GetEntries()==0) {
      NoCalibData = kTRUE;
      NoT0Data    = kTRUE;
      return;
    }
    
    /* initialize */
    Float_t GainMean[4096];
    Float_t GainSigma[4096];
    Float_t MeVCoeff[4096];
    Float_t T0Mean[4096];
    Float_t T0Sigma[4096];
    
    /* get tree data */
    calib.SetMakeClass(1);
    calib.SetBranchStatus("*", 0);
    calib.SetBranchStatus(std::string(DetectorName+".*").c_str(), 1);
    {
      std::string bname = DetectorName + ".GainMean[4096]";
      KanaFunctions::ActivateBranch(&calib, bname.c_str(), GainMean);
    }
    {
      std::string bname = DetectorName + ".GainSigma[4096]";
      KanaFunctions::ActivateBranch(&calib, bname.c_str(), GainSigma);
    }
    {
      std::string bname = DetectorName + ".MeVCoeff[4096]";
      KanaFunctions::ActivateBranch(&calib, bname.c_str(), MeVCoeff);
    }
    {
      std::string bname = DetectorName + ".T0Mean[4096]";
      KanaFunctions::ActivateBranch(&calib, bname.c_str(), T0Mean);
    }
    {
      std::string bname = DetectorName + ".T0Sigma[4096]";
      KanaFunctions::ActivateBranch(&calib, bname.c_str(), T0Sigma);
    }
    calib.GetEntry(0);
    
    CalibConst.clear();
    for (std::vector<Int_t>::iterator itr = E14IDList.begin(); itr != E14IDList.end(); ++itr) {
      const Int_t e14_ID = (*itr);
      CalibConstData &data_temp = CalibConst[e14_ID];
      data_temp.GainMean  = GainMean[e14_ID];
      data_temp.GainSigma = GainSigma[e14_ID];
      data_temp.MeVCoeff  = MeVCoeff[e14_ID];
      data_temp.T0Mean    = T0Mean[e14_ID];
      data_temp.T0Sigma   = T0Sigma[e14_ID];
    }
    
    NoCalibData = kFALSE;
    NoT0Data    = kFALSE;
  }
  
  
  void KanaCalibConstManager::DumpCalibConst(const Int_t n_ch)
  {
    std::cout << "   - Calibration Constants for " << DetectorName << std::endl;
    std::cout << "     -- top dir    : " << TopDirectory << std::endl;
    std::cout << "     -- acc. run ID: " << AccRunID     << std::endl;
    std::cout << "     -- pro version: " << ProVersion   << std::endl;
    
    std::cout << "     -- energy calib const" << std::endl;
    Int_t n_dump = 0;
    for (std::vector<Int_t>::iterator itr = E14IDList.begin(); itr != E14IDList.end(); ++itr) {
      const Int_t e14_ID = (*itr);
      const CalibConstData &calib_const_data = CalibConst[e14_ID];
      std::cout << "     CH " << e14_ID << ": " << std::endl;
      std::cout << "       GainMean : " << calib_const_data.GainMean  << std::endl;
      std::cout << "       GainSigma: " << calib_const_data.GainSigma << std::endl;
      std::cout << "       MeVCoeff : " << calib_const_data.MeVCoeff  << std::endl;
      std::cout << "       T0Mean   : " << calib_const_data.T0Mean    << std::endl;
      std::cout << "       T0Sigma  : " << calib_const_data.T0Sigma   << std::endl;
      ++n_dump;
      
      if (n_dump>=n_ch) break;
    }
  }
  
  
  KanaCalibConstManager::~KanaCalibConstManager()
  {
    if (FileReader !=NULL) delete FileReader;
  }
  
}
