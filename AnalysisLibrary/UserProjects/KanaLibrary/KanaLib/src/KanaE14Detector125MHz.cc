#include "KanaLib/KanaE14Detector125MHz.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <algorithm>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaE14Detector125MHz)

namespace KanaLibrary
{

  KanaE14Detector125MHz::KanaE14Detector125MHz()
    : KanaE14DetectorBase()
  {
    InitKanaE14Detector125MHz();
  }

  KanaE14Detector125MHz::KanaE14Detector125MHz(const Int_t acc_run_ID, const Int_t pro_version,
  					     const std::string name)
    : KanaE14DetectorBase(acc_run_ID, pro_version, name)
  {
    InitKanaE14Detector125MHz();

    // allocate memories
    Peak          = new Short_t [nChannels];
    PeakTime      = new Short_t [nChannels];
    IntegratedADC = new Float_t [nChannels];
    InitialTime   = new Float_t [nChannels];
    PTime         = new Float_t [nChannels];
    Ene           = new Float_t [nChannels];
    Time          = new Float_t [nChannels];
  }

  void KanaE14Detector125MHz::InitKanaE14Detector125MHz()
  {
    FADCnSamples       = 64;
    FADCSampleInterval = 8e-9;
    
    Peak          = NULL;
    PeakTime      = NULL;
    IntegratedADC = NULL;
    InitialTime   = NULL;
    PTime         = NULL;
    Ene           = NULL;
    Time          = NULL;
  }

  void KanaE14Detector125MHz::SetBranchAddresses(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branch addresses: " << Name << std::endl;
    
    // set branch addresses of KanaE14DetectorBase
    SetBranchAddressesBase(tr, b_list);

    // set branch addresses of KanaE14Detector125MHz
    SetBranchAddresses125MHz(tr, b_list);
  }

  void KanaE14Detector125MHz::SetBranchAddresses125MHz(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "Peak") != end) {
      const std::string bname = Name + "Peak";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Peak);
      std::cout << "     -- Peak: enabled" << std::endl;
    }
    if (std::find(begin, end, "PeakTime") != end) {
      const std::string bname = Name + "PeakTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PeakTime);
      std::cout << "     -- PeakTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "IntegratedADC") != end) {
      const std::string bname = Name + "IntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), IntegratedADC);
      std::cout << "     -- IntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "InitialTime") != end) {
      const std::string bname = Name + "InitialTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), InitialTime);
      std::cout << "     -- InitialTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "PTime") != end) {
      const std::string bname = Name + "PTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PTime);
      std::cout << "     -- PTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "Ene") != end) {
      const std::string bname = Name + "Ene";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Ene);
      std::cout << "     -- Ene: enabled" << std::endl;
    }
    if (std::find(begin, end, "Time") != end) {
      const std::string bname = Name + "Time";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Time);
      std::cout << "     -- Time: enabled" << std::endl;
    }

  }


  void KanaE14Detector125MHz::AddBranches(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branches: " << Name << std::endl;

    // add branches of KanaE14DetectorBase
    AddBranchesBase(tr, b_list);

    // add branches of KanaE14Detector125MHz
    AddBranches125MHz(tr, b_list);
  }

  void KanaE14Detector125MHz::AddBranches125MHz(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "Peak") != end) {
      const std::string bname = Name + "Peak";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/S";
      tr->Branch(bname.c_str(), Peak, leaflist.str().c_str());
      std::cout << "     -- Peak: enabled" << std::endl;
    }
    if (std::find(begin, end, "PeakTime") != end) {
      const std::string bname = Name + "PeakTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/S";
      tr->Branch(bname.c_str(), PeakTime, leaflist.str().c_str());
      std::cout << "     -- PeakTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "IntegratedADC") != end) {
      const std::string bname = Name + "IntegratedADC";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/F";
      tr->Branch(bname.c_str(), IntegratedADC, leaflist.str().c_str());
      std::cout << "     -- IntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "InitialTime") != end) {
      const std::string bname = Name + "InitialTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/F";
      tr->Branch(bname.c_str(), InitialTime, leaflist.str().c_str());
      std::cout << "     -- InitialTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "PTime") != end) {
      const std::string bname = Name + "PTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/F";
      tr->Branch(bname.c_str(), PTime, leaflist.str().c_str());
      std::cout << "     -- PTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "Ene") != end) {
      const std::string bname = Name + "Ene";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/F";
      tr->Branch(bname.c_str(), Ene, leaflist.str().c_str());
      std::cout << "     -- Ene: enabled" << std::endl;
    }
    if (std::find(begin, end, "Time") != end) {
      const std::string bname = Name + "Time";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/F";
      tr->Branch(bname.c_str(), Time, leaflist.str().c_str());
      std::cout << "     -- Time: enabled" << std::endl;
    }
  }


  void KanaE14Detector125MHz::ZeroSuppression(const Double_t th)
  {
    std::vector<Int_t> old_num_list(0);

    for (Int_t num=0; num < Number; ++num) {
      if (Ene[num]<th) continue;
      old_num_list.push_back(num);
    }
    
    Number=0;
    for (Int_t i=0, n=old_num_list.size(); i < n; ++i) {
      const Int_t old_num = old_num_list.at(i);
      ModID[Number]         = ModID[old_num];
      Peak[Number]          = Peak[old_num];
      PeakTime[Number]      = PeakTime[old_num];
      IntegratedADC[Number] = IntegratedADC[old_num];
      InitialTime[Number]   = InitialTime[old_num];
      PTime[Number]         = PTime[old_num];
      Ene[Number]           = Ene[old_num];
      Time[Number]          = Time[old_num];
      ++Number;
    }
  }


  KanaE14Detector125MHz::~KanaE14Detector125MHz()
  {
    if (Peak          != NULL) delete [] Peak;
    if (PeakTime      != NULL) delete [] PeakTime;
    if (IntegratedADC != NULL) delete [] IntegratedADC;
    if (InitialTime   != NULL) delete [] InitialTime;
    if (PTime         != NULL) delete [] PTime;
    if (Ene           != NULL) delete [] Ene;
    if (Time          != NULL) delete [] Time;
  }
  
}
