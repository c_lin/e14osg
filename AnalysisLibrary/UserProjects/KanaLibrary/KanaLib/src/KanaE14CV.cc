#include "KanaLib/KanaE14CV.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaE14CV)

namespace KanaLibrary
{

  KanaE14CV::KanaE14CV()
    : KanaE14Detector125MHz()
  {
    InitKanaE14CV();
  }

  KanaE14CV::KanaE14CV(const Int_t acc_run_ID, const Int_t pro_version)
    : KanaE14Detector125MHz(acc_run_ID, pro_version, "CV")
  {
    InitKanaE14CV();
    
    LoadPositionMap();
    
    // allocate memories
    ModulePeak          = new Short_t [nModules][2];
    ModuleIntegratedADC = new Float_t [nModules][2];
    ModuleInitialTime   = new Float_t [nModules][2];
    
    ModuleSumIntegratedADC      = new Float_t [nModules];
    ModuleInitialTimeAverage    = new Float_t [nModules];
    ModuleInitialTimeDifference = new Float_t [nModules];
  }

  void KanaE14CV::InitKanaE14CV()
  {
    ModulePeak          = NULL;
    ModuleIntegratedADC = NULL;
    ModuleInitialTime   = NULL;
    
    ModuleSumIntegratedADC      = NULL;
    ModuleInitialTimeAverage    = NULL;
    ModuleInitialTimeDifference = NULL;
  }


  void KanaE14CV::LoadPositionMap()
  {
    /* open input CSV file */
    const std::string input
      = std::string(E14AS_LOCAL_PATH) + "/data/E14Detector/CV/CV_map.csv";
    
    std::ifstream fin(input.c_str());
    if (!fin) {
      DumpError("KanaE14CV::LoadPositionMap", "cannot find '" + input + "'");
      return;
    }
    

    /* load position inforamtion */
    // remove the first line
    std::string line;
    std::getline(fin, line);

    std::map<Int_t,KanaCVPositionData> pos_map_temp;
    while (!fin.eof()) {
      
      std::getline(fin, line);
      if (KanaFunctions::NoCharacter(line)) {
        if (fin.eof()) break;
        else           continue;
      }
      
      std::vector<std::string> vars;
      if (KanaFunctions::ReadCSVLine(line, vars)!=14) {
        DumpError("KanaE14CV::LoadPositionMap",
  		"invalid number of data in a line in '" + input + "'");
        return;
      }
      
      const Int_t module_ID = std::atoi(vars.at(0).c_str());
      struct KanaCVPositionData pos_data = {
        std::atoi(vars.at(1).c_str()),  // ShortSideE14ID
        std::atoi(vars.at(2).c_str()),  // LongSideE14ID
        std::atof(vars.at(3).c_str()),  // MinX
        std::atof(vars.at(4).c_str()),  // MaxX
        std::atof(vars.at(5).c_str()),  // MinY
        std::atof(vars.at(6).c_str()),  // MaxY
        std::atof(vars.at(7).c_str()),  // MinZ
        std::atof(vars.at(8).c_str()),  // MaxZ
        std::atof(vars.at(9).c_str()),  // FiberCenterX
        std::atof(vars.at(10).c_str()), // FiberCenterY
        std::atof(vars.at(11).c_str()), // FiberCenterZ
        std::atof(vars.at(12).c_str()), // FiberLength
        std::atoi(vars.at(13).c_str())  // Quadrant
      };
      
      pos_map_temp[module_ID] = pos_data;
    }
    
    PositionMap = pos_map_temp;
  }


  void KanaE14CV::SetBranchAddresses(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branch addresses: " << Name << std::endl;
    
    // set branch addresses of KanaE14DetectorBase
    SetBranchAddressesBase(tr, b_list);

    // set branch addresses of KanaE14Detector125MHz
    SetBranchAddresses125MHz(tr, b_list);

    // set branch addresses of KanaE14CV
    SetBranchAddressesCV(tr, b_list);
  }

  void KanaE14CV::SetBranchAddressesCV(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "ModulePeak") != end) {
      const std::string bname = Name + "ModulePeak";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModulePeak);
      std::cout << "     -- ModulePeak: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleIntegratedADC") != end) {
      const std::string bname = Name + "ModuleIntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleIntegratedADC);
      std::cout << "     -- ModuleIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTime") != end) {
      const std::string bname = Name + "ModuleInitialTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleInitialTime);
      std::cout << "     -- ModuleInitialTime: enabled" << std::endl;
    }

    if (std::find(begin, end, "ModuleSumIntegratedADC") != end) {
      const std::string bname = Name + "ModuleSumIntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleSumIntegratedADC);
      std::cout << "     -- ModuleSumIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTimeAverage") != end) {
      const std::string bname = Name + "ModuleInitialTimeAverage";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleInitialTimeAverage);
      std::cout << "     -- ModuleInitialTimeAverage: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTimeDifference") != end) {
      const std::string bname = Name + "ModuleInitialTimeDifference";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleInitialTimeDifference);
      std::cout << "     -- ModuleInitialTimeDifference: enabled" << std::endl;
    }
  }


  void KanaE14CV::AddBranches(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branches: " << Name << std::endl;

    // add branches of KanaE14DetectorBase
    AddBranchesBase(tr, b_list);

    // add branches of KanaE14Detector125MHz
    AddBranches125MHz(tr, b_list);
    
    // add branches of KanaE14CV
    AddBranchesCV(tr, b_list);
  }


  void KanaE14CV::AddBranchesCV(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "ModulePeak") != end) {
      const std::string bname = Name + "ModulePeak";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber][2]/S";
      tr->Branch(bname.c_str(), ModulePeak, leaflist.str().c_str());
      std::cout << "     -- ModulePeak: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleIntegratedADC") != end) {
      const std::string bname = Name + "ModuleIntegratedADC";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber][2]/F";
      tr->Branch(bname.c_str(), ModuleIntegratedADC, leaflist.str().c_str());
      std::cout << "     -- ModuleIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTime") != end) {
      const std::string bname = Name + "ModuleInitialTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber][2]/F";
      tr->Branch(bname.c_str(), ModuleInitialTime, leaflist.str().c_str());
      std::cout << "     -- ModuleInitialTime: enabled" << std::endl;
    }
    
    if (std::find(begin, end, "ModuleSumIntegratedADC") != end) {
      const std::string bname = Name + "ModuleSumIntegratedADC";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleSumIntegratedADC, leaflist.str().c_str());
      std::cout << "     -- ModuleSumIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTimeAverage") != end) {
      const std::string bname = Name + "ModuleInitialTimeAverage";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleInitialTimeAverage, leaflist.str().c_str());
      std::cout << "     -- ModuleInitialTimeAverage: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTimeDifference") != end) {
      const std::string bname = Name + "ModuleInitialTimeDifference";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleInitialTimeDifference, leaflist.str().c_str());
      std::cout << "     -- ModuleInitialTimeDifference: enabled" << std::endl;
    }
  }


  void KanaE14CV::ProcessModuleInfo()
  {
    /*
      Calculate module related values, such as ModuleSumIntegratedADC or so
      In usual case, Number, ModID, IntegratedADC, ..., are expected to be loaded
      from some ROOT file, for instance DST data, before calling this function.
    */
    
    std::map<Int_t,Int_t> num_map;
    for (Int_t num=0; num < Number; ++num) {
      num_map[ ModID[num] ] = num;
    }
    
    /* fill */
    // initialize
    for (Int_t i_mod=0; i_mod < nModules; ++i_mod) {
      ModuleModID[i_mod]                 = 0;
      ModuleSumIntegratedADC[i_mod]      = 0.;
      ModuleInitialTimeAverage[i_mod]    = 0.;
      ModuleInitialTimeDifference[i_mod] = 0.;
    }
    ModuleNumber = 0;
    
    // loop for modules
    for (Int_t i_mod=0; i_mod < nModules; ++i_mod) {
      
      const Int_t module_ID = ModuleIDList.at(i_mod);
      
      const Int_t e14_ID_A = module_ID;
      const Int_t e14_ID_B = module_ID + 1;
      
      const Int_t num_A = (num_map.find(e14_ID_A)!=num_map.end() ? num_map[e14_ID_A] : -1);
      const Int_t num_B = (num_map.find(e14_ID_B)!=num_map.end() ? num_map[e14_ID_B] : -1);
      
      if (num_A==-1 || num_B==-1) continue;
      
      ModuleModID[ModuleNumber]                 = module_ID;
      ModuleSumIntegratedADC[ModuleNumber]      = IntegratedADC[num_A]+IntegratedADC[num_B];
      ModuleInitialTimeAverage[ModuleNumber]    = (InitialTime[num_A]+InitialTime[num_B])*0.5;
      ModuleInitialTimeDifference[ModuleNumber] = (InitialTime[num_A]-InitialTime[num_B]);
      ++ModuleNumber;
    }
  }


  KanaE14CV::~KanaE14CV()
  {
    if (ModulePeak          != NULL) delete [] ModulePeak;
    if (ModuleIntegratedADC != NULL) delete [] ModuleIntegratedADC;
    if (ModuleInitialTime   != NULL) delete [] ModuleInitialTime;
    
    if (ModuleSumIntegratedADC      != NULL) delete [] ModuleSumIntegratedADC;
    if (ModuleInitialTimeAverage    != NULL) delete [] ModuleInitialTimeAverage;
    if (ModuleInitialTimeDifference != NULL) delete [] ModuleInitialTimeDifference;
  }
  
}
