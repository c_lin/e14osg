#include "KanaLib/KanaE14CSI.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include <TAxis.h>
#include <TMath.h>

#include "KanaLib/KanaErrorManager.h"
#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaE14CSI)

namespace KanaLibrary
{

  KanaE14CSI::KanaE14CSI()
    : KanaE14Detector125MHz()
  {
    InitKanaE14CSI();
  }

  KanaE14CSI::KanaE14CSI(const Int_t acc_run_ID, const Int_t pro_version)
    : KanaE14Detector125MHz(acc_run_ID, pro_version, "CSI")
  {
    InitKanaE14CSI();
    
    LoadPositionMap();
    
    // allocate memories
    PosX = new Float_t [nChannels];
    PosY = new Float_t [nChannels];
    
    CosmicModID = new Int_t   [nChannels];
    CosmicEne   = new Float_t [nChannels];
    CosmicTime  = new Float_t [nChannels];
    CosmicPosX  = new Float_t [nChannels];
    CosmicPosEX = new Float_t [nChannels];
    CosmicPosY  = new Float_t [nChannels];
    CosmicPosEY = new Float_t [nChannels];
    
    f_CosmicTrack = new TF1("f_CosmicTrack", "pol1");

    // function & hitogram for hough transformation used in cosmic ray tracking
    f_HoughLine  = new TF1("f_HoughLine", "[0]*cos(x)+[1]*sin(x)", 0, TMath::TwoPi());
    h_HoughSpace = new TH2D("h_HoughSpace", "Hough Space;#theta [radian];r [mm]",
  			  100, 0, TMath::TwoPi(), 100, 0, 2000);
  }

  void KanaE14CSI::InitKanaE14CSI()
  {
    PosX = NULL;
    PosY = NULL;
    
    HoughR         = 0.;
    HoughTheta     = 0.;
    HoughIntercept = 0.;
    HoughSlope     = 0.;
    
    CosmicNumber = 0;
    CosmicModID  = NULL;
    CosmicEne    = NULL;
    CosmicTime   = NULL;
    CosmicPosX   = NULL;
    CosmicPosEX  = NULL;
    CosmicPosY   = NULL;
    CosmicPosEY  = NULL;
    
    CosmicIntercept = 0.;
    CosmicSlope     = 0.;
    CosmicFitChi2   = 0.;
    CosmicFitNDF    = 0;
    
    CosmicEnergyThreshold = 0.;
    f_CosmicTrack = NULL;

    f_HoughLine   = NULL;
    h_HoughSpace  = NULL;
  }

  void KanaE14CSI::LoadPositionMap()
  {
    const std::string fin_path = "/home/had/ikamiji/work/Main/Run62/ChPosMap/ChPosMapCsI.dat";
    std::ifstream fin(fin_path.c_str());
    if (!fin) {
      DumpError("KanaE14CSI::LoadPoisitionMap", "cannot open file '"+fin_path+"'");
      SetErrorFlag(LoadPositionMapError);
      return;
    }
    
    std::map<Int_t, std::pair<Double_t,Double_t> > pos_map_temp;
    while (!fin.eof()) {
      
      std::string line;
      std::getline(fin, line);
      
      if (KanaFunctions::NoCharacter(line)) {
        if (fin.eof()) break;
        else           continue;
      }
      
      std::vector<std::string> vars;
      if (KanaFunctions::ReadSSVLine(line, vars) != 5) {
        DumpError("KanaE14CSI::LoadPositionMap", "invalid number of values in a line");
        SetErrorFlag(LoadPositionMapError);
        return;
      }
      
      const Int_t e14_ID = std::atoi(vars.at(0).c_str());

      // coordinates: view from downsteam (+x: north, +y: up)
      const Double_t pos_x  = (-1)*std::atof(vars.at(1).c_str());
      const Double_t pos_y  = std::atof(vars.at(2).c_str());
      
      pos_map_temp[e14_ID] = std::pair<Double_t,Double_t>(pos_x, pos_y);
    }
    
    PositionMap = pos_map_temp;
  }


  void KanaE14CSI::SetBranchAddresses(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branch addresses: " << Name << std::endl;
    
    // set branch addresses of KanaE14DetectorBase
    SetBranchAddressesBase(tr, b_list);

    // set branch addresses of KanaE14Detector125MHz
    SetBranchAddresses125MHz(tr, b_list);
    
    // set branch addresses of KanaE14CSI
    SetBranchAddressesCSI(tr, b_list);
  }

  void KanaE14CSI::SetBranchAddressesCSI(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();
    
    if (std::find(begin, end, "PosX") != end) {
      const std::string bname = Name + "PosX";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PosX);
      std::cout << "     -- PosX: enabled" << std::endl;
    }
    if (std::find(begin, end, "PosY") != end) {
      const std::string bname = Name + "PosY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PosY);
      std::cout << "     -- PosY: enabled" << std::endl;
    }
    if (std::find(begin, end, "HoughR") != end) {
      const std::string bname = Name + "HoughR";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &HoughR);
      std::cout << "     -- HoughR: enabled" << std::endl;
    }
    if (std::find(begin, end, "HoughTheta") != end) {
      const std::string bname = Name + "HoughTheta";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &HoughTheta);
      std::cout << "     -- HoughTheta: enabled" << std::endl;
    }
    if (std::find(begin, end, "HoughIntercept") != end) {
      const std::string bname = Name + "HoughIntercept";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &HoughIntercept);
      std::cout << "     -- HoughIntercept: enabled" << std::endl;
    }
    if (std::find(begin, end, "HoughSlope") != end) {
      const std::string bname = Name + "HoughSlope";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &HoughSlope);
      std::cout << "     -- HoughSlope: enabled" << std::endl;
    }

    if (std::find(begin, end, "CosmicNumber") != end) {
      const std::string bname = Name + "CosmicNumber";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicNumber);
      std::cout << "     -- CosmicNumber: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModID") != end) {
      const std::string bname = Name + "CosmicModID";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModID);
      std::cout << "     -- CosmicModID: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicEne") != end) {
      const std::string bname = Name + "CosmicEne";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicEne);
      std::cout << "     -- CosmicEne: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicTime") != end) {
      const std::string bname = Name + "CosmicTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicTime);
      std::cout << "     -- CosmicTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPosX") != end) {
      const std::string bname = Name + "CosmicPosX";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicPosX);
      std::cout << "     -- CosmicPosX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPosEX") != end) {
      const std::string bname = Name + "CosmicPosEX";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicPosEX);
      std::cout << "     -- CosmicPosEX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPosY") != end) {
      const std::string bname = Name + "CosmicPosY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicPosY);
      std::cout << "     -- CosmicPosY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPosEY") != end) {
      const std::string bname = Name + "CosmicPosEY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicPosEY);
      std::cout << "     -- CosmicPosEY: enabled" << std::endl;
    }

    if (std::find(begin, end, "CosmicIntercept") != end) {
      const std::string bname = Name + "CosmicIntercept";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicIntercept);
      std::cout << "     -- CosmicIntercept: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicSlope") != end) {
      const std::string bname = Name + "CosmicSlope";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicSlope);
      std::cout << "     -- CosmicSlope: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicFitChi2") != end) {
      const std::string bname = Name + "CosmicFitChi2";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicFitChi2);
      std::cout << "     -- CosmicFitChi2: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicFitNDF") != end) {
      const std::string bname = Name + "CosmicFitNDF";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicFitNDF);
      std::cout << "     -- CosmicFitNDF: enabled" << std::endl;
    }

    if (std::find(begin, end, "CosmicEnergyThreshold") != end) {
      const std::string bname = Name + "CosmicEnergyThreshold";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicEnergyThreshold);
      std::cout << "     -- CosmicEnergyThreshold: enabled" << std::endl;
    }
  }


  void KanaE14CSI::AddBranches(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branches: " << Name << std::endl;
    
    // add branches of KanaE14DetectorBase
    AddBranchesBase(tr, b_list);

    // add branches of KanaE14Detector125MHz
    AddBranches125MHz(tr, b_list);
    
    // add branches of KanaE14CSI
    AddBranchesCSI(tr, b_list);
  }


  void KanaE14CSI::AddBranchesCSI(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "PosX") != end) {
      const std::string bname = Name + "PosX";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/F";
      tr->Branch(bname.c_str(), PosX, leaflist.str().c_str());
      std::cout << "     -- PosX: enabled" << std::endl;
    }
    if (std::find(begin, end, "PosY") != end) {
      const std::string bname = Name + "PosY";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/F";
      tr->Branch(bname.c_str(), PosY, leaflist.str().c_str());
      std::cout << "     -- PosY: enabled" << std::endl;
    }

    if (std::find(begin, end, "HoughR") != end) {
      const std::string bname = Name + "HoughR";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &HoughR, leaflist.str().c_str());
      std::cout << "     -- HoughR: enabled" << std::endl;
    }
    if (std::find(begin, end, "HoughTheta") != end) {
      const std::string bname = Name + "HoughTheta";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &HoughTheta, leaflist.str().c_str());
      std::cout << "     -- HoughTheta: enabled" << std::endl;
    }
    if (std::find(begin, end, "HoughIntercept") != end) {
      const std::string bname = Name + "HoughIntercept";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &HoughIntercept, leaflist.str().c_str());
      std::cout << "     -- HoughIntercept: enabled" << std::endl;
    }
    if (std::find(begin, end, "HoughSlope") != end) {
      const std::string bname = Name + "HoughSlope";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &HoughSlope, leaflist.str().c_str());
      std::cout << "     -- HoughSlope: enabled" << std::endl;
    }

    if (std::find(begin, end, "CosmicNumber") != end) {
      const std::string bname = Name + "CosmicNumber";
      std::stringstream leaflist;
      leaflist << bname << "/I";
      tr->Branch(bname.c_str(), &CosmicNumber, leaflist.str().c_str());
      std::cout << "     -- CosmicNumber: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModID") != end) {
      const std::string bname = Name + "CosmicModID";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/I";
      tr->Branch(bname.c_str(), CosmicModID, leaflist.str().c_str());
      std::cout << "     -- CosmicModID: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicEne") != end) {
      const std::string bname = Name + "CosmicEne";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicEne, leaflist.str().c_str());
      std::cout << "     -- CosmicEne: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicTime") != end) {
      const std::string bname = Name + "CosmicTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicTime, leaflist.str().c_str());
      std::cout << "     -- CosmicTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPosX") != end) {
      const std::string bname = Name + "CosmicPosX";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicPosX, leaflist.str().c_str());
      std::cout << "     -- CosmicPosX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPosEX") != end) {
      const std::string bname = Name + "CosmicPosEX";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicPosEX, leaflist.str().c_str());
      std::cout << "     -- CosmicPosEX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPosY") != end) {
      const std::string bname = Name + "CosmicPosY";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicPosY, leaflist.str().c_str());
      std::cout << "     -- CosmicPosY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicPosEY") != end) {
      const std::string bname = Name + "CosmicPosEY";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicNumber]/F";
      tr->Branch(bname.c_str(), CosmicPosEY, leaflist.str().c_str());
      std::cout << "     -- CosmicPosEY: enabled" << std::endl;
    }

    if (std::find(begin, end, "CosmicIntercept") != end) {
      const std::string bname = Name + "CosmicIntercept";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicIntercept, leaflist.str().c_str());
      std::cout << "     -- CosmicIntercept: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicSlope") != end) {
      const std::string bname = Name + "CosmicSlope";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicSlope, leaflist.str().c_str());
      std::cout << "     -- CosmicSlope: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicFitChi2") != end) {
      const std::string bname = Name + "CosmicFitChi2";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicFitChi2, leaflist.str().c_str());
      std::cout << "     -- CosmicFitChi2: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicFitNDF") != end) {
      const std::string bname = Name + "CosmicFitNDF";
      std::stringstream leaflist;
      leaflist << bname << "/I";
      tr->Branch(bname.c_str(), &CosmicFitNDF, leaflist.str().c_str());
      std::cout << "     -- CosmicFitNDF: enabled" << std::endl;
    }

    if (std::find(begin, end, "CosmicEnergyThreshold") != end) {
      const std::string bname = Name + "CosmicEnergyThreshold";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicEnergyThreshold, leaflist.str().c_str());
      std::cout << "     -- CosmicEnergyThreshold: enabled" << std::endl;
    }
  }

  void KanaE14CSI::CalcCosmicTrack(const Double_t energy_threshold)
  {
    /* initialization */
    CosmicEnergyThreshold = energy_threshold;
    
    // Int_t    n_points = 0;
    // Double_t hit_pos_x[4096]={0.};
    // Double_t hit_pos_y[4096]={0.};
    // Double_t hit_pos_error[4096]={0.};
    
    CosmicNumber = 0;
    
    /* Hough transformation to get initial value */
    HoughTransform();
    
    /* get hit crystals of CsI */
    //   - hit condition
    //      -- (distance from the line obtained from hough trans.) < 100 mm
    //      -- energy deposit >= energy_threshold (default: 3 MeV)

    // estimated line w/ Hough trans.: ax + by = R
    const Double_t a = TMath::Cos(HoughTheta);
    const Double_t b = TMath::Sin(HoughTheta);
    
    // hit position error for small/large crystals (uniform dist. assumption)
    const Double_t small_width_error = 25. / TMath::Sqrt(12);
    const Double_t large_width_error = 50. / TMath::Sqrt(12);
    
    for (Int_t num=0; num < Number; ++num) {
      
      if (Ene[num] < CosmicEnergyThreshold) continue;
      
      const Int_t e14_ID = ModID[num];
      const Double_t x = PositionMap[e14_ID].first;
      const Double_t y = PositionMap[e14_ID].second;
      
      // calculate distance from the line obtained from hough trans.
      const Double_t distance = TMath::Abs(a*x + b*y - HoughR) / TMath::Sqrt(a*a + b*b);
      if (distance < 100) {
        // hit_pos_x[n_points] = x;
        // hit_pos_y[n_points] = y;
        // hit_pos_error[n_points] = (e14_ID<2240) ? small_width_error : large_width_error;
        // ++n_points;
        CosmicModID[CosmicNumber] = e14_ID;
        CosmicEne[CosmicNumber]   = Ene[num];
        CosmicTime[CosmicNumber]  = Time[num];
        CosmicPosX[CosmicNumber]  = x;
        CosmicPosY[CosmicNumber]  = y;
        CosmicPosEX[CosmicNumber]	= CosmicPosEY[CosmicNumber]
  	= ((e14_ID<2240) ? small_width_error : large_width_error);
        ++CosmicNumber;
      }
    }

    
    /* fitting */
    // set initial parameters
    if (-40<HoughSlope && HoughSlope<40) { // set upper limit on the initial slope
      f_CosmicTrack->SetParameters(HoughIntercept, HoughSlope);
    } else {
      f_CosmicTrack->SetParameters(HoughIntercept, ((HoughSlope>0)?40:-40) );
    }
    
    // TGraphErrors g_hit(n_points, hit_pos_x, hit_pos_y, hit_pos_error, hit_pos_error);
    TGraphErrors g_hit(CosmicNumber, CosmicPosX, CosmicPosY, CosmicPosEX, CosmicPosEY);
    g_hit.Fit(f_CosmicTrack, "qn");
    
    // get parameters
    CosmicIntercept = f_CosmicTrack->GetParameter(0);
    CosmicSlope     = f_CosmicTrack->GetParameter(1);
    CosmicFitChi2   = f_CosmicTrack->GetChisquare();
    CosmicFitNDF    = f_CosmicTrack->GetNDF();
  }


  void KanaE14CSI::HoughTransform()
  {
    /* initialization */
    h_HoughSpace->Reset();
    
    HoughR         = 0;
    HoughTheta     = 0;
    HoughSlope     = 0;
    HoughIntercept = 0;

    TAxis *x_axis = h_HoughSpace->GetXaxis();
    TAxis *y_axis = h_HoughSpace->GetYaxis();
    
    const Int_t n_binsx = h_HoughSpace->GetNbinsX();
    const Int_t n_binsy = h_HoughSpace->GetNbinsY();


    /* fill hough space */
    for (Int_t num=0; num < Number; ++num) {
      
      if (Ene[num] < CosmicEnergyThreshold) continue;
      
      const Int_t e14_ID = ModID[num];
      
      const Double_t pos_x = PositionMap[e14_ID].first;
      const Double_t pos_y = PositionMap[e14_ID].second;
      f_HoughLine->SetParameters(pos_x, pos_y);
      
      for (Int_t xbin_ID=1; xbin_ID <= n_binsx; ++xbin_ID) {
        
        const Double_t x_low_edge = x_axis->GetBinLowEdge(xbin_ID);
        const Double_t x_up_edge  = x_axis->GetBinUpEdge(xbin_ID);
        const Double_t r_min      = f_HoughLine->GetMinimum(x_low_edge, x_up_edge);
        const Double_t r_max      = f_HoughLine->GetMaximum(x_low_edge, x_up_edge);
        
        for (Int_t ybin_ID=1; ybin_ID <= n_binsy; ++ybin_ID) {
  	
  	const Double_t y_low_edge = y_axis->GetBinLowEdge(ybin_ID);
  	const Double_t y_up_edge  = y_axis->GetBinUpEdge(ybin_ID);
  	
  	if (y_low_edge>=r_min && y_low_edge<r_max)
  	  h_HoughSpace->Fill( (x_low_edge+x_up_edge)*0.5, (y_low_edge+y_up_edge)*0.5);
        }
      }
    }
    
    /* get most likely line parameter in hough space */
    Int_t binx;
    Int_t biny;
    Int_t binz;
    h_HoughSpace->GetMaximumBin(binx, biny, binz);
    
    HoughR     = y_axis->GetBinCenter(biny);
    HoughTheta = x_axis->GetBinCenter(binx);
    /* NOTE: in old LCV calibration (<v1.2), GetBinLowEdge() was used */
    
    HoughIntercept = HoughR / TMath::Sin(HoughTheta);
    HoughSlope     = - TMath::Tan(TMath::Pi()*0.5 - HoughTheta);
  }


  void KanaE14CSI::DrawOutline(KanaDrawManager &man)
  {
    TBox box;
    box.SetFillStyle(0);
    
    for (Int_t i=0; i < nChannels; ++i) {
      const Int_t    e14_ID = E14IDList.at(i);
      const Double_t pos_x  = PositionMap[e14_ID].first;
      const Double_t pos_y  = PositionMap[e14_ID].second;
      if (IsLarge(e14_ID)) {
        man.DrawBox(&box, pos_x-25,   pos_y-25,   pos_x+25,   pos_y+25  );
      } else {
        man.DrawBox(&box, pos_x-12.5, pos_y-12.5, pos_x+12.5, pos_y+12.5);
      }
    }
  }


  KanaE14CSI::~KanaE14CSI()
  {
    if (PosX != NULL) delete [] PosX;
    if (PosY != NULL) delete [] PosY;
    
    if (CosmicModID != NULL) delete [] CosmicModID;
    if (CosmicEne   != NULL) delete [] CosmicEne;
    if (CosmicTime  != NULL) delete [] CosmicTime;
    if (CosmicPosX  != NULL) delete [] CosmicPosX;
    if (CosmicPosEX != NULL) delete [] CosmicPosEX;
    if (CosmicPosY  != NULL) delete [] CosmicPosY;
    if (CosmicPosEY != NULL) delete [] CosmicPosEY;
    
    if (f_CosmicTrack != NULL) delete f_CosmicTrack;
    if (f_HoughLine   != NULL) delete f_HoughLine;
    if (h_HoughSpace  != NULL) delete h_HoughSpace;
  }
    
}
