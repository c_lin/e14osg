#include "KanaLib/KanaFunctions.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <sys/types.h>
#include <dirent.h>

#include <TChain.h>
#include <TLine.h>
#include <TBox.h>
#include <TMath.h>

#include "KanaLib/KanaE14IDManager.h"

ClassImp(KanaLibrary::KanaFunctions)

namespace KanaLibrary
{
  KanaFunctions::KanaFunctions() {}
  KanaFunctions::~KanaFunctions() {}


  /* If "str" does not have any character (i.e., other than space/tab) */
  /* NoCharacter returns true.                                         */
  Bool_t KanaFunctions::NoCharacter(const std::string &str)
  {
    for (int i=0, n=str.size(); i < n; ++i) {
      if ((str.at(i)!=' ') && (str.at(i)!='\t')) return false;
    }
    return true;
  } // NoCharacter

  /* read a line of input stream         */
  /* getting strings separated by spaces */
  Int_t KanaFunctions::ReadSSVLine(const std::string &line, std::vector<std::string> &vars)
  {
    std::vector<std::string> temp(0);
    
    for (Int_t i=0, n=line.size(); i < n; ++i) {
      
      if ( (line.at(i)==' ') || (line.at(i)=='\t') ) continue;
      
      std::string var = "";
      do {
        if ( (line.at(i)==' ') || (line.at(i)=='\t') ) break;
        var += line.at(i);
        ++i;
      } while (i<n);
      
      temp.push_back(var);
    }
    
    vars = temp;
    return static_cast<Int_t>(temp.size());
  }

  /* read a line of input stream         */
  /* getting strings separated by commas */
  Int_t KanaFunctions::ReadCSVLine(const std::string &line, std::vector<std::string> &vars)
  {
    std::vector<std::string> temp(0);
    
    for (Int_t i=0, n=line.size(); i < n; ++i) {
      
      if (line.at(i)==',') continue;
      
      std::string var = "";
      do {
        if (line.at(i)==',') break;
        var += line.at(i);
        ++i;
      } while (i<n);
      
      temp.push_back(var);
    }
    
    vars = temp;
    return static_cast<Int_t>(temp.size());
  }


  /* split a line divided by a delimiter into a list of words */
  std::vector<std::string> KanaFunctions::SplitLine(const std::string &line, const char delimiter)
  {
    std::vector<std::string> list_temp(0);
    
    std::string word = "";
    for (Int_t i=0, n=line.size(); i < n; ++i) {
      std::string next = line.substr(i, 1);
      if (next==delimiter) {
        list_temp.push_back(word);
        word = "";
      } else {
        word += next;
      }
    }
    if (word.size()!=0) list_temp.push_back(word);
    
    return list_temp;
  }


  /* translate hexadicimal string to unsigned integer */
  UInt_t KanaFunctions::HexStringToUInt(std::string hex_str)
  {
    // remove prefix '0x'
    if (hex_str.substr(0,2) == "0x") {
      hex_str = hex_str.substr(2);
    }
    
    // to lower
    std::transform(hex_str.begin(), hex_str.end(), hex_str.begin(), tolower);
    
    // interplet
    UInt_t value = 0;
    Int_t  place = 1;
    for (Int_t i=hex_str.size()-1; i>=0; --i) {
      const char digit = hex_str.at(i);
      if ('0'<=digit && digit<='9') {
        value += place * (digit-'0');
      } else {
        value += place * (digit-'a'+10);
      }
      place*=16;
    }
    return value;
  } // HexStringToUInt(std::string hex_str)


  /* convert string date/time to local UNIX time */
  Int_t KanaFunctions::GetUNIXTime(const std::string str_time, const std::string format)
  {
    struct std::tm tm_time;
    strptime(str_time.c_str(), format.c_str(), &tm_time);
    const Int_t time = std::mktime(&tm_time);
    return time;
  } // GetUNIXTime(const std::string str_time, const std::string format)


  /* set branch status & address */
  Int_t KanaFunctions::ActivateBranch(TTree *tr, const std::string bname, void *add)
  {
    tr->SetBranchStatus(bname.c_str(), 1);
    return tr->SetBranchAddress(bname.c_str(), add);
  } // ActivateBranch

  /* get run ID list from file */
  Int_t KanaFunctions::GetRunList(const std::string fin_path, std::vector<Int_t> &list)
  {
    std::ifstream fin(fin_path.c_str());
    if (!fin) {
      std::cerr << "ERROR: in KanaFunctions::GetRunList: "
  		<< "cannot open file '" << fin_path << "'" << std::endl;
      return 1;
    }
    
    std::vector<Int_t> list_temp(0);
    while (!fin.eof()) {

      std::string line;
      std::getline(fin, line);

      if (NoCharacter(line)) {
  	if (fin.eof()) break;
  	else           continue;
      }
      
      std::vector<std::string> vars;
      ReadSSVLine(line, vars);
      if (vars.size() != 1) {
  	std::cerr << "ERROR: in KanaFunctions::GetRunList: "
  		  << "invalid format of file" << std::endl;
  	return 2;
      }
      
      const Int_t run_ID = std::atoi(vars.at(0).c_str());
      list_temp.push_back(run_ID);
    }
    
    list = list_temp;
    
    return 0;
  } // GetRunList


  /* get node ID list */
  Int_t KanaFunctions::GetDstNodeIDList(const Int_t acc_run_ID, const Int_t pro_version,
  				      std::vector<Int_t> &list)
  {
    return GetNodeIDList(acc_run_ID, pro_version, "dst", list);
  }

  Int_t KanaFunctions::GetConvNodeIDList(const Int_t acc_run_ID, const Int_t pro_version,
  				       std::vector<Int_t> &list)
  {
    return GetNodeIDList(acc_run_ID, pro_version, "conv", list);
  }

  // - name: conv or dst
  Int_t KanaFunctions::GetNodeIDList(const Int_t acc_run_ID, const Int_t pro_version,
  				   const std::string name, std::vector<Int_t> &list)
  {
    // set top directory
    std::string conv_or_dst = name;
    std::transform(name.begin(), name.end(), conv_or_dst.begin(), tolower);
    
    std::stringstream top_dir;
    top_dir << "/home/had/togawa/work/production/"
  	    << "run" << acc_run_ID << "/pro" << pro_version << "/work/fiber_out/"
  	    << conv_or_dst << "_data";
    
    return GetNodeIDList(top_dir.str(), list);
  } // GetNodeIDList

  Int_t KanaFunctions::GetNodeIDList(const std::string top_dir, std::vector<Int_t> &list)
  {
    // open directory
    DIR *dp = opendir(top_dir.c_str());
    if (dp==NULL) {
      std::cerr << "ERROR: in GetNodeIDList: cannot find directory '" << top_dir << "'"
  	      << std::endl;
      return 1;
    }
    
    // get entry of the directory
    std::vector<Int_t> list_temp;
    struct dirent *dent = NULL;
    do {
      
      dent = readdir(dp);
      if (dent==NULL) break;
      
      const std::string name = dent->d_name;
      if (name.find("node") != std::string::npos) {
  	const Int_t node_ID = std::atoi(name.substr(4).c_str());
  	list_temp.push_back(node_ID);
      }
      
    } while (dent != NULL);
    
    // sort
    std::sort(list_temp.begin(), list_temp.end());
    
    // close directory
    closedir(dp);
    
    list = list_temp;
    
    return 0;
  } // GetNodeIDList


  Int_t KanaFunctions::GetDetectorList(const Int_t acc_run_ID, std::vector<std::string> det_list)
  {
    std::stringstream fin_path;
    fin_path << "/home/had/ikamiji/work/Main/Common/data/Run" << acc_run_ID << "/"
  	   << "Basic/DetectorList.txt";
    std::ifstream fin(fin_path.str().c_str());
    if (!fin) {
      std::cerr << "ERROR: cannot open file '" << fin_path.str() << "'" << std::endl;
      return 1;
    }
    
    std::vector<std::string> list_temp(0);
    while (!fin.eof()) {

      std::string line;
      std::getline(fin, line);
      
      if (NoCharacter(line)) {
  	if (fin.eof()) break;
  	else           continue;
      }
      
      std::vector<std::string> vars;
      if (ReadSSVLine(line, vars)!=1) {
  	std::cerr << "ERROR: invalid file format" << std::endl;
  	return 2;
      }

      list_temp.push_back(vars.at(0));
    }
    
    det_list = list_temp;
    
    return 0;
  } // GetDetectorList


  TGraphErrors* KanaFunctions::GetRatioGraph(TGraphErrors *g1, TGraphErrors *g2)
  {
    TGraphErrors *g_ratio = new TGraphErrors(*g1);
    
    for (Int_t i=0, n=g1->GetN(); i < n; ++i) {
      
      const Double_t x = g1->GetX()[i];
      
      const Double_t y1  = g1->GetY()[i];
      const Double_t ey1 = g1->GetEY()[i];
      
      const Double_t y2  = g2->GetY()[i];
      const Double_t ey2 = g2->GetEY()[i];
      
      const Double_t ratio   = (y2!=0 ? y1/y2 : 0);
      const Double_t e_ratio
        = (y2!=0 ? TMath::Sqrt(TMath::Power(ey1/y1,2)+TMath::Power(ey2/y2,2))*ratio : 0);
      
      g_ratio->SetPoint(i, x, ratio);
      g_ratio->SetPointError(i, 0, e_ratio);
    }
    
    return g_ratio;
  } // GetRatioGraph


  TGraphErrors* KanaFunctions::GetDiffGraph(TGraphErrors *g1, TGraphErrors *g2)
  {
    TGraphErrors *g_diff = new TGraphErrors(g1->GetN());
    for (Int_t i=0, n=g1->GetN(); i < n; ++i) {
      const Double_t x   = g1->GetX()[i];
      const Double_t y1  = g1->GetY()[i];
      const Double_t ey1 = g1->GetEY()[i];
      const Double_t y2  = g2->GetY()[i];
      const Double_t ey2 = g2->GetEY()[i];
      const Double_t diff   = y1-y2;
      const Double_t e_diff = TMath::Sqrt(ey1*ey1+ey2*ey2);
      g_diff->SetPoint(i, x, diff);
      g_diff->SetPointError(i, 0, e_diff);
    }
    return g_diff;
  } // GetDiffGraph

  TH1* KanaFunctions::GetRatioHisto(const TH1 *h1, const TH1 *h2)
  {
    TH1 *h_ratio = (TH1*)h1->Clone("h_ratio");
    h_ratio->Divide(h1, h2);
    return h_ratio;
  }

  TH1* KanaFunctions::GetDiffHisto(const TH1 *h1, const TH1 *h2)
  {
    TH1 *h_diff = (TH1*)h1->Clone("h_diff");
    h_diff->Add(h2, -1);
    return h_diff;
  }

  /* calculate pedestal */
  /* compare standard deviation of first and last 10 samples */
  /* and take mean of 10 samples w/ the smaller std dev as pedestal*/
  Double_t KanaFunctions::GetPedestal(const Int_t n_samples, const Short_t *data, const Int_t stat)
  {
    Double_t first_sum  = 0.;
    Double_t first_sum2 = 0.;
    for (Int_t i=0; i < stat; ++i) {
      first_sum  += data[i];
      first_sum2 += data[i]*data[i];
    }
    Double_t first_mean   = first_sum/stat;
    Double_t first_stddev = TMath::Sqrt(first_sum2/stat - first_mean*first_mean);
    
    Double_t last_sum  = 0.;
    Double_t last_sum2 = 0.;
    for (Int_t i=n_samples-stat; i < n_samples; ++i) {
      last_sum  += data[i];
      last_sum2 += data[i]*data[i];
    }
    Double_t last_mean   = last_sum/stat;
    Double_t last_stddev = TMath::Sqrt(last_sum2/stat - last_mean*last_mean);
    
    return (first_stddev < last_stddev) ? first_mean : last_mean;
  } // GetPedestal

  Double_t KanaFunctions::GetPedestal(const Int_t n_samples, const Long64_t *data, const Int_t stat)
  {
    Double_t first_sum  = 0.;
    Double_t first_sum2 = 0.;
    for (Int_t i=0; i < stat; ++i) {
      first_sum  += data[i];
      first_sum2 += data[i]*data[i];
    }
    Double_t first_mean   = first_sum/stat;
    Double_t first_stddev = TMath::Sqrt(first_sum2/stat - first_mean*first_mean);
    
    Double_t last_sum  = 0.;
    Double_t last_sum2 = 0.;
    for (Int_t i=n_samples-stat; i < n_samples; ++i) {
      last_sum  += data[i];
      last_sum2 += data[i]*data[i];
    }
    Double_t last_mean   = last_sum/stat;
    Double_t last_stddev = TMath::Sqrt(last_sum2/stat - last_mean*last_mean);
    
    return (first_stddev < last_stddev) ? first_mean : last_mean;
  } // GetPedestal

  Double_t KanaFunctions::GetPedestal(const Int_t n_samples, const Double_t *data, const Int_t stat)
  {
    Double_t first_sum  = 0.;
    Double_t first_sum2 = 0.;
    for (Int_t i=0; i < stat; ++i) {
      first_sum  += data[i];
      first_sum2 += data[i]*data[i];
    }
    Double_t first_mean   = first_sum/stat;
    Double_t first_stddev = TMath::Sqrt(first_sum2/stat - first_mean*first_mean);
    
    Double_t last_sum  = 0.;
    Double_t last_sum2 = 0.;
    for (Int_t i=n_samples-stat; i < n_samples; ++i) {
      last_sum  += data[i];
      last_sum2 += data[i]*data[i];
    }
    Double_t last_mean   = last_sum/stat;
    Double_t last_stddev = TMath::Sqrt(last_sum2/stat - last_mean*last_mean);
    
    return (first_stddev < last_stddev) ? first_mean : last_mean;
  } // GetPedestal


  /* calculate baseline */
  /* as 3 bin mean around most probable FADC counts in a set of waveform samples */
  /*    - resolution   : resoultion of ADC [bit] */
  /*    - n_merged_bins: number of merged bins   */
  Double_t KanaFunctions::GetBaseline(const Int_t resolution, const Int_t n_merged_bins,
  				    const Int_t n_samples, const Short_t *data)
  {
    /* sum up number of entires in each n_merged_bins */
    const Int_t dynamic_range = (1 << resolution);
    const Int_t n_sums
      = dynamic_range/n_merged_bins + ( ((dynamic_range%n_merged_bins)>0) ? 1 : 0 );
    // Int_t sum[n_sums];
    std::vector<Int_t> sum(n_sums);
    for (Int_t i=0; i < n_sums; ++i) sum[i] = 0;
    
    for (Int_t i_sample=0; i_sample < n_samples; ++i_sample) {
      const Int_t index = data[i_sample] / n_merged_bins;
      if (0<=index&&index<n_sums-1) ++sum[index];
      else std::cerr << "ERROR: in KanaFunctions::GetBaseline():"
  		   << " unexpected overflow/underflow of waveform data"
  		   << std::endl;
    }
    
    /* search maximum (excluding groups containing underflow & overflow) */
    Int_t max_sum = sum[1];
    Int_t max_sum_index = 1;
    for (Int_t i=2, n=(n_sums-1); i < n; ++i) {
      if (max_sum < sum[i]) {
        max_sum = sum[i];
        max_sum_index = i;
      }
    }
    
    const Int_t participants
      = sum[max_sum_index-1] + sum[max_sum_index] + sum[max_sum_index+1];
    
    const Double_t baseline
      = (sum[max_sum_index-1] *(max_sum_index-0.5)
         +sum[max_sum_index]  *(max_sum_index+0.5)
         +sum[max_sum_index+1]*(max_sum_index+1.5) ) * n_merged_bins
      / static_cast<Double_t>(participants);
      
    return baseline;
  } // GetBaseline

  /* automatic optimization of n_merged_bins */
  Double_t KanaFunctions::GetBaseline(const Int_t resolution, const Int_t n_samples, const Short_t *data, const Double_t contrast_threshold)
  {
    const Int_t    dynamic_range = (1 << resolution);
    
    /* threshold for contrast of maximum bin in y-projection */
    // const Double_t contrast_threshold = 0.05;

    /* get optimum number of merged bins  */
    Int_t n_merged_bins = 1;
    Double_t baseline = 0.;
    for (Int_t i_itr=0; i_itr < resolution; ++i_itr) {

      // set number of merged bins
      n_merged_bins = (1<<i_itr);
      
      // get y-projection w/ given number of merged bins
      const Int_t n_sums
  	= dynamic_range/n_merged_bins + ( ((dynamic_range%n_merged_bins)>0) ? 1 : 0 );
      // Int_t sum[n_sums];
      std::vector<Int_t> sum(n_sums);
      for (Int_t i_sum=0; i_sum < n_sums; ++i_sum) sum[i_sum] = 0;
      
      for (Int_t i_sample=0; i_sample < n_samples; ++i_sample) {
        const Int_t index = data[i_sample] / n_merged_bins;
        if (0<=index&&index<n_sums) ++sum[index];
        else std::cerr << "ERROR: in KanaFunctions::GetBaseline():"
  		     << " unexpected overflow/underflow of waveform data"
  		     << std::endl;
      }
      
      // search maximum (excluding groups containing underflow & overflow)
      Int_t max_sum = sum[1];
      Int_t max_sum_index = 1;
      for (Int_t i_sum=2, n=(n_sums-1); i_sum < n; ++i_sum) {
        if (max_sum < sum[i_sum]) {
  	max_sum = sum[i_sum];
  	max_sum_index = i_sum;
        }
      }
      
      // check peak contrast
      const Double_t contrast = 1. * max_sum / n_samples;
      if (contrast > contrast_threshold) { // return baseline value
        const Int_t participants
  	= sum[max_sum_index-1] + sum[max_sum_index] + sum[max_sum_index+1];
        
        baseline
  	= (sum[max_sum_index-1] *(max_sum_index-0.5)
  	   +sum[max_sum_index]  *(max_sum_index+0.5)
  	   +sum[max_sum_index+1]*(max_sum_index+1.5) ) * n_merged_bins
  	/ static_cast<Double_t>(participants);
        break;
      }
    }
    return baseline;
  } // GetBaseline


  /* return if a data point is peak or not */
  /*   - n_search: number of points included in peak finding */
  /*   - index   : target point index                        */
  /*   - data    : data points                               */
  Bool_t KanaFunctions::IsPeak(Int_t n_search, Int_t index, Short_t *data)
  {
    const Int_t begin_ID = index - n_search/2;
    const Int_t end_ID   = begin_ID + n_search - 1;
    
    Int_t is_peak = 1;
    
    for (Int_t i=begin_ID; i < index; ++i) {
      is_peak *= (data[i] < data[i+1]);
    }
    
    is_peak *= (data[index] >= data[index+1]);
    
    for (Int_t i=index+1; i < end_ID; ++i) {
      is_peak *= (data[i] > data[i+1]);
    }
    
    return static_cast<Bool_t>(is_peak);
  }


  /* calculate parabolic peak time & height */
  /*    - use 3 points of parabolic function */
  /*    - parameter: y = a*x^2 + b*x + c     */
  Double_t KanaFunctions::GetPTime(const Double_t x1, const Double_t y1,
  				 const Double_t x2, const Double_t y2,
  				 const Double_t x3, const Double_t y3)
  {
    const Double_t a = ((y1-y2)*(x1-x3)-(y1-y3)*(x1-x2)) / ((x1-x2)*(x1-x3)*(x2-x3));
    const Double_t b = (y1-y2)/(x1-x2) - a*(x1+x2);
    // const Double_t c = y1 - a*x1*x1 - b*x1;

    Double_t ptime = x2;
    if (a!=0) ptime = -b/(2*a);
    // else      std::cerr << "WARNING: in KanaFunctions::GetPTime(): "
    // 		      << "cannot get parabolic peak, use center value" << std::endl;
    return ptime;
  }
  Double_t KanaFunctions::GetPPeak(const Double_t x1, const Double_t y1,
  				 const Double_t x2, const Double_t y2,
  				 const Double_t x3, const Double_t y3)
  {
    const Double_t a = ((y1-y2)*(x1-x3)-(y1-y3)*(x1-x2)) / ((x1-x2)*(x1-x3)*(x2-x3));
    const Double_t b = (y1-y2)/(x1-x2) - a*(x1+x2);
    const Double_t c = y1 - a*x1*x1 - b*x1;

    Double_t ppeak = y2;
    if (a!=0) ppeak = -b*b/(4*a) + c;
    // else      std::cerr << "WARNING: in KanaFunctions::GetPPeak(): "
    // 		      << "cannot get parabolic peak, use center value" << std::endl;
    return ppeak;
  }


  /*
    set first derivative of "data" to "derivative" using 5-point approximation
  */
  void KanaFunctions::
  GetFirstDerivative(const Int_t n_samples, const Short_t *data, Double_t *derivative)
  {
    derivative[0]=derivative[1]=derivative[n_samples-3]=derivative[n_samples-2]=0;
    for (Int_t i=2, n=n_samples-2; i<n; ++i) {
      derivative[i] = (data[i-2] - 8*data[i-1] + 8*data[i+1] - data[i+2]) / 12.;
    }
  }

  void KanaFunctions::
  GetFirstDerivative(const Int_t n_samples, const Double_t *data, Double_t *derivative)
  {
    derivative[0]=derivative[1]=derivative[n_samples-3]=derivative[n_samples-2]=0;
    for (Int_t i=2, n=n_samples-2; i<n; ++i) {
      derivative[i] = (data[i-2] - 8*data[i-1] + 8*data[i+1] - data[i+2]) / 12.;
    }
  }


  /* 1-dimensional low pass filter            */
  /*   - n     : numbr of input data points   */
  /*   - time  : array for time [sec]         */
  /*   - input : array for input data points  */
  /*   - cutoff: cut off frequency [sec^-1]   */
  /*   - output: array for output data points */
  /* using 4-dim Runge-Kutta method           */
  void KanaFunctions::LowPassFilter(const Double_t cutoff,
  				  const Int_t n, const Double_t *time,
  				  const Double_t *input, Double_t *output)
  {
    const Double_t tau = 1./(TMath::TwoPi()*cutoff);
    
    output[0] = input[0];
    
    for (Int_t i=1; i<n; ++i) {
      
      const Int_t nxt   = i;
      const Int_t cur   = i-1;
      
      const Double_t dt = time[nxt]-time[cur];
      
      const Double_t k1 = (input[cur] - output[cur])/tau;
      const Double_t k2 = ((input[nxt]+input[cur])*0.5 - (output[cur]+k1*dt*0.5))/tau;
      const Double_t k3 = ((input[nxt]+input[cur])*0.5 - (output[cur]+k2*dt*0.5))/tau;
      const Double_t k4 = (input[nxt] - (output[cur]+k3*dt))/tau;
      
      output[nxt] = output[cur] + (k1+2*k2+2*k3+k4) * dt/6.;
      // std::cout << output[nxt] << std::endl;
    }
  } // LowPassFilter


  /* get moving averaged waveform */
  /*   - n        : number of points in input waveform */
  /*   - input    : array for input waveform           */
  /*   - output   : array for moving averaged waveform */
  /*   - n_average: number of points for averaging     */
  void KanaFunctions::MovingAverage(const Int_t n, const Short_t *input,
  				  Double_t *output, const Int_t n_average)
  {
    // initialization
    for (Int_t i=0; i < n; ++i) output[i] = 0;
    
    // moving average
    const Int_t n_one_side = n_average / 2;
    for (Int_t i=0, end=n-n_average; i <= end; ++i) {
      Double_t sum = input[i];
      for (Int_t j=1; j<n_average; ++j) {
  	sum += input[i+j];
      }
      output[i+n_one_side] = sum / n_average;
    }
  } // MovingAverage


  /* asymmetric Gaussian function */
  Double_t KanaFunctions::AsymGauss(Double_t *x, Double_t *par)
  {
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
    // Fit parameters:                                                         //
    // par[0] = overall scale factor                                           //
    // par[1] = most probable point                                            //
    // par[2] = lower-side sigma                                               //
    // par[3] = upper-side sigma                                               //
    //                                                                         //
    // Note1: this is NOT the so-called "(Iwai's) asymmetric Gaussian" in KOTO //
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
    
    const Double_t dx = x[0] - par[1];
    const Double_t val = par[0] * TMath::Gaus(x[0], par[1], (dx<0?par[2]:par[3]) );
    return val;
  }

  /* Landau-Gauss convolution function (created: 2016/05/01       */
  /*   - original: ROOT tutorial %ROOTSYS%tutorials/fit/langaus.C */
  /*   - modified by I.Kamiji                                     */
  /*     -- remove unnecessary local variables                    */
  Double_t KanaFunctions::LandauGauss(Double_t *x, Double_t *par)
  {
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
    // Fit parameters:                                                       //
    // par[0] = Width (scale) parameter of Landau density                    //
    // par[1] = Most Probable (MP, location) paramter of Landau density      //
    // par[2] = Total area (integral -inf. to inf., normalization constant)  //
    // par[3] = Width (sigma) of convoluted Gaussian function                //
    //                                                                       //
    // Note1: This code is based on the code in the ROOT tutorial            //
    // Note2:                                                                //
    // In the Landau dist. in the CERNLIB, the maximum is located at         //
    // x = -0.22278298 with the location parameter = 0.                      //
    // This shift is corrected within this function, so that the actual      //
    // maximum is identical to the MP parameter.                             //
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
    
    // Numeric constants
    const Double_t invsq2pi = 0.3989422804014; // (2 pi)^(-1/2)
    const Double_t mpshift   = -0.22278298;    // default location shift of landau maximum
    
    // Control constants
    const Int_t    np = 100; // number of convolution steps
    const Double_t gsigma_range = 5.0; // convolution extends to +-gsigma_range*(Gauss-sigma)
    
    // MP shift correction
    const Double_t mpc = par[1] - mpshift * par[0];
    
    // Range and step size of convolution integral
    const Double_t xlow = x[0] - gsigma_range * par[3];
    const Double_t xupp = x[0] + gsigma_range * par[3];
    const Double_t step = (xupp - xlow) / (Double_t)np;
    
    Double_t sum = 0.;
    const Int_t n_conv = np / 2; // number of convolution loop
    for (Int_t i_conv = 0; i_conv < n_conv; i_conv++) {
      Double_t xx = xlow + i_conv * step;
      sum += TMath::Landau(xx, mpc, par[0]) * TMath::Gaus(x[0], xx, par[3]);
      
      xx = xupp - i_conv * step;
      sum += TMath::Landau(xx, mpc, par[0]) * TMath::Gaus(x[0], xx, par[3]);
    }
    
    if (np%2) { // odd number of convolution steps
      Double_t xx = xlow + n_conv * step;
      sum += TMath::Landau(xx, mpc, par[0]) * TMath::Gaus(x[0], xx, par[3]);
      
      xx = xupp - n_conv * step;
      sum += TMath::Landau(xx, mpc, par[0]) * TMath::Gaus(x[0], xx, par[3]);
    } else {
      sum += TMath::Landau(x[0], mpc, par[0]) * TMath::Gaus(x[0], x[0], par[3]);
    }
    
    return sum*step*par[2]*invsq2pi / (par[3]*par[0]);
  } // LandauGauss()


  /* fitting by using Landau-Gauss convolution (created: 2016/05/01) */
  void KanaFunctions::LandauGaussFit(TF1 *f_LG, TH1 *h)
  {
    const Double_t mip_peak    = h->GetXaxis()->GetBinCenter(h->GetMaximumBin());
    const Double_t integral    = h->GetEntries() * h->GetBinWidth(0);
    const Double_t width       = mip_peak * 0.16;
    const Double_t gauss_sigma = mip_peak * 0.1;
    f_LG->SetParameters(width, mip_peak, integral, gauss_sigma);
    
    { // 1st fit
      const Double_t fit_xmin = mip_peak - width*3;
      const Double_t fit_xmax = mip_peak + width*20;
      h->Fit(f_LG, "q", "", fit_xmin, fit_xmax);
    }
    { // 2nd fit
      const Double_t fit_xmin = f_LG->GetParameter(1)*0.8;
      const Double_t fit_xmax = f_LG->GetParameter(1)*2.5;
      h->Fit(f_LG, "q", "", fit_xmin, fit_xmax);
    }
    { // 3rd fit
      const Double_t fit_xmin = f_LG->GetParameter(1)*0.7;
      const Double_t fit_xmax = f_LG->GetParameter(1)*3.0;
      h->Fit(f_LG, "q", "", fit_xmin, fit_xmax);
    }
  } // LandauGaussFit()

  void KanaFunctions::LandauGaussFit(TF1 *f_LG, TH1 *h, const Double_t xmin, const Double_t xmax)
  {
    const Int_t xmin_bin_ID = h->GetXaxis()->FindBin(xmin);
    const Int_t xmax_bin_ID = h->GetXaxis()->FindBin(xmax);
    
    Double_t max      = 0;
    Double_t mip_peak = 0;
    for (Int_t bin_ID=xmin_bin_ID, n_bins=h->GetNbinsX();
  	 bin_ID<=xmax_bin_ID && bin_ID<=n_bins; ++bin_ID) {
      if ( (bin_ID == xmin_bin_ID) || (max < h->GetBinContent(bin_ID)) ) {
  	max      = h->GetBinContent(bin_ID);
  	mip_peak = h->GetXaxis()->GetBinCenter(bin_ID);
      }
    }
    const Double_t integral    = h->Integral(xmin_bin_ID, xmax_bin_ID) * h->GetBinWidth(0);
    const Double_t width       = mip_peak * 0.16;
    const Double_t gauss_sigma = mip_peak * 0.1;
    f_LG->SetParameters(width, mip_peak, integral, gauss_sigma);
    
    { // 1st fit
      const Double_t fit_xmin = mip_peak - width*3;
      const Double_t fit_xmax = mip_peak + width*20;
      h->Fit(f_LG, "q", "", (fit_xmin<xmin?xmin:fit_xmin), (fit_xmax>xmax?xmax:fit_xmax) );
    }
    { // 2nd fit
      const Double_t fit_xmin = f_LG->GetParameter(1)*0.8;
      const Double_t fit_xmax = f_LG->GetParameter(1)*2.5;
      h->Fit(f_LG, "q", "", (fit_xmin<xmin?xmin:fit_xmin), (fit_xmax>xmax?xmax:fit_xmax) );
    }
    { // 3rd fit
      const Double_t fit_xmin = f_LG->GetParameter(1)*0.7;
      const Double_t fit_xmax = f_LG->GetParameter(1)*3.0;
      h->Fit(f_LG, "q", "", (fit_xmin<xmin?xmin:fit_xmin), (fit_xmax>xmax?xmax:fit_xmax) );
    }
  } // LandauGaussFit()


  /* get a tree in a conv data */
  TChain* KanaFunctions::GetConvTree(const Int_t acc_run_ID, const Int_t pro_version,
  				   const Int_t run_ID, const Int_t node_ID, const Int_t file_ID,
  				   const char *name)
  {
    std::stringstream rootfile_path;
    rootfile_path << std::setfill('0')
  		  << "/home/had/togawa/work/production/"
  		  << "run" << acc_run_ID << "/pro" << pro_version << "/"
  		  << "work/fiber_out/conv_data/"
  		  << "node"  << std::setw(3) << node_ID << "/"
  		  << "run"   << std::setw(8) << run_ID  << "_conv"
  		  << "_node" << std::setw(3) << node_ID << "_file" << file_ID << ".root";
    TChain *tr = new TChain(name);
    
    tr->Add(rootfile_path.str().c_str(), -1);
    return tr;
  } // GetConvTree()

  /* get a tree in a dst data */
  TChain* KanaFunctions::GetDstTree(const Int_t acc_run_ID, const Int_t pro_version,
  				  const Int_t run_ID, const Int_t node_ID, const Int_t file_ID,
  				  const char *name)
  {
    std::stringstream rootfile_path;
    rootfile_path << std::setfill('0')
  		  << "/home/had/togawa/work/production/"
  		  << "run" << acc_run_ID << "/pro" << pro_version << "/"
  		  << "work/fiber_out/dst_data/"
  		  << "node"  << std::setw(3) << node_ID << "/"
  		  << "run"   << std::setw(8) << run_ID
  		  << "_node" << std::setw(3) << node_ID << "_file" << file_ID << ".root";
    TChain *tr = new TChain(name);

    tr->Add(rootfile_path.str().c_str(), -1);
    return tr;
  } // GetDstTree()

  /* get a tree in dst files (all nodes) */
  TChain* KanaFunctions::GetDstTree(const Int_t acc_run_ID, const Int_t pro_version, const Int_t run_ID,
  				  const char *name)
  {
    std::stringstream top_dir;
    top_dir << "/home/had/togawa/work/production/"
  		<< "run" << acc_run_ID << "/pro" << pro_version << "/"
  		<< "work/fiber_out/dst_data";

    std::vector<Int_t> node_list;
    if (GetNodeIDList(top_dir.str().c_str(), node_list) != 0) return NULL;
    
    TChain *tr = new TChain(name);
    for (Int_t i_node=0, n_nodes=node_list.size(); i_node < n_nodes; ++i_node) {
      const Int_t node_ID = node_list.at(i_node);
      std::stringstream rootfile_path;
      rootfile_path << std::setfill('0') << top_dir.str() << "/"
  		    << "node"  << std::setw(3) << node_ID << "/"
  		    << "run"   << std::setw(8) << run_ID
  		    << "_node" << std::setw(3) << node_ID << "_file*.root";
      tr->Add(rootfile_path.str().c_str());
    }

    return tr;
  }


  /* get E14 ID list of a detector for given acc. run ID & production version */
  std::vector<Int_t> KanaFunctions::GetE14IDList(const Int_t acc_run_ID, const Int_t pro_version,
  					       const std::string name)
  {
    std::vector<Int_t> e14_ID_list(0);
    KanaE14IDManager e14_ID_man(acc_run_ID, pro_version);
    if (e14_ID_man.GetE14IDList(name, e14_ID_list) != 0) {
      return std::vector<Int_t>(0);
    } else {
      return e14_ID_list;
    }
  }

  /* get maximum number of file ID for the acc run ID */
  Int_t KanaFunctions::GetMaxFileID(const Int_t acc_run_ID)
  {
    if (acc_run_ID<=63) {
      return 5;
    } else {
      return 10;
    }
  }


  /* judge good physics trig event */
  Bool_t KanaFunctions::IsGoodPhysicsEvent(const Int_t isGoodRun, const Int_t isGoodSpill,
  					 const Short_t Error, const UInt_t ScaledTrigBit)
  {
    const UInt_t trig_bit = 0x101;
    return ( (isGoodRun!=0) && (isGoodSpill!=0) && (Error==0)
  	   && ((ScaledTrigBit&trig_bit)==trig_bit) );
  }

  /* judge good normalization trig event */
  Bool_t KanaFunctions::IsGoodNormEvent(const Int_t isGoodRun, const Int_t isGoodSpill,
  				      const Short_t Error, const UInt_t ScaledTrigBit)
  {
    const UInt_t trig_bit = 0x102;
    return ( (isGoodRun!=0) && (isGoodSpill!=0) && (Error==0)
  	   && ((ScaledTrigBit&trig_bit)==trig_bit) );
  }

  /* judge good minimum bias trig event */
  Bool_t KanaFunctions::IsGoodMinBiasEvent(const Int_t isGoodRun, const Int_t isGoodSpill,
  					 const Short_t Error, const UInt_t ScaledTrigBit)
  {
    const UInt_t trig_bit = 0x104;
    return ( (isGoodRun!=0) && (isGoodSpill!=0) && (Error==0)
  	   && ((ScaledTrigBit&trig_bit)==trig_bit) );
  }


  /* draw region lines */
  void KanaFunctions::DrawCutRegion(KanaDrawManager &man,
  				  const Double_t min_Zvtx, const Double_t max_Zvtx,
  				  const Double_t min_Pt, const Double_t max_Pt)
  {
    const Double_t blind_min_pt = 120;
    const Double_t blind_max_pt = 260;
    const Double_t blind_min_z  = 2900;
    const Double_t blind_max_z  = 5100;

    TLine line;
    line.SetLineWidth(2);
    
    TBox signal_box;
    signal_box.SetFillStyle(0);
    signal_box.SetLineWidth(1);
    
    man.DrawLine(&line, blind_min_z, 0, blind_min_z, 1e6);
    man.DrawLine(&line, blind_max_z, 0, blind_max_z, 1e6);
    man.DrawLine(&line, blind_min_z, blind_min_pt, 1e6, blind_min_pt);
    man.DrawLine(&line, blind_min_z, blind_max_pt, 1e6, blind_max_pt);
    
    man.DrawBox(&signal_box, min_Zvtx, min_Pt, max_Zvtx, max_Pt);
  }

  void KanaFunctions::DrawNumbers(KanaDrawManager &man, const Double_t n_NCC,
  				const Double_t n_low_Pt, const Double_t n_blinded,
  				const Double_t n_signal, const Double_t n_high_Pt,
  				const Double_t n_down_low_Pt,
  				const Double_t n_down_middle_Pt,
  				const Double_t n_down_high_Pt,
  				const Bool_t is_blinded )
  {
    TText num;
    num.SetTextSize(0.05);
    num.SetTextAlign(22);
    
    man.DrawText(&num, 2000, 300, Form("%.3g", n_NCC));  
    man.DrawText(&num, 4000, 50,  Form("%.3g", n_low_Pt));
    if (!is_blinded) {
      man.DrawText(&num, 4000, 220, Form("%.3g", n_blinded));
      man.DrawText(&num, 4000, 190, Form("(%.3g)", n_signal));
    }
    man.DrawText(&num, 4000, 300, Form("%.3g", n_high_Pt));
    man.DrawText(&num, 6000, 50,  Form("%.3g", n_down_low_Pt));
    man.DrawText(&num, 6000, 200, Form("%.3g", n_down_middle_Pt));
    man.DrawText(&num, 6000, 300, Form("%.3g", n_down_high_Pt));
  }

  void KanaFunctions::CountNumbers(KanaDrawManager &man,
  				 std::map<Int_t, std::pair<Double_t,Double_t> > &list,
  				 const Bool_t is_blinded,
  				 const Double_t min_Zvtx, const Double_t max_Zvtx,
  				 const Double_t min_Pt, const Double_t max_Pt)
  {
    const Double_t blind_min_pt = 120;
    const Double_t blind_max_pt = 260;
    const Double_t blind_min_z  = 2900;
    const Double_t blind_max_z  = 5100;

    // upstream
    Int_t n_NCC=0, n_low_Pt=0, n_blinded=0, n_signal=0, n_high_Pt=0,
      n_down_low_Pt=0, n_down_middle_Pt=0, n_down_high_Pt=0;
    for (std::map<Int_t, std::pair<Double_t,Double_t> >::iterator itr=list.begin();
         itr != list.end(); ++itr) {
      const Double_t rec_Z = itr->second.first;
      const Double_t Pt    = itr->second.second;
      if (rec_Z<blind_min_z) {
        ++n_NCC;
      } else if (blind_min_z<=rec_Z && rec_Z<blind_max_z) {
        if (Pt<blind_min_pt) {
  	++n_low_Pt;
        } else if (blind_min_pt<=Pt && Pt<blind_max_pt) {
  	++n_blinded;
        } else {
  	++n_high_Pt;
        }
      } else {
        if (Pt<blind_min_pt) {
  	++n_down_low_Pt;
        } else if (blind_min_pt<=Pt && Pt<blind_max_pt) {
  	++n_down_middle_Pt;
        } else {
  	++n_down_high_Pt;
        }
      }
      
      if (min_Zvtx<=rec_Z && rec_Z<max_Zvtx
  	&& min_Pt<=Pt && Pt<max_Pt) ++n_signal;
    }

    DrawNumbers(man, n_NCC, n_low_Pt, n_blinded, n_signal, n_high_Pt,
  	      n_down_low_Pt, n_down_middle_Pt, n_down_high_Pt,
  	      is_blinded);
  }

  void KanaFunctions::CountNumbers(KanaDrawManager &man, const TH2 *h, const Bool_t is_blinded,
  				 const Double_t min_Zvtx, const Double_t max_Zvtx,
  				 const Double_t min_Pt, const Double_t max_Pt)
  {
    const Double_t blind_min_pt = 120;
    const Double_t blind_max_pt = 260;
    const Double_t blind_min_z  = 2900;
    const Double_t blind_max_z  = 5100;

    // upstream
    Double_t n_NCC=0., n_low_Pt=0., n_blinded=0., n_signal=0., n_high_Pt=0.,
      n_down_low_Pt=0., n_down_middle_Pt=0., n_down_high_Pt=0.;
    {
      const Int_t xmin_bin = 0;
      const Int_t xmax_bin = h->GetXaxis()->FindBin(blind_min_z)-1;
      const Int_t ymin_bin = 0;
      const Int_t ymax_bin = h->GetNbinsY()+1;
      n_NCC = h->Integral(xmin_bin, xmax_bin, ymin_bin, ymax_bin);
    }
    {
      const Int_t xmin_bin = h->GetXaxis()->FindBin(blind_min_z);
      const Int_t xmax_bin = h->GetXaxis()->FindBin(blind_max_z)-1;
      const Int_t ymin_bin = 0;
      const Int_t ymax_bin = h->GetYaxis()->FindBin(blind_min_pt)-1;
      n_low_Pt = h->Integral(xmin_bin, xmax_bin, ymin_bin, ymax_bin);
    }
    if (!is_blinded) {
      {
        const Int_t xmin_bin = h->GetXaxis()->FindBin(blind_min_z);
        const Int_t xmax_bin = h->GetXaxis()->FindBin(blind_max_z)-1;
        const Int_t ymin_bin = h->GetYaxis()->FindBin(blind_min_pt);
        const Int_t ymax_bin = h->GetYaxis()->FindBin(blind_max_pt)-1;
        n_blinded = h->Integral(xmin_bin, xmax_bin, ymin_bin, ymax_bin);
      }
      {
        const Int_t xmin_bin = h->GetXaxis()->FindBin(min_Zvtx);
        const Int_t xmax_bin = h->GetXaxis()->FindBin(max_Zvtx)-1;
        const Int_t ymin_bin = h->GetYaxis()->FindBin(min_Pt);
        const Int_t ymax_bin = h->GetYaxis()->FindBin(max_Pt)-1;
        n_signal = h->Integral(xmin_bin, xmax_bin, ymin_bin, ymax_bin);
      }
    }
    {
      const Int_t xmin_bin = h->GetXaxis()->FindBin(blind_min_z);
      const Int_t xmax_bin = h->GetXaxis()->FindBin(blind_max_z)-1;
      const Int_t ymin_bin = h->GetYaxis()->FindBin(blind_max_pt);
      const Int_t ymax_bin = h->GetNbinsY();
      n_high_Pt = h->Integral(xmin_bin, xmax_bin, ymin_bin, ymax_bin);
    }
    {
      const Int_t xmin_bin = h->GetXaxis()->FindBin(blind_max_z);
      const Int_t xmax_bin = h->GetNbinsX();
      const Int_t ymin_bin = 0;
      const Int_t ymax_bin = h->GetYaxis()->FindBin(blind_min_pt)-1;
      n_down_low_Pt = h->Integral(xmin_bin, xmax_bin, ymin_bin, ymax_bin);
    }
    {
      const Int_t xmin_bin = h->GetXaxis()->FindBin(blind_max_z);
      const Int_t xmax_bin = h->GetNbinsX();
      const Int_t ymin_bin = h->GetYaxis()->FindBin(blind_min_pt);
      const Int_t ymax_bin = h->GetYaxis()->FindBin(blind_max_pt)-1;
      n_down_middle_Pt = h->Integral(xmin_bin, xmax_bin, ymin_bin, ymax_bin);
    }
    {
      const Int_t xmin_bin = h->GetXaxis()->FindBin(blind_max_z);
      const Int_t xmax_bin = h->GetNbinsX();
      const Int_t ymin_bin = h->GetYaxis()->FindBin(blind_max_pt);
      const Int_t ymax_bin = h->GetNbinsY();
      n_down_high_Pt = h->Integral(xmin_bin, xmax_bin, ymin_bin, ymax_bin);
    }

    DrawNumbers(man, n_NCC, n_low_Pt, n_blinded, n_signal, n_high_Pt,
  	      n_down_low_Pt, n_down_middle_Pt, n_down_high_Pt,
  	      is_blinded);
  }

  void KanaFunctions::CountNumbers(KanaDrawManager &man,
  				 const TGraph *g, const Bool_t is_blinded,
  				 const Double_t min_Zvtx, const Double_t max_Zvtx,
  				 const Double_t min_Pt, const Double_t max_Pt)
  {
    const Double_t blind_min_pt = 120;
    const Double_t blind_max_pt = 260;
    const Double_t blind_min_z  = 2900;
    const Double_t blind_max_z  = 5100;

    // upstream
    Int_t n_NCC=0, n_low_Pt=0, n_blinded=0, n_signal=0, n_high_Pt=0,
      n_down_low_Pt=0, n_down_middle_Pt=0, n_down_high_Pt=0;
    for (Int_t i_point=0, n_points=g->GetN(); i_point < n_points; ++i_point) {
      const Double_t rec_Z = g->GetX()[i_point];
      const Double_t Pt    = g->GetY()[i_point];
      if (rec_Z<blind_min_z) {
        ++n_NCC;
      } else if (blind_min_z<=rec_Z && rec_Z<blind_max_z) {
        if (Pt<blind_min_pt) {
  	++n_low_Pt;
        } else if (blind_min_pt<=Pt && Pt<blind_max_pt) {
  	++n_blinded;
        } else {
  	++n_high_Pt;
        }
      } else {
        if (Pt<blind_min_pt) {
  	++n_down_low_Pt;
        } else if (blind_min_pt<=Pt && Pt<blind_max_pt) {
  	++n_down_middle_Pt;
        } else {
  	++n_down_high_Pt;
        }
      }
      
      if (min_Zvtx<=rec_Z && rec_Z<max_Zvtx
  	&& min_Pt<=Pt && Pt<max_Pt) ++n_signal;
    }

    DrawNumbers(man, n_NCC, n_low_Pt, n_blinded, n_signal, n_high_Pt,
  	      n_down_low_Pt, n_down_middle_Pt, n_down_high_Pt,
  	      is_blinded);
  }

}
