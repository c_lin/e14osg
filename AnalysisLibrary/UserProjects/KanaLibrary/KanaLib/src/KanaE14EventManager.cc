#include "KanaLib/KanaE14EventManager.h"

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaE14EventManager)

namespace KanaLibrary
{

  KanaE14EventManager::KanaE14EventManager()
  {
    InitKanaE14EventManager();
  }


  void KanaE14EventManager::InitKanaE14EventManager()
  {
    EventNo         = 0;
    L1TrigNo        = 0;
    L2TrigNo        = 0;
    SpillNo         = 0;
    TimeStamp       = 0;
    L2TimeStamp     = 0;
    Error           = 0;
    TrigTagMismatch = 0;
    L2AR            = 0;
    DetectorBit     = 0;
    RawTrigBit      = 0;
    ScaledTrigBit   = 0;
  }


  void KanaE14EventManager::SetBranchAddresses(TTree *tr)
  {
    KanaFunctions::ActivateBranch(tr, "EventNo",         &EventNo        );
    KanaFunctions::ActivateBranch(tr, "L1TrigNo",        &L1TrigNo       );
    KanaFunctions::ActivateBranch(tr, "L2TrigNo",        &L2TrigNo       );
    KanaFunctions::ActivateBranch(tr, "SpillNo",         &SpillNo        );
    KanaFunctions::ActivateBranch(tr, "TimeStamp",       &TimeStamp      );
    KanaFunctions::ActivateBranch(tr, "L2TimeStamp",     &L2TimeStamp    );
    KanaFunctions::ActivateBranch(tr, "Error",           &Error          );
    KanaFunctions::ActivateBranch(tr, "TrigTagMismatch", &TrigTagMismatch);
    KanaFunctions::ActivateBranch(tr, "L2AR",            &L2AR           );
    KanaFunctions::ActivateBranch(tr, "DetectorBit",     &DetectorBit    );
    KanaFunctions::ActivateBranch(tr, "RawTrigBit",      &RawTrigBit     );
    KanaFunctions::ActivateBranch(tr, "ScaledTrigBit",   &ScaledTrigBit  );
  }


  KanaE14EventManager::~KanaE14EventManager()
  {
  }
  
}
