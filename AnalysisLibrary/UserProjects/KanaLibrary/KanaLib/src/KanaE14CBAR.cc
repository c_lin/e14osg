#include "KanaLib/KanaE14CBAR.h"

#include <iostream>
#include <iomanip>
#include <sstream>

#include <TGraphErrors.h>
#include <TMath.h>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaE14CBAR)

namespace KanaLibrary
{
  
  KanaE14CBAR::KanaE14CBAR()
    : KanaE14Detector125MHz(),
      
      /* geometrical information */
      // taken mainly from gsim inputs
      //   - InnerRadius: inner raidus of CBAR
      //   - OuterRadius: outer raidus of CBAR
      //   - InnerHitRadius: center radius of CBAR inner module
      //   - OuterHitRadius: center radius of CBAR outer module
      InnerRadius    ( 1019.                     ), // [mm]
      InnerThickness ( (1337.-1019)/3.           ), // [mm]
      InnerWidth     ( 210                       ), // [mm]
      OuterRadius    ( 1337.                     ), // [mm]
      OuterThickness ( (1337.-1019)*2./3.        ), // [mm]
      OuterWidth     ( 240                       ), // [mm]
      InnerHitRadius ( 1019.+(1337.-1019.)/6.    ), // [mm]
      OuterHitRadius ( 1019.+(1337.-1019.)*2./3. ), // [mm]
      ModuleLength   ( 5500.                     ), // [mm]
      
      ModuleCenterZ  ( 4100.5 ),
      
      /* detector basic performance */
      // taken from NIM A 592 (2008) 261-272 (CBAR paper)
      LightPropSpeed     ( 175. ), // [mm/ns]
      InnerPosResolution ( 52.  ),  // [mm]
      OuterPosResolution ( 38.  )  // [mm]
  {
    InitKanaE14CBAR();
  }

  KanaE14CBAR::KanaE14CBAR(const Int_t acc_run_ID, const Int_t pro_version)
    : KanaE14Detector125MHz(acc_run_ID, pro_version, "CBAR"),
      
      /* geometrical information */
      // taken mainly from gsim inputs
      //   - InnerRadius: inner raidus of CBAR
      //   - OuterRadius: outer raidus of CBAR
      //   - InnerHitRadius: center radius of CBAR inner module
      //   - OuterHitRadius: center radius of CBAR outer module
      InnerRadius    ( 1019.                     ), // [mm]
      InnerThickness ( (1337.-1019)/3.           ), // [mm]
      InnerWidth     ( 210                       ), // [mm]
      OuterRadius    ( 1337.                     ), // [mm]
      OuterThickness ( (1337.-1019)*2./3.        ), // [mm]
      OuterWidth     ( 240                       ), // [mm]
      InnerHitRadius ( 1019.+(1337.-1019.)/6.    ), // [mm]
      OuterHitRadius ( 1019.+(1337.-1019.)*2./3. ), // [mm]
      ModuleLength   ( 5500.                     ), // [mm]
      
      ModuleCenterZ  ( 4100.5 ),
      
      /* detector basic performance */
      // taken from NIM A 592 (2008) 261-272 (CBAR paper)
      LightPropSpeed     ( 175. ), // [mm/ns]
      InnerPosResolution ( 52.  ),  // [mm]
      OuterPosResolution ( 38.  )  // [mm]
  {
    InitKanaE14CBAR();
    
    SetModulePositionMap();
    
    // allocate memories
    ModulePeak          = new Short_t [nModules][2];
    ModuleIntegratedADC = new Float_t [nModules][2];
    ModuleInitialTime   = new Float_t [nModules][2];
    
    ModuleSumIntegratedADC      = new Float_t [nModules];
    ModuleInitialTimeAverage    = new Float_t [nModules];
    ModuleInitialTimeDifference = new Float_t [nModules];
    
    ModuleEne       = new Float_t [nModules];
    ModuleHitTime   = new Float_t [nModules];
    ModuleDeltaTime = new Float_t [nModules];
    
    ModuleHitX = new Float_t [nModules];
    ModuleHitY = new Float_t [nModules];
    ModuleHitZ = new Float_t [nModules];

    CosmicModuleModID          = new Int_t   [nModules];
    CosmicModuleEne            = new Float_t [nModules];
    CosmicModuleTimeAverage    = new Float_t [nModules];
    CosmicModuleTimeDifference = new Float_t [nModules];
    CosmicModuleHitX           = new Float_t [nModules];
    CosmicModuleHitY           = new Float_t [nModules];
    CosmicModuleHitZ           = new Float_t [nModules];
    CosmicModuleHitEX          = new Float_t [nModules];
    CosmicModuleHitEY          = new Float_t [nModules];
    CosmicModuleHitEZ          = new Float_t [nModules];
    
    // set linear functions for cosmic ray tracking
    // f_HoughLine = new TF1(std::string("f_HoughLine_"+Name).c_str(),
    // 			"[0]*cos(x)+[1]*sin(x)", 0, TMath::TwoPi());
    
    // h_HoughSpaceXY = new TH2D(std::string("h_HoughSpace"+Name).c_str(),
    // 			    "Hough Space (xy-plane);#theta [radian];r [mm]",
    // 			    100, 0, TMath::TwoPi(), 100, 0, 125, 2500);
    // h_HoughSpaceZY = new TH2D(std::string("h_HoughSpace"+Name).c_str(),
    // 			    "Hough Space (zy-plane);#theta [radian];r [mm]",
    // 			    100, 0, TMath::TwoPi(), 100, 0, 125, 2500);
    // h_HoughSpaceTY = new TH2D(std::string("h_HoughSpace"+Name).c_str(),
    // 			    "Hough Space (ty-plane);#theta [radian];r [mm]",
    // 			    100, 0, TMath::TwoPi(), 100, 0, 125, 2500);
    
    f_CosmicTrack = new TF1("f_CosmicTrack", "pol1");
    // f_CosmicTrackXY = new TF1("f_CosmicTrackXY", "pol1");
    // f_CosmicTrackZY = new TF1("f_CosmicTrackZY", "pol1");
    // f_CosmicTrackTY = new TF1("f_CosmicTrackTY", "pol1");
  }

  void KanaE14CBAR::InitKanaE14CBAR()
  {
    ModulePeak          = NULL;
    ModuleIntegratedADC = NULL;
    ModuleInitialTime   = NULL;
    
    ModuleSumIntegratedADC      = NULL;
    ModuleInitialTimeAverage    = NULL;
    ModuleInitialTimeDifference = NULL;

    ModuleEne       = NULL;
    ModuleHitTime   = NULL;
    ModuleDeltaTime = NULL;

    ModuleHitX = NULL;
    ModuleHitY = NULL;
    ModuleHitZ = NULL;

    CosmicModuleNumber         = 0;
    CosmicModuleModID          = NULL;
    CosmicModuleEne            = NULL;
    CosmicModuleTimeAverage    = NULL;
    CosmicModuleTimeDifference = NULL;
    CosmicModuleHitX           = NULL;
    CosmicModuleHitY           = NULL;
    CosmicModuleHitZ           = NULL;
    CosmicModuleHitEX          = NULL;
    CosmicModuleHitEY          = NULL;
    CosmicModuleHitEZ          = NULL;

    CosmicEnergyThreshold = 0.;
    CosmicInterceptXY     = 0.;
    CosmicSlopeXY         = 0.;
    CosmicInterceptZY     = 0.;
    CosmicSlopeZY         = 0.;
    CosmicInterceptTY     = 0.;
    CosmicSlopeTY         = 0.;
    
    f_CosmicTrack = NULL;
    // f_CosmicTrackXY = NULL;
    // f_CosmicTrackZY = NULL;
    // f_CosmicTrackZT = NULL;
  }


  void KanaE14CBAR::SetModulePositionMap()
  {
    const Double_t delta_theta = TMath::TwoPi() / 32.;
    
    for (Int_t i_mod=0, n_ch=ModuleIDList.size(); i_mod < n_ch; ++i_mod) {
      
      const Int_t module_ID = ModuleIDList.at(i_mod);
      
      const Int_t    index  = (module_ID&0x1f);
      const Double_t radius = ((module_ID<32) ? InnerHitRadius : OuterHitRadius);
      const Double_t thick  = ((module_ID<32) ? InnerThickness : OuterThickness);
      const Double_t width  = ((module_ID<32) ? InnerWidth     : OuterWidth    );
      
      PositionMap[module_ID].first  = radius * TMath::Cos(delta_theta*index);
      PositionMap[module_ID].second = radius * TMath::Sin(delta_theta*index);
      
      PosErrorMap[module_ID].first
        = TMath::Sqrt(TMath::Power(thick*0.5*TMath::Cos(delta_theta*index), 2)
  		    + TMath::Power(width*0.5*TMath::Sin(delta_theta*index), 2)
  		    ) / TMath::Sqrt(12.);
      PosErrorMap[module_ID].second
        = TMath::Sqrt(TMath::Power(thick*0.5*TMath::Sin(delta_theta*index), 2)
  		    + TMath::Power(width*0.5*TMath::Cos(delta_theta*index), 2)
  		    ) / TMath::Sqrt(12.);
    }
  }


  Double_t KanaE14CBAR::GetPosX(const Int_t module_ID)
  {
    if (PositionMap.find(module_ID) != PositionMap.end()) {
      return PositionMap[module_ID].first;
    } else {
      return 0;
    }
  }

  Double_t KanaE14CBAR::GetPosY(const Int_t module_ID)
  {
    if (PositionMap.find(module_ID) != PositionMap.end()) {
      return PositionMap[module_ID].second;
    } else {
      return 0;
    }
  }


  void KanaE14CBAR::SetBranchAddresses(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branch addresses: " << Name << std::endl;
    
    // set branch addresses of KanaE14DetectorBase
    SetBranchAddressesBase(tr, b_list);

    // set branch addresses of KanaE14Detector125MHz
    SetBranchAddresses125MHz(tr, b_list);

    // set branch addresses of KanaE14CBAR
    SetBranchAddressesCBAR(tr, b_list);
  }

  void KanaE14CBAR::SetBranchAddressesCBAR(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "ModulePeak") != end) {
      const std::string bname = Name + "ModulePeak";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModulePeak);
      std::cout << "     -- ModulePeak: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleIntegratedADC") != end) {
      const std::string bname = Name + "ModuleIntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleIntegratedADC);
      std::cout << "     -- ModuleIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTime") != end) {
      const std::string bname = Name + "ModuleInitialTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleInitialTime);
      std::cout << "     -- ModuleInitialTime: enabled" << std::endl;
    }

    if (std::find(begin, end, "ModuleSumIntegratedADC") != end) {
      const std::string bname = Name + "ModuleSumIntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleSumIntegratedADC);
      std::cout << "     -- ModuleSumIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTimeAverage") != end) {
      const std::string bname = Name + "ModuleInitialTimeAverage";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleInitialTimeAverage);
      std::cout << "     -- ModuleInitialTimeAverage: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTimeDifference") != end) {
      const std::string bname = Name + "ModuleInitialTimeDifference";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleInitialTimeDifference);
      std::cout << "     -- ModuleInitialTimeDifference: enabled" << std::endl;
    }

    if (std::find(begin, end, "ModuleEne") != end) {
      const std::string bname = Name + "ModuleEne";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleEne);
      std::cout << "     -- ModuleEne: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleHitTime") != end) {
      const std::string bname = Name + "ModuleHitTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleHitTime);
      std::cout << "     -- ModuleHitTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleDeltaTime") != end) {
      const std::string bname = Name + "ModuleDeltaTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleDeltaTime);
      std::cout << "     -- ModuleDeltaTime: enabled" << std::endl;
    }

    if (std::find(begin, end, "ModuleHitX") != end) {
      const std::string bname = Name + "ModuleHitX";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleHitX);
      std::cout << "     -- ModuleHitX: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleHitY") != end) {
      const std::string bname = Name + "ModuleHitY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleHitY);
      std::cout << "     -- ModuleHitY: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleHitZ") != end) {
      const std::string bname = Name + "ModuleHitZ";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleHitZ);
      std::cout << "     -- ModuleHitZ: enabled" << std::endl;
    }


    if (std::find(begin, end, "CosmicModuleNumber") != end) {
      const std::string bname = Name + "CosmicModuleNumber";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicModuleNumber);
      std::cout << "     -- CosmicModuleNumber: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleModID") != end) {
      const std::string bname = Name + "CosmicModuleModID";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleModID);
      std::cout << "     -- CosmicModuleModID: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleEne") != end) {
      const std::string bname = Name + "CosmicModuleEne";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleEne);
      std::cout << "     -- CosmicModuleEne: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleTimeAverage") != end) {
      const std::string bname = Name + "CosmicModuleTimeAverage";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleTimeAverage);
      std::cout << "     -- CosmicModuleTimeAverage: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleTimeDifference") != end) {
      const std::string bname = Name + "CosmicModuleTimeDifference";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleTimeDifference);
      std::cout << "     -- CosmicModuleTimeDifference: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitX") != end) {
      const std::string bname = Name + "CosmicModuleHitX";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleHitX);
      std::cout << "     -- CosmicModuleHitX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitY") != end) {
      const std::string bname = Name + "CosmicModuleHitY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleHitY);
      std::cout << "     -- CosmicModuleHitY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitZ") != end) {
      const std::string bname = Name + "CosmicModuleHitZ";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleHitZ);
      std::cout << "     -- CosmicModuleHitZ: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitEX") != end) {
      const std::string bname = Name + "CosmicModuleHitEX";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleHitEX);
      std::cout << "     -- CosmicModuleHitEX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitEY") != end) {
      const std::string bname = Name + "CosmicModuleHitEY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleHitEY);
      std::cout << "     -- CosmicModuleHitEY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitEZ") != end) {
      const std::string bname = Name + "CosmicModuleHitEZ";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), CosmicModuleHitEZ);
      std::cout << "     -- CosmicModuleHitEZ: enabled" << std::endl;
    }


    if (std::find(begin, end, "CosmicEnergyThreshold") != end) {
      const std::string bname = Name + "CosmicEnergyThreshold";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicEnergyThreshold);
      std::cout << "     -- CosmicEnergyThreshold: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicInterceptXY") != end) {
      const std::string bname = Name + "CosmicInterceptXY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicInterceptXY);
      std::cout << "     -- CosmicInterceptXY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicSlopeXY") != end) {
      const std::string bname = Name + "CosmicSlopeXY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicSlopeXY);
      std::cout << "     -- CosmicSlopeXY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicInterceptZY") != end) {
      const std::string bname = Name + "CosmicInterceptZY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicInterceptZY);
      std::cout << "     -- CosmicInterceptZY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicSlopeZY") != end) {
      const std::string bname = Name + "CosmicSlopeZY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicSlopeZY);
      std::cout << "     -- CosmicSlopeZY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicInterceptTY") != end) {
      const std::string bname = Name + "CosmicInterceptTY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicInterceptTY);
      std::cout << "     -- CosmicInterceptTY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicSlopeTY") != end) {
      const std::string bname = Name + "CosmicSlopeTY";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &CosmicSlopeTY);
      std::cout << "     -- CosmicSlopeTY: enabled" << std::endl;
    }
    
    
  }


  void KanaE14CBAR::AddBranches(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branches: " << Name << std::endl;

    // add branches of KanaE14DetectorBase
    AddBranchesBase(tr, b_list);

    // add branches of KanaE14Detector125MHz
    AddBranches125MHz(tr, b_list);
    
    // add branches of KanaE14CBAR
    AddBranchesCBAR(tr, b_list);
  }


  void KanaE14CBAR::AddBranchesCBAR(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "ModulePeak") != end) {
      const std::string bname = Name + "ModulePeak";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber][2]/S";
      tr->Branch(bname.c_str(), ModulePeak, leaflist.str().c_str());
      std::cout << "     -- ModulePeak: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleIntegratedADC") != end) {
      const std::string bname = Name + "ModuleIntegratedADC";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber][2]/F";
      tr->Branch(bname.c_str(), ModuleIntegratedADC, leaflist.str().c_str());
      std::cout << "     -- ModuleIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTime") != end) {
      const std::string bname = Name + "ModuleInitialTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber][2]/F";
      tr->Branch(bname.c_str(), ModuleInitialTime, leaflist.str().c_str());
      std::cout << "     -- ModuleInitialTime: enabled" << std::endl;
    }
    
    if (std::find(begin, end, "ModuleSumIntegratedADC") != end) {
      const std::string bname = Name + "ModuleSumIntegratedADC";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleSumIntegratedADC, leaflist.str().c_str());
      std::cout << "     -- ModuleSumIntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTimeAverage") != end) {
      const std::string bname = Name + "ModuleInitialTimeAverage";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleInitialTimeAverage, leaflist.str().c_str());
      std::cout << "     -- ModuleInitialTimeAverage: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleInitialTimeDifference") != end) {
      const std::string bname = Name + "ModuleInitialTimeDifference";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleInitialTimeDifference, leaflist.str().c_str());
      std::cout << "     -- ModuleInitialTimeDifference: enabled" << std::endl;
    }

    if (std::find(begin, end, "ModuleEne") != end) {
      const std::string bname = Name + "ModuleEne";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleEne, leaflist.str().c_str());
      std::cout << "     -- ModuleEne: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleHitTime") != end) {
      const std::string bname = Name + "ModuleHitTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleHitTime, leaflist.str().c_str());
      std::cout << "     -- ModuleHitTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleDeltaTime") != end) {
      const std::string bname = Name + "ModuleDeltaTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleDeltaTime, leaflist.str().c_str());
      std::cout << "     -- ModuleDeltaTime: enabled" << std::endl;
    }

    if (std::find(begin, end, "ModuleHitX") != end) {
      const std::string bname = Name + "ModuleHitX";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleHitX, leaflist.str().c_str());
      std::cout << "     -- ModuleHitX: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleHitY") != end) {
      const std::string bname = Name + "ModuleHitY";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleHitY, leaflist.str().c_str());
      std::cout << "     -- ModuleHitY: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleHitZ") != end) {
      const std::string bname = Name + "ModuleHitZ";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/F";
      tr->Branch(bname.c_str(), ModuleHitZ, leaflist.str().c_str());
      std::cout << "     -- ModuleHitZ: enabled" << std::endl;
    }


    if (std::find(begin, end, "CosmicModuleNumber") != end) {
      const std::string bname = Name + "CosmicModuleNumber";
      std::stringstream leaflist;
      leaflist << bname << "/I";
      tr->Branch(bname.c_str(), &CosmicModuleNumber, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleNumber: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleModID") != end) {
      const std::string bname = Name + "CosmicModuleModID";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/I";
      tr->Branch(bname.c_str(), CosmicModuleModID, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleModID: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleEne") != end) {
      const std::string bname = Name + "CosmicModuleEne";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/F";
      tr->Branch(bname.c_str(), CosmicModuleEne, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleEne: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleTimeAverage") != end) {
      const std::string bname = Name + "CosmicModuleTimeAverage";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/F";
      tr->Branch(bname.c_str(), CosmicModuleTimeAverage, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleTimeAverage: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleTimeDifference") != end) {
      const std::string bname = Name + "CosmicModuleTimeDifference";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/F";
      tr->Branch(bname.c_str(), CosmicModuleTimeDifference, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleTimeDifference: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitX") != end) {
      const std::string bname = Name + "CosmicModuleHitX";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/F";
      tr->Branch(bname.c_str(), CosmicModuleHitX, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleHitX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitY") != end) {
      const std::string bname = Name + "CosmicModuleHitY";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/F";
      tr->Branch(bname.c_str(), CosmicModuleHitY, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleHitY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitZ") != end) {
      const std::string bname = Name + "CosmicModuleHitZ";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/F";
      tr->Branch(bname.c_str(), CosmicModuleHitZ, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleHitZ: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitEX") != end) {
      const std::string bname = Name + "CosmicModuleHitEX";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/F";
      tr->Branch(bname.c_str(), CosmicModuleHitEX, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleHitEX: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitEY") != end) {
      const std::string bname = Name + "CosmicModuleHitEY";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/F";
      tr->Branch(bname.c_str(), CosmicModuleHitEY, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleHitEY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicModuleHitEZ") != end) {
      const std::string bname = Name + "CosmicModuleHitEZ";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "CosmicModuleNumber]/F";
      tr->Branch(bname.c_str(), CosmicModuleHitEZ, leaflist.str().c_str());
      std::cout << "     -- CosmicModuleHitEZ: enabled" << std::endl;
    }


    if (std::find(begin, end, "CosmicEnergyThreshold") != end) {
      const std::string bname = Name + "CosmicEnergyThreshold";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicEnergyThreshold, leaflist.str().c_str());
      std::cout << "     -- CosmicEnergyThreshold: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicInterceptXY") != end) {
      const std::string bname = Name + "CosmicInterceptXY";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicInterceptXY, leaflist.str().c_str());
      std::cout << "     -- CosmicInterceptXY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicSlopeXY") != end) {
      const std::string bname = Name + "CosmicSlopeXY";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicSlopeXY, leaflist.str().c_str());
      std::cout << "     -- CosmicSlopeXY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicInterceptZY") != end) {
      const std::string bname = Name + "CosmicInterceptZY";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicInterceptZY, leaflist.str().c_str());
      std::cout << "     -- CosmicInterceptZY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicSlopeZY") != end) {
      const std::string bname = Name + "CosmicSlopeZY";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicSlopeZY, leaflist.str().c_str());
      std::cout << "     -- CosmicSlopeZY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicInterceptTY") != end) {
      const std::string bname = Name + "CosmicInterceptTY";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicInterceptTY, leaflist.str().c_str());
      std::cout << "     -- CosmicInterceptTY: enabled" << std::endl;
    }
    if (std::find(begin, end, "CosmicSlopeTY") != end) {
      const std::string bname = Name + "CosmicSlopeTY";
      std::stringstream leaflist;
      leaflist << bname << "/F";
      tr->Branch(bname.c_str(), &CosmicSlopeTY, leaflist.str().c_str());
      std::cout << "     -- CosmicSlopeTY: enabled" << std::endl;
    }
  }


  void KanaE14CBAR::CalcCosmicTrack(const Double_t energy_threshold)
  {
    CosmicEnergyThreshold = energy_threshold;
      
    /* check hit modules */
    std::map<Int_t, Int_t> num_map;
    for (Int_t num=0; num < Number; ++num) {
      num_map[ModID[num]] = num;
    }
    std::map<Int_t, Int_t>::iterator num_map_end = num_map.end();
    
    // fill data
    CosmicModuleNumber = 0;
    // std::map<Int_t, Int_t> module_num_map;
    for (Int_t i_mod=0; i_mod < nModules; ++i_mod) {
      
      const Int_t module_ID = ModuleIDList.at(i_mod);
      
      if ( (num_map.find(module_ID) == num_map_end)
  	 || (num_map.find(module_ID+100) == num_map_end) ) continue;

      const Int_t num_up   = num_map[module_ID];
      const Int_t num_down = num_map[module_ID+100];
      
      if (Ene[num_up]+Ene[num_down] < CosmicEnergyThreshold) continue;
      
      const Double_t z_pos
        = (Time[num_up]-Time[num_down])*(FADCSampleInterval*1e9)*LightPropSpeed*0.5;
      if (z_pos<-2500 || z_pos>2500) continue; // out of range
      
      // module_num_map[module_ID] = CosmicModuleNumber;
      
      CosmicModuleModID[CosmicModuleNumber] = module_ID;
      CosmicModuleEne[CosmicModuleNumber]   = Ene[num_up] + Ene[num_down];
      
      CosmicModuleTimeAverage[CosmicModuleNumber]    = (Time[num_up] + Time[num_down])*0.5;
      CosmicModuleTimeDifference[CosmicModuleNumber] = Time[num_up] - Time[num_down];
      
      CosmicModuleHitX[CosmicModuleNumber]  = PositionMap[module_ID].first;
      CosmicModuleHitEX[CosmicModuleNumber] = PosErrorMap[module_ID].first;
      
      CosmicModuleHitY[CosmicModuleNumber]  = PositionMap[module_ID].second;
      CosmicModuleHitEY[CosmicModuleNumber] = PosErrorMap[module_ID].second;
      
      CosmicModuleHitZ[CosmicModuleNumber] = z_pos + ModuleCenterZ;
      CosmicModuleHitEZ[CosmicModuleNumber]
        = (module_ID<100 ? InnerPosResolution : OuterPosResolution);
      
      ++CosmicModuleNumber;
    }

    if (CosmicModuleNumber<=1) {
      // DumpWarning("KanaE14CBAR::CalcCosmicTrack", "too small number of hits (<= 1 module)");

      CosmicInterceptXY = 0.;
      CosmicSlopeXY     = 0.;
      CosmicInterceptZY = 0.;
      CosmicSlopeZY     = 0.;
      CosmicInterceptTY = 0.;
      CosmicSlopeTY     = 0.;
      
      return;
    }
    
    /* get track information in each plane */
    // xy-plane
    GetCosmicTrack(CosmicModuleNumber,
  		 CosmicModuleHitX, CosmicModuleHitY, CosmicModuleHitEX, CosmicModuleHitEY,
  		 CosmicInterceptXY, CosmicSlopeXY);
    
    // zy-plane
    GetCosmicTrack(CosmicModuleNumber,
  		 CosmicModuleHitZ, CosmicModuleHitY, CosmicModuleHitEZ, CosmicModuleHitEY,
  		 CosmicInterceptZY, CosmicSlopeZY);
    
    // ty-plane
    GetCosmicTrack(CosmicModuleNumber,
  		 CosmicModuleTimeAverage, CosmicModuleHitY, NULL, CosmicModuleHitEY,
  		 CosmicInterceptTY, CosmicSlopeTY);
  }

  void KanaE14CBAR::CalcCosmicTrack(const Double_t energy_threshold,
  				  const Double_t intercept_XY, const Double_t slope_XY)
  {
    CosmicEnergyThreshold = energy_threshold;

    const Double_t delta_theta  = TMath::TwoPi()/32.;
    const Double_t theta_offset = delta_theta*0.5;
    
    const Double_t a = slope_XY;
    const Double_t b = intercept_XY;
    
    std::vector<Int_t> candidate_module_ID;
    { // inner module hit position (intersection of cosmic track w/ CBAR inner ring middle)
      const Double_t r = InnerHitRadius;
      const Double_t x[2] = {
        ( -(a*b)+TMath::Sqrt(a*a*b*b-(1+a*a)*(b*b-r*r)) ) / (1+a*a),
        ( -(a*b)-TMath::Sqrt(a*a*b*b-(1+a*a)*(b*b-r*r)) ) / (1+a*a)
      };
      for (Int_t i=0; i<2; ++i) {
        const Double_t y         = a*x[i]+b;
        const Double_t theta     = GetTheta(x[i], y);
        const Int_t    module_ID = static_cast<Int_t>((theta+theta_offset)/delta_theta);
        // std::cout << theta/TMath::TwoPi()*360. << " " << module_ID << std::endl;
        candidate_module_ID.push_back(module_ID);
      }
    }
    { // outer module hit position (intersection of cosmic track w/ CBAR outer ring middle)
      const Double_t r = OuterHitRadius;
      const Double_t x[2] = {
        ( -(a*b)+TMath::Sqrt(a*a*b*b-(1+a*a)*(b*b-r*r)) ) / (1+a*a),
        ( -(a*b)-TMath::Sqrt(a*a*b*b-(1+a*a)*(b*b-r*r)) ) / (1+a*a)
      };
      for (Int_t i=0; i<2; ++i) {
        const Double_t y         = a*x[i]+b;
        const Double_t theta     = GetTheta(x[i], y);
        const Int_t    module_ID = static_cast<Int_t>((theta+theta_offset)/delta_theta) + 32;
        // std::cout << theta/TMath::TwoPi()*360. << " " << module_ID << std::endl;
        candidate_module_ID.push_back(module_ID);
      }
    }
    
    /* check hit modules */
    std::map<Int_t, Int_t> num_map;
    for (Int_t num=0; num < Number; ++num) {
      // std::cout << ModID[num] << std::endl;
      num_map[ModID[num]] = num;
    }
    std::map<Int_t, Int_t>::iterator num_map_end = num_map.end();
    
    // fill data
    CosmicModuleNumber = 0;
    // std::map<Int_t, Int_t> module_num_map;
    for (Int_t i_mod=0, n_mod=candidate_module_ID.size(); i_mod < n_mod; ++i_mod) {
      
      const Int_t module_ID = candidate_module_ID.at(i_mod);
      
      if ( (num_map.find(module_ID) == num_map_end)
  	 || (num_map.find(module_ID+100) == num_map_end) ) continue;
      
      const Int_t num_up   = num_map[module_ID];
      const Int_t num_down = num_map[module_ID+100];
      
      if (Ene[num_up]+Ene[num_down] < CosmicEnergyThreshold) continue;
      
      const Double_t z_pos
        = (Time[num_up]-Time[num_down])*(FADCSampleInterval*1e9)*LightPropSpeed*0.5;
      if (z_pos<-2500 || z_pos>2500) continue; // out of range
      
      // module_num_map[module_ID] = CosmicModuleNumber;
      
      CosmicModuleModID[CosmicModuleNumber] = module_ID;
      CosmicModuleEne[CosmicModuleNumber]   = Ene[num_up] + Ene[num_down];
      
      CosmicModuleTimeAverage[CosmicModuleNumber]    = (Time[num_up] + Time[num_down])*0.5;
      CosmicModuleTimeDifference[CosmicModuleNumber] = Time[num_up] - Time[num_down];
      
      CosmicModuleHitX[CosmicModuleNumber]  = PositionMap[module_ID].first;
      CosmicModuleHitEX[CosmicModuleNumber] = PosErrorMap[module_ID].first;
      
      CosmicModuleHitY[CosmicModuleNumber]  = PositionMap[module_ID].second;
      CosmicModuleHitEY[CosmicModuleNumber] = PosErrorMap[module_ID].second;
      
      CosmicModuleHitZ[CosmicModuleNumber] = z_pos + ModuleCenterZ;
      CosmicModuleHitEZ[CosmicModuleNumber]
        = (module_ID<100 ? InnerPosResolution : OuterPosResolution);
      
      ++CosmicModuleNumber;
    }

    if (CosmicModuleNumber<=1) {
      // DumpWarning("KanaE14CBAR::CalcCosmicTrack", "too small number of hits (<= 1 module)");

      CosmicInterceptXY = 0.;
      CosmicSlopeXY     = 0.;
      CosmicInterceptZY = 0.;
      CosmicSlopeZY     = 0.;
      CosmicInterceptTY = 0.;
      CosmicSlopeTY     = 0.;
      
      return;
    }
    
    /* get track information in each plane */
    // xy-plane: fixed
    CosmicInterceptXY = intercept_XY;
    CosmicSlopeXY     = slope_XY;
    
    // zy-plane
    GetCosmicTrack(CosmicModuleNumber,
  		 CosmicModuleHitZ, CosmicModuleHitY, CosmicModuleHitEZ, CosmicModuleHitEY,
  		 CosmicInterceptZY, CosmicSlopeZY);
    
    // ty-plane
    GetCosmicTrack(CosmicModuleNumber,
  		 CosmicModuleTimeAverage, CosmicModuleHitY, NULL, CosmicModuleHitEY,
  		 CosmicInterceptTY, CosmicSlopeTY);
  }


  void KanaE14CBAR::GetCosmicTrack(const Int_t n, const Float_t *x, const Float_t *y,
  				 const Float_t *ex, const Float_t *ey,
  				 Float_t &intercept, Float_t &slope)
  {
    TGraphErrors g(n, x, y, ex, ey);
    
    const Double_t a = (y[1]-y[0])/(x[1]-x[0]);
    const Double_t b = y[0]-a*x[0];
    
    f_CosmicTrack->SetParameters(b, a);
    g.Fit(f_CosmicTrack, "qn");
    
    intercept = f_CosmicTrack->GetParameter(0);
    slope     = f_CosmicTrack->GetParameter(1);
  }


  Double_t KanaE14CBAR::GetTheta(const Double_t x, const Double_t y)
  {
    const Double_t theta = TMath::ATan(y/x);
    if (y>=0) {
      if (x>0) {
        return theta;
      } else if (x<0) {
        return (theta + TMath::Pi());
      } else { // +90 deg.
        return TMath::Pi()*0.5;
      }
    } else { // y<0
      if (x>0) {
        return (theta+TMath::TwoPi());
      } else if (x<0) {
        return (theta + TMath::Pi());
      } else { // +270 deg.
        return TMath::Pi()*1.5;
      }
    }
  }


  KanaE14CBAR::~KanaE14CBAR()
  {
    if (ModulePeak          != NULL) delete [] ModulePeak;
    if (ModuleIntegratedADC != NULL) delete [] ModuleIntegratedADC;
    if (ModuleInitialTime   != NULL) delete [] ModuleInitialTime;
    
    // std::cout << "1" << std::endl;
    
    if (ModuleSumIntegratedADC      != NULL) delete [] ModuleSumIntegratedADC;
    if (ModuleInitialTimeAverage    != NULL) delete [] ModuleInitialTimeAverage;
    if (ModuleInitialTimeDifference != NULL) delete [] ModuleInitialTimeDifference;

    // std::cout << "2" << std::endl;
    
    if (ModuleEne       != NULL) delete [] ModuleEne;
    if (ModuleHitTime   != NULL) delete [] ModuleHitTime;
    if (ModuleDeltaTime != NULL) delete [] ModuleDeltaTime;

    // std::cout << "3" << std::endl;

    if (ModuleHitX != NULL) delete [] ModuleHitX;
    if (ModuleHitY != NULL) delete [] ModuleHitY;
    if (ModuleHitZ != NULL) delete [] ModuleHitZ;
    
    // std::cout << "4" << std::endl;
    
    if (CosmicModuleModID          != NULL) delete [] CosmicModuleModID;
    if (CosmicModuleEne            != NULL) delete [] CosmicModuleEne;
    if (CosmicModuleTimeAverage    != NULL) delete [] CosmicModuleTimeAverage;
    if (CosmicModuleTimeDifference != NULL) delete [] CosmicModuleTimeDifference;
    if (CosmicModuleHitX           != NULL) delete [] CosmicModuleHitX;
    if (CosmicModuleHitY           != NULL) delete [] CosmicModuleHitY;
    if (CosmicModuleHitZ           != NULL) delete [] CosmicModuleHitZ;
    if (CosmicModuleHitEX          != NULL) delete [] CosmicModuleHitEX;
    if (CosmicModuleHitEY          != NULL) delete [] CosmicModuleHitEY;
    if (CosmicModuleHitEZ          != NULL) delete [] CosmicModuleHitEZ;
    
    // std::cout << "5" << std::endl;
    
    if (f_CosmicTrack != NULL) delete f_CosmicTrack;
    // if (f_CosmicTrackXY != NULL) delete f_CosmicTrackXY;
    // if (f_CosmicTrackZY != NULL) delete f_CosmicTrackZY;
    // if (f_CosmicTrackTY != NULL) delete f_CosmicTrackTY;

    // std::cout << "6" << std::endl;
  }
  
}
