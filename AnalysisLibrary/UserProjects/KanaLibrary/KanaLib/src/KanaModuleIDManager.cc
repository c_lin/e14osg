#include "KanaLib/KanaModuleIDManager.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

#include <Rtypes.h>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaModuleIDManager)

namespace KanaLibrary
{
  
  KanaModuleIDManager::KanaModuleIDManager()
    : AccRunID(-1),
      ProVersion(-1)
  {
  }
  
  KanaModuleIDManager::KanaModuleIDManager(const Int_t acc_run_ID, const Int_t pro_version)
    : AccRunID(acc_run_ID),
      ProVersion(pro_version)
  {
  }    

  KanaModuleIDManager::~KanaModuleIDManager()
  {
  }
  
  Int_t KanaModuleIDManager::GetModuleIDList(const std::string detector_name, std::vector<Int_t> &list)
  {
    Int_t error = 0;
    if (detector_name=="BCV") {
      list = GetBCVModuleIDList();
    } else if (detector_name=="BHCV") {
      list = GetBHCVModuleIDList();
    } else if (detector_name=="BHGC") {
      list = GetBHGCModuleIDList();
    } else if (detector_name=="BHPV") {
      list = GetBHPVModuleIDList();
    } else if (detector_name=="BPCV") {
      list = GetBPCVModuleIDList();
    } else if (detector_name=="CBAR") {
      list = GetCBARModuleIDList();
    } else if (detector_name=="CC03") {
      list = GetCC03ModuleIDList();
    } else if (detector_name=="CC04") {
      list = GetCC04ModuleIDList();
    } else if (detector_name=="CC05") {
      list = GetCC05ModuleIDList();
    } else if (detector_name=="CC06") {
      list = GetCC06ModuleIDList();
    } else if (detector_name=="CSI") {
      list = GetCSIModuleIDList();
    } else if (detector_name=="CV") {
      list = GetCVModuleIDList();
    } else if (detector_name=="FBAR") {
      list = GetFBARModuleIDList();
    } else if (detector_name=="LCV") {
      list = GetLCVModuleIDList();
    } else if (detector_name=="NCC") {
      list = GetNCCModuleIDList();
    } else if (detector_name=="newBHCV") {
      list = GetnewBHCVModuleIDList();
    } else if (detector_name=="OEV") {
      list = GetOEVModuleIDList();
    } else {
      list = std::vector<Int_t>(0);
      error = 1;
    }
    
    return error;
  }


  std::vector<Int_t> KanaModuleIDManager::GetBCVModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<32; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }

    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetBHCVModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<8; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetBHGCModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<8; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetBHPVModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    // BHPV modules
    if (AccRunID<62) {
      for (Int_t module_ID=0; module_ID<24; ++module_ID) {
        module_ID_list.push_back(module_ID);
      }
    } else {
      for (Int_t module_ID=0; module_ID<32; ++module_ID) {
        module_ID_list.push_back(module_ID);
      }
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetBPCVModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<4; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetCBARModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<64; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetCC03ModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<32; module_ID+=2) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetCC04ModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    // CSI crystals
    for (Int_t module_ID=0; module_ID<58; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }

    // plastic scintillators
    for (Int_t module_ID=60; module_ID<66; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetCC05ModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    // CSI crystals
    for (Int_t module_ID=0; module_ID<54; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }

    // plastic scintillators
    for (Int_t module_ID=60; module_ID<66; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetCC06ModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    // CSI crystals
    for (Int_t module_ID=0; module_ID<54; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }

    // plastic scintillators
    for (Int_t module_ID=60; module_ID<66; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetCSIModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<2716; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetCVModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    // FCV
    for (Int_t module_ID=0; module_ID<96; module_ID+=2) {
      module_ID_list.push_back(module_ID);
    }
    
    // RCV
    for (Int_t module_ID=100; module_ID<188; module_ID+=2) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetFBARModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<32; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetLCVModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<4; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetNCCModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);

    // common & individual
    for (Int_t mod_ID=0; mod_ID<48; ++mod_ID) {
      module_ID_list.push_back(mod_ID*10  ); // common
      module_ID_list.push_back(mod_ID*10+1); // front
      module_ID_list.push_back(mod_ID*10+2); // middle
      module_ID_list.push_back(mod_ID*10+3); // rear
    }

    // outer
    for (Int_t mod_ID=50; mod_ID<58; ++mod_ID) {
      module_ID_list.push_back(mod_ID*10);
    }
    
    // HINEMOS
    for (Int_t mod_ID=60; mod_ID<64; ++mod_ID) {
      module_ID_list.push_back(mod_ID*10);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetnewBHCVModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<48; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }

  std::vector<Int_t> KanaModuleIDManager::GetOEVModuleIDList()
  {
    std::vector<Int_t> module_ID_list(0);
    
    for (Int_t module_ID=0; module_ID<44; ++module_ID) {
      module_ID_list.push_back(module_ID);
    }
    
    return module_ID_list;
  }
  
}
