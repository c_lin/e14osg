// change log
// Apr 8th, 2016
//    - GetE14IDList()
//      -- add functionalit to set ChannelMap in the same way of LoadChannelMap()

#include "KanaLib/KanaE14IDManager.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

#include <Rtypes.h>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaE14IDManager)

namespace KanaLibrary
{
  
  KanaE14IDManager::KanaE14IDManager()
    : AccRunID(-1),
      ProVersion(-1)
  {
  }

  KanaE14IDManager::KanaE14IDManager(const Int_t acc_run_ID, const Int_t pro_version)
    : AccRunID(acc_run_ID),
      ProVersion(pro_version)
  {
  }    

  KanaE14IDManager::~KanaE14IDManager()
  {
  }    

  Int_t KanaE14IDManager::GetE14IDList(const std::string detector_name, std::vector<Int_t> &list)
  {
    Int_t error = 0;
    if (detector_name=="BCV") {
      list = GetBCVE14IDList();
    } else if (detector_name=="BHCV") {
      list = GetBHCVE14IDList();
    } else if (detector_name=="BHGC") {
      list = GetBHGCE14IDList();
    } else if (detector_name=="BHPV") {
      list = GetBHPVE14IDList();
    } else if (detector_name=="BPCV") {
      list = GetBPCVE14IDList();
    } else if (detector_name=="CBAR") {
      list = GetCBARE14IDList();
    } else if (detector_name=="CC03") {
      list = GetCC03E14IDList();
    } else if (detector_name=="CC04") {
      list = GetCC04E14IDList();
    } else if (detector_name=="CC05") {
      list = GetCC05E14IDList();
    } else if (detector_name=="CC06") {
      list = GetCC06E14IDList();
    } else if (detector_name=="CSI") {
      list = GetCSIE14IDList();
    } else if (detector_name=="CV") {
      list = GetCVE14IDList();
    } else if (detector_name=="FBAR") {
      list = GetFBARE14IDList();
    } else if (detector_name=="IB") {
      list = GetIBE14IDList();
    } else if (detector_name=="IBCV") {
      list = GetIBCVE14IDList();
    } else if (detector_name=="LCV") {
      list = GetLCVE14IDList();
    } else if (detector_name=="MBCV") {
      list = GetMBCVE14IDList();
    } else if (detector_name=="NCC") {
      list = GetNCCE14IDList();
    } else if (detector_name=="newBHCV") {
      list = GetnewBHCVE14IDList();
    } else if (detector_name=="OEV") {
      list = GetOEVE14IDList();
    } else if (detector_name=="TRIGGERTAG") {
      list = GetTRIGGERTAGE14IDList();
    } else {
      list = std::vector<Int_t>(0);
      error = 1;
    }
    
    return error;
  }


  std::vector<Int_t> KanaE14IDManager::GetBCVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    // upstream
    for (Int_t e14_ID=0; e14_ID<32; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }

    // downstream
    for (Int_t e14_ID=100; e14_ID<132; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetBHCVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<8; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetBHGCE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<8; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetBHPVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    // BHPV modules
    if (AccRunID<62) {
      for (Int_t e14_ID=0; e14_ID<24; ++e14_ID) {
        e14_ID_list.push_back(e14_ID);
      }
    } else {
      for (Int_t e14_ID=0; e14_ID<32; ++e14_ID) {
        e14_ID_list.push_back(e14_ID);
      }
    }
    
    // extra channels (BHTS, special triggers, etc.)
    if (AccRunID<62) {
      e14_ID_list.push_back(30);
    } else {
      e14_ID_list.push_back(40);
      e14_ID_list.push_back(41);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetBPCVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<4; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetCBARE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    // upstream
    for (Int_t e14_ID=0; e14_ID<64; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }

    // downstream
    for (Int_t e14_ID=100; e14_ID<164; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetCC03E14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<32; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetCC04E14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    // CSI crystals
    for (Int_t e14_ID=0; e14_ID<58; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }

    // plastic scintillators
    for (Int_t e14_ID=60; e14_ID<66; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetCC05E14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    // CSI crystals
    for (Int_t e14_ID=0; e14_ID<54; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }

    // plastic scintillators
    for (Int_t e14_ID=60; e14_ID<66; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetCC06E14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    // CSI crystals
    for (Int_t e14_ID=0; e14_ID<54; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }

    // plastic scintillators
    for (Int_t e14_ID=60; e14_ID<66; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetCSIE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<2716; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetCVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    // FCV
    for (Int_t e14_ID=0; e14_ID<96; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    // RCV
    for (Int_t e14_ID=100; e14_ID<188; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetFBARE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<32; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetIBE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<32; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    for (Int_t e14_ID=100; e14_ID<132; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetIBCVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<32; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    for (Int_t e14_ID=100; e14_ID<132; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetLCVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<4; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetMBCVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<16; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetNCCE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);

    // common & individual
    for (Int_t mod_ID=0; mod_ID<48; ++mod_ID) {
      e14_ID_list.push_back(mod_ID*10  ); // common
      e14_ID_list.push_back(mod_ID*10+1); // front
      e14_ID_list.push_back(mod_ID*10+2); // middle
      e14_ID_list.push_back(mod_ID*10+3); // rear
    }

    // outer
    for (Int_t mod_ID=50; mod_ID<58; ++mod_ID) {
      e14_ID_list.push_back(mod_ID*10);
    }
    
    // HINEMOS
    for (Int_t mod_ID=60; mod_ID<64; ++mod_ID) {
      e14_ID_list.push_back(mod_ID*10);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetnewBHCVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<48; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetOEVE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<44; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }

  std::vector<Int_t> KanaE14IDManager::GetTRIGGERTAGE14IDList()
  {
    std::vector<Int_t> e14_ID_list(0);
    
    for (Int_t e14_ID=0; e14_ID<6; ++e14_ID) {
      e14_ID_list.push_back(e14_ID);
    }
    
    return e14_ID_list;
  }


  Int_t KanaE14IDManager::LoadChannelMap(const char *detector_name)
  {
    // reset channel map
    ChannelMap.clear();
    
    // read mapfile
    /* modified on Apr 8th */
    /*   - use my own files (symbolic links to actual files) */
    std::stringstream mapfile;
    // mapfile << "/sw/koto/production/run" << AccRunID << "/pro" << ProVersion << "/"
    // 	  << "Fiber/mapfile/ch_map_" << detector_name << ".txt";
    mapfile << "/home/had/ikamiji/work/Main/Common/data/Run" << AccRunID << "/Basic/"
  	  << "ChannelMap/pro" << ProVersion << "/ch_map_" << detector_name << ".txt";
    
    std::ifstream fin_mapfile(mapfile.str().c_str());
    if (!fin_mapfile) {
      std::cerr << "ERROR: cannot find mapfle '" << mapfile.str() << "'" << std::endl;
      return 1;
    }
    
    // get total number of channels
    Int_t n_channels = -9999;
    while (!fin_mapfile.eof()) {
      std::string line;
      std::getline(fin_mapfile, line);
      
      if (KanaFunctions::NoCharacter(line)) {
        if (!fin_mapfile.eof()) break;
        else                    continue;
      }
      
      n_channels = std::atoi(line.c_str());
      break;
    }
    
    if (n_channels <= 0) {
      std::cerr << "ERROR: invalid number of channels in mapfile '" << mapfile.str() << "'"
  	      << "(" << n_channels << ")" << std::endl;
      return 2;
    }
    
    // get mod IDs
    while (!fin_mapfile.eof()) {
      
      std::string line;
      std::getline(fin_mapfile, line);
      
      if (KanaFunctions::NoCharacter(line)) { // only space or tab in a line
        if (fin_mapfile.eof()) break;
        else                   continue;
      }
      
      /* removed on Apr 7, 2016 */
      // Int_t n_val = 0; // number of values in a line
      // Int_t e14_ID, crate_ID, mod_ID, ch_ID;
      // for (Int_t i=0, n=line.size(); i < n; ++i) {
        
      //   if ( (line.at(i)==' ') || (line.at(i)=='\t') ) continue;
        
      //   std::string num = "";
      //   do {
      // 	if ( (line.at(i)==' ') || (line.at(i)=='\t') ) break;
      // 	num += line.at(i);
      // 	++i;
      //   } while (i<n);
        
      //   if (n_val==0) {
      // 	e14_ID   = std::atoi(num.c_str());
      //   } else if (n_val==1) {
      // 	crate_ID = std::atoi(num.c_str());
      //   } else if (n_val==2) {
      // 	mod_ID   = std::atoi(num.c_str());
      //   } else if (n_val==3) {
      // 	ch_ID    = std::atoi(num.c_str());
      //   }
        
      //   ++n_val;
      // }
      

      /* modified on Apr 7th, 2016 */
      // if (n_val!=4) {
      std::vector<std::string> vars;
      if (KanaFunctions::ReadSSVLine(line, vars) != 4) {
        std::cerr << "ERROR: wrong number of values in '" << mapfile.str() << ")"
  		<< std::endl;
        return 3;
      }

      const Int_t e14_ID   = std::atoi(vars.at(0).c_str());
      const Int_t crate_ID = std::atoi(vars.at(1).c_str());
      const Int_t mod_ID   = std::atoi(vars.at(2).c_str());
      const Int_t ch_ID    = std::atoi(vars.at(3).c_str());
      
      ChannelMap[e14_ID].FADCCrateID   = crate_ID;
      ChannelMap[e14_ID].FADCModID     = mod_ID;
      ChannelMap[e14_ID].FADCChannelID = ch_ID;
    }
    
    if (ChannelMap.size() != static_cast<size_t>(n_channels)) {
      std::cerr << "ERROR: mapfile '" << mapfile.str() << "' has wrong number of lines"
  	      << std::endl;
      return 4;
    }
    
    return 0;
  }


  Int_t KanaE14IDManager::GetFADCCrateID(const Int_t e14_ID)
  {
    if (ChannelMap.find(e14_ID) == ChannelMap.end())
      return -1;
    else
      return ChannelMap[e14_ID].FADCCrateID;
  }

  Int_t KanaE14IDManager::GetFADCModID(const Int_t e14_ID)
  {
    if (ChannelMap.find(e14_ID) == ChannelMap.end())
      return -1;
    else
      return ChannelMap[e14_ID].FADCModID;
  }

  Int_t KanaE14IDManager::GetFADCChannelID(const Int_t e14_ID)
  {
    if (ChannelMap.find(e14_ID) == ChannelMap.end())
      return -1;
    else
      return ChannelMap[e14_ID].FADCChannelID;
  }
  
}
