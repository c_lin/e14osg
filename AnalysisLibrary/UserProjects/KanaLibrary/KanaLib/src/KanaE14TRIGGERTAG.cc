#include "KanaLib/KanaE14TRIGGERTAG.h"

ClassImp(KanaLibrary::KanaE14TRIGGERTAG)

namespace KanaLibrary
{

  KanaE14TRIGGERTAG::KanaE14TRIGGERTAG()
  {
  }


  KanaE14TRIGGERTAG::KanaE14TRIGGERTAG(const Int_t acc_run_ID, const Int_t pro_version)
    : KanaE14Detector125MHz(acc_run_ID, pro_version, "TRIGGERTAG")
  {
    InitKanaE14TRIGGERTAG();
  }


  void KanaE14TRIGGERTAG::InitKanaE14TRIGGERTAG()
  {
    ClockModID = 0;
    LASERModID = 1;
    LEDModID   = 2;
    
    TrigTagPeakThreshold = 1000;
  }

  Bool_t KanaE14TRIGGERTAG::IsClock()
  {
    return IsTrigTagEvent(ClockModID);
  }

  Bool_t KanaE14TRIGGERTAG::IsLASER()
  {
    return IsTrigTagEvent(LASERModID);
  }

  Bool_t KanaE14TRIGGERTAG::IsLED()
  {
    return IsTrigTagEvent(LEDModID);
  }

  Bool_t KanaE14TRIGGERTAG::IsTrigTagEvent(Int_t trig_tag_mod_ID)
  {
    Bool_t is_trig_tag_event = kFALSE;
    for (Int_t num=0; num < Number; ++num) {
      if (ModID[num]==trig_tag_mod_ID && Peak[num]>=TrigTagPeakThreshold) {
        is_trig_tag_event = kTRUE;
        break;
      }
    }
    return is_trig_tag_event;
  }


  KanaE14TRIGGERTAG::~KanaE14TRIGGERTAG()
  {
  }

}
