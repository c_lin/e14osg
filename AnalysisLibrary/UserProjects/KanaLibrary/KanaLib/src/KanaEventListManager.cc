#include "KanaLib/KanaEventListManager.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdlib>

ClassImp(KanaLibrary::KanaEventListManager)

namespace KanaLibrary
{
  
  KanaEventListManager::KanaEventListManager()
    : TopDir("/home/had/ikamiji/work/Main/Common/project/EventList")
  {
    InitKanaEventListManager();
  }

  void KanaEventListManager::InitKanaEventListManager()
  {
    RootFile  = NULL;
    EventList = NULL;
  }

  KanaEventListManager::~KanaEventListManager()
  {
  }

  Int_t KanaEventListManager::LoadEventList(const Int_t acc_run_ID, const Int_t pro_version,
					    const Int_t run_ID, const Int_t node_ID, const Int_t file_ID,
					    const Int_t version, const std::string name)
  {
    std::stringstream rootfile_path;
    rootfile_path << TopDir << "/v" << version << "/root/" << name << "/"
		  << "Run" << acc_run_ID << "/pro" << pro_version << "/"
		  << "run" << run_ID << "/run" << run_ID
		  << "_node" << node_ID << "_file" << file_ID << ".root";
    
    if (RootFile != NULL) {
      RootFile->Close();
      delete RootFile;
    }
    
    RootFile = new TFile(rootfile_path.str().c_str());
    if (RootFile->IsZombie()) return 1;
    
    EventList = (TEventList*)RootFile->Get("EventList");
    if (EventList==NULL) {
      std::cerr << "ERROR: cannot get EventList from '" << rootfile_path.str() << "'"
		<< std::endl;
      return 2;
    }
    
    return 0;
  }
  
  Int_t KanaEventListManager::GetN()
  {
    if (EventList!=NULL) {
      return EventList->GetN();
    } else {
      return 0;
    }
  }
  
  Long64_t KanaEventListManager::GetEntry(Int_t index)
  {
    if (EventList!=NULL) {
      return EventList->GetEntry(index);
    } else {
      return -1;
    }
  }
  
}
