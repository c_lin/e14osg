#include "KanaLib/KanaE14Detector500MHz.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <algorithm>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaE14Detector500MHz)

namespace KanaLibrary
{
  
  KanaE14Detector500MHz::KanaE14Detector500MHz()
    : KanaE14DetectorBase()
  {
    InitKanaE14Detector500MHz();
  }

  KanaE14Detector500MHz::KanaE14Detector500MHz(const Int_t acc_run_ID, const Int_t pro_version,
  					     const std::string name)
    : KanaE14DetectorBase(acc_run_ID, pro_version, name)
  {
    InitKanaE14Detector500MHz();
    
    // allocate memories
    nHits         = new Short_t [nChannels];
    Peak          = new Short_t [nChannels][MaxnHits];
    PeakTime      = new Short_t [nChannels][MaxnHits];
    IntegratedADC = new Float_t [nChannels][MaxnHits];
    InitialTime   = new Float_t [nChannels][MaxnHits];
    Ene           = new Float_t [nChannels][MaxnHits];
    Time          = new Float_t [nChannels][MaxnHits];
  }

  void KanaE14Detector500MHz::InitKanaE14Detector500MHz()
  {
    FADCnSamples       = 256;
    FADCSampleInterval = 2e-9;
    
    nHits         = NULL;
    Peak          = NULL;
    PeakTime      = NULL;
    IntegratedADC = NULL;
    InitialTime   = NULL;
    Ene           = NULL;
    Time          = NULL;
  }

  void KanaE14Detector500MHz::SetBranchAddresses(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branch addresses: " << Name << std::endl;
    
    // set branch addresses of KanaE14DetectorBase
    SetBranchAddressesBase(tr, b_list);

    // set branch addresses of KanaE14Detector500MHz
    SetBranchAddresses500MHz(tr, b_list);
  }

  void KanaE14Detector500MHz::SetBranchAddresses500MHz(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "nHits") != end) {
      const std::string bname = Name + "nHits";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), nHits);
      std::cout << "     -- nHits: enabled" << std::endl;
    }
    if (std::find(begin, end, "Peak") != end) {
      const std::string bname = Name + "Peak";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Peak);
      std::cout << "     -- Peak: enabled" << std::endl;
    }
    if (std::find(begin, end, "PeakTime") != end) {
      const std::string bname = Name + "PeakTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), PeakTime);
      std::cout << "     -- PeakTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "IntegratedADC") != end) {
      const std::string bname = Name + "IntegratedADC";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), IntegratedADC);
      std::cout << "     -- IntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "InitialTime") != end) {
      const std::string bname = Name + "InitialTime";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), InitialTime);
      std::cout << "     -- InitialTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "Ene") != end) {
      const std::string bname = Name + "Ene";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Ene);
      std::cout << "     -- Ene: enabled" << std::endl;
    }
    if (std::find(begin, end, "Time") != end) {
      const std::string bname = Name + "Time";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Time);
      std::cout << "     -- Time: enabled" << std::endl;
    }

  }


  void KanaE14Detector500MHz::AddBranches(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branches: " << Name << std::endl;

    // add branches of KanaE14DetectorBase
    AddBranchesBase(tr, b_list);

    // add branches of KanaE14Detector500MHz
    AddBranches500MHz(tr, b_list);
  }

  void KanaE14Detector500MHz::AddBranches500MHz(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "nHits") != end) {
      const std::string bname = Name + "nHits";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/S";
      tr->Branch(bname.c_str(), nHits, leaflist.str().c_str());
      std::cout << "     -- nHits: enabled" << std::endl;
    }
    if (std::find(begin, end, "Peak") != end) {
      const std::string bname = Name + "Peak";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][" << MaxnHits << "]/S";
      tr->Branch(bname.c_str(), Peak, leaflist.str().c_str());
      std::cout << "     -- Peak: enabled" << std::endl;
    }
    if (std::find(begin, end, "PeakTime") != end) {
      const std::string bname = Name + "PeakTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][" << MaxnHits << "]/S";
      tr->Branch(bname.c_str(), PeakTime, leaflist.str().c_str());
      std::cout << "     -- PeakTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "IntegratedADC") != end) {
      const std::string bname = Name + "IntegratedADC";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][" << MaxnHits << "]/F";
      tr->Branch(bname.c_str(), IntegratedADC, leaflist.str().c_str());
      std::cout << "     -- IntegratedADC: enabled" << std::endl;
    }
    if (std::find(begin, end, "InitialTime") != end) {
      const std::string bname = Name + "InitialTime";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][" << MaxnHits << "]/F";
      tr->Branch(bname.c_str(), InitialTime, leaflist.str().c_str());
      std::cout << "     -- InitialTime: enabled" << std::endl;
    }
    if (std::find(begin, end, "Ene") != end) {
      const std::string bname = Name + "Ene";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][" << MaxnHits << "]/F";
      tr->Branch(bname.c_str(), Ene, leaflist.str().c_str());
      std::cout << "     -- Ene: enabled" << std::endl;
    }
    if (std::find(begin, end, "Time") != end) {
      const std::string bname = Name + "Time";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][" << MaxnHits << "]/F";
      tr->Branch(bname.c_str(), Time, leaflist.str().c_str());
      std::cout << "     -- Time: enabled" << std::endl;
    }
  }


  void KanaE14Detector500MHz::ZeroSuppression(const Double_t th)
  {
    std::vector<Int_t> old_num_list(0);
    
    for (Int_t num=0; num < Number; ++num) {
      for (Int_t i_hit=0; i_hit < nHits[num]; ++i_hit) {
        if (Ene[num][i_hit]<th) continue;
        old_num_list.push_back(num);
        break;
      }
    }
    
    Number=0;
    for (Int_t i=0, n=old_num_list.size(); i < n; ++i) {

      const Int_t old_num = old_num_list.at(i);
      ModID[Number]         = ModID[old_num];
      
      const Int_t old_n_hits = nHits[Number];
      Int_t new_n_hits = 0;
      for (Int_t i_hit=0; i_hit < old_n_hits; ++i_hit) {
        if (Ene[old_num][i_hit]<th) continue;
        Peak[Number][new_n_hits]          = Peak[old_num][i_hit];
        PeakTime[Number][new_n_hits]      = PeakTime[old_num][i_hit];
        IntegratedADC[Number][new_n_hits] = IntegratedADC[old_num][i_hit];
        InitialTime[Number][new_n_hits]   = InitialTime[old_num][i_hit];
        Ene[Number][new_n_hits]           = Ene[old_num][i_hit];
        Time[Number][new_n_hits]          = Time[old_num][i_hit];
        ++new_n_hits;
      }
      nHits[Number] = new_n_hits;
      ++Number;
    }
  }


  KanaE14Detector500MHz::~KanaE14Detector500MHz()
  {
    if (nHits         != NULL) delete [] nHits;
    if (Peak          != NULL) delete [] Peak;
    if (PeakTime      != NULL) delete [] PeakTime;
    if (IntegratedADC != NULL) delete [] IntegratedADC;
    if (InitialTime   != NULL) delete [] InitialTime;
    if (Ene           != NULL) delete [] Ene;
    if (Time          != NULL) delete [] Time;
  }
  
}
