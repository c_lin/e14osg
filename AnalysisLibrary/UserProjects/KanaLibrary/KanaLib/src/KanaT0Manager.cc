#include "KanaLib/KanaT0Manager.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <map>

#include "KanaLib/KanaE14IDManager.h"
#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaT0Manager)

namespace KanaLibrary
{
  
  KanaT0Manager::KanaT0Manager()
    : Name(""),
      AccRunID(0),
      ProVersion(0),
      ErrorFlag(0),
      nChannels(0),
      E14IDList(0)
  {
  }

  KanaT0Manager::KanaT0Manager(const char *name,
  			     const Int_t acc_run_ID, const Int_t pro_version)
    : Name(name),
      AccRunID(acc_run_ID),
      ProVersion(pro_version),
      ErrorFlag(0),
      nChannels(0),
      E14IDList(0)
  {
    GetE14IDList();
    InitT0List();
  }

  KanaT0Manager::~KanaT0Manager()
  {
  }

  void KanaT0Manager::GetE14IDList()
  {
    KanaE14IDManager e14ID_man(AccRunID, ProVersion);
    if (e14ID_man.GetE14IDList(Name.c_str(), E14IDList) != 0) {
      SetErrorFlag(GetE14IDListError);
      return;
    }
    
    nChannels = E14IDList.size();
  }

  void KanaT0Manager::InitT0List()
  {
    if ( (ErrorFlag&GetE14IDListError) != 0) {
      SetErrorFlag(InitT0ListError);
      return;
    }
    
    for (Int_t i_ch=0; i_ch < nChannels; ++i_ch) {
      const Int_t e14_ID = E14IDList.at(i_ch);
      T0List[e14_ID] = 0.;
    }
  }

  Int_t KanaT0Manager::LoadT0(const char *path)
  {
    std::ifstream fin(path);
    if (!fin) {
      std::cerr << "ERROR: cannot open file '" << path << "'" << std::endl;
      return 1;
    }
    
    // check number of channels
    Int_t n_ch = -1;
    while (!fin.eof()) {

      std::string line;
      std::getline(fin, line);
      
      if (KanaFunctions::NoCharacter(line)) {
        if (fin.eof()) break;
        else           continue;
      }

      std::vector<std::string> vars(0);
      KanaFunctions::ReadSSVLine(line, vars);
      if (vars.size()!=1) {
        std::cerr << "ERROR: unexpected file format for number of channels" << std::endl;
        return 2;
      } else {
        n_ch = std::atoi(vars.at(0).c_str());
        break;
      }
    }
    
    if (n_ch != nChannels) {
      std::cerr << "ERROR: wrong number of channels" << std::endl;
      std::cerr << "   in map file: " << nChannels << std::endl;
      std::cerr << "   in T0 file : " << n_ch      << std::endl;
      return 3;
    }
    
    // get T0 list
    while (!fin.eof()) {
      
      std::string line;
      std::getline(fin, line);
      
      if (KanaFunctions::NoCharacter(line)) {
        if (fin.eof()) break;
        else           continue;
      }
      
      std::vector<std::string> vars(0);
      KanaFunctions::ReadSSVLine(line, vars);
      if (vars.size()!=3) {
        std::cerr << "ERROR: unexpected file format for T0 values" << std::endl;
        return 4;
      }
      
      const Int_t    e14_ID = std::atoi(vars.at(0).c_str());
      const Double_t t0     = std::atof(vars.at(1).c_str());

      if (T0List.find(e14_ID) == T0List.end()) {
        std::cerr << "ERROR: E14 ID = " << e14_ID << " is not exist in T0 list" << std::endl;
        InitT0List();
        return 5;
      } else {
        T0List[e14_ID] = t0;
      }
    }
    
    // check number of loaded T0
    if (T0List.size() != static_cast<size_t>(nChannels)) {
      std::cerr << "ERROR: number of load t0 values is not equal to numebr of channels" << std::endl;
      InitT0List();
      return 6;
    }
    
    return 0;
  }
  
}
