#include "KanaLib/KanaFileReader.h"

#include <algorithm>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaFileReader)

namespace KanaLibrary
{
  
  KanaFileReader::KanaFileReader()
  {
    InitKanaFileReader();
  }
  
  void KanaFileReader::InitKanaFileReader()
  {
  }

  void KanaFileReader::SetFile(const std::string path)
  {
    if (InputFile.is_open()) InputFile.close();
    LinePositionList.clear();
    
    InputFile.open(path.c_str());
    if (!InputFile) {
      std::cerr << "ERROR: in KanaFileReader: cannot open file '" << path << "'" << std::endl;
      return;
    }
    
    while (!InputFile.eof()) {

      // remember current position
      std::ifstream::pos_type current_pos = InputFile.tellg();

      // read line
      std::string line;
      std::getline(InputFile, line);
      if (KanaFunctions::NoCharacter(line)) {
        if (InputFile.eof()) break;
        else                 continue;
      }
      
      // set position
      LinePositionList.push_back(current_pos);
    }
  }


  void KanaFileReader
  ::ReadLine(std::vector<std::basic_istream<char,char_traits<char> >::pos_type>::size_type index,
	     std::string option)
  {
    /* clear EOF bit */
    if (InputFile.eof()) InputFile.clear();
    
    /* read line */
    InputFile.seekg(LinePositionList.at(index));

    std::string line;
    std::getline(InputFile, line);

    /* get words */
    std::transform(option.begin(), option.end(), option.begin(), tolower);

    WordList.clear();
    if (option=="ssv") {
      KanaFunctions::ReadSSVLine(line, WordList);
    } else if (option=="csv") {
      KanaFunctions::ReadCSVLine(line, WordList);
    }
  }


  KanaFileReader::~KanaFileReader()
  {
  }
  
}
