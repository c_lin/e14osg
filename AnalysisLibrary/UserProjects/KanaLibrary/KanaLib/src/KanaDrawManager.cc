#include "KanaLib/KanaDrawManager.h"

#include <iostream>
#include <string>

#include <TBox.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TF1.h>
#include <TGraph.h>
#include <TH1.h>
#include <TH2.h>
#include <TLegend.h>
#include <TLine.h>
#include <TMath.h>
#include <TPad.h>
#include <TPaveStats.h>
#include <TPaveText.h>
#include <TROOT.h>
#include <TString.h>
#include <TStyle.h>
#include <TSystem.h>

ClassImp(KanaLibrary::KanaDrawManager)

namespace
{
  const Bool_t debug_flag = kFALSE;
}

namespace KanaLibrary
{
  
  KanaDrawManager::KanaDrawManager()
  {
    /*** initialize all variables ***/
    fName  = NULL;
    fTitle = NULL;

    fCanvas        = NULL;
    fDivisionRatio = 1.0;

    fDrawRegionHeight = 0;
    fDrawPad     = NULL;
    fPadFrame[0] = NULL;
    fPadFrame[1] = NULL;
    fPadFrame[2] = NULL;
    fInfoPad     = NULL;
    fInfoTitle   = NULL;
    fTextInfo    = NULL;
    fLegendPad   = NULL;
    fLegend      = NULL;
    fPDFName     = NULL;
    
    fStatNrow    = 0;
    fStatNcolumn = 0;
    
    fNhistos = 0;
    fNlegend = 0;
    fNstats  = 0;
    
    fIsInitialized = kFALSE;
    fIsDivided     = kFALSE;
  }


  KanaDrawManager::KanaDrawManager(const char *name, const char *title)
  {
    PrintDebugInfo("<< ctor of KanaDrawManager: START >>");
    
    /*** initialize all variables ***/
    fName  = NULL;
    fTitle = NULL;

    fCanvas = NULL;
    fDivisionRatio    = 1.0;

    fDrawRegionHeight = 0;
    fDrawPad     = NULL;
    fPadFrame[0] = NULL;
    fPadFrame[1] = NULL;
    fPadFrame[2] = NULL;
    fInfoPad     = NULL;
    fInfoTitle   = NULL;
    fTextInfo    = NULL;
    fLegendPad   = NULL;
    fLegend      = NULL;
    fPDFName     = NULL;
    
    fStatNrow    = 0;
    fStatNcolumn = 0;
    
    fNhistos = 0;
    fNlegend = 0;
    fNstats  = 0;
    
    fIsInitialized = kFALSE;
    fIsDivided     = kFALSE;
    
    /*** Set Default Style ***/
    SetRootStyle();
    PrintDebugInfo("   + set default style");

    /*** set name & title ***/
    fName  = new TString(name);
    fTitle = new TString(title);

    /*** intialize ***/
    Init();
    
    PrintDebugInfo("<< ctor of KanaDrawManager: END>>");
  }


  KanaDrawManager::KanaDrawManager(const char *pdf_name)
  {
    PrintDebugInfo("<< ctor of KanaDrawManager: START >>");
    
    /*** initialize all variables ***/
    fName  = NULL;
    fTitle = NULL;

    fCanvas = NULL;
    fDivisionRatio    = 1.0;

    fDrawRegionHeight = 0;
    fDrawPad     = NULL;
    fPadFrame[0] = NULL;
    fPadFrame[1] = NULL;
    fPadFrame[2] = NULL;
    fInfoPad     = NULL;
    fInfoTitle   = NULL;
    fTextInfo    = NULL;
    fLegendPad   = NULL;
    fLegend      = NULL;
    fPDFName     = NULL;
    
    fStatNrow    = 0;
    fStatNcolumn = 0;
    
    fNhistos = 0;
    fNlegend = 0;
    fNstats  = 0;
    
    fIsInitialized = kFALSE;
    fIsDivided     = kFALSE;
    
    /*** Set Default Style ***/
    SetRootStyle();
    PrintDebugInfo("   + set default style");
    
    /*** Open Output PDF (create main canvas)***/
    OpenNewPDF(pdf_name);
    
    /*** intialize ***/
    Init();
    
    PrintDebugInfo("<< ctor of KanaDrawManager: END>>");
  }


  KanaDrawManager::KanaDrawManager(Int_t, char **argv, const char *pdf_name)
  {
    PrintDebugInfo("<< ctor of KanaDrawManager: START >>");
    
    /*** initialize all variables ***/
    fName  = NULL;
    fTitle = NULL;

    fCanvas = NULL;
    fDivisionRatio    = 1.0;

    fDrawRegionHeight = 0;
    fDrawPad     = NULL;
    fPadFrame[0] = NULL;
    fPadFrame[1] = NULL;
    fPadFrame[2] = NULL;
    fInfoPad     = NULL;
    fInfoTitle   = NULL;
    fTextInfo    = NULL;
    fLegendPad   = NULL;
    fLegend      = NULL;
    fPDFName     = NULL;
    
    fStatNrow    = 0;
    fStatNcolumn = 0;
    
    fNhistos = 0;
    fNlegend = 0;
    fNstats  = 0;
    
    fIsInitialized = kFALSE;
    fIsDivided     = kFALSE;
    
    /*** Set Default Style ***/
    SetRootStyle();
    PrintDebugInfo("   + set default style");
    
    /*** Open Output PDF (create main canvas)***/
    if ( !std::string(pdf_name).size() )
      OpenNewPDF( std::string(std::string(argv[0])+".pdf").c_str() );
    else 
      OpenNewPDF(pdf_name);
    
    /*** intialize ***/
    Init();
    
    PrintDebugInfo("<< ctor of KanaDrawManager: END>>");
  }


  void KanaDrawManager::ClosePDF()
  {
    if (fPDFName==NULL) {
      // std::cerr << "ERROR: in KanaDrawManager::ClosePDF(): PDF is not opened" << std::endl;
      return;
    }
    
    fCanvas->Print( std::string(std::string(fPDFName->Data())+std::string("]")).c_str() );
    PrintDebugInfo("   + PDF is closed");
    
    delete fPDFName;
    fPDFName = NULL;
  }


  KanaDrawManager::~KanaDrawManager()
  {
    PrintDebugInfo("<< dtor of KanaDrawManager: START >>");

    // close output PDF
    // fCanvas->Print( Form("%s]", fPDFName->Data()) );
    ClosePDF();
    PrintDebugInfo("   + PDF is closed");
    
    // delete canvas
    if (fDrawPad) delete fDrawPad;
    if (fInfoPad) delete fInfoPad;
    if (fCanvas)  delete fCanvas;
    
    std::cout << "   Canvas is deleted" << std::endl;
    
    if (fPDFName) delete fPDFName;
    
    if (fTitle) delete fTitle;
    if (fName)  delete fName;
    
    std::cout << "   terminate KanaDrawManager" << std::endl;
    
    PrintDebugInfo("<< dtor of KanaDrawManager: END >>");
  }


  void KanaDrawManager::Init()
  {
    if (fIsInitialized) {
      std::cerr << "ERROR: in KanaDrawManager::Init(): This method can be called from Ctor ONLY"
  	      << std::endl;
      return;
    }

    
    PrintDebugInfo("<< Initialization of KanaDrawManager: START >>");
    
    /*** Set Sub-Pads: DrawPad, InfoPad & LegendPad***/
    // set height of drawing region: [0, 1] (relative size to the main canvas)
    fDrawRegionHeight = 0.7;
    if (debug_flag) {
      std::cout << "   + set pad division ratio: " << fDrawRegionHeight << std::endl;
    }
    
    // pad for graphics (histograms, graphs, etc...)
    const Double_t DrawPad_xlow       = 0.;
    const Double_t DrawPad_ylow       = 0.;
    const Double_t DrawPad_xup        = 1.;
    const Double_t DrawPad_yup        = 1.;
    const Color_t  DrawPad_color      = 0;
    const Short_t  DrawPad_bordersize = 4;
    const Short_t  DrawPad_bordermode = 1;
    fDrawPad = new TPad("fDrawPad", "Draw Region",
  		      DrawPad_xlow, DrawPad_ylow, DrawPad_xup, DrawPad_yup,
  		      DrawPad_color, DrawPad_bordersize, DrawPad_bordermode );
    fDrawPad->SetFillStyle(4000); // transparent
    fDrawPad->Draw();
    PrintDebugInfo("   + pad for graphics is created");
    
    // pad for text information
    const Double_t InfoPad_xlow       = 0.70;
    const Double_t InfoPad_ylow       = 0.00;
    const Double_t InfoPad_xup        = 1.00;
    const Double_t InfoPad_yup        = 0.30;
    // const Double_t InfoPad_yup        = fDrawPad->GetBottomMargin() - 0.10;
    const Color_t  InfoPad_color      = 0;
    const Short_t  InfoPad_bordersize = 4;
    const Short_t  InfoPad_bordermode = 1;
    fInfoPad = new TPad("fInfoPad", "Info Region",
  		      InfoPad_xlow, InfoPad_ylow, InfoPad_xup, InfoPad_yup,
  		      InfoPad_color, InfoPad_bordersize, InfoPad_bordermode );
    fInfoPad->SetFillStyle(4000); // transparent
    fInfoPad->Draw();
    PrintDebugInfo("   + pad for text info is created");
    
    // pad for legend
    const Double_t LegendPad_xlow       = 0.55;
    const Double_t LegendPad_ylow       = 0.;
    const Double_t LegendPad_xup        = 1.;
    const Double_t LegendPad_yup        = 0.30;
    // const Double_t LegendPad_yup        = fDrawPad->GetBottomMargin() - 0.10;
    const Color_t  LegendPad_color      = 0;
    const Short_t  LegendPad_bordersize = 4;
    const Short_t  LegendPad_bordermode = 1;
    // fLegendPad = new TPad("fLegendPad", "Legend Region", 0.50, 0, 1, 1-fDrawRegionHeight, 0, 4, 1); 
    fLegendPad = new TPad("fLegendPad", "Legend Region",
  			LegendPad_xlow, LegendPad_ylow, LegendPad_xup, LegendPad_yup,
  			LegendPad_color, LegendPad_bordersize, LegendPad_bordermode );
    fLegendPad->SetFillStyle(4000); // transparent
    fLegendPad->Draw();
    PrintDebugInfo("   + pad for legend is created");
    ///////////////
    
    /*** Region of Text Information ***/
    const Double_t InfoTitle_x1ndc = 0;
    const Double_t InfoTitle_y1ndc = 1 - 0.13*1.2;
    const Double_t InfoTitle_x2ndc = 1;
    const Double_t InfoTitle_y2ndc = 1;
    // fInfoTitle = new TPaveText(0.05, 0.71, 0.45, 0.90, "NDC");
    fInfoTitle = new TPaveText(InfoTitle_x1ndc, InfoTitle_y1ndc,
  			     InfoTitle_x2ndc, InfoTitle_y2ndc, "NDC");
    fInfoTitle->SetTextAlign(12);
    fInfoTitle->SetTextColor(kCyan+3);
    fInfoTitle->SetTextSize(0.13);
    fInfoTitle->SetFillStyle(0);
    fInfoTitle->SetBorderSize(0);
    fInfoTitle->SetLineColor(kCyan+3);
    
    fInfoPad->cd();
    fInfoTitle->AddText("#### Information ####");
    // fInfoTitle->Draw();
    PrintDebugInfo("   + set title of text information");
    
    // pave box for information
    const Double_t TextInfo_x1ndc = 0;
    const Double_t TextInfo_y1ndc = 0;
    const Double_t TextInfo_x2ndc = 1;
    const Double_t TextInfo_y2ndc = 1 - 0.13*1.2;
    // fTextInfo = new TPaveText(0.05, 0.05, 0.90, 0.69, "NDC");
    fTextInfo = new TPaveText(TextInfo_x1ndc, TextInfo_y1ndc,
  			    TextInfo_x2ndc, TextInfo_y2ndc, "NDC");
    fTextInfo->SetTextAlign(12);
    fTextInfo->SetTextColor(kCyan+3);
    fTextInfo->SetTextSize(0.13);
    fTextInfo->SetFillStyle(0);
    fTextInfo->SetBorderSize(0);
    PrintDebugInfo("   + set pave box for text information");
    ///////////////
    
    /*** Legend Box ***/
    // const Double_t Legend_x1ndc = 0.05;
    // const Double_t Legend_y1ndc = 0.05;
    // const Double_t Legend_x2ndc = 0.95;
    // const Double_t Legend_y2ndc = 0.95;
    fLegend = new TLegend(0.00, 0.00, 1.00, 1.00);
    // fLegend = new TLegend(Legend_x1ndc, Legend_y1ndc, Legend_x2ndc, Legend_y2ndc);
    fLegend->SetTextSize(0.13);
    // fLegend->SetFillStyle(0);
    fLegend->SetFillColor(kWhite);
    fLegend->SetBorderSize(1);
    PrintDebugInfo("   + set legend box");
    ///////////////
    
    /*** set default stat divition ***/
    fStatNrow    = 2;
    fStatNcolumn = 1;
    // fStatNrow    = 3;
    // fStatNcolumn = 2;

    /*** Initialize Other Variables ***/
    fNhistos = 0;
    fNlegend = 0;
    fNstats  = 0;
    
    fIsInitialized = kTRUE;
    
    PrintDebugInfo("<< Initialization of KanaDrawManager: END >>");
  }


  void KanaDrawManager::AddInfo(const char *info)
  {
    PrintDebugInfo("<< KanaDrawManager::AddInfo: START >>");
    
    // add information to the pave box for text information
    fTextInfo->AddText( Form("- %s", info) );
    
    PrintDebugInfo("<< KanaDrawManager::AddInfo: END >>");
  }


  void KanaDrawManager::AddLegendEntry(const TObject *obj, const char *label, Option_t *option)
  {
    PrintDebugInfo("<< KanaDrawManager::AddLegendEntry: START >>");

    fLegend->AddEntry(obj, label, option);
    ++fNlegend;

    PrintDebugInfo("<< KanaDrawManager::AddLegendEntry: END >>");
  }


  void KanaDrawManager::Clear()
  {
    PrintDebugInfo("<< KanaDrawManager::Clear START >>");

    /*** Clear Pads ***/
    fDrawPad->Clear();
    fDrawPad->SetLogx(0);
    fDrawPad->SetLogy(0);
    fDrawPad->SetLogz(0);
    fInfoPad->Clear();
    fLegendPad->Clear();
    PrintDebugInfo("   + clear all pads");
    
    // InitializePads();
    
    if (fInfoTitle) delete fInfoTitle;
    if (fTextInfo ) delete fTextInfo;
    if (fLegend   ) delete fLegend;
    if (fLegendPad) delete fLegendPad;
    if (fInfoPad  ) delete fInfoPad;
    if (fDrawPad  ) delete fDrawPad;
    PrintDebugInfo("   + delete pave boxes");
    
    // reset division ratio
    fDivisionRatio = 1.0;

    // pad for graphics (histograms, graphs, etc...)
    const Double_t DrawPad_xlow       = 0.;
    const Double_t DrawPad_ylow       = 0.;
    const Double_t DrawPad_xup        = 1.;
    const Double_t DrawPad_yup        = 1.;
    const Color_t  DrawPad_color      = 0;
    const Short_t  DrawPad_bordersize = 4;
    const Short_t  DrawPad_bordermode = 1;
    fDrawPad = new TPad("fDrawPad", "Draw Region",
  		      DrawPad_xlow, DrawPad_ylow, DrawPad_xup, DrawPad_yup,
  		      DrawPad_color, DrawPad_bordersize, DrawPad_bordermode );
    fDrawPad->SetFillStyle(4000); // transparent
    fDrawPad->Draw();
    PrintDebugInfo("   + pad for graphics is created");
    
    // pad for text information
    const Double_t InfoPad_xlow       = 0.70;
    const Double_t InfoPad_ylow       = 0.00;
    const Double_t InfoPad_xup        = 1.00;
    const Double_t InfoPad_yup        = 0.30;
    const Color_t  InfoPad_color      = 0;
    const Short_t  InfoPad_bordersize = 4;
    const Short_t  InfoPad_bordermode = 1;
    fInfoPad = new TPad("fInfoPad", "Info Region",
  		      InfoPad_xlow, InfoPad_ylow, InfoPad_xup, InfoPad_yup,
  		      InfoPad_color, InfoPad_bordersize, InfoPad_bordermode );
    fInfoPad->SetFillStyle(4000); // transparent
    fInfoPad->Draw();
    PrintDebugInfo("   + pad for text info is created");

    // pad for legend
    const Double_t LegendPad_xlow       = 0.55;
    const Double_t LegendPad_ylow       = 0.;
    const Double_t LegendPad_xup        = 1.;
    const Double_t LegendPad_yup        = fDrawPad->GetBottomMargin() - 0.10;
    const Color_t  LegendPad_color      = 0;
    const Short_t  LegendPad_bordersize = 4;
    const Short_t  LegendPad_bordermode = 1;
    // fLegendPad = new TPad("fLegendPad", "Legend Region", 0.50, 0, 1, 1-fDrawRegionHeight, 0, 4, 1); 
    fLegendPad = new TPad("fLegendPad", "Legend Region",
  			LegendPad_xlow, LegendPad_ylow, LegendPad_xup, LegendPad_yup,
  			LegendPad_color, LegendPad_bordersize, LegendPad_bordermode );
    fLegendPad->SetFillStyle(4000); // transparent
    fLegendPad->Draw();
    PrintDebugInfo("   + pad for legend is created");
    ///////////////
    
    /*** Region of Text Information ***/
    // title of information pad
    const Double_t InfoTitle_x1ndc = 0;
    const Double_t InfoTitle_y1ndc = 1 - 0.13*1.2;
    const Double_t InfoTitle_x2ndc = 1;
    const Double_t InfoTitle_y2ndc = 1;
    fInfoTitle = new TPaveText(InfoTitle_x1ndc, InfoTitle_y1ndc,
  			     InfoTitle_x2ndc, InfoTitle_y2ndc, "NDC");
    fInfoTitle->SetTextAlign(12);
    fInfoTitle->SetTextColor(kCyan+3);
    fInfoTitle->SetTextSize(0.13);
    fInfoTitle->SetFillStyle(0);
    fInfoTitle->SetBorderSize(0);
    fInfoTitle->SetLineColor(kCyan+3);
    
    fInfoTitle->AddText("#### Information ####");
    fInfoPad->cd();
    // fInfoTitle->Draw();
    PrintDebugInfo("   + set title of text information");

    // pave box for information
    const Double_t TextInfo_x1ndc = 0;
    const Double_t TextInfo_y1ndc = 0;
    const Double_t TextInfo_x2ndc = 1;
    const Double_t TextInfo_y2ndc = 1 - 0.13*1.2;
    fTextInfo = new TPaveText(TextInfo_x1ndc, TextInfo_y1ndc,
  			    TextInfo_x2ndc, TextInfo_y2ndc, "NDC");
    fTextInfo->SetTextAlign(12);
    fTextInfo->SetTextColor(kCyan+3);
    fTextInfo->SetTextSize(0.13);
    fTextInfo->SetFillStyle(0);
    fTextInfo->SetBorderSize(0);
    PrintDebugInfo("   + set pave box for text information");
    ///////////////
    
    /*** Legend Box ***/
    fLegend = new TLegend(0.00, 0.00, 1.00, 1.00);
    fLegend->SetTextSize(0.13);
    fLegend->SetFillColor(kWhite);
    // fLegend->SetFillStyle(0);
    fLegend->SetBorderSize(1);
    PrintDebugInfo("   + set legend box");
    ///////////////
    
    /*** set default stat divition ***/
    fStatNrow    = 2;
    fStatNcolumn = 1;
    // fStatNrow    = 3;
    // fStatNcolumn = 2;

    /*** Initialize Other Variables ***/
    fNhistos = 0;
    fNlegend = 0;
    fNstats  = 0;
    
    /*** clear divided flag ***/
    fIsDivided = kFALSE;

    PrintDebugInfo("<< KanaDrawManager::Clear END >>");
  }


  void KanaDrawManager::Divide(const Double_t division_ratio)
  {
    PrintDebugInfo("<< KanaDrawManager::Divide START >>");

    fDivisionRatio = division_ratio;
    
    /*** divide DrawPad ***/
    fDrawPad->Divide(1, 2);
    
    /*** set pad1 ***/
    TPad *pad1 = (TPad*)fDrawPad->GetPad(1);
    const Double_t pad1_x1 = 0.;
    const Double_t pad1_y1 = 1-fDivisionRatio;
    const Double_t pad1_x2 = 1.;
    const Double_t pad1_y2 = 1.;
    pad1->SetPad(pad1_x1, pad1_y1, pad1_x2, pad1_y2);
    pad1->SetFillStyle(4000);
    pad1->SetTickx(0);
    
    const Double_t pad1_right_margin  = gStyle->GetPadRightMargin();
    const Double_t pad1_left_margin   = gStyle->GetPadLeftMargin();
    const Double_t pad1_bottom_margin = 0;
    const Double_t pad1_top_margin    = 0.1 / fDivisionRatio;
    pad1->SetRightMargin(pad1_right_margin);
    pad1->SetLeftMargin(pad1_left_margin);
    pad1->SetBottomMargin(pad1_bottom_margin);
    pad1->SetTopMargin(pad1_top_margin);

    pad1->SetFillStyle(4000);
    
    
    /*** set pad2 ***/
    TPad *pad2 = (TPad*)fDrawPad->GetPad(2);
    const Double_t pad2_x1 = 0.;
    const Double_t pad2_y1 = 0.;
    const Double_t pad2_x2 = 1.;
    const Double_t pad2_y2 = 1-fDivisionRatio;
    pad2->SetPad(pad2_x1, pad2_y1, pad2_x2, pad2_y2);
    pad2->SetFillStyle(4000);
    pad2->SetTickx(0);
    
    const Double_t pad2_right_margin  = gStyle->GetPadRightMargin();
    const Double_t pad2_left_margin   = gStyle->GetPadLeftMargin();
    const Double_t pad2_bottom_margin = 0.1 / (1-fDivisionRatio);
    const Double_t pad2_top_margin    = 0;
    pad2->SetRightMargin(pad2_right_margin);
    pad2->SetLeftMargin(pad2_left_margin);
    pad2->SetBottomMargin(pad2_bottom_margin);
    pad2->SetTopMargin(pad2_top_margin);
    
    pad2->SetFillStyle(4000);


    /*** set info & legend pad ***/
    if (fInfoTitle) delete fInfoTitle;
    if (fTextInfo ) delete fTextInfo;
    if (fLegend   ) delete fLegend;
    if (fLegendPad) delete fLegendPad;
    if (fInfoPad  ) delete fInfoPad;
    PrintDebugInfo("   + delete pave boxes");
    
    fCanvas->cd();
    
    // pad for text information
    const Double_t InfoPad_xlow       = 0.55;
    const Double_t InfoPad_ylow       = 0.20;
    const Double_t InfoPad_xup        = 1.00;
    const Double_t InfoPad_yup        = 0.30;
    const Color_t  InfoPad_color      = 0;
    const Short_t  InfoPad_bordersize = 4;
    const Short_t  InfoPad_bordermode = 1;
    fInfoPad = new TPad("fInfoPad", "Info Region",
  		      InfoPad_xlow, InfoPad_ylow, InfoPad_xup, InfoPad_yup,
  		      InfoPad_color, InfoPad_bordersize, InfoPad_bordermode );
    fInfoPad->SetFillStyle(4000); // transparent
    fInfoPad->Draw();
    PrintDebugInfo("   + pad for text info is created");
    
    // pad for legend
    const Double_t LegendPad_xlow       = 0.55;
    const Double_t LegendPad_ylow       = 0.00;
    const Double_t LegendPad_xup        = 1.00;
    const Double_t LegendPad_yup        = 0.20;
    const Color_t  LegendPad_color      = 0;
    const Short_t  LegendPad_bordersize = 4;
    const Short_t  LegendPad_bordermode = 1;
    // fLegendPad = new TPad("fLegendPad", "Legend Region", 0.50, 0, 1, 1-fDrawRegionHeight, 0, 4, 1); 
    fLegendPad = new TPad("fLegendPad", "Legend Region",
  			LegendPad_xlow, LegendPad_ylow, LegendPad_xup, LegendPad_yup,
  			LegendPad_color, LegendPad_bordersize, LegendPad_bordermode );
    fLegendPad->SetFillStyle(4000); // transparent
    fLegendPad->Draw();
    PrintDebugInfo("   + pad for legend is created");
    ///////////////
    
    /*** Region of Text Information ***/
    // title of information pad
    const Double_t InfoPad_height = InfoPad_yup - InfoPad_ylow;
    const Double_t InfoPad_width  = InfoPad_xup - InfoPad_xlow;
    const Double_t InfoTitle_textsize = 0.13 * (0.30/InfoPad_height);
    const Double_t InfoTitle_x1       = 0;
    const Double_t InfoTitle_y1       = 1 - 0.13*1.2;
    const Double_t InfoTitle_x2       = 1;
    const Double_t InfoTitle_y2       = 1;
    // fInfoTitle = new TPaveText(0.05, 0.71, 0.45, 0.90, "NDC");
    fInfoTitle = new TPaveText(InfoTitle_x1, InfoTitle_y1, InfoTitle_x2, InfoTitle_y2, "NDC");
    fInfoTitle->SetTextAlign(12);
    fInfoTitle->SetTextColor(kCyan+3);
    fInfoTitle->SetTextSize(InfoTitle_textsize);
    fInfoTitle->SetFillStyle(0);
    fInfoTitle->SetBorderSize(0);
    fInfoTitle->SetLineColor(kCyan+3);
    
    fInfoTitle->AddText("##### Info #####");
    fInfoPad->cd();
    // fInfoTitle->Draw();
    PrintDebugInfo("   + set title of text information");
    
    // pave box for information
    const Double_t TextInfo_textsize = 0.13 * (0.30/InfoPad_height);
    const Double_t TextInfo_x1       = 0;
    const Double_t TextInfo_y1       = 0;
    const Double_t TextInfo_x2       = 1;
    const Double_t TextInfo_y2       = 1. - 0.13*1.2;
    // fTextInfo = new TPaveText(0.05, 0.05, 0.90, 0.69, "NDC");
    fTextInfo = new TPaveText(TextInfo_x1, TextInfo_y1, TextInfo_x2, TextInfo_y2, "NDC");
    fTextInfo->SetTextAlign(12);
    fTextInfo->SetTextColor(kCyan+3);
    // fTextInfo->SetTextSize(0.13 * 0.30/0.20);
    fTextInfo->SetTextSize(TextInfo_textsize);
    fTextInfo->SetFillStyle(0);
    fTextInfo->SetBorderSize(0);
    PrintDebugInfo("   + set pave box for text information");
    ///////////////
    
    /*** Legend Box ***/
    //const Double_t LegendPad_height = LegendPad_yup - LegendPad_ylow;
    //const Double_t LegendPad_width  = LegendPad_xup - LegendPad_xlow;
    const Double_t Legend_textsize =      (0.13*0.30)/InfoPad_height;
    const Double_t Legend_x1       =      (0.05*0.55)/InfoPad_width;
    const Double_t Legend_y1       =      (0.05*0.30)/InfoPad_height;
    const Double_t Legend_x2       = 1. - (0.05*0.55)/InfoPad_width;
    const Double_t Legend_y2       = 1. - (0.05*0.30)/InfoPad_height;
    // fLegend = new TLegend(0.05, 0.05, 0.95, 0.95);
    fLegend = new TLegend(Legend_x1, Legend_y1, Legend_x2, Legend_y2);
    // fLegend->SetTextSize(0.13 * 0.30/0.20);
    fLegend->SetTextSize(Legend_textsize);
    fLegend->SetFillColor(kWhite);
    // fLegend->SetFillStyle(0);
    fLegend->SetBorderSize(1);
    PrintDebugInfo("   + set legend box");
    ///////////////
    
    /*** set default stat divition ***/
    fStatNrow    = 2;
    fStatNcolumn = 1;
    // fStatNrow    = 3;
    // fStatNcolumn = 2;
    
    /*** Initialize Other Variables ***/
    fNhistos = 0;
    fNlegend = 0;
    fNstats  = 0;
    
    /*** set division flag ***/
    fIsDivided = kTRUE;
    
    PrintDebugInfo("<< KanaDrawManager::Divide END >>");
  }


  void KanaDrawManager::Draw(TObject *obj, Option_t *option, const Int_t pad_number)
  {
    fDrawPad->cd(pad_number);
    TString opt(option); opt.ToLower();
    if (opt.Contains("same")) {
      obj->Draw(opt.Data());
    } else {
      obj->Draw(opt.Data());
      // AdjustTitle(pad_number);
    }
    
    // fDrawPad->cd(pad_number);
    // obj->Draw(option);
    
    // AdjustTitle(pad_number);
  }


  void KanaDrawManager::Draw(TTree *tr, const char* varexp, const char* selection, Option_t* option,
  		    const Int_t pad_number)
  {
    Draw(tr, varexp, selection, option, 1000000000, 0, pad_number);
  }


  void KanaDrawManager::Draw(TTree *tr, const char* varexp, const char* selection,
  		    Option_t* option, Long64_t nentries, Long64_t firstentry,
  		    const Int_t pad_number)
  {
    fDrawPad->cd(pad_number);
    tr->Draw(varexp, selection, option, nentries, firstentry);
  }

  void KanaDrawManager::DrawGraph(TGraph *g, Option_t *option, const Int_t pad_number)
  {
    fDrawPad->cd(pad_number);
    
    TString opt(option);
    opt.ToLower();
    if (opt.Contains("a")) {
      
      g->Draw(opt.Data());
      
      // AdjustTitle(pad_number);
      
      // if (pad_number==1) {
      
      //   gPad->Update();
      //   TPaveText *title = (TPaveText*)gPad->GetPrimitive("title");
      
      //   const Double_t height = (title->GetY2NDC() - title->GetY1NDC()) / fDivisionRatio;
      //   const Double_t width  =  title->GetX2NDC() - title->GetX1NDC();
      //   // const Double_t width  = gStyle->GetTitleW();
      //   const Double_t x1 = 0.11;
      //   const Double_t y1 = (1 - 0.05/fDivisionRatio) - height*0.5;
      //   const Double_t x2 = x1 + width;
      //   const Double_t y2 = (1 - 0.05/fDivisionRatio) + height*0.5;
      //   title->SetX1NDC(x1);
      //   title->SetY1NDC(y1);
      //   title->SetX2NDC(x2);
      //   title->SetY2NDC(y2);
        
      // } else if (pad_number==2) {

      //   gPad->Update();
      //   TPaveText *title = (TPaveText*)gPad->GetPrimitive("title");
      //   title->SetBorderSize(0);
      //   title->SetX1NDC(0);
      //   title->SetY1NDC(0);
      //   title->SetX2NDC(0);
      //   title->SetY2NDC(0);

      // }

      g->GetHistogram()->SetStats(0);
      AdjustFrame(g->GetHistogram(), pad_number);
      
      fNstats = 0; // reset the number of stats
    } else {
      g->Draw(opt.Data());
    }
    
  }


  void KanaDrawManager::DrawFrame(const Double_t xmin, const Double_t ymin,
  			 const Double_t xmax, const Double_t ymax,
  			 const char *title,
  			 const Int_t pad_number )
  {
    PrintDebugInfo("<< KanaDrawManager::DrawFrame START >>");
    
    if (pad_number<0 || pad_number>2) {
      std::cerr << "ERROR: in KanaDrawManager::DrawFrame: pad number "
  	      << pad_number << "is not allowed, please choose [0, 2]" << std::endl;
      return;
    }

    fPadFrame[pad_number] = fDrawPad->cd(pad_number)->DrawFrame(xmin, ymin, xmax, ymax, title);
    PrintDebugInfo(" + draw frame");
    
    // AdjustTitle(pad_number);
    PrintDebugInfo(" + adjust title");
    
    AdjustFrame(fPadFrame[pad_number], pad_number);
    PrintDebugInfo(" + adjust frame");
    // TH1F *current_frame = fPadFrame[pad_number];
    // if (pad_number == 1) {
      
    //   const Double_t xlabel_size   = 0;
    //   const Double_t xtitle_size   = 0;
    //   const Double_t xtick_length  = 0;
    //   current_frame->GetXaxis()->SetLabelSize(xlabel_size);
    //   current_frame->GetXaxis()->SetTitleSize(xtitle_size);
    //   current_frame->GetXaxis()->SetTickLength(xtick_length);
      
    //   current_frame->GetYaxis()->CenterTitle();
    //   const Double_t ytitle_size   = gStyle->GetTitleSize("Y")   / fDivisionRatio;
    //   const Double_t ytitle_offset = gStyle->GetTitleOffset("Y") * fDivisionRatio;
    //   const Double_t ylabel_size   = gStyle->GetLabelSize("Y")   / fDivisionRatio;
    //   const Double_t ylabel_offset = gStyle->GetLabelOffset("Y") * fDivisionRatio;
    //   current_frame->GetYaxis()->SetTitleSize(ytitle_size);
    //   current_frame->GetYaxis()->SetTitleOffset(ytitle_offset);
    //   current_frame->GetYaxis()->SetLabelSize(ylabel_size);
    //   current_frame->GetYaxis()->SetLabelOffset(ylabel_offset);
      
    // } else if (pad_number == 2) {
      
    //   const Double_t xlabel_size   = gStyle->GetLabelSize("X")   / (1-fDivisionRatio);
    //   const Double_t xlabel_offset = gStyle->GetLabelOffset("X") * (1-fDivisionRatio);
    //   const Double_t xtitle_size   = gStyle->GetTitleSize("X")   / (1-fDivisionRatio);
    //   const Double_t xtitle_offset = gStyle->GetTitleOffset("X") * (1-fDivisionRatio);
    //   current_frame->GetXaxis()->SetLabelSize(xlabel_size);
    //   current_frame->GetXaxis()->SetLabelOffset(xlabel_offset);
    //   current_frame->GetXaxis()->SetTitleSize(xtitle_size);
    //   current_frame->GetXaxis()->SetTitleOffset(xtitle_offset);
      
    //   current_frame->GetYaxis()->CenterTitle();
    //   const Double_t ylabel_size   = gStyle->GetLabelSize("Y")   / (1-fDivisionRatio);
    //   const Double_t ylabel_offset = gStyle->GetLabelOffset("Y") * (1-fDivisionRatio);
    //   const Double_t ytitle_size   = gStyle->GetTitleSize("Y")   / (1-fDivisionRatio);
    //   const Double_t ytitle_offset = gStyle->GetTitleOffset("Y") * (1-fDivisionRatio);
    //   current_frame->GetYaxis()->SetLabelSize(ylabel_size);
    //   current_frame->GetYaxis()->SetLabelOffset(ylabel_offset);
    //   current_frame->GetYaxis()->SetTitleSize(ytitle_size);
    //   current_frame->GetYaxis()->SetTitleOffset(ytitle_offset);
      
    // }  

    PrintDebugInfo("<< KanaDrawManager::DrawFrame END >>");
  }


  void KanaDrawManager::DrawBox(TBox *box, const Double_t x1, const Double_t y1, const Double_t x2, const Double_t y2, const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::DrawBox START >>");
    
    // get border of drawing frame
    fDrawPad->cd(pad_number);
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->Update();

    Double_t xmin = pad->GetUxmin();
    Double_t ymin = pad->GetUymin();
    Double_t xmax = pad->GetUxmax();
    Double_t ymax = pad->GetUymax();
    if (pad->GetLogx()) {
      xmin = TMath::Power(10, pad->GetUxmin());
      xmax = TMath::Power(10, pad->GetUxmax());
    }
    if (pad->GetLogy()) {
      ymin = TMath::Power(10, pad->GetUymin());
      ymax = TMath::Power(10, pad->GetUymax());
    }
    
    const Double_t xlow = (x1<x2 ? x1 : x2);
    const Double_t ylow = (y1<y2 ? y1 : y2);
    const Double_t xup  = (x1>x2 ? x1 : x2);
    const Double_t yup  = (y1>y2 ? y1 : y2);
    
    if ( (xlow>xmax) || (ylow>ymax) || (xup<xmin) || (yup<ymin) ) {
      std::cerr << "Warning: in KanaDrawManager::DrawBox" << std::endl;
      std::cerr << "  - box lies out of frame"     << std::endl;
      return;
    }
    

    Double_t bx1, by1, bx2, by2;
    Bool_t xmin_out = kFALSE, ymin_out = kFALSE, xmax_out = kFALSE, ymax_out = kFALSE;
    if (xlow<xmin) { bx1 = xmin; xmin_out = kTRUE; }
    else           { bx1 = xlow; }
    if (ylow<ymin) { by1 = ymin; ymin_out = kTRUE; }
    else           { by1 = ylow; }
    if (xup>xmax ) { bx2 = xmax; xmax_out = kTRUE; }
    else           { bx2 = xup; }
    if (yup>ymax ) { by2 = ymax; ymax_out = kTRUE; }
    else           { by2 = yup; }
    
    /*** draw box ***/
    if (xmin_out || ymin_out || xmax_out || ymax_out) { // frame out
      
      // draw box without frame
      TBox box_temp(*box);
      box_temp.SetLineWidth(0);
      box_temp.DrawBox(bx1, by1, bx2, by2);

      // draw frame lines
      TLine line_temp;
      line_temp.SetLineColor(box->GetLineColor());
      line_temp.SetLineStyle(box->GetLineStyle());
      line_temp.SetLineWidth(box->GetLineWidth());
      if ( !xmin_out ) line_temp.DrawLine(bx1, by1, bx1, by2);
      if ( !ymin_out ) line_temp.DrawLine(bx1, by1, bx2, by1);
      if ( !xmax_out ) line_temp.DrawLine(bx2, by1, bx2, by2);
      if ( !ymax_out ) line_temp.DrawLine(bx1, by2, bx2, by2);

    } else { // inside drawing frame
      // draw box
      box->DrawBox(bx1, by1, bx2, by2);
    }
    
    
    
    // const Double_t corner[4][2] = {
    //   {(x1<x2 ? x1 : x2), (y1<y2 ? y1 : y2)}, // left bottom
    //   {(x1<x2 ? x2 : x1), (y1<y2 ? y1 : y2)}, // right bottom
    //   {(x1<x2 ? x2 : x1), (y1<y2 ? y2 : y1)}, // right top
    //   {(x1<x2 ? x1 : x2), (y1<y2 ? y2 : y1)}, // left top
    // };
    
    // if ( (corner[0][0]>xmax)||(corner[0][1]>ymax)
    //      /*|| (corner[1][0]<xmin)||(corner[1][1]>ymax)*/
    //      || (corner[2][0]<xmin)||(corner[2][1]<ymin)
    //      /*|| (corner[3][0]>xmax)||(corner[3][1]<ymin)*/ ) {
    //   std::cerr << "Warning: in KanaDrawManager::DrawBox" << std::endl;
    //   std::cerr << "  - box lies out of frame"     << std::endl;
    // }
    
    // Bool_t is_outside = kFALSE;
    // for (Int_t i=0; i < 4; ++i) {
    //   const Double_t x = corner[i][0];
    //   const Double_t y = corner[i][1];
    //   if ( (xmin<=x) && (x<=xmax) && (ymin<=y) && (y<=ymax) ) {
    //     is_inside = kTRUE;
    //     break;
    //   }
    // }
    
    // // check border & set box coordinates
    // Double_t bx1, by1, bx2, by2;
    // Bool_t xmin_out = kFALSE, ymin_out = kFALSE, xmax_out = kFALSE, ymax_out = kFALSE;
    // if ( x1<xmin ) { bx1 = xmin; xmin_out = kTRUE; }
    // else           { bx1 = x1; }
    // if ( y1<ymin ) { by1 = ymin; ymin_out = kTRUE; }
    // else           { by1 = y1; }
    // if ( x2>xmax ) { bx2 = xmax; xmax_out = kTRUE; }
    // else           { bx2 = x2; }
    // if ( y2>ymax ) { by2 = ymax; ymax_out = kTRUE; }
    // else           { by2 = y2; }
    
    // /*** draw box ***/
    // if (xmin_out || ymin_out || xmax_out || ymax_out) { // frame out

    //   // draw box without frame
    //   TBox box_temp(*box);
    //   box_temp.SetLineWidth(0);
    //   box_temp.DrawBox(bx1, by1, bx2, by2);

    //   // draw frame lines
    //   TLine line_temp;
    //   line_temp.SetLineColor(box->GetLineColor());
    //   line_temp.SetLineStyle(box->GetLineStyle());
    //   line_temp.SetLineWidth(box->GetLineWidth());
    //   if ( !xmin_out ) line_temp.DrawLine(bx1, by1, bx1, by2);
    //   if ( !ymin_out ) line_temp.DrawLine(bx1, by1, bx2, by1);
    //   if ( !xmax_out ) line_temp.DrawLine(bx2, by1, bx2, by2);
    //   if ( !ymax_out ) line_temp.DrawLine(bx1, by2, bx2, by2);

    // } else { // inside drawing frame
    //   // draw box
    //   box->DrawBox(bx1, by1, bx2, by2);
    // }

    PrintDebugInfo("<< KanaDrawManager::DrawBox END >>");
  }


  void KanaDrawManager::DrawEllipse(TEllipse *ellipse,
  			   const Double_t x1, const Double_t y1,
  			   const Double_t r1, const Double_t r2,
  			   const Double_t phimin, const Double_t phimax,
  			   const Double_t theta, const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::DrawEllipse START >>");
    
    fDrawPad->cd(pad_number);
    ellipse->DrawEllipse(x1, y1, r1, r2, phimin, phimax, theta);
    
    PrintDebugInfo("<< KanaDrawManager::DrawEllipse END >>");
  }


  void KanaDrawManager::DrawLine(TLine *line, const Double_t x1, const Double_t y1, const Double_t x2, const Double_t y2, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::DrawLine START");
    
    Double_t x[2] = {x1, x2};
    Double_t y[2] = {y1, y2};
    
    // // exchange (x1, y1) to (x2, y[1]) if x1 > x2
    // if (x[0] > x[1]) {
    //   const Double_t x_temp = x[0];
    //   x[0] = x[1];
    //   x[1] = x_temp;
    //   const Double_t y_temp = y[0];
    //   y[0] = y[1];
    //   y[1] = y_temp;
    // }
    
    // get border of drawing frame
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    
    pad->cd();
    pad->Update();
    
    Double_t xmin, xmax;
    if (pad->GetLogx()) {
      xmin = TMath::Power(10, pad->GetUxmin());
      xmax = TMath::Power(10, pad->GetUxmax());
    } else {
      xmin = pad->GetUxmin();
      xmax = pad->GetUxmax();
    }
    
    Double_t ymin, ymax; 
    if (pad->GetLogy()) {
      ymin = TMath::Power(10, pad->GetUymin());
      ymax = TMath::Power(10, pad->GetUymax());
    } else {
      ymin = pad->GetUymin();
      ymax = pad->GetUymax();
    }
    // const Double_t xmin = pad->GetUxmin();
    // const Double_t ymin = pad->GetUymin();
    // const Double_t xmax = pad->GetUxmax();
    // const Double_t ymax = pad->GetUymax();
    
    // check border of drawing frame and set end points of line
    Double_t lx[2];
    Double_t ly[2];
    if ((x[1]-x[0])==0.) { /** line is parallel to y-axis **/
      if (x[0]<xmin || x[0]>xmax) {
        std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
        std::cerr << "  - line lies out of frame"     << std::endl;
        return;
      }
      lx[0] = x[0]; lx[1] = x[1];
      if (y[0]>y[1]) { // sort y in increasing order
        const Double_t temp = y[0];
        y[0] = y[1];
        y[1] = temp;
      }
      if (y[0]>ymax || y[1]<ymin) {
        std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
        std::cerr << "  - line lies out of frame"     << std::endl;
        return;
      }
      ly[0] = (y[0]<ymin ? ymin : y[0]);
      ly[1] = (y[1]>ymax ? ymax : y[1]);
      // if ( y[0]<ymin ) ly[0] = ymin; else ly[0] = y[0];
      // if ( y[1]>ymax ) ly[1] = ymax; else ly[1] = y[1];
      // std::cout << ymin << " " << ymax << std::endl;
      // std::cout << y[0] << " " << y[1] << std::endl;
      // std::cout << ly[0] << " " << ly[1] << std::endl;
    } else if ((y[1]-y[0])==0.) { /** line is parallel to x-axis **/
      if (y[0]<ymin || y[0]>ymax) {
        std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
        std::cerr << "  - line lies out of frame"     << std::endl;
        return;
      }
      ly[0] = y[0]; ly[1] = y[0];
      if (x[0]>x[1]) { // sort x in increasing order
        const Double_t temp = x[0];
        x[0] = x[1];
        x[1] = temp;
      }
      if (x[0]>xmax || x[1]<xmin) {
        std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
        std::cerr << "  - line lies out of frame"     << std::endl;
        return;
      }
      lx[0] = (x[0]<xmin ? xmin : x[0]);
      lx[1] = (x[1]>xmax ? xmax : x[1]);
      // if ( x[0]<xmin ) lx[0] = xmin; else lx[0] = x[0];
      // if ( x[1]>xmax ) lx[1] = xmax; else lx[1] = x[1];

    } else { /** line is NOT parallel to either x-axis or y-axis **/

      for (Int_t i=0; i<2; ++i) { // i=0: (x1, y1), i=1: (x2, y2)
        if ( (x[i]>=xmin && x[i]<=xmax) && (y[i]>=ymin && y[i]<=ymax) ) { // inside frame
      	lx[i] = x[i];
      	ly[i] = y[i];
        } else { // outside frame
  	
      	// const Double_t k = (y[1]-y[0])/(x[1]-x[0]);
  	
      	// const Double_t y_xmin = (xmin-x[0])*k + y[0];
      	// const Double_t y_xmax = (xmax-x[0])*k + y[0];
      	// const Double_t x_ymin = (ymin-y[0])/k + x[0];
      	// const Double_t x_ymax = (ymax-y[0])/k + x[0];
  	
      	// Int_t n=0;
      	// if (ymin<=y_xmin && y_xmin<=ymax) {
      	//   lx[n] = xmin; ly[n] = y_xmin; ++n;
      	// }
      	// if (ymin<=y_xmax && y_xmax<=ymax) {
      	//   lx[n] = xmax; ly[n] = y_xmax; ++n;
      	// }
      	// if (ymin<=y_xmin && y_xmin<=ymax) {
      	//   lx[n] = xmin; ly[n] = ymin; ++n;
      	// }
      	// if (ymin<=y_xmin && y_xmin<=ymax) {
      	//   lx[n] = xmin; ly[n] = ymin; ++n;
      	// }
  	
      	const Double_t k = (y[1]-y[0])/(x[1]-x[0]);
      	if ( x[i]<xmin ) { // x1 is on left side of frame
      	  const Double_t y_left = k*(xmin-x[0])+y[0];
      	  if ( y_left>ymax ) {
      	    const Double_t x_top = (ymax-y[0])/k+x[0];
      	    if ( x_top>=xmin && x_top<=xmax ) {
      	      lx[i] = x_top;
      	      ly[i] = ymax;
      	    } else {
      	      std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
      	      std::cerr << "  - line lies out of frame"     << std::endl;
      	      return;
      	    }
      	  } else if ( y_left < ymin ) {
      	    const Double_t x_bottom = (ymin-y[0])/k+x[0];
      	    if ( x_bottom>=xmin && x_bottom<=xmax ) {
      	      lx[i] = x_bottom;
      	      ly[i] = ymin;
      	    } else {
      	      std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
      	      std::cerr << "  - line lies out of frame"     << std::endl;
      	      return;
      	    }
      	  } else { // y_left >= ymin && y_left <= ymax
      	    lx[i] = xmin;
      	    ly[i] = y_left;
      	  }
      	} else if ( x[i]>xmax ) { // x1 is on rigtht side of frame
      	  const Double_t y_right = k*(xmax-x[0])+y[0];
      	  if ( y_right>ymax ) {
      	    const Double_t x_top = (ymax-y[0])/k+x[0];
      	    if ( x_top>=xmin && x_top<=xmax ) {
      	      lx[i] = x_top;
      	      ly[i] = ymax;
      	    } else {
      	      std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
      	      std::cerr << "  - line lies out of frame"     << std::endl;
      	      return;
      	    }
      	  } else if ( y_right<ymin ) {
      	    const Double_t x_bottom = (ymin-y[0])/k+x[0];
      	    if ( x_bottom>=xmin && x_bottom<=xmax ) {
      	      lx[i] = x_bottom;
      	      ly[i] = ymin;
      	    } else {
      	      std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
      	      std::cerr << "  - line lies out of frame"     << std::endl;
      	      return;
      	    }
      	  } else { // y_right >= ymin && y_right <= ymax
      	    lx[i] = xmax;
      	    ly[i] = y_right;
      	  }
      	} else { // x is between xmin & xmax
      	  if ( y[i]>ymax ) {
      	    const Double_t x_top = (ymax-y[0])/k+x[0];
      	    if ( x_top>=xmin && x_top<=xmax ) {
      	      lx[i] = x_top;
      	      ly[i] = ymax;
      	    } else {
      	      std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
      	      std::cerr << "  - line lies out of frame"     << std::endl;
      	      return;
      	    }
      	  } else { // y < ymin
      	    const Double_t x_bottom = (ymin-y[0])/k+x[0];
      	    if ( x_bottom>=xmin && x_bottom<=xmax ) {
      	      lx[i] = x_bottom;
      	      ly[i] = ymin;
      	    } else {
      	      std::cerr << "Warning: in KanaDrawManager::DrawLine" << std::endl;
      	      std::cerr << "  - line lies out of frame"     << std::endl;
      	      return;
      	    }
      	  }
      	}
        }
      }    
    }
    
    // draw line
    pad->cd();
    line->DrawLine(lx[0], ly[0], lx[1], ly[1]);
    
    PrintDebugInfo("KanaDrawManager::DrawLine END");
  }

  // void KanaDrawManager::DrawLine(TLine *line, const Double_t x1, const Double_t y1, const Double_t x2, const Double_t y2, const Int_t pad_number)
  // {
  // #ifdef DEBUG
  //   std::cout << "DEBUG: KanaDrawManager::DrawLine START" << std::endl;
  // #endif

  //   // get border of drawing frame
  //   fDrawPad->cd(pad_number);
  //   TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
  //   pad->Update();

  //   const Double_t xmin = pad->GetUxmin();
  //   const Double_t ymin = pad->GetUymin();
  //   const Double_t xmax = pad->GetUxmax();
  //   const Double_t ymax = pad->GetUymax();

  //   // check border of drawing frame
  //   Double_t lx1, ly1, lx2, ly2;
  //   if (x1 < xmin) lx1 = xmin;
  //   else           lx1 = x1;
  //   if (y1 < ymin) ly1 = ymin;
  //   else           ly1 = y1;
  //   if (x2 > xmax) lx2 = xmax;
  //   else           lx2 = x2;
  //   if (y2 > ymax) ly2 = ymax;
  //   else           ly2 = y2;
  	  
  //   // draw line
  //   line->DrawLine(lx1, ly1, lx2, ly2);
    
  // #ifdef DEBUG
  //   std::cout << "DEBUG: KanaDrawManager::DrawLine END" << std::endl;
  // #endif
  // }


  void KanaDrawManager::DrawMarker(TMarker *marker, const Double_t x, const Double_t y, const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::DrawMarker START >>");
    
    // get border of drawing frame
    fDrawPad->cd(pad_number);
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->Update();

    Double_t xmin = pad->GetUxmin();
    Double_t ymin = pad->GetUymin();
    Double_t xmax = pad->GetUxmax();
    Double_t ymax = pad->GetUymax();
    if (pad->GetLogx()) {
      xmin = TMath::Power(10, pad->GetUxmin());
      xmax = TMath::Power(10, pad->GetUxmax());
    }
    if (pad->GetLogy()) {
      ymin = TMath::Power(10, pad->GetUymin());
      ymax = TMath::Power(10, pad->GetUymax());
    }
    
    // check border
    if ( (x<xmin) || (x>xmax) || (y<ymin) || (y>ymax) ) {
      std::cerr << "Warning: in KanaDrawManager::DrawMarker: out of drawing frame" << std::endl;
      return;
    }
    
    // draw marker
    marker->DrawMarker(x, y);
    
    PrintDebugInfo("<< KanaDrawManager::DrawMarker END >>");
  }


  void KanaDrawManager::DrawText(TText *text, const Double_t x, const Double_t y, const char *msg,
  			const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::DrawText START >>");
    
    // get border of drawing frame
    fDrawPad->cd(pad_number);
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->Update();
    
    text->DrawText(x, y, msg);
    
    PrintDebugInfo("<< KanaDrawManager::DrawText END >>");
  }


  void KanaDrawManager::DrawHisto(const char *hname, Option_t *option, const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::DrawHisto START >>");
    
    TH1 *histo = (TH1*)gDirectory->GetList()->FindObject(hname);
    if (!histo) {
      std::cout << "Warning: histogram \"" << hname << "\" does not exist" << std::endl;
      return;
    }

    PrintDebugInfo( std::string("get histo (name: ")+std::string(hname)+std::string(")") );
    
    /** draw histo **/
    fDrawPad->cd(pad_number);
    TString opt(option); opt.ToLower();
    if (opt.Contains("same")) {
      histo->Draw(opt.Data());
    } else {
      histo->Draw(opt.Data());
      
      // AdjustTitle(pad_number);
      
      fNstats = 0; // reset the number of stats
      AdjustFrame(histo, pad_number);
    }
    PrintDebugInfo( std::string("draw histo (name: ")+std::string(hname)+std::string(")") );
    
    /** set stat **/
    SetStat(histo, pad_number);
    // fCanvas->Update();
    // TPaveStats *ps = (TPaveStats*)histo->FindObject("stats");
    // if (ps) {
    //   if (fDivisionRatio<1.0) {
        
    //     TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
        
    //     const Double_t width  = (pad->GetRightMargin()-0.02) / static_cast<Double_t>(fStatNcolumn);
    //     const Double_t height = (0.60-0.04/fDivisionRatio)      / static_cast<Double_t>(fStatNrow);
    //     const Double_t x1     = (1-pad->GetRightMargin()+0.02) + (fNstats/fStatNcolumn)   * width;
    //     const Double_t y1     = (1-0.04/fDivisionRatio)        - ((fNstats-1)%fStatNrow+1) * height;
    //     const Double_t x2     = x1 + width*0.95;
    //     const Double_t y2     = y1 + height*0.95;
    //     ps->SetX1NDC(x1);
    //     ps->SetY1NDC(y1);
    //     ps->SetX2NDC(x2);
    //     ps->SetY2NDC(y2);
        
    //   } else {
        
    //     TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);

    //     const Double_t width  = (pad->GetRightMargin()-0.02) / static_cast<Double_t>(fStatNcolumn);
    //     const Double_t height = 0.60 / static_cast<Double_t>(fStatNrow);
    //     const Double_t x1     = (1-pad->GetRightMargin()+0.02) + (fNstats/fStatNcolumn) * width;
    //     const Double_t y1     = 0.96 - ((fNstats-1)/fStatNrow+1) * height;
    //     const Double_t x2     = x1 + width*0.95;
    //     const Double_t y2     = y1 + height*0.95;
    //     ps->SetX1NDC(x1);
    //     ps->SetY1NDC(y1);
    //     ps->SetX2NDC(x2);
    //     ps->SetY2NDC(y2);

    //   }

    //   ps->SetLineColor(histo->GetLineColor());
    //   ps->SetTextColor(histo->GetLineColor());
    //   ++fNstats;
    // }
    PrintDebugInfo( std::string("set stats of histo (name: ")+std::string(hname)+std::string(")") );
    PrintDebugInfo("<< KanaDrawManager::DrawHisto END>>");
  }


  void KanaDrawManager::DrawHisto(TH1 *h, Option_t *option, const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::DrawH START >>");
    
    if (!h) {
      std::cout << "Warning: null pointer is specified" << std::endl;
      return;
    }
    TH1 *histo = h;
    
    /** draw histo **/
    fDrawPad->cd(pad_number);
    TString opt(option); opt.ToLower();
    if (opt.Contains("same")) {
      histo->Draw(opt.Data());
    } else {
      histo->Draw(opt.Data());

      // AdjustTitle(pad_number);
      
      fNstats = 0; // reset the number of stats
      AdjustFrame(histo, pad_number);
    }
    PrintDebugInfo( std::string("draw histo (name: ")+std::string(histo->GetName())+std::string(")") );
    
    /** set stat **/
    SetStat(histo, pad_number);
    // fCanvas->Update();
    // //fDrawPad->Update();
    // TPaveStats *ps = (TPaveStats*)histo->FindObject("stats");
    // if (ps) {
    //   // if (fDivisionRatio<1.0) {
    //   if (fIsDivided) {
        
    //     TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);

    //     const Double_t width  = (pad->GetRightMargin()-0.02) / static_cast<Double_t>(fStatNcolumn);
    //     const Double_t height = (0.60-0.04/fDivisionRatio)      / static_cast<Double_t>(fStatNrow);
    //     const Double_t x1     = (1-pad->GetRightMargin()+0.02) + (fNstats/fStatNcolumn)   * width;
    //     const Double_t y1     = (1-0.04/fDivisionRatio)        - ((fNstats-1)%fStatNrow+1) * height;
    //     const Double_t x2     = x1 + width*0.95;
    //     const Double_t y2     = y1 + height*0.95;
    //     // const Double_t width  = 0.43                    / static_cast<Double_t>(fStatNcolumn);
    //     // const Double_t height = (1-0.08/fDivisionRatio) / static_cast<Double_t>(fStatNraw);
    //     // const Double_t x1 = 0.57 + (fNstats / fStatNraw) * width;
    //     // const Double_t y1 = (1-0.04/fDivisionRatio) - (fNstats % fStatNraw) * height - height;
    //     // const Double_t x2 = x1 + width*0.95;
    //     // const Double_t y2 = y1 + height*0.95;
    //     ps->SetX1NDC(x1);
    //     ps->SetY1NDC(y1);
    //     ps->SetX2NDC(x2);
    //     ps->SetY2NDC(y2);
        
    //   } else {
        
    //     TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);

    //     const Double_t width  = (pad->GetRightMargin()-0.02) / static_cast<Double_t>(fStatNcolumn);
    //     const Double_t height = 0.60 / static_cast<Double_t>(fStatNrow);
    //     const Double_t x1     = (1-pad->GetRightMargin()+0.02) + (fNstats/fStatNcolumn) * width;
    //     const Double_t y1     = 0.96 - ((fNstats-1)/fStatNrow+1) * height;
    //     const Double_t x2     = x1 + width*0.95;
    //     const Double_t y2     = y1 + height*0.95;
    //     // const Double_t width  = 0.43 / static_cast<Double_t>(fStatNcolumn);
    //     // const Double_t height = 0.65 / static_cast<Double_t>(fStatNraw);
    //     // const Double_t x1 = 0.57 + (fNstats / fStatNraw) * width;
    //     // const Double_t y1 = 0.96 - (fNstats % fStatNraw) * height - height;
    //     // const Double_t x2 = x1 + width*0.95;
    //     // const Double_t y2 = y1 + height*0.95;
    //     ps->SetX1NDC(x1);
    //     ps->SetY1NDC(y1);
    //     ps->SetX2NDC(x2);
    //     ps->SetY2NDC(y2);

    //   }

    //   // const Double_t width  = 0.43 / static_cast<Double_t>(fStatNcolumn);
    //   // const Double_t height = 0.65 / static_cast<Double_t>(fStatNraw);
    //   // const Double_t x1 = 0.57 + (fNstats / fStatNraw) * width;
    //   // const Double_t y1 = 0.96 - (fNstats % fStatNraw) * height - height;
    //   // const Double_t x2 = x1 + width*0.95;
    //   // const Double_t y2 = y1 + height*0.95;
    //   // ps->SetX1NDC(x1);
    //   // ps->SetY1NDC(y1);
    //   // ps->SetX2NDC(x2);
    //   // ps->SetY2NDC(y2);
    //   ps->SetLineColor(histo->GetLineColor());
    //   ps->SetTextColor(histo->GetLineColor());
    //   ++fNstats;
    // }
    PrintDebugInfo( std::string("set stats of histo (name: ")+std::string(histo->GetName())+std::string(")") );
    PrintDebugInfo("<< KanaDrawManager::DrawHisto END>>");
  }

  void KanaDrawManager::DrawHisto2D(const char *hname, Option_t *option, const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::DrawHisto2D START >>");
    
    TH2 *histo = (TH2*)gDirectory->GetList()->FindObject(hname);
    if (!histo) {
      std::cout << "Warning: histogram \"" << hname << "\" does not exist" << std::endl;
      return;
    }
    PrintDebugInfo( std::string("get histo (name: ")+std::string(hname)+std::string(")") );
    
    /** draw histo **/
    fDrawPad->cd(pad_number);
    TString opt(option); opt.ToLower();
    if (opt.Contains("same")) {
      histo->Draw(opt.Data());
    } else {
      histo->Draw(opt.Data());

      // AdjustTitle(pad_number);

      fNstats = 0; // reset the number of stats
      AdjustFrame(histo, pad_number);
    }
    PrintDebugInfo( std::string("draw histo (name: ")+std::string(hname)+std::string(")") );
    
    /** set stat **/
    SetStat(histo, pad_number);
    // fCanvas->Update();
    // TPaveStats *ps = (TPaveStats*)histo->FindObject("stats");
    // if (ps) {
    //   // if (fDivisionRatio<1.0) {
    //   if (fIsDivided) {
    
    //     TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    
    //     const Double_t width  = (pad->GetRightMargin()-0.02) / static_cast<Double_t>(fStatNcolumn);
    //     const Double_t height = (0.60-0.04/fDivisionRatio)      / static_cast<Double_t>(fStatNrow);
    //     const Double_t x1     = (1-pad->GetRightMargin()+0.02) + (fNstats/fStatNcolumn)   * width;
    //     const Double_t y1     = (1-0.04/fDivisionRatio)        - ((fNstats-1)%fStatNrow+1) * height;
    //     const Double_t x2     = x1 + width*0.95;
    //     const Double_t y2     = y1 + height*0.95;
    //     // const Double_t width  = 0.43                    / static_cast<Double_t>(fStatNcolumn);
    //     // const Double_t height = (1-0.08/fDivisionRatio) / static_cast<Double_t>(fStatNraw);
    //     // const Double_t x1 = 0.57 + (fNstats / fStatNraw) * width;
    //     // const Double_t y1 = (1-0.04/fDivisionRatio) - (fNstats % fStatNraw) * height - height;
    //     // const Double_t x2 = x1 + width*0.95;
    //     // const Double_t y2 = y1 + height*0.95;
    //     ps->SetX1NDC(x1);
    //     ps->SetY1NDC(y1);
    //     ps->SetX2NDC(x2);
    //     ps->SetY2NDC(y2);
        
    //   } else {
        
    //     TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);

    //     const Double_t width  = (pad->GetRightMargin()-0.02) / static_cast<Double_t>(fStatNcolumn);
    //     const Double_t height = 0.6 / static_cast<Double_t>(fStatNrow);
    //     const Double_t x1     = (1-pad->GetRightMargin()+0.02) + (fNstats/fStatNcolumn) * width;
    //     const Double_t y1     = 0.96 - ((fNstats-1)/fStatNrow+1) * height;
    //     const Double_t x2     = x1 + width*0.95;
    //     const Double_t y2     = y1 + height*0.95;
    //     // const Double_t width  = 0.43 / static_cast<Double_t>(fStatNcolumn);
    //     // const Double_t height = 0.65 / static_cast<Double_t>(fStatNraw);
    //     // const Double_t x1 = 0.57 + (fNstats / fStatNraw) * width;
    //     // const Double_t y1 = 0.96 - (fNstats % fStatNraw) * height - height;
    //     // const Double_t x2 = x1 + width*0.95;
    //     // const Double_t y2 = y1 + height*0.95;
    //     ps->SetX1NDC(x1);
    //     ps->SetY1NDC(y1);
    //     ps->SetX2NDC(x2);
    //     ps->SetY2NDC(y2);

    //   }

    //   // const Double_t width  = 0.43 / static_cast<Double_t>(fStatNcolumn);
    //   // const Double_t height = 0.65 / static_cast<Double_t>(fStatNraw);
    //   // const Double_t x1 = 0.57 + (fNstats / fStatNraw) * width;
    //   // const Double_t y1 = 0.96 - (fNstats % fStatNraw) * height - height;
    //   // const Double_t x2 = x1 + width*0.95;
    //   // const Double_t y2 = y1 + height*0.95;
    //   // ps->SetX1NDC(x1);
    //   // ps->SetY1NDC(y1);
    //   // ps->SetX2NDC(x2);
    //   // ps->SetY2NDC(y2);
    //   ps->SetLineColor(histo->GetLineColor());
    //   ps->SetTextColor(histo->GetLineColor());
    //   ++fNstats;
    // }
    PrintDebugInfo( std::string("set stats of histo (name: ")+std::string(hname)+std::string(")") );
    PrintDebugInfo("<< KanaDrawManager::DrawHisto2D END>>");
  }

  void KanaDrawManager::DrawInfo(Option_t *option)
  {
    PrintDebugInfo("KanaDrawManager::DrawInfo START");
    
    fInfoPad->cd();
    fInfoTitle->Draw();
    fTextInfo->Draw(option);
    
    PrintDebugInfo("KanaDrawManager::DrawInfo END");
  }

  void KanaDrawManager::DrawLegend(Option_t *option)
  {
    PrintDebugInfo("KanaDrawManager::DrawLegend START");

    fLegendPad->cd();
    // fLegend->SetNColumns( 1 + (fNlegend-1)/3 );
    fLegend->Draw(option);

    PrintDebugInfo("KanaDrawManager::DrawLegend END");
  }



  void KanaDrawManager::FitHisto(const char *hname, const char *formula, Option_t *option, Option_t *goption, const Double_t xmin, const Double_t xmax)
  {
    PrintDebugInfo("KanaDrawManager::FitHisto START");

    TH1 *histo = (TH1*)gDirectory->GetList()->FindObject(hname);
    histo->Fit(formula, option, goption, xmin, xmax);

    PrintDebugInfo("KanaDrawManager::FitHisto END");
  }

  void KanaDrawManager::FitHisto(const char *hname, TF1 *f1, Option_t *option, Option_t *goption, const Double_t xmin, const Double_t xmax)
  {
    PrintDebugInfo("KanaDrawManager::FitHisto START");

    TH1 *histo = (TH1*)gDirectory->GetList()->FindObject(hname);
    histo->Fit(f1, option, goption, xmin, xmax);

    PrintDebugInfo("KanaDrawManager::FitHisto END");
  }

  TAxis* KanaDrawManager::GetXaxis(const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::GetXaxis START");
    
    if (!fPadFrame[pad_number]) {
      std::cout << "ERROR: in TManger::GetXaxis" << std::endl;
      std::cout << "  - use KanaDrawManager::DrawFrame or draw primitive with frame before calling this method" << std::endl;
      return NULL;
    }
    return fPadFrame[pad_number]->GetXaxis();
    
    PrintDebugInfo("KanaDrawManager::GetXaxis END");
  }


  TAxis* KanaDrawManager::GetYaxis(const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::GetYaxis START");
    
    if (!fPadFrame[pad_number]) {
      std::cout << "ERROR: in TManger::GetYaxis" << std::endl;
      std::cout << "  - use KanaDrawManager::DrawFrame or draw primitive with frame before calling this method" << std::endl;
      return NULL;
    }
    return fPadFrame[pad_number]->GetYaxis();
    
    PrintDebugInfo("KanaDrawManager::GetYaxis END");
  }


  TAxis* KanaDrawManager::GetZaxis(const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::GetZaxis START");
    
    if (!fPadFrame[pad_number]) {
      std::cout << "ERROR: in TManger::GetZaxis" << std::endl;
      std::cout << "  - use KanaDrawManager::DrawFrame or draw primitive with frame before calling this method" << std::endl;
      return NULL;
    }
    return fPadFrame[pad_number]->GetZaxis();
    
    PrintDebugInfo("KanaDrawManager::GetZaxis END");
  }


  void KanaDrawManager::OpenNewPDF(const char *pdf_name)
  {
    PrintDebugInfo("KanaDrawManager::OpenNewPDF START");
    
    if ( !std::string(pdf_name).size() ) { // null string is invalid
      std::cerr << "ERROR: in KanaDrawManager::OpenNewPDF: PDF name with null string is not allowed"
  	      << std::endl;
      return;
    }
    
    // if (fPDFName!=NULL) {
    //   if (fCanvas!=NULL) { // close PDF file
    //     std::string close_pdf = std::string(fPDFName->Data()) + "]";
    //     PrintDebugInfo("Close PDF: " + close_pdf);
    //     fCanvas->Print(close_pdf.c_str());
    //   }
    //   delete fPDFName;
    //   fPDFName = NULL;
    // }
    ClosePDF();
    
    /*** Open Output PDF (create main canvas)***/
    fPDFName = new TString(pdf_name);
    if (fCanvas==NULL) {
      fCanvas  = new TCanvas("fCanvas", "Main Canvas (supported by KanaDrawManager)");
    }
    std::string open_pdf = std::string(fPDFName->Data()) + "[";
    PrintDebugInfo("Open PDF: " + open_pdf);
    fCanvas->Print(open_pdf.c_str());
    
    PrintDebugInfo("main canvas is created");
    
    PrintDebugInfo("KanaDrawManager::OpenNewPDF END");
  }


  void KanaDrawManager::PrintCanvas()
  {
    PrintDebugInfo("KanaDrawManager::PrintCanvas START");
    
    // adjust title
    AdjustTitle(0);
    if (fIsDivided) {
      AdjustTitle(1);
      AdjustTitle(2);
    }
    PrintDebugInfo(" + Adjust Title");

    if (fIsDivided) {
      fDrawPad->cd(1)->Modified();
      fDrawPad->cd(2)->Modified();
    }
    fDrawPad->Modified();
    
    fInfoPad->Modified();
    fLegendPad->Modified();

    fCanvas->Modified();
    fCanvas->Update();
    fCanvas->Print(fPDFName->Data());
    PrintDebugInfo("KanaDrawManager::PrintCanvas END");
  }


  void KanaDrawManager::SetInfoPad(const Double_t xlow, const Double_t ylow,
  			  const Double_t xup,  const Double_t yup )
  {
    // fCanvas->cd();
    fInfoPad->SetPad(xlow, ylow, xup, yup);
    
    //const Double_t width  = xup - xlow;
    const Double_t height = yup - ylow;
    
    const Double_t InfoTitle_textsize = 0.13 * (0.30/height);
    const Double_t InfoTitle_x1       = 0;
    const Double_t InfoTitle_y1       = 1 - 0.13*1.2;
    const Double_t InfoTitle_x2       = 1;
    const Double_t InfoTitle_y2       = 1;
    fInfoTitle->SetTextSize(InfoTitle_textsize);
    fInfoTitle->SetX1NDC(InfoTitle_x1);
    fInfoTitle->SetY1NDC(InfoTitle_y1);
    fInfoTitle->SetX2NDC(InfoTitle_x2);
    fInfoTitle->SetY2NDC(InfoTitle_y2);
    
    const Double_t TextInfo_textsize = 0.13 * (0.30/height);
    const Double_t TextInfo_x1       = 0;
    const Double_t TextInfo_y1       = 0;
    const Double_t TextInfo_x2       = 1;
    const Double_t TextInfo_y2       = 1. - 0.13*1.2;
    fTextInfo->SetTextSize(TextInfo_textsize);
    fTextInfo->SetX1NDC(TextInfo_x1);
    fTextInfo->SetY1NDC(TextInfo_y1);
    fTextInfo->SetX2NDC(TextInfo_x2);
    fTextInfo->SetY2NDC(TextInfo_y2);
  }


  void KanaDrawManager::SetLegendPad(const Double_t xlow, const Double_t ylow,
  			    const Double_t xup,  const Double_t yup )
  {
    fLegendPad->SetPad(xlow, ylow, xup, yup);

    const Double_t width  = xup - xlow;
    const Double_t height = yup - ylow;

    const Double_t Legend_textsize =      (0.13*0.30)/height;
    const Double_t Legend_x1       =      (0.05*0.45)/width;
    const Double_t Legend_y1       =      (0.05*0.30)/height;
    const Double_t Legend_x2       = 1. - (0.05*0.45)/width;
    const Double_t Legend_y2       = 1. - (0.05*0.30)/height;
    fLegend->SetTextSize(Legend_textsize);
    fLegend->SetX1NDC(Legend_x1);
    fLegend->SetY1NDC(Legend_y1);
    fLegend->SetX2NDC(Legend_x2);
    fLegend->SetY2NDC(Legend_y2);
  }


  void KanaDrawManager::SetLegendHeader(const char *header)
  {
    PrintDebugInfo("KanaDrawManager::SetLegendHeader START");
    fLegend->SetHeader(header);
    PrintDebugInfo("KanaDrawManager::SetLegendHeader END");
  }

  void KanaDrawManager::SetLegendNColumns(Int_t n_columns)
  {
    PrintDebugInfo("KanaDrawManager::SetLegendNColumns START");
    fLegend->SetNColumns(n_columns);
    PrintDebugInfo("KanaDrawManager::SetLegendNColumns END");
  }

  void KanaDrawManager::SetLogx(const Int_t value, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::SetLogx START");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->SetLogx(value);

    PrintDebugInfo("KanaDrawManager::SetLogx END");
  }

  void KanaDrawManager::SetLogy(const Int_t value, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::SetLogy START");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->SetLogy(value);

    PrintDebugInfo("KanaDrawManager::SetLogy END");
  }

  void KanaDrawManager::SetLogz(const Int_t value)
  {
    PrintDebugInfo("KanaDrawManager::SetLogz START");
    
    fDrawPad->SetLogz(value);

    PrintDebugInfo("KanaDrawManager::SetLogz END");
  }


  void KanaDrawManager::SetOptStat(TH1 *h, Int_t stat)
  {
    PrintDebugInfo("KanaDrawManager::SetStat START");
    fCanvas->Update();
    TPaveStats *ps = (TPaveStats*)h->FindObject("stats");
    if (ps!=NULL) ps->SetOptStat(stat);
    PrintDebugInfo("KanaDrawManager::SetStat END");
  }


  void KanaDrawManager::SetPadGridx(Int_t value, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::SetPadGridx START");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->SetGridx(value);
    
    PrintDebugInfo("KanaDrawManager::SetPadGridx END");
  }

  void KanaDrawManager::SetPadGridy(Int_t value, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::SetPadGridy START");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->SetGridy(value);
    
    PrintDebugInfo("KanaDrawManager::SetPadGridy END");
  }

  void KanaDrawManager::SetPadGrid(Int_t valuex, Int_t valuey, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::SetPadGrid START");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->SetGrid(valuex, valuey);
    
    PrintDebugInfo("KanaDrawManager::SetPadGrid END");
  }


  void KanaDrawManager::SetPadLeftMargin(Float_t leftmargin, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::SetPadLeftMargin START");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->SetLeftMargin(leftmargin);
    
    PrintDebugInfo("KanaDrawManager::SetPadLeftMargin END");
  }


  void KanaDrawManager::SetPadRightMargin(Float_t rightmargin, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::SetPadRightMargin START");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->SetRightMargin(rightmargin);
    
    PrintDebugInfo("KanaDrawManager::SetPadRightMargin END");
  }


  void KanaDrawManager::SetPadBottomMargin(Float_t bottommargin, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::SetPadBottomMargin START");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->SetBottomMargin(bottommargin);
    
    PrintDebugInfo("KanaDrawManager::SetPadBottomMargin END");
  }


  void KanaDrawManager::SetPadTopMargin(Float_t topmargin, const Int_t pad_number)
  {
    PrintDebugInfo("KanaDrawManager::SetPadTopMargin START");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    pad->SetTopMargin(topmargin);
    
    PrintDebugInfo("KanaDrawManager::SetPadTopMargin END");
  }


  void KanaDrawManager::SetStatColor(const char *hname, Color_t color)
  {
    PrintDebugInfo("<< KanaDrawManager::SetStatColor START >>");
    
    fDrawPad->Update();
    TH1 *histo = (TH1*)gDirectory->GetList()->FindObject(hname);
    TPaveStats *ps = (TPaveStats*)histo->FindObject("stats");
    if (ps) {
      ps->SetLineColor(color);
      ps->SetTextColor(color);
    }

    PrintDebugInfo("<< KanaDrawManager::SetStatColor END >>");
  }

  void KanaDrawManager::SetStatColor(TH1 *h, Color_t color)
  {
    PrintDebugInfo("<< KanaDrawManager::SetStatColor START >>");
    
    fDrawPad->Update();
    TPaveStats *ps = (TPaveStats*)h->FindObject("stats");
    if (ps) {
      ps->SetLineColor(color);
      ps->SetTextColor(color);
    }

    PrintDebugInfo("<< KanaDrawManager::SetStatColor END >>");
  }

  void KanaDrawManager::SetStatColor(TGraph *g, Color_t color)
  {
    PrintDebugInfo("<< KanaDrawManager::SetStatColor START >>");
    
    fDrawPad->Update();
    TPaveStats *ps = (TPaveStats*)g->GetListOfFunctions()->FindObject("stats");
    if (ps) {
      ps->SetLineColor(color);
      ps->SetTextColor(color);
    }

    PrintDebugInfo("<< KanaDrawManager::SetStatColor END >>");
  }


  void KanaDrawManager::SetStatPos(const char *hname, const Double_t x1, const Double_t y1, const Double_t x2, const Double_t y2)
  {
    PrintDebugInfo("<< KanaDrawManager::SetStatPos START >>");
    
    fDrawPad->Update();
    TH1 *histo = (TH1*)gDirectory->GetList()->FindObject(hname);
    TPaveStats *ps = (TPaveStats*)histo->FindObject("stats");
    if (ps) {
      if (fIsDivided) {
        
        const Double_t stat_x1 = x1;
        const Double_t stat_y1 = 1. - (1. - y1)/fDivisionRatio;
        const Double_t stat_x2 = x2;
        const Double_t stat_y2 = 1. - (1. - y2)/fDivisionRatio;
        
        ps->SetX1NDC(stat_x1);
        ps->SetY1NDC(stat_y1);
        ps->SetX2NDC(stat_x2);
        ps->SetY2NDC(stat_y2);
        
      } else {

        ps->SetX1NDC(x1);
        ps->SetY1NDC(y1);
        ps->SetX2NDC(x2);
        ps->SetY2NDC(y2);

      }
    }
    
    PrintDebugInfo("<< KanaDrawManager::SetStatPos END >>");
  }

  void KanaDrawManager::SetStatPos(TH1 *histo, const Double_t x1, const Double_t y1, const Double_t x2, const Double_t y2)
  {
    PrintDebugInfo("<< KanaDrawManager::SetStatPos START >>");
    
    fDrawPad->Update();
    TPaveStats *ps = (TPaveStats*)histo->FindObject("stats");
    if (ps) {
      if (fIsDivided) {

        const Double_t stat_x1 = x1;
        const Double_t stat_y1 = 1. - (1. - y1)/fDivisionRatio;
        const Double_t stat_x2 = x2;
        const Double_t stat_y2 = 1. - (1. - y2)/fDivisionRatio;

        ps->SetX1NDC(stat_x1);
        ps->SetY1NDC(stat_y1);
        ps->SetX2NDC(stat_x2);
        ps->SetY2NDC(stat_y2);

      } else {

        ps->SetX1NDC(x1);
        ps->SetY1NDC(y1);
        ps->SetX2NDC(x2);
        ps->SetY2NDC(y2);

      }
    }
    // if (ps) {
    //   ps->SetX1NDC(x1);
    //   ps->SetY1NDC(y1);
    //   ps->SetX2NDC(x2);
    //   ps->SetY2NDC(y2);
    //   fDrawPad->Modified();
    // }
    
    PrintDebugInfo("<< KanaDrawManager::SetStatPos END >>");
  }

  void KanaDrawManager::SetStatPos(TGraph *g, const Double_t x1, const Double_t y1, const Double_t x2, const Double_t y2)
  {
    PrintDebugInfo("<< KanaDrawManager::SetStatPos START >>");

    fDrawPad->Update();
    TPaveStats *ps = (TPaveStats*)g->GetListOfFunctions()->FindObject("stats");
    if (ps) {
      if (fIsDivided) {
        
        const Double_t stat_x1 = x1;
        const Double_t stat_y1 = 1. - (1. - y1)/fDivisionRatio;
        const Double_t stat_x2 = x2;
        const Double_t stat_y2 = 1. - (1. - y2)/fDivisionRatio;
        
        ps->SetX1NDC(stat_x1);
        ps->SetY1NDC(stat_y1);
        ps->SetX2NDC(stat_x2);
        ps->SetY2NDC(stat_y2);

      } else {

        ps->SetX1NDC(x1);
        ps->SetY1NDC(y1);
        ps->SetX2NDC(x2);
        ps->SetY2NDC(y2);

      }
    }
    // if (ps) {
    //   ps->SetX1NDC(x1);
    //   ps->SetY1NDC(y1);
    //   ps->SetX2NDC(x2);
    //   ps->SetY2NDC(y2);
    //   fDrawPad->Modified();
    // }

    PrintDebugInfo("<< KanaDrawManager::SetStatPos END >>");
  }


  void KanaDrawManager::SetFitFormat(const char *hname, const char *format)
  {
    PrintDebugInfo("<< KanaDrawManager::SetFitFormat START >>");
    
    fDrawPad->Update();
    TH1 *histo = (TH1*)gDirectory->GetList()->FindObject(hname);
    TPaveStats *ps = (TPaveStats*)histo->FindObject("stats");
    if (ps) {
      ps->SetFitFormat(format);
    }
    
    PrintDebugInfo("<< KanaDrawManager::SetFitFormat END >>");
  }

  void KanaDrawManager::SetFitFormat(TH1 *histo, const char *format)
  {
    PrintDebugInfo("<< KanaDrawManager::SetFitFormat START >>");
    
    fDrawPad->Update();
    TPaveStats *ps = (TPaveStats*)histo->FindObject("stats");
    if (ps) {
      ps->SetFitFormat(format);
    }
    
    PrintDebugInfo("<< KanaDrawManager::SetFitFormat END >>");
  }

  void KanaDrawManager::SetFitFormat(TGraph *g, const char *format)
  {
    PrintDebugInfo("<< KanaDrawManager::SetFitFormat START >>");

    fDrawPad->Update();
    TPaveStats *ps = (TPaveStats*)g->GetListOfFunctions()->FindObject("stats");
    if (ps) {
      ps->SetFitFormat(format);
    }

    PrintDebugInfo("<< KanaDrawManager::SetFitFormat END >>");
  }


  void KanaDrawManager::SetStatDivision(const Int_t n_row, const Int_t n_column)
  {
    PrintDebugInfo("<< KanaDrawManager::SetStatDivision START >>");

    if (fNstats) std::cout << "Warning: in KanaDrawManager::SetStatDivision: This method should be called BEFORE any graphics are drawn" << std::endl;
    
    fStatNrow    = n_row;
    fStatNcolumn = n_column;

    PrintDebugInfo("<< KanaDrawManager::SetStatDivision END >>");
  }


  void KanaDrawManager::SetXTitle(const char *title, const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::SetXTitle START >>");
    
    if (!fPadFrame[pad_number]) {
      std::cout << "ERROR: in TManger::SetXTitle" << std::endl;
      std::cout << "  - use KanaDrawManager::DrawFrame or draw primitive with frame before calling this method" << std::endl;
      return;
    }
    fPadFrame[pad_number]->SetXTitle(title);
    
    PrintDebugInfo("<< KanaDrawManager::SetXTitle END >>");
  }

  void KanaDrawManager::SetYTitle(const char *title, const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::SetYTitle START >>");

    if (!fPadFrame[pad_number]) {
      std::cout << "ERROR: in TManger::SetYTitle" << std::endl;
      std::cout << "  - use KanaDrawManager::DrawFrame or draw primitive with frame before calling this method" << std::endl;
      return;
    }
    fPadFrame[pad_number]->SetYTitle(title);

    PrintDebugInfo("<< KanaDrawManager::SetYTitle END >>");
  }

  void KanaDrawManager::SetRootStyle()
  {
    const Double_t text_size = 0.05;

    /*** set default style ***/
    //gROOT->SetStyle("Plain");

    /** color palette **/
    gStyle->SetPalette(1, 0);
    
    /** axis **/
    gStyle->SetAxisColor(kBlack);
    
    /** bar **/
    gStyle->SetBarOffset(0.25);
    gStyle->SetBarWidth(0.5);
    
    /** canvas **/
    gStyle->SetCanvasBorderMode(0);
    //gStyle->SetCanvasBorderSize(10);
    gStyle->SetCanvasColor(0);   // bkg color of canvas
    gStyle->SetCanvasDefH(928);  // height of canvas including application frame
    gStyle->SetCanvasDefW(1804); // width of canvas including application frame
    gStyle->SetCanvasDefX(0);    // x position of upper-left edge of canvas
    gStyle->SetCanvasDefY(0);    // y position of upper-left edge of canvas
    
    /** date **/
    gStyle->SetOptDate(23);
    // gStyle->SetDateX(0.85);
    // gStyle->SetDateY(0.01);

    /** frame **/
    gStyle->SetFrameBorderMode(0);     // border mode of frame
    //gStyle->SetFrameBorderSize(10);  // border size of frame
    //gStyle->SetFrameFillColor(0);    // fill color inside frame
    gStyle->SetFrameFillStyle(0);      // fill style inside frame
    gStyle->SetFrameLineColor(kBlack); // line color of frame
    gStyle->SetFrameLineWidth(1);      // line width of frame
    
    /** grid **/
    gStyle->SetGridColor(kGray+1); // color of grid line
    
    /** histo ***/
    gStyle->SetHistFillStyle(3002);   // fill style of histo
    gStyle->SetHistLineColor(kBlack); // line color of histo
    // gStyle->SetHistLineWidth(2);      // line width of histo
    gStyle->SetHistLineWidth(1);      // line width of histo // 2016/06/27 modified: 2->1
    
    /** label **/
    gStyle->SetLabelColor(kBlack);
    gStyle->SetLabelFont(42, "X");
    gStyle->SetLabelFont(42, "Y");
    gStyle->SetLabelFont(42, "Z");
    gStyle->SetLabelSize(text_size, "X");
    gStyle->SetLabelSize(text_size, "Y");
    gStyle->SetLabelSize(text_size, "Z");
    
    /** legend **/
    gStyle->SetLegendBorderSize(1);
    //gStyle->SetLegendFillColor(0);
    //gStyle->SetLegendFont(42);
    
    /** pad **/
    gStyle->SetPadBorderMode(0);
    gStyle->SetPadBorderSize(1);
    gStyle->SetPadTopMargin(0.10);
    gStyle->SetPadBottomMargin(0.10);
    gStyle->SetPadRightMargin(0.30);
    // gStyle->SetPadBottomMargin(0.40);
    // gStyle->SetPadRightMargin(0.50);
    gStyle->SetPadLeftMargin(0.10);
    gStyle->SetPadColor(0);
    gStyle->SetPadGridX(1);
    gStyle->SetPadGridY(1);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    
    const Double_t xtick_length
      = 0.03 / ( (1-gStyle->GetPadLeftMargin()-gStyle->GetPadRightMargin()) / 0.8 ); // * ( 1-gStyle->GetPadTopMargin()-gStyle->GetPadBottomMargin() / 0.8 );
    const Double_t ytick_length
      = 0.03 * ( (1-gStyle->GetPadTopMargin()-gStyle->GetPadBottomMargin()) / 0.8 ); // * ( 1-gStyle->GetPadLeftMargin()-gStyle->GetPadRightMargin() / 0.8 );
    gStyle->SetTickLength(xtick_length, "X");
    gStyle->SetTickLength(ytick_length, "Y");
    
    /** stat **/
    gStyle->SetOptStat(1001111111);
    gStyle->SetDrawBorder(0); // maybe affect on stat box
    //gStyle->SetStatBorderSize(1); 
    gStyle->SetStatFont(42);
    gStyle->SetStatX(0.74);
    gStyle->SetStatY(0.95);
    gStyle->SetStatH(0.12);
    gStyle->SetStatW(0.19);

    /** text **/
    gStyle->SetTextFont(42);
    gStyle->SetTextSize(0.04);
    
    /** title **/
    gStyle->SetTitleAlign(12);
    gStyle->SetTitleBorderSize(1);
    gStyle->SetTitleFont(42, "X");
    gStyle->SetTitleFont(42, "Y");
    gStyle->SetTitleFont(42, "Z");
    gStyle->SetTitleFont(42, "T");
    gStyle->SetTitleSize(text_size, "X");
    gStyle->SetTitleSize(text_size, "Y");
    gStyle->SetTitleSize(text_size, "Z");
    gStyle->SetTitleSize(text_size*1.3, "T");
    gStyle->SetTitleOffset(0.035/gStyle->GetTitleSize("X")*1.3, "X");
    // gStyle->SetTitleOffset(0.035/gStyle->GetTitleSize("Y"),     "Y");
    gStyle->SetTitleOffset(1, "Y");
    //gStyle->SetTitleTextColor(kCyan+3);
    gStyle->SetTitleX(0.15);
    gStyle->SetTitleY(0.95);
    gStyle->SetTitleW(0.30);
    
    
    /** other options **/
    //gStyle->SetOptFile(1);
    gStyle->SetOptFit(1111);
    //gStyle->SetOptTitle(1);


    /** setting my color palette **/
    // const Int_t NRGBs = 5;
    // const Int_t Ncont = 255;
    
    // Double_t stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
    // Double_t red[NRGBs]   = {0.00, 0.00, 0.87, 1.00, 0.51};
    // Double_t green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
    // Double_t blue[NRGBs]  = {0.51, 1.00, 0.12, 0.00, 0.00};
    // TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, Ncont);
    // gStyle->SetNumberContours(Ncont);
    
    /** force current style **/
    gROOT->ForceStyle();
  }


  void KanaDrawManager::PrintDebugInfo(std::string message)
  {
    if (debug_flag) {
      std::cout << "DEBUG: " << message << std::endl;
    }
  }


  void KanaDrawManager::AdjustFrame(TH1 *frame, const Int_t pad_number)
  {
    if (pad_number == 0) {
      
      const Double_t pad_top_margin    = fDrawPad->GetTopMargin();
      const Double_t pad_bottom_margin = fDrawPad->GetBottomMargin();
      const Double_t pad_left_margin   = fDrawPad->GetLeftMargin();
      const Double_t pad_right_margin  = fDrawPad->GetRightMargin();
      
      const Double_t xtick_length = 0.01 / ( (1 - pad_left_margin - pad_right_margin) / 0.80) * 2;
      const Double_t ytick_length = 0.01 / ( (1 - pad_top_margin - pad_bottom_margin) / 0.80);
      frame->GetXaxis()->SetTickLength(xtick_length);
      frame->GetYaxis()->SetTickLength(ytick_length);
      
    } else if (pad_number == 1) {
      
      const Double_t xlabel_size   = 0;
      const Double_t xtitle_size   = 0;
      const Double_t xtick_length  = 0;
      frame->GetXaxis()->SetLabelSize(xlabel_size);
      frame->GetXaxis()->SetTitleSize(xtitle_size);
      frame->GetXaxis()->SetTickLength(xtick_length);
      
      frame->GetYaxis()->CenterTitle();
      const Double_t ytitle_size   = gStyle->GetTitleSize("Y")   / fDivisionRatio;
      const Double_t ytitle_offset = gStyle->GetTitleOffset("Y") * fDivisionRatio;
      const Double_t ylabel_size   = gStyle->GetLabelSize("Y")   / fDivisionRatio;
      //const Double_t ylabel_offset = gStyle->GetLabelOffset("Y") * fDivisionRatio;
      frame->GetYaxis()->SetTitleSize(ytitle_size);
      frame->GetYaxis()->SetTitleOffset(ytitle_offset);
      frame->GetYaxis()->SetLabelSize(ylabel_size);
      // frame->GetYaxis()->SetLabelOffset(ylabel_offset);

      TPad *pad = (TPad*)fDrawPad->GetPad(1);
      const Double_t pad_top_margin    = pad->GetTopMargin();
      const Double_t pad_bottom_margin = pad->GetBottomMargin();
      
      const Double_t ytick_length
        = 0.01 / ( (1 - pad_top_margin - pad_bottom_margin) / 0.80);
      frame->GetYaxis()->SetTickLength(ytick_length);

    } else if (pad_number == 2) {
      
      const Double_t xlabel_size   = gStyle->GetLabelSize("X")   / (1-fDivisionRatio);
      //const Double_t xlabel_offset = gStyle->GetLabelOffset("X") * (1-fDivisionRatio);
      const Double_t xtitle_size   = gStyle->GetTitleSize("X")   / (1-fDivisionRatio);
      //const Double_t xtitle_offset = gStyle->GetTitleOffset("X") * (1-fDivisionRatio);
      const Double_t xtick_length  = gStyle->GetTickLength("X")  / (1-fDivisionRatio);
      frame->GetXaxis()->SetLabelSize(xlabel_size);
      // frame->GetXaxis()->SetLabelOffset(xlabel_offset);
      frame->GetXaxis()->SetTitleSize(xtitle_size);
      // frame->GetXaxis()->SetTitleOffset(xtitle_offset);
      frame->GetXaxis()->SetTickLength(xtick_length);
      
      frame->GetYaxis()->CenterTitle();
      const Double_t ylabel_size   = gStyle->GetLabelSize("Y")   / (1-fDivisionRatio);
      //const Double_t ylabel_offset = gStyle->GetLabelOffset("Y") * (1-fDivisionRatio);
      const Double_t ytitle_size   = gStyle->GetTitleSize("Y")   / (1-fDivisionRatio);
      const Double_t ytitle_offset = gStyle->GetTitleOffset("Y") * (1-fDivisionRatio);
      frame->GetYaxis()->SetLabelSize(ylabel_size);
      // frame->GetYaxis()->SetLabelOffset(ylabel_offset);
      frame->GetYaxis()->SetTitleSize(ytitle_size);
      frame->GetYaxis()->SetTitleOffset(ytitle_offset);
      
      TPad *pad = (TPad*)fDrawPad->GetPad(2);
      const Double_t pad_top_margin    = pad->GetTopMargin();
      const Double_t pad_bottom_margin = pad->GetBottomMargin();
      
      const Double_t ytick_length
        = 0.01 / ( (1 - pad_top_margin - pad_bottom_margin) / 0.80);
      frame->GetYaxis()->SetTickLength(ytick_length);

    }  

  }


  void KanaDrawManager::AdjustTitle(const Int_t pad_number)
  {
    PrintDebugInfo("<< KanaDrawManager::AdjustTitle: START >>");
    
    fCanvas->Update();
    // fDrawPad->Update();
    PrintDebugInfo(" + pad updated");
    
    TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    TPaveText *title = (TPaveText*)pad->GetPrimitive("title");
    if (title) {
      if (pad_number == 0) {
        
        const Double_t height = 0.08;
        // const Double_t height = title->GetY2NDC() - title->GetY1NDC();
        // const Double_t height = (title->GetY2NDC() - title->GetY1NDC()) / fDivisionRatio;
        const Double_t x1 = pad->GetLeftMargin()  + 0.06;
        const Double_t y1 = (1 - 0.05) - height * 0.5;
        const Double_t x2 = (1 - pad->GetRightMargin()) - 0.06;
        const Double_t y2 = (1 - 0.05) + height * 0.5;
        title->SetX1NDC(x1);
        title->SetY1NDC(y1);
        title->SetX2NDC(x2);
        title->SetY2NDC(y2);
        
      } else if (pad_number == 1) {
        
        const Double_t height = 0.08 / fDivisionRatio;
        // const Double_t height = (title->GetY2NDC() - title->GetY1NDC()) / fDivisionRatio;
        const Double_t x1 = pad->GetLeftMargin()  + 0.06;
        const Double_t y1 = (1 - 0.05/fDivisionRatio) - height * 0.5;
        const Double_t x2 = (1 - pad->GetRightMargin()) - 0.06;
        const Double_t y2 = (1 - 0.05/fDivisionRatio) + height * 0.5;
        title->SetX1NDC(x1);
        title->SetY1NDC(y1);
        title->SetX2NDC(x2);
        title->SetY2NDC(y2);

      } else if (pad_number == 2) {

        title->SetX1NDC(0);
        title->SetY1NDC(0);
        title->SetX2NDC(0);
        title->SetY2NDC(0);

      }
    }
    
    // fDrawPad->cd(pad_number);
    
    // if (pad_number==1) {
      
    //   TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
    //   TPaveText *title = (TPaveText*)pad->GetPrimitive("title");
      
    //   if (title) {
    //     const Double_t height = (title->GetY2NDC() - title->GetY1NDC()) / fDivisionRatio;
    //     const Double_t width  =  title->GetX2NDC() - title->GetX1NDC();
    //     // const Double_t width  = gStyle->GetTitleW();
    //     const Double_t x1 = title->GetX1NDC();
    //     const Double_t y1 = (1 - 0.05/fDivisionRatio) - height*0.5;
    //     const Double_t x2 = x1 + width;
    //     const Double_t y2 = (1 - 0.05/fDivisionRatio) + height*0.5;
    //     title->SetX1NDC(x1);
    //     title->SetY1NDC(y1);
    //     title->SetX2NDC(x2);
    //     title->SetY2NDC(y2);
    //   }
      
    // } else if (pad_number==2) {

    //   TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);    
    //   TPaveText *title = (TPaveText*)pad->GetPrimitive("title");
      
    //   if (title) {
    //     title->SetBorderSize(0);
    //     title->SetX1NDC(0);
    //     title->SetY1NDC(0);
    //     title->SetX2NDC(0);
    //     title->SetY2NDC(0);
    //   }
      
    // }
    
    PrintDebugInfo("<< KanaDrawManager::AdjustTitle: END >>");
  }

  void KanaDrawManager::SetStat(TH1 *h, const Int_t pad_number)
  {
    fCanvas->Update();
    TPaveStats *ps = (TPaveStats*)h->FindObject("stats");
    if (!ps) return;
    
    if (fIsDivided) {
      
      TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
      
      const Double_t width  = (pad->GetRightMargin()-0.02) / static_cast<Double_t>(fStatNcolumn);
      const Double_t height = ((0.60-0.04)/fDivisionRatio)   / static_cast<Double_t>(fStatNrow);
      
      const Double_t x1 = (1-pad->GetRightMargin()+0.02) + (fNstats/fStatNrow) * width;
      const Double_t y1 = (1-0.04/fDivisionRatio)        - (fNstats%fStatNrow+1)  * height;
      const Double_t x2 = x1 + width*0.95;
      const Double_t y2 = y1 + height*0.95;
      
      ps->SetX1NDC(x1);
      ps->SetY1NDC(y1);
      ps->SetX2NDC(x2);
      ps->SetY2NDC(y2);
      
    } else {
      
      TPad *pad = (TPad*)fDrawPad->GetPad(pad_number);
      
      const Double_t width  = (pad->GetRightMargin()-0.02) / static_cast<Double_t>(fStatNcolumn);
      const Double_t height = 0.56                         / static_cast<Double_t>(fStatNrow);
      
      const Double_t x1 = (1-pad->GetRightMargin()+0.02) + (fNstats/fStatNrow) * width;
      const Double_t y1 = 0.96 - (fNstats%fStatNrow+1) * height;
      const Double_t x2 = x1 + width*0.95;
      const Double_t y2 = y1 + height*0.95;
      
      ps->SetX1NDC(x1);
      ps->SetY1NDC(y1);
      ps->SetX2NDC(x2);
      ps->SetY2NDC(y2);
      
    }
    
    ps->SetLineColor(h->GetLineColor());
    ps->SetTextColor(h->GetLineColor());
    ++fNstats;
  }
    
}
