#include "KanaLib/KanaErrorManager.h"

#include <iostream>
#include <sstream>

ClassImp(KanaLibrary::KanaErrorManager)

namespace KanaLibrary
{
  
  KanaErrorManager::KanaErrorManager()
    : ErrorFlag(0)
  {
  }

  KanaErrorManager::~KanaErrorManager()
  {
  }

  void KanaErrorManager::DumpMsg(const std::string type,
  			       const std::string name, const std::string content)
  {
    std::cerr << type << ": in " << name << ": " << content << std::endl;
  }

  void KanaErrorManager::DumpError(const std::string name, const std::string content)
  {
    DumpMsg("ERROR", name, content);
  }

  void KanaErrorManager::DumpWarning(const std::string name, const std::string content)
  {
    DumpMsg("WARNING", name, content);
  }
  
}
