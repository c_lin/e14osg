#include "KanaLib/KanaE14DetectorBase.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <algorithm>

#include "KanaLib/KanaFunctions.h"

ClassImp(KanaLibrary::KanaE14DetectorBase)

namespace KanaLibrary
{

  KanaE14DetectorBase::KanaE14DetectorBase()
    : AccRunID(-1),
      ProVersion(-1),
      Name("")
  {
    InitKanaE14DetectorBase();
  }

  KanaE14DetectorBase::KanaE14DetectorBase(const Int_t acc_run_ID,
  					 const Int_t pro_version,
  					 const std::string name)
    : AccRunID(acc_run_ID),
      ProVersion(pro_version),
      Name(name)
  {
    InitKanaE14DetectorBase();
    GetE14IDList();
    GetModuleIDList();
    
    // allocate memories
    ModID    = new Int_t   [nChannels];
    Pedestal = new Float_t [nChannels];
    
    MonitorOutMeanOnSpill   = new Float_t [nChannels][2];
    MonitorOutMeanOffSpill  = new Float_t [nChannels][2];
    MonitorOutSigmaOnSpill  = new Float_t [nChannels][2];
    MonitorOutSigmaOffSpill = new Float_t [nChannels][2];
    
    MonitorOutFitMeanOnSpill   = new Float_t [nChannels][2];
    MonitorOutFitMeanOffSpill  = new Float_t [nChannels][2];
    MonitorOutFitSigmaOnSpill  = new Float_t [nChannels][2];
    MonitorOutFitSigmaOffSpill = new Float_t [nChannels][2];
    
    MonitorTdiffTrigMeanOnSpill   = new Float_t [nChannels][2];
    MonitorTdiffTrigMeanOffSpill  = new Float_t [nChannels][2];
    MonitorTdiffTrigSigmaOnSpill  = new Float_t [nChannels][2];
    MonitorTdiffTrigSigmaOffSpill = new Float_t [nChannels][2];

    MonitorTdiffTrigFitMeanOnSpill   = new Float_t [nChannels][2];
    MonitorTdiffTrigFitMeanOffSpill  = new Float_t [nChannels][2];
    MonitorTdiffTrigFitSigmaOnSpill  = new Float_t [nChannels][2];
    MonitorTdiffTrigFitSigmaOffSpill = new Float_t [nChannels][2];

    MonitorTdiff0MeanOnSpill   = new Float_t [nChannels][2];
    MonitorTdiff0MeanOffSpill  = new Float_t [nChannels][2];
    MonitorTdiff0SigmaOnSpill  = new Float_t [nChannels][2];
    MonitorTdiff0SigmaOffSpill = new Float_t [nChannels][2];

    MonitorTdiff0FitMeanOnSpill   = new Float_t [nChannels][2];
    MonitorTdiff0FitMeanOffSpill  = new Float_t [nChannels][2];
    MonitorTdiff0FitSigmaOnSpill  = new Float_t [nChannels][2];
    MonitorTdiff0FitSigmaOffSpill = new Float_t [nChannels][2];

    ModuleModID = new Int_t [nModules];
  }

  void KanaE14DetectorBase::InitKanaE14DetectorBase()
  {
    nChannels = 0;
    nModules  = 0;

    Number   = 0;
    ModID    = NULL;
    Pedestal = NULL;

    ModuleNumber = 0;
    ModuleModID  = NULL;
  }

  void KanaE14DetectorBase::GetE14IDList()
  {
    KanaE14IDManager E14_ID_man(AccRunID, ProVersion);
    
    // get E14 ID list
    if (E14_ID_man.GetE14IDList(Name.c_str(), E14IDList) != 0) {
      SetErrorFlag(GetE14IDListError);
      return;
    }
    
    nChannels = E14IDList.size();
  }

  void KanaE14DetectorBase::GetModuleIDList()
  {
    KanaModuleIDManager module_ID_man(AccRunID, ProVersion);
    
    // get E14 ID list
    if (module_ID_man.GetModuleIDList(Name.c_str(), ModuleIDList) != 0) {
      SetErrorFlag(GetModuleIDListError);
      return;
    }
    
    nModules = ModuleIDList.size();
  }


  void KanaE14DetectorBase::LoadChannelMap(const std::string path)
  {
    /* reset channel map */
    Int_t n_ch = -9999;
    std::vector<Int_t> e14_ID_list;
    std::map<Int_t,KanaChMapData> ch_map;
    
    
    /* read map file */
    std::ifstream fin(path.c_str());
    if (!fin) {
      std::cerr << "ERROR: cannot find mapfile '" << path << "'" << std::endl;
      return;
    }
    
    // get number of channels
    while (!fin.eof()) {
      
      std::string line;
      std::getline(fin, line);
      if (KanaFunctions::NoCharacter(line)) {
        if (!fin.eof()) break;
        else            continue;
      }
      
      n_ch = std::atoi(line.c_str());
      break;
    }
    
    if (n_ch <= 0) {
      std::cerr << "ERROR: invalid number of channels in mapfile '" << path << "'"
  	      << "(" << n_ch << ")" << std::endl;
      return;
    }
    
    // get channel map
    while (!fin.eof()) {
      
      std::string line;
      std::getline(fin, line);
      if (KanaFunctions::NoCharacter(line)) { // only space or tab in a line
        if (fin.eof()) break;
        else           continue;
      }
      
      std::vector<std::string> vars;
      if (KanaFunctions::ReadSSVLine(line, vars) != 4) {
        std::cerr << "ERROR: wrong number of values in '" << path << ")" << std::endl;
        return;
      }
      
      const Int_t e14_ID   = std::atoi(vars.at(0).c_str());
      const Int_t crate_ID = std::atoi(vars.at(1).c_str());
      const Int_t mod_ID   = std::atoi(vars.at(2).c_str());
      const Int_t ch_ID    = std::atoi(vars.at(3).c_str());
      
      e14_ID_list.push_back(e14_ID);
      ch_map[e14_ID].FADCCrateID   = crate_ID;
      ch_map[e14_ID].FADCModID     = mod_ID;
      ch_map[e14_ID].FADCChannelID = ch_ID;
    }
    
    if (e14_ID_list.size() != static_cast<size_t>(n_ch)) {
      std::cerr << "ERROR: mapfile '" << path << "' has wrong number of lines" << std::endl;
      return;
    }
    
    /* set new value */
    nChannels      = n_ch;
    E14IDList      = e14_ID_list;
    FADCChannelMap = ch_map;
  }

  Int_t KanaE14DetectorBase::GetFADCCrateID(const Int_t e14_ID)
  {
    if (FADCChannelMap.find(e14_ID) != FADCChannelMap.end()) {
      return FADCChannelMap[e14_ID].FADCCrateID;
    } else {
      return -1;
    }
  }

  Int_t KanaE14DetectorBase::GetFADCModID(const Int_t e14_ID)
  {
    if (FADCChannelMap.find(e14_ID) != FADCChannelMap.end()) {
      return FADCChannelMap[e14_ID].FADCModID;
    } else {
      return -1;
    }
  }

  Int_t KanaE14DetectorBase::GetFADCChannelID(const Int_t e14_ID)
  {
    if (FADCChannelMap.find(e14_ID) != FADCChannelMap.end()) {
      return FADCChannelMap[e14_ID].FADCChannelID;
    } else {
      return -1;
    }
  }

  Int_t KanaE14DetectorBase::LoadCalib(const std::string path)
  {
    // open input file
    std::ifstream fin(path.c_str());
    if (!fin) {
      DumpError("KanaE14Detector ("+Name+")", "cannot open file '"+path+"'");
      return 1;
    }
    
    // check number of channels
    const Int_t n_ch = GetnChannelsFromCalibFile(fin);
    if (n_ch != nChannels) return 2;
    
    // read constants
    while (!fin.eof()) {
      
      std::string line;
      std::getline(fin, line);
      
      if (KanaFunctions::NoCharacter(line)) {
        if (fin.eof()) break;
        else           continue;
      }
      
      std::vector<std::string> vars;
      if (KanaFunctions::ReadSSVLine(line, vars) != 4) {
        DumpError("KanaE14Detector ("+Name+")",
  		"invalid format of calibration constant file");
        return 3;
      }
      
      const Int_t    e14_ID = std::atoi(vars.at(0).c_str());
      const Double_t mean   = std::atof(vars.at(1).c_str());
      // const Double_t sigma = std::atof(vars.at(2).c_str());
      const Double_t MeV_coeff  = std::atof(vars.at(3).c_str());
      
      // CalibFactorList[e14_ID] = MeV_coeff / mean;
      GainMeanList[e14_ID] = mean;
      MeVCoeffList[e14_ID] = MeV_coeff;
    }
    
    for (Int_t i_ch=0, n_ch=E14IDList.size(); i_ch < n_ch; ++i_ch) {
      const Int_t e14_ID = E14IDList.at(i_ch);
      // if (CalibFactorList.find(e14_ID) == CalibFactorList.end()) {
      if ( GainMeanList.find(e14_ID) == GainMeanList.end()
  	 || MeVCoeffList.find(e14_ID) == MeVCoeffList.end() ) {
        std::stringstream msg;
        msg  << "cannot find energy calib const for E14ID = " << e14_ID;
        DumpWarning("KanaE14Detector ("+Name+")", msg.str());
      }
    }
    
    return 0;
  }

  Int_t KanaE14DetectorBase::LoadT0(const std::string path)
  {
    // open input file
    std::ifstream fin(path.c_str());
    if (!fin) {
      DumpError("KanaE14Detector ("+Name+")", "cannot open file '"+path+"'");
      return 1;
    }
    
    // check number of channels
    const Int_t n_ch = GetnChannelsFromCalibFile(fin);
    if (n_ch != nChannels) return 2;
    
    // read constants
    while (!fin.eof()) {
      
      std::string line;
      std::getline(fin, line);
      
      if (KanaFunctions::NoCharacter(line)) {
        if (fin.eof()) break;
        else           continue;
      }
      
      std::vector<std::string> vars;
      if (KanaFunctions::ReadSSVLine(line, vars) != 3) {
        DumpError("KanaE14Detector ("+Name+")", "invalid format of calibration constant file");
        return 3;
      }
      
      const Int_t    e14_ID = std::atoi(vars.at(0).c_str());
      const Double_t mean   = std::atof(vars.at(1).c_str());
      // const Double_t sigma = std::atof(vars.at(2).c_str());
      
      T0MeanList[e14_ID] = mean;
    }
    
    for (Int_t i_ch=0, n_ch=E14IDList.size(); i_ch < n_ch; ++i_ch) {
      const Int_t e14_ID = E14IDList.at(i_ch);
      if (T0MeanList.find(e14_ID) == T0MeanList.end()) {
        std::stringstream msg;
        msg  << "cannot find T0 calib const for E14ID = " << e14_ID;
        DumpWarning("KanaE14Detector ("+Name+")", msg.str());
      }
    }
    
    return 0;
  }

  Double_t KanaE14DetectorBase::GetGainMean(const Int_t e14_ID)
  {
    if (GainMeanList.find(e14_ID) != GainMeanList.end()) {
      return GainMeanList[e14_ID];
    } else {
      return -1;
    }
  }

  Double_t KanaE14DetectorBase::GetMeVCoeff(const Int_t e14_ID)
  {
    if (MeVCoeffList.find(e14_ID) != MeVCoeffList.end()) {
      return MeVCoeffList[e14_ID];
    } else {
      return 0;
    }
  }

  Double_t KanaE14DetectorBase::GetT0Mean(const Int_t e14_ID)
  {
    if (T0MeanList.find(e14_ID) != T0MeanList.end()) {
      return T0MeanList[e14_ID];
    } else {
      return -9999;
    }
  }

  Int_t KanaE14DetectorBase::GetnChannelsFromCalibFile(std::ifstream &fin)
  {
    // get number of channels written at the top of the input calib file
    Int_t n_ch=-9999;
    while (!fin.eof()) {
      
      std::string line;
      std::getline(fin, line);
      
      if (KanaFunctions::NoCharacter(line)) {
        if (fin.eof()) break;
        else           continue;
      }
      
      n_ch = std::atoi(line.c_str());
      break;
    }
    
    // check number of channels written in calib file
    if (n_ch==-9999) {
      std::stringstream msg;
      msg  << "cannot find number of channels" << n_ch;
      DumpError("KanaE14Detector ("+Name+")", msg.str());
      return -1;
    } else if (n_ch != nChannels) {
      std::stringstream msg;
      msg  << "wrong number of channels in calib file: " << n_ch;
      DumpError("KanaE14Detector ("+Name+")", msg.str());
      return -2;
    } else {
      return n_ch;
    }
  }

  void KanaE14DetectorBase::SetBranchAddresses(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branch addresses: " << Name << std::endl;
    
    // set branch addresses of KanaE14DetectorBase
    SetBranchAddressesBase(tr, b_list);
  }

  void KanaE14DetectorBase::SetBranchAddressesBase(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "Number") != end) {
      const std::string bname = Name + "Number";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &Number);
      std::cout << "     -- Number: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModID") != end) {
      const std::string bname = Name + "ModID";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModID);
      std::cout << "     -- ModID: enabled" << std::endl;
    }
    if (std::find(begin, end, "Pedestal") != end) {
      const std::string bname = Name + "Pedestal";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), Pedestal);
      std::cout << "     -- Pedestal: enabled" << std::endl;
    }


    if (std::find(begin, end, "MonitorOutMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorOutMeanOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorOutMeanOnSpill);
      std::cout << "     -- MonitorOutMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorOutMeanOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorOutMeanOffSpill);
      std::cout << "     -- MonitorOutMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorOutSigmaOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorOutSigmaOnSpill);
      std::cout << "     -- MonitorOutSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorOutSigmaOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorOutSigmaOffSpill);
      std::cout << "     -- MonitorOutSigmaOffSpill: enabled" << std::endl;
    }

    if (std::find(begin, end, "MonitorOutFitMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorOutFitMeanOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorOutFitMeanOnSpill);
      std::cout << "     -- MonitorOutFitMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutFitMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorOutFitMeanOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorOutFitMeanOffSpill);
      std::cout << "     -- MonitorOutFitMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutFitSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorOutFitSigmaOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorOutFitSigmaOnSpill);
      std::cout << "     -- MonitorOutFitSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutFitSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorOutFitSigmaOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorOutFitSigmaOffSpill);
      std::cout << "     -- MonitorOutFitSigmaOffSpill: enabled" << std::endl;
    }
    

    if (std::find(begin, end, "MonitorTdiffTrigMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigMeanOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiffTrigMeanOnSpill);
      std::cout << "     -- MonitorTdiffTrigMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigMeanOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiffTrigMeanOffSpill);
      std::cout << "     -- MonitorTdiffTrigMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigSigmaOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiffTrigSigmaOnSpill);
      std::cout << "     -- MonitorTdiffTrigSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigSigmaOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiffTrigSigmaOffSpill);
      std::cout << "     -- MonitorTdiffTrigSigmaOffSpill: enabled" << std::endl;
    }

    if (std::find(begin, end, "MonitorTdiffTrigFitMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigFitMeanOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiffTrigFitMeanOnSpill);
      std::cout << "     -- MonitorTdiffTrigFitMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigFitMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigFitMeanOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiffTrigFitMeanOffSpill);
      std::cout << "     -- MonitorTdiffTrigFitMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigFitSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigFitSigmaOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiffTrigFitSigmaOnSpill);
      std::cout << "     -- MonitorTdiffTrigFitSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigFitSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigFitSigmaOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiffTrigFitSigmaOffSpill);
      std::cout << "     -- MonitorTdiffTrigFitSigmaOffSpill: enabled" << std::endl;
    }
    

    if (std::find(begin, end, "MonitorTdiff0MeanOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0MeanOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiff0MeanOnSpill);
      std::cout << "     -- MonitorTdiff0MeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0MeanOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0MeanOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiff0MeanOffSpill);
      std::cout << "     -- MonitorTdiff0MeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0SigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0SigmaOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiff0SigmaOnSpill);
      std::cout << "     -- MonitorTdiff0SigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0SigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0SigmaOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiff0SigmaOffSpill);
      std::cout << "     -- MonitorTdiff0SigmaOffSpill: enabled" << std::endl;
    }

    if (std::find(begin, end, "MonitorTdiff0FitMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0FitMeanOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiff0FitMeanOnSpill);
      std::cout << "     -- MonitorTdiff0FitMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0FitMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0FitMeanOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiff0FitMeanOffSpill);
      std::cout << "     -- MonitorTdiff0FitMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0FitSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0FitSigmaOnSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiff0FitSigmaOnSpill);
      std::cout << "     -- MonitorTdiff0FitSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0FitSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0FitSigmaOffSpill";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), MonitorTdiff0FitSigmaOffSpill);
      std::cout << "     -- MonitorTdiff0FitSigmaOffSpill: enabled" << std::endl;
    }
    
    
    if (std::find(begin, end, "ModuleNumber") != end) {
      const std::string bname = Name + "ModuleNumber";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), &ModuleNumber);
      std::cout << "     -- ModuleNumber: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleModID") != end) {
      const std::string bname = Name + "ModuleModID";
      KanaFunctions::ActivateBranch(tr, bname.c_str(), ModuleModID);
      std::cout << "     -- ModuleModID: enabled" << std::endl;
    }
  }


  void KanaE14DetectorBase::AddBranches(TTree *tr, const std::string b_list)
  {
    std::cout << "   - set branches: " << Name << std::endl;
    
    // add branches of KanaE14DetectorBase
    AddBranchesBase(tr, b_list);
  }

  void KanaE14DetectorBase::AddBranchesBase(TTree *tr, const std::string b_list)
  {
    std::vector<std::string> branch_list(0);
    GetBranchList(b_list, branch_list);
    
    const std::vector<std::string>::iterator begin = branch_list.begin();
    const std::vector<std::string>::iterator end   = branch_list.end();

    if (std::find(begin, end, "Number") != end) {
      const std::string bname = Name + "Number";
      std::stringstream leaflist;
      leaflist << bname << "/I";
      tr->Branch(bname.c_str(), &Number, leaflist.str().c_str());
      std::cout << "     -- Number: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModID") != end) {
      const std::string bname = Name + "ModID";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/I";
      tr->Branch(bname.c_str(), ModID, leaflist.str().c_str());
      std::cout << "     -- ModID: enabled" << std::endl;
    }
    if (std::find(begin, end, "Pedestal") != end) {
      const std::string bname = Name + "Pedestal";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number]/F";
      tr->Branch(bname.c_str(), Pedestal, leaflist.str().c_str());
      std::cout << "     -- Pedestal: enabled" << std::endl;
    }


    if (std::find(begin, end, "MonitorOutMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorOutMeanOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorOutMeanOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorOutMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorOutMeanOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorOutMeanOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorOutMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorOutSigmaOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorOutSigmaOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorOutSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorOutSigmaOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorOutSigmaOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorOutSigmaOffSpill: enabled" << std::endl;
    }

    if (std::find(begin, end, "MonitorOutFitMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorOutFitMeanOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorOutFitMeanOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorOutFitMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutFitMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorOutFitMeanOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorOutFitMeanOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorOutFitMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutFitSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorOutFitSigmaOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorOutFitSigmaOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorOutFitSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorOutFitSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorOutFitSigmaOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorOutFitSigmaOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorOutFitSigmaOffSpill: enabled" << std::endl;
    }


    if (std::find(begin, end, "MonitorTdiffTrigMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigMeanOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiffTrigMeanOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiffTrigMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigMeanOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiffTrigMeanOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiffTrigMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigSigmaOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiffTrigSigmaOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiffTrigSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigSigmaOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiffTrigSigmaOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiffTrigSigmaOffSpill: enabled" << std::endl;
    }

    if (std::find(begin, end, "MonitorTdiffTrigFitMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigFitMeanOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiffTrigFitMeanOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiffTrigFitMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigFitMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigFitMeanOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiffTrigFitMeanOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiffTrigFitMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigFitSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigFitSigmaOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiffTrigFitSigmaOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiffTrigFitSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiffTrigFitSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiffTrigFitSigmaOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiffTrigFitSigmaOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiffTrigFitSigmaOffSpill: enabled" << std::endl;
    }
    

    if (std::find(begin, end, "MonitorTdiff0MeanOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0MeanOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiff0MeanOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiff0MeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0MeanOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0MeanOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiff0MeanOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiff0MeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0SigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0SigmaOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiff0SigmaOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiff0SigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0SigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0SigmaOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiff0SigmaOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiff0SigmaOffSpill: enabled" << std::endl;
    }

    if (std::find(begin, end, "MonitorTdiff0FitMeanOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0FitMeanOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiff0FitMeanOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiff0FitMeanOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0FitMeanOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0FitMeanOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiff0FitMeanOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiff0FitMeanOffSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0FitSigmaOnSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0FitSigmaOnSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiff0FitSigmaOnSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiff0FitSigmaOnSpill: enabled" << std::endl;
    }
    if (std::find(begin, end, "MonitorTdiff0FitSigmaOffSpill") != end) {
      const std::string bname = Name + "MonitorTdiff0FitSigmaOffSpill";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "Number][2]/F";
      tr->Branch(bname.c_str(), MonitorTdiff0FitSigmaOffSpill, leaflist.str().c_str());
      std::cout << "     -- MonitorTdiff0FitSigmaOffSpill: enabled" << std::endl;
    }
    

    if (std::find(begin, end, "ModuleNumber") != end) {
      const std::string bname = Name + "ModuleNumber";
      std::stringstream leaflist;
      leaflist << bname << "/I";
      tr->Branch(bname.c_str(), &ModuleNumber, leaflist.str().c_str());
      std::cout << "     -- ModuleNumber: enabled" << std::endl;
    }
    if (std::find(begin, end, "ModuleModID") != end) {
      const std::string bname = Name + "ModuleModID";
      std::stringstream leaflist;
      leaflist << bname << "[" << Name << "ModuleNumber]/I";
      tr->Branch(bname.c_str(), ModuleModID, leaflist.str().c_str());
      std::cout << "     -- ModuleModID: enabled" << std::endl;
    }
  }


  void KanaE14DetectorBase::GetBranchList(const std::string &b_list,
  					std::vector<std::string> &branch_list)
  {
    std::vector<std::string> list_temp(0);
    
    std::string bname = "";
    for (Int_t i=0, n=b_list.size(); i < n; ++i) {
      
      std::string next = b_list.substr(i, 1);
      
      if (next == ";") {
        list_temp.push_back(bname);
        bname = "";
      } else {
        bname += next;
      }
    }
    
    if (bname.size() != 0) list_temp.push_back(bname);
    
    branch_list = list_temp;
  }


  Int_t KanaE14DetectorBase::GetNumber(const Int_t e14_ID)
  {
    for (Int_t num=0; num < Number; ++num) {
      if (ModID[num] == e14_ID) return num;
    }
    return -1;
  }

  Int_t KanaE14DetectorBase::GetModuleNumber(const Int_t module_ID)
  {
    for (Int_t num=0; num < ModuleNumber; ++num) {
      if (ModID[num] == module_ID) return num;
    }
    return -1;
  }


  void KanaE14DetectorBase::ZeroSuppression(const Double_t)
  {
    // function for DST zero suppression
    //    - th: energy threshold for zero suppression
    // should be redefined in derived classes
  }



  KanaE14DetectorBase::~KanaE14DetectorBase()
  {
    if (ModID       != NULL) delete [] ModID;
    if (Pedestal    != NULL) delete [] Pedestal;
    if (ModuleModID != NULL) delete [] ModuleModID;
  }
  
}
