#include "KanaLib/KanaHistoManager.h"

#include <iostream>
#include <iomanip>
#include <sstream>

#include <TClass.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TIterator.h>
#include <TKey.h>
#include <TROOT.h>

ClassImp(KanaLibrary::KanaHistoManager)

namespace KanaLibrary
{
  
  KanaHistoManager::KanaHistoManager()
    : Path("")
  {
  }

  KanaHistoManager::KanaHistoManager(const std::string path)
    : Path(path)
  {
    InitKanaHistoManager();
    OpenInputFile();
    GetHistograms();
  }

  void KanaHistoManager::InitKanaHistoManager()
  {
    InputFile = NULL;
    
    Error = 0;
  }

  void KanaHistoManager::OpenInputFile()
  {
    InputFile = new TFile(Path.c_str());
    if (InputFile->IsZombie()) Error = 1;
  }

  void KanaHistoManager::GetHistograms()
  {
    InputFile->cd();
    
    TIter next(InputFile->GetListOfKeys());
    TKey *key = NULL;
    while ( (key=(TKey*)next()) ) {
      const std::string name = key->GetClassName();
      if (name.find("TH1")!=std::string::npos) {
        TH1* h = (TH1*)key->ReadObj();
        Histo1[std::string(h->GetName())] = h;
      } else if (name.find("TH2")!=std::string::npos) {
        TH2* h = (TH2*)key->ReadObj();
        Histo2[std::string(h->GetName())] = h;
      }
    }
  }


  std::vector<std::string> KanaHistoManager::GetHisto1List()
  {
    std::vector<std::string> name_list(0);
    for (std::map<std::string,TH1*>::iterator itr = Histo1.begin(); itr != Histo1.end(); ++itr) {
      name_list.push_back(itr->first);
    }
    return name_list;
  }


  std::vector<std::string> KanaHistoManager::GetHisto2List()
  {
    std::vector<std::string> name_list(0);
    for (std::map<std::string,TH2*>::iterator itr = Histo2.begin(); itr != Histo2.end(); ++itr) {
      name_list.push_back(itr->first);
    }
    return name_list;
  }


  TH1* KanaHistoManager::GetHisto1(const std::string name, const std::string h_name)
  {
    InputFile->cd();
    if (Histo1.find(name)!=Histo1.end()) {
      return (TH1*)Histo1[name]->Clone(h_name.c_str());
    } else {
      return NULL;
    }
  }


  TH2* KanaHistoManager::GetHisto2(const std::string name, const std::string h_name)
  {
    InputFile->cd();
    if (Histo2.find(name)!=Histo2.end()) {
      return (TH2*)Histo2[name]->Clone(h_name.c_str());
    } else {
      return NULL;
    }
  }


  Double_t KanaHistoManager::GetEntries(const std::string name)
  {
    InputFile->cd();
    if (Histo1.find(name)!=Histo1.end()) {
      return Histo1[name]->GetEntries();
    } else if (Histo2.find(name)!=Histo2.end()) {
      return Histo2[name]->GetEntries();
    } else {
      return 0;
    }
  }
  
  
  Double_t KanaHistoManager::IntegralHisto1(const std::string name,
					    const Double_t xmin, const Double_t xmax)
  {
    InputFile->cd();
    const TH1 *h_temp = Histo1[name];
    Int_t binx1 = h_temp->GetXaxis()->FindBin(xmin);
    Int_t binx2 = h_temp->GetXaxis()->FindBin(xmax);
    return h_temp->Integral(binx1, binx2);
  }


  Double_t KanaHistoManager::IntegralHisto2(const std::string name,
					    const Double_t xmin, const Double_t xmax,
					    const Double_t ymin, const Double_t ymax)
  {
    InputFile->cd();
    const TH2 *h_temp = Histo2[name];
    Int_t binx1 = h_temp->GetXaxis()->FindBin(xmin);
    Int_t binx2 = h_temp->GetXaxis()->FindBin(xmax);
    Int_t biny1 = h_temp->GetYaxis()->FindBin(ymin);
    Int_t biny2 = h_temp->GetYaxis()->FindBin(ymax);
    return h_temp->Integral(binx1, binx2, biny1, biny2);
  }



  KanaHistoManager::~KanaHistoManager()
  {
    if (InputFile!=NULL) delete InputFile;
  }
  
}
