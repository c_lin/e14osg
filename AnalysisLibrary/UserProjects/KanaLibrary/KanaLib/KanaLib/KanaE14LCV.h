/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAE14LCV_H
#define E14ANA_KANALIB_KANAE14LCV_H

#include <Rtypes.h>

#include "KanaLib/KanaE14Detector125MHz.h"

namespace KanaLibrary
{

  class KanaE14LCV : public KanaE14Detector125MHz
  {
  public:
    const Double_t FrontEdgeZ;
    const Double_t RearEdgeZ;
    const Double_t Width;
    const Double_t Thickness;
    
    const Double_t MIPPeakEnergy;
    
  public:
    KanaE14LCV(); // default ctor
    KanaE14LCV(const Int_t acc_run_ID, const Int_t pro_version);
    virtual ~KanaE14LCV();
    
  private:
    void InitKanaE14LCV();
    
    void SetModulePositionMap();
    std::map<Int_t, std::pair<Double_t, Double_t> > PositionMap; // first: X, first: Y
    std::map<Int_t, std::pair<Double_t, Double_t> > ModuleRangeX; // first: min, first: max
    std::map<Int_t, std::pair<Double_t, Double_t> > ModuleRangeY; // first: min, first: max
    
  public:
    Int_t    CosmicNumber;
    Int_t   *CosmicModID;
    Short_t *CosmicPeak;
    Float_t *CosmicCorrectedPeak;
    Float_t *CosmicIntegratedADC;
    Float_t *CosmicCorrectedIntegratedADC;
    Float_t *CosmicInitialTime;
    Float_t *CosmicHitX;
    Float_t *CosmicHitY;
    Float_t *CosmicHitZ;
    
  public:
    virtual void SetBranchAddresses(TTree *tr, const std::string b_list);
    virtual void AddBranches(TTree *tr, const std::string b_list);

  protected:
    void SetBranchAddressesLCV(TTree *tr, const std::string b_list);
    void AddBranchesLCV(TTree *tr, const std::string b_list);
    
  public:
    void GetCosmicHitModules(const Double_t intercept_xy, const Double_t slope_xy,
  			   const Double_t intercept_zy, const Double_t slope_zy);
    
    ClassDef(KanaE14LCV, 1);
    
  };
  
}

#endif
