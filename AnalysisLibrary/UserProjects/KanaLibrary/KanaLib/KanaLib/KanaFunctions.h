/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAFUNCTIONS_H
#define E14ANA_KANALIB_KANAFUNCTIONS_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <TChain.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>

#include "KanaDrawManager.h"

namespace KanaLibrary
{
  
  class KanaFunctions
  {
  public:
    KanaFunctions();
    virtual ~KanaFunctions();

    static Bool_t NoCharacter(const std::string &str);
    
    static Int_t ReadSSVLine(const std::string &line, std::vector<std::string> &vars);
    
    static Int_t ReadCSVLine(const std::string &line, std::vector<std::string> &vars);
    
    static std::vector<std::string> SplitLine(const std::string &line, const char delimiter);
    
    static UInt_t HexStringToUInt(std::string hex_str);

    static Int_t GetUNIXTime(const std::string str_time, const std::string format);
    
    static Int_t ActivateBranch(TTree *tr, const std::string bname, void *add);

    static Int_t GetRunList(const std::string fin_path, std::vector<Int_t> &list);
    
    static Int_t GetDstNodeIDList(const Int_t acc_run_ID, const Int_t pro_version,
  				std::vector<Int_t> &list);
    static Int_t GetConvNodeIDList(const Int_t acc_run_ID, const Int_t pro_version,
  				 std::vector<Int_t> &list);
    static Int_t GetNodeIDList(const Int_t acc_run_ID, const Int_t pro_version,
  			     const std::string name, std::vector<Int_t> &list);
    static Int_t GetNodeIDList(const std::string top_dir, std::vector<Int_t> &list);
    
    static Int_t GetDetectorList(const Int_t acc_run_ID, std::vector<std::string> det_list);
    
    static TGraphErrors* GetRatioGraph(TGraphErrors *g1, TGraphErrors *g2);
    
    static TGraphErrors* GetDiffGraph(TGraphErrors *g1, TGraphErrors *g2);
    
    static TH1* GetRatioHisto(const TH1 *h1, const TH1 *h2);
    
    static TH1* GetDiffHisto(const TH1 *h1, const TH1 *h2);
    
    static Double_t GetPedestal(const Int_t n_samples, const Short_t *data, const Int_t stat=10); // stat: number of points used for pedestal calculation
    static Double_t GetPedestal(const Int_t n_samples, const Long64_t *data, const Int_t stat=10); // stat: number of points used for pedestal calculation
    static Double_t GetPedestal(const Int_t n_samples, const Double_t *data, const Int_t stat=10); // stat: number of points used for pedestal calculation

    static Double_t GetBaseline(const Int_t resolution, const Int_t n_merged_bins,
  			      const Int_t n_samples, const Short_t *data);
    static Double_t GetBaseline(const Int_t resolution, const Int_t n_samples, const Short_t *data, const Double_t contrast_threshold=0.05); // automatic optimization of n_merged_bins
    
    static Bool_t IsPeak(Int_t n_search, Int_t index, Short_t *Data);

    static Double_t GetPTime(const Double_t x1, const Double_t y1,
  			   const Double_t x2, const Double_t y2,
  			   const Double_t x3, const Double_t y3);
    static Double_t GetPPeak(const Double_t x1, const Double_t y1,
  			   const Double_t x2, const Double_t y2,
  			   const Double_t x3, const Double_t y3);

    static void
    GetFirstDerivative(const Int_t n_samples, const Short_t *data, Double_t *derivative);

    static void
    GetFirstDerivative(const Int_t n_samples, const Double_t *data, Double_t *derivative);
    
    static void LowPassFilter(const Double_t cutoff,
  			    const Int_t n, const Double_t *time,
  			    const Double_t *input, Double_t *output);
    
    static void MovingAverage(const Int_t n, const Short_t *input,
  			    Double_t *output, const Int_t n_average);
    
    static Double_t AsymGauss(Double_t *x, Double_t *par);

    static Double_t LandauGauss(Double_t *x, Double_t *par);
    static void     LandauGaussFit(TF1 *f_LG, TH1 *h);
    static void     LandauGaussFit(TF1 *f_LG, TH1 *h,
  			  const Double_t xmin, const Double_t xmax);
    
    
    static TChain* GetConvTree(const Int_t acc_run_ID, const Int_t pro_version,
  			     const Int_t run_ID, const Int_t node_ID, const Int_t file_ID,
  			     const char *name);
    static TChain* GetDstTree(const Int_t acc_run_ID, const Int_t pro_version,
  			    const Int_t run_ID, const Int_t node_ID, const Int_t file_ID,
  			    const char *name);
    static TChain* GetDstTree(const Int_t acc_run_ID, const Int_t pro_version, const Int_t run_ID,
  			    const char *name);
    
    static std::vector<Int_t> GetE14IDList(const Int_t acc_run_ID, const Int_t pro_version,
  					 const std::string name);
    
    static Int_t GetMaxFileID(const Int_t acc_run_ID);
    
    

    static Bool_t IsGoodPhysicsEvent(const Int_t isGoodRun, const Int_t isGoodSpill,
  				   const Short_t Error, const UInt_t ScaledTrigBit);
    static Bool_t IsGoodNormEvent(const Int_t isGoodRun, const Int_t isGoodSpill,
  				const Short_t Error, const UInt_t ScaledTrigBit);
    static Bool_t IsGoodMinBiasEvent(const Int_t isGoodRun, const Int_t isGoodSpill,
  				   const Short_t Error, const UInt_t ScaledTrigBit);
    
    
    static void DrawCutRegion(KanaDrawManager &man,
  			    const Double_t min_Zvtx=3000, const Double_t max_Zvtx=5000,
  			    const Double_t min_Pt=130, const Double_t max_Pt=250);
    static void DrawNumbers(KanaDrawManager &man, const Double_t n_NCC,
  			  const Double_t n_low_Pt, const Double_t n_blinded,
  			  const Double_t n_signal, const Double_t n_high_Pt,
  			  const Double_t n_down_low_Pt,
  			  const Double_t n_down_middle_Pt,
  			  const Double_t n_down_high_Pt,
  			  const Bool_t is_blinded );
    static void CountNumbers(KanaDrawManager &man,
  			   std::map<Int_t, std::pair<Double_t,Double_t> > &list,
  			   const Bool_t is_blinded,
  			   const Double_t min_Zvtx=3000, const Double_t max_Zvtx=5000,
  			   const Double_t min_Pt=130, const Double_t max_Pt=250);
    static void CountNumbers(KanaDrawManager &man, const TH2 *h, const Bool_t is_blinded,
  			   const Double_t min_Zvtx=3000, const Double_t max_Zvtx=5000,
  			   const Double_t min_Pt=130, const Double_t max_Pt=250);
    static void CountNumbers(KanaDrawManager &man, const TGraph *g, const Bool_t is_blinded,
  			   const Double_t min_Zvtx=3000, const Double_t max_Zvtx=5000,
  			   const Double_t min_Pt=130, const Double_t max_Pt=250);

    
    
    ClassDef(KanaFunctions, 1);
  };
  
}

#endif
