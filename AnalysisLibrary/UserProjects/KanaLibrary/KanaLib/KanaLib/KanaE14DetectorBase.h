/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAE14DETECTORBASE_H
#define E14ANA_KANALIB_KANAE14DETECTORBASE_H

#include <vector>
#include <map>

#include <TTree.h>

#include "KanaLib/KanaE14IDManager.h"
#include "KanaLib/KanaModuleIDManager.h"
#include "KanaLib/KanaErrorManager.h"

namespace KanaLibrary
{
  
  class KanaE14DetectorBase : public KanaErrorManager
  {
  protected:
    const Int_t AccRunID;
    const Int_t ProVersion;
    const std::string Name;
    
    enum { // for error flag
      GetE14IDListError = 0,
      GetModuleIDListError
    };
    
  public:
    KanaE14DetectorBase();
    KanaE14DetectorBase(const Int_t acc_run_ID, const Int_t pro_version, const std::string name);
    virtual ~KanaE14DetectorBase();
    
    inline Int_t GetAccRunID()   { return AccRunID;     }
    inline Int_t GetProVersion() { return ProVersion;   }
    inline const char* GetName() { return Name.c_str(); }
    
  private:
    void InitKanaE14DetectorBase();
    
    void GetE14IDList();
    void GetModuleIDList();
    
  protected:
    Int_t nChannels;
    std::vector<Int_t> E14IDList;

    Int_t nModules;
    std::vector<Int_t> ModuleIDList;
    
  public:
    inline Int_t GetnChannels() const { return nChannels; }
    inline Int_t GetnModules()  const { return nModules;  }
    inline Int_t GetE14ID(const Int_t index) const { return E14IDList.at(index); }
    inline Int_t GetModuleID(const Int_t index) const { return ModuleIDList.at(index); }
    
  public:
    void LoadChannelMap(const std::string path);
    
    Int_t GetFADCCrateID(const Int_t e14_ID);
    Int_t GetFADCModID(const Int_t e14_ID);
    Int_t GetFADCChannelID(const Int_t e14_ID);
    
  protected:
    std::map<Int_t, KanaChMapData> FADCChannelMap;

    
  public:
    Int_t LoadCalib(const std::string path);
    Int_t LoadT0(const std::string path);
    Double_t GetGainMean(const Int_t e14_ID);
    Double_t GetMeVCoeff(const Int_t e14_ID);
    Double_t GetT0Mean(const Int_t e14_ID);
    
  protected:
    // std::map<Int_t, Double_t> CalibFactorList;
    std::map<Int_t, Double_t> GainMeanList;
    std::map<Int_t, Double_t> MeVCoeffList;
    std::map<Int_t, Double_t> T0MeanList;
    
  private:
    Int_t GetnChannelsFromCalibFile(std::ifstream &fin);
    
  public:
    virtual void SetBranchAddresses(TTree *tr, const std::string b_list);
    virtual void AddBranches(TTree *tr, const std::string b_list);
    
  protected:
    void GetBranchList(const std::string &b_list,	std::vector<std::string> &branch_list);
    void SetBranchAddressesBase(TTree *tr, const std::string b_list);
    void AddBranchesBase(TTree *tr, const std::string b_list);
    
  public:
    Int_t GetNumber(const Int_t e14_ID);
    Int_t GetModuleNumber(const Int_t module_ID);
    
  public:
    Int_t    Number;
    Int_t   *ModID;
    Float_t *Pedestal;
    
    Float_t (*MonitorOutMeanOnSpill)[2];
    Float_t (*MonitorOutMeanOffSpill)[2];
    Float_t (*MonitorOutSigmaOnSpill)[2];
    Float_t (*MonitorOutSigmaOffSpill)[2];

    Float_t (*MonitorOutFitMeanOnSpill)[2];
    Float_t (*MonitorOutFitMeanOffSpill)[2];
    Float_t (*MonitorOutFitSigmaOnSpill)[2];
    Float_t (*MonitorOutFitSigmaOffSpill)[2];

    Float_t (*MonitorTdiffTrigMeanOnSpill)[2];
    Float_t (*MonitorTdiffTrigMeanOffSpill)[2];
    Float_t (*MonitorTdiffTrigSigmaOnSpill)[2];
    Float_t (*MonitorTdiffTrigSigmaOffSpill)[2];

    Float_t (*MonitorTdiffTrigFitMeanOnSpill)[2];
    Float_t (*MonitorTdiffTrigFitMeanOffSpill)[2];
    Float_t (*MonitorTdiffTrigFitSigmaOnSpill)[2];
    Float_t (*MonitorTdiffTrigFitSigmaOffSpill)[2];

    Float_t (*MonitorTdiff0MeanOnSpill)[2];
    Float_t (*MonitorTdiff0MeanOffSpill)[2];
    Float_t (*MonitorTdiff0SigmaOnSpill)[2];
    Float_t (*MonitorTdiff0SigmaOffSpill)[2];

    Float_t (*MonitorTdiff0FitMeanOnSpill)[2];
    Float_t (*MonitorTdiff0FitMeanOffSpill)[2];
    Float_t (*MonitorTdiff0FitSigmaOnSpill)[2];
    Float_t (*MonitorTdiff0FitSigmaOffSpill)[2];
    
    Int_t  ModuleNumber;
    Int_t *ModuleModID;
    
  public:
    virtual void ZeroSuppression(const Double_t th);
    
    ClassDef(KanaE14DetectorBase, 1); // omajinai
  };
  
}

#endif
