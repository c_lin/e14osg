/* -*- C++ -*- */
#ifndef E14ANA_KANALIB_KANAE14TRIGGERTAG_H
#define E14ANA_KANALIB_KANAE14TRIGGERTAG_H

#include "KanaLib/KanaE14Detector125MHz.h"

namespace KanaLibrary
{
  
  class KanaE14TRIGGERTAG : public KanaE14Detector125MHz
  {
  protected:
    Int_t ClockModID;
    Int_t LASERModID;
    Int_t LEDModID;
    
    Double_t TrigTagPeakThreshold;
    
  public:
    KanaE14TRIGGERTAG(); // default ctor
    KanaE14TRIGGERTAG(const Int_t acc_run_ID, const Int_t pro_version); // default ctor
    virtual ~KanaE14TRIGGERTAG();
    
  private:
    void InitKanaE14TRIGGERTAG();
    
  public:
    Bool_t IsClock();
    Bool_t IsLASER();
    Bool_t IsLED();
    Bool_t IsTrigTagEvent(Int_t trig_tag_mod_ID);
    
    ClassDef(KanaE14TRIGGERTAG, 1);
    
  };

}

#endif
