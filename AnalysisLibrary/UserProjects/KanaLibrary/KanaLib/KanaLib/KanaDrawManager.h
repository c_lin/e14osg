/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANADRAWMANAGER_H
#define E14ANA_KANALIB_KANADRAWMANAGER_H

#include <string>

#include <TAxis.h>
#include <TBox.h>
#include <TCanvas.h>
#include <TEllipse.h>
#include <TF1.h>
#include <TFile.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TLine.h>
#include <TMarker.h>
#include <TPad.h>
#include <TPaveText.h>
#include <TString.h>
#include <TTree.h>

namespace KanaLibrary
{
  
  class KanaDrawManager
  {
  public:
    KanaDrawManager(); // default ctor
    KanaDrawManager(const char *name, const char *title);
    KanaDrawManager(const char *pdf_name);
    KanaDrawManager(Int_t argc, char **argv, const char *pdf_name = "");
    virtual ~KanaDrawManager(); // dtor
    
    void ClosePDF();
    
    void AddInfo(const char *info);
    void AddLegendEntry(const TObject *obj, const char *label = "", Option_t *option = "");
    
    void Clear();
    
    void Divide(const Double_t division_ratio = 0.6);
    
    void DrawFrame(const Double_t xmin, const Double_t ymin,
		   const Double_t xmax, const Double_t ymax,
		   const char *title = "",
		   const Int_t pad_number = 0);
    void DrawBox(TBox *box,
		 const Double_t x1, const Double_t y1,
		 const Double_t x2, const Double_t y2,
		 const Int_t pad_number = 0);
    void DrawEllipse(TEllipse *ellipse,
		     const Double_t x1, const Double_t y1,
		     const Double_t r1, const Double_t r2,
		     const Double_t phimin, const Double_t phimax,
		     const Double_t theta, const Int_t pad_number = 0);
    void DrawLine(TLine *line,
		  const Double_t x1, const Double_t y1, const Double_t x2, const Double_t y2,
		  const Int_t pad_number = 0);
    void DrawMarker(TMarker *marker,
		    const Double_t x, const Double_t y,
		    const Int_t pad_number = 0);
    void DrawText(TText *text, const Double_t x, const Double_t y, const char *msg,
		  const Int_t pad_number = 0);
    
    void Draw(TObject *obj, Option_t *option = "", const Int_t pad_number = 0);
    void Draw(TTree *tr, const char* varexp, const char* selection, Option_t* option = "",
	      const Int_t pad_number = 0);
    void Draw(TTree *tr, const char* varexp, const char* selection, Option_t* option,
	      Long64_t nentries, Long64_t firstentry, const Int_t pad_number = 0);
    
    void DrawGraph(TGraph *g, Option_t *option = "", const Int_t pad_number = 0);
    
    void DrawHisto(   const char *hname, Option_t *option = "", const Int_t pad_number = 0);
    void DrawHisto(         TH1  *h,     Option_t *option = "", const Int_t pad_number = 0);
    void DrawHisto2D( const char *hname, Option_t *option = "", const Int_t pad_number = 0);
    
    void DrawInfo(Option_t *option = "");
    void DrawLegend(Option_t *option = "");
    
    void FitHisto(const char *hname, const char *formula,
		  Option_t *option = "", Option_t *goption = "",
		  const Double_t xmin = 0, const Double_t xmax = 0);
    void FitHisto(const char *hname, TF1 *f1,
		  Option_t *option = "", Option_t *goption = "",
		  const Double_t xmin = 0, const Double_t xmax = 0);
    
    TAxis* GetXaxis(const Int_t pad_number = 0);
    TAxis* GetYaxis(const Int_t pad_number = 0);
    TAxis* GetZaxis(const Int_t pad_number = 0);
    
    void OpenNewPDF(const char *pdf_name);
    void PrintCanvas();
    
    void SetInfoPad(const Double_t xlow, const Double_t ylow,
		    const Double_t xup,  const Double_t yup );
    void SetLegendPad(const Double_t xlow, const Double_t ylow,
		      const Double_t xup,  const Double_t yup );
    void SetLegendHeader(const char *header="");
    void SetLegendNColumns(Int_t n_columns);
    
    void SetLogx(const Int_t value = 1, const Int_t pad_number = 0);
    void SetLogy(const Int_t value = 1, const Int_t pad_number = 0);
    void SetLogz(const Int_t value = 1);
    
    void SetOptStat(TH1 *h, Int_t stat=1);
    
    void SetPadGridx(Int_t value = 1, const Int_t pad_number = 0);
    void SetPadGridy(Int_t value = 1, const Int_t pad_number = 0);
    void SetPadGrid(Int_t valuex = 1, Int_t valuey = 1, const Int_t pad_number = 0);
    
    void SetPadLeftMargin(Float_t leftmargin, const Int_t pad_number = 0);
    void SetPadRightMargin(Float_t rightmargin, const Int_t pad_number = 0);
    void SetPadBottomMargin(Float_t bottommargin, const Int_t pad_number = 0);
    void SetPadTopMargin(Float_t topmargin, const Int_t pad_number = 0);
    
    void SetStatColor(const char *hname, Color_t color);
    void SetStatColor(TH1 *h, Color_t color);
    void SetStatColor(TGraph *g, Color_t color);
    
    void SetStatPos(const char *hname,
		    const Double_t x1, const Double_t y1,
		    const Double_t x2, const Double_t y2 );
    void SetStatPos(TH1  *h,
		    const Double_t x1, const Double_t y1,
		    const Double_t x2, const Double_t y2 );
    void SetStatPos(TGraph *g,
		    const Double_t x1, const Double_t y1,
		    const Double_t x2, const Double_t y2 );
    
    void SetFitFormat(const char *hname, const char *format);
    void SetFitFormat(TH1  *h, const char *format);
    void SetFitFormat(TGraph *g, const char *format);
    
    void SetStatDivision(const Int_t n_row, const Int_t n_column);
    
    void SetXTitle(const char *title, const Int_t pad_number = 0);
    void SetYTitle(const char *title, const Int_t pad_number = 0);
    
  private:
    void Init();
    void PrintDebugInfo(std::string message);
    void AdjustFrame(TH1 *frame, const Int_t pad_number);
    void AdjustTitle(const Int_t pad_number);
    void SetRootStyle();
    void SetStat(TH1 *h, const Int_t pad_number);
    
    TString *fName;
    TString *fTitle;
    
    TCanvas *fCanvas;
    Double_t fDivisionRatio; // pad division ratio (for multi-pad)
    
    Double_t   fDrawRegionHeight;
    TPad      *fDrawPad;
    TH1F      *fPadFrame[3]; // 0: main, 1: sub-pad1, 2: sub-pad2
    TPad      *fInfoPad;
    TPaveText *fInfoTitle;
    TPaveText *fTextInfo;
    TPad      *fLegendPad;
    TLegend   *fLegend;
    TString   *fPDFName;
    
    Int_t fStatNrow;
    Int_t fStatNcolumn;
    
    Int_t fNhistos;
    Int_t fNlegend;
    Int_t fNstats;
    
    Bool_t fIsInitialized;
    Bool_t fIsDivided;
    
    ClassDef(KanaDrawManager, 1);
    
  };
  
}

#endif
