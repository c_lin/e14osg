/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAERRORMANAGER_H
#define E14ANA_KANALIB_KANAERRORMANAGER_H

#include <Rtypes.h>

namespace KanaLibrary
{
  
  class KanaErrorManager
  {
  public:
    KanaErrorManager(); // default ctor
    virtual ~KanaErrorManager();
    
  protected:
    UInt_t ErrorFlag;
    inline void SetErrorFlag(const UInt_t error_ID)
    {
      ErrorFlag |= (1<<error_ID);
    }
    inline void ResetErrorFlag(const UInt_t error_ID)
    {
      ErrorFlag &= ( ~(1<<error_ID) );
    }
    void DumpMsg(const std::string type, const std::string name, const std::string content);
    void DumpError(const std::string name, const std::string content);
    void DumpWarning(const std::string name, const std::string content);
    
  public:
    inline UInt_t GetErrorFlag()
    {
      return ErrorFlag;
    }

    ClassDef(KanaErrorManager, 1);
  };
  
}

#endif
