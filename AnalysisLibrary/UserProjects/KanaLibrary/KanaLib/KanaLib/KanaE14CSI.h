/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAE14CSI_H
#define E14ANA_KANALIB_KANAE14CSI_H

#include <map>

#include <TF1.h>
#include <TH2.h>

#include "KanaLib/KanaDrawManager.h"
#include "KanaLib/KanaE14Detector125MHz.h"

namespace KanaLibrary
{

  class KanaE14CSI : public KanaE14Detector125MHz
  {
  private:
    enum {
      LoadPositionMapError = 0
    };
    
  public:
    KanaE14CSI(); // default ctor
    KanaE14CSI(const Int_t acc_run_ID, const Int_t pro_version);
    virtual ~KanaE14CSI();
    
  private:
    void InitKanaE14CSI();
    
    void LoadPositionMap();
    std::map<Int_t, std::pair<Double_t,Double_t> > PositionMap; // first:X, second:Y

  public:
    Bool_t   IsLarge(const Int_t e14_ID) const
    {
      return (e14_ID<2240 ? kFALSE : kTRUE);
    }
    Double_t GetPosX(const Int_t e14_ID) const
    {
      std::map<Int_t,std::pair<Double_t,Double_t> >::const_iterator
        itr = PositionMap.find(e14_ID);
      return (itr!=PositionMap.end() ? itr->second.first : -9999.);
    }
    Double_t GetPosY(const Int_t e14_ID) const
    {
      std::map<Int_t,std::pair<Double_t,Double_t> >::const_iterator
        itr = PositionMap.find(e14_ID);
      return (itr!=PositionMap.end() ? itr->second.second : -9999.);
    }
    
  public:
    virtual void SetBranchAddresses(TTree *tr, const std::string b_list);
    virtual void AddBranches(TTree *tr, const std::string b_list);

  protected:
    void SetBranchAddressesCSI(TTree *tr, const std::string b_list);
    void AddBranchesCSI(TTree *tr, const std::string b_list);
    
  public:
    Float_t *PosX;
    Float_t *PosY;
    
    Float_t HoughR;
    Float_t HoughTheta;
    Float_t HoughIntercept;
    Float_t HoughSlope;
    
    Int_t    CosmicNumber;
    Int_t   *CosmicModID;
    Float_t *CosmicEne;
    Float_t *CosmicTime;
    Float_t *CosmicPosX;
    Float_t *CosmicPosEX;
    Float_t *CosmicPosY;
    Float_t *CosmicPosEY;
    Float_t  CosmicIntercept;
    Float_t  CosmicSlope;
    Float_t  CosmicFitChi2;
    Int_t    CosmicFitNDF;
    
    Double_t CosmicEnergyThreshold;

  public:
    void CalcCosmicTrack(const Double_t energy_threshold = 0.3);
    
  private:
    TF1     *f_CosmicTrack; // pol1 functions for a track in xy-plane
    
    void HoughTransform();
    TF1  *f_HoughLine;
    TH2D *h_HoughSpace;
    
  public:
    void DrawOutline(KanaDrawManager &man);

    ClassDef(KanaE14CSI, 1);
    
  };
  
}    

#endif
