/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAT0MANAGER_H
#define E14ANA_KANALIB_KANAT0MANAGER_H

#include <vector>
#include <map>

#include <Rtypes.h>

namespace KanaLibrary
{

  class KanaT0Manager
  {
  public:
    const std::string Name;
    const Int_t AccRunID;
    const Int_t ProVersion;

    Int_t ErrorFlag;
    enum {
      GetE14IDListError = 0,
      InitT0ListError
    };
    inline void SetErrorFlag(Int_t error_ID) { ErrorFlag |= (1<<error_ID); }
    
  public:
    KanaT0Manager(); // default ctor
    KanaT0Manager(const char *name, const Int_t acc_run_ID, const Int_t pro_version);
    virtual ~KanaT0Manager();

  private:
    void GetE14IDList();
    Int_t nChannels;
    std::vector<Int_t> E14IDList;
    
    void InitT0List();
    std::map<Int_t, Double_t> T0List;
    
  public:
    Int_t LoadT0(const char *path);

  public:
    Double_t GetCorrectedTime(const Int_t e14_ID, const Double_t itime)
    {
      return itime + T0List[e14_ID];
    }
    
    ClassDef(KanaT0Manager, 1); // omajinai
  };
  
}

#endif
