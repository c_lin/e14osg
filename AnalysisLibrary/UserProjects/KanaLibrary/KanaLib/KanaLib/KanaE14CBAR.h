/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAE14CBAR_H
#define E14ANA_KANALIB_KANAE14CBAR_H

#include <map>

#include <TF1.h>

#include "KanaLib/KanaE14Detector125MHz.h"

namespace KanaLibrary
{

  class KanaE14CBAR : public KanaE14Detector125MHz
  {
  public:
    const Double_t InnerRadius;
    const Double_t InnerThickness;
    const Double_t InnerWidth;
    const Double_t OuterRadius;
    const Double_t OuterThickness;
    const Double_t OuterWidth;
    const Double_t InnerHitRadius;
    const Double_t OuterHitRadius;
    const Double_t ModuleLength;
    
    const Double_t ModuleCenterZ;
    
    const Double_t LightPropSpeed;
    const Double_t InnerPosResolution;
    const Double_t OuterPosResolution;
    
    
  public:
    KanaE14CBAR(); // default ctor
    KanaE14CBAR(const Int_t acc_run_ID, const Int_t pro_version);
    virtual ~KanaE14CBAR();
    
  private:
    void InitKanaE14CBAR();
    
    void SetModulePositionMap();
    std::map<Int_t, std::pair<Double_t, Double_t> > PositionMap; // first: X, second: Y
    std::map<Int_t, std::pair<Double_t, Double_t> > PosErrorMap; // first: eX, second: eY

  public:
    Double_t GetPosX(const Int_t module_ID);
    Double_t GetPosY(const Int_t module_ID);
    
  public:
    Short_t (*ModulePeak)[2];
    Float_t (*ModuleIntegratedADC)[2];
    Float_t (*ModuleInitialTime)[2];
    
    Float_t *ModuleSumIntegratedADC;
    Float_t *ModuleInitialTimeAverage;
    Float_t *ModuleInitialTimeDifference;
    
    Float_t *ModuleEne;
    Float_t *ModuleHitTime;
    Float_t *ModuleDeltaTime;
    
    Float_t *ModuleHitX;
    Float_t *ModuleHitY;
    Float_t *ModuleHitZ;
    
    Int_t    CosmicModuleNumber;
    Int_t   *CosmicModuleModID;
    Float_t *CosmicModuleEne;
    Float_t *CosmicModuleTimeAverage;
    Float_t *CosmicModuleTimeDifference;
    Float_t *CosmicModuleHitX;
    Float_t *CosmicModuleHitY;
    Float_t *CosmicModuleHitZ;
    Float_t *CosmicModuleHitEX;
    Float_t *CosmicModuleHitEY;
    Float_t *CosmicModuleHitEZ;
    
    Float_t CosmicEnergyThreshold;
    Float_t CosmicInterceptXY;
    Float_t CosmicSlopeXY;
    Float_t CosmicInterceptZY;
    Float_t CosmicSlopeZY;
    Float_t CosmicInterceptTY;
    Float_t CosmicSlopeTY;
    
  public:
    virtual void SetBranchAddresses(TTree *tr, const std::string b_list);
    virtual void AddBranches(TTree *tr, const std::string b_list);
    
  protected:
    void SetBranchAddressesCBAR(TTree *tr, const std::string b_list);
    void AddBranchesCBAR(TTree *tr, const std::string b_list);
    
  public:
    void CalcCosmicTrack(const Double_t energy_threshold);
    
    void CalcCosmicTrack(const Double_t energy_threshold,
    		       const Double_t intercept_XY, const Double_t slope_XY);
    
  private:
    void GetCosmicTrack(const Int_t n, const Float_t *x, const Float_t *y,
  		      const Float_t *ex, const Float_t *ey,
  		      Float_t &intercept, Float_t &slope);
    Double_t GetTheta(const Double_t x, const Double_t y);
    
    // void GetCosmicTrackXY();
    // void GetCosmicTrackZY();
    // void GetCosmicTrackTY();
    
    // TF1  *f_HoughLine;
    // TH2D *h_HoughSpaceXY;
    // TH2D *h_HoughSpaceZY;
    // TH2D *h_HoughSpaceTY;
    
    TF1 *f_CosmicTrack;
    // TF1 *f_CosmicTrackXY;
    // TF1 *f_CosmicTrackZY;
    // TF1 *f_CosmicTrackTY;

    ClassDef(KanaE14CBAR, 1);
    
  };
  
}

#endif
