/* -*- C++ -*- */
#ifndef E14ANA_KANALIB_KANACALIBCONSTMANAGER_H
#define E14ANA_KANALIB_KANACALIBCONSTMANAGER_H

#include <string>
#include <vector>
#include <map>

#include <TTree.h>

#include "KanaE14DetectorBase.h"
#include "KanaFileReader.h"

namespace KanaLibrary
{
  
  class KanaCalibConstManager
  {
  private:
    
    const Int_t AccRunID;
    const Int_t ProVersion;
    const std::string DetectorName;
    
    KanaFileReader *FileReader;
    
  public:

    struct CalibFileListData
    {
      Int_t FileID;
      Int_t FirstRunID;
      Int_t LastRunID;
    };
    
    struct CalibConstData
    {
    public: 
      Float_t GainMean;
      Float_t GainSigma;
      Float_t MeVCoeff;
      Float_t T0Mean;
      Float_t T0Sigma;
    };

    KanaCalibConstManager();
    KanaCalibConstManager(const Int_t acc_run_ID, const Int_t pro_version, const std::string detector_name);
    virtual ~KanaCalibConstManager();
    
  private:
    void InitKanaCalibConstManager();
    
  public:
    void SetCalibDir(const std::string top_dir);
    
    Int_t GetnChannels() const { return E14IDList.size(); }
    Int_t GetE14ID(const Int_t index) const
    {
      return E14IDList.at(index);
    }
    
  private:
    std::string TopDirectory;
    
    std::vector<Int_t> E14IDList;
    
    std::vector<KanaCalibConstManager::CalibFileListData> LoadListFile(const std::string path);
    std::vector<CalibFileListData> CalibList;
    std::vector<CalibFileListData> T0List;
    
  public:
    Int_t GetCalibFileID(const Int_t run_ID);
    Int_t GetT0FileID(const Int_t run_ID);
    
  private:
    Int_t GetFileID(const std::vector<CalibFileListData> &list, const Int_t run_ID);
    
  public:
    void   LoadCalibConstFromCalibFile(const Int_t run_ID);
    void   LoadCalibConstFromDst(std::string path);
    Bool_t CalibReady() { return !NoCalibData; }
    Bool_t T0Ready()    { return !NoT0Data;    }
    
  private:
    Bool_t NoCalibData;
    Bool_t NoT0Data;
    std::map<Int_t,CalibConstData> CalibConst;
    
  public:
    Float_t GetGainMean(const Int_t e14_ID) const
    {
      std::map<Int_t,CalibConstData>::const_iterator itr = CalibConst.find(e14_ID);
      return (itr!=CalibConst.end() ? itr->second.GainMean : -1);
    }
    Float_t GetGainSigma(const Int_t e14_ID) const
    {
      std::map<Int_t,CalibConstData>::const_iterator itr = CalibConst.find(e14_ID);
      return (itr!=CalibConst.end() ? itr->second.GainSigma : -1);
    }
    Float_t GetMeVCoeff(const Int_t e14_ID) const
    {
      std::map<Int_t,CalibConstData>::const_iterator itr = CalibConst.find(e14_ID);
      return (itr!=CalibConst.end() ? itr->second.MeVCoeff : -1);
    }
    Float_t GetT0Mean(const Int_t e14_ID) const
    {
      std::map<Int_t,CalibConstData>::const_iterator itr = CalibConst.find(e14_ID);
      return (itr!=CalibConst.end() ? itr->second.T0Mean : -1);
    }
    Float_t GetT0Sigma(const Int_t e14_ID) const
    {
      std::map<Int_t,CalibConstData>::const_iterator itr = CalibConst.find(e14_ID);
      return (itr!=CalibConst.end() ? itr->second.T0Sigma : -1);
    }
    
  public:
    void DumpCalibConst(const Int_t n_ch);
    
    ClassDef(KanaCalibConstManager, 1);
  };
  
}

#endif
