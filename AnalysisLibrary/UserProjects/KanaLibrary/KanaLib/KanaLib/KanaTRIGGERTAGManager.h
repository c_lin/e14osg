/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANATRIGGERTAGMANAGER_H
#define E14ANA_KANALIB_KANATRIGGERTAGMANAGER_H

#include <TTree.h>

namespace KanaLibrary
{
  
  class KanaTRIGGERTAGManager
  {
  private:
    const Int_t AccRunID;
    const Int_t ProVersion;
    
    enum {
      nChannels = 6,
      Threshold = 10000
    };
    
    enum {
      CLOCK = 0,
      LASER,
      LED,
      CBARCosmic,
      FBARCosmic,
      CSICosmic
    };
    
  public:
    KanaTRIGGERTAGManager(); // default ctor
    KanaTRIGGERTAGManager(const Int_t acc_run_ID, const Int_t pro_version);
    virtual ~KanaTRIGGERTAGManager();
    
  public:
    void SetBranchAddresses(TTree *tr);
    
  private:
    Int_t    Number;
    Int_t    ModID[nChannels];
    Float_t  IntegratedADC[nChannels];
    
  public:
    Bool_t IsClockEvent();
    Bool_t IsLaserEvent();
    Bool_t IsLEDEvent();
    
    ClassDef(KanaTRIGGERTAGManager, 1); // omajinai
  };
  
}

#endif
