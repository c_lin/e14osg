/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAE14CV_H
#define E14ANA_KANALIB_KANAE14CV_H

#include <map>

#include <Rtypes.h>

#include "KanaLib/KanaE14Detector125MHz.h"

namespace KanaLibrary
{

  struct KanaCVPositionData
  {
    Int_t    ShortSideE14ID;
    Int_t    LongSideE14ID;
    Double_t MinX;
    Double_t MaxX;
    Double_t MinY;
    Double_t MaxY;
    Double_t MinZ;
    Double_t MaxZ;
    Double_t FiberCenterX;
    Double_t FiberCenterY;
    Double_t FiberCenterZ;
    Double_t FiberLength;
    Int_t    Quadrant;
  };

  class KanaE14CV : public KanaE14Detector125MHz
  {
  public:
    KanaE14CV(); // default ctor
    KanaE14CV(const Int_t acc_run_ID, const Int_t pro_version);
    virtual ~KanaE14CV();
    
  private:
    void InitKanaE14CV();
    
    void LoadPositionMap();
    std::map<Int_t,KanaCVPositionData> PositionMap;
    
  public:
    KanaCVPositionData GetPositionData(const Int_t module_ID) const
    {
      std::map<Int_t,KanaCVPositionData>::const_iterator itr = PositionMap.find(module_ID);
      return itr->second;
    }
    
  public:
    Short_t (*ModulePeak)[2];
    Float_t (*ModuleIntegratedADC)[2];
    Float_t (*ModuleInitialTime)[2];
    
    Float_t *ModuleSumIntegratedADC;
    Float_t *ModuleInitialTimeAverage;
    Float_t *ModuleInitialTimeDifference;

  public:
    virtual void SetBranchAddresses(TTree *tr, const std::string b_list);
    virtual void AddBranches(TTree *tr, const std::string b_list);

  protected:
    void SetBranchAddressesCV(TTree *tr, const std::string b_list);
    void AddBranchesCV(TTree *tr, const std::string b_list);
    
  public:
    void ProcessModuleInfo();
    
    ClassDef(KanaE14CV, 1);
    
  };
  
}

#endif
