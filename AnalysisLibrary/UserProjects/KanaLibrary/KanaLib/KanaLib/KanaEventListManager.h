/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAEVENTLISTMANAGER_H
#define E14ANA_KANALIB_KANAEVENTLISTMANAGER_H

#include <iostream>
#include <sstream>

#include <TFile.h>
#include <TEventList.h>

#include "KanaLib/KanaErrorManager.h"

namespace KanaLibrary
{
    
  class KanaEventListManager : public KanaErrorManager
  {
  private:
    std::string TopDir;
    
  public:
    KanaEventListManager(); // default ctor
    virtual ~KanaEventListManager();
    
  private:
    void InitKanaEventListManager();
    TFile      *RootFile;
    TEventList *EventList;
    
  public:
    Int_t LoadEventList(const Int_t acc_run_ID, const Int_t pro_version,
			const Int_t run_ID, const Int_t node_ID, const Int_t file_ID,
			const Int_t version, const std::string name);
    
  public:
    Int_t    GetN();
    Long64_t GetEntry(Int_t index);
    
    ClassDef(KanaEventListManager, 1);
  };
  
}

#endif
