/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANACHMAPDATA_H
#define E14ANA_KANALIB_KANACHMAPDATA_H

namespace KanaLibrary
{

  struct KanaChMapData
  {
    Int_t FADCCrateID;
    Int_t FADCModID;
    Int_t FADCChannelID;
  };

}

#endif

