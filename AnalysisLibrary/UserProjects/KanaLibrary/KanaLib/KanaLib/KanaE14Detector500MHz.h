/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAE14DETECTOR500MHZ_H
#define E14ANA_KANALIB_KANAE14DETECTOR500MHZ_H

#include "KanaLib/KanaE14DetectorBase.h"

namespace KanaLibrary
{

  class KanaE14Detector500MHz : public KanaE14DetectorBase
  {
  protected:
    enum {
      MaxnHits = 20
    };
    
    Int_t    FADCnSamples;
    Double_t FADCSampleInterval;
    
  public:
    KanaE14Detector500MHz(); // default ctor
    KanaE14Detector500MHz(const Int_t acc_run_ID, const Int_t pro_version, const std::string name);
    virtual ~KanaE14Detector500MHz();
    
  private:
    void InitKanaE14Detector500MHz();
    
  public:
    Short_t *nHits;
    Short_t (*Peak)[MaxnHits];
    Short_t (*PeakTime)[MaxnHits];
    Float_t (*IntegratedADC)[MaxnHits];
    Float_t (*InitialTime)[MaxnHits];
    Float_t (*Ene)[MaxnHits];
    Float_t (*Time)[MaxnHits];
    
  public:
    virtual void SetBranchAddresses(TTree *tr, const std::string b_list);
    virtual void AddBranches(TTree *tr, const std::string b_list);

  protected:
    void SetBranchAddresses500MHz(TTree *tr, const std::string b_list);
    void AddBranches500MHz(TTree *tr, const std::string b_list);

  public:
    virtual void ZeroSuppression(const Double_t th);

    ClassDef(KanaE14Detector500MHz, 1);
    
  };
  
}

#endif
