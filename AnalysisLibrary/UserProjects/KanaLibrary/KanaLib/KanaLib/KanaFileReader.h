/* -*- C++ -*- */
#ifndef E14ANA_KANALIB_KANAFILEREADER_H
#define E14ANA_KANALIB_KANAFILEREADER_H

#include <istream>
#include <fstream>
#include <string>
#include <vector>

#include <Rtypes.h>

namespace KanaLibrary
{
  using std::char_traits;
  
  class KanaFileReader
  {
  public:
    KanaFileReader();
    virtual ~KanaFileReader();
    
  private:
    void InitKanaFileReader();
    
  public:
    void SetFile(const std::string path);
    
    std::vector<std::basic_istream<char,char_traits<char> >::pos_type>::size_type GetnLines() const
    {
      return LinePositionList.size();
    }
    void ReadLine(std::vector<std::basic_istream<char,char_traits<char> >::pos_type>::size_type index,
		  std::string option = "ssv");
    
    std::vector<std::string>::size_type GetnWords() const
    {
      return WordList.size();
    }
    std::string GetWord(std::vector<std::string>::size_type index) const
    {
      return WordList.at(index);
    }
    
    bool IsOpen() { return InputFile.is_open(); }
    
  private:
    std::ifstream InputFile;
    std::vector<std::basic_istream<char,char_traits<char> >::pos_type> LinePositionList;
    std::vector<std::string> WordList;
    
    ClassDef(KanaFileReader, 1);
  };
  
}

#endif
