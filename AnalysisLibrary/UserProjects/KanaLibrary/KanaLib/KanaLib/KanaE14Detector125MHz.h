/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAE14DETECTOR125MHZ_H
#define E14ANA_KANALIB_KANAE14DETECTOR125MHZ_H

#include "KanaLib/KanaE14DetectorBase.h"

namespace KanaLibrary
{

  class KanaE14Detector125MHz : public KanaE14DetectorBase
  {
  protected:
    Int_t    FADCnSamples;
    Double_t FADCSampleInterval;
    
  public:
    KanaE14Detector125MHz(); // default ctor
    KanaE14Detector125MHz(const Int_t acc_run_ID, const Int_t pro_version, const std::string name);
    virtual ~KanaE14Detector125MHz();
    
  private:
    void InitKanaE14Detector125MHz();
    
  public:
    Short_t *Peak;
    Short_t *PeakTime;
    Float_t *IntegratedADC;
    Float_t *InitialTime;
    Float_t *PTime;
    Float_t *Ene;
    Float_t *Time;
    
  public:
    virtual void SetBranchAddresses(TTree *tr, const std::string b_list);
    virtual void AddBranches(TTree *tr, const std::string b_list);

  protected:
    void SetBranchAddresses125MHz(TTree *tr, const std::string b_list);
    void AddBranches125MHz(TTree *tr, const std::string b_list);

  public:
    virtual void ZeroSuppression(const Double_t th);

    ClassDef(KanaE14Detector125MHz, 1);
    
  };
  
}

#endif
