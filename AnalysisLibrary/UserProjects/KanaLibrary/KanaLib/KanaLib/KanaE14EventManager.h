/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAE14EVENTMANAGER_H
#define E14ANA_KANALIB_KANAE14EVENTMANAGER_H

#include <TTree.h>

namespace KanaLibrary
{
  
  class KanaE14EventManager
  {
  public:
    KanaE14EventManager(); // default ctor
    virtual ~KanaE14EventManager();
    
  private:
    void InitKanaE14EventManager();
    
  private:
    Int_t   EventNo;
    Int_t   L1TrigNo;
    Int_t   L2TrigNo;
    Short_t SpillNo;
    Int_t   TimeStamp;
    UInt_t  L2TimeStamp;
    Short_t Error;
    Short_t TrigTagMismatch;
    Short_t L2AR;
    UInt_t  DetectorBit;
    UInt_t  RawTrigBit;
    UInt_t  ScaledTrigBit;
    
  public:
    inline Int_t   GetEventNo()         { return EventNo;         }
    inline Int_t   GetL1TrigNo()        { return L1TrigNo;        }
    inline Int_t   GetL2TrigNo()        { return L2TrigNo;        }
    inline Short_t GetSpillNo()         { return SpillNo;         }
    inline Int_t   GetTimeStamp()       { return TimeStamp;       }
    inline UInt_t  GetL2TimeStamp()     { return L2TimeStamp;     }
    inline Short_t GetError()           { return Error;           }
    inline Short_t GetTrigTagMismatch() { return TrigTagMismatch; }
    inline Short_t GetL2AR()            { return L2AR;            }
    inline UInt_t  GetDetectorBit()     { return DetectorBit;     }
    inline UInt_t  GetRawTrigBit()      { return RawTrigBit;      }
    inline UInt_t  GetScaledTrigBit()   { return ScaledTrigBit;   }
    
  public:
    void SetBranchAddresses(TTree *tr);
    
    
    ClassDef(KanaE14EventManager, 1);
    
  };
  
}    

#endif
