/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAE14IDMANAGER_H
#define E14ANA_KANALIB_KANAE14IDMANAGER_H

#include <vector>
#include <map>

#include <Rtypes.h>

#include "KanaLib/KanaChMapData.h"

namespace KanaLibrary
{

  class KanaE14IDManager
  {
  private:
    const Int_t AccRunID;   // accelerator run ID
    const Int_t ProVersion; // production version
    
  public:
    KanaE14IDManager(); // default ctor
    KanaE14IDManager(const Int_t acc_run_ID, const Int_t pro_version);
    virtual ~KanaE14IDManager();
    
  public:
    Int_t GetE14IDList(const std::string detector_name, std::vector<Int_t> &list);
    
  private:
    
    std::vector<Int_t> GetBCVE14IDList();
    std::vector<Int_t> GetBHCVE14IDList();
    std::vector<Int_t> GetBHGCE14IDList();
    std::vector<Int_t> GetBHPVE14IDList();
    std::vector<Int_t> GetBPCVE14IDList();
    std::vector<Int_t> GetCBARE14IDList();
    std::vector<Int_t> GetCC03E14IDList();
    std::vector<Int_t> GetCC04E14IDList();
    std::vector<Int_t> GetCC05E14IDList();
    std::vector<Int_t> GetCC06E14IDList();
    std::vector<Int_t> GetCSIE14IDList();
    std::vector<Int_t> GetCVE14IDList();
    std::vector<Int_t> GetFBARE14IDList();
    std::vector<Int_t> GetIBE14IDList();
    std::vector<Int_t> GetIBCVE14IDList();
    std::vector<Int_t> GetLCVE14IDList();
    std::vector<Int_t> GetMBCVE14IDList();
    std::vector<Int_t> GetNCCE14IDList();
    std::vector<Int_t> GetnewBHCVE14IDList();
    std::vector<Int_t> GetOEVE14IDList();
    std::vector<Int_t> GetTRIGGERTAGE14IDList();
    
  public:
    Int_t LoadChannelMap(const char *detector_name);
    
  private:
    std::map<Int_t, KanaChMapData> ChannelMap;
    
  public:
    Int_t GetFADCCrateID(const Int_t e14_ID);
    Int_t GetFADCModID(const Int_t e14_ID);
    Int_t GetFADCChannelID(const Int_t e14_ID);
    
    ClassDef(KanaE14IDManager, 1); // omajinai
  };
  
}

#endif
