/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAMODULEIDMANAGER_H
#define E14ANA_KANALIB_KANAMODULEIDMANAGER_H

#include <vector>
#include <map>

#include <Rtypes.h>

namespace KanaLibrary
{
  
  class KanaModuleIDManager
  {
  private:
    const Int_t AccRunID;   // accelerator run ID
    const Int_t ProVersion; // production version
    
  public:
    KanaModuleIDManager(); // default ctor
    KanaModuleIDManager(const Int_t acc_run_ID, const Int_t pro_version);
    virtual ~KanaModuleIDManager();
    
  public:
    Int_t GetModuleIDList(const std::string detector_name, std::vector<Int_t> &list);

  private:  

    std::vector<Int_t> GetBCVModuleIDList();
    std::vector<Int_t> GetBHCVModuleIDList();
    std::vector<Int_t> GetBHGCModuleIDList();
    std::vector<Int_t> GetBHPVModuleIDList();
    std::vector<Int_t> GetBPCVModuleIDList();
    std::vector<Int_t> GetCBARModuleIDList();
    std::vector<Int_t> GetCC03ModuleIDList();
    std::vector<Int_t> GetCC04ModuleIDList();
    std::vector<Int_t> GetCC05ModuleIDList();
    std::vector<Int_t> GetCC06ModuleIDList();
    std::vector<Int_t> GetCSIModuleIDList();
    std::vector<Int_t> GetCVModuleIDList();
    std::vector<Int_t> GetFBARModuleIDList();
    std::vector<Int_t> GetLCVModuleIDList();
    std::vector<Int_t> GetNCCModuleIDList();
    std::vector<Int_t> GetnewBHCVModuleIDList();
    std::vector<Int_t> GetOEVModuleIDList();

    ClassDef(KanaModuleIDManager, 1); // omajinai
  };
  
}

#endif
