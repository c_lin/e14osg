/* -*- C++ -*- */

#ifndef E14ANA_KANALIB_KANAHISTOMANAGER_H
#define E14ANA_KANALIB_KANAHISTOMANAGER_H

#include <string>

#include <TClass.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

namespace KanaLibrary
{
  
  class KanaHistoManager
  {
  private:
    const std::string Path;

    UInt_t Error;
    
  public:
    KanaHistoManager();
    KanaHistoManager(const std::string path);
    virtual ~KanaHistoManager();

    inline UInt_t GetError() { return Error; }
    
  private:
    void InitKanaHistoManager();
    
    void OpenInputFile();
    TFile *InputFile;
    
    void GetHistograms();
    std::map<std::string,TH1*> Histo1;
    std::map<std::string,TH2*> Histo2;
    
  public:
    std::vector<std::string> GetHisto1List();
    std::vector<std::string> GetHisto2List();
    
  public:
    TH1* GetHisto1(const std::string name, const std::string h_name);
    TH2* GetHisto2(const std::string name, const std::string h_name);

  public:
    Double_t GetEntries(const std::string name);
    
  public:
    Double_t IntegralHisto1(const std::string name,
			    const Double_t xmin, const Double_t xmax);
    Double_t IntegralHisto2(const std::string name,
			    const Double_t xmin, const Double_t xmax,
			    const Double_t ymin, const Double_t ymax);
    
    ClassDef(KanaHistoManager, 1);
  };
  
}

#endif
