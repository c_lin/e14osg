#ifndef E14ANA_SSHINOSUPPCONFINFOMANAGER_SUPPCONFINFOMANAGER_H
#define E14ANA_SSHINOSUPPCONFINFOMANAGER_SUPPCONFINFOMANAGER_H

#include <TChain.h>
#include <TString.h>
#include <TFile.h>

namespace SshinoLib
{
  
  class SuppConfInfoManager
  {
  public:
    SuppConfInfoManager( const Char_t *InputFileName );
    ~SuppConfInfoManager();
    void SetInputFile( const Char_t *InputFileName );
    void WriteSuppConfInfo( TFile *OutputTFile ) ;

  private:

    TFile  *tfi;
    TTree  *tri_suppconf;
    TTree  *tri_supp_pfconf;
    TTree  *tri_pfconf;

    TTree  *tro_suppconf;
    TTree  *tro_supp_pfconf;
    TTree  *tro_pfconf;

    Bool_t m_suppconfFlag;
    Bool_t m_supp_pfconfFlag;
    Bool_t m_pfconfFlag;

    void FillSuppConf();
    void FillSuppPfConf();
    void FillPfConf();
    
  };
}

#endif
