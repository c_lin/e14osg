#include <iostream>
#include <fstream>

#include <TSystem.h>
#include <TChain.h>
#include <TString.h>

#include "SshinoSuppConfInfoManager/SuppConfInfoManager.h"

namespace SshinoLib
{
  SuppConfInfoManager::SuppConfInfoManager( const Char_t *InputFileName )
  {

    m_suppconfFlag    = false;
    m_supp_pfconfFlag = false;
    m_pfconfFlag      = false;

    SetInputFile(InputFileName);

  }

  SuppConfInfoManager::~SuppConfInfoManager()
  {
    delete tfi;
  }
  
  void SuppConfInfoManager::SetInputFile( const Char_t *InputFileName ) 
  { 

    tfi = new TFile(InputFileName);

    tri_suppconf = dynamic_cast<TTree*>(tfi->Get("suppconf"));
    if( tri_suppconf!=NULL ) m_suppconfFlag = true;

    tri_supp_pfconf = dynamic_cast<TTree*>(tfi->Get("supp_pfconf"));
    if( tri_supp_pfconf!=NULL ) m_supp_pfconfFlag = true;

    tri_pfconf = dynamic_cast<TTree*>(tfi->Get("pfconf"));
    if( tri_pfconf!=NULL ) m_pfconfFlag = true;

  }

  void SuppConfInfoManager::FillSuppConf() 
  { 
    
    tro_suppconf = tri_suppconf -> CloneTree(0);
    tri_suppconf ->GetEntry(0);
    tro_suppconf ->Fill();

  }

  void SuppConfInfoManager::FillSuppPfConf() 
  {

    tro_supp_pfconf = tri_supp_pfconf -> CloneTree(0);
    tri_supp_pfconf ->GetEntry(0);
    tro_supp_pfconf ->Fill();

  }

  void SuppConfInfoManager::FillPfConf() 
  {

    tro_pfconf = tri_pfconf -> CloneTree(0);
    tri_pfconf ->GetEntry(0);
    tro_pfconf ->Fill();

  }
  
  void SuppConfInfoManager::WriteSuppConfInfo( TFile *OutputTFile )
  {

    TFile *tfo = OutputTFile;
    if( m_suppconfFlag ){
      FillSuppConf();
      tfo->cd();
      tro_suppconf->Write();
    }
    if( m_supp_pfconfFlag ){
      FillSuppPfConf();
      tfo->cd();
      tro_supp_pfconf->Write();
    }
    if( m_pfconfFlag ){
      FillPfConf();
      tfo->cd();
      tro_pfconf->Write();
    }


  }
}
