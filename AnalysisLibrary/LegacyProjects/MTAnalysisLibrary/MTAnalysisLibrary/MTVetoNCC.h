#ifndef __MTVETONCC_H__
#define __MTVETONCC_H__

#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTVetoNCC : public MTVeto
{
public:
  MTVetoNCC( Int_t userFlag );
  ~MTVetoNCC();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );

private:
  Int_t   NCCVetoModID;
  Float_t NCCVetoEne;
  Float_t NCCVetoTime;
  Float_t NCCTotalVetoEne;

  Int_t   NCCIndividualVetoModID;
  Float_t NCCIndividualVetoEne;
  Float_t NCCIndividualVetoTime;

  ///// added by Nakagiri (2017/6/30)
  Int_t    NCCScintiVetoModID;
  Float_t  NCCScintiVetoEne;
  Float_t  NCCScintiVetoTime;

  Int_t    ScintiDetNumber;
  Int_t   *ScintiDetModID;
  Float_t *ScintiDetEne;
  Float_t *ScintiDetTime;
  Float_t *ScintiDetSmoothness;
  Short_t *ScintiDetPSBit;
  
  Int_t    IndividualDetNumber;
  Int_t   *IndividualDetModID;
  Float_t *IndividualDetEne;
  Float_t *IndividualDetTime;
  Float_t *IndividualDetSmoothness;
  Short_t *IndividualDetPSBit;

  /////
  
  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );
  
  const Int_t m_userFlag;

};

#endif //__MTVETONCC_H__
