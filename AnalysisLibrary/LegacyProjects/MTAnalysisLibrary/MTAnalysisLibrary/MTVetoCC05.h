#ifndef __MTVETOCC05_H__
#define __MTVETOCC05_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoCC05:public MTVeto
{
 public:
  MTVetoCC05( Int_t userFlag );
  ~MTVetoCC05();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


 private:
  Int_t CC05VetoModID;
  float CC05VetoEne;
  float CC05VetoTime;

  Int_t CC05E391VetoModID;
  float CC05E391VetoEne;
  float CC05E391VetoTime;

  Int_t CC05ScintiVetoModID;
  float CC05ScintiVetoEne;
  float CC05ScintiVetoTime;
  
  int E391DetNumber;
  int* E391DetModID;
  float* E391DetEne;
  float* E391DetTime;
  float* E391DetSmoothness;
  float* E391DetAreaR;
  short* E391DetWfmCorrectNumber;
  short* E391DetPSBit;

  int ScintiDetNumber;
  int* ScintiDetModID;
  float* ScintiDetEne;
  float* ScintiDetTime;
  float* ScintiDetSmoothness;
  float* ScintiDetAreaR;
  short* ScintiDetWfmCorrectNumber;
  short* ScintiDetPSBit;
  
  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOCC05_H__
