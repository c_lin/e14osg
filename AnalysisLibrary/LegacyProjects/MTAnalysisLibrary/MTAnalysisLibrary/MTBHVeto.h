#ifndef __MTBHVETO_H__
#define __MTBHVETO_H__

#include <iostream>
#include "TROOT.h"
#include "TTree.h"

#include "klong/Klong.h"
#include "pi0/Pi0.h"
#include "gnana/E14GNAnaDataContainer.h"
#include "MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTVeto.h"

/*
struct pVetoHit{
  int modID;
  float energy;
  float time;
  float position;
  static bool fasterHit( const pVetoHit& et0, const pVetoHit& et1 ){ return et0.time < et1.time; }
  static bool laterHit( const pVetoHit& et0, const pVetoHit& et1 ){ return et0.time > et1.time; }
  static bool largerHit( const pVetoHit& et0, const pVetoHit& et1 ){ return et0.energy > et1.energy; }
  static bool smallerHit( const pVetoHit& et0, const pVetoHit& et1 ){ return et0.energy < et1.energy; }
};
*/


class MTBHVeto
{
 public:
  MTBHVeto( void );
  ~MTBHVeto( void );


  virtual void SetBranchAddress( TTree* tr );
  void SetDataContainer( E14GNAnaDataContainer* data ) { m_data = data; };
  virtual void Branch( TTree* tr );
  virtual void UpdatedBranch( TTree* tr ){};
  virtual void BranchOriginalData( TTree* tr ){};
  virtual void Initialize( void );
  virtual void Initialize2( void ){};
  virtual bool Process( int mode=0 ) = 0;
  virtual bool Process( int mode, bool& vetoFlag, bool& looseVetoFlag, bool& tightVetoFlag ){};

  void SetEventStartInfo( double eventStartZ, double eventStartTime );  
  void SetEventStartZ( double z );
  double GetEventStartZ( void );
  void SetEventStartTime( double t );
  double GetEventStartTime( void );
  virtual void SetKlInfo( Klong* kl ) {}; // for MTBHVetoCSI only so far.
  virtual void SetPi0Info( Pi0* pi0 ) {}; // for MTBHVetoCSI only so far.
  void SetVetoWindow( double windowLength, double windowOffset );
  void GetVetoWindow( double& windowLength, double& windowOffset );
  double GetVetoStartTime( void );
  double GetVetoEndTime( void );
  void SetVetoThreshold( double eThreshold );
  void SetLooseVetoThreshold( double eThreshold );
  void SetTightVetoThreshold( double eThreshold );
  double GetVetoThreshold( void );
  double GetLooseVetoThreshold( void );
  double GetTightVetoThreshold( void );

  void SetClusteringTree(TTree* tree) { m_clusterTree=tree;}
  void SetTimeOffset(double offset);
  
  // same format as dst file
  std::string DetName;
  Int_t    DetNumber;
  Int_t*   DetModID;
  Short_t* DetNHit;
  Short_t* DetPSBit;
  Float_t (*DetAreaR500)[20];
  Float_t (*DetEne)[20];
  Float_t (*DetTime)[20];

  Float_t (*DetSuppWfm500)[MTBP::nSample500];
  int      DetSuppWfmNumber;
  int     *DetSuppWfmModID;

  //// for IB Wfm MC ////
  Float_t  *AccidentalDetECalibConst;
  Float_t  *AccidentalDetTCalibConst;

 protected:
  TTree* m_clusterTree;
  TTree* m_recTree;
  E14GNAnaDataContainer* m_data;
  double m_eventStartZ;
  double m_eventStartTime;
  double m_vetoWindowLength;
  double m_vetoWindowOffset;
  double m_vetoStartTime;
  double m_vetoEndTime;
  double m_vetoThreshold;
  double m_looseVetoThreshold;
  double m_tightVetoThreshold;

  float vetoEne;
  float vetoTime;

  bool IsInVetoWindow( double t );
  void MakeWfmArray();

};


#endif //__MTBHVETO_H__
