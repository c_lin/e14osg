#ifndef __MTVETOUCV_H__
#define __MTVETOUCV_H__

#include "MTBHVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTVetoUCV:public MTBHVeto
{
 public:
  MTVetoUCV( Int_t userFlag );
  ~MTVetoUCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );
  
  Int_t   UCVVetoModID;
  Float_t UCVTotalVetoEne;
  Float_t UCVVetoEne;
  Float_t UCVVetoTime;
  
  Float_t (*DetPeakTime)[20];
  Float_t (*DetFallTime)[20];
  Float_t (*DetPeakWidth)[20];
  
  Int_t     ModuleNumber;
  Int_t*    ModuleModID;
  Float_t (*ModuleTime);
  Float_t (*ModuleEne);
  Float_t (*ModuleHitZ);

 private:


  float* UCVModuleDeltaTime;
  Float_t* TOFCorrection;

  pVetoHit* vetoHitBuffer;
  Int_t MaxBufferNum;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};


#endif //__MTVETOUCV_H__
