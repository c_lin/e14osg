#ifndef __MTVETODetector_H__
#define __MTVETODetector_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoDetector:public MTVeto
{
 public:
  MTVetoDetector(int detID);
  ~MTVetoDetector();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );

  int DetectorID;
  Float_t* DetHitZ;

 private:

  pVetoHit* vetoHitBuffer;


};


#endif // __MTVETODetector_H__
