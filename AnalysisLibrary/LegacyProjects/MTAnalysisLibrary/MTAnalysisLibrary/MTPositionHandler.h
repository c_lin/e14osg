#ifndef __MTPOSITIONHANDLER_H__
#define __MTPOSITIONHANDLER_H__

#include <iostream>
#include "MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTPositionHandler
{
 private:
  MTPositionHandler();
  ~MTPositionHandler();
  MTPositionHandler( const MTPositionHandler& ph );
  MTPositionHandler& operator=( const MTPositionHandler& ph );
  
  // CSI
  double* CSIXPosition;
  double* CSIYPosition;
  bool LoadCSIPosition( void );


  // CV
  double* CVXCenter;
  double* CVYCenter;
  double* CVXMin;
  double* CVXMax;
  double* CVYMin;
  double* CVYMax;
  double* CVFiberLength;
  int* CVModID;
  int* CVShortSideChannel;
  int* CVLongSideChannel;
  int* CVQuadrant;
  int* CVDirection; // 0:holizontal, 1:vertical
  bool LoadCVPosition( void );

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( int userFlag, char* detName ) const;

 public:

  static MTPositionHandler* GetInstance()
  {
    static MTPositionHandler instance;
    return &instance;
  }

  // CSI
  double GetCSIXPosition( int ich ) const;
  double GetCSIYPosition( int ich ) const;
  double GetCSIZPosition( int ich, int userFlg ) const;
  bool   GetCSIXYZPosition( int ich, double& x, double& y, double&z, int userFlag ) const;

  // CV
  int    GetCVModIndex( int imod ) const;
  double GetCVXCenter( int imod ) const;
  double GetCVYCenter( int imod ) const;
  double GetCVXMin( int imod ) const;
  double GetCVYMin( int imod ) const;
  double GetCVXMax( int imod ) const;
  double GetCVYMax( int imod ) const;
  double GetCVZCenter( int imod, int userFlag ) const;
  bool   GetCVXYZCenter( int imod, double& x, double& y, double& z, int userFlag ) const;
  int    GetCVQuadrant( int imod ) const;
  int    GetCVDirection( int imod ) const;


};


#endif //__MTPOSITIONHANDLER_H__
