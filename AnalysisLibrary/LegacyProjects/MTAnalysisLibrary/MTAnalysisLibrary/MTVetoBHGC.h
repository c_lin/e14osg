#ifndef __MTVETOBHGC_H__
#define __MTVETOBHGC_H__

#include "MTBHVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTVetoBHGC:public MTBHVeto
{
 public:
  MTVetoBHGC( Int_t userFlag );
  ~MTVetoBHGC();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  //  void UpdatedBranch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );

  Int_t BHGCVetoModID;
  Float_t BHGCVetoEne;
  Float_t BHGCVetoTime;

  Int_t ModuleNumber;
  Int_t* ModuleModID;
  Short_t* ModuleNHit;
  Float_t (*ModuleEne)[20];
  Float_t (*ModuleHitTime)[20];
  Float_t (*ModuleDeltaTime)[20];
 
 private:
  Float_t* TOFCorrection;

  pVetoHit* vetoHitBuffer;
  Int_t MaxBufferNum;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};

#endif //__MTVETOBHGC_H__
