#ifndef __MTVETOIBCV_H__
#define __MTVETOIBCV_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoIBCV:public MTVeto
{
 public:
  MTVetoIBCV( Int_t userFlag );
  ~MTVetoIBCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );

  Float_t* DetHitZ;
  
 private:
  Int_t IBCVVetoModID;
  float IBCVVetoEne;
  float IBCVVetoTime;
  float IBCVVetoHitZ;
  float* IBCVModuleDeltaTime;

  float IBCVCH27VetoEne;
  float IBCVCH27VetoTime;
  
  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOIBCV_H__
