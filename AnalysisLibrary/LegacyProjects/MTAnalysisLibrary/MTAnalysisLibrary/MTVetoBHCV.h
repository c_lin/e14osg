#ifndef __MTVETOBHCV_H__
#define __MTVETOBHCV_H__

#include "MTBHVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

//distributed at 12th Dec, 2013 by Maeda Yosuke
// *TOF correction is added

class MTVetoBHCV:public MTBHVeto
{
 public:
  MTVetoBHCV( Int_t userFlag );
  ~MTVetoBHCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  //  void UpdatedBranch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


  Int_t BHCVVetoModID;
  Float_t BHCVVetoEne;
  Float_t BHCVVetoTime;

 private:
  Float_t* TOFCorrection;

  pVetoHit* vetoHitBuffer;
  Int_t MaxBufferNum;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOBHCV_H__
