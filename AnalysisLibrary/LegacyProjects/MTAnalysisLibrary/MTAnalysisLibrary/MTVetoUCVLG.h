#ifndef __MTVETOUCVLG_H__
#define __MTVETOUCVLG_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoUCVLG:public MTVeto
{
 public:
  MTVetoUCVLG( Int_t userFlag );
  ~MTVetoUCVLG();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );

 private:

  Int_t   UCVLGVetoModID;
  Float_t UCVLGTotalVetoEne;
  Float_t UCVLGVetoEne;
  Float_t UCVLGVetoTime;


  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

  
};







#endif //__MTVETOUCVLG_H__
