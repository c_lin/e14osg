#ifndef __MTVETOOEV_H__
#define __MTVETOOEV_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoOEV:public MTVeto
{
 public:
  MTVetoOEV( Int_t userFlag );
  ~MTVetoOEV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


 private:
  Int_t OEVVetoModID;
  float OEVVetoEne;
  float OEVVetoTime;

  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOOEV_H__
