#ifndef __MTVETOBPCV_H__
#define __MTVETOBPCV_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTVetoBPCV:public MTVeto
{
 public:
  MTVetoBPCV( Int_t userFlag );
  ~MTVetoBPCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


 private:
  Int_t BPCVVetoModID;
  float BPCVVetoEne;
  float BPCVVetoTime;

  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOBPCV_H__
