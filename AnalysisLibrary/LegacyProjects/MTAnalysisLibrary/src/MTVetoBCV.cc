#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoBCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoBCV::MTVetoBCV( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "BCV"; 
  DetNumber = MTBP::BCVNModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetHitZ  = new Float_t [DetNumber]();

  BCVModuleDeltaTime = new float [DetNumber]();

  vetoHitBuffer = new pVetoHit [DetNumber]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  MakeWfmArray();

  Initialize();
}


MTVetoBCV::~MTVetoBCV()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetHitZ;
  delete[] BCVModuleDeltaTime;
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoBCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "BCVModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "BCVModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "BCVModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "BCVModuleEne", DetEne );
  m_clusterTree->SetBranchAddress( "BCVModuleHitZ", DetHitZ );
  MTVeto::SetBranchAddress(tr);
}


void MTVetoBCV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "BCVModuleNumber", &DetNumber, "BCVModuleNumber/I");
  m_recTree->Branch( "BCVModuleModID", DetModID, "BCVModuleModID[BCVModuleNumber]/I");
  m_recTree->Branch( "BCVModuleEne", DetEne, "BCVModuleEne[BCVModuleNumber]/F");
  m_recTree->Branch( "BCVModuleTime", DetTime, "BCVModuleTime[BCVModuleNumber]/F");
  m_recTree->Branch( "BCVModuleDeltaTime", BCVModuleDeltaTime, "BCVModuleDeltaTime[BCVModuleNumber]/F" );
  
  m_recTree->Branch( "BCVVetoModID", &BCVVetoModID,"BCVVetoModID/I");
  m_recTree->Branch( "BCVVetoEne", &BCVVetoEne,"BCVVetoEne/F");
  m_recTree->Branch( "BCVVetoTime", &BCVVetoTime,"BCVVetoTime/F");
  
  MTVeto::Branch(tr);
  
}


void MTVetoBCV::Initialize( void )
{

  for( int imod=0; imod<MTBP::BCVNModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
    vetoHitBuffer[imod].position = 0;
  }

  BCVVetoModID = -1;
  BCVVetoEne = 0;
  BCVVetoTime = 9999.;

}


bool MTVetoBCV::Process( int mode ){

  Initialize();
  
  static const double s_BCVZPosition = GetDetZPosition("BCV");

  for( int imod=0; imod<DetNumber; imod++ ){
    // veto time calculation
    BCVModuleDeltaTime[imod] = DetTime[imod] - m_eventStartTime;
    float deltaZ = m_eventStartZ - ( s_BCVZPosition + MTBP::BCVLength/2. );
    if( DetHitZ[imod] < -3000 ){
      deltaZ += 3000;
    }else if( DetHitZ[imod] > 3000 ){
      deltaZ -= 3000;
    }else{
      deltaZ -= DetHitZ[imod];
    }
    BCVModuleDeltaTime[imod] -= sqrt( pow( deltaZ, 2) + pow( MTBP::BCVInnerR, 2) ) / (TMath::C()/1E6);
        
    if( IsInVetoWindow( BCVModuleDeltaTime[imod] )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = BCVModuleDeltaTime[imod];
      vetoHitBuffer[imod].position = DetHitZ[imod];
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = BCVModuleDeltaTime[imod];
      vetoHitBuffer[imod].position = DetHitZ[imod];
    }

  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );
  if( vetoHitBuffer[0].energy>0){
    BCVVetoModID = vetoHitBuffer[0].modID;
    BCVVetoEne   = vetoHitBuffer[0].energy;
    BCVVetoTime  = vetoHitBuffer[0].time;
    BCVVetoHitZ  = vetoHitBuffer[0].position;
  } else {
    BCVVetoModID = -1;
    BCVVetoEne   = 0;
    BCVVetoTime  = -9999.;
    BCVVetoHitZ  = -9999.;
  }
  
  if( BCVVetoEne > m_vetoThreshold ) return true;

  return false;

}


double MTVetoBCV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoBCV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
