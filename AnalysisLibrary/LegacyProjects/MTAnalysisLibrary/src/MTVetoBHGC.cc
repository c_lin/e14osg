#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoBHGC.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoBHGC::MTVetoBHGC( Int_t userFlag ) : m_userFlag( userFlag )
{

  DetName = "BHGC"; 
  DetNumber = MTBP::BHGCNChannels;
  DetModID = new Int_t[DetNumber]();
  DetNHit = new Short_t[DetNumber]();
  DetEne   = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  DetTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();

  ModuleNumber = MTBP::BHGCNModules;
  ModuleModID = new Int_t[ModuleNumber]();
  ModuleNHit = new Short_t[ModuleNumber]();
  ModuleEne   = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  ModuleHitTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  ModuleDeltaTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  
  TOFCorrection = new Float_t[DetNumber];
  for( Int_t iMod=0 ; iMod<DetNumber ; iMod++ ){
    //TOFCorrection[iMod] = MTBP::BHGCDeltaZ[iMod]/(TMath::C()*1e-6);
  }
  
  int maxhit=MTBP::NMaxHitPerEvent;
  MaxBufferNum= DetNumber*maxhit;  
  vetoHitBuffer = new pVetoHit [MaxBufferNum]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  MakeWfmArray();

  Initialize();
}


MTVetoBHGC::~MTVetoBHGC()
{

  delete[] DetModID;
  delete[] DetNHit;
  delete[] DetEne;  
  delete[] DetTime;
  delete[] TOFCorrection;
  
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoBHGC::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "BHGCNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "BHGCModID", DetModID );
  m_clusterTree->SetBranchAddress( "BHGCnHits", DetNHit );
  m_clusterTree->SetBranchAddress( "BHGCTime", DetTime );
  m_clusterTree->SetBranchAddress( "BHGCEne", DetEne );
  MTBHVeto::SetBranchAddress(tr);
}


void MTVetoBHGC::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "BHGCNumber", &DetNumber, "BHGCNumber/I");
  m_recTree->Branch( "BHGCModID", DetModID, "BHGCModID[BHGCNumber]/I");
  m_recTree->Branch( "BHGCnHits", DetNHit, "BHGCnHits[BHGCNumber]/S");
  std::ostringstream leafstr[2];
  leafstr[0] << "BHGCEne[BHGCNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  leafstr[1] << "BHGCTime[BHGCNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  m_recTree->Branch( "BHGCEne", DetEne, leafstr[0].str().c_str() );
  m_recTree->Branch( "BHGCTime", DetTime, leafstr[1].str().c_str() );

  
  m_recTree->Branch( "BHGCVetoModID", &BHGCVetoModID,"BHGCVetoModID/I");
  m_recTree->Branch( "BHGCVetoEne", &BHGCVetoEne,"BHGCVetoEne/F");
  m_recTree->Branch( "BHGCVetoTime", &BHGCVetoTime,"BHGCVetoTime/F");
  
  MTBHVeto::Branch(tr);
}


void MTVetoBHGC::Initialize( void )
{

  for( int imod=0; imod<MaxBufferNum; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  BHGCVetoModID = -1;
  BHGCVetoEne = 0;
  BHGCVetoTime = MTBP::Invalid;
}


bool MTVetoBHGC::Process( int mode )
{
  
  Initialize();
  
  static const double s_BHGCZPosition = GetDetZPosition("BHGC");
  const Float_t BHGCZeroSuppThreshold = 0.5; //[p.e.]

  /* set number map */
  std::map<Int_t, Int_t> num_map;
  for (Int_t num=0; num < DetNumber; ++num) {
    num_map[DetModID[num]] = num;
  }
    
  /* calculate module information */
  ModuleNumber=0;
  for (Int_t mod_ID=0; mod_ID < MTBP::BHGCNModules; ++mod_ID) {
    
    const Int_t chA_ID = mod_ID*2;
    const Int_t chB_ID = mod_ID*2+1;
    
    std::map<Int_t,Int_t>::iterator end = num_map.end();
    
    const Int_t chA_num = (num_map.find(chA_ID)!=end ? num_map[chA_ID] : -1);
    const Int_t chB_num = (num_map.find(chB_ID)!=end ? num_map[chB_ID] : -1);
    
    if (chA_num==-1 || chB_num==-1) continue;
    //    if (chA_num==-1 && chB_num==-1) continue; //obsolete from 20190420

    ModuleModID[ModuleNumber] = mod_ID;
    
    double AddTimeOffset=0;
    Double_t TOF       = (s_BHGCZPosition - m_eventStartZ) / (TMath::C()*1e-6);
    Double_t base_time = TOF + m_eventStartTime - AddTimeOffset;

    const Double_t time_correction
      = base_time + ( (mod_ID<2) ? 0 : MTBP::BHGCModuleInterval/(TMath::C()*1e-6) );
    
    if (chA_num==-1 || chB_num==-1) { // hit only in one end (obsolete if-sentense from 20190420)

      const Int_t num = (chA_num==-1 ? chB_num : chA_num);
      ModuleNHit[ModuleNumber] = DetNHit[num];

      for (Int_t i_hit=0; i_hit < DetNHit[num]; ++i_hit) {
        ModuleHitTime[ModuleNumber][i_hit]   =   DetTime[num][i_hit];
	ModuleDeltaTime[ModuleNumber][i_hit] =   DetTime[num][i_hit] - time_correction;
        ModuleEne[ModuleNumber][i_hit]       =   DetEne[num][i_hit];
      }
      
    } else { // hit in both end                                                                                                                            
      Short_t &mod_n_hits = ModuleNHit[ModuleNumber];
      
      mod_n_hits = 0;
      for (Int_t i_hitA=0, n_hitsA=DetNHit[chA_num]; i_hitA < n_hitsA; ++i_hitA) {
        for (Int_t i_hitB=0, n_hitsB=DetNHit[chB_num]; i_hitB < n_hitsB; ++i_hitB) {
	  
	  //          if (TMath::Abs(DetTime[chA_num][i_hitA]-DetTime[chB_num][i_hitB])<10) {
          if (TMath::Abs(DetTime[chA_num][i_hitA]-DetTime[chB_num][i_hitB])<10 
	      && DetEne[chA_num][i_hitA]>BHGCZeroSuppThreshold && DetEne[chB_num][i_hitB]>BHGCZeroSuppThreshold ) {
            ModuleHitTime[ModuleNumber][mod_n_hits]
              = (DetTime[chA_num][i_hitA] + DetTime[chB_num][i_hitB]) * 0.5;
            ModuleDeltaTime[ModuleNumber][mod_n_hits]
              = (DetTime[chA_num][i_hitA] + DetTime[chB_num][i_hitB]) * 0.5 - time_correction;
            ModuleEne[ModuleNumber][mod_n_hits]
              = DetEne[chA_num][i_hitA] + DetEne[chB_num][i_hitB];
            ++mod_n_hits;

          }
        }
      }
      
    }

    ++ModuleNumber;
  }

  BHGCVetoModID = -1;
  BHGCVetoEne   = 0.;
  BHGCVetoTime  = -9999.;

  double VetoWindow[2]={-76-7.5,-76+7.5};
  
  for (Int_t num=0; num < ModuleNumber; ++num) {
    
    for (Int_t i_hit=0, n_hits=ModuleNHit[num]; i_hit < n_hits; ++i_hit) {
      
      const Double_t delta_time = ModuleDeltaTime[num][i_hit];
      const Double_t energy     = ModuleEne[num][i_hit];
      //if (VetoWindow[0]<=delta_time && delta_time<=VetoWindow[1] && energy>BHGCVetoEne) {
      if (IsInVetoWindow(delta_time) && energy>BHGCVetoEne) {
        BHGCVetoModID = ModuleModID[num];
        BHGCVetoEne   = energy;
        BHGCVetoTime  = delta_time;
      }
    }
  }

  if( BHGCVetoEne > m_vetoThreshold ) return true;
    
  return false;
  
}

double MTVetoBHGC::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();
  
  return DetZPosition;
}

std::vector<int> MTVetoBHGC::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
