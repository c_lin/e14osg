#include <fstream>
#include <unistd.h>
#include "MTAnalysisLibrary/MTPositionHandler.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTPositionHandler::MTPositionHandler()
{
  //CSIXPosition = new double [MTBP::CSINChannels] ();
  //CSIXPosition = new double [MTBP::CSINChannels] ();
  CSIXPosition = new double [2716] ();
  CSIYPosition = new double [2716] ();
  for( int ich=0; ich<MTBP::CSINChannels; ich++ ){
    CSIXPosition[ich] = 0.0;
    CSIYPosition[ich] = 0.0;
  }
  LoadCSIPosition();


  // CV
  CVXCenter = new double [92] ();
  CVYCenter = new double [92] ();
  CVXMin = new double [92] ();
  CVXMax = new double [92] ();
  CVYMin = new double [92] ();
  CVYMax = new double [92] ();
  CVFiberLength = new double [92] ();
  CVModID = new int [92] ();
  CVShortSideChannel = new int [92] ();
  CVLongSideChannel  = new int [92] ();
  CVQuadrant = new int [92] ();
  CVDirection = new int [92] ();
  LoadCVPosition();
  
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

}


MTPositionHandler::~MTPositionHandler()
{

  delete[] CSIXPosition;
  delete[] CSIYPosition;

  delete[] CVXCenter;
  delete[] CVYCenter;
  delete[] CVXMax;
  delete[] CVXMin;
  delete[] CVYMax;
  delete[] CVYMin;
  delete[] CVFiberLength;
  delete[] CVModID;
  delete[] CVShortSideChannel;
  delete[] CVLongSideChannel;
  delete[] CVQuadrant;
  delete[] CVDirection;

  delete   m_E14BP;
}



bool MTPositionHandler::LoadCSIPosition( void )
{

  std::string dirName
    = std::string( std::getenv(MTBP::EnvName.c_str()) )+"/AnalysisLibrary/LegacyProjects/MTAnalysisLibrary";
  std::string mapFile = dirName + "/data/CSIPosition.txt";

  // check file exists or not.
  if( access( mapFile.c_str(), R_OK) != 0 ){
    std::cout << mapFile.c_str() << " is not found." << std::endl;
    exit(1);
  }

  std::ifstream ifs( mapFile.c_str() );

  int dummy;
  for( int ich=0; ich<MTBP::CSINChannels; ich++ ){
    ifs >> dummy;
    ifs >> CSIXPosition[ich] >> CSIYPosition[ich];
    //std::cout << ich << " " << dummy << " " << CSIXPosition[ich] << " " << CSIYPosition[ich] << std::endl;
    if( dummy!=ich ) return false;
  }


  return true;

}


bool MTPositionHandler::LoadCVPosition( void )
{
  std::string dirName
    = std::string( std::getenv(MTBP::EnvName.c_str()) )+"/AnalysisLibrary/LegacyProjects/MTAnalysisLibrary";
  std::string mapFile = dirName + "/data/CV_map.csv";

  // check file exists or not.
  if( access( mapFile.c_str(), R_OK) != 0 ){
    std::cout << mapFile.c_str() << " is not found." << std::endl;
    exit(1);
  }

  std::ifstream ifs( mapFile.c_str() );

  std::string title;
  getline( ifs, title );

  double dummy;
  for( int imod=0; imod<92; imod++ ){
    ifs >> CVModID[imod] >> CVShortSideChannel[imod] >> CVLongSideChannel[imod];
    ifs >> CVXMin[imod] >> CVXMax[imod] >> CVYMin[imod] >> CVYMax[imod];
    ifs >> dummy >> dummy >> dummy >> dummy >> dummy;
    ifs >> CVFiberLength[imod] >> CVQuadrant[imod];

    CVXCenter[imod]   = ( CVXMin[imod] + CVXMax[imod] ) / 2.;
    CVYCenter[imod]   = ( CVYMin[imod] + CVYMax[imod] ) / 2.;
    CVDirection[imod] = (CVQuadrant[imod]+CVModID[imod]/100) % 2;

    //std::cout << __LINE__ << " " << imod << " " << CVModID[imod] << " " << CVXCenter[imod] << " " << CVYCenter[imod] << " " << CVDirection[imod] << std::endl;
      
  }



  return true;
}


double MTPositionHandler::GetCSIXPosition( int ich ) const
{
  if( ich>=MTBP::CSINChannels || ich<0 ){
    return -9999;
  }
  return CSIXPosition[ich];
}


double MTPositionHandler::GetCSIYPosition( int ich ) const
{
  if( ich>=MTBP::CSINChannels || ich<0 ){
    return -9999;
  }
  return CSIYPosition[ich];
}


double MTPositionHandler::GetCSIZPosition( int ich, int userFlag ) const
{
  if( ich>=MTBP::CSINChannels || ich<0 ){
    return -9999;
  }
  static const double s_CSIZPosition = GetDetZPosition( userFlag, "CSI");
  return s_CSIZPosition;
}

bool MTPositionHandler::GetCSIXYZPosition( int ich, double& x, double& y, double& z, int userFlag ) const
{
  if( ich>=MTBP::CSINChannels || ich<0 ) return false;

  static const double s_CSIZPosition = GetDetZPosition( userFlag, "CSI");

  x = CSIXPosition[ich];
  y = CSIYPosition[ich];
  z = s_CSIZPosition;
  return true;
}


int MTPositionHandler::GetCVModIndex( int imod ) const
{
  for( int index=0; index<92; index++ ){
    if( CVModID[index] == imod ){
      return index;
    }
  }
  return -1;
}
    

double MTPositionHandler::GetCVXCenter( int imod ) const
{
  return CVXCenter[ GetCVModIndex(imod) ];
}

double MTPositionHandler::GetCVYCenter( int imod ) const
{
  return CVYCenter[ GetCVModIndex(imod) ];
}

double MTPositionHandler::GetCVXMin( int imod ) const
{
  return CVXMin[ GetCVModIndex(imod) ];
}

double MTPositionHandler::GetCVYMin( int imod ) const
{
  return CVYMin[ GetCVModIndex(imod) ];
}

double MTPositionHandler::GetCVXMax( int imod ) const
{
  return CVXMax[ GetCVModIndex(imod) ];
}

double MTPositionHandler::GetCVYMax( int imod ) const
{
  return CVYMax[ GetCVModIndex(imod) ];
}

double MTPositionHandler::GetCVZCenter( int imod, int userFlag ) const
{
  static const double s_CVFZPosition = GetDetZPosition( userFlag, "CVF");
  static const double s_CVRZPosition = GetDetZPosition( userFlag, "CVR");

  if( imod < 100 ){
    return s_CVFZPosition + MTBP::CVFLength/2.;
  }else{
    return s_CVRZPosition + MTBP::CVRLength/2.;
  }
}

bool MTPositionHandler::GetCVXYZCenter( int imod, double& x, double& y, double& z, int userFlag ) const
{

  if( GetCVModIndex( imod ) == -1 ) return false;

  x = GetCVXCenter( imod );
  y = GetCVYCenter( imod );
  z = GetCVZCenter( imod, userFlag );
  return true;
}
  

int MTPositionHandler::GetCVQuadrant( int imod ) const
{
  return CVQuadrant[ GetCVModIndex(imod) ];
}


int MTPositionHandler::GetCVDirection( int imod ) const
{
  return CVDirection[ GetCVModIndex(imod) ];
}

double MTPositionHandler::GetDetZPosition( int userFlag, char* detName ) const
{
  m_E14BP->SetDetBasicParam( userFlag, detName);
  const double s_DetZPosition = m_E14BP->GetDetZPosition();

  return s_DetZPosition;
}
