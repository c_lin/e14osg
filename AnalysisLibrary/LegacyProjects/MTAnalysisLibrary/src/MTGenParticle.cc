#include "MTAnalysisLibrary/MTGenParticle.h"




MTGenParticle::MTGenParticle( void )
{
  GenParticleTrack     = new Int_t [MaxGenParticleN]();
  GenParticleMother    = new Int_t [MaxGenParticleN]();
  GenParticlePid       = new Int_t [MaxGenParticleN]();
  GenParticleMom       = new Double_t [MaxGenParticleN][3]();
  GenParticleEk        = new Double_t [MaxGenParticleN]();
  GenParticleMass      = new Double_t [MaxGenParticleN]();
  GenParticleTime      = new Double_t [MaxGenParticleN]();
  GenParticlePos       = new Double_t [MaxGenParticleN][3]();
  GenParticleEndMom    = new Double_t [MaxGenParticleN][3]();
  GenParticleEndEk     = new Double_t [MaxGenParticleN]();
  GenParticleEndTime   = new Double_t [MaxGenParticleN]();
  GenParticleEndPos    = new Double_t [MaxGenParticleN][3]();

  Initialize();
}


MTGenParticle::~MTGenParticle()
{
  delete[] GenParticleTrack;
  delete[] GenParticleMother;
  delete[] GenParticlePid;
  //delete[] GenParticleMotherPid;
  delete[] GenParticleMom;
  delete[] GenParticleEk;
  delete[] GenParticleMass;
  delete[] GenParticleTime;
  delete[] GenParticlePos;
  delete[] GenParticleEndMom;
  delete[] GenParticleEndEk;
  delete[] GenParticleEndTime;
  delete[] GenParticleEndPos;
}


void MTGenParticle::Initialize( void )
{
   GenParticleNumber = 0;
  for( int index=0; index<MaxGenParticleN; index++ ){
    GenParticleTrack[index] = -1;
    GenParticleMother[index] = -1;
    GenParticlePid[index] = -1;
    GenParticleEk[index] = -1;
    GenParticleMass[index] = -1;
    GenParticleTime[index] = -1;
    GenParticleEndEk[index] = -1;
    GenParticleEndTime[index] = -1;
    for( int xyz=0; xyz<3; xyz++ ){
      GenParticleMom[index][xyz] = -1;
      GenParticlePos[index][xyz] = -1;
      GenParticleEndMom[index][xyz] = -1;
      GenParticleEndPos[index][xyz] = -1;
    }
  }

}



void MTGenParticle::SetBranchAddress( TTree* tr )
{
  m_itr = tr;
  
  m_itr->SetBranchStatus( "GenParticle*" );
  m_itr->SetBranchAddress( "GenParticleNumber", &GenParticleNumber );
  m_itr->SetBranchAddress( "GenParticleTrack", GenParticleTrack );
  m_itr->SetBranchAddress( "GenParticleMother", GenParticleMother );
  m_itr->SetBranchAddress( "GenParticlePid", GenParticlePid );
  m_itr->SetBranchAddress( "GenParticleMom", GenParticleMom );
  m_itr->SetBranchAddress( "GenParticleEk", GenParticleEk );
  m_itr->SetBranchAddress( "GenParticleMass", GenParticleMass );
  m_itr->SetBranchAddress( "GenParticleTime", GenParticleTime );
  m_itr->SetBranchAddress( "GenParticlePos", GenParticlePos );
  m_itr->SetBranchAddress( "GenParticleEndMom", GenParticleEndMom );
  m_itr->SetBranchAddress( "GenParticleEndEk", GenParticleEndEk );
  m_itr->SetBranchAddress( "GenParticleEndTime", GenParticleEndTime );
  m_itr->SetBranchAddress( "GenParticleEndPos", GenParticleEndPos );
  
}


void MTGenParticle::Branch( TTree* tr )
{
  m_otr = tr;

  m_otr->Branch( "GenParticleNumber", &GenParticleNumber, "GenParticleNumber/I" );
  m_otr->Branch( "GenParticleTrack", GenParticleTrack, "GenParticleTrack[GenParticleNumber]/I" );
  m_otr->Branch( "GenParticleMother", GenParticleMother, "GenParticleMother[GenParticleNumber]/I" );
  m_otr->Branch( "GenParticlePid", GenParticlePid, "GenParticlePid[GenParticleNumber]/I" );
  m_otr->Branch( "GenParticleMom", GenParticleMom, "GenParticleMom[GenParticleNumber][3]/D" );
  m_otr->Branch( "GenParticleEk", GenParticleEk, "GenParticleEk[GenParticleNumber]/D" );
  m_otr->Branch( "GenParticleMass", GenParticleMass, "GenParticleMass[GenParticleNumber]/D" );
  m_otr->Branch( "GenParticleTime", GenParticleTime, "GenParticleTime[GenParticleNumber]/D" );
  m_otr->Branch( "GenParticlePos", GenParticlePos, "GenParticlePos[GenParticleNumber][3]/D" );
  m_otr->Branch( "GenParticleEndMom", GenParticleEndMom, "GenParticleEndMom[GenParticleNumber][3]/D" );
  m_otr->Branch( "GenParticleEndEk", GenParticleEndEk, "GenParticleEndEk[GenParticleNumber]/D" );
  m_otr->Branch( "GenParticleEndTime", GenParticleEndTime, "GenParticleEndTime[GenParticleNumber]/D" );
  m_otr->Branch( "GenParticleEndPos", GenParticleEndPos, "GenParticleEndPos[GenParticleNumber][3]/D" );
  
}
