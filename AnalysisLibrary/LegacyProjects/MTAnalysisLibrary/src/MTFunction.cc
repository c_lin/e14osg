#include <cmath>
#include "TMath.h"
#include "MTAnalysisLibrary/MTFunction.h"

Double_t MTFUNC::langau( Double_t *x, Double_t *par )
{

  /*
    p0 : width of Landau density
    p1 : Most probable value
    p2 : Integral of -inf to +inf
    p3 : sigma of Gaussian
   */

  Double_t mpshift = -0.22278298; // Landau maximum location
  Double_t np = 100.; // number of convolution steps
  Double_t sc = 5.0; // convolution extends to +/- sc Gaussian sigmas

  Double_t mpc = par[1] - mpshift * par[0];
  Double_t xlow = x[0] - sc * par[3];
  Double_t xupp = x[0] + sc * par[3];
  Double_t step = (xupp-xlow) / np;

  Double_t xx=0, fland=0, sum=0;

  // Convolution integral of Landau and Gaussian by sum
  for( int index=1; index<np/2; index++ ){
    xx = xlow + (index-0.5)*step;
    fland = TMath::Landau( xx, mpc, par[0]) / par[0];
    sum += fland * TMath::Gaus( x[0], xx, par[3]);

    xx = xupp - (index-0.5) * step;
    fland = TMath::Landau( xx, mpc, par[0]) / par[0];
    sum += fland * TMath::Gaus( x[0], xx, par[3]);
  }

  return par[2] * step * sum / sqrt(2*TMath::Pi()) / par[3];
}



Double_t MTFUNC::asymgau( Double_t *x, Double_t *par )
{
  /*
    p0 : Constant
    p1 : mu
    p2 : sigma0
    p3 : asymmetric ratio
    p4 : offset
   */

  double t=x[0];
  double t_fcn=0;
  // asynmetric gaussian : [0]*G([1],sigma)+[4], sigma=[3]*(t-[1])+[2]
  double sigma=par[3]*(t-par[1])+par[2];
  t_fcn=par[0]*TMath::Gaus(t,par[1],sigma)+par[4];
  return t_fcn;
}


Double_t MTFUNC::WeightedAverage( int n, Double_t *values, Double_t *weights )
{
  
  /*
    Caution!!
    Weight is not the resolution itself. Weight is 1/(sigma^2)
  */

  double weightedValue = 0.0;
  double totalWeight = 0.0;
  for( int index=0; index<n; index++ ){
    weightedValue += values[index] * weights[index];
    totalWeight   += weights[index];
  }
  return weightedValue / totalWeight;
}




Double_t MTFUNC::TResolutionOfCluster( double e )
{
  return sqrt( pow( 3.8/sqrt(e), 2) + pow( 0.19, 2)); // [ns]
}


Double_t MTFUNC::Distance( double x0, double x1, double y0, double y1, double z0, double z1 )
{
  double distance = pow( x0-x1, 2) + pow( y0-y1, 2) + pow( z0-z1, 2);
  return sqrt( distance );
}

