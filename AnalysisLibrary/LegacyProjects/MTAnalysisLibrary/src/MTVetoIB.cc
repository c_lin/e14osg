#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoIB.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoIB::MTVetoIB( Int_t userFlag ) : m_userFlag( userFlag )
{

  DetName = "IB"; 
  DetNumber = MTBP::IBNChannels;
  DetModID = new Int_t[DetNumber]();
  DetNHit = new Short_t[DetNumber]();
  DetEne   = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  DetTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();

  ModuleNumber = MTBP::IBNModules;
  ModuleModID = new Int_t [ModuleNumber]();
  ModuleTime  = new Float_t [ModuleNumber]();
  ModuleEne   = new Float_t [ModuleNumber]();
  ModuleHitZ  = new Float_t [ModuleNumber]();

  IBModuleDeltaTime = new float [ModuleNumber]();
  
  int maxhit=MTBP::NMaxHitPerEvent;
  MaxBufferNum= DetNumber*maxhit;  
  vetoHitBuffer = new pVetoHit [MaxBufferNum]();

  //// for IB wfm MC ////
  AccidentalDetECalibConst = new Float_t [DetNumber];
  AccidentalDetTCalibConst = new Float_t [DetNumber]; 

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  MakeWfmArray();
  Initialize();
}


MTVetoIB::~MTVetoIB()
{

  delete[] DetModID;
  delete[] DetNHit;
  delete[] DetEne;  
  delete[] DetTime;
  delete[] ModuleModID;
  delete[] ModuleTime;
  delete[] ModuleEne;
  delete[] ModuleHitZ;
  delete[] IBModuleDeltaTime;

  delete[] AccidentalDetECalibConst;
  delete[] AccidentalDetTCalibConst;
  
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoIB::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "IBNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "IBModID",   DetModID );
  m_clusterTree->SetBranchAddress( "IBnHits",   DetNHit );
  m_clusterTree->SetBranchAddress( "IBTime",    DetTime );
  m_clusterTree->SetBranchAddress( "IBEne",     DetEne );

  if( m_clusterTree->GetBranch("IBTCalibConst") ){
    m_clusterTree->SetBranchAddress( "IBECalibConst", AccidentalDetECalibConst );
    m_clusterTree->SetBranchAddress( "IBTCalibConst", AccidentalDetTCalibConst );
  }

  MTBHVeto::SetBranchAddress(tr);
}


void MTVetoIB::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "IBNumber", &DetNumber, "IBNumber/I");
  m_recTree->Branch( "IBModID",   DetModID,  "IBModID[IBNumber]/I");
  m_recTree->Branch( "IBnHits",   DetNHit,   "IBnHits[IBNumber]/S");
  std::ostringstream leafstr[2];
  leafstr[0] << "IBEne[IBNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  leafstr[1] << "IBTime[IBNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  m_recTree->Branch( "IBEne",  DetEne,  leafstr[0].str().c_str() );
  m_recTree->Branch( "IBTime", DetTime, leafstr[1].str().c_str() );
  
  m_recTree->Branch( "IBModuleNumber",   &ModuleNumber,      "IBModuleNumber/I" );
  m_recTree->Branch( "IBModuleModID",     ModuleModID,       "IBModuleModID[IBModuleNumber]/I" );
  m_recTree->Branch( "IBModuleHitTime",   ModuleTime,        "IBModuleHitTime[IBModuleNumber]/F" );
  m_recTree->Branch( "IBModuleEne",       ModuleEne,         "IBModuleEne[IBModuleNumber]/F" );
  m_recTree->Branch( "IBModuleHitZ",      ModuleHitZ,        "IBModuleHitZ[IBModuleNumber]/F" );
  m_recTree->Branch( "IBModuleDeltaTime", IBModuleDeltaTime, "IBModuleDeltaTime[IBModuleNumber]/F" );
  
  m_recTree->Branch( "IBVetoModID", &IBVetoModID,"IBVetoModID/I");
  m_recTree->Branch( "IBVetoEne",   &IBVetoEne,  "IBVetoEne/F");
  m_recTree->Branch( "IBVetoTime",  &IBVetoTime, "IBVetoTime/F");

  m_recTree->Branch( "IBWideVetoModID", &IBWideVetoModID,"IBWideVetoModID/I");
  m_recTree->Branch( "IBWideVetoEne",   &IBWideVetoEne,  "IBWideVetoEne/F");
  m_recTree->Branch( "IBWideVetoTime",  &IBWideVetoTime, "IBWideVetoTime/F");

  m_recTree->Branch( "IBCH55VetoEne",   &IBCH55VetoEne,  "IBCH55VetoEne/F");
  m_recTree->Branch( "IBCH55VetoTime",  &IBCH55VetoTime, "IBCH55VetoTime/F");
    
  MTBHVeto::Branch(tr);
}
  
void MTVetoIB::Initialize( void )
{

  for( int ihit=0; ihit<MaxBufferNum; ihit++ ){
    vetoHitBuffer[ihit].modID = 0;
    vetoHitBuffer[ihit].energy = 0;
    vetoHitBuffer[ihit].time = 0;
  }

  for( int imod=0; imod<MTBP::IBNModules; imod++){
    ModuleModID[imod]=0;
    ModuleTime[imod]=0;
    ModuleHitZ[imod]=0;
    IBModuleDeltaTime[imod]=0;
  }

  IBVetoModID = -1;
  IBVetoEne = 0;
  IBVetoTime = MTBP::Invalid;

  IBWideVetoModID = -1;
  IBWideVetoEne = 0;
  IBWideVetoTime = MTBP::Invalid;

  IBCH55VetoEne  = 0;
  IBCH55VetoTime = MTBP::Invalid;

}

bool MTVetoIB::Process( int mode )
{

  Initialize();

  static const double s_IBZPosition = GetDetZPosition("IB");
  const Float_t IBCH55TimeWindow[2] = {-200, 300};

  float IBEneBest[64]  = {0};
  float IBTimeBest[64] = {0}; // ns
  int IBnHitsOnTime[64] = {0};
  
  for(int ich=0;ich<DetNumber;ich++){
    for(int ihit=0;ihit<DetNHit[ich];ihit++){      
      int   IndexID = ( DetModID[ich]<100 ) ? DetModID[ich] : DetModID[ich] - 100 + 32; // translate from e14ID to index
      float Time    = DetTime[ich][ihit] - m_eventStartTime;
      if( TMath::Abs(Time - MTBP::VetoTiming[MTBP::IB])< MTBP::VetoWidth[MTBP::IB]/2.+5 ){ //select from nhits
	if( IBEneBest[IndexID] < DetEne[ich][ihit] ){
	  IBEneBest[IndexID] = DetEne[ich][ihit];
	  IBTimeBest[IndexID] = DetTime[ich][ihit];
	  IBnHitsOnTime[IndexID]++;
	} 
      }
    }
  }

  ModuleNumber=0;  
  for(int imod=0;imod<MTBP::IBNModules;imod++){
    if( IBEneBest[imod]>0 && IBEneBest[imod+32]>0 ){
      ModuleTime[ModuleNumber] = GetHitTime(IBTimeBest[imod],IBTimeBest[imod+32]);
      ModuleHitZ[ModuleNumber] = GetHitZ(IBTimeBest[imod],IBTimeBest[imod+32]);
      ModuleEne[ModuleNumber] =  GetEne( IBEneBest[imod], IBEneBest[imod+32],
					 IBTimeBest[imod], IBTimeBest[imod+32] );
      ModuleModID[ModuleNumber] = imod;
      ModuleNumber+=1;
    }
  }

  int modnum=0;  
  for( int imod=0; imod<ModuleNumber; imod++ ){

    // veto time calculation
    IBModuleDeltaTime[imod] = ModuleTime[imod] - m_eventStartTime;
    float deltaZ = m_eventStartZ - ( s_IBZPosition + MTBP::IBLength/2. );
    if( ModuleHitZ[imod] < -1500 ){
      deltaZ += 1500;
    }else if( ModuleHitZ[imod] > 1500 ){
      deltaZ -= 1500;
    }else{
      deltaZ -= ModuleHitZ[imod];
    }
    IBModuleDeltaTime[imod] -= sqrt( pow( deltaZ, 2) + pow( MTBP::IBInnerInnerR, 2) ) / (TMath::C()/1E6);
    modnum+=1;
    
    if( IsInVetoWindow( IBModuleDeltaTime[imod] )){
      vetoHitBuffer[imod].modID = ModuleModID[imod];
      vetoHitBuffer[imod].energy = ModuleEne[imod];
      vetoHitBuffer[imod].time = IBModuleDeltaTime[imod];
      vetoHitBuffer[imod].position = ModuleHitZ[imod];
    }else{
      vetoHitBuffer[imod].modID = ModuleModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = IBModuleDeltaTime[imod];
      vetoHitBuffer[imod].position = ModuleHitZ[imod];
    }
  }

  std::sort( &(vetoHitBuffer[0]), &(vetoHitBuffer[modnum]), pVetoHit::largerHit );
  if( vetoHitBuffer[0].energy > 0 && modnum>0 ){
    IBVetoModID = vetoHitBuffer[0].modID;
    IBVetoEne   = vetoHitBuffer[0].energy;
    IBVetoTime  = vetoHitBuffer[0].time;
    IBVetoHitZ  = vetoHitBuffer[0].position;    
  }else{
    IBVetoModID = -1;
    IBVetoEne   = 0;
    IBVetoTime  = -9999.;
    IBVetoHitZ  = -9999.;
  }

  // for IB delayed hit calculation based on ModuleHitTime
  Float_t DelayedHitWindow[2]={ 0, 100 };
  Int_t   tmpIBWideVetoModID=-1;
  Float_t tmpIBWideVetoEne=-9999;
  Float_t tmpIBWideVetoTime=-9999;
  
  for(int imod=0;imod<32;imod++){
    for(int ihit_U=0;ihit_U<DetNHit[imod];ihit_U++){
      Float_t Time_U = DetTime[imod][ihit_U]-m_eventStartTime;
      
      for(int ihit_D=0;ihit_D<DetNHit[imod+32];ihit_D++){
	Float_t Time_D = DetTime[imod+32][ihit_D]-m_eventStartTime;
	if( TMath::Abs(Time_U-Time_D)<20 ){ // treat as one hit within 20ns
	  if( DelayedHitWindow[0]<GetHitTime(Time_U,Time_D) && GetHitTime(Time_U,Time_D)<DelayedHitWindow[1]
	      && GetEne(DetEne[imod][ihit_U],DetEne[imod+32][ihit_D],Time_U,Time_D)>0.5){ // select module hit time within DelayedHitWindow
	    if(GetEne(DetEne[imod][ihit_U],DetEne[imod+32][ihit_D],Time_U,Time_D)>tmpIBWideVetoEne){ // select largest energy
	      tmpIBWideVetoModID= imod;
	      tmpIBWideVetoEne  = GetEne(DetEne[imod][ihit_U],DetEne[imod+32][ihit_D],Time_U,Time_D);
	      tmpIBWideVetoTime = GetHitTime(Time_U,Time_D);
	    }
	  }
	}
      } // end of downstream ch search
    } // end of upstream ch search
  } // end of module search

  IBWideVetoModID=tmpIBWideVetoModID;
  IBWideVetoEne=tmpIBWideVetoEne;
  IBWideVetoTime=tmpIBWideVetoTime;
  if( IBWideVetoModID==-1 ){
    IBWideVetoModID = -1;
    IBWideVetoEne = 0;
    IBWideVetoTime = MTBP::Invalid;
  }

  //// treatment of IB dead channel ////
  IBCH55VetoEne=0;
  IBCH55VetoTime=-9999;
  if( (20170401<=m_userFlag && m_userFlag<20180701) || (m_userFlag>=20210201) ){
    for( int ich=0;ich<DetNumber;ich++){
      Int_t imod = DetModID[ich];
      if( imod==123 ){ //e14ID:123 (=100+23), chID: 55 (=32+23)
	for( int ihit=0;ihit<DetNHit[ich];ihit++){
	  Float_t TimeIB55 = DetTime[ich][ihit] - m_eventStartTime;
	  if( IBCH55TimeWindow[0]<TimeIB55 && TimeIB55<IBCH55TimeWindow[1] && IBCH55VetoEne<DetEne[ich][ihit] ){
	    IBCH55VetoEne  = DetEne[ich][ihit];
	    IBCH55VetoTime = TimeIB55;
	  }
	}
      }
    }
  }

  if( IBVetoEne > m_vetoThreshold ){
    return true;
  }

  return false;
}


float MTVetoIB::GetHitTime( float frontTime, float rearTime )
{
  return ( frontTime+rearTime ) / 2.;
}

float MTVetoIB::GetHitZ( float frontTime, float rearTime )
{
  return ( frontTime - rearTime ) / 2. * MTBP::IBPropVelo;
}

float MTVetoIB::GetEne( float frontEne, float rearEne, float frontTime, float rearTime)
{
  float hitZ = GetHitZ( frontTime, rearTime );

  if( hitZ > MTBP::IBLength / 2. ){   // downstream overflow
    return rearEne;
  }
  if( hitZ < - MTBP::IBLength / 2. ){ // upstream overflow 
    return frontEne;
  }

  float e = frontEne / exp( -hitZ / ( MTBP::IBLAMBDA_U + MTBP::IBALPHA_U * hitZ ) );
  e      += rearEne  / exp( hitZ  / ( MTBP::IBLAMBDA_D - MTBP::IBALPHA_D * hitZ ) );

  return e;

}

double MTVetoIB::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoIB::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
