#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoBPCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoBPCV::MTVetoBPCV( Int_t userFlag ) : m_userFlag( userFlag )
{
  std::string DetName = "BPCV"; 
  DetNumber = MTBP::BPCVNModules;

  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();

  vetoHitBuffer = new pVetoHit [DetNumber]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  MakeWfmArray();

  Initialize();
}


MTVetoBPCV::~MTVetoBPCV()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoBPCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "BPCVModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "BPCVModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "BPCVModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "BPCVModuleEne", DetEne );
  MTVeto::SetBranchAddress(tr);
}


void MTVetoBPCV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "BPCVModuleNumber", &DetNumber, "BPCVModuleNumber/I");
  m_recTree->Branch( "BPCVModuleModID", DetModID, "BPCVModuleModID[BPCVModuleNumber]/I");
  m_recTree->Branch( "BPCVModuleEne", DetEne, "BPCVModuleEne[BPCVModuleNumber]/F");
  m_recTree->Branch( "BPCVModuleTime", DetTime, "BPCVModuleTime[BPCVModuleNumber]/F");

  m_recTree->Branch( "BPCVVetoModID", &BPCVVetoModID,"BPCVVetoModID/I");
  m_recTree->Branch( "BPCVVetoEne", &BPCVVetoEne,"BPCVVetoEne/F");
  m_recTree->Branch( "BPCVVetoTime", &BPCVVetoTime,"BPCVVetoTime/F");
  
  MTVeto::Branch(tr);
}


void MTVetoBPCV::Initialize( void )
{

  for( int imod=0; imod<MTBP::BPCVNModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  BPCVVetoModID = -1;
  BPCVVetoEne = 0;
  BPCVVetoTime = 9999.;

}


bool MTVetoBPCV::Process( int mode ){

  Initialize();

  static const double s_BPCVZPosition = GetDetZPosition("BPCV");

  // Front
  for( int imod=0; imod<DetNumber; imod++ ){
    //vetoTime = DetTime[imod] - m_eventStartTime;
    vetoTime = DetTime[imod] - m_eventStartTime-(s_BPCVZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }
    //std::cout<<imod<<" "<<DetModID[imod]<<" "<<DetEne[imod]<<" "<<vetoTime<<std::endl;
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );

  if(vetoHitBuffer[0].energy>0){  
    BPCVVetoModID = vetoHitBuffer[0].modID;
    BPCVVetoEne   = vetoHitBuffer[0].energy;
    BPCVVetoTime  = vetoHitBuffer[0].time;
  } else {
    BPCVVetoModID = -1;
    BPCVVetoEne   = 0;
    BPCVVetoTime  = -9999.;
  }

  //std::cout<<"BPCV :"<<vetoHitBuffer[0].modID<<" "<<vetoHitBuffer[0].energy<<" "<<vetoHitBuffer[0].time<<std::endl;
  //std::getchar();
  
  if( BPCVVetoEne > m_vetoThreshold ) return true;

  return false;

}


double MTVetoBPCV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoBPCV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
