#include <cmath>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include "TString.h"
#include "MTAnalysisLibrary/MTTimeShiftCorrection.h"

MTTimeShiftCorrection::MTTimeShiftCorrection(int RunID) 
{ 
  TimeShift = new double [2716] ();
  for(int i=0; i<2716; i++) TimeShift[i]=0;
  
  int num=0;
  if(RunID<16532) num=0;  
  else if(16532<=RunID&&RunID<16635) num=1;
  else if(16635<=RunID&&RunID<16728) num=2;
  else if(16728<=RunID) num=2;
  //else if(16728<=RunID) num=3;

  std::string dirName
    = std::string( std::getenv(MTBP::EnvName.c_str()) )+"/AnalysisLibrary/LegacyProjects/MTAnalysisLibrary";
  std::ifstream ifs[3];
  ifs[0].open(Form("%s/data/obsolete/timeshift_run49/timeshift_c3m12.dat",dirName.c_str()));
  ifs[1].open(Form("%s/data/obsolete/timeshift_run49/timeshift_c6m4.dat", dirName.c_str()));
  ifs[2].open(Form("%s/data/obsolete/timeshift_run49/timeshift_c1m11.dat",dirName.c_str()));

  int csiid;
  double shift_time,error;
  for(int i=0; i<num; i++){
    while(ifs[i]>>csiid>>shift_time>>error){
      TimeShift[csiid]=shift_time;
    }
  }

  //for(int i=0; i<2716; i++){
  //if(TimeShift[i]!=0) std::cout<<i<<" "<<TimeShift[i]<<std::endl;
  //}
  
  for(int i=0; i<3; i++) ifs[i].close();
  
}


MTTimeShiftCorrection::~MTTimeShiftCorrection()
{
  delete TimeShift;
}


double MTTimeShiftCorrection::GetCorrection( int csiID ) const
{
  return TimeShift[csiID];
}

double MTTimeShiftCorrection::Correction( int csiID, double time )
{
  return time - GetCorrection(csiID);
}




