#include <cmath>
#include <unistd.h>
#include "TString.h"
#include "MTAnalysisLibrary/MTGainCorrection.h"
MTGainCorrection::MTGainCorrection() 
{ 
  tr = new TChain("Tree");
  //tr->Add("/home/had/shiomi/work2/beam2013_05/LaserCSI/LaserInfo.root");   // run49
  //tr->Add("/home/had/shiomi/work2/beam2015/LaserCSI/LaserInfo.root");      // run62
  //tr->Add("/home/had/shiomi/work2/beam2015_10/LaserCSI/LaserInfo.root");   // run64
  tr->Add("/home/had/shiomi/work2/beam2015_11/LaserCSISum/LaserInfo.root");  // run65
  
  BaseGain = new double [2716] ();
  Gain     = new double [2716] ();
  SetBranchAddress();
  tr->GetEntry(0);
  for(int i=0; i<2716; i++) BaseGain[i]=Gain[i];
  
  int nevent = tr->GetEntries();
  for(int i = 0 ; i < nevent ; ++i ){
    tr->GetEntry(i);
    runEntryMap.insert( std::map< unsigned int, unsigned int>::value_type( RunID, i ) );
  }
  int Nchannel = MTBP::CSINGainCorrectChannel;
  for(int i = 0; i < 2716;      ++i ) CorrectFlag[i] = 0;
  for(int i = 0; i < Nchannel ; ++i ) CorrectFlag[MTBP::CSIGainCorrectChannelID[i]] = 1;
}

MTGainCorrection::~MTGainCorrection()
{
  delete tr;
  delete Gain;
  delete BaseGain;
}

void MTGainCorrection::SetBranchAddress( void )
{
  tr->SetBranchAddress( "RunID",     &RunID );
  tr->SetBranchAddress( "GainFactor", Gain );
}

bool MTGainCorrection::IsExist( int run ) const{ return ( runEntryMap.count(run)==1 );}

bool MTGainCorrection::SetRunID( int run )
{
  if( IsExist( run ) ){
    tr->GetEntry( runEntryMap[run] );
    return true;
  }
  return false;
}

double MTGainCorrection::GetCorrectionFactor( int csiID ) const
{
  if( CorrectFlag[csiID]==1 && Gain[csiID]>0 )
    return BaseGain[csiID]/Gain[csiID];
  else 
    return 1;
}

double MTGainCorrection::Correction( int csiID, double energy ){ return energy*GetCorrectionFactor(csiID);}
