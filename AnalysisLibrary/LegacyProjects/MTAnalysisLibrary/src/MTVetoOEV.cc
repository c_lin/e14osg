#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoOEV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoOEV::MTVetoOEV( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "OEV"; 
  DetNumber = MTBP::OEVNChannels;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();

  DetWfmCorrupted = NULL;
  vetoHitBuffer = new pVetoHit [DetNumber]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  MakeWfmArray();
  Initialize();
}


MTVetoOEV::~MTVetoOEV()
{
  delete[] DetPSBit;
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetSmoothness;
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoOEV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "OEVModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "OEVModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "OEVModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "OEVModuleEne", DetEne );
  if(m_clusterTree->GetBranch("OEVModulePSBit"))
	  m_clusterTree->SetBranchAddress( "OEVModulePSBit", DetPSBit );
  if(m_clusterTree->GetBranch("OEVModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "OEVModuleSmoothness", DetSmoothness );  
  }
  MTVeto::SetBranchAddress(tr);
}


void MTVetoOEV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "OEVModuleNumber", &DetNumber, "OEVModuleNumber/I");
  m_recTree->Branch( "OEVModuleModID", DetModID, "OEVModuleModID[OEVModuleNumber]/I");
  m_recTree->Branch( "OEVModuleEne", DetEne, "OEVModuleEne[OEVModuleNumber]/F");
  m_recTree->Branch( "OEVModuleTime", DetTime, "OEVModuleTime[OEVModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "OEVModuleSmoothness", DetSmoothness, "OEVModuleSmoothness[OEVModuleNumber]/F" );
  if(m_clusterTree->GetBranch("OEVModulePSBit"))
	  m_recTree->Branch( "OEVModulePSBit", DetPSBit, "OEVModulePSBit[OEVModuleNumber]/S");

  m_recTree->Branch( "OEVVetoModID", &OEVVetoModID,"OEVVetoModID/I");
  m_recTree->Branch( "OEVVetoEne", &OEVVetoEne,"OEVVetoEne/F");
  m_recTree->Branch( "OEVVetoTime", &OEVVetoTime,"OEVVetoTime/F");
  
  MTVeto::Branch(tr);
}


void MTVetoOEV::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
  	if(!((*DetWfmCorrupted)&(1<<MTBP::OEV))){
  	    for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
  	}
  }
  for( int imod=0; imod<MTBP::OEVNChannels; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  OEVVetoModID = -1;
  OEVVetoEne = 0;
  OEVVetoTime = 9999.;

}


bool MTVetoOEV::Process( int mode ){

  Initialize();

  static const double s_OEVZPosition = GetDetZPosition("OEV");

  // Front
  for( int imod=0; imod<DetNumber; imod++ ){
    //vetoTime = DetTime[imod] - m_eventStartTime;
    vetoTime = DetTime[imod] - m_eventStartTime-(s_OEVZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }
    //std::cout<<imod<<" "<<DetModID[imod]<<" "<<DetEne[imod]<<" "<<vetoTime<<std::endl;
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );

  if(vetoHitBuffer[0].energy>0){
    OEVVetoModID = vetoHitBuffer[0].modID;
    OEVVetoEne   = vetoHitBuffer[0].energy;
    OEVVetoTime  = vetoHitBuffer[0].time;
  } else{
    OEVVetoModID = -1;
    OEVVetoEne   = 0;
    OEVVetoTime  = -9999.;
  }

  //std::cout<<"OEV :"<<vetoHitBuffer[0].modID<<" "<<vetoHitBuffer[0].energy<<" "<<vetoHitBuffer[0].time<<std::endl;
  //std::getchar();
  
  if( OEVVetoEne > m_vetoThreshold ) return true;

  return false;

}

double MTVetoOEV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoOEV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
