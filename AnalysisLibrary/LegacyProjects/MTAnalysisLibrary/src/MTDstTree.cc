#include <iostream>
#include <unistd.h>
#include "MTAnalysisLibrary/MTDstTree.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


int MTDstTree::ModID2Index(int detID, int ModID){

  if(detID==MTBP::CC03   ||
     detID==MTBP::FBAR   ||
     detID==MTBP::CSI    ||
     detID==MTBP::BHPV   ||
     detID==MTBP::BHGC   ||
     detID==MTBP::DCV    ||
     detID==MTBP::LCV    ||
     detID==MTBP::BPCV   ||
     detID==MTBP::MBCV   ||
     detID==MTBP::newBHCV||
     detID==MTBP::COSMIC ||
     detID==MTBP::LASER  ||
     detID==MTBP::TRIGGERTAG ||
     detID==MTBP::ETC    ||
     detID==MTBP::OEV) return ModID;


  if(detID==MTBP::CV){
    if(ModID<100)return ModID;
    else return ModID-100+96;
  }

  if(detID==MTBP::CC04){
    if(ModID<58) return ModID;
    else return ModID-60+58;
  }
  if(detID==MTBP::CC05 || detID==MTBP::CC06){
    if(ModID<54) return ModID;
    else return ModID-60+54;
  }
  if(detID==MTBP::IB   ||
     detID==MTBP::IBCV){
    if(ModID<100) return ModID;
    else return ModID-100+32;
  }
	
  if(detID==MTBP::CBAR){
    if(ModID<100) return ModID;
    else return ModID-100+64;
  }
	

  if(detID==MTBP::NCC){
    if(ModID<500){
      int comid = ModID/10;
      int indid = ModID%10;
      return comid*4+indid;
    }else if(ModID<600){
      return (ModID-500)/10 + 192;
    }else{
      return (ModID-600)/10 + 200;
    }
  }

  return ModID;
}

MTDstTree::MTDstTree( TTree* tr ,bool runMCFlag, Int_t userFlag)
{
  Allocation();
  Initialize(userFlag);

  RunMCFlag= runMCFlag;
  
  tfAllocationFlag = false;
  T = tr;

}


MTDstTree::MTDstTree( const char* filename , bool runMCFlag, Int_t userFlag)
{
  Allocation();
  Initialize(userFlag);
  RunMCFlag = runMCFlag;
 
  if( access( filename, R_OK ) != 0 ){
    std::cout << filename << " does not exist." << std::endl;
  }else{
    TFile* tf = new TFile( filename );
    T = (TTree*)tf->Get( "T" );
    tfAllocationFlag = true;
  }
}


int MTDstTree::Allocation( void )
{
 
  CSIWfm_ProdDer2threshold = -300;
  CSIWfm_Smoothnessthreshold = 80;
  
  UTCWfm_ProdDer2threshold = -150;
  UTCWfm_Smoothnessthreshold = 50;

  DetNumber        = new Int_t   [MTBP::nDetectors];
  DetModID         = new Int_t   [MTBP::nDetectors][nMaxChannels];
  DetPeak          = new Short_t [MTBP::nDetectors][nMaxChannels];
  DetPedestal      = new Float_t [MTBP::nDetectors][nMaxChannels];
  DetIntegratedADC = new Float_t [MTBP::nDetectors][nMaxChannels];
  DetEne           = new Float_t [MTBP::nDetectors][nMaxChannels];
  DetInitialTime   = new Float_t [MTBP::nDetectors][nMaxChannels];
  DetTime          = new Float_t [MTBP::nDetectors][nMaxChannels];
  DetEtSum         = new Long64_t[MTBP::nDetectors][64];
  DetPSbit         = new Short_t[MTBP::nDetectors][nMaxChannels];
  DetSmoothness    = new Float_t [MTBP::nDetectors][nMaxChannels];
  DetProdDer2      = new Float_t [MTBP::nDetectors][nMaxChannels];

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  return 0;
}


MTDstTree::~MTDstTree()
{
  delete[] DetNumber;
  delete[] DetModID;
  delete[] DetPeak;
  delete[] DetPedestal;
  delete[] DetIntegratedADC;
  delete[] DetEne;
  delete[] DetInitialTime;
  delete[] DetTime;
  delete[] DetEtSum;
  delete[] DetPSbit;
  delete[] DetProdDer2;
  delete[] DetSmoothness;


  if( tfAllocationFlag ){
    delete tf;
  }

  delete  m_E14BP;

}


void MTDstTree::Initialize( Int_t userFlag )
{
  EventNo = 0;
  L1TrigNo = 0;
  L2TrigNo = 0;
  SpillNo = 0;
  SpillNo2019 = 0;
  TimeStamp = 0;
  L2TimeStamp = 0;
  Error = 0;
  TrigTagMismatch = 0;
  L2AR =0;
  COE_Et_overflow=0;
  COE_L2_override=0;
  COE_Esum = 0;
  COE_Ex = 0;
  COE_Ey = 0;
  DetectorBit = 0;
  RawTrigBit = 0;
  ScaledTrigBit = 0;
  LeftEt = 0;
  LeftEt_raw = 0;
  RightEt = 0;
  RightEt_raw = 0;

  userflag = userFlag;
  
  for( int idet=0; idet<MTBP::nDetectors; idet++ ){
    DetNumber[idet] = 0;
    for( int ich=0; ich<nMaxChannels; ich++ ){
      DetModID[idet][ich] = 0;
      DetPeak[idet][ich] = 0;
      DetPedestal[idet][ich] = 0;
      DetIntegratedADC[idet][ich] = 0;
      DetEne[idet][ich] = 0;
      DetInitialTime[idet][ich] = 0;
      DetTime[idet][ich] = 0;
      DetPSbit[idet][ich] = 0;
      DetProdDer2[idet][ich] = 0;
      DetSmoothness[idet][ich] = 0;


    }
    for( int sample=0; sample<64; sample++ ){
      DetEtSum[idet][sample] = 0;
    }
  }

  // Dead channel
  static const std::vector<int> s_CSIDeadChannelIDVec = GetDeadChannelIDVec(userFlag,"CSI");
  for( UInt_t ich=0; ich<s_CSIDeadChannelIDVec.size(); ich++ ){
    CSIDeadChannelSet.insert( s_CSIDeadChannelIDVec.at(ich) );
  }

  // CDT clustering trigger
  CDTNum=0;
  for(int iadc=0;iadc<3;iadc++){
    for(int isam=0;isam<64;isam++) CDTData[iadc][isam]=0;
  }
  for(int ibitX=0;ibitX<38;ibitX++){
    for(int ibitY=0;ibitY<38;ibitY++) CDTBit[ibitX][ibitY]=0;
  }

  // CDT Veto
  for(int iadc=0;iadc<4;iadc++){
    for(int isam=0;isam<64;isam++) CDTVetoData[iadc][isam]=0;
  }
  for(int idet=0;idet<13;idet++) CDTVetoBitSum[idet]=0;

}


Int_t MTDstTree::CheckCSIWfmCorruption(){
  for( int imod=0; imod< DetNumber[MTBP::CSI] ; imod ++ ){
    if(DetModID[MTBP::CSI][imod]<2716){
      if(fabs(DetSmoothness[MTBP::CSI][imod])>CSIWfm_Smoothnessthreshold)	CSIWfm_corrupted++;
      if(DetProdDer2[MTBP::CSI][imod]<CSIWfm_ProdDer2threshold)           CSIWfm_corrupted += 10000;
    }else{
      if(fabs(DetSmoothness[MTBP::CSI][imod])>UTCWfm_Smoothnessthreshold) UTCWfm_corrupted++;
      if(DetProdDer2[MTBP::CSI][imod]<UTCWfm_ProdDer2threshold)           UTCWfm_corrupted += 10000;
    }
  }
  return 0;
}

Int_t MTDstTree::GetEntry( Long64_t entry, int apply_PS)
{
  int ret = T->GetEntry( entry );

  UTCWfm_corrupted = 0;
  CSIWfm_corrupted = 0;

  if(apply_PS){
    PedestalSuppression(apply_PS);
  }else{
    CheckCSIWfmCorruption();
  }

  for(int i=0;i<nMaxChannels;i++){
    CSISmoothness_asModID[i] = 0;
    CSIProdDer2_asModID[i] = 0;
  }
 
  for(int i=0;i<DetNumber[MTBP::CSI];i++){
    int index = ModID2Index(MTBP::CSI, DetModID[MTBP::CSI][i]);
    CSISmoothness_asModID[index] = DetSmoothness[MTBP::CSI][i];
    CSIProdDer2_asModID[index]   = DetProdDer2[MTBP::CSI][i];
  }

  return ret;
}

void MTDstTree::PedestalSuppression(int apply_PS){


  // if apply PS == 2, suppress only for CSI 
  // else apply all detectors
  for( int idet=0; idet<MTBP::nDetectors; idet++ ){
 
    if(apply_PS==2 && idet!=MTBP::CSI) continue;

    Int_t    bufDetNumber;
    Int_t    bufDetModID[nMaxChannels];
    Short_t  bufDetPeak[nMaxChannels];
    Float_t  bufDetPedestal[nMaxChannels];
    Float_t  bufDetIntegratedADC[nMaxChannels];
    Float_t  bufDetEne[nMaxChannels];
    Float_t  bufDetInitialTime[nMaxChannels];
    Float_t  bufDetTime[nMaxChannels];
    Float_t  bufDetSmoothness[nMaxChannels];
    Float_t  bufDetProdDer2[nMaxChannels];
    Short_t  bufDetPSbit[nMaxChannels];
		
    // once copy
    for( int ich=0; ich<nMaxChannels; ich++ ){
    	  
      bufDetModID[ich] = DetModID[idet][ich];
      bufDetPeak[ich]= DetPeak[idet][ich];
      bufDetPedestal[ich]= DetPedestal[idet][ich];
      bufDetIntegratedADC[ich]= DetIntegratedADC[idet][ich];
      bufDetEne[ich]= DetEne[idet][ich];
      bufDetInitialTime[ich]= DetInitialTime[idet][ich];
      bufDetTime[ich]= DetTime[idet][ich];
      bufDetProdDer2[ich]= DetProdDer2[idet][ich];
      bufDetSmoothness[ich]= DetSmoothness[idet][ich];
      bufDetPSbit[ich]= DetPSbit[idet][ich];
		
      DetModID[idet][ich] = 0;
      DetPeak[idet][ich] = 0;
      DetPedestal[idet][ich] = 0;
      DetIntegratedADC[idet][ich] = 0;
      DetEne[idet][ich] = 0;
      DetInitialTime[idet][ich] = 0;
      DetTime[idet][ich] = 0;
      DetProdDer2[idet][ich] = 0;
      DetSmoothness[idet][ich] = 0;
    }

    // refill
    bufDetNumber = DetNumber[idet];
    DetNumber[idet] = 0;
    for( int ich=0; ich<bufDetNumber; ich++ ){
      int modid = bufDetModID[ich];
      int index = ModID2Index(idet, modid);
      if(bufDetPSbit[index]==0) continue;

      int c = DetNumber[idet];
      DetModID[idet][c]         = bufDetModID[ich]        ;
      DetPeak[idet][c]          = bufDetPeak[ich]         ;
      DetPedestal[idet][c]      = bufDetPedestal[ich]     ;
      DetIntegratedADC[idet][c] = bufDetIntegratedADC[ich];
      DetEne[idet][c]           = bufDetEne[ich]          ;
      DetInitialTime[idet][c]   = bufDetInitialTime[ich]  ;
      DetTime[idet][c]          = bufDetTime[ich]         ;
      DetSmoothness[idet][c]    = bufDetSmoothness[ich]         ;
      DetProdDer2[idet][c]      = bufDetProdDer2[ich]         ;

	
      if(idet==MTBP::CSI){
	if(modid<2716){
	  if(fabs(DetSmoothness[idet][c])>CSIWfm_Smoothnessthreshold) CSIWfm_corrupted++;
	  if(DetProdDer2[idet][c]<CSIWfm_ProdDer2threshold)           CSIWfm_corrupted += 10000;
	}else{
	  if(fabs(DetSmoothness[idet][c])>UTCWfm_Smoothnessthreshold) UTCWfm_corrupted++;
	  if(DetProdDer2[idet][c]<UTCWfm_ProdDer2threshold)           UTCWfm_corrupted += 10000;
	}
      }
      DetNumber[idet]++;
    }


  }


}

short MTDstTree::GetCSIPSBit(int ModID){
  int index = ModID2Index(MTBP::CSI, ModID);
  return DetPSbit[MTBP::CSI][index];
}

float MTDstTree::GetCSIProdDer2(int ModID){
  int index = ModID2Index(MTBP::CSI, ModID);
  return CSIProdDer2_asModID[index];
}

float MTDstTree::GetCSISmoothness(int ModID){
  int index = ModID2Index(MTBP::CSI, ModID);
  return CSISmoothness_asModID[index];
}

void MTDstTree::SetBranchAddress( void )
{
  

	
  if(RunMCFlag){
    T->SetBranchAddress( "L1TrigNo", &L1TrigNo );

    if(std::string(T->GetBranch("SpillNo")->GetTitle())=="SpillNo/I"){
      typeSpillNo = 1;
      T->SetBranchAddress( "SpillNo", &SpillNo2019 );
    }else{
      typeSpillNo = 0;
      T->SetBranchAddress( "SpillNo", &SpillNo );
    }
	
    if(std::string(T->GetBranch("L2TrigNo")->GetTitle())=="L2TrigNo/I"){
      typeL2Trig = 1;
      T->SetBranchAddress( "L2TrigNo", &L2TrigNo2019 );
    }else{
      typeL2Trig = 0;
      T->SetBranchAddress( "L2TrigNo", &L2TrigNo );
    }
	
    if( T->GetBranch("ExtTrigType" )) 
      T->SetBranchAddress( "ExtTrigType", &ExtTrigType );
    if( T->GetBranch("DeltaTrig" ))
      T->SetBranchAddress( "DeltaTrig", &DeltaTrig );


    if( T->GetBranch("EventID") )     T->SetBranchAddress( "EventID", &EventNo );
    if( T->GetBranch("L1TrigNo") )    T->SetBranchAddress( "L1TrigNo", &L1TrigNo );
    if( T->GetBranch("TimeStamp") )   T->SetBranchAddress( "TimeStamp", &TimeStamp );
    if( T->GetBranch("L2TimeStamp") ) T->SetBranchAddress( "L2TimeStamp", &L2TimeStamp );
    if( T->GetBranch("Error") )       T->SetBranchAddress( "Error", &Error );
    if( T->GetBranch("TrigTagMismatch") )T->SetBranchAddress( "TrigTagMismatch", &TrigTagMismatch );
    if( T->GetBranch("L2AR") )           T->SetBranchAddress( "L2AR", &L2AR );
    if( T->GetBranch("COE_Et_overflow") )T->SetBranchAddress( "COE_Et_overflow", &COE_Et_overflow);
    if( T->GetBranch("COE_L2_override") )T->SetBranchAddress( "COE_L2_override", &COE_L2_override);
    if( T->GetBranch("COE_Esum") )      T->SetBranchAddress( "COE_Esum", &COE_Esum );
    if( T->GetBranch("COE_Ex") )        T->SetBranchAddress( "COE_Ex", &COE_Ex );
    if( T->GetBranch("COE_Ey") )        T->SetBranchAddress( "COE_Ey", &COE_Ey );
    if( T->GetBranch("DetectorBit") )   T->SetBranchAddress( "DetectorBit", &DetectorBit );
    if( T->GetBranch("RawTrigBit") )    T->SetBranchAddress( "RawTrigBit", &RawTrigBit );
    if( T->GetBranch("ScaledTrigBit") ) T->SetBranchAddress( "ScaledTrigBit", &ScaledTrigBit );
    if( T->GetBranch("LeftEt") )        T->SetBranchAddress( "LeftEt", &LeftEt );
    if( T->GetBranch("LeftEt_raw") )    T->SetBranchAddress( "LeftEt_raw", &LeftEt_raw );
    if( T->GetBranch("RightEt") )       T->SetBranchAddress( "RightEt", &RightEt );
    if( T->GetBranch("RightEt_raw") )   T->SetBranchAddress( "RightEt_raw", &RightEt_raw );
  }

  for( int idet=0; idet<MTBP::nDetectors; idet++){
    ///// Check the existence of the detector.
    ///// If it doesn't exist, DetectorHandler of the detector is not generated.
    if( !T->GetBranch(Form("%sNumber",MTBP::detectorName[idet].c_str())) )
      continue;

    if( idet==MTBP::UCV || idet==MTBP::UCVLG || idet==MTBP::BHPV || idet==MTBP::BHCV || 
	idet==MTBP::CC05 || idet==MTBP::CC06 || idet==MTBP::newBHCV || idet==MTBP::BHGC || 
	idet==MTBP::BCV  || idet==MTBP::IB ) continue;

    if(!RunMCFlag){
      if( idet==MTBP::COSMIC || idet==MTBP::LASER || idet==MTBP::ETC || idet==MTBP::TRIGGERTAG ) continue;
    }

    T->SetBranchAddress( Form( "%sNumber", MTBP::detectorName[idet].c_str()), &(DetNumber[idet]) );
    T->SetBranchAddress( Form( "%sModID",  MTBP::detectorName[idet].c_str()),   DetModID[idet] );
    T->SetBranchAddress( Form( "%sEne",    MTBP::detectorName[idet].c_str()),   DetEne[idet] );
    T->SetBranchAddress( Form( "%sTime",   MTBP::detectorName[idet].c_str()),   DetTime[idet] );
    if( T->GetBranch(Form( "%sEtSum",   MTBP::detectorName[idet].c_str())) ) 
      T->SetBranchAddress( Form( "%sEtSum",  MTBP::detectorName[idet].c_str()),   DetEtSum[idet] );

    if( T->GetBranch(Form( "%sSmoothness",   MTBP::detectorName[idet].c_str())) ){ 
      T->SetBranchAddress( Form( "%sSmoothness",  MTBP::detectorName[idet].c_str()),   DetSmoothness[idet] );
    }
    if( T->GetBranch(Form( "%sProdDer2",     MTBP::detectorName[idet].c_str())) ) 
      T->SetBranchAddress( Form( "%sProdDer2",  MTBP::detectorName[idet].c_str()),   DetProdDer2[idet] );
    
    if(RunMCFlag){
      T->SetBranchAddress( Form( "%sPeak",          MTBP::detectorName[idet].c_str()), DetPeak[idet] );
      T->SetBranchAddress( Form( "%sPedestal",      MTBP::detectorName[idet].c_str()), DetPedestal[idet] );
      T->SetBranchAddress( Form( "%sIntegratedADC", MTBP::detectorName[idet].c_str()), DetIntegratedADC[idet] );
      if(T->GetBranch( Form( "%sInitialPTime",   MTBP::detectorName[idet].c_str()))){
	if(userflag>=20190101){
	  if(MTBP::is125MHzDetector[idet] && idet!=MTBP::CSI)
	    T->SetBranchAddress( Form( "%sInitialPTime",   MTBP::detectorName[idet].c_str()), DetInitialTime[idet] );
	}
      } 
	
      if(T->GetBranch( Form( "%sInitialTime",    MTBP::detectorName[idet].c_str()))){
	if(userflag>=20190101){
	  if(!MTBP::is125MHzDetector[idet] || idet==MTBP::CSI) 
	    T->SetBranchAddress( Form( "%sInitialTime",   MTBP::detectorName[idet].c_str()), DetInitialTime[idet] );
	}else{
	  T->SetBranchAddress( Form( "%sInitialTime",   MTBP::detectorName[idet].c_str()), DetInitialTime[idet] );
	}
      }
      T->SetBranchAddress( Form( "%sPSBit",         MTBP::detectorName[idet].c_str()), DetPSbit[idet] );
    

    }    
  }

  if( T->GetBranch("CDTNum") )  T->SetBranchAddress( "CDTNum",  &CDTNum );
  if( T->GetBranch("CDTData") ) T->SetBranchAddress( "CDTData", CDTData );
  if( T->GetBranch("CDTBit") )  T->SetBranchAddress( "CDTBit",  CDTBit );
  

  if( T->GetBranch("CDTVetoData") ){
    T->SetBranchAddress( "CDTVetoData",    CDTVetoData );
    T->SetBranchAddress( "CDTVetoBitSum",  CDTVetoBitSum );
  }

}

void* MTDstTree::GetL2TrigPtr(std::string& strL2trig){
  if(typeL2Trig){
    strL2trig = "L2Trig/I";
    return (void*) &L2TrigNo2019;
  }else{
    strL2trig = "L2Trig/S";
    return (void*) &L2TrigNo;
  }
}
void* MTDstTree::GetSpillNoPtr(std::string& strSpillNo){
  if(typeSpillNo){
    strSpillNo = "SpillNo/I";
    return (void*) &SpillNo2019;
  }else{
    strSpillNo = "SpillNo/S";
    return (void*) &SpillNo;
  }
}
  
int MTDstTree::GetSpillNo(){
  if(typeSpillNo) return SpillNo2019;
  else return SpillNo;
}

int MTDstTree::GetL2TrigNo(){
  if(typeL2Trig) return L2TrigNo2019;
  else return L2TrigNo;
}


void MTDstTree::SetBranchStatus( const char* bname, Bool_t status )
{
  T->SetBranchStatus( bname, status);
}

bool MTDstTree::IsClock( void )
{
  return DetPeak[MTBP::TRIGGERTAG][MTBP::TRIG_10HzCLOCK] > 5000;
}

bool MTDstTree::IsLaser( void )
{
  return DetPeak[MTBP::TRIGGERTAG][MTBP::TRIG_LASER] > 5000;
}

bool MTDstTree::IsLED( void )
{
  return DetPeak[MTBP::TRIGGERTAG][MTBP::TRIG_LED] > 5000;
}

bool MTDstTree::IsMBCR( void )
{
  return DetPeak[MTBP::TRIGGERTAG][MTBP::TRIG_MBCR] > 5000;
}

bool MTDstTree::IsFBCR( void )
{
  return DetPeak[MTBP::TRIGGERTAG][MTBP::TRIG_FBCR] > 5000;
}

bool MTDstTree::IsCSICR( void )
{
  return DetPeak[MTBP::TRIGGERTAG][MTBP::TRIG_CSICR] > 5000;
}

bool MTDstTree::IsTRIGGERTAG( void )
{
  for( int imod=0; imod<MTBP::TRIGGERTAGNChannels; imod++ ){
    if( DetPeak[MTBP::TRIGGERTAG][imod] > 5000 ){
      return true;
    }
  }
  return false;
}

void MTDstTree::GetData( const int detID, int& number, int* modID, double* ene, double* time , int useinitialdata)
{
  number = DetNumber[detID];
  
  for( int imod=0; imod<number; imod++ ){
    modID[imod] = DetModID[detID][imod];
    
    if((useinitialdata>>0)&0x01) ene[imod] = (double)DetIntegratedADC[detID][imod];
    else ene[imod]   = (double)DetEne[detID][imod];
    
    if((useinitialdata>>1)&0x01) time[imod]  = (double)DetInitialTime[detID][imod];
    else time[imod]  = (double)DetTime[detID][imod];
  }
}

void MTDstTree::GetCSIData(int& number, int* modID, double* ene, double* time , int useinitialdata)
{
  GetData( MTBP::CSI, number, modID, ene, time, useinitialdata);
}

void MTDstTree::GetCSIDataWithoutDeadChannels( int& number, int* modID, double* ene, double* time , int useinitialdata)
{
  number = 0;
  for( int imod=0; imod< DetNumber[MTBP::CSI] ; imod ++ ){
    // Neglect dead channels
    if( CSIDeadChannelSet.find( DetModID[MTBP::CSI][imod] ) != CSIDeadChannelSet.end() ) continue;
    
    // Fill data
    modID[number] = DetModID[MTBP::CSI][imod];
    

    if((useinitialdata>>0)&0x01) ene[number] = (double)DetIntegratedADC[MTBP::CSI][imod];
    else ene[number]   = (double)DetEne[MTBP::CSI][imod];
    
    if((useinitialdata>>1)&0x01) time[number]  = (double)DetInitialTime[MTBP::CSI][imod];
    else time[number]  = (double)DetTime[MTBP::CSI][imod];
     
    number++;
  }
}

void MTDstTree::GetCSIEtSum( Long64_t* EtSum)
{
  for(int i=0; i<64; i++){
    EtSum[i]=DetEtSum[MTBP::CSI][i];
  }
}

std::vector<int> MTDstTree::GetDeadChannelIDVec( int userFlag, char* detName ){

  m_E14BP->SetDetBasicParam(userFlag,detName);
  const std::vector<int> DeadChannelIDVec = m_E14BP->GetDeadChannelIDVec();

  return DeadChannelIDVec;
}
