#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoUCV.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoUCV::MTVetoUCV( Int_t userFlag ) : m_userFlag( userFlag )
{

  DetName = "UCV"; 
  DetNumber = MTBP::UCVNChannels;
  DetModID = new Int_t[DetNumber]();
  DetPSBit  = new Short_t[DetNumber]();
  DetNHit = new Short_t[DetNumber]();
  DetEne   = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  DetTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  
  DetPeakTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  DetFallTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  DetPeakWidth  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();

  ModuleNumber = MTBP::UCVNModules;
  ModuleModID = new Int_t [ModuleNumber]();
  ModuleTime  = new Float_t [ModuleNumber]();
  ModuleEne   = new Float_t [ModuleNumber]();

  UCVModuleDeltaTime = new float [ModuleNumber]();
  
  int maxhit=MTBP::NMaxHitPerEvent;
  MaxBufferNum= DetNumber*maxhit;  
  vetoHitBuffer = new pVetoHit [MaxBufferNum]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  MakeWfmArray();
  Initialize();
}


MTVetoUCV::~MTVetoUCV()
{

  delete[] DetModID;
  delete[] DetNHit;
  delete[] DetEne;  
  delete[] DetTime;
  delete[] DetPeakTime;
  delete[] DetFallTime;
  delete[] DetPSBit;
  delete[] ModuleModID;
  delete[] ModuleTime;
  delete[] ModuleEne;
  delete[] UCVModuleDeltaTime;

  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoUCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "UCVNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "UCVModID",   DetModID );
  m_clusterTree->SetBranchAddress( "UCVnHits",   DetNHit );
  m_clusterTree->SetBranchAddress( "UCVTime",    DetTime );
  m_clusterTree->SetBranchAddress( "UCVPeakTime",    DetPeakTime );
  m_clusterTree->SetBranchAddress( "UCVFallTime",    DetFallTime );
  m_clusterTree->SetBranchAddress( "UCVEne",     DetEne );
  if(m_clusterTree->GetBranch("UCVPSBit"))
	  m_clusterTree->SetBranchAddress( "UCVPSBit",   DetPSBit );

  MTBHVeto::SetBranchAddress(tr);
}


void MTVetoUCV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "UCVNumber", &DetNumber, "UCVNumber/I");
  m_recTree->Branch( "UCVModID",   DetModID,  "UCVModID[UCVNumber]/I");
  if(m_clusterTree->GetBranch("UCVPSBit"))
	  m_recTree->Branch( "UCVModPSBit",   DetPSBit,"UCVModPSBit[UCVNumber]/S");
  m_recTree->Branch( "UCVnHits",   DetNHit,   "UCVnHits[UCVNumber]/S");
  std::ostringstream leafstr[3];
  leafstr[0] << "UCVEne[UCVNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  leafstr[1] << "UCVTime[UCVNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  leafstr[2] << "UCVWidth[UCVNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  m_recTree->Branch( "UCVEne",  DetEne,  leafstr[0].str().c_str() );
  m_recTree->Branch( "UCVTime", DetTime, leafstr[1].str().c_str() );
  m_recTree->Branch( "UCVWidth", DetPeakWidth, leafstr[2].str().c_str() );
  
  //m_recTree->Branch( "UCVModuleNumber",   &ModuleNumber,      "UCVModuleNumber/I" );
  //m_recTree->Branch( "UCVModuleModID",     ModuleModID,       "UCVModuleModID[UCVModuleNumber]/I" );
  //m_recTree->Branch( "UCVModuleHitTime",   ModuleTime,        "UCVModuleHitTime[UCVModuleNumber]/F" );
  //m_recTree->Branch( "UCVModuleEne",       ModuleEne,         "UCVModuleEne[UCVModuleNumber]/F" );
  //m_recTree->Branch( "UCVModuleHitZ",      ModuleHitZ,        "UCVModuleHitZ[UCVModuleNumber]/F" );
  //m_recTree->Branch( "UCVModuleDeltaTime", UCVModuleDeltaTime, "UCVModuleDeltaTime[UCVModuleNumber]/F" );
  
  m_recTree->Branch( "UCVVetoModID", &UCVVetoModID,"UCVVetoModID/I");
  m_recTree->Branch( "UCVVetoEne",   &UCVVetoEne,  "UCVVetoEne/F");
  m_recTree->Branch( "UCVVetoTime",  &UCVVetoTime, "UCVVetoTime/F");

  MTBHVeto::Branch(tr);
}
  
void MTVetoUCV::Initialize( void )
{

  for( int ihit=0; ihit<MaxBufferNum; ihit++ ){
    vetoHitBuffer[ihit].modID = 0;
    vetoHitBuffer[ihit].energy = 0;
    vetoHitBuffer[ihit].time = 0;
  }

  for( int imod=0; imod<MTBP::UCVNModules; imod++){
    ModuleModID[imod]=0;
    ModuleTime[imod]=0;
    UCVModuleDeltaTime[imod]=0;
	for(int i=0;i<MTBP::NMaxHitPerEvent;i++){
		DetPeakWidth[imod][i] = 0;
	}
  }

  UCVVetoModID = -1;
  UCVVetoEne = 0;
  UCVVetoTime = MTBP::Invalid;


}

bool MTVetoUCV::Process( int mode )
{

  Initialize();


  static const double s_UCVZPosition = GetDetZPosition("UCV");
  static const double s_CSIZPosition  = GetDetZPosition("CSI");

  
  float UCVEneBest[12]  = {0};
  float UCVTimeBest[12] = {0}; // ns
  int UCVnHitsOnTime[12] = {0};


  float UCVVetoWidth=0;
  if(m_userFlag<20210201) UCVVetoWidth=MTBP::VetoWidth[MTBP::UCV]/2.;
  else UCVVetoWidth=10;
  
  for(int ich=0;ich<DetNumber;ich++){
    for(int ihit=0; ihit<DetNHit[ich]; ihit++){

	  DetPeakWidth[ich][ihit] = DetFallTime[ich][ihit]*2 - DetTime[ich][ihit];

      int   IndexID = DetModID[ich];
      float Time    = DetTime[ich][ihit] - m_eventStartTime - (s_CSIZPosition - m_eventStartZ)/(TMath::C()/1e6);

      if( m_userFlag<20210201){   // for prototype UCV
	if( TMath::Abs(Time - MTBP::VetoTiming[MTBP::UCV])< UCVVetoWidth && 18<DetPeakWidth[ich][ihit] && DetPeakWidth[ich][ihit]<32){ //select from nhits	  
	  if( UCVEneBest[IndexID] < DetEne[ich][ihit] ){
	    UCVEneBest[IndexID] = DetEne[ich][ihit];
	    UCVTimeBest[IndexID] = DetTime[ich][ihit];
	    UCVnHitsOnTime[IndexID]++;
	  } 
	}
      }
      else{                      // for UCV
	if( TMath::Abs(Time - MTBP::VetoTiming[MTBP::UCV])< UCVVetoWidth ){ //select from nhits	  
	  if( UCVEneBest[IndexID] < DetEne[ich][ihit] ){
	    UCVEneBest[IndexID] = DetEne[ich][ihit];
	    UCVTimeBest[IndexID] = DetTime[ich][ihit];
	    UCVnHitsOnTime[IndexID]++;
	  } 
	}
      }
    }
  }
	
  ModuleNumber=0;  
  for(int imod=0;imod<MTBP::UCVNModules;imod++){
    if( UCVEneBest[imod]>0 ){
      ModuleTime[ModuleNumber] = UCVTimeBest[imod];
      ModuleEne[ModuleNumber] =  UCVEneBest[imod];
      ModuleModID[ModuleNumber] = imod;
      ModuleNumber+=1;
    }
  }
  
  int modnum=0;  
  for( int imod=0; imod<ModuleNumber; imod++ ){
    
	vetoTime = ModuleTime[imod] - m_eventStartTime - (s_CSIZPosition - m_eventStartZ)/(TMath::C()/1e6);
    UCVModuleDeltaTime[imod] = vetoTime;
    
    //if( IsInVetoWindow( UCVModuleDeltaTime[imod] )){
    if( TMath::Abs(UCVModuleDeltaTime[imod]-MTBP::VetoTiming[MTBP::UCV])< UCVVetoWidth ){
      vetoHitBuffer[imod].modID = ModuleModID[imod];
      vetoHitBuffer[imod].energy = ModuleEne[imod];
      vetoHitBuffer[imod].time = UCVModuleDeltaTime[imod];
    }else{
      vetoHitBuffer[imod].modID = ModuleModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = UCVModuleDeltaTime[imod];
    }
	modnum++;
  }


  std::sort( &(vetoHitBuffer[0]), &(vetoHitBuffer[modnum]), pVetoHit::largerHit );
  if( vetoHitBuffer[0].energy > 0 && modnum>0 ){
    UCVVetoModID = vetoHitBuffer[0].modID;
    UCVVetoEne   = vetoHitBuffer[0].energy;
    UCVVetoTime  = vetoHitBuffer[0].time;
  }else{
    UCVVetoModID = -1;
    UCVVetoEne   = 0;
    UCVVetoTime  = -9999.;
  }

  

  if( UCVVetoEne > m_vetoThreshold ){
    return true;
  }

  return false;
}


double MTVetoUCV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoUCV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
