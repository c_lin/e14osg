#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoCV::MTVetoCV( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "CV"; 
  DetNumber = MTBP::CVNModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();
  
  DetWfmCorrupted = NULL;
  CVModuleDeltaTime = new float [DetNumber]();
  CVModuleRiseTime = new Float_t[DetNumber][2];//added at 4th May, 2014 by Maeda Yosuke, name changed at 26th May, 2014
  
  vetoHitBuffer = new pVetoHit [DetNumber]();
  
  x_min = new float [DetNumber]();
  x_max = new float [DetNumber]();
  x_center = new float [DetNumber]();
  y_min = new float [DetNumber]();
  y_max = new float [DetNumber]();
  y_center = new float [DetNumber]();
  z_min = new float [DetNumber]();
  z_max = new float [DetNumber]();
  z_center = new float [DetNumber]();
  quadrant = new Int_t [DetNumber]();
  direction = new Int_t [DetNumber]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
 
  ReadGeometory(); 
  DetNumber = MTBP::CVNChannels;
  MakeWfmArray();
  DetNumber = MTBP::CVNModules;
  Initialize();
}


MTVetoCV::~MTVetoCV()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetSmoothness;
  delete[] DetPSBit;
  delete[] CVModuleDeltaTime;
  delete[] CVModuleRiseTime;
  delete[] vetoHitBuffer;
  delete[] x_min;
  delete[] x_max;
  delete[] x_center;
  delete[] y_min;
  delete[] y_max;
  delete[] y_center;
  delete[] z_min;
  delete[] z_max;
  delete[] z_center;
  delete[] quadrant;
  delete[] direction;
  delete   m_E14BP;
}


void MTVetoCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "CVModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "CVModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "CVModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "CVModuleEne", DetEne );
  if(m_clusterTree->GetBranch("CVModulePSBit"))
	  m_clusterTree->SetBranchAddress( "CVModulePSBit", DetPSBit );
  
  //added at 4th May, 2014 by MY 
  m_clusterTree->SetBranchAddress( "CVModuleRiseTime", CVModuleRiseTime );

  if(m_clusterTree->GetBranch("CVModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "CVModuleSmoothness", DetSmoothness );  
  }
  MTVeto::SetBranchAddress(tr);
}


void MTVetoCV::Branch( TTree* tr )
{
  m_recTree = tr;

  m_recTree->Branch( "CVVetoModID", &CVVetoModID, "CVVetoModID/I");
  m_recTree->Branch( "CVVetoEne", &CVVetoEne, "CVVetoEne/F" );
  m_recTree->Branch( "CVVetoTime", &CVVetoTime, "CVVetoTime/F" );
  m_recTree->Branch( "CVTotalVetoEne", &CVTotalVetoEne, "CVTotalVetoEne/F" );

  m_recTree->Branch( "CVFrontVetoModID", &CVFrontVetoModID, "CVFrontVetoModID/I");
  m_recTree->Branch( "CVFrontVetoEne", &CVFrontVetoEne, "CVFrontVetoEne/F" );
  m_recTree->Branch( "CVFrontVetoTime", &CVFrontVetoTime, "CVFrontVetoTime/F" );
  m_recTree->Branch( "CVFrontTotalVetoEne", &CVFrontTotalVetoEne, "CVFrontTotalVetoEne/F" );

  m_recTree->Branch( "CVRearVetoModID", &CVRearVetoModID, "CVRearVetoModID/I");
  m_recTree->Branch( "CVRearVetoEne", &CVRearVetoEne, "CVRearVetoEne/F" );
  m_recTree->Branch( "CVRearVetoTime", &CVRearVetoTime, "CVRearVetoTime/F" );
  m_recTree->Branch( "CVRearTotalVetoEne", &CVRearTotalVetoEne, "CVRearTotalVetoEne/F" );
  
  //added at 4th May, 2014 (MY)
  m_recTree->Branch( "CVMaxPulseWidth", &CVMaxPulseWidth, "CVMaxPulseWidth/F" );
  
}


void MTVetoCV::BranchOriginalData( TTree* tr )
{
  tr->Branch( "CVModuleNumber", &DetNumber, "CVModuleNumber/I" );
  tr->Branch( "CVModuleModID", DetModID, "CVModuleModID[CVModuleNumber]/I" );
  tr->Branch( "CVModuleHitTime", DetTime, "CVModuleHitTime[CVModuleNumber]/F" );
  tr->Branch( "CVModuleEne", DetEne, "CVModuleEne[CVModuleNumber]/F" );
  if(m_clusterTree->GetBranch("CVModulePSBit"))
	  tr->Branch( "CVModulePSBit", DetPSBit, "CVModulePSBit[CVModuleNumber]/S" );
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "CVModuleSmoothness", DetSmoothness, "CVModuleSmoothness[CVModuleNumber]/F" );
  tr->Branch( "CVModuleDeltaTime", CVModuleDeltaTime, "CVModuleDeltaTime[CVModuleNumber]/F" );

  //added at 4th May, 2014 by MY 
  tr->Branch( "CVModuleRiseTime", CVModuleRiseTime, "CVModuleRiseTime[CVModuleNumber][2]/F" );
  MTVeto::Branch(tr);
}


void MTVetoCV::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::CV))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  for( int imod=0; imod<MTBP::CVNModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  CVVetoModID = -1;
  CVVetoEne = 0;
  CVVetoTime = 9999.;
  CVTotalVetoEne = 0;

  CVFrontVetoModID = -1;
  CVFrontVetoEne = 0;
  CVFrontVetoTime = 9999.;
  CVFrontTotalVetoEne = 0;

  CVRearVetoModID = -1;
  CVRearVetoEne = 0;
  CVRearVetoTime = 9999.;
  CVRearTotalVetoEne = 0;
  
  CVMaxPulseWidth = -1;//added at 4th May, 2014 (MY)
}


bool MTVetoCV::Process( int mode ){

  Initialize();

  static const double CVFZPosition = GetCVFZPosition();//Front
  static const double CVRZPosition = GetCVRZPosition();//Rear

  int frontnum=0;
  int rearnum=0;
  
  for( int imod=0; imod<DetNumber; imod++ ){
    // Front
    //if(DetModID[imod]<48){
    if(DetModID[imod]<98){    // change from "DetModID[imod]<48" to "DetModID[imod]<98" by shiomi at 2016 Mar. 17
      
      //modified at 4th May, 2014 (MY)
      Float_t TOFCorrection = sqrt( pow( CVFZPosition-m_eventStartZ, 2) + pow( x_center[imod], 2) + pow( y_center[imod], 2)) / (TMath::C()/1E6);
      CVModuleDeltaTime[imod] = DetTime[imod] - m_eventStartTime;
      CVModuleDeltaTime[imod] -= TOFCorrection;
      for( Int_t sl=0 ; sl<2 ; sl++ ){
	CVModuleRiseTime[imod][sl] -= (m_eventStartTime+TOFCorrection);
      }    
      if( IsInVetoWindow( CVModuleDeltaTime[imod] )){
	vetoHitBuffer[imod].modID = DetModID[imod];
	vetoHitBuffer[imod].energy = DetEne[imod];
	vetoHitBuffer[imod].time = CVModuleDeltaTime[imod];
	
	CVFrontTotalVetoEne += DetEne[imod];
      }else{
	vetoHitBuffer[imod].modID = DetModID[imod];
	vetoHitBuffer[imod].energy = 0;
	vetoHitBuffer[imod].time = CVModuleDeltaTime[imod];
      }    
      //added at 4th May, 2014 (MY) -> obsolete (20190405)
      if( DetEne[imod]>m_vetoThreshold ){
	for( Int_t sl=0 ; sl<2 ; sl++ ){
	  Float_t PulseWidth = CVModuleDeltaTime[imod]-CVModuleRiseTime[imod][sl];
	  //	  Float_t PulseWidth = CVModuleFallTime[imod][sl]-CVModuleRiseTime[imod][sl];
	  if( PulseWidth<MTBP::nSample125*8
	      && (CVMaxPulseWidth<0 || CVMaxPulseWidth<PulseWidth ) ){
	    CVMaxPulseWidth = PulseWidth;
	  }
	}
      }
      frontnum+=1;
    }
    // Rear
    else {
      Float_t TOFCorrection = sqrt( pow( CVRZPosition-m_eventStartZ, 2) + pow( x_center[imod], 2) + pow( y_center[imod], 2) ) / (TMath::C()/1E6);
      CVModuleDeltaTime[imod] = DetTime[imod] - m_eventStartTime;
      CVModuleDeltaTime[imod] -= TOFCorrection;
      for( Int_t sl=0 ; sl<2 ; sl++ ){
	CVModuleRiseTime[imod][sl] -= (m_eventStartTime+TOFCorrection);
      }
      
      if( IsInVetoWindow( CVModuleDeltaTime[imod] )){
	vetoHitBuffer[imod].modID  = DetModID[imod];
	vetoHitBuffer[imod].energy = DetEne[imod];
	vetoHitBuffer[imod].time   = CVModuleDeltaTime[imod];
	
	CVRearTotalVetoEne += DetEne[imod];
      }else{
	vetoHitBuffer[imod].modID  = DetModID[imod];
	vetoHitBuffer[imod].energy = 0;
	vetoHitBuffer[imod].time   = CVModuleDeltaTime[imod];
      }
      
      //added at 4th May, 2014 (MY) -> obsolete (20190405)
      if( DetEne[imod]>m_vetoThreshold ){
	for( Int_t sl=0 ; sl<2 ; sl++ ){
	  Float_t PulseWidth = CVModuleDeltaTime[imod]-CVModuleRiseTime[imod][sl];
	  //	  Float_t PulseWidth = CVModuleFallTime[imod][sl]-CVModuleRiseTime[imod][sl];
	  if( PulseWidth<MTBP::nSample125*8
	      && (CVMaxPulseWidth<0 || CVMaxPulseWidth<PulseWidth ) ){
	    CVMaxPulseWidth = PulseWidth;
	  }
	}
      }
      rearnum+=1;
    }

  }

  std::sort( vetoHitBuffer, &(vetoHitBuffer[frontnum]), pVetoHit::largerHit );
  if( vetoHitBuffer[0].energy > 0 && frontnum>0){
    CVFrontVetoModID = vetoHitBuffer[0].modID;
    CVFrontVetoEne   = vetoHitBuffer[0].energy;
    CVFrontVetoTime  = vetoHitBuffer[0].time;
  }else{
    CVFrontVetoModID = -1;
    CVFrontVetoEne = 0;
    CVFrontVetoTime = -999.;
  }
  
  std::sort( &(vetoHitBuffer[frontnum]), &(vetoHitBuffer[frontnum+rearnum]), pVetoHit::largerHit );
  if( vetoHitBuffer[frontnum].energy > 0 ){
    CVRearVetoModID = vetoHitBuffer[frontnum].modID;
    CVRearVetoEne   = vetoHitBuffer[frontnum].energy;
    CVRearVetoTime  = vetoHitBuffer[frontnum].time;
  }else{
    CVRearVetoModID = -1;
    CVRearVetoEne   = 0;
    CVRearVetoTime  = -999.;
  }

  // Both
  if( CVFrontVetoEne < CVRearVetoEne ){
    CVVetoModID = CVRearVetoModID;
    CVVetoEne = CVRearVetoEne;
    CVVetoTime = CVRearVetoTime;
  }else{
    CVVetoModID = CVFrontVetoModID;
    CVVetoEne = CVFrontVetoEne;
    CVVetoTime = CVFrontVetoTime;
  }
  CVTotalVetoEne = CVFrontTotalVetoEne + CVRearTotalVetoEne;

  if( CVVetoEne > m_vetoThreshold ) return true;

  return false;

}



void MTVetoCV::ReadGeometory( void )
{

  std::string dirName
    = std::string( std::getenv(MTBP::EnvName.c_str()) )+"/AnalysisLibrary/LegacyProjects/MTAnalysisLibrary";
  std::string mapFile = dirName + "/data/CV_map.csv";

  if( access( mapFile.c_str(), R_OK) != 0 ){
    std::cout << mapFile.c_str() << " is not found." << std::endl;
    exit(1);
  }

  std::string header;
  std::ifstream ifs( mapFile.c_str() );
  std::getline( ifs, header );

  int dummyI = 0;
  float dummyD = 0;

  for( int imod=0; imod<MTBP::CVNModules; imod++ ){
    ifs >> dummyI >> dummyI >> dummyI >> x_min[imod] >> x_max[imod] >> y_min[imod] >> y_max[imod] >> z_min[imod] >> z_max[imod] >> dummyD >> dummyD >> dummyD >> dummyD >> quadrant[imod];

    x_center[imod] = ( x_min[imod] + x_max[imod] ) / 2.;
    y_center[imod] = ( y_min[imod] + y_max[imod] ) / 2.;
    z_center[imod] = ( z_min[imod] + z_max[imod] ) / 2.;

    if( ( imod<48 && quadrant[imod]==0 ) || ( imod>47 && quadrant[imod]==3 ) ){
      direction[imod] = 0;
    }else if(  ( imod<48 && quadrant[imod]==2 ) || ( imod>47 && quadrant[imod]==1 ) ){
      direction[imod] = 2;
    }else if(  ( imod<48 && quadrant[imod]==1 ) || ( imod>47 && quadrant[imod]==0 ) ){
      direction[imod] = 1;
    }else if(  ( imod<48 && quadrant[imod]==3 ) || ( imod>47 && quadrant[imod]==2 ) ){
      direction[imod] = 3;
    }
  }

  ifs.close();
}


double MTVetoCV::GetCVFZPosition(){

  m_E14BP->SetDetBasicParam(m_userFlag,"CVF");
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

double MTVetoCV::GetCVRZPosition(){

  m_E14BP->SetDetBasicParam(m_userFlag,"CVR");
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoCV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
