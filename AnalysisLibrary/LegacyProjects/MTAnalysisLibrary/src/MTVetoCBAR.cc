#include <sstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoCBAR.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoCBAR::MTVetoCBAR( Int_t userFlag ) : m_userFlag( userFlag )
{
  
  DetName = "CBAR"; 
  DetNumber = MTBP::CBARNModules;
  DetModID = new Int_t [DetNumber]();
  DetTime  = new Float_t [DetNumber]();
  DetEne   = new Float_t [DetNumber]();
  DetHitZ  = new Float_t [DetNumber]();
  DetPSBit  = new Short_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();
  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 


  DetWfmCorrupted = NULL;
  CBARModuleDeltaTime = new float [DetNumber]();

  vetoHitBuffer = new pVetoHit [MTBP::CBARNModules]();

  CopyDstBranchFlag = false;
  DetDstNumber   = MTBP::CBARNChannels;
  DetDstModID    = new Int_t  [DetDstNumber];
  DetDstEne      = new Float_t[DetDstNumber];
  DetDstTime     = new Float_t[DetDstNumber];
  DetDstPTime    = new Float_t[DetDstNumber];
  
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  DetNumber = MTBP::CBARNChannels;
  MakeWfmArray();
  DetNumber = MTBP::CBARNModules;

  Initialize();
}


MTVetoCBAR::~MTVetoCBAR()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetPSBit;
  delete[] DetHitZ;
  delete[] DetSmoothness;
  delete[] CBARModuleDeltaTime;
  delete[] vetoHitBuffer;
  
  delete[] DetDstModID;
  delete[] DetDstEne;
  delete[] DetDstTime;
  delete[] DetDstPTime;
  delete   m_E14BP;
}


void MTVetoCBAR::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "CBARModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "CBARModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "CBARModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "CBARModuleEne", DetEne );
  if(m_clusterTree->GetBranch("CBARModulePSBit"))
	  m_clusterTree->SetBranchAddress( "CBARModulePSBit", DetPSBit );
  m_clusterTree->SetBranchAddress( "CBARModuleHitZ", DetHitZ );

  if(m_clusterTree->GetBranch("CBARModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress("CBARModuleSmoothness", DetSmoothness );  
  }

  if( m_clusterTree->GetBranch("CBARNumber") ){
    CopyDstBranchFlag = true;
    m_clusterTree->SetBranchAddress( "CBARNumber",  &DetDstNumber);
    m_clusterTree->SetBranchAddress( "CBARModID",    DetDstModID );
    m_clusterTree->SetBranchAddress( "CBAREne",      DetDstEne );
    m_clusterTree->SetBranchAddress( "CBARTime",     DetDstTime );
    m_clusterTree->SetBranchAddress( "CBARPTime",    DetDstPTime );
    m_clusterTree->SetBranchAddress( "CBARFallTime", DetDstFallTime );
    m_clusterTree->SetBranchAddress( "CBARPSBit",    DetPSBit );
  }

  if( m_clusterTree->GetBranch("CBARCH21Ene") ){
    m_clusterTree->SetBranchAddress( "CBARCH21Ene",  &CBARCH21Ene);
    m_clusterTree->SetBranchAddress( "CBARCH21Time", &CBARCH21Time );    
  }
  
  MTVeto::SetBranchAddress(tr);
}


void MTVetoCBAR::Branch( TTree* tr )
{
  m_recTree = tr;

  m_recTree->Branch( "CBARVetoModID", &CBARVetoModID, "CBARVetoModID/I");
  m_recTree->Branch( "CBARVetoEne", &CBARVetoEne, "CBARVetoEne/F" );
  m_recTree->Branch( "CBARVetoTime", &CBARVetoTime, "CBARVetoTime/F" );
  m_recTree->Branch( "CBARVetoHitZ", &CBARVetoHitZ, "CBARVetoHitZ/F" );
  
  m_recTree->Branch( "CBARTotalVetoEne", &CBARTotalVetoEne, "CBARTotalVetoEne/F" );  
  
  m_recTree->Branch( "CBARInnerVetoModID", &CBARInnerVetoModID, "CBARInnerVetoModID/I");
  m_recTree->Branch( "CBARInnerVetoEne", &CBARInnerVetoEne, "CBARInnerVetoEne/F" );
  m_recTree->Branch( "CBARInnerVetoTime", &CBARInnerVetoTime, "CBARInnerVetoTime/F" );
  m_recTree->Branch( "CBARInnerVetoHitZ", &CBARInnerVetoHitZ, "CBARInnerVetoHitZ/F" );

  m_recTree->Branch( "CBAROuterVetoModID", &CBAROuterVetoModID, "CBAROuterVetoModID/I");
  m_recTree->Branch( "CBAROuterVetoEne", &CBAROuterVetoEne, "CBAROuterVetoEne/F" );
  m_recTree->Branch( "CBAROuterVetoTime", &CBAROuterVetoTime, "CBAROuterVetoTime/F" );
  m_recTree->Branch( "CBAROuterVetoHitZ", &CBAROuterVetoHitZ, "CBAROuterVetoHitZ/F" );
  
}


void MTVetoCBAR::BranchOriginalData( TTree* tr )
{
  tr->Branch( "CBARModuleNumber", &DetNumber, "CBARModuleNumber/I" );
  tr->Branch( "CBARModuleModID", DetModID, "CBARModuleModID[CBARModuleNumber]/I" );
  tr->Branch( "CBARModuleHitTime", DetTime, "CBARModuleHitTime[CBARModuleNumber]/F" );
  tr->Branch( "CBARModuleEne", DetEne, "CBARModuleEne[CBARModuleNumber]/F" );
  tr->Branch( "CBARModuleHitZ", DetHitZ, "CBARModuleHitZ[CBARModuleNumber]/F" );
  tr->Branch( "CBARModuleDeltaTime", CBARModuleDeltaTime, "CBARModuleDeltaTime[CBARModuleNumber]/F" );
  
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  tr->Branch( "CBARModuleSmoothness", DetSmoothness, "CBARSmoothness[CBARModuleNumber]/F" );
  if(m_clusterTree->GetBranch("CBARModulePSBit"))
	  tr->Branch( "CBARModulePSBit", DetPSBit, "CBARModulePSBit[CBARModuleNumber]/S" );
        
  if( CopyDstBranchFlag ){
    tr->Branch( "CBARNumber",  &DetDstNumber,   "CBARNumber/I");
    tr->Branch( "CBARModID",    DetDstModID,    "CBARModID[CBARNumber]/I");
    tr->Branch( "CBAREne",      DetDstEne,      "CBAREne[CBARNumber]/F" );
    tr->Branch( "CBARTime",     DetDstTime,     "CBARTime[CBARNumber]/F" );
    tr->Branch( "CBARPTime",    DetDstPTime,    "CBARPTime[CBARNumber]/F" );
  }

  if( m_clusterTree->GetBranch("CBARCH21Ene") ){
    tr->Branch("CBARCH21Ene",&CBARCH21Ene,"CBARCH21Ene/F");
    tr->Branch("CBARCH21Time",&CBARCH21Time,"CBARCH21Time/F");
  }
  
  MTVeto::Branch(tr);
}


void MTVetoCBAR::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::CBAR))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  for( int imod=0; imod<MTBP::CBARNModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
    vetoHitBuffer[imod].position = 0;
  }
}


bool MTVetoCBAR::Process( int mode ){

  CBARTotalVetoEne=0;

  Initialize();

  static const double s_CBARZPosition = GetDetZPosition("CBAR");

  int innernum=0;
  int outernum=0;
  
  for( int imod=0; imod<DetNumber; imod++ ){

    // veto time calculation
    CBARModuleDeltaTime[imod] = DetTime[imod] - m_eventStartTime;
    float deltaZ = m_eventStartZ - ( s_CBARZPosition + MTBP::CBARLength/2. );
    if( DetHitZ[imod] < -3000 ){
      deltaZ += 3000;
    }else if( DetHitZ[imod] > 3000 ){
      deltaZ -= 3000;
    }else{
      deltaZ -= DetHitZ[imod];
    }
    if( DetModID[imod] < 32 ){
      CBARModuleDeltaTime[imod] -= sqrt( pow( deltaZ, 2) + pow( MTBP::CBARInnerInnerR, 2) ) / (TMath::C()/1E6);
      innernum+=1;
    }else{
      CBARModuleDeltaTime[imod] -= sqrt( pow( deltaZ, 2) + pow( MTBP::CBAROuterInnerR, 2) ) / (TMath::C()/1E6);
      outernum+=1;
    }

    if( IsInVetoWindow( CBARModuleDeltaTime[imod] )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = CBARModuleDeltaTime[imod];
      vetoHitBuffer[imod].position = DetHitZ[imod];
      CBARTotalVetoEne+=DetEne[imod];
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = CBARModuleDeltaTime[imod];
      vetoHitBuffer[imod].position = DetHitZ[imod];
    }

  }

  //std::cout<<innernum<<" "<<outernum<<std::endl;
  
  // Inner
  std::sort( &(vetoHitBuffer[0]), &(vetoHitBuffer[innernum]), pVetoHit::largerHit );
  if( vetoHitBuffer[0].energy > 0 && innernum>0 ){
    CBARInnerVetoModID = vetoHitBuffer[0].modID;
    CBARInnerVetoEne   = vetoHitBuffer[0].energy;
    CBARInnerVetoTime  = vetoHitBuffer[0].time;
    CBARInnerVetoHitZ  = vetoHitBuffer[0].position;    
    
  }else{
    CBARInnerVetoModID = -1;
    CBARInnerVetoEne   = 0;
    CBARInnerVetoTime  = -9999.;
    CBARInnerVetoHitZ  = -9999.;
  }

  // Outer
  std::sort( &(vetoHitBuffer[innernum]), &(vetoHitBuffer[innernum+outernum]), pVetoHit::largerHit );
  if( vetoHitBuffer[innernum].energy > 0 && outernum>0 ){
    CBAROuterVetoModID = vetoHitBuffer[innernum].modID;
    CBAROuterVetoEne   = vetoHitBuffer[innernum].energy;
    CBAROuterVetoTime  = vetoHitBuffer[innernum].time;
    CBAROuterVetoHitZ  = vetoHitBuffer[innernum].position;
  }else{
    CBAROuterVetoModID = -1;
    CBAROuterVetoEne   = 0;
    CBAROuterVetoTime  = -9999.;
    CBAROuterVetoHitZ  = -9999.;
  }

  // Both
  if( CBARInnerVetoEne > CBAROuterVetoEne ){
    CBARVetoModID = CBARInnerVetoModID;
    CBARVetoEne   = CBARInnerVetoEne;
    CBARVetoTime  = CBARInnerVetoTime;
    CBARVetoHitZ  = CBARInnerVetoHitZ;
  }else{
    CBARVetoModID = CBAROuterVetoModID;
    CBARVetoEne   = CBAROuterVetoEne;
    CBARVetoTime  = CBAROuterVetoTime;
    CBARVetoHitZ  = CBAROuterVetoHitZ;
  }


  if( CBARVetoEne > m_vetoThreshold ){
    return true;
  }
  return false;

}


double MTVetoCBAR::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoCBAR::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
