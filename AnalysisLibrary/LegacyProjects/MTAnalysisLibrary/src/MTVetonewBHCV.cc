#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetonewBHCV.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetonewBHCV::MTVetonewBHCV( Int_t userFlag ) : m_userFlag( userFlag )
{

  DetName = "newBHCV"; 
  DetNumber = MTBP::newBHCVNChannels;
  DetModID = new Int_t[DetNumber]();
  DetNHit = new Short_t[DetNumber]();
  DetEne   = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  DetTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();

  TOFCorrection = new Float_t[DetNumber];
  for( Int_t iMod=0 ; iMod<DetNumber ; iMod++ ){
    //TOFCorrection[iMod] = MTBP::newBHCVDeltaZ[iMod]/(TMath::C()*1e-6);
  }
  
  
  int maxhit=MTBP::NMaxHitPerEvent;
  MaxBufferNum= DetNumber*maxhit;  
  vetoHitBuffer = new pVetoHit [MaxBufferNum]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  MakeWfmArray();
  Initialize();
}


MTVetonewBHCV::~MTVetonewBHCV()
{

  delete[] DetModID;
  delete[] DetNHit;
  delete[] DetEne;  
  delete[] DetTime;
  delete[] TOFCorrection;
  
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetonewBHCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "newBHCVNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "newBHCVModID", DetModID );
  m_clusterTree->SetBranchAddress( "newBHCVnHits", DetNHit );
  m_clusterTree->SetBranchAddress( "newBHCVTime", DetTime );
  m_clusterTree->SetBranchAddress( "newBHCVEne", DetEne );
  MTBHVeto::SetBranchAddress(tr);
}


void MTVetonewBHCV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "newBHCVNumber", &DetNumber, "newBHCVNumber/I");
  m_recTree->Branch( "newBHCVModID", DetModID, "newBHCVModID[newBHCVNumber]/I");
  m_recTree->Branch( "newBHCVnHits", DetNHit, "newBHCVnHits[newBHCVNumber]/S");
  std::ostringstream leafstr[2];
  leafstr[0] << "newBHCVEne[newBHCVNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  leafstr[1] << "newBHCVTime[newBHCVNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  m_recTree->Branch( "newBHCVEne", DetEne, leafstr[0].str().c_str() );
  m_recTree->Branch( "newBHCVTime", DetTime, leafstr[1].str().c_str() );


  m_recTree->Branch( "newBHCVVetoModID", &newBHCVVetoModID,"newBHCVVetoModID/I");
  m_recTree->Branch( "newBHCVVetoEne", &newBHCVVetoEne,"newBHCVVetoEne/F");
  m_recTree->Branch( "newBHCVVetoTime", &newBHCVVetoTime,"newBHCVVetoTime/F");
  m_recTree->Branch( "newBHCVHitCount",&newBHCVHitCount,"newBHCVHitCount/I");
  m_recTree->Branch( "newBHCVModHitCount",&newBHCVModHitCount,"newBHCVModHitCount/I");
  MTBHVeto::Branch(tr);
}


void MTVetonewBHCV::Initialize( void )
{

  for( int imod=0; imod<MaxBufferNum; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  newBHCVVetoModID = -1;
  newBHCVVetoEne = 0;
  newBHCVVetoTime = MTBP::Invalid;

  newBHCVHitCount  = 0;
  newBHCVModHitCount  = 0;

}


bool MTVetonewBHCV::Process( int mode )
{
  
  Initialize();

  static const double s_newBHCVZPosition = GetDetZPosition("newBHCV");

  const Double_t Ethre = 884.e-6/4;
  Float_t WideWindow[2];
  Float_t NarrowWindow[2];
  if(m_userFlag<20160101){
    WideWindow[0]  =-105.;WideWindow[1]  =-75.;
    NarrowWindow[0]=-101.;NarrowWindow[1]=-76.;
  }else{
    WideWindow[0]  =-110.;WideWindow[1]  =-80.;
    NarrowWindow[0]=-106.;NarrowWindow[1]=-81.;
  }

  Float_t newBHCVModEne[3]  = {-9999,-9999,-9999};
  Float_t newBHCVModTime[3] = {-9999,-9999,-9999};
  int hitflag[3] ={0,0,0};
  
  for(Int_t inum = 0 ; inum < DetNumber ; ++inum){
    Int_t modID = DetModID[inum];
    Double_t TOFCorr = ( (s_newBHCVZPosition+MTBP::newBHCVmoduleThickness*(modID/16)) - m_eventStartZ ) / (TMath::C()*1e-6);
    
    for(Int_t ihit = 0 ; ihit < DetNHit[inum] ; ++ihit){
      Float_t energy = DetEne[inum][ihit];
      Float_t time   = DetTime[inum][ihit]-TOFCorr-m_eventStartTime;
      
      //std::cout<<energy<<" "<<time<<std::endl;
      //std::getchar();
      
      if( IsInWideWindow(time,WideWindow[0],WideWindow[1]) && newBHCVModEne[modID/16]<energy){
	newBHCVModEne[modID/16]  = energy;
	newBHCVModTime[modID/16] = time;	
      }
      if( IsInNarrowWindow(time,NarrowWindow[0],NarrowWindow[1]) && newBHCVVetoEne<energy){
	newBHCVVetoEne   = energy;
	newBHCVVetoTime  = time;
	newBHCVVetoModID  = modID;
      }
      if( IsInNarrowWindow(time,NarrowWindow[0],NarrowWindow[1]) && energy>Ethre ){
	++newBHCVHitCount;	  
      }
    }
  }
  
  for(int i=0; i<3; i++){
    if(newBHCVModEne[i]>Ethre) newBHCVModHitCount+=1;
  }

  if( newBHCVModHitCount>1 && newBHCVVetoEne>Ethre ) return true;
       
  return false;
  
}
double MTVetonewBHCV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetonewBHCV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
