#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoIBCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoIBCV::MTVetoIBCV( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "IBCV"; 
  DetNumber = MTBP::IBCVNModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();
  DetHitZ  = new Float_t [DetNumber]();

  IBCVModuleDeltaTime = new float [DetNumber]();

  DetWfmCorrupted = NULL;
  vetoHitBuffer = new pVetoHit [DetNumber]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  DetNumber = MTBP::IBCVNChannels;
  MakeWfmArray();
  DetNumber = MTBP::IBCVNModules;
  Initialize();
}


MTVetoIBCV::~MTVetoIBCV()
{
  delete[] DetModID;
  delete[] DetPSBit;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetSmoothness;
  delete[] DetHitZ;
  delete[] IBCVModuleDeltaTime;
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoIBCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "IBCVModuleNumber", &DetNumber);
  if(m_clusterTree->GetBranch("IBCVModulePSBit"))
	  m_clusterTree->SetBranchAddress( "IBCVModulePSBit", DetPSBit);

  if(m_clusterTree->GetBranch("IBCVModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "IBCVModuleSmoothness", DetSmoothness );  
  }
  m_clusterTree->SetBranchAddress( "IBCVModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "IBCVModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "IBCVModuleEne", DetEne );
  m_clusterTree->SetBranchAddress( "IBCVModuleHitZ", DetHitZ );
  MTVeto::SetBranchAddress(tr);
}


void MTVetoIBCV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "IBCVModuleNumber", &DetNumber, "IBCVModuleNumber/I");
  m_recTree->Branch( "IBCVModuleModID", DetModID, "IBCVModuleModID[IBCVModuleNumber]/I");
  m_recTree->Branch( "IBCVModuleEne", DetEne, "IBCVModuleEne[IBCVModuleNumber]/F");
  m_recTree->Branch( "IBCVModuleHitTime", DetTime, "IBCVModuleHitTime[IBCVModuleNumber]/F");
  m_recTree->Branch( "IBCVModuleDeltaTime", IBCVModuleDeltaTime, "IBCVModuleDeltaTime[IBCVModuleNumber]/F" );
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "IBCVModuleSmoothness", DetSmoothness, "IBCVModuleSmoothness[IBCVModuleNumber]/F" );
  if(m_clusterTree->GetBranch("IBCVModulePSBit"))
	  m_recTree->Branch( "IBCVModulePSBit", DetPSBit, "IBCVModulePSBit[IBCVModuleNumber]/S" );
  
  m_recTree->Branch( "IBCVVetoModID", &IBCVVetoModID,"IBCVVetoModID/I");
  m_recTree->Branch( "IBCVVetoEne", &IBCVVetoEne,"IBCVVetoEne/F");
  m_recTree->Branch( "IBCVVetoTime", &IBCVVetoTime,"IBCVVetoTime/F");
  m_recTree->Branch( "IBCVVetoHitZ", &IBCVVetoHitZ,"IBCVVetoHitZ/F");

  m_recTree->Branch( "IBCVCH27VetoEne",   &IBCVCH27VetoEne,  "IBCVCH27VetoEne/F");
  m_recTree->Branch( "IBCVCH27VetoTime",  &IBCVCH27VetoTime, "IBCVCH27VetoTime/F");
  
  MTVeto::Branch(tr);
}


void MTVetoIBCV::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::IBCV))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  for( int imod=0; imod<MTBP::IBCVNModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
    vetoHitBuffer[imod].position = 0;
  }

  IBCVVetoModID = -1;
  IBCVVetoEne = 0;
  IBCVVetoTime = 9999.;

  IBCVCH27VetoEne = 0;
  IBCVCH27VetoTime = -9999.;

}


bool MTVetoIBCV::Process( int mode ){

  Initialize();
  
  static const double s_CSIZPosition  = GetDetZPosition("CSI");
  static const double s_IBCVZPosition = GetDetZPosition("IBCV");
  static const std::vector<int> s_DeadModuleIDVec = GetDeadModuleIDVec("IBCV");
  const Float_t IBCVNewTimeWindow[2] = {-20, 40};
  const Float_t IBCVCH27TimeWindow[2] = {-50, 100};

  for( int imod=0; imod<DetNumber; imod++ ){
    // veto time calculation
    IBCVModuleDeltaTime[imod] = DetTime[imod] - m_eventStartTime;
    float deltaZ = m_eventStartZ - ( s_IBCVZPosition + MTBP::IBCVLength/2. );
    if( DetHitZ[imod] < -1500 ){
      deltaZ += 1500;
    }else if( DetHitZ[imod] > 1500 ){
      deltaZ -= 1500;
    }else{
      deltaZ -= DetHitZ[imod];
    }
    IBCVModuleDeltaTime[imod] -= sqrt( pow( deltaZ, 2) + pow( MTBP::IBCVInnerR, 2) ) / (TMath::C()/1E6);

    if( IsInVetoWindow( IBCVModuleDeltaTime[imod] )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = IBCVModuleDeltaTime[imod];
      vetoHitBuffer[imod].position = DetHitZ[imod];
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = IBCVModuleDeltaTime[imod];
      vetoHitBuffer[imod].position = DetHitZ[imod];
    }

    // dead channel treatment
    Bool_t DeadFlag=false;
    for(int ideadmod=0;ideadmod<s_DeadModuleIDVec.size();ideadmod++){
      if( DetModID[imod]==s_DeadModuleIDVec.at(ideadmod) ){
	DeadFlag=true;
	break;
      }
    }
    if( DeadFlag ){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }


  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );
  if( vetoHitBuffer[0].energy>0){
    IBCVVetoModID = vetoHitBuffer[0].modID;
    IBCVVetoEne   = vetoHitBuffer[0].energy;
    IBCVVetoTime  = vetoHitBuffer[0].time;
    IBCVVetoHitZ  = vetoHitBuffer[0].position;
  } else {
    IBCVVetoModID = -1;
    IBCVVetoEne   = 0;
    IBCVVetoTime  = -9999.;
    IBCVVetoHitZ  = -9999.;
  }

  //// treatment of IBCV dead channel ////
  if( 20160101<=m_userFlag && m_userFlag<20180701 ){
    for( int imod=0;imod<DetNumber;imod++){
      if( DetModID[imod]==27 ){
	Float_t Time = DetTime[imod] - m_eventStartTime;
	if( IBCVCH27TimeWindow[0]<IBCVModuleDeltaTime[imod] && IBCVModuleDeltaTime[imod]<IBCVCH27TimeWindow[1] ){
	  IBCVCH27VetoEne  = DetEne[imod];
	  IBCVCH27VetoTime = Time;
	}

      }
    }
  }

  if( IBCVVetoEne > m_vetoThreshold ) return true;

  return false;

}


double MTVetoIBCV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName, 0);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoIBCV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName, 0);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
