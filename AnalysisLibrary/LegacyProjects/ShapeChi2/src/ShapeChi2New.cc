#include "ShapeChi2/ShapeChi2New.h"
#include "csimap/CsiMap.h"
#include <string>
#include <map>
#include <sstream>
#include <iostream>
#include <fstream>
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"

//#define SHAPECHI_DEBUG
#ifdef SHAPECHI_DEBUG
#include "myplot/Plotter.h"
#include "TText.h"
#include "TGraph.h"
TCanvas *canvas = 0;
std::string psfile = "shapeChi2_debug.ps";
#endif

TH2D** ShapeChi2New::getHistograms(){
	std::cout<<"h"<<std::endl;
	if(m_histResult[MEAN]==0){
		m_histResult[MEAN] = new TH2D("histMean","map mean",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);
		m_histResult[RMS2] = new TH2D("histRMS2","map rms^{2}",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);
		m_histResult[PROB] = new TH2D("histProb","map event prob.",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);
		m_histResult[NUMHIT] = new TH2D("histNumHit","MC stat.of hits",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);
		m_histResult[NUMGEN] = new TH2D("histNumGen","MC stat.of gen",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);
		m_histResult[CHI2GAUS] = new TH2D("histChi2Gaus","#chi^{2}_{gaus} on each crystal.",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);
		m_histResult[CHI2BINVV] = new TH2D("histChi2BinVV","#chi^{2}_{binVV} on each crystal.",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);
		m_histResult[CHI2BINLL] = new TH2D("histChi2BinLL","#chi^{2}_{binLL} on each crystal.",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);
		m_histResult[DATA] = m_histData;
		m_histResult[TYPE] = m_histType;
	}

	for(int i=0;i<s_nHistogram;i++){
		if(i!=DATA&&i!=TYPE) m_histResult[i]->Reset();
	}
	for(int i=0;i<s_mapSize;i++){
		for(int j=0;j<s_mapSize;j++){
			if((int)(m_histType->GetBinContent(i+1,j+1)+1e-3) == NONEXIST ) continue;
			m_histResult[MEAN]->SetBinContent(i+1,j+1,m_mean[i][j]);
			m_histResult[RMS2]->SetBinContent(i+1,j+1,m_rms2[i][j]);
			m_histResult[PROB]->SetBinContent(i+1,j+1,m_prob[i][j]);
			m_histResult[NUMHIT]->SetBinContent(i+1,j+1,m_numHit[i][j]);
			m_histResult[NUMGEN]->SetBinContent(i+1,j+1,m_numGen[i][j]);
			m_histResult[CHI2GAUS]->SetBinContent(i+1,j+1,m_chi2Gaus[i][j]);
			m_histResult[CHI2BINVV]->SetBinContent(i+1,j+1,m_chi2BinVV[i][j]);
			m_histResult[CHI2BINLL]->SetBinContent(i+1,j+1,m_chi2BinLL[i][j]);
		}
	}

#ifdef SHAPECHI_DEBUG
	std::cout<<"a"<<std::endl;
	canvas->Clear();
	canvas->Divide(2,2);
	TText txt;
	TGraph gra;
	int n=0;
	double x[s_mapSize*s_mapSize],y[s_mapSize*s_mapSize];
	for(int i=0;i<s_mapSize;i++){
		for(int j=0;j<s_mapSize;j++){
			if(m_isUse[i][j]){
				x[n] = i+0.5;
				y[n] = j+0.5;
				n++;
			}
		}
	}

	canvas->cd(1)->SetLogz();
	m_histResult[DATA]->Draw("colz");
	txt.DrawTextNDC(0.6,0.5,Form("e%dt%dp%dx%dy%d",m_index[0],m_index[1],m_index[2],m_index[3],m_index[4]));
	txt.DrawTextNDC(0.6,0.4,Form("chi2Mode:%d",m_chi2Mode));
	txt.DrawTextNDC(0.6,0.3,Form("hsize:%d",m_hitMapSize));
	txt.DrawTextNDC(0.6,0.2,Form("nhsize:%g",m_nonHitMapSize));
	canvas->cd(2)->SetLogz();
	m_histResult[MEAN]->Draw("colz");
	canvas->cd(3)->SetLogz();
	m_histResult[RMS2]->Draw("colz");
	canvas->cd(4)->SetLogz();
	m_histResult[PROB]->Draw("colz");
	canvas->Print(psfile.c_str());
	for(int i=1;i<=4;i++){
		canvas->cd(i);
		gra.DrawGraph(n,x,y,"*");
	}
	canvas->Print(psfile.c_str());

	canvas->cd(1)->SetLogz();
	m_histResult[DATA]->Draw("colz");
	canvas->cd(2)->SetLogz();
	m_histResult[CHI2GAUS]->Draw("colz");
	canvas->cd(3)->SetLogz();
	m_histResult[CHI2BINVV]->Draw("colz");
	canvas->cd(4)->SetLogz();
	m_histResult[CHI2BINLL]->Draw("colz");
	canvas->Print(psfile.c_str());
	for(int i=1;i<=4;i++){
		canvas->cd(i);
		gra.DrawGraph(n,x,y,"*");
	}
	canvas->Print(psfile.c_str());

	canvas->Clear();
	canvas->SetLogz(0);
#endif

	return m_histResult;
}

ShapeChi2New::ShapeChi2New()
	:m_smallCsiWidth(25.),m_maxArrSize(3000),m_threshold(3)
{

#ifdef SHAPECHI_DEBUG
	canvas = Plotter::getPlotter()->canvas();
	canvas->Print((psfile+"[").c_str());
#endif

	int arrSize = 3*s_numEvalue*s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin;
	for(int i=0;i<arrSize;i++){
		mapNum[i]=0;
		mapNum0[i]=0;
		mapMean[i]=mapSquare[i]=0;
	}

	for(int i0=0;i0<2;i0++){
		for(int i1=0;i1<2;i1++){
			for(int i2=0;i2<s_numEvalue;i2++){
				for(int i3=0;i3<s_numTvalue;i3++){
					for(int i4=0;i4<s_numPvalue;i4++){
						distRowE[i0][i1][i2][i3][i4] = 0;
					}
				}
			}
		}
	}
	loadMap();
	createHist();
}


ShapeChi2New::~ShapeChi2New(){
	for(int type=0;type<3;type++){
		for(int e=0;e<s_numEvalue;e++){
			for(int t=0;t<s_numTvalue;t++){
				for(int p=0;p<s_numPvalue;p++){
					for(int ctrx = 0;ctrx<s_numCtrBin;ctrx++){
						for(int ctry= 0;ctry<s_numCtrBin;ctry++){
							int index = getMapAddress(type,e,t,p,ctrx,ctry);
							if(mapNum[index]!=0)  delete [] mapNum[index];
							if(mapMean[index]!=0)  delete [] mapMean[index];
							if(mapSquare[index]!=0)  delete [] mapSquare[index];
						}
					}
				}
			}
		}
	}
	for(int i0=0;i0<2;i0++){
		for(int i1=0;i1<2;i1++){
			for(int i2=0;i2<s_numEvalue;i2++){
				for(int i3=0;i3<s_numTvalue;i3++){
					for(int i4=0;i4<s_numPvalue;i4++){
						if( distRowE[i0][i1][i2][i3][i4] !=0 )
							delete distRowE[i0][i1][i2][i3][i4];
					}
				}
			}
		}
	}
#ifdef SHAPECHI_DEBUG
	canvas->Print((psfile+"]").c_str());
#endif
}


void ShapeChi2New::loadMap()
{


	float val[729][3];
	float val2[169][3];
	int id;


	std::string dirName = std::getenv("E14ANA_EXTERNAL_DATA") + std::string("/LegacyProjects/ShapeChi2/data");
	std::string mapName = dirName + "/ShapeChi2DB.root"; 
	TFile* f = new TFile(mapName.c_str());
	TTree* tr;

	tr = (TTree*) f->Get("mapFineCsi_f");
	tr->SetBranchAddress("id", &id);
	tr->SetBranchAddress("val", val);

	{
		// type 0
		int type=0;

		int c = 0;
		
		int size = s_mapSize;
		if(type==2)size/=2;
		for(int e=0;e<s_numEvalue;e++){
			for(int t=0;t<s_numTvalue;t++){
				for(int p=0;p<s_numPvalue;p++){
					for(int ctrx = 0;ctrx<s_numCtrBin;ctrx++){
						for(int ctry= 0;ctry<s_numCtrBin;ctry++){
							int index = getMapAddress(type,e,t,p,ctrx,ctry);
							mapNum[index] = new int[size*size];
							mapMean[index] = new float[size*size];
							mapSquare[index] = new float[size*size];
							
							tr->GetEntry(c);

							mapNum0[index] = id;

							int c2 = 0;
							for(int x=0;x<size;x++){
								for(int y=0;y<size;y++){
									mapNum[index][x*size+y] = val[c2][0];
									mapMean[index][x*size+y] = val[c2][1];
									mapSquare[index][x*size+y] = val[c2++][2];
							
							}}

							c++;
						}}}}}
	}
	
	delete tr;

	tr = (TTree*) f->Get("mapFineCsi_l");
	tr->SetBranchAddress("id", &id);
	tr->SetBranchAddress("val", val);

	{
		// type 1
		int type=1;

		int c = 0;

		int size = s_mapSize;
		if(type==2)size/=2;
		for(int e=0;e<s_numEvalue;e++){
			for(int t=0;t<s_numTvalue;t++){
				for(int p=0;p<s_numPvalue;p++){
					for(int ctrx = 0;ctrx<s_numCtrBin;ctrx++){
						for(int ctry= 0;ctry<s_numCtrBin;ctry++){
							int index = getMapAddress(type,e,t,p,ctrx,ctry);
							mapNum[index] = new int[size*size];
							mapMean[index] = new float[size*size];
							mapSquare[index] = new float[size*size];

							tr->GetEntry(c);

							mapNum0[index] = id;

							int c2 = 0;
							for(int x=0;x<size;x++){
								for(int y=0;y<size;y++){
									mapNum[index][x*size+y] = val[c2][0];
									mapMean[index][x*size+y] = val[c2][1];
									mapSquare[index][x*size+y] = val[c2++][2];
								}}

							c++;
						}}}}}
	}


	delete tr;


	tr = (TTree*) f->Get("mapLargeCsi");
	tr->SetBranchAddress("id", &id);
	tr->SetBranchAddress("val", val2);

	{
		// type 2
		int type=2;

		int c = 0;

		int size = s_mapSize;
		if(type==2)size/=2;
		for(int e=0;e<s_numEvalue;e++){
			for(int t=0;t<s_numTvalue;t++){
				for(int p=0;p<s_numPvalue;p++){
					for(int ctrx = 0;ctrx<s_numCtrBin;ctrx++){
						for(int ctry= 0;ctry<s_numCtrBin;ctry++){
							int index = getMapAddress(type,e,t,p,ctrx,ctry);
							mapNum[index] = new int[size*size];
							mapMean[index] = new float[size*size];
							mapSquare[index] = new float[size*size];

							tr->GetEntry(c);

							mapNum0[index] = id;

							int c2 = 0;
							for(int x=0;x<size;x++){
								for(int y=0;y<size;y++){
									mapNum[index][x*size+y] = val2[c2][0];
									mapMean[index][x*size+y] = val2[c2][1];
									mapSquare[index][x*size+y] = val2[c2++][2];
								}}

							c++;
						}}}}}
	}

	delete tr;

	f->Close();





	{
		std::string fname =std::getenv("E14ANA_EXTERNAL_DATA") + std::string("/LegacyProjects/ShapeChi2/data");
		fname+="/distRowEnergy.dat";
		std::ifstream ifs(fname.c_str());
		if(!ifs){
			std::cout<<"can't find "<<fname<<std::endl;
			exit(1);
		}
		for(int i0=0;i0<2;i0++){
			for(int i1=0;i1<2;i1++){
				for(int i2=0;i2<s_numEvalue;i2++){
					for(int i3=0;i3<s_numTvalue;i3++){
						for(int i4=0;i4<s_numPvalue;i4++){
							distRowE[i0][i1][i2][i3][i4] = new float[1000];
							for(int i=0;i<1000;i++)
								ifs>>distRowE[i0][i1][i2][i3][i4][i];
						}}}}}
		ifs.close();
	}

}



int ShapeChi2New::getMapAddress(int const &forl,int const &e,int const &t,
	int const &p,int const &x,int const &y) const{
	static int const coX=s_numCtrBin;   
	static int const coP=s_numCtrBin*s_numCtrBin;   
	static int const coT=s_numPvalue*s_numCtrBin*s_numCtrBin;   
	static int const coE=s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin;   
	static int const coFORL=s_numEvalue*s_numTvalue*s_numPvalue
		*s_numCtrBin*s_numCtrBin;   

	return coFORL*forl+coE*e+coT*t+coP*p+coX*x+y;
}


void ShapeChi2New::createHist(){
	for(int i=0;i<s_nHistogram;i++){
		m_histResult[i] = 0;
	}
	m_histData = new TH2D("histData","Edep(ratio)",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);
	m_histType = new TH2D("histType","csi type",s_mapSize,0,s_mapSize,s_mapSize,0,s_mapSize);

	int const nbin = 80; //  = 2000 / smallCsiWidth
	m_hist1DCsiSmall=new TH1D("hSmall","csi projection (small)",nbin,0,nbin);  
	m_hist1DCsiLarge=new TH1D("hLarge","csi projection (large)",nbin/2,0,nbin);  
	m_histCsiMap=new TH2D("histCsiMap","map of CsI.",nbin,0,nbin,nbin,0,nbin);
	for(int i=0;i<nbin+2;i++){
		for(int j=0;j<nbin+2;j++){
			m_histCsiMap->SetBinContent(i,j,NONEXIST);
		}
	}

	int numCsi = CsiMap::getCsiMap()->getN();
	int size=0;
	double x[3000],y[3000],w[3000];
	for(int i=0;i<numCsi;i++){
		CsiMap::getCsiMap()->getXYW(i,x[size],y[size],w[size]);
		if(w[size]<0) continue;
		size++;
	}

	double x0[3000],y0[3000],w0[3000];
	for(int i=0;i<size;i++){
		x0[i] = x[i];
		y0[i] = y[i];
		w0[i] = w[i];
	}

	double *pos[2] = {x,y};
	changeCoordinate(size,pos);
	for(int i=0;i<size;i++){
		double val  = (w[i]<30) ? SMALLTYPE : LARGETYPE;
		int bin = m_histCsiMap->FindBin(x[i],y[i]);
		m_histCsiMap->SetBinContent(bin,val);
	}

#ifdef SHAPECHI_DEBUG
	m_histCsiMap->Draw("colz");
	canvas->Print(psfile.c_str());
#endif

	//for check
	/*
	for(int phi=0;phi<33;phi++){
	m_histCsiMap->Reset();
	for(int i=0;i<nbin;i++){
	for(int j=0;j<nbin;j++){
	m_histCsiMap->SetBinContent(i+1,j+1,-1);
	}
	}
	for(int i=0;i<size;i++){
	x[i] = x0[i];
	y[i] = y0[i];
	w[i] = w0[i];
	}

	double phi=phi*(360/33.);
	double *pos[2] = {x,y};
	rotAndMirror(phi,size,pos);      
	changeCoordinate(size,pos);
	for(int i=0;i<size;i++){
	double val  = (w[i]<30) ? 1 : 10;
	m_histCsiMap->Fill(x[i],y[i],val);
	}
	m_histCsiMap->Draw("colz");
	canvas->Print(psfile.c_str());
	}
	*/

}


void ShapeChi2New::changeCoordinate(int size,double **csipos){
	double const originOfThisCoodinate = 1000;
	for(int i=0;i<size;i++){
		for(int ixy=0;ixy<2;ixy++){
			csipos[ixy][i] += originOfThisCoodinate;
			csipos[ixy][i] = csipos[ixy][i] / m_smallCsiWidth - 0.25;
		}
	}
}






void ShapeChi2New::shapeChi2( Gamma& gamma)
{

	for(int i=0;i<5;i++){
		m_param[i]=0;
		m_index[i]=0;
		m_percentage[i]= 0;
	}
	double logE = log(gamma.e());
	double theta = gamma.p3().theta()*TMath::RadToDeg();
	double phi = gamma.p3().phi()*TMath::RadToDeg();

	m_mode = -1;
	int size =0 ;
	static double *csiedep = new double[m_maxArrSize];
	static double *csiposx = new double[m_maxArrSize];
	static double *csiposy = new double[m_maxArrSize];
	static double *csiwidth = new double[m_maxArrSize];
	getCsiInformation(gamma,m_mode,size,csiedep,csiposx,csiposy,csiwidth);

	double* csipos[2]={csiposx,csiposy};
	rotAndMirror(phi,size,csipos);  
	changeCoordinate(size,csipos);

	double rowE[2][3];
	double ctrPos[2];
	for(int ixy=0;ixy<2;ixy++)
		searchForMaxRow(size,csipos[ixy],csiedep,ctrPos[ixy],rowE[ixy]);
	adjustCenter(size,csipos,ctrPos);
	//  searchBoundary(size,csipos,csiwidth);

	// set parameters to m_param
	m_param[0] = logE;
	m_param[1] = theta;
	m_param[2] = phi;
	for(int i=3;i<5;i++){
		int iXY = i-3;
		m_param[i] = (rowE[iXY][0]>rowE[iXY][2])
			? -rowE[iXY][0]/rowE[iXY][1] : rowE[iXY][2]/rowE[iXY][1];
	}

	// select map ( fill m_index and m_percentage )
	for(int i=0;i<s_nParam;i++)
		selectMap(i,m_param[i],m_index[i],m_percentage[i]);

	setMask(ctrPos);  
	setData(size,csipos,csiedep,csiwidth);  
	setMap();// use m_index,m_percentage

	calcChi2ForEachCsi();
	double chi2 = getChi2();
	gamma.setChisq(chi2);
}

void  ShapeChi2New::getCsiInformation(Gamma const & gamma, int &mode,int &size,double* &csiedep,double* &csiposx,double* &csiposy,double* &csiwidth){
	double const ene = gamma.e();
	int sizeAll = gamma.clusterIdVec().size();
	size = 0;
	bool isLarge = false,isSmall = false;
	for(int i=0;i<sizeAll;i++){
		csiedep[size]=gamma.clusterEVec()[i];
		if(csiedep[size]<m_threshold) continue;
		csiedep[size]/=ene;
		int id = gamma.clusterIdVec()[i];
		csiposx[size]=CsiMap::getCsiMap()->getX(id);
		csiposy[size]=CsiMap::getCsiMap()->getY(id);
		csiwidth[size]=CsiMap::getCsiMap()->getW(id);

		if(csiwidth[size]>30) isLarge = true;
		else isSmall = true;
		size++;
	}

	if(isLarge && isSmall) mode = SMALL_AND_LARGE;
	else if(isSmall) mode = SMALL_ONLY;
	else if(isLarge) mode = LARGE_ONLY;
	else{
		std::cout<<"error : not isLArge nor isSmall"<<std::endl;
		exit(1);
	}
}


void ShapeChi2New::rotAndMirror(double& phi,int const &csize,
	double * const *csipos)
{
	int rot = (int)(phi/90.+2.5);
	int mirror =((int)(phi/45.+5)%2*2-1);

	phi = (phi-(rot-2)*90)*mirror;
	for(int i = 0;i<csize;i++){
		double x = csipos[0][i];
		double y = csipos[1][i];      
		if(rot==1){
			csipos[0][i]=-y;      
			csipos[1][i]=x*mirror;
		}else if(rot==2){
			csipos[0][i]=x;
			csipos[1][i]=y*mirror;
		}else if(rot==3){
			csipos[0][i]=y;
			csipos[1][i]=-x*mirror;
		}else if(rot==4||rot==0){
			csipos[0][i]=-x;
			csipos[1][i]=-y*mirror;
		}else{
			std::cout<<"Err: rotAndMirrot"<<std::endl;
			exit(0);
		}
	}
}


void ShapeChi2New::searchForMaxRow(int size,double *csipos,double *csiedep,
	double &ctrPos,double rowE[3]){
	TH1D *hist;
	if(m_mode == SMALL_ONLY ){
		hist = m_hist1DCsiSmall;
	}else{
		hist = m_hist1DCsiLarge;
	}
	rowE[0]=rowE[1]=rowE[2] = -1;
	ctrPos = -1;
	hist->Reset();
	//  std::cout<<"fill:m_mode="<<m_mode<<std::endl;
	for(int i=0;i<size;i++){
		//    std::cout<<csipos[i]<<" "<<csiedep[i]<<std::endl;
		hist->Fill(csipos[i],csiedep[i]);
	}
	int maxBin = hist->GetMaximumBin();
	rowE[0] = hist->GetBinContent(maxBin-1);
	rowE[1] = hist->GetBinContent(maxBin);
	rowE[2] = hist->GetBinContent(maxBin+1);
	ctrPos = hist->GetBinLowEdge(maxBin);
	//  std::cout<<"ctrPos:"<<ctrPos<<std::endl;
#ifdef SHAPECHI_DEBUG
	hist->SetTitle(Form("projection to ctrPos=%g",ctrPos));
	hist->Draw();
	canvas->Print(psfile.c_str());
#endif
} 


void ShapeChi2New::adjustCenter(int size,double **csipos,double ctrPos[2]){
	for(int i=0;i<size;i++){
		for(int ixy=0;ixy<2;ixy++){
			if(m_mode==SMALL_ONLY)
				csipos[ixy][i] = csipos[ixy][i]-1e-3-ctrPos[ixy]+s_mapCtr;
			else
				csipos[ixy][i] = csipos[ixy][i]-1e-3-ctrPos[ixy]+s_mapCtr/2*2;
		}
	}
}


void ShapeChi2New::searchBoundary(int size,double **csipos,double *csiwidth){
	if(m_mode==LARGE_ONLY){
		m_lsBound[0] = m_lsBound[1] = 0;
	}else if(m_mode==SMALL_ONLY){
		m_lsBound[0] = m_lsBound[1] = s_mapSize;
	}else{
		m_lsBound[0] = m_lsBound[1] = 0;
		for(int i=0;i<size;i++){
			if(csiwidth[i]<30.){
				double x = csipos[0][i];
				double y = csipos[1][i];
				if(m_lsBound[0]<x) m_lsBound[0] = x+0.5;
				if(m_lsBound[1]<y) m_lsBound[1] = y+0.5;
			}
		}
	}
	std::cout<<"Bound:"<<m_lsBound[0]<<" "<<m_lsBound[1]<<std::endl;
}



void ShapeChi2New::setData(int size,double **csipos,double *csiedep,double* csiwidth){
	m_histData->Reset();
	for(int i=0;i<size;i++){
		int bin =m_histType->FindBin(csipos[0][i],csipos[1][i]);
		if(bin!=m_histData->FindBin(csipos[0][i],csipos[1][i])){
			std::cout<<"faa!?"<<std::endl;
			exit(1);
		}
		int binc = (int)(m_histType->GetBinContent(bin)+1e-3);
		if(binc==NONEXIST){
			std::cout<<"Ahhh!?"<<std::endl;
			exit(1);
		}else{
			if((binc==LARGETYPE && csiwidth[i] < 30.)
				||(binc==SMALLTYPE && csiwidth[i] > 30.)){
				std::cout<<"EEEE!?"<<std::endl;
				exit(1);
			}
		}
		m_histData->SetBinContent(bin,csiedep[i]);
	}
#ifdef SHAPECHI_DEBUG
	canvas->SetLogz();
	m_histData->Draw("colz");
	canvas->Print(psfile.c_str());
	canvas->SetLogz(0);
#endif

}


void  ShapeChi2New::setMap(){
	//  m_histMean->Reset();
	//  m_histRMS2->Reset();
	//  m_histProb->Reset();
	//  m_histNumHit->Reset();
	//  m_histNumGen->Reset();

	for(int i=0;i<s_mapSize;i++){
		for(int j=0;j<s_mapSize;j++){
			m_mean[i][j]=m_rms2[i][j]=m_prob[i][j]=m_numHit[i][j]=m_numGen[i][j]=0;
			int mapCoordinate,ForL;
			double mapVal = m_histType->GetBinContent(i+1,j+1);
			if(mapVal == NONEXIST ) continue;
			else if(mapVal == LARGETYPE ){
				int size = s_mapSize/2;
				int xi=(int)(i/2);
				int yi=(int)(j/2);
				ForL=2;
				if(xi>=size||xi<0) continue;
				if(yi>=size||yi<0) continue;
				mapCoordinate = xi*size+yi;      
				if(mapCoordinate>=size*size){
					std::cout<<"err: mapCoo>="<<size*size<<std::endl;
					exit(0);
				}
			}else if(mapVal == SMALLTYPE ){
				int xi=i;
				int yi=j;
				if(m_mode==SMALL_ONLY){
					ForL = 0;
					mapCoordinate = xi*s_mapSize+yi;      
				}else{
					if(xi>=s_mapSize-1||yi>=s_mapSize-1) continue;
					ForL = 1;
					mapCoordinate = (xi+1)*s_mapSize+(yi+1);      
				}
				if(mapCoordinate>=s_mapSize*s_mapSize){
					std::cout<<"err: mapCoo>="<<s_mapSize*s_mapSize<<std::endl;
					exit(0);
				}
			}else{
				std::cout<<"strange type"<<mapVal<<std::endl;
				exit(0);
			}

			double num0,num,mean,rms2;
			num0=num=mean=rms2=0;
			for(int ei=0;ei<2;ei++){
				double coeffE =TMath::Abs(ei-m_percentage[0]);
				for(int ti=0;ti<2;ti++){
					double coeffT =TMath::Abs(ti-m_percentage[1]);
					for(int pi=0;pi<2;pi++){
						double coeffP =TMath::Abs(pi-m_percentage[2]);
						double coeff=coeffE*coeffT*coeffP;
						int address=getMapAddress(ForL,m_index[0]+ei,m_index[1]+ti,
							m_index[2]+pi,m_index[3],m_index[4]);
						num0+=coeff*mapNum0[address];
						num+=coeff*mapNum[address][mapCoordinate];
						mean+=mapMean[address][mapCoordinate]*coeff;
						rms2+=(mapSquare[address][mapCoordinate]-pow(mapMean[address][mapCoordinate],2))*coeff;
					}
				}
			}
			if(num0==0){
				std::cout<<"error!"<<std::endl;
				exit(1);
			}
			m_mean[i][j] = mean;
			m_rms2[i][j] = rms2;
			m_prob[i][j] = num/num0;
			m_numHit[i][j] = num;
			m_numGen[i][j] = num0;

		}
	}
}


void ShapeChi2New::setMask(double *ctrPos){
	m_histType->Reset();
	int mapCtr = (m_mode==SMALL_ONLY)?s_mapCtr:s_mapCtr/2*2;
	for(int i=0;i<s_mapSize;i++){
		double x = i+0.5+ctrPos[0]-mapCtr;
		for(int j=0;j<s_mapSize;j++){
			double y = j+0.5+ctrPos[1]-mapCtr;
			int bin = m_histCsiMap->FindBin(x,y);
			double binc = m_histCsiMap->GetBinContent(bin);
			m_histType->SetBinContent(i+1,j+1,binc);
		}
	}
#ifdef SHAPECHI_DEBUG
	m_histType->Draw("colz");
	canvas->Print(psfile.c_str());
#endif

}


////////////////////////
//  private functions //
////////////////////////




void ShapeChi2New::selectMap(int parType,double const &val,int &index,double &percentage){
	// parType : 0:logE 1:theta 2:phi 3:ctrx 4:ctry 
	// 3,4 should be called after 0,1 and 2 are called.
	//  std::cout<<"parType,val:"<<parType<<" "<<val<<std::endl;
	static int const numValArr[3]={s_numEvalue,s_numTvalue,s_numPvalue};
	static double const minValArr[3]={log(100),0,0};
	static double const maxValArr[3]={log(2000),45.,45.};
	static double const deltaValArr[3] 
		={(maxValArr[0]-minValArr[0])/(numValArr[0]-1),
		(maxValArr[1]-minValArr[1])/(numValArr[1]-1),
		(maxValArr[2]-minValArr[2])/(numValArr[2]-1)};

	if(parType<3){
		int const numVal = numValArr[parType];
		double const minVal = minValArr[parType];
		double const maxVal = maxValArr[parType];
		double const delVal = deltaValArr[parType];

		if(parType == 2 && (val<minVal-1e-7||val>maxVal+1e-7)){
			std::cout<<"error : shape Chi2 Calculation"<<std::endl;
			std::cout<<"parType,val:"<<parType<<" "<<val<<std::endl;
			exit(0);
		}

		if(val<minVal){
			index=0;
			percentage=1;
		}else{
			double div=(val-minVal)/delVal;
			index=(int)(div);
			if(index>=numVal-1){
				index=numVal-2;
				percentage=0;
			}else{
				percentage=1-fmod(div,1);
			}
		}
	}else{
		if(fabs(val)<1e-7){
			index=s_numCtrBin/2;
		}else{
			int mode = (m_mode == SMALL_ONLY) ? 0 : 1;
			double const hmin = -1;
			double const hwidth = 0.002;
			int const xy = parType-3;
			int bin = (int)((val-hmin)/hwidth);      

                        const int max_bin = 1000;
                        if( bin >= max_bin ){
                           std::cout<<" Warning in <ShapeChi2New::selectMap> " 
                                    <<" try to access the inexistent bin: " << bin << std::endl;
                           bin = max_bin - 1;
                        }
 
			float ctrPosAverage=0;
			for(int ei=0;ei<2;ei++){
				double coeffE =TMath::Abs(ei-m_percentage[0]);
				for(int ti=0;ti<2;ti++){       
					double coeffT =TMath::Abs(ti-m_percentage[1]);
					for(int pi=0;pi<2;pi++){
						double coeffP =TMath::Abs(pi-m_percentage[2]);
						float ctrPos=distRowE[mode][xy][m_index[0]+ei][m_index[1]+ti][m_index[2]+pi][bin];
						ctrPosAverage+=coeffE*coeffT*coeffP*ctrPos;
					}
				}
			}
			index =(int)(ctrPosAverage*s_numCtrBin);
			if(index>s_numCtrBin){std::cout<<"e"<<std::endl;exit(0);}
			if(index==s_numCtrBin){
				percentage = 0;
				index--;
			}else{
				percentage = 1-(ctrPosAverage*s_numCtrBin-index);
				//	std::cout<<ctrPosAverage<<" "<<index<<" "<<percentage<<std::endl;
			}
		}
	}
}


void ShapeChi2New::calcChi2ForEachCsi(){
	double const ProhibitValue = 10000;
	//  std::cout<<"calcChi2"<<std::endl;
	for(int i=0;i<s_mapSize;i++){
		for(int j=0;j<s_mapSize;j++){
			m_isHitCsi[i][j] = 0;
			m_chi2Gaus[i][j]=m_chi2BinVV[i][j]=m_chi2BinLL[i][j] = 0;

			int type =(int)(m_histType->GetBinContent(i+1,j+1)+1e-3);
			if(type == NONEXIST){
				m_isHitCsi[i][j] = -1;
				continue;
			}

			double dat = m_histData->GetBinContent(i+1,j+1);
			double prob = m_prob[i][j];
			double mean = m_mean[i][j];
			double rms2 = m_rms2[i][j];

			m_isHitCsi[i][j] = (dat>1e-6)?1:0;
			double stat = m_numHit[i][j];
			double const minimumStat = 20;
			if(m_isHitCsi[i][j]==1){ // hit CsI
				m_chi2Gaus[i][j] = (stat < minimumStat)
					? ProhibitValue : pow(dat-mean,2)/rms2;
				//	if(stat>=30) std::cout<<"x,y,dat,mean,sigma:"<<i<<" "<<j<<" "<<dat<<" "<<mean<<" "<<rms2<<" "<<m_chi2Gaus[i][j]<<std::endl;
				if(stat<1){
					m_chi2BinVV[i][j]=m_chi2BinLL[i][j]=ProhibitValue;
				}else{
					m_chi2BinVV[i][j] = (1-prob)/prob;
					m_chi2BinLL[i][j] = (prob==1)
						? 1 : 1-2*(1-prob)*log(prob/(1-prob));
				}
			}else{	// no hit CsI
				m_chi2Gaus[i][j] = (stat < minimumStat)
					? -1 : pow(0-mean,2)/rms2;
				if(stat<1){
					m_chi2BinVV[i][j] = 0; 
					m_chi2BinLL[i][j] = 1;
				}else{
					if(prob == 1){
						m_chi2BinVV[i][j] = m_chi2BinLL[i][j] = ProhibitValue;   
					}else{
						m_chi2BinVV[i][j] = prob/(1-prob);
						m_chi2BinLL[i][j] = 1-2*prob*log((1-prob)/prob);
					}
				}
			}
		}
	}
}


double ShapeChi2New::getChi2(int method,int mapSizeHit,double mapSizeNoneD){
	double const prohibitValue = 10000;
	m_chi2Mode=method;
	m_hitMapSize = mapSizeHit;
	m_nonHitMapSize = mapSizeNoneD;
	if(method!=SIMPLE&&method!=VV&&method!=LL){
		std::cout<<"error: method type "<<method<<" is not defined."<<std::endl; 
		return prohibitValue;
	}

	if(mapSizeHit%3!=0){
		std::cout<<"error: map size for hit CsI should be a multiple number of 3."<<std::endl; 
		return prohibitValue;
	}
	int centerHit = mapSizeHit/3;

	int mapSizeNoneI = (int)mapSizeNoneD;
	if(mapSizeNoneI==0){
		mapSizeNoneI=s_mapSize;
	}else if(mapSizeNoneD>1){
		if(fabs(mapSizeNoneD-mapSizeNoneI)>1e-6 || mapSizeNoneI%3!=0){
			std::cout<<"error:map size for non-hit CsI should be a multiple number of 3 or a decimal number less than 1."<<std::endl;
			return prohibitValue;
		}
	}
	int centerNone = mapSizeNoneI/3;

	int ndf = -1;
	double chi2=0;
	for(int i=0;i<s_mapSize;i++){
		for(int j=0;j<s_mapSize;j++){
			m_localChi2[i*s_mapSize+j] = 0;
			m_isUse[i][j] = 0;
			if(m_isHitCsi[i][j]==-1){
				//	std::cout<<"koko"<<std::endl;
				continue;
			}
			if(m_isHitCsi[i][j]==1){
				if( i < s_mapCtr-centerHit || i >= s_mapCtr + 2*centerHit ||
					j < s_mapCtr-centerHit || j >= s_mapCtr + 2*centerHit ) continue;
				m_localChi2[i*s_mapSize+j]=m_chi2Gaus[i][j];
				//	std::cout<<"i,j,m_chi2Gaus,chi2:"<<i<<" "<<j<<" "<<m_chi2Gaus[i][j]<<" "<<chi2<<std::endl;
				if(method==VV){
					m_localChi2[i*s_mapSize+j]+=m_chi2BinVV[i][j];
				}else if(method==LL){
					m_localChi2[i*s_mapSize+j]+=m_chi2BinLL[i][j];
				}
			}else{
				if( i < s_mapCtr-centerNone || i >= s_mapCtr + 2*centerNone ||
					j < s_mapCtr-centerNone || j >= s_mapCtr + 2*centerNone ) continue;
				double prob = m_prob[i][j];
				if(mapSizeNoneD<1&&prob<mapSizeNoneD) continue;
				if(method==SIMPLE){
					if(m_chi2Gaus[i][j] == -1) continue;
					m_localChi2[i*s_mapSize+j]=m_chi2Gaus[i][j];
				}else if(method==VV){
					m_localChi2[i*s_mapSize+j]=m_chi2BinVV[i][j];
				}else if(method==LL){
					m_localChi2[i*s_mapSize+j]=m_chi2BinLL[i][j];
				}
			}
			chi2+=m_localChi2[i*s_mapSize+j];
			m_isUse[i][j] = 1;
			ndf++;
		}
	}
	if(ndf<1) return prohibitValue;
	return chi2/ndf;
}

