#include <rec2g/Rec2g.h>
#include <gnana/E14GNAnaFunction.h>
#include <csimap/CsiMap.h>
#include "ShapeChi2/GammaAnalyzer.h"
#include "TChain.h"
using namespace iwai;

class MyPolynomial {
public:
  double a[5];
  double b[5];
  double c[3];
  double a3,c3,P,Q;
  double z[3];
  double zz;	
  double cb;
  std::vector<double> roots;

  MyPolynomial(double aa[]){
    roots.clear();
    for(int i=0;i<5;i++){
      a[i]=aa[i];
    }
    cb=1./3;
    for(int i=0;i<4;i++){
      b[i]=a[i+1]/a[0];
    }
    c[0]=-b[1];
    c[1]=b[2]*b[0]-4.0*b[3];
    c[2]=b[3]*(4.0*b[1]-b[0]*b[0])-b[2]*b[2];
    
    P=(c[1]-c[0]*c[0]/3.0)/3.0;
    Q=(c[2]-c[0]*c[1]/3.0+2.0*pow(c[0], 3)/27.0)/2.0;
  }

  double Kai_z(){
    double z=0;
    if(c[2]==0.0){
      z=c[0]/3.0;
    }else if(TMath::Abs(Q)<1e-8){
      z=0.0;
    }else if(Q>0 ){
      z=Kai_zz();
    }else if(Q<0){
      Q=-Q;
      z=-Kai_zz();   		
    }
    return z;
  }
  
  double Kai_zz(){
    double zz=0;
    double DD=pow(P,3)+Q*Q;
    if(DD<0){
      zz=-2*sqrt(-P)*cos(atan(sqrt(-DD)/Q)/3.0);
    }else if(TMath::Abs(DD)<1e-8){
      zz=-2.0*pow(Q,cb);
    }else if(DD>0){
      zz=Kai_P(DD);
    }
    return zz;
  }
  
  double Kai_P(double D){
    double z=0;
    if(P<0){
      z=-pow((Q+sqrt(D)),cb)-pow((Q-sqrt(D)),cb);
    }else if(P ==0){
      z=-pow((2.0*Q),cb);
    }else if(P>0){
      z=pow((sqrt(D)-Q),cb)-pow((sqrt(D)+Q),cb);
    }
    return z;
  }
  
  double Kai_all(){
    double zzz,za,zb,k;
    k=Kai_z();
    if(c[2]==0.0){
      z[0]=k;
      za=-c[0]/2.0;
      double dd=sqrt(c[0]*c[0]-4.0*c[1]);
      zb=dd/2.0;
      if(dd>=0.0){
	z[1]=za+zb;
	z[2]=za-zb;
      }else if(TMath::Abs(dd)<1e-8){
	z[1]=z[2]=za;
      }else{
	z[1]=z[2]=k;
      }			
    }else{
      double d=k*k-4.0*(3.0*P+k*k);
      za=-k/2.0;
      if(d>0){
	zb=sqrt(d)/2.0;			
	z[1]=za+zb;
	z[2]=za-zb;
      }else if(d==0){
	z[1]=z[2]=za;
      }else if(d<0){
	z[0]=z[1]=z[2]=k;
      }
    }
    if(TMath::Abs(z[0])<TMath::Abs(z[1])){
      zzz=z[1];			
    }else{zzz=z[0];}
    if(TMath::Abs(zzz)<TMath::Abs(z[2])){
      zzz=z[2];
    }
    return zzz-c[0]/3.0;
  }
  
  std::vector<double> FindRealRoots(){
    roots.clear();
    double A,B,C,D,kon,hantei1,hantei2;
    kon=Kai_all();
    A=b[0]/2.0;
    B=kon/2.0;
    hantei1=B*B-b[3];
    hantei2=A*A-b[1]+kon;
    if((hantei1>=0.0)&&(hantei2>=0.0)){ 
      D=sqrt(hantei1);	    
      if(D<1e-8){
	C=sqrt(hantei2);
      }else{
	C=(-b[2]/2.0+A*B)/D;
      }
      NijiHouteisiki(A-C,B-D);
      NijiHouteisiki(A+C,B+D);    	
    }
    //    std::cout<<"root:";
    //    for(int i=0;i<roots.size();i++){
    //      std::cout<<roots[i]<<" ";
    //    }
    //    std::cout<<std::endl;
    return roots;
  }
	
  void NijiHouteisiki(double b,double c){
    //x^2+bx+c=0
    double y1,y2;
    double EPS=1e-5;
    double d=b*b-4.0*c; 		
    if(TMath::Abs(d)<EPS) d=0.0;
    y1=-b/2.0;
    if(TMath::Abs(y1)<EPS) y1=0.0;

    if(d>EPS){
      y2=sqrt(d)/2.0;
      //printf("%10.4f %10.4f\n",y1+y2,y1-y2);         
      roots.push_back(y1+y2);
      roots.push_back(y1-y2);
    }
    else if(d<0.0){
      y2=sqrt(-d)/2.0;
      //      printf("complex : %10.4f �� %10.4f i\n",y1,y2);
    }       
    else{
      //      printf("%10.4f %10.4f\n",y1,y1);
      roots.push_back(y1);
    }
  }
};	


double iwai::shapeLikelihood(std::list<Pi0> plist,double likelihood[2][2][2]){
  double tcut[2];
  for(int i=0;i<2;i++){
    for(int j=0;j<2;j++){
      tcut[j] = 18.;
      for(int k=0;k<2;k++){
	likelihood[i][j][k]=10; //[pi or K][gammaID][xy]
      }
    }
  }

  static GammaAnalyzer *ga = new GammaAnalyzer();
  std::list<Gamma> glist;
  for ( int i=0;i<2;i++ ){
    Gamma &g=(i==0)?plist.front().g1():plist.front().g2();
    ga->Analyze(g);
    likelihood[0][i][0] = ga->likelihood[0];
    likelihood[0][i][1] = ga->likelihood[1];
    glist.push_back(g);
    double le = log10(g.e());
    if ( le<2.55 ){ tcut[i]=8.+10./0.55*(le-2.); }
  }
    
  double mass=Rec2g::M_KL;
  std::list<Pi0> klList=rec2gWithConstM(glist,mass,kFALSE);
  if ( klList.size()==1 ){
    Pi0 kl=klList.front();
    klList.clear();
                                
    // position correction for angle dependency
    E14GNAnaFunction::getFunction()->correctPosition(kl.g1());  
    E14GNAnaFunction::getFunction()->correctPosition(kl.g2());  
                                
    glist.clear();
    glist.push_back(kl.g1());
    glist.push_back(kl.g2());
                                
    double recZ=kl.recZ();
    klList=rec2gWithConstM(glist,mass,recZ);
    if ( klList.size()==1 ){ kl=klList.front(); }
    klList.clear();
    
    glist.clear();
    for ( int i=0;i<2;i++ ){
      Gamma &g=(i==0)?kl.g1():kl.g2();
      ga->Analyze(g);
      likelihood[1][i][0] = ga->likelihood[0];
      likelihood[1][i][1] = ga->likelihood[1];
      glist.push_back(g);
    }
    
    recZ=kl.recZ();
    klList=rec2gWithConstM(glist,mass,recZ);
    if ( klList.size()==1 ){ kl=klList.front(); }
    klList.clear();
                                
    klList.push_back(kl);
  }else{
    return 1;
  }

  return getLikelihoodRatio(likelihood,tcut);
}


double iwai::getLikelihoodRatio(double likelihood[2][2][2],double tcut[2]){
  double lSum[2];
  for ( int i=0;i<2;i++ ){
    lSum[i]=1.;
    for ( int j=0;j<2;j++ ){
      if ( likelihood[i][j][0]>=-1.e-6 ){ lSum[i]*=likelihood[i][j][0]; }
      if ( likelihood[i][j][1]>=-1.e-6 ){ lSum[i]*=likelihood[i][j][1]; }
    }
  }
  
  bool b_cut=kFALSE;
  for ( int i=0;i<2;i++ ){
    for ( int j=0;j<2;j++ ){
      if ( likelihood[0][i][j]<0 ){ continue; }
      if ( -TMath::Log10(likelihood[0][i][j])>tcut[i] ){ b_cut=kTRUE; }
    }
  }
  if ( b_cut ){ return 0; }

  if ( lSum[0]+lSum[1]!=0. ){
    double llRatio = lSum[0]/(lSum[0]+lSum[1]);
    return llRatio;
  }

  return 1;

}


std::list<Pi0> iwai::rec2gWithConstM(std::list<Gamma> glist, double mass, bool b_beamAxis){
	std::list<Gamma>* pGlist;
	
	double totE=0.;
	CLHEP::Hep3Vector wm;
	std::list<Gamma> tglist;
	if ( !b_beamAxis ){
		for( std::list<Gamma>::const_iterator g=glist.begin();g!=glist.end(); g++ ){
			totE+=g->e();
			wm+=g->e()*g->pos();
			tglist.push_back(*g);
		}
		wm/=totE;
		wm.setZ(0.);
		for( std::list<Gamma>::iterator g=tglist.begin();g!=tglist.end(); g++ ){ g->setPos(g->pos()-wm); }
		
		pGlist=&tglist;
	}else{
		pGlist=&glist;
	}
  
	Rec2g rec2g;
	std::list<Pi0> m2gList=rec2g.recPi0withConstM(*pGlist,mass);
	if ( !b_beamAxis ){
		for ( std::list<Pi0>::iterator m=m2gList.begin();m!=m2gList.end();m++ ){
			m->g1().pos()+=wm;
			m->g2().pos()+=wm;
			m->v()+=wm;
		}
	}
	return m2gList;
}

std::list<Pi0> iwai::rec2gWithConstM(std::list<Gamma> glist, double mass, double aproxZ){
	
	std::list<Pi0> m2gList;
	if ( glist.size()!=2 ){ return m2gList; }
	
	int nGamma=0;
	Gamma g[2];
	for ( std::list<Gamma>::const_iterator tg=glist.begin();tg!=glist.end();tg++ ){
		g[nGamma++]=*tg;
	}
	
	CLHEP::Hep3Vector tr=g[1].pos()-g[0].pos();
	double deltaE2=TMath::Power(g[1].e(),2)-TMath::Power(g[0].e(),2);
	double E2Z2=TMath::Power(g[1].e(),2)*TMath::Power(g[0].z()-aproxZ,2);
	
	double par[5];
	par[0]=deltaE2*tr.mag2();
	par[1]=-2*deltaE2*tr.mag2();
	par[2]=deltaE2*(tr.mag2()+TMath::Power(g[0].z()-aproxZ,2));
	par[3]=-2*E2Z2;
	par[4]=E2Z2;
	
	//	ROOT::Math::Polynomial rf(par[0],par[1],par[2],par[3],par[4]);
	MyPolynomial rf(par);
	std::vector<double> candidate=rf.FindRealRoots();

	
	std::vector<double> answer;
	for ( int i=0;i<(int)candidate.size();i++ ){
		if ( 0<=candidate[i] && candidate[i]<=1 ){ answer.push_back(candidate[i]); }
	}
	if ( answer.size()!=1 ){ return m2gList; }
	
	CLHEP::Hep3Vector wm=g[0].pos()+answer[0]*tr;
	wm.setZ(0);
	std::list<Gamma> tglist;
	for ( int i=0;i<2;i++ ){
		g[i].setPos(g[i].pos()-wm);
		tglist.push_back(g[i]);
	}
  
	Rec2g rec2g;
	m2gList=rec2g.recPi0withConstM(tglist,mass);
	for ( std::list<Pi0>::iterator m=m2gList.begin();m!=m2gList.end();m++ ){
		m->g1().pos()+=wm;
		m->g2().pos()+=wm;
		m->v()+=wm;
	}
	return m2gList;
}

const int GammaAnalyzer::m_nPDF[2]={500,250}; // small/large

GammaAnalyzer::GammaAnalyzer(void):m_xMin(-150.),m_xMax(350.){
	// initialize objects
	
	
  //	TFile* tf=new TFile("/Users/sato/local/Gsim4/e14_20130107/e14/share/test/GammaAnalyzer_ShowerShapeTemplate.root");
  //	TTree* ttr=(TTree*)tf->Get("configTree");
  TChain* ttr = new TChain("configTree");
  ttr->Add("/home/had/kazufumi/forSato3/GammaAnalyzer_ShowerShapeTemplate.root");
  std::cout<<"start reading template"<<std::endl;
	const int tN=1950;
	double x[tN], eRatio[tN];
	ttr->SetBranchAddress("x",x);
	ttr->GetEntry(0);
	//	TTree* tr=(TTree*)tf->Get("templateTree");
	TChain* tr= new TChain("templateTree");
	tr->Add("/home/had/kazufumi/forSato3/GammaAnalyzer_ShowerShapeTemplate.root");
	tr->SetBranchAddress("eRatio",eRatio);
	
	// shower shape templates
	for ( int i=0;i<m_nEne;i++ ){
		for ( int j=0;j<m_nTheta;j++ ){
			for ( int k=0;k<m_nPhi;k++ ){
				tr->GetEntry((i*m_nTheta+j)*m_nPhi+k);
				m_sp[i][j][k]=new TSpline3(Form("sp%d_%d_%d",i,j,k),x,eRatio,tN);
			}
		}
	}
 	delete ttr;
 	delete tr;
	std::cout<<"finish reading template"<<std::endl;
	// row energy to analyze
	const int nBin=64;
	double bin[nBin+1];
	bin[0]=-40*m_CsISize;
	for ( int i=1;i<=nBin;i++ ){
		if ( i<=8 || i>56 ){ bin[i]=bin[i-1]+2*m_CsISize; }
		else{ bin[i]=bin[i-1]+m_CsISize;}
	}
	for ( int i=0;i<2;i++ ){
		m_rowEnergy[0][i]=new TH1F(Form("rowEnergySL_%d",i),"",nBin,bin);
		m_rowEnergy[1][i]=new TH1F(Form("rowEnergyL_%d",i),"",40,-40*m_CsISize,40*m_CsISize);
	}
	
	// dummy histograms
	m_hDummy[0]=new TH1F("m_hDummyE","",m_nEne-1,m_eArray);
	m_hDummy[1]=new TH1F("m_hDummyTheta","",m_nTheta-1,m_thArray);
	double phiBin[m_nPhi];
	for ( int i=0;i<m_nPhi;i++ ){ phiBin[i]=TMath::ACos(1-1./(m_nPhi-1)*i)*TMath::RadToDeg(); }
	m_hDummy[2]=new TH1F("m_hDummyPhi","",m_nPhi-1,phiBin);
	
	// row energy fitting function
	m_f1=new TF1("m_f1",iwai::rowEnergyFunction,-1000.,1000.,3);
	
	/* likelihood calculation */
	// allocate a big array to store the pdf information
	m_pdfArray=new int*****[2];
	for ( int i=0;i<2;i++ ){
		m_pdfArray[i]=new int****[m_nEne];
		for ( int ne=0;ne<m_nEne;ne++ ){
			m_pdfArray[i][ne]=new int***[m_nTheta];
			for ( int nt=0;nt<m_nTheta;nt++ ){
				m_pdfArray[i][ne][nt]=new int**[m_nPhi];
				for ( int np=0;np<m_nPhi;np++ ){ m_pdfArray[i][ne][nt][np]=new int*[m_nPDF[i]]; }
			}
		}
	}
	m_pdfArray[0][0][0][0][0]=new int[m_nEne*m_nTheta*m_nPhi*(m_nPDF[0]+m_nPDF[1])*m_nBin];
	for ( int i=0;i<2;i++ ){
		if ( i==1 ){ m_pdfArray[i][0][0][0][0]=m_pdfArray[0][0][0][0][0]+i*m_nEne*m_nTheta*m_nPhi*m_nPDF[0]*m_nBin; }
		for ( int ne=0;ne<m_nEne;ne++ ){
			m_pdfArray[i][ne][0][0][0]=m_pdfArray[i][0][0][0][0]+ne*m_nTheta*m_nPhi*m_nPDF[i]*m_nBin;
			for ( int nt=0;nt<m_nTheta;nt++ ){
				m_pdfArray[i][ne][nt][0][0]=m_pdfArray[i][ne][0][0][0]+nt*m_nPhi*m_nPDF[i]*m_nBin;
				for ( int np=0;np<m_nPhi;np++ ){
					m_pdfArray[i][ne][nt][np][0]=m_pdfArray[i][ne][nt][0][0]+np*m_nPDF[i]*m_nBin;
					for ( int nn=0;nn<m_nPDF[i];nn++ ){ m_pdfArray[i][ne][nt][np][nn]=m_pdfArray[i][ne][nt][np][0]+nn*m_nBin; }
				}
			}
		}
	}
	
	//	TFile* pdfF=new TFile("/Users/sato/local/Gsim4/e14_20130107/e14/share/test/GammaAnalyzer_ShowerShapePDF.root");
	//	TTree* pdfTree=(TTree*)pdfF->Get("pdfTree");
	std::cout<<"start reading PDF"<<std::endl;
	TChain* pdfTree=new TChain("pdfTree");
	pdfTree->Add("/home/had/kazufumi/forSato3/GammaAnalyzer_ShowerShapePDF.root");
	pdfTree->SetBranchAddress("pdfS",m_pdfArray[0][0][0][0][0]);
	pdfTree->SetBranchAddress("pdfL",m_pdfArray[1][0][0][0][0]);
	pdfTree->GetEntry(0);
	delete pdfTree;
	//	delete pdfF;
	std::cout<<"finish reading PDF"<<std::endl;
	// dummy histograms for likelihood calculation
	for ( int ne=0;ne<m_nEne;ne++ ){
		double tBin[m_nBin-1];
		for ( int i=0;i<=m_nBin-2;i++ ){ tBin[i]=m_rowThres/m_eArray[ne]*TMath::Exp(TMath::Log(m_eArray[ne]/m_rowThres)/(m_nBin-2)*i); }
		m_hDummy2[ne]=new TH1F(Form("m_hDummy2_%d",ne),"",m_nBin-2,tBin);
	}
	
}

GammaAnalyzer::~GammaAnalyzer(void){
  /*
    for ( int i=0;i<m_nEne;i++ ){
		for ( int j=0;j<m_nTheta;j++ ){
			for ( int k=0;k<m_nPhi;k++ ){ delete m_sp[i][j][k]; }
		}
	}
	
	for ( int i=0;i<2;i++ ){ for ( int j=0;j<2;j++ ){ delete m_rowEnergy[i][j]; } }
	for ( int i=0;i<3;i++ ){ delete m_hDummy[i]; }
	delete m_f1;
	
	delete[] m_pdfArray[0][0][0][0][0];
	for ( int ne=0;ne<m_nEne;ne++ ){ delete m_hDummy2[ne]; }
  */
}

void GammaAnalyzer::Analyze(Gamma& g, bool updatePos){
  //  std::cout<<"call Analyze"<<std::endl;
  likelihood[0]=-1.;
  likelihood[1]=-1.;
  //nFill[0]=-1;
  //nFill[1]=-1;
  
  for ( int i=0;i<2;i++ ){
    for ( int j=0;j<2;j++ ){ m_rowEnergy[i][j]->Reset(); }
  }
  //  std::cout<<"get CsiMap"<<std::endl;
  CsiMap* cmap=CsiMap::getCsiMap();
  //  std::cout<<"a"<<std::endl;
  //  std::cout<<g.clusterIdVec().size()<<std::endl;
  std::vector<int> tclsIdVec=g.clusterIdVec();
  //  std::cout<<"a"<<std::endl;
  std::vector<double> tclsEVec=g.clusterEVec();
  //  std::cout<<"a"<<std::endl;
  for ( int i=0;i<(int)tclsIdVec.size();i++ ){
    int tIdx=(cmap->getW(tclsIdVec[i])==50.&&TMath::Abs(cmap->getX(tclsIdVec[i]))<24*m_effCsISize)?1:0;
    m_rowEnergy[tIdx][0]->Fill(cmap->getX(tclsIdVec[i]),tclsEVec[i]);
    tIdx=(cmap->getW(tclsIdVec[i])==50.&&TMath::Abs(cmap->getY(tclsIdVec[i]))<24*m_effCsISize)?1:0;
    m_rowEnergy[tIdx][1]->Fill(cmap->getY(tclsIdVec[i]),tclsEVec[i]);
  }
  //  std::cout<<"b"<<std::endl;
  //TH1* th[2];
  double te=g.e();
  double tx[2]={g.x(),g.y()};
  double tTheta=g.p3().theta()*TMath::RadToDeg(); // [deg]
  double tPhi=g.p3().phi()*TMath::RadToDeg(); // [deg]
  for ( int i=0;i<2;i++ ){
    int tID=-1;
    if ( m_rowEnergy[0][i]->Integral()==0. ){ tID=1; }
    else if ( m_rowEnergy[1][i]->Integral()==0. ){ tID=0; }
    else if ( m_rowEnergy[1][i]->Integral()/(m_rowEnergy[0][i]->Integral()+m_rowEnergy[1][i]->Integral())>m_leakCut ){
      tID=1;
      for ( int j=1;j<=m_rowEnergy[0][i]->GetNbinsX();j++ ){
	m_rowEnergy[1][i]->Fill(m_rowEnergy[0][i]->GetBinCenter(j),m_rowEnergy[0][i]->GetBinContent(j));
      }
    }else{ tID=0; }
    //    std::cout<<"c"<<std::endl;    
    int tnFill=0;
    th[i]=m_rowEnergy[tID][i];
    for ( int j=1;j<=th[i]->GetNbinsX();j++ ){
      if ( th[i]->GetBinContent(j)>5. ){ tnFill++; }
      th[i]->SetBinContent(j,th[i]->GetBinContent(j));
    }
    //nFill[i]=tnFill;
    //if ( tnFill<=2 ){ continue; }
    if ( tnFill<2 ){ continue; }
    
    // m_idxW[0][] : energy
    if ( te<m_hDummy[0]->GetBinLowEdge(1) ){ m_idxW[0][0]=0; m_idxW[0][1]=1.; m_idxW[0][2]=0.; }
    else if ( te>=m_hDummy[0]->GetXaxis()->GetBinUpEdge(m_hDummy[0]->GetNbinsX()) ){ m_idxW[0][0]=m_hDummy[0]->GetNbinsX()-1; m_idxW[0][1]=0.; m_idxW[0][2]=1.; }
    else{
      m_idxW[0][0]=m_hDummy[0]->Fill(te)-1;
      m_idxW[0][1]=TMath::Log(m_hDummy[0]->GetBinLowEdge(m_idxW[0][0]+2))-TMath::Log(te);
      m_idxW[0][2]=TMath::Log(te)-TMath::Log(m_hDummy[0]->GetBinLowEdge(m_idxW[0][0]+1));
    }
    //    std::cout<<"d"<<std::endl;    
    // m_idxW[1][] : theta
    if ( tTheta<m_hDummy[1]->GetBinLowEdge(1) ){ m_idxW[1][0]=0; m_idxW[1][1]=1.; m_idxW[1][2]=0.; }
    else if ( tTheta>=m_hDummy[1]->GetXaxis()->GetBinUpEdge(m_hDummy[1]->GetNbinsX()) ){ m_idxW[1][0]=m_hDummy[1]->GetNbinsX()-1; m_idxW[1][1]=0.; m_idxW[1][2]=1.; }
    else{
      m_idxW[1][0]=m_hDummy[1]->Fill(tTheta)-1;
      //m_idxW[1][1]=m_hDummy[1]->GetBinLowEdge(m_idxW[1][0]+2)-tTheta;
      //m_idxW[1][2]=tTheta-m_hDummy[1]->GetBinLowEdge(m_idxW[1][0]+1);
      m_idxW[1][1]=TMath::Sin(m_hDummy[1]->GetBinLowEdge(m_idxW[1][0]+2)*TMath::DegToRad())-TMath::Sin(tTheta*TMath::DegToRad());
      m_idxW[1][2]=TMath::Sin(tTheta*TMath::DegToRad())-TMath::Sin(m_hDummy[1]->GetBinLowEdge(m_idxW[1][0]+1)*TMath::DegToRad());
    }
    // m_idxW[2][] : Phi
    double ttPhi=0;
    if ( i==0 ){ // x
      if ( TMath::Abs(tPhi)<90 ){ ttPhi=TMath::Abs(tPhi); m_idxW[3][1]=1.; }
      else{ ttPhi=180-TMath::Abs(tPhi); m_idxW[3][1]=-1.; }
    }else{ // y
      ttPhi=TMath::Abs(90-TMath::Abs(tPhi));
      if ( tPhi>0 ){ m_idxW[3][1]=1.; }
      else{ m_idxW[3][1]=-1.; }
    }
    //    std::cout<<"e"<<std::endl;    
    if ( ttPhi<m_hDummy[2]->GetBinLowEdge(1) ){ m_idxW[2][0]=0; m_idxW[2][1]=1.; m_idxW[2][2]=0.; }
    else if ( ttPhi>=m_hDummy[2]->GetXaxis()->GetBinUpEdge(m_hDummy[2]->GetNbinsX()) ){ m_idxW[2][0]=m_hDummy[2]->GetNbinsX()-1; m_idxW[2][1]=0.; m_idxW[2][2]=1.; }
    else{
      m_idxW[2][0]=m_hDummy[2]->Fill(ttPhi)-1;
      m_idxW[2][1]=TMath::Cos(ttPhi*TMath::DegToRad())-TMath::Cos(m_hDummy[2]->GetBinLowEdge(m_idxW[2][0]+2)*TMath::DegToRad());
      m_idxW[2][2]=TMath::Cos(m_hDummy[2]->GetBinLowEdge(m_idxW[2][0]+1)*TMath::DegToRad())-TMath::Cos(ttPhi*TMath::DegToRad());
    }
    // m_idxW[3][] : misc
    m_idxW[3][0]=tID; // use Large
    //    std::cout<<"f"<<std::endl;    
    if ( updatePos ){
      double trng[2];
      int tn=1;
      while ( th[i]->GetBinContent(tn)<5. ){ tn++; }
      if ( tn==1 ){ tn++; }
      trng[0]=th[i]->GetBinLowEdge(tn-1);
      tn=th[i]->GetNbinsX()-1;
      while ( th[i]->GetBinContent(tn)<5. ){ tn--; }
      if ( tn+1>th[i]->GetNbinsX() ){ tn=th[i]->GetNbinsX()-1; }
      trng[1]=th[i]->GetBinLowEdge(tn+2);
      
      m_f1->SetParameters(1.,tx[i],th[i]->Integral());
      m_f1->SetParLimits(1,trng[0]-120.,trng[1]+120.);
      m_f1->FixParameter(2,th[i]->Integral());
      m_f1->SetRange(trng[0],trng[1]);
      
      //th[i]->Fit(m_f1,"QR0LL");
      th[i]->Fit(m_f1,"QR0");
      
      if ( i==0 ){ g.pos().setX(m_f1->GetParameter(1)/m_CsISize*m_effCsISize); }
      else{ g.pos().setY(m_f1->GetParameter(1)/m_CsISize*m_effCsISize); }
    }
    //    std::cout<<"g"<<std::endl;    
    // calculating likelihood of the shower shape
    double tProb=0.;
    //double tpos=(i==0) ? g.coex() : g.coey();
    double tpos=th[i]->GetMean();
    for ( int j=1;j<=th[i]->GetNbinsX();j++ ){
      double offset=th[i]->GetBinCenter(j)-tpos;
      offset*=m_idxW[3][1];
      
      if ( offset<m_xMin ){ continue; }
      if ( offset>=m_xMax ){ continue; }
      int tIdx=(int)((offset-m_xMin)/(th[i]->GetBinWidth(j)/m_nDiv));
      int tSL=0;
      if ( th[i]->GetBinWidth(j)>30 ){ tSL=1; }
      
      double tSum=0.;
      double ttProb=0.;
      for ( int p=0;p<2;p++ ){ // energy
	for ( int q=0;q<2;q++ ){ // theta
	  for ( int r=0;r<2;r++ ){ // phi
	    int ttID=m_hDummy2[(int)m_idxW[0][0]+p]->Fill(th[i]->GetBinContent(j)/th[i]->Integral());
	    if ( ttID<0 ){ ttID=0; }
	    double tWeight=m_idxW[0][1+p]*m_idxW[1][1+q]*m_idxW[2][1+r];
	    tSum+=tWeight;
	    ttProb+=tWeight*m_pdfArray[tSL][(int)m_idxW[0][0]+p][(int)m_idxW[1][0]+q][(int)m_idxW[2][0]+r][tIdx][ttID]/m_pdfArray[tSL][(int)m_idxW[0][0]+p][(int)m_idxW[1][0]+q][(int)m_idxW[2][0]+r][tIdx][m_nBin-1];
	  }
	}
      }
      ttProb/=tSum;
      
      tProb+=TMath::Log10(ttProb);
    }
    //    std::cout<<"h"<<std::endl;    
    likelihood[i]=TMath::Power(10,tProb);
  }
  
}




double iwai::rowEnergyFunction(double* y, double* par){
	double xx=y[0];
	double c=par[0];
	double x0=par[1];
	double totE=par[2];
	
	//std::cerr << xx << ", " << x0 << std::endl;
	if ( TMath::IsNaN(x0) ){ return 1.e+50; }
	
	double tVal=0.;
	if ( m_idxW[3][0]==1. || TMath::Abs(xx)>24*m_CsISize ){ // large crystal
		double offset=((int)((50*(2*m_CsISize)+xx)/(2*m_CsISize))-50+0.5)*(2*m_CsISize)-x0;
		offset*=m_idxW[3][1];
		
		double t_offset[2]={offset-m_CsISize/2.,offset+m_CsISize/2.};
		for ( int n=0;n<2;n++ ){
			double ttVal=0.,ttSum=0.;
			for ( int i=0;i<2;i++ ){
				for ( int j=0;j<2;j++ ){
					for ( int k=0;k<2;k++ ){
						if ( t_offset[n]<m_sp[(int)m_idxW[0][0]+i][(int)m_idxW[1][0]+j][(int)m_idxW[2][0]+k]->GetXmin() || t_offset[n]>m_sp[(int)m_idxW[0][0]+i][(int)m_idxW[1][0]+j][(int)m_idxW[2][0]+k]->GetXmax() ){ continue; }
						double tWeight=m_idxW[0][1+i]*m_idxW[1][1+j]*m_idxW[2][1+k];
						ttVal+=tWeight*m_sp[(int)m_idxW[0][0]+i][(int)m_idxW[1][0]+j][(int)m_idxW[2][0]+k]->Eval(t_offset[n]);
						ttSum+=tWeight;
					}
				}
			}
			ttVal/=ttSum;
			tVal+=ttVal;
		}
	}else{
		double offset=((int)((100*m_CsISize+xx)/m_CsISize)-100+0.5)*m_CsISize-x0;
		offset*=m_idxW[3][1];
		
		double tSum=0.;
		for ( int i=0;i<2;i++ ){
			for ( int j=0;j<2;j++ ){
				for ( int k=0;k<2;k++ ){
					if ( offset<m_sp[(int)m_idxW[0][0]+i][(int)m_idxW[1][0]+j][(int)m_idxW[2][0]+k]->GetXmin() || offset>m_sp[(int)m_idxW[0][0]+i][(int)m_idxW[1][0]+j][(int)m_idxW[2][0]+k]->GetXmax() ){ continue; }
					double tWeight=m_idxW[0][1+i]*m_idxW[1][1+j]*m_idxW[2][1+k];
					tVal+=tWeight*m_sp[(int)m_idxW[0][0]+i][(int)m_idxW[1][0]+j][(int)m_idxW[2][0]+k]->Eval(offset);
					tSum+=tWeight;
				}
			}
		}
		tVal/=tSum;
	}
	return totE*c*tVal;
}

