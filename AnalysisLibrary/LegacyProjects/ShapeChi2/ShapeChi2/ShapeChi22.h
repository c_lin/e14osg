/**
 *  @file   ShapeChi22.h
 *  @brief  shape chi2 calculator
 *  This class provides a method to calculate shape chi2. 
 *
 *  @author Kazufumi Sato <sato@champ.hep.sci.osaka-u.ac.jp>
 *  @date   2011.9.23
 *  $Id: ShapeChi22.h,v 1.0 2011/09/23 12:38 sato Exp $
 *  $Log: ShapeChi22.h,v $
 */



#ifndef ShapeChi22_h_
#define ShapeChi22_h_

#include <cstdlib>
#include "GsimPersistency/GsimMessage.h"
#include "gamma/Gamma.h"
#include "TH2D.h"

class ShapeChi22
{
 public:
  ShapeChi22();
  ~ShapeChi22();
  void shapeChi2(Gamma& gamma,int mode=0);
  void drawMapInHist(TH2D* &hist,int type,int e,int t,int p,int x,int y,int kindOfMap);
  void drawMapInHist(TH2D* &hist,int kindOfMap,int imode=0);
  


  void getLocalChi2(double*lochi2){
    for(int i=0;i<s_mapSize*s_mapSize;i++){
      lochi2[i] = m_localChi2[i];
    }
  }
  void getIndex(int *index,double*percentage){
    for(int i=0;i<5;i++) index[i] = m_index[i];
    for(int i=0;i<5;i++) percentage[i] = m_percentage[i];
  }

 private:

  void loadMap();
  void rotAndMirror(double &phi,int const &csize,double *const *csipos);
  void selectMap(double const &ene,double const &theta,
		 double const &phi,int *index,double *percentage);
  int calcRatioOfRowE(int const &csize,double const *csiedep,
		      double *const *csipos,double const *csiwidth,
		      int *index,double *percentage);
  double compare(int const &csize,double const *csiedep,
		 double const *const *csipos,double const *csiwidth,
		 int const *index,double const *percentage,
		 int const &mode,int const chi2Mode = 0);
  
  static int const s_numEvalue=10;
  static int const s_numTvalue=10;
  static int const s_numPvalue=10;
  static int const s_numCtrBin=5;
  static int const s_mapSize=27;
  static int const s_mapCtr=9;
  int *mapNum[3*s_numEvalue*s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin];
  int mapNum0[3*s_numEvalue*s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin];
  float *mapMean[3*s_numEvalue*s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin];
  float *mapSquare[3*s_numEvalue*s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin];
  //float* distRowE[2][2][11][11][7]={{{{{0}}}}};
  float* distRowE[2][2][s_numEvalue][s_numTvalue][s_numPvalue];
  
  int getMapAddress(int const &forl,int const &e,int const &t,
		    int const &p,int const &x,int const &y) const;

  int m_index[5];
  double m_percentage[5];
  double m_localChi2[s_mapSize*s_mapSize];
  double m_recordMean[s_mapSize*s_mapSize];
  double m_recordDisp[s_mapSize*s_mapSize];
  double m_recordNum[s_mapSize*s_mapSize];
  double m_recordDepe[s_mapSize*s_mapSize];
  ////  
};

#endif
