/**
 *  @file   ShapeChi2New.h
 *  @brief  shape chi2 calculator
 *  This class provides a method to calculate shape chi2. 
 *
 *  @author Kazufumi Sato <sato@champ.hep.sci.osaka-u.ac.jp>
 *  @date   2011.9.23
 *  $Id: ShapeChi2New.h,v 1.0 2011/09/23 12:38 sato Exp $
 *  $Log: ShapeChi2New.h,v $
 */



#ifndef ShapeChi2New_h_
#define ShapeChi2New_h_

#include <cstdlib>
#include "GsimPersistency/GsimMessage.h"
#include "gamma/Gamma.h"
#include "TH2D.h"
#include "TH1D.h"

class ShapeChi2New
{
 public:

  enum {NONEXIST=0x1,SMALLTYPE=0x2,LARGETYPE=0x4};
  enum {SMALL_ONLY=0,SMALL_AND_LARGE,LARGE_ONLY};
  enum {SIMPLE=0,VV,LL};
  static int const s_nHistogram=10;
  enum {MEAN=0,RMS2,PROB,NUMHIT,NUMGEN,CHI2GAUS,CHI2BINVV,CHI2BINLL,DATA,TYPE};
  
  ShapeChi2New();
  ~ShapeChi2New();
  void shapeChi2(Gamma& gamma);

  void getIndex(int *index,double*percentage,double* param=0){
    for(int i=0;i<s_nParam;i++) index[i] = m_index[i];
    for(int i=0;i<s_nParam;i++) percentage[i] = m_percentage[i];
    if(param!=0){
      for(int i=0;i<s_nParam;i++) param[i] = m_param[i];
    }
  }
  
  TH2D** getHistograms();
  double getChi2(int method=LL,int mapSizeHit=s_mapSize,double mapSizeNoneD=0.999);

  void getLocalChi2(double *chi2){
    for(int i=0;i<s_mapSize;i++){
      for(int j=0;j<s_mapSize;j++){
	int ID = i*s_mapSize+j;
	chi2[ID] = m_localChi2[ID];
      }
    }
  }
 private:

  void loadMap();
  void createHist();
  int getMapAddress(int const &forl,int const &e,int const &t,
		    int const &p,int const &x,int const &y) const;
  void changeCoordinate(int size,double **csipos);
  
  void getCsiInformation(Gamma const & gamma, int &mode,int &size,
			 double* &csiedep,double* &csiposx,
			 double* &csiposy,double* &csiwidth);
  void rotAndMirror(double& phi,int const &csize,double * const *csipos);

  void searchForMaxRow(int size,double *csipos,double*csiedep,
		       double &ctrPos,double rowE[3]);
  void adjustCenter(int size,double **csipos,double ctrPos[2]);
  void searchBoundary(int size,double **csipos,double *csiwidth);
  void setData(int size,double **csipos,double *csiedep,double* csiwidth);
  void setMap();
  void setMask(double *ctrPos);
  void selectMap(int parType,double const &val,int &index,double &percentage);
  void calcChi2ForEachCsi();

  static int const s_numEvalue=10;
  static int const s_numTvalue=10;
  static int const s_numPvalue=10;
  static int const s_numCtrBin=5;
  static int const s_mapSize=27;
  static int const s_mapCtr=9;
  int *mapNum[3*s_numEvalue*s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin];
  int mapNum0[3*s_numEvalue*s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin];
  float *mapMean[3*s_numEvalue*s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin];
  float *mapSquare[3*s_numEvalue*s_numTvalue*s_numPvalue*s_numCtrBin*s_numCtrBin];
  float* distRowE[2][2][s_numEvalue][s_numTvalue][s_numPvalue];
 
  int m_mode;
  static int const s_nParam = 5;
  double m_param[s_nParam];
  int m_index[s_nParam];
  double m_percentage[s_nParam];
  double const m_smallCsiWidth; // 25.mm, set in constructor
  int const m_maxArrSize;       // 3000, set in constructor
  double const m_threshold;     // 3.MeV, set in constructor
  double m_lsBound[2];
  double m_localChi2[s_mapSize*s_mapSize];

  double m_mean[s_mapSize][s_mapSize];
  double m_rms2[s_mapSize][s_mapSize];
  double m_prob[s_mapSize][s_mapSize];
  double m_numHit[s_mapSize][s_mapSize];
  double m_numGen[s_mapSize][s_mapSize];
  double m_chi2Gaus[s_mapSize][s_mapSize];
  double m_chi2BinVV[s_mapSize][s_mapSize];
  double m_chi2BinLL[s_mapSize][s_mapSize];
  int m_isHitCsi[s_mapSize][s_mapSize];
  bool m_isUse[s_mapSize][s_mapSize];

  int m_chi2Mode;
  int m_hitMapSize;
  double m_nonHitMapSize;

  TH2D *m_histResult[s_nHistogram];

  TH1D *m_hist1DCsiSmall;
  TH1D *m_hist1DCsiLarge;
  TH2D *m_histData;
  TH2D *m_histType;
  TH2D *m_histCsiMap;
};

#endif
