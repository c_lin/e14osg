#ifndef IwaiGammaAnalyser_h__
#define IwaiGammaAnalyser_h__

#include <TSpline.h>
#include <TH1F.h>
#include <TF1.h>
#include <TFile.h>
#include <TMath.h>
#include <TTree.h>
//#include <Math/Polynomial.h>

#include <pi0/Pi0.h>
#include <gamma/Gamma.h>


namespace iwai{
  double shapeLikelihood(std::list<Pi0> plist,double likelihood[2][2][2]);

  double getLikelihoodRatio(double likeli[2][2][2],double tcut[2]);

  std::list<Pi0> rec2gWithConstM(std::list<Gamma> glist, double mass, bool b_beamAxis);

  std::list<Pi0> rec2gWithConstM(std::list<Gamma> glist, double mass, double aproxZ);

  const double m_effCsISize=25.15; // [mm]
  const double m_CsISize=25.;

  const double m_leakCut=0.1; // 10%

  const int m_nTheta=25;
  const int m_nPhi=15;
  const int m_nEne=9;
  
  const int m_nBin=102; // 1+100+1
  const double m_rowThres=5.; // MeV, thres. for row energy
  const int m_nDiv=25;
  
  const double m_eArray[m_nEne]={100,145,210,310,450,650,950,1380,2000};
  const double m_thArray[m_nTheta]={
    0, 5, 8,10,12,
    14,16,18,20,22,
    25,28,30,32,35,
    40,45,50,55,60,
    65,70,75,80,86
  };

  TSpline3* m_sp[m_nEne][m_nTheta][m_nPhi];

  double m_idxW[4][3]; // E/Theta/Phi/misc, idxL/w1/w2
  
  double rowEnergyFunction(double* y, double* par);
}

class GammaAnalyzer{
public:
	GammaAnalyzer(void);
	virtual ~GammaAnalyzer(void);
	
	void Analyze(Gamma& gamma, bool updatePos=kTRUE);
	
	// tempolraly for developping
	double likelihood[2];
	//TH1* getHistogram(int ixy){ return th[ixy]; }
	//int nFill[2];
	
protected:

private:
	TH1* th[2];
	
	TH1F* m_rowEnergy[2][2];
	TH1F* m_hDummy[3];
	TF1* m_f1;
	
	int****** m_pdfArray;
	TH1F* m_hDummy2[iwai::m_nEne];
	
	const double m_xMin;
	const double m_xMax;
	static const int m_nPDF[2];
};

#endif // IwaiGammaAnalyser_h__
