#ifndef __CSIENERGYCORRECTOR__CSITEMPERATURECORRECTOR_H__
#define __CSIENERGYCORRECTOR__CSITEMPERATURECORRECTOR_H__

#include "CSIEnergyCorrector/VirtualCSICorrector.h"

class CSITemperatureCorrector : public VirtualCSICorrector
{
public:
  CSITemperatureCorrector( const Char_t *DirName ) : VirtualCSICorrector( DirName ){;}
  
private: 
  Bool_t   SetCorrectionFactor();

};
#endif // __CSIENERGYCORRECTOR__CSITEMPERATURECORRECTOR_H__
