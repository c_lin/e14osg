#ifndef __CSIENERGYCORRECTOR__CSI3PI0CALIBCORRECTOR_H__
#define __CSIENERGYCORRECTOR__CSI3PI0CALIBCORRECTOR_H__

#include "CSIEnergyCorrector/VirtualCSICorrector.h"

class CSI3pi0CalibCorrector : public VirtualCSICorrector
{
public:
  CSI3pi0CalibCorrector( const Char_t *DirName ) : VirtualCSICorrector( DirName ){;}
  
private:
  Bool_t   SetCorrectionFactor();

};
#endif // __CSIENERGYCORRECTOR__CSI3PI0CALIBCORRECTOR_H__
