#ifndef __CSIENERGYCORRECTOR__CSIENERGYCORRECTOR_H__
#define __CSIENERGYCORRECTOR__CSIENERGYCORRECTOR_H__

#include <TMath.h>
#include "CSIEnergyCorrector/VirtualCSICorrector.h"
#include "CSIEnergyCorrector/CSIOverallCorrector.h"
#include "CSIEnergyCorrector/CSI3pi0CalibCorrector.h"
#include "CSIEnergyCorrector/CSILaserGainCorrector.h"
#include "CSIEnergyCorrector/CSITemperatureCorrector.h"
#include "CSIEnergyCorrector/CSITimeDependentGainCorrector.h"
class CSIEnergyCorrector
{
public:
  /**
   * @brief Constructor.
   * @param DirName Name of the top directory of correction data files.
   */
  CSIEnergyCorrector( const Char_t *DirName );

  /**
   * @brief Destructor.
   */
  ~CSIEnergyCorrector();
  

  static const Int_t NCorrector = 5;
  /**
   * @brief List of sub Correctors.<br> 
   *        The number of the elements is equal to NCorrector.
   */
  enum { K3pi0,       /**< CSI3pi0CalibCorrector         : channel by channel correction factor obtained from 3pi0 calibration */
	 Overall,     /**< CSIOverallCorrector           : overall correction factor obtained from Al target calibraion */
	 Laser,       /**< CSILaserGainCorrector         : gain correction with laser event information for several specified channels */
	 Temperature, /**< CSITemperatureCorrector       : gain and L.Y. correction with temperature information */
	 Time         /**< CSITimeDependentGainCorrector : time-dependent gain correction as an overall factor. */
  };// NCorrector
  

  /**
   * @brief  Set run ID.
   * @param  runID
   * @retval true    Correction factors are found.
   * @retval false   Correction factors are NOT found.
   */
  Bool_t  SetRunID( const Int_t runID );


  /**
   * @brief   Get a correction factor for the designated channel. The corrected energy is given by multiplying the original energy by the correction factor.  
   * @param   ModID CSI channel ID
   * @return  The correction factor which is the product of all the sub correction factors 
   *          for the designated channel (with ModID ).<br>
   *          If the correction factor is not set, it returns 0.
   */
  Double_t GetEnergyCorrectionFactor( const Int_t ModID );


  /**
   * @brief   Get a correction factor of a specified corrector. The corrected energy is given by multiplying the original energy by the correction factor.
   * @param   CorrectorID Specify a corrector using the enumerator. ( K3pi0() / Overall() / Laser() / Temperature() / Time() )
   * @param   ModID CSI channel ID.
   * @return  This function returns the correction factor for the designated Corrector and channel.
   * 
   */
  Double_t GetSubEnergyCorrectionFactor( const Int_t CorrectorID, const Int_t ModID );


  /**
   * @brief Set ForceReady flag of all correctors.<br>
   *        The behavior of GetEnergyCorrectionFactor() and GetSubEnergyCorrectionFactor() changes 
   *        in the case that the correction factor of a corrector is not set.<br>
   *        If the ForceReady flag (isForceReady) is NOT set (default) , the corrector returns 0.<br>
   *        If that is set , the corrector returns 1.<br>
   */
  void    ForceReady(){ for( Int_t i = 0 ; i < NCorrector ; ++i) SubCorrector[i]->ForceReady(); }


  /**
   * @param CorrectorID  Specify a corrector using the enumerator. ( K3pi0() / Overall() / Laser() / Temperature() / Time() )
   * @brief Set ForceReady flag of a specified corrector. <br>
   *        The behavior of GetEnergyCorrectionFactor() and GetSubEnergyCorrectionFactor() changes 
   *        in the case that the correction factor of a corrector is not set.<br>
   *        If the ForceReady flag (isForceReady) is NOT set (default) , the corrector returns 0.<br>
   *        If that is set , the corrector returns 1.<br>
   */
  void    ForceReady( const Int_t CorrectorID ){ SubCorrector[CorrectorID]->ForceReady(); }


  /**
   * @brief Disable the corrector. GetEnergyCorrectionFactor() and GetSubEnergyCorrectionFactor() always return 1.
   */
  void    SetDisable(){ for(Int_t i = 0 ; i < NCorrector ; ++i) SubCorrector[i]->SetDisable(); }

  /**
   * @param CorrectorID  Specify a corrector using the enumerator. ( K3pi0() / Overall() / Laser() / Temperature() / Time() )
   * @brief Disable the designated corrector. The Corrector always returns 1 
   *        in the GetEnergyCorrectionFactor() and GetSubEnergyCorrectionFactor() functions. 
   */
  void    SetDisable( const Int_t CorrectorID ){ SubCorrector[CorrectorID]->SetDisable(); }


  /**
   * @brief Eneble all the correctors. ( all correctors are enable by default )
   * @sa    SetDisable()
   */
  void    SetEnable(){  for(Int_t i = 0 ; i < NCorrector ; ++i) SubCorrector[i]->SetEnable(); }  


  /**
   * @brief Eneble the specified corrector. ( all correctors are enable by default )
   * @sa    SetDisable()
   */
  void    SetEnable(  const Int_t CorrectorID ){ SubCorrector[CorrectorID]->SetEnable(); }
  
private:
  Int_t    RunID;
  Bool_t   isReady;
  Bool_t   isForceReady;

  VirtualCSICorrector *SubCorrector[NCorrector];
};
#endif // __CSIENERGYCORRECTOR__CSIENERGYCORRECTOR_H__
