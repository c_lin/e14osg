#ifndef __CSIENERGYCORRECTOR__CSITIMEDEPENDENTGAINCORRECTOR_H__
#define __CSIENERGYCORRECTOR__CSITIMEDEPENDENTGAINCORRECTOR_H__

#include "CSIEnergyCorrector/VirtualCSICorrector.h"

class CSITimeDependentGainCorrector : public VirtualCSICorrector
{
public:
  CSITimeDependentGainCorrector( const Char_t *DirName ) : VirtualCSICorrector( DirName ){;}
  
private: 
  Bool_t   SetCorrectionFactor();

};
#endif // __CSIENERGYCORRECTOR__CSITIMEDEPENDENTGAINCORRECTOR_H__
