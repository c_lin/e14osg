#ifndef __CSIENERGYCORRECTOR__CSILASERGAINCORRECTOR_H__
#define __CSIENERGYCORRECTOR__CSILASERGAINCORRECTOR_H__

#include "CSIEnergyCorrector/VirtualCSICorrector.h"

class CSILaserGainCorrector : public VirtualCSICorrector
{
public:
  CSILaserGainCorrector( const Char_t *DirName ) : VirtualCSICorrector( DirName ){;}

private:
  Bool_t   SetCorrectionFactor();

};
#endif // __CSIENERGYCORRECTOR__CSILASERGAINCORRECTOR_H__
