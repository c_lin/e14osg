#ifndef __CSIENERGYCORRECTOR__CSIOVERALLCORRECTOR_H__
#define __CSIENERGYCORRECTOR__CSIOVERALLCORRECTOR_H__

#include "CSIEnergyCorrector/VirtualCSICorrector.h"

class CSIOverallCorrector : public VirtualCSICorrector
{
public:
  CSIOverallCorrector( const Char_t *DirName ) : VirtualCSICorrector( DirName ){;}

private:
  Bool_t   SetCorrectionFactor();

};
#endif // __CSIENERGYCORRECTOR__CSIOVERALLCORRECTOR_H__
