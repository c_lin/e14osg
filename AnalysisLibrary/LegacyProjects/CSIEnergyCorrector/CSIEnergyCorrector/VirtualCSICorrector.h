#ifndef __CSIENERGYCORRECTOR__VIRTUALCSICORRECTOR_H__
#define __CSIENERGYCORRECTOR__VIRTUALCSICORRECTOR_H__

#include <iostream>
#include <fstream>
#include <TMath.h>
#include <TChain.h>

class VirtualCSICorrector
{
public:
  VirtualCSICorrector( const Char_t *DirName );
  ~VirtualCSICorrector();

  ///// SetRunID( Int_t runID ) : 
  ///// if correction factors for the run are not found, this function returns "false."
  /////  (call SetCorrectionFactor() and return isReady)
  Bool_t   SetRunID( const Int_t runID );
  
  ///// GetCorrectionFactor()
  ///// this function returns the correction factor for the designated channel.
  ///// if correction factors are not set, it returns 0.
  Double_t GetCorrectionFactor( const Int_t ModID );
  
  ///// ForceReady() :
  ///// if correction factor is not set, GetCorrectionFactor() returns 1.
  void     ForceReady(){ isForceReady = true; }

  ///// if isDisable flag is unset, GetCorrectionFactor always returns 1.
  void     SetEnable(){  isDisable = false; }
  void     SetDisable(){ isDisable = true; }

protected:
  static const Int_t nCSICH = 2716;
  Int_t        RunID;
  Bool_t       isReady;///// set if CorrectionFactor is set.
  Bool_t       isForceReady;
  Bool_t       isDisable;
  Double_t    *CorrectionFactor;

  std::string DataDir;

  void        Initialize();

  ///// SetCorrectionFactor()
  ///// implemented in daughter classes.
  /////  case 1 : correction factor for the designated run ID is found
  /////    --> set values to CorrectionFactor array and retrun TRUE
  /////  case 2 : correction factor for the designated run ID is NOT found
  /////    --> set all elements of CorrectionFactor array to 1 and return FALSE
  virtual Bool_t   SetCorrectionFactor() = 0;
};
#endif // __CSIENERGYCORRECTOR__VIRTUALCSICORRECTOR_H__
