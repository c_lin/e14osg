#include "CSIEnergyCorrector/CSIEnergyCorrector.h"

CSIEnergyCorrector::CSIEnergyCorrector( const Char_t *DirName )
{
  SubCorrector[K3pi0]       = new CSI3pi0CalibCorrector( DirName );
  SubCorrector[Overall]     = new CSIOverallCorrector( DirName );
  SubCorrector[Laser]       = new CSILaserGainCorrector( DirName );
  SubCorrector[Temperature] = new CSITemperatureCorrector( DirName );
  SubCorrector[Time]        = new CSITimeDependentGainCorrector( DirName );

  RunID        = 0;
  isReady      = false;
}
//////////////////////////////////////////////////
CSIEnergyCorrector::~CSIEnergyCorrector()
{
  for( Int_t i = 0 ; i < NCorrector ; ++i )
    delete SubCorrector[i];
}
//////////////////////////////////////////////////
Bool_t CSIEnergyCorrector::SetRunID( const Int_t runID )
{
  RunID = runID;

  Bool_t flag = true;
  for( Int_t i = 0 ; i < NCorrector ; ++i )
    flag = ( SubCorrector[i]->SetRunID(RunID) && flag );
  isReady = flag;

  return isReady;
}
//////////////////////////////////////////////////
Double_t CSIEnergyCorrector::GetEnergyCorrectionFactor( const Int_t ModID )
{
  if( RunID==0 && !isForceReady ) return 0;

  Double_t CorrectionFactor = 1;
  for( Int_t i = 0 ; i < NCorrector ; ++i )
    CorrectionFactor *= SubCorrector[i]->GetCorrectionFactor(ModID);

  return CorrectionFactor;
}
//////////////////////////////////////////////////
Double_t CSIEnergyCorrector::GetSubEnergyCorrectionFactor( const Int_t CorrectorID, const Int_t ModID )
{
  if( RunID==0 && !isForceReady )                         return 0;
  if( !( 0 <= CorrectorID && CorrectorID < NCorrector ) ) return 0;

  Double_t CorrectionFactor = SubCorrector[CorrectorID]->GetCorrectionFactor(ModID);

  return CorrectionFactor;
}
