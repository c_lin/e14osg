#include "CSIEnergyCorrector/CSITimeDependentGainCorrector.h"

Bool_t CSITimeDependentGainCorrector::SetCorrectionFactor()
{
  Initialize();
  
  isReady = false;
  std::string fname;
  if      ( 17533 <= RunID  &&  RunID <= 19411 ){// RUN62
    //fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN62.root");
    isReady = true; return isReady;///// this correction is not used in this period.
  }else if( 19412 <= RunID  &&  RunID <= 20207 ){// RUN63
    //fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN63.root");
    isReady = true; return isReady;///// this correction is not used in this period.
  }else if( 20208 <= RunID  &&  RunID <= 21327 ){// RUN64
    //fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN64.root");
    isReady = true; return isReady;///// this correction is not used in this period.
  }else if( 21328 <= RunID  &&  RunID <= 22510 ){// RUN65
    fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN65.root");
  }else if( 22600 <= RunID  &&  RunID <= 24422 ){// RUN69
    fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN69.root");
  }else if( 24530 <= RunID  &&  RunID <= 25434 ){// RUN74
    //fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN74.root");
    isReady = true; return isReady;
  }else if( 25435 <= RunID  &&  RunID <= 26709 ){// RUN75 (dst ID or accelarator ID is run74)
    fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN75.root");
  }else if( 26710 <= RunID  &&  RunID <= 28394 ){// RUN78
    //fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN74.root");
    isReady = true; return isReady;
  }else if( 28395 <= RunID  &&  RunID <= 29826 ){// RUN79
    //fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN74.root");
    isReady = true; return isReady;
  }else if( 29827 <= RunID  &&  RunID <= 31531 ){// RUN81
    isReady = true; return isReady;    /// this correction is not used in thie period.
  }else if( 31532 <= RunID  &&  RunID <= 34000 ){// RUN82 to RUN85
    isReady = true; return isReady; /// this correction is not used in thie period.
  }else if( 34001 <= RunID  &&  RunID <= 34500 ){// RUN86 Feb
    fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN86_Feb.root");
  }else if( 34501 <= RunID  &&  RunID <= 34900 ){// RUN86 March
    isReady = true; return isReady; /// this correction is not used in thie period.    
  }else if( 34931 <= RunID ){ // RUN87
    fname = DataDir + std::string("/TimeDependentGainCorrectionData/TimeCorrection_RUN87.root");
  }
      

  
  TChain *Tree = new TChain("tree");
  Tree->Add( fname.c_str() );
  if( Tree->GetEntries() == 0 ){
    std::cout << "Time-dependent overall gain correction file : " <<  fname.c_str() << "is not found." << std::endl;
    delete Tree;
    return isReady;/////(=false)
  }

  Int_t    run;
  Double_t OverallFactor;
  Tree->SetBranchAddress( "RunID",            &run );
  Tree->SetBranchAddress( "CorrectionFactor", &OverallFactor );
  
  Long64_t nentry = Tree->GetEntries();
  for( Long64_t ientry = 0 ; ientry < nentry ; ++ientry ){
    Tree->GetEntry(ientry);
    if( run == RunID ){
      isReady  = true;
      break;
    }
  }
  
  if( !isReady ){
    Initialize();
  }else{
    for( Int_t i = 0 ; i < nCSICH ; ++i )
      CorrectionFactor[i] = OverallFactor;
  }
  
  delete   Tree;
  
  return isReady;
}
