#include "CSIEnergyCorrector/CSIOverallCorrector.h"

Bool_t CSIOverallCorrector::SetCorrectionFactor()
{
  Initialize();
  
  isReady = false; 
  std::string fname;
  if      ( 17533 <= RunID  &&  RunID <= 19411 ){// RUN62
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN62.dat";
  }else if( 19412 <= RunID  &&  RunID <= 20207 ){// RUN63
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN63.dat";
  }else if( 20208 <= RunID  &&  RunID <= 21327 ){// RUN64
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN64.dat";
  }else if( 21328 <= RunID  &&  RunID <= 22510 ){// RUN65
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN65.dat";
  }else if( 22600 <= RunID  &&  RunID <= 24422 ){// RUN69
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN69.dat";
  }else if( 24530 <= RunID  &&  RunID <= 26709 ){// RUN75
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN75.dat";
  }else if( 26710 <= RunID  &&  RunID <= 27123 ){// RUN78 Period1
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN78_Period1.dat";
  }else if( 27124 <= RunID  &&  RunID <= 27866 ){// RUN78 Period2
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN78_Period2.dat";
  }else if( 27867 <= RunID  &&  RunID <= 28394 ){// RUN78 Period3
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN78_Period3.dat";
  }else if( 28395 <= RunID  &&  RunID <= 29826 ){// RUN79
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN79.dat";
  }else if( 29827 <= RunID  &&  RunID <= 31531 ){// RUN81
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN81.dat";
  }else if( 31532 <= RunID  && RunID <= 32475  ){// RUN82
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN82.dat";
  } else if( 32476< RunID && RunID<=33890 ){ // RUN85
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN85_v1.dat";
  } else if( 33890< RunID && RunID<=34550){ // RUN86 Feb
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN86_v0.dat";
  } else if( 34550< RunID && RunID<=34930){ // RUN86 April
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN86_v1.dat";
  } else if( 34930< RunID ){ // RUN87
    fname = DataDir + "/OverallCorrectionData/OverallFactor_RUN87.dat";
  }

  
  std::ifstream ifs( fname.c_str() );
  if( ifs.fail() ){
    std::cout << "Overall correction(DVU Al) file : " << fname.c_str() << "is not found." << std::endl;
    return isReady;/////(=false)
  }
  
  Double_t OverallFactor;
  ifs >> OverallFactor;
  
  for( Int_t i = 0 ; i < nCSICH ; ++i )
    CorrectionFactor[i] = OverallFactor;
  isReady = true;
  
  return isReady;
}
