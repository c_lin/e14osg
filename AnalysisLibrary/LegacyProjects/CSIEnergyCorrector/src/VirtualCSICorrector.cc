#include "CSIEnergyCorrector/VirtualCSICorrector.h"

VirtualCSICorrector::VirtualCSICorrector( const Char_t *DirName ) : RunID(0),
								    isReady(false),
								    isForceReady(false),
								    isDisable(false)
{
  CorrectionFactor = new Double_t[nCSICH];
  DataDir = DirName;
}

VirtualCSICorrector::~VirtualCSICorrector(){
  delete[] CorrectionFactor;
}

Bool_t VirtualCSICorrector::SetRunID( const Int_t runID )
{
  RunID = runID;
  return SetCorrectionFactor();
}

Double_t VirtualCSICorrector::GetCorrectionFactor( const Int_t ModID )
{
  if(  isDisable )                               return 1;
  if(  isForceReady && (!isReady || RunID==0) )  return 1;

  if( !isForceReady && (!isReady || RunID==0) )  return 0;

  return CorrectionFactor[ModID];
}

void VirtualCSICorrector::Initialize()
{
  for( Int_t i = 0 ; i < nCSICH ; ++i )
    CorrectionFactor[i] = 1;
}
