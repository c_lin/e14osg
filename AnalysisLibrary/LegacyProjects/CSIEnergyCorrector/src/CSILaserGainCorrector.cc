#include "CSIEnergyCorrector/CSILaserGainCorrector.h"

Bool_t CSILaserGainCorrector::SetCorrectionFactor()
{
  Initialize();
 
  isReady = false;
  std::string fname;
  if      ( 17533 <= RunID  &&  RunID <= 19411 ){// RUN62
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN62.root";
  }else if( 19412 <= RunID  &&  RunID <= 20207 ){// RUN63
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN63.root";  
  }else if( 20208 <= RunID  &&  RunID <= 21327 ){// RUN64
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN64.root";
  }else if( 21328 <= RunID  &&  RunID <= 22510 ){// RUN65
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN65.root";
  }else if( 22600 <= RunID  &&  RunID <= 24422 ){// RUN69
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN69.root";
  }else if( 24530 <= RunID  &&  RunID <= 26709 ){// RUN75
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN75.root";
  }else if( 26710 <= RunID  &&  RunID <= 27123 ){// RUN78 Period1
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN78_Period1.root";
  }else if( 27124 <= RunID  &&  RunID <= 27866 ){// RUN78 Period2
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN78_Period2.root";
  }else if( 27867 <= RunID  &&  RunID <= 28394 ){// RUN78 Period3
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN78_Period3.root";
  }else if( 28395 <= RunID  &&  RunID <= 29826 ){// RUN79
    fname = DataDir + "/LaserCorrectionData/LaserInfo_RUN79.root";    
  }else if( 29827 <= RunID  &&  RunID <= 31531 ){// RUN81
    isReady = true; return isReady; /// this correction is not used in thie period.
  }else if( 31532 <= RunID  ){// RUN82 and RUN85
    isReady = true; return isReady; /// this correction is not used in thie period.
  }
  
  
  TChain *Tree = new TChain("Tree");
  Tree->Add( fname.c_str() );
  if( Tree->GetEntries() == 0 ){
    std::cout << "Laser calib file : " <<  fname.c_str() << "is not found." << std::endl;
    delete Tree;
    return isReady;/////(=false)
  }
  Int_t     run;
  Double_t *GainFactor      = new Double_t[nCSICH];
  Bool_t   *GainCorrectFlag = new Bool_t  [nCSICH];
  Tree->SetBranchAddress( "RunID",          &run );
  Tree->SetBranchAddress( "GainFactor",      GainFactor );
  Tree->SetBranchAddress( "GainCorrectFlag", GainCorrectFlag );
  
  Double_t *BaseGainFactor  = new Double_t[nCSICH];
  Tree->GetEntry(0);
  for( Int_t i = 0 ; i < nCSICH ; ++i ) 
    BaseGainFactor[i] = GainFactor[i];
  
  Long64_t nentry = Tree->GetEntries();
  for( Long64_t ientry = 0 ; ientry < nentry ; ++ientry ){
    Tree->GetEntry(ientry);
    if( run == RunID ){
      isReady = true;
      break;
    }
  }
  
  if( isReady ){
    for( Int_t i = 0 ; i < nCSICH ; ++i ){
      if( GainCorrectFlag[i] && GainFactor[i] > 0 )
	CorrectionFactor[i] = BaseGainFactor[i] / GainFactor[i] ;
    }
  }else{
    Initialize();
  }
  
  delete   Tree;
  delete[] GainFactor;
  delete[] GainCorrectFlag;
  delete[] BaseGainFactor;
  
  return isReady;
}
