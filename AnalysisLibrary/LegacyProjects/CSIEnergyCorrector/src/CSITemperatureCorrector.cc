#include "CSIEnergyCorrector/CSITemperatureCorrector.h"

Bool_t CSITemperatureCorrector::SetCorrectionFactor()
{
  Initialize();
  
  isReady = false;
  std::string fname;
  if      ( 17533 <= RunID  &&  RunID <= 19411 ){// RUN62
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN62.root");
  }else if( 19412 <= RunID  &&  RunID <= 20207 ){// RUN63
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN63.root");
  }else if( 20208 <= RunID  &&  RunID <= 21327 ){// RUN64
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN64.root");
  }else if( 21328 <= RunID  &&  RunID <= 22510 ){// RUN65
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN65.root");
  }else if( 22600 <= RunID  &&  RunID <= 24422 ){// RUN69
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN69.root");
  }else if( 24530 <= RunID  &&  RunID <= 26709 ){// RUN75
    isReady=true;
    return isReady;
  }else if( 26710 <= RunID  &&  RunID <= 28394 ){// RUN78
    isReady=true;
    return isReady;
  }else if( 28395 <= RunID  &&  RunID <= 29826 ){// RUN79
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN79.root");
  }else if( 29827 <= RunID  &&  RunID <= 31531 ){// RUN81
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN81_v0.root");    
  }else if( 31532 <= RunID  &&  RunID <= 32475){// RUN82
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN82_v0.root");    
  } else if( 32476< RunID && RunID<=33890 ){ // RUN85
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN85_v1.root");    
  } else if( 33890< RunID && RunID<=34550){ // RUN86
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN86_v0.root");      
  } else if( 34550< RunID && RunID<=34930){ // RUN86 April
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN86_v1.root");      
  } else if( 34930< RunID ){ // RUN87
    fname = DataDir + std::string("/TemperatureCorrectionData/TempCorrectoin_RUN87_v1.root");      
  }
  
  
  
  TChain *Tree = new TChain("TempRunTree");
  Tree->Add( fname.c_str() );
  if( Tree->GetEntries() == 0 ){
    std::cout << "Temperature correction file : " <<  fname.c_str() << "is not found." << std::endl;
    delete Tree;
    return isReady;/////(=false)
  }
  Int_t    run;
  Tree->SetBranchAddress( "RunID",           &run );
  Tree->SetBranchAddress( "CorrectionFactor", CorrectionFactor );
  
  Long64_t nentry = Tree->GetEntries();
  for( Long64_t ientry = 0 ; ientry < nentry ; ++ientry ){
    Tree->GetEntry(ientry);
    if( run == RunID ){
      isReady  = true;
      break;
    }
  }
  
  if( !isReady )
    Initialize();
  
  delete   Tree;
  
  return isReady;
}
