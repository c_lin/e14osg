#include "CSIEnergyCorrector/CSI3pi0CalibCorrector.h"

Bool_t CSI3pi0CalibCorrector::SetCorrectionFactor()
{
  Initialize();

  isReady = false;
  std::string fname;
  if      ( 17533 <= RunID  &&  RunID <= 19411 ){// RUN62
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN62.dat";
  }else if( 19412 <= RunID  &&  RunID <= 20207 ){// RUN63
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN63.dat";
  }else if( 20208 <= RunID  &&  RunID <= 21327 ){// RUN64
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN64.dat";
  }else if( 21328 <= RunID  &&  RunID <= 22510 ){// RUN65
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN65.dat";
  }else if( 22600 <= RunID  &&  RunID <= 24422 ){// RUN69
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN69.dat";
  }else if( 24530 <= RunID  &&  RunID <= 26709 ){// RUN75
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN75.dat";
  }else if( 26710 <= RunID  &&  RunID <= 27123 ){// RUN78 Period1
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN78_Period1.dat";
  }else if( 27124 <= RunID  &&  RunID <= 27866 ){// RUN78 Period2
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN78_Period2.dat";
  }else if( 27867 <= RunID  &&  RunID <= 28394 ){// RUN78 Period3
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN78_Period3.dat";
  }else if( 28395 <= RunID  &&  RunID <= 29826 ){// RUN79
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN79.dat";
  }else if( 29827 <= RunID  &&  RunID <= 31531 ){// RUN81
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN81_v0.dat";    
  }else if( 31532 <= RunID  &&  RunID<= 32475){// RUN82
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN82_v0.dat";
  } else if( 32476< RunID && RunID<=33890 ){ // RUN85
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN85_v1.dat";
  } else if( 33890< RunID && RunID<=34550){ // RUN86 Feb
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN86_v0.dat";
  } else if( 34550< RunID && RunID<= 34930){ // RUN86 April
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN86_v1.dat";
  } else if( 34930< RunID){ // RUN87
    fname = DataDir + "/KL3pi0CalibData/K3piCalib_RUN87_v0.dat";
  }
  
  
  
  std::ifstream ifs( fname.c_str() );
  if( ifs.fail() ){
    std::cout << "3pi0 calib file : " << fname.c_str() << "is not found." << std::endl;
    return isReady;/////(=false)
  }
  
  UInt_t  id;
  Double_t factor;
  while( ifs >> id >> factor )
    CorrectionFactor[id] = factor;
  isReady = true;
  
  return isReady;
}
