#ifndef __DSTETCORRECTOR__TIMINGSHIFTCORRECTOR_H__
#define __DSTETCORRECTOR__TIMINGSHIFTCORRECTOR_H__

#include <iostream>
#include <cmath>
#include <map>
#include <string>
#include <sys/stat.h>

#include <TROOT.h>
#include <TChain.h>
#include <TString.h>
#include <TMath.h>

#include "MTAnalysisLibrary/MTBasicParameters.h"

class TimingShiftCorrector
{
public:
  //TimingShiftCorrector( Int_t runid, Int_t nodeid, Int_t fileid, Int_t detid );
  TimingShiftCorrector( Int_t detid, Int_t runid, Int_t nodeid=-1, Int_t fileid=-1 );
  ~TimingShiftCorrector();
  void    SetNodeFileID( const Int_t nodeid, const Int_t fileid );
  Float_t GetCorrValue( Int_t modID, std::string unit = "clock" );// unit : "clock" or "ns"
  
  
  static void SetDataDirName( const Char_t *DirName ){ DataDirName = DirName; }
  static void SetSpillID( const Short_t spillID ){     SpillID     = spillID; }
  static void SetL2TrigNo( const Short_t l2trigNo ){   L2TrigNo    = l2trigNo; }
  static std::string DataDirName;
  static Short_t     SpillID;
  static Short_t     L2TrigNo;
  

private:
  const Int_t   RunID;
  const Int_t   DetectorID;
  const Char_t *DetectorName;
  const Int_t   DetectorNCH;
  const Float_t ClockToNs;

  ///// for Run by Run correction
  TChain  *tree_RBR;
  Bool_t   RBRFlag;
  Int_t    Nmod_RBR;
  Int_t   *modID_RBR;
  Float_t *CorrectionInClock_RBR;
  void     SetBranchAddressRBR();
  Float_t  GetRBRCorrValue( Int_t modID );
  
  ///// for Spill by Spill correction
  TChain  *tree_SBS;
  Bool_t   SBSFlag;
  Int_t    Nmod_SBS;
  Int_t   *modID_SBS;
  Float_t *CorrectionInClock_SBS;  
  void     SetBranchAddressSBS();
  Float_t  GetSBSCorrValue( Int_t modID );
  Short_t  SpillID_SBS;
  Long64_t NEntry_SBS;

  ///// for Event by Event correction
  TChain  *tree_EBE;
  Bool_t   EBEDirFlag;
  Bool_t   EBEFlag;
  Int_t    Nmod_EBE;
  Int_t   *modID_EBE;
  Float_t *CorrectionInClock_EBE;
  void     SetBranchAddressEBE();
  Float_t  GetEBECorrValue( Int_t modID );  
  Short_t  SpillID_EBE;
  Short_t  L2TrigNo_EBE;
  Long64_t NEntry_EBE;
  Long64_t EntryID_EBE;
  Int_t    NodeID;
  Int_t    FileID;
  Int_t    CurrentNodeID;
  Int_t    CurrentFileID;
  
};

#endif // __DSTETCORRECTOR__TIMINGSHIFTCORRECTOR_H__
