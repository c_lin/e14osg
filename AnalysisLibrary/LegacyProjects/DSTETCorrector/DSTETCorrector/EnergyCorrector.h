#ifndef __DSTETCORRECTOR__ENERGYCORRECTOR_H__
#define __DSTETCORRECTOR__ENERGYCORRECTOR_H__

#include <iostream>
#include <cmath>
#include <map>
#include <string>
#include <sys/stat.h>

#include <TROOT.h>
#include <TChain.h>
#include <TString.h>
#include <TMath.h>

#include "MTAnalysisLibrary/MTBasicParameters.h"

class EnergyCorrector
{
public:
  //EnergyCorrector( Int_t runid, Int_t nodeid, Int_t fileid, Int_t detid );
  EnergyCorrector( Int_t detid, Int_t runid, Int_t nodeid=-1, Int_t fileid=-1 );
  ~EnergyCorrector();
  void    SetNodeFileID( const Int_t nodeid, const Int_t fileid );
  Float_t GetCorrFactor( Int_t modID );
  
  
  static void SetDataDirName( const Char_t *DirName ){ DataDirName = DirName; }
  static void SetSpillID( const Short_t spillid ){     SpillID     = spillid; }
  static void SetL2TrigNo( const Short_t l2trigNo ){   L2TrigNo    = l2trigNo; }
  static std::string DataDirName;
  static Short_t     SpillID;
  static Short_t     L2TrigNo;


private:
  const Int_t   RunID;
  const Int_t   DetectorID;
  const Char_t *DetectorName;
  const Int_t   DetectorNCH;

  ///// for Run by Run correction
  TChain  *tree_RBR;
  Bool_t   RBRFlag;
  Int_t    Nmod_RBR;
  Int_t   *modID_RBR;
  Float_t *CorrectionFactor_RBR;
  void     SetBranchAddressRBR();
  Float_t  GetRBRCorrFactor( Int_t modID );
  
  ///// for Spill by Spill correction
  TChain  *tree_SBS;
  Bool_t   SBSFlag;
  Int_t    Nmod_SBS;
  Int_t   *modID_SBS;
  Float_t *CorrectionFactor_SBS;  
  void     SetBranchAddressSBS();
  Float_t  GetSBSCorrFactor( Int_t modID );
  Short_t  SpillID_SBS;
  Long64_t NEntry_SBS;

  ///// for Event by Event correction
  TChain  *tree_EBE;
  Bool_t   EBEDirFlag;
  Bool_t   EBEFlag;
  Int_t    Nmod_EBE;
  Int_t   *modID_EBE;
  Float_t *CorrectionFactor_EBE;
  void     SetBranchAddressEBE();
  Float_t  GetEBECorrFactor( Int_t modID );  
  Short_t  SpillID_EBE;
  Short_t  L2TrigNo_EBE;
  Long64_t NEntry_EBE;
  Long64_t EntryID_EBE;
  Int_t    NodeID;
  Int_t    FileID;
  Int_t    CurrentNodeID;
  Int_t    CurrentFileID;
  
};

#endif // __DSTETCORRECTOR__ENERGYCORRECTOR_H__
