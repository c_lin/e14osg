#include "DSTETCorrector/TimingShiftCorrector.h"

std::string TimingShiftCorrector::DataDirName = "";
Short_t     TimingShiftCorrector::SpillID     = 0;
Short_t     TimingShiftCorrector::L2TrigNo    = -1;

TimingShiftCorrector::TimingShiftCorrector( Int_t detid,
					    Int_t runid,
					    Int_t nodeid,
					    Int_t fileid ):RunID(runid),
							   DetectorID(detid),
							   DetectorName(MTBP::detectorName[detid].c_str()),
							   DetectorNCH( MTBP::DetNChannels[detid]),
							   ClockToNs(  (MTBP::is125MHzDetector[detid])? 8 : 2 ),
							   RBRFlag(false),
							   SBSFlag(false),
							   EBEDirFlag(false),
							   EBEFlag(false),
							   NodeID(nodeid),
							   FileID(fileid),
							   CurrentNodeID(-1),
							   CurrentFileID(-1)
{
  std::cout << "TimingShiftCorrector for " << MTBP::detectorName[detid].c_str() << " is generated." << std::endl;
  if( DataDirName=="" )
    std::cout << "TimingShiftCorrector::DataDirName is not set." << std::endl
	      << "You need to set TimingShiftCorrector::DataDirName BEFORE calling TimingShiftCorrector class constructor." << std::endl;
  {///// Run By Run
    struct stat st;
    TString fname_RBR = Form("%s/%s/Time/RunByRun/run%d.root",DataDirName.c_str(),DetectorName,RunID);  
    if( stat(fname_RBR.Data(),&st)==0 ){///// file exists
      std::cout << "\t Timing correction file is found : " << fname_RBR.Data() << std::endl;
      tree_RBR = new TChain("tree");
      tree_RBR->Add( fname_RBR.Data() );
      if( tree_RBR->GetEntries()>0 ){
	RBRFlag = true;
	modID_RBR             = new Int_t  [DetectorNCH];
	CorrectionInClock_RBR = new Float_t[DetectorNCH];
	SetBranchAddressRBR();
      }else{
	delete tree_RBR;
      }
    }
  }
  
  {///// Spill By Spill
    struct stat st;
    TString fname_SBS = Form("%s/%s/Time/SpillBySpill/run%d.root",DataDirName.c_str(),DetectorName,RunID);  
    if( stat(fname_SBS.Data(),&st)==0 ){///// file exists
      std::cout << "\t Timing correction file is found : " << fname_SBS.Data() << std::endl;
      tree_SBS = new TChain("tree");
      tree_SBS->Add( fname_SBS.Data() );
      if( tree_SBS->GetEntries()>0 ){
	SBSFlag = true;
	modID_SBS             = new Int_t  [DetectorNCH];
	CorrectionInClock_SBS = new Float_t[DetectorNCH];
	SetBranchAddressSBS();
      }else{
	delete tree_SBS;
      }
    }
  }
  
  {///// Event By Event
    struct stat st;
    TString dirname_EBE = Form("%s/%s/Time/EventByEvent/run%d",DataDirName.c_str(),DetectorName,RunID);
    if( stat(dirname_EBE.Data(),&st)==0 ){///// directory exists
      EBEDirFlag = true;
      modID_EBE             = new Int_t  [DetectorNCH];
      CorrectionInClock_EBE = new Float_t[DetectorNCH];
      
      if( NodeID!=-1 && FileID!=-1 )
	SetNodeFileID( NodeID, FileID );
    }
  }
}

void TimingShiftCorrector::SetNodeFileID( const Int_t nodeid, const Int_t fileid )
{
  if( !EBEDirFlag ) return;
  if( CurrentNodeID==nodeid && CurrentFileID==fileid ) return;
  CurrentNodeID = nodeid;
  CurrentFileID = fileid;
  
  if( EBEFlag ){
    delete tree_EBE;
    EBEFlag = false;
  }
  struct stat st;
  TString fname_EBE = Form("%s/%s/Time/EventByEvent/run%d/run%d_node%d_file%d.root",DataDirName.c_str(),DetectorName,RunID,RunID,nodeid,fileid);
  if( stat(fname_EBE.Data(),&st)==0 ){///// file exists
    std::cout << "\t Timing correction file is found : " << fname_EBE.Data() << std::endl;
    tree_EBE = new TChain("tree");
    tree_EBE->Add( fname_EBE.Data() );
    if( tree_EBE->GetEntries()>0 ){
      EBEFlag = true;
      EntryID_EBE  = 0;
      SetBranchAddressEBE();
    }else{
      delete tree_EBE;
    }
  }
}


TimingShiftCorrector::~TimingShiftCorrector()
{
  if(RBRFlag){
    delete   tree_RBR;
    delete[] modID_RBR;
    delete[] CorrectionInClock_RBR;
  }
  if(SBSFlag){
    delete   tree_SBS;
    delete[] modID_SBS;
    delete[] CorrectionInClock_SBS;
  }
  if(EBEDirFlag){
    if(EBEFlag)
      delete tree_EBE;
    delete[] modID_EBE;
    delete[] CorrectionInClock_EBE;
  }
}

void TimingShiftCorrector::SetBranchAddressRBR()
{
  tree_RBR->SetBranchAddress("Nmod",             &Nmod_RBR);
  tree_RBR->SetBranchAddress("modID",             modID_RBR);
  tree_RBR->SetBranchAddress("CorrectionInClock", CorrectionInClock_RBR);
  tree_RBR->GetEntry(0);
}
void TimingShiftCorrector::SetBranchAddressSBS()
{
  tree_SBS->SetBranchAddress("SpillID",          &SpillID_SBS);
  tree_SBS->SetBranchAddress("Nmod",             &Nmod_SBS);
  tree_SBS->SetBranchAddress("modID",             modID_SBS);
  tree_SBS->SetBranchAddress("CorrectionInClock", CorrectionInClock_SBS);
  NEntry_SBS = tree_SBS->GetEntries();
  tree_SBS->GetEntry(0);
}
void TimingShiftCorrector::SetBranchAddressEBE()
{
  tree_EBE->SetBranchAddress("L2TrigNo",         &L2TrigNo_EBE);
  tree_EBE->SetBranchAddress("SpillID",          &SpillID_EBE);
  tree_EBE->SetBranchAddress("Nmod",             &Nmod_EBE);
  tree_EBE->SetBranchAddress("modID",             modID_EBE);
  tree_EBE->SetBranchAddress("CorrectionInClock", CorrectionInClock_EBE);
  NEntry_EBE = tree_EBE->GetEntries();
  tree_EBE->GetEntry(0);
}

Float_t TimingShiftCorrector::GetCorrValue( Int_t modID, std::string unit )
{
  Float_t RBRCorrValue = GetRBRCorrValue( modID );
  Float_t SBSCorrValue = GetSBSCorrValue( modID );
  Float_t EBECorrValue = GetEBECorrValue( modID );
  Float_t CorrValue =  RBRCorrValue + SBSCorrValue + EBECorrValue;
  if( unit == "ns" )
    CorrValue *= ClockToNs;
  return CorrValue;
}

Float_t TimingShiftCorrector::GetRBRCorrValue( Int_t modID )
{
  if(!RBRFlag) return 0; 
  
  for(Int_t ich = 0 ; ich < Nmod_RBR ; ++ich)
    if( modID_RBR[ich] == modID )
      return CorrectionInClock_RBR[ich]; 
  return 0;
}

Float_t TimingShiftCorrector::GetSBSCorrValue( Int_t modID )
{
  if(!SBSFlag) return 0; 
  
  if( SpillID == SpillID_SBS ){
    for(Int_t ich = 0 ; ich < Nmod_SBS ; ++ich)
      if( modID_SBS[ich] == modID )
	return CorrectionInClock_SBS[ich];
  }else{
    if( 0 <= SpillID && SpillID < NEntry_SBS ){
      tree_SBS->GetEntry(SpillID);
      for(Int_t ich = 0 ; ich < Nmod_SBS ; ++ich)
	if( modID_SBS[ich] == modID )
	  return CorrectionInClock_SBS[ich];
    }
  }
  return 0;
}

Float_t TimingShiftCorrector::GetEBECorrValue( Int_t modID )
{
  if(!EBEFlag) return 0; 

  while( EntryID_EBE < NEntry_EBE ){
    if( SpillID==SpillID_EBE  &&  L2TrigNo==L2TrigNo_EBE ){
      for(Int_t ich = 0 ; ich < Nmod_EBE ; ++ich)
	if( modID_EBE[ich] == modID )
	  return CorrectionInClock_EBE[ich];
      break;
    }else{
      ++EntryID_EBE;
      tree_EBE->GetEntry(EntryID_EBE);
    }
  }
  if( EntryID_EBE >=  NEntry_EBE )
    std::cout << " Event by Event timing correction data is not found." << std::endl;
  return 0;
}
