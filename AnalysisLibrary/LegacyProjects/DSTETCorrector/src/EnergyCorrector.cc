#include "DSTETCorrector/EnergyCorrector.h"

std::string EnergyCorrector::DataDirName = "";
Short_t     EnergyCorrector::SpillID     = 0;
Short_t     EnergyCorrector::L2TrigNo    = -1;

EnergyCorrector::EnergyCorrector( Int_t detid,
				  Int_t runid,
				  Int_t nodeid,
				  Int_t fileid ):RunID(runid),
						 DetectorID(detid),
						 DetectorName(MTBP::detectorName[detid].c_str()),
						 DetectorNCH( MTBP::DetNChannels[detid]),
						 RBRFlag(false),
						 SBSFlag(false),
						 EBEDirFlag(false),
						 EBEFlag(false),
						 NodeID(nodeid),
						 FileID(fileid),
						 CurrentNodeID(-1),
						 CurrentFileID(-1)
{
  std::cout << "EnergyCorrector for " << MTBP::detectorName[detid].c_str() << " is generated." << std::endl;
  if( DataDirName=="" )
    std::cout << "EnergyCorrector::DataDirName is not set." << std::endl
              << "You need to set EnergyCorrector::DataDirName BEFORE calling EnergyCorrector class constructor." << std::endl;  
  {///// Run By Run
    struct stat st;
    TString fname_RBR = Form("%s/%s/Energy/RunByRun/run%d.root",DataDirName.c_str(),DetectorName,RunID);  
    if( stat(fname_RBR.Data(),&st)==0 ){///// file exists
      std::cout << "\t Energy correction file is found : " << fname_RBR.Data() << std::endl;
      tree_RBR = new TChain("tree");
      tree_RBR->Add( fname_RBR.Data() );
      if( tree_RBR->GetEntries()>0 ){
	RBRFlag = true;
	modID_RBR            = new Int_t  [DetectorNCH];
	CorrectionFactor_RBR = new Float_t[DetectorNCH];
	SetBranchAddressRBR();
      }else{
	delete tree_RBR;
      }
    }
  }
  
  {///// Spill By Spill
    struct stat st;
    TString fname_SBS = Form("%s/%s/Energy/SpillBySpill/run%d.root",DataDirName.c_str(),DetectorName,RunID);
    if( stat(fname_SBS.Data(),&st)==0 ){///// file exists
      std::cout << "\t Energy correction file is found : " << fname_SBS.Data() << std::endl;
      tree_SBS = new TChain("tree");
      tree_SBS->Add( fname_SBS.Data() );
      if( tree_SBS->GetEntries()>0 ){
	SBSFlag = true;
	modID_SBS            = new Int_t  [DetectorNCH];
	CorrectionFactor_SBS = new Float_t[DetectorNCH];
	SetBranchAddressSBS();
      }else{
	delete tree_SBS;
      }
    }
  }

  {///// Event By Event
    struct stat st;
    TString dirname_EBE = Form("%s/%s/Energy/EventByEvent/run%d",DataDirName.c_str(),DetectorName,RunID);
    if( stat(dirname_EBE.Data(),&st)==0 ){///// directory exists
      EBEDirFlag = true;
      modID_EBE            = new Int_t  [DetectorNCH];
      CorrectionFactor_EBE = new Float_t[DetectorNCH];

      if( NodeID!=-1 && FileID!=-1)
	SetNodeFileID( NodeID, FileID );
    }
  } 
}

void EnergyCorrector::SetNodeFileID( const Int_t nodeid, const Int_t fileid )
{
  if( !EBEDirFlag ) return;
  if( CurrentNodeID==nodeid && CurrentFileID==fileid ) return;
  CurrentNodeID = nodeid;
  CurrentFileID = fileid;
  
  if( EBEFlag ){
    delete tree_EBE;
    EBEFlag = false;
  }
  struct stat st;
  TString fname_EBE = Form("%s/%s/Energy/EventByEvent/run%d/run%d_node%d_file%d.root",DataDirName.c_str(),DetectorName,RunID,RunID,nodeid,fileid);
  if( stat(fname_EBE.Data(),&st)==0 ){///// file exists
    std::cout << "\t Energy correction file is found : " << fname_EBE.Data() << std::endl;
    tree_EBE = new TChain("tree");
    tree_EBE->Add( fname_EBE.Data() );
    if( tree_EBE->GetEntries()>0 ){
      EBEFlag = true;
      EntryID_EBE  = 0;
      SetBranchAddressEBE();
    }else{
      delete tree_EBE;
    }
  }
}


EnergyCorrector::~EnergyCorrector()
{
  if(RBRFlag){
    delete   tree_RBR;
    delete[] modID_RBR;
    delete[] CorrectionFactor_RBR;
  }
  if(SBSFlag){
    delete   tree_SBS;
    delete[] modID_SBS;
    delete[] CorrectionFactor_SBS;
  }
  if(EBEDirFlag){
    if(EBEFlag)
      delete tree_EBE;
    delete[] modID_EBE;
    delete[] CorrectionFactor_EBE;
  }
}

void EnergyCorrector::SetBranchAddressRBR()
{
  tree_RBR->SetBranchAddress("Nmod",             &Nmod_RBR);
  tree_RBR->SetBranchAddress("modID",             modID_RBR);
  tree_RBR->SetBranchAddress("CorrectionFactor",  CorrectionFactor_RBR);
  tree_RBR->GetEntry(0);
}
void EnergyCorrector::SetBranchAddressSBS()
{
  tree_SBS->SetBranchAddress("SpillID",          &SpillID_SBS);
  tree_SBS->SetBranchAddress("Nmod",             &Nmod_SBS);
  tree_SBS->SetBranchAddress("modID",             modID_SBS);
  tree_SBS->SetBranchAddress("CorrectionFactor",  CorrectionFactor_SBS);
  NEntry_SBS = tree_SBS->GetEntries();
  tree_SBS->GetEntry(0);
}
void EnergyCorrector::SetBranchAddressEBE()
{
  tree_EBE->SetBranchAddress("L2TrigNo",         &L2TrigNo_EBE);
  tree_EBE->SetBranchAddress("SpillID",          &SpillID_EBE);
  tree_EBE->SetBranchAddress("Nmod",             &Nmod_EBE);
  tree_EBE->SetBranchAddress("modID",             modID_EBE);
  tree_EBE->SetBranchAddress("CorrectionFactor", CorrectionFactor_EBE);
  NEntry_EBE = tree_EBE->GetEntries();
  tree_EBE->GetEntry(0);
}

Float_t EnergyCorrector::GetCorrFactor( Int_t modID )
{
  Float_t RBRCorrFactor = GetRBRCorrFactor( modID );
  Float_t SBSCorrFactor = GetSBSCorrFactor( modID );
  Float_t EBECorrFactor = GetEBECorrFactor( modID );
  Float_t CorrFactor    = RBRCorrFactor * SBSCorrFactor * EBECorrFactor;

  return CorrFactor;
}

Float_t EnergyCorrector::GetRBRCorrFactor( Int_t modID )
{
  if(!RBRFlag) return 1;
  
  for(Int_t ich = 0 ; ich < Nmod_RBR ; ++ich)
    if( modID_RBR[ich] == modID )
      return CorrectionFactor_RBR[ich]; 
  return 1;
}

Float_t EnergyCorrector::GetSBSCorrFactor( Int_t modID )
{
  if(!SBSFlag) return 1;
  
  if( SpillID == SpillID_SBS ){
    for(Int_t ich = 0 ; ich < Nmod_SBS ; ++ich)
      if( modID_SBS[ich] == modID )
	return CorrectionFactor_SBS[ich];
  }else{
    if( 0 <= SpillID && SpillID < NEntry_SBS ){
      tree_SBS->GetEntry(SpillID);
      for(Int_t ich = 0 ; ich < Nmod_SBS ; ++ich)
	if( modID_SBS[ich] == modID )
	  return CorrectionFactor_SBS[ich];
    }
  }
  return 1;
}

Float_t EnergyCorrector::GetEBECorrFactor( Int_t modID )
{
  if(!EBEFlag) return 1; 

  while( EntryID_EBE < NEntry_EBE ){
    if( SpillID==SpillID_EBE  &&  L2TrigNo==L2TrigNo_EBE ){
      for(Int_t ich = 0 ; ich < Nmod_EBE ; ++ich)
	if( modID_EBE[ich] == modID )
	  return CorrectionFactor_EBE[ich];
      break;
    }else{
      ++EntryID_EBE;
      tree_EBE->GetEntry(EntryID_EBE);
    }
  }
  if( EntryID_EBE >=  NEntry_EBE )
    std::cout << " Event by Event energy correction data is not found." << std::endl;
  return 1;
}
