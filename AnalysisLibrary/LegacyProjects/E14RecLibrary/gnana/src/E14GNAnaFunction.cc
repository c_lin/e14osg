#include "gnana/E14GNAnaFunction.h"
#include "csimap/CsiMap.h"
#include <string>
#include <map>
#include <sstream>
#include <iostream>
#include <fstream>
#include "TMath.h"
#include "TSpline.h"
#include <vector>
#include <algorithm>
#include <math.h>
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "gamma/Gamma.h"
#include "pi0/Pi0.h"
#include "klong/RecKlong.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "TArrayD.h"


//#include "TCanvas.h"
#define MAX_ITERACTIONS 30
#define MASS_KL 497.648 //MeV

E14GNAnaFunction* E14GNAnaFunction::s_function=0;


E14GNAnaFunction::E14GNAnaFunction()
{
  if( s_function ){
    std::cerr << "E14GNAnaFunction is constructed twice." << std::endl;
    return;
  }
  s_function = this;
}


E14GNAnaFunction::~E14GNAnaFunction(){
  ;
}


E14GNAnaFunction* E14GNAnaFunction::getFunction() {
  if(!s_function) {
    s_function  = new E14GNAnaFunction();
  }
  return s_function;
}

void E14GNAnaFunction::correctEnergy(Gamma &gamma){
  double thre = gamma.cluster().threshold();
  double edep = gamma.edep();
  double ws=0,wl=0;
  std::vector<int> const &idvec = gamma.clusterIdVec();
  std::vector<double> const &evec = gamma.clusterEVec();
  
  int size = idvec.size();
  for(int i=0;i<size;i++){
    double width = CsiMap::getCsiMap()->getW(idvec[i]);
    if(width<30)  ws+=evec[i];
    else wl+=evec[i];
  }
  ws/=edep;
  wl/=edep;
  double leakFactor = showerLeakage(thre,edep,ws,wl);
  
  gamma.setEnergy(edep*(1+leakFactor));
}


void E14GNAnaFunction::correctPosition(Gamma &gam){
  static double const Pcor[2]={6.49003,0.99254};
  static double const CsIX0=18.5;//mm
  if( gam.p3().x()==0 && gam.p3().y()==0 && gam.p3().z()==0 ){
    return;
  }
  double ene = gam.e();  
  double L = CsIX0*(Pcor[0]+Pcor[1]*log(ene/1000.));//mm
  double sinTheta = sin(gam.p3().theta());
  double newx = gam.coex()-L*sinTheta*cos(gam.p3().phi());
  double newy = gam.coey()-L*sinTheta*sin(gam.p3().phi());
  
  gam.setPos( newx, newy, gam.z());
}

void E14GNAnaFunction::correctEnergyWithAngle(Gamma &gamma){
  double thre = gamma.cluster().threshold();
  double ene = gamma.e();
  double edep = gamma.edep();
  double theta  = gamma.p3().theta();
  double ws=0,wl=0;
  std::vector<int> const &idvec = gamma.clusterIdVec();
  std::vector<double> const &evec = gamma.clusterEVec();
  
  int size = idvec.size();
  for(int i=0;i<size;i++){
    double width = CsiMap::getCsiMap()->getW(idvec[i]);
    if(width<30)  ws+=evec[i];
    else wl+=evec[i];
  }
  ws/=edep;
  wl/=edep;
  double leakFactor = showerLeakage2(thre,ene,theta,ws,wl);
  
  gamma.setEnergy(edep*(1+leakFactor));
}


void E14GNAnaFunction::shapeChi2( Gamma& gamma )
{
  static ShapeChi2 calculator;

  calculator.shapeChi2(gamma);
}

void E14GNAnaFunction::shapeANN( Gamma& gamma ,int CsiNumber,int *CsiId,double *CsiEne )
{
  static ShapeANN calculator;
  calculator.shapeANN(gamma,CsiNumber,CsiId,CsiEne);
}

void E14GNAnaFunction::correctEnergy(Pi0 &pi){
  correctEnergy(pi.g1());
  correctEnergy(pi.g2());
}

void E14GNAnaFunction::correctEnergy(Klong &kl ){
  for(std::vector<Pi0>::iterator it = kl.pi0().begin();
      it!= kl.pi0().end(); it++){
    for(int igam=0;igam<2;igam++){
      Gamma& gam = (igam==0) ? it->g1() : it->g2();
      correctEnergy(gam);
    }
  }
}

void E14GNAnaFunction::correctEnergyWithAngle(Pi0 &pi){
  correctEnergyWithAngle(pi.g1());
  correctEnergyWithAngle(pi.g2());
}

void E14GNAnaFunction::correctEnergyWithAngle(Klong &kl ){
  for(std::vector<Pi0>::iterator it = kl.pi0().begin();
      it!= kl.pi0().end(); it++){
    for(int igam=0;igam<2;igam++){
      Gamma& gam = (igam==0) ? it->g1() : it->g2();
      correctEnergyWithAngle(gam);
    }
  }
}


void E14GNAnaFunction::correctPosition(Pi0 &pi){
  for(int igam=0;igam<2;igam++){
    Gamma& gam = (igam==0) ? pi.g1() : pi.g2();
    correctPosition(gam);
  }
}

void E14GNAnaFunction::correctPosition(Klong &kl ){
  int npi = kl.pi0().size();
  for(int ipi=0; ipi<npi; ipi++){
    Pi0& pi0 = kl.pi0().at(ipi);
    for(int igam=0;igam<2;igam++){
      Gamma& gam = (igam==0) ? pi0.g1() : pi0.g2();
      correctPosition(gam);
    }
  }
}

void E14GNAnaFunction::shapeChi2(Pi0 &pi){
  for(int igam=0;igam<2;igam++){
    Gamma& gam = (igam==0) ? pi.g1() : pi.g2();
    shapeChi2(gam);
  }
}

void E14GNAnaFunction::shapeChi2(Klong &kl ){
  int npi = kl.pi0().size();
  for(int ipi=0; ipi<npi; ipi++){
    Pi0& pi0 = kl.pi0().at(ipi);
    for(int igam=0;igam<2;igam++){
      Gamma& gam = (igam==0) ? pi0.g1() : pi0.g2();
      shapeChi2(gam);
    }
  }
}

////////////////////////
//  private functions //
////////////////////////

/////////////////////
//for correctEnergy//
/////////////////////

double E14GNAnaFunction::showerLeakage(double thre,double E,double ws,double wl){
  static int isWarning = true;
  static double const par[5][6]
    ={{-0.0124228, 0.0535973, -0.00570087, -0.0272626, 0.0492056, -0.0143836},
      {0.0135769, 0.0516771, 0.00738265,-0.0200259, 0.0532812, -0.00951445},
      {0.0451242, 0.0446529, 0.0220706,-0.00882033, 0.0543621, -0.00272351},
      {0.0632153, 0.050921, 0.0275368,0.0051316, 0.0534536, 0.00466786},
      {0.0705192, 0.063754, 0.0273419,0.0162835, 0.0534304, 0.010156} };
  static double const parthre[5] = {1.5,3.0,5.0,7.5,10};
  
  int ID = 0;
  double weight = 1;
  
  if(thre<parthre[0]||thre>=parthre[4]){
    if(isWarning){
      std::cout<<"warning : CsI energy threshold=="<<thre<<". The function for gamma energy correction is not prepared in this range"<<std::endl;
      isWarning = false;
    }
    if(thre>=parthre[4]){
      ID = 3;
      weight = 0;
    }
  }else{
    bool isFound = false;
    for(int i=1;i<5;i++){
      if(thre<parthre[i]){
	ID = i-1;
	weight = (parthre[i]-thre)/(parthre[i]-parthre[i-1]);
	isFound = true;
	//	std::cout<<"i:"<<i<<" ID:"<<ID<<std::endl;
	break;
      }
    }
    if( !isFound ){
      std::cout<<"Gamma::showerLeakage() : Error"<<std::endl;
      std::cout<<thre<<" "<<E<<std::endl;
      exit(1);
    }
  }

  double f0 = ws*(par[ID][0]+par[ID][1]/sqrt(E/1000.)-par[ID][2]*log(E/1000.))
    +wl*(par[ID][3]+par[ID][4]/sqrt(E/1000.)-par[ID][5]*log(E/1000.));
  ID++;
  double f1 = ws*(par[ID][0]+par[ID][1]/sqrt(E/1000.)-par[ID][2]*log(E/1000.))
    +wl*(par[ID][3]+par[ID][4]/sqrt(E/1000.)-par[ID][5]*log(E/1000.));


  double f = weight*f0+(1-weight)*f1;
  
  return f;
}

double E14GNAnaFunction::showerLeakage2(double thre,double E,double theta,double ws,double wl){
  static int isFirstTime = true;
  static int isWarning = true;
  
  int const ncost = 11;
  double const minCost = 0.5;
  double const diffCost = 0.05;
  int const nEne = 30;
  double const minEne = 100;//MeV
  double diffEne = 100;//MeV
  int const nthre = 4;
  double const parthre[nthre] = {1.5,3.0,5.0,7.5};//MeV
  
  static TSpline3 *spl[4][2][ncost]={{{0}}};
  if(isFirstTime){
    isFirstTime = false;

    std::string input
      = std::string(std::getenv("MY_TOP_DIR"))
      + "/AnalysisLibrary/LegacyProjects/E14RecLibrary/gnana/data/correction_table_showerLeakage2.dat";
    std::ifstream ifs(input.c_str());
    if(!ifs){
      std::cout<<"error E14GNAnaFunction::showerLeakage2"<<std::endl; 
      std::cout<<"can't find data file."<<std::endl; 
      return 0;
    }
    std::string trash;
    double thre,cost;
    int sOrL;
    while(ifs>>trash>>thre>>trash>>sOrL>>trash>>cost){
      int icost = int((cost-minCost)/diffCost+0.1);
      int ithre=-1;
      for(int i=0;i<nthre;i++){
	if(fabs(thre-parthre[i])<1e-6) ithre = i;
      }
      if(ithre<0){
	std::cout<<"error E14GNAnaFunction::showerLeakage2"<<std::endl; 
	return 0;
      }
      double ene[nEne],leak[nEne];
      for(int i=0;i<nEne;i++){
	ifs>>ene[i]>>leak[i];
      }
      spl[ithre][sOrL][icost] = new TSpline3(Form("shower_leak_spl%d_%d_%d",ithre,sOrL,icost),ene,leak,nEne);
      
    }
  }

  double cost = cos(theta);
  int ID_cost = int((cost-minCost)/diffCost);
  double weight_cost  = 1+ID_cost-(cost-minCost)/diffCost;
  if(ID_cost<0){
    ID_cost = 0; weight_cost = 1;
  }else if(ID_cost>=ncost-1){
    ID_cost = ncost-2; weight_cost = 0;
  }
  
  int ID = 0;
  double weight = 1;
  if(thre<parthre[0]||thre>=parthre[nthre-1]){
    if(isWarning){
      std::cout<<"warning : CsI energy threshold=="<<thre<<". The function for gamma energy correction is not prepared in this range"<<std::endl;
      isWarning = false;
    }
    if(thre>=parthre[nthre-1]){
      ID = nthre-2;
      weight = 0;
    }
  }else{
    bool isFound = false;
    for(int i=1;i<nthre;i++){
      if(thre<parthre[i]){
	ID = i-1;
	weight = (parthre[i]-thre)/(parthre[i]-parthre[i-1]);
	isFound = true;
	break;
      }
    }
    if( !isFound ){
      std::cout<<"Gamma::showerLeakage2() : Error"<<std::endl;
      std::cout<<thre<<" "<<E<<std::endl;
      exit(1);
    }
  }

  if(E<minEne) E = minEne;
  if(E>=minEne+diffEne*(nEne-1)) E = minEne+diffEne*(nEne-1);
  double f[2][2] = {{0}};
  for(int i=0;i<2;i++){
    for(int j=0;j<2;j++){
      int ithre = ID+i;
      int icost = ID_cost+j;
      //      f[i][j] = ws*spl[ithre][0][icost]->Eval(E)+wl*spl[ithre][1][icost]->Eval(E);
      f[i][j] = getCorFactor(ithre,icost,wl,E,spl[ithre][0][icost]->Eval(E),
      			     spl[ithre][1][icost]->Eval(E));
    }
  }
  double mean = weight_cost*(weight*f[0][0]+(1-weight)*f[1][0])
    + (1-weight_cost)*(weight*f[0][1]+(1-weight)*f[1][1]);
  
  return mean;
}


double E14GNAnaFunction::getCorFactor(int ithre,int icost,double wl,double e,double cors,double corl){
  int const ncost = 11;
  int const nthre = 4;
  int const nkind=3;
  int const nparam=4;

  static double *param[nthre][ncost][3]={{{0}}};
  if(param[0][0][0]==0){
    std::string input
      = std::string(std::getenv("MY_TOP_DIR"))
      + "/AnalysisLibrary/LegacyProjects/E14RecLibrary/gnana/data/correction_table_large-small_boundary.dat";
    std::ifstream ifs(input.c_str());
    for(int i=0;i<nthre;i++){
      for(int j=0;j<ncost;j++){
        for(int k=0;k<nkind;k++){
          param[i][j][k] = new double[nparam];
        }
      }
    }
    std::string str;
    while(std::getline(ifs,str)){
      std::stringstream ss(str);
      int i,j,k;
      ss>>str;      if(str=="#") continue;
      i = atoi(str.c_str());
      ss>>j>>k;
      for(int ipar=0;ipar<nparam;ipar++)
        ss>>param[i][j][k][ipar];
    }
  }

  double *par = param[ithre][icost][0];
  double slope1 = par[0]/e+par[1]*log(par[2]*e)+par[3];
  par = param[ithre][icost][1];
  double slope2 = par[0]/e+par[1]*log(par[2]*e)+par[3];
  par = param[ithre][icost][2];
  double offset = par[0]/e+par[1]*log(par[2]*e)+par[3];
  double f;
  if(wl<0.5){
    if(wl<1e-7) f = cors;
    else {
      double x = log(wl);
      f = offset+slope1*(x+log(2));
      if(f>cors) f = cors;
    }
  }else{
    if(fabs(wl-1)<1e-7) f = corl;
    else {
      double x = -log(1-wl);
      f = offset+slope2*(x-log(2));
      if(f<corl) f = corl;
    }
  }

  return f;

}

double E14GNAnaFunction::cfit_klpi0pi0X(Klong &KL)
{
        double T1TargetZ = MTBP::T1TargetZ;
        double CSIZPosition = MTBP::CSIZPosition;
        double MASS_PI0 = MTBP::Pi0_MASS;
        double chisq_keep   = HUGE;
        double chisq_keep_v = HUGE;
        double chisq = -1.0;

        int ifail = 0;
        int error_flag = 0;
        int fit_updated = 0;
        int npi0s = 3; // really 2, but with 2 extra gammas
        int counter; 

        double px1[3];
        double py1[3];
        double pz1[3];
        double E1[3];
        double x1[3];
        double y1[3];
        double z1[3];
        double sx1[3];
        double sy1[3];
        double sE1[3];
        double R1[3];

        double px2[3];
        double py2[3];
        double pz2[3];
        double E2[3];
        double x2[3];
        double y2[3];
        double z2[3];
        double sx2[3];
        double sy2[3];
        double sE2[3];
        double R2[3];

        double klpx, klpy, klpz, klE, klE0 = 0.;
        double vx, vy, vz;

        // Prepare initial values
        int idx;
        for( idx=0; idx<npi0s; idx++) {
                x1[idx] = KL.pi0()[idx].g1().x();
                y1[idx] = KL.pi0()[idx].g1().y();
                z1[idx] = KL.pi0()[idx].g1().z();
                E1[idx] = KL.pi0()[idx].g1().e();
                sx1[idx] = KL.pi0()[idx].g1().sigmaX();
                sy1[idx] = KL.pi0()[idx].g1().sigmaY();
                sE1[idx] = KL.pi0()[idx].g1().sigmaE();

                x2[idx] = KL.pi0()[idx].g2().x();
                y2[idx] = KL.pi0()[idx].g2().y();
                z2[idx] = KL.pi0()[idx].g2().z();
                E2[idx] = KL.pi0()[idx].g2().e();
                sx2[idx] = KL.pi0()[idx].g2().sigmaX();
                sy2[idx] = KL.pi0()[idx].g2().sigmaY();
                sE2[idx] = KL.pi0()[idx].g2().sigmaE();

                klE0 += E1[idx] + E2[idx];
        }
        vx = KL.vx();
        vy = KL.vy();
        vz = KL.vz();


        TVectorD a0(npi0s*6);
        TVectorD a(npi0s*6);
        TVectorD v0(3);
        TVectorD v(3);
        for( idx=0; idx<npi0s; idx++) {
                a0(idx*6 + 0)  = x1[idx]; 
                a0(idx*6 + 1)  = y1[idx];
                a0(idx*6 + 2)  = E1[idx];
                a0(idx*6 + 3)  = x2[idx];
                a0(idx*6 + 4)  = y2[idx];
                a0(idx*6 + 5)  = E2[idx];
        }
        v0(0) = vx;
        v0(1) = vy;
        v0(2) = vz;
        TMatrixDSym Va0(npi0s*6);  // (npi0s*6) x (npi0s*6)

        for( idx=0; idx<npi0s; idx++) {
                Va0(idx*6+0 ,idx*6+0) = pow(sx1[idx],2);
                Va0(idx*6+1 ,idx*6+1) = pow(sy1[idx],2);
                Va0(idx*6+2 ,idx*6+2) = pow(sE1[idx],2);
                Va0(idx*6+3 ,idx*6+3) = pow(sx2[idx],2);
                Va0(idx*6+4 ,idx*6+4) = pow(sy2[idx],2);
                Va0(idx*6+5 ,idx*6+5) = pow(sE2[idx],2);
        }
        a = a0;
        v = v0;

        TMatrixD D(npi0s+2,npi0s*6); // = del H/del alpha(alpha_A)
        TMatrixD E(npi0s+2,3);      // = del H/del alpha(v) //remove ,0
        TVectorD d(npi0s+2); // = H(alpha_A)

        TVectorD da0(npi0s*6);
        TVectorD dv0(3);

        TMatrixD VD(npi0s+2,npi0s+2);
        TMatrixD VE(3,3);
        TVectorD Lambda0(npi0s+2);
        TVectorD Lambda(npi0s+2);
        for (int k = 0; k < npi0s + 2; k++){ //initialize Lambda vectors to be 1 
           Lambda0(k) = 1;
           Lambda(k) = 1;
        }
 
        TVectorD v_keep(3);
        v_keep = v;
 
        TMatrixD VE_keep(3,3);
        VE_keep = VE;

        TMatrixD VD_keep(npi0s+2,npi0s+2);
        VD_keep = VD;

        TVectorD Lambda0_keep(npi0s+2);
        Lambda0_keep = Lambda0;

        TMatrixD E_keep(npi0s+2, 3);
        E_keep = E;

        TMatrixD D_keep(npi0s+2,npi0s*6);
        D_keep = D;

        TVectorD a_keep(npi0s*6);
        a_keep = a;

        TVectorD v_keep_v(3);
        v_keep_v = v;

        TMatrixD VE_keep_v(3,3);
        VE_keep_v = VE;

        TMatrixD VD_keep_v(npi0s+2,npi0s+2);
        VD_keep_v = VD;

        TVectorD Lambda0_keep_v(npi0s+2);
        Lambda0_keep_v = Lambda0;

        TMatrixD E_keep_v(npi0s+2, 3);
        E_keep_v = E;

        TMatrixD D_keep_v(npi0s+2,npi0s*6);
        D_keep_v = D;        

//=============================================================
//        // start minizing loop

        int it,it_v;
        for(it=0;it<MAX_ITERACTIONS;it++) {             // Global minizing loop
        chisq_keep_v = HUGE;                            // reset chisq_keep_v
        for(it_v=0;it_v<MAX_ITERACTIONS;it_v++) {       // KL vertex (v) minizing loop
        v0 = v;         // set initial vertex to be expanding point
        error_flag = 0; // reset error flag

        double dpx1_dx1[3], dpx2_dx2[3];
        double dpx1_dy1[3], dpx2_dy2[3];
        double dpx1_dE1[3], dpx2_dE2[3];

        double dpy1_dx1[3], dpy2_dx2[3];
        double dpy1_dy1[3], dpy2_dy2[3];
        double dpy1_dE1[3], dpy2_dE2[3];

        double dpz1_dx1[3], dpz2_dx2[3];
        double dpz1_dy1[3], dpz2_dy2[3];
        double dpz1_dE1[3], dpz2_dE2[3];

        double dR1_dx1[3],  dR2_dx2[3];
        double dR1_dy1[3],  dR2_dy2[3];
        double dR1_dE1[3],  dR2_dE2[3];

        double dpx1_dvx[3], dpx2_dvx[3];
        double dpx1_dvy[3], dpx2_dvy[3];
        double dpx1_dvz[3], dpx2_dvz[3];

        double dpy1_dvx[3], dpy2_dvx[3];
        double dpy1_dvy[3], dpy2_dvy[3];
        double dpy1_dvz[3], dpy2_dvz[3];

        double dpz1_dvx[3], dpz2_dvx[3];
        double dpz1_dvy[3], dpz2_dvy[3];
        double dpz1_dvz[3], dpz2_dvz[3];

        double dR1_dvx[3],  dR2_dvx[3];
        double dR1_dvy[3],  dR2_dvy[3];
        double dR1_dvz[3],  dR2_dvz[3];

        double dklpx_dvx, dklpy_dvx, dklpz_dvx;
        double dklpx_dvy, dklpy_dvy, dklpz_dvy;
        double dklpx_dvz, dklpy_dvz, dklpz_dvz;

        klpx = klpy = klpz = klE = 0.;
        dklpx_dvx = dklpy_dvx = dklpz_dvx = 0.;
        dklpx_dvy = dklpy_dvy = dklpz_dvy = 0.;
        dklpx_dvz = dklpy_dvz = dklpz_dvz = 0.;
        for( idx=0; idx<npi0s; idx++) {

                R1[idx] = sqrt(pow(x1[idx]-vx,2) + pow(y1[idx]-vy,2) + pow(z1[idx]-vz,2));
                R2[idx] = sqrt(pow(x2[idx]-vx,2) + pow(y2[idx]-vy,2) + pow(z2[idx]-vz,2));

                px1[idx] = (x1[idx]-vx)/R1[idx] * E1[idx];
                py1[idx] = (y1[idx]-vy)/R1[idx] * E1[idx];
                pz1[idx] = (z1[idx]-vz)/R1[idx] * E1[idx];

                px2[idx] = (x2[idx]-vx)/R2[idx] * E2[idx];
                py2[idx] = (y2[idx]-vy)/R2[idx] * E2[idx];
                pz2[idx] = (z2[idx]-vz)/R2[idx] * E2[idx];

                klpx += px1[idx] + px2[idx];
                klpy += py1[idx] + py2[idx];
                klpz += pz1[idx] + pz2[idx];
                klE  += E1[idx]  + E2[idx];

                dR1_dx1[idx] = (x1[idx] - vx) / R1[idx];
                dR1_dy1[idx] = (y1[idx] - vy) / R1[idx];
                dR1_dE1[idx] = 0;

                dpx1_dx1[idx] = (E1[idx] - px1[idx] * dR1_dx1[idx])/R1[idx];
                dpx1_dy1[idx] = (        - px1[idx] * dR1_dy1[idx])/R1[idx];
                dpx1_dE1[idx] = (x1[idx] - vx) / R1[idx];

                dpy1_dx1[idx] = (        - py1[idx] * dR1_dx1[idx])/R1[idx];
                dpy1_dy1[idx] = (E1[idx] - py1[idx] * dR1_dy1[idx])/R1[idx];
                dpy1_dE1[idx] = (y1[idx] - vy) / R1[idx];

                dpz1_dx1[idx] = (        - pz1[idx] * dR1_dx1[idx])/R1[idx];
                dpz1_dy1[idx] = (        - pz1[idx] * dR1_dy1[idx])/R1[idx];
                dpz1_dE1[idx] = (z1[idx] - vz) / R1[idx];

                dR2_dx2[idx] = (x2[idx] - vx) / R2[idx];
                dR2_dy2[idx] = (y2[idx] - vy) / R2[idx];
                dR2_dE2[idx] = 0;
               
                dpx2_dx2[idx] = (E2[idx] - px2[idx] * dR2_dx2[idx])/R2[idx];
                dpx2_dy2[idx] = (        - px2[idx] * dR2_dy2[idx])/R2[idx];
                dpx2_dE2[idx] = (x2[idx] - vx) / R2[idx];

                dpy2_dx2[idx] = (        - py2[idx] * dR2_dx2[idx])/R2[idx];
                dpy2_dy2[idx] = (E2[idx] - py2[idx] * dR2_dy2[idx])/R2[idx];
                dpy2_dE2[idx] = (y2[idx] - vy) / R2[idx];

                dpz2_dx2[idx] = (        - pz2[idx] * dR2_dx2[idx])/R2[idx];
                dpz2_dy2[idx] = (        - pz2[idx] * dR2_dy2[idx])/R2[idx];
                dpz2_dE2[idx] = (z2[idx] - vz) / R2[idx];

                dR1_dvx[idx] = -(x1[idx] - vx) / R1[idx];
                dR1_dvy[idx] = -(y1[idx] - vy) / R1[idx];
                dR1_dvz[idx] = -(z1[idx] - vz) / R1[idx];

                dpx1_dvx[idx] = (-E1[idx] - px1[idx] * dR1_dvx[idx])/R1[idx];
                dpx1_dvy[idx] = (         - px1[idx] * dR1_dvy[idx])/R1[idx];
                dpx1_dvz[idx] = (         - px1[idx] * dR1_dvz[idx])/R1[idx];

                dpy1_dvx[idx] = (         - py1[idx] * dR1_dvx[idx])/R1[idx];
                dpy1_dvy[idx] = (-E1[idx] - py1[idx] * dR1_dvy[idx])/R1[idx];
                dpy1_dvz[idx] = (         - py1[idx] * dR1_dvz[idx])/R1[idx];

                dpz1_dvx[idx] = (         - pz1[idx] * dR1_dvx[idx])/R1[idx];
                dpz1_dvy[idx] = (         - pz1[idx] * dR1_dvy[idx])/R1[idx];
                dpz1_dvz[idx] = (-E1[idx] - pz1[idx] * dR1_dvz[idx])/R1[idx];

                dR2_dvx[idx] = -(x2[idx] - vx) / R2[idx];
                dR2_dvy[idx] = -(y2[idx] - vy) / R2[idx];
                dR2_dvz[idx] = -(z2[idx] - vz) / R2[idx];

                dpx2_dvx[idx] = (-E2[idx] - px2[idx] * dR2_dvx[idx])/R2[idx];
                dpx2_dvy[idx] = (         - px2[idx] * dR2_dvy[idx])/R2[idx];
                dpx2_dvz[idx] = (         - px2[idx] * dR2_dvz[idx])/R2[idx];

                dpy2_dvx[idx] = (         - py2[idx] * dR2_dvx[idx])/R2[idx];
                dpy2_dvy[idx] = (-E2[idx] - py2[idx] * dR2_dvy[idx])/R2[idx];
                dpy2_dvz[idx] = (         - py2[idx] * dR2_dvz[idx])/R2[idx];

                dpz2_dvx[idx] = (         - pz2[idx] * dR2_dvx[idx])/R2[idx];
                dpz2_dvy[idx] = (         - pz2[idx] * dR2_dvy[idx])/R2[idx];
                dpz2_dvz[idx] = (-E2[idx] - pz2[idx] * dR2_dvz[idx])/R2[idx];

                dklpx_dvx += (dpx1_dvx[idx] + dpx2_dvx[idx]);
                dklpx_dvy += (dpx1_dvy[idx] + dpx2_dvy[idx]);
                dklpx_dvz += (dpx1_dvz[idx] + dpx2_dvz[idx]);

                dklpy_dvx += (dpy1_dvx[idx] + dpy2_dvx[idx]);
                dklpy_dvy += (dpy1_dvy[idx] + dpy2_dvy[idx]);
                dklpy_dvz += (dpy1_dvz[idx] + dpy2_dvz[idx]);

                dklpz_dvx += (dpz1_dvx[idx] + dpz2_dvx[idx]);
                dklpz_dvy += (dpz1_dvy[idx] + dpz2_dvy[idx]);
                dklpz_dvz += (dpz1_dvz[idx] + dpz2_dvz[idx]);
        }
        for( idx=0; idx<npi0s-1; idx++) {
                d(idx) =   pow(E1[idx]  + E2[idx] ,2)
                           - pow(px1[idx] + px2[idx],2)
                           - pow(py1[idx] + py2[idx],2)
                           - pow(pz1[idx] + pz2[idx],2)
                           - MASS_PI0*MASS_PI0;
        }
        d(npi0s-1) = klE*klE - klpx*klpx - klpy*klpy - klpz*klpz - MASS_KL*MASS_KL;

	d(npi0s+0) = - (CSIZPosition - T1TargetZ)*vx*klE; // length to Csi
        d(npi0s+1) = - (CSIZPosition - T1TargetZ)*vy*klE; //T1TargetZ is negative

        double ce_x = 0 ;
        double ce_y = 0 ;


        for( idx=0; idx<npi0s; idx++) {
                d(npi0s+0) += (vz-T1TargetZ)*(E1[idx]*x1[idx] + E2[idx]*x2[idx]);
                d(npi0s+1) += (vz-T1TargetZ)*(E1[idx]*y1[idx] + E2[idx]*y2[idx]);
                ce_x += E1[idx]*x1[idx] + E2[idx]*x2[idx];
                ce_y += E1[idx]*y1[idx] + E2[idx]*y2[idx];
        }
        //KL.setCx(ce_x/klE);
        //KL.setCy(ce_y/klE);
        //
        // Reset core matrix D & E


        for(int i=0;i<npi0s+2;i++) {
                for(int j=0;j<npi0s*6;j++)     D(i,j) = 0.;
                for(int j=0;j<3;j++)           E(i,j) = 0.;
        }
        // Calculate core matrix D & E
        for( idx=0; idx<npi0s-1; idx++) {

        //d(idx+1) =   pow(E1[idx]  + E2[idx] ,2)
        //           - pow(px1[idx] + px2[idx],2)
        //           - pow(py1[idx] + py2[idx],2)
        //           - pow(pz1[idx] + pz2[idx],2)
        //           - MASS_PI0*MASS_PI0;



        D(idx+0,idx*6+0) = -2.*(px1[idx] + px2[idx])*dpx1_dx1[idx]
                           -2.*(py1[idx] + py2[idx])*dpy1_dx1[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz1_dx1[idx]; // del x1
        D(idx+0,idx*6+1) = -2.*(px1[idx] + px2[idx])*dpx1_dy1[idx]
                           -2.*(py1[idx] + py2[idx])*dpy1_dy1[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz1_dy1[idx]; // del y1
        D(idx+0,idx*6+2) =  2.*(E1[idx]  + E2[idx])
                           -2.*(px1[idx] + px2[idx])*dpx1_dE1[idx]
                           -2.*(py1[idx] + py2[idx])*dpy1_dE1[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz1_dE1[idx]; // del E1

        D(idx+0,idx*6+3) = -2.*(px1[idx] + px2[idx])*dpx2_dx2[idx]
                           -2.*(py1[idx] + py2[idx])*dpy2_dx2[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz2_dx2[idx]; // del x2
        D(idx+0,idx*6+4) = -2.*(px1[idx] + px2[idx])*dpx2_dy2[idx]
                           -2.*(py1[idx] + py2[idx])*dpy2_dy2[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz2_dy2[idx]; // del y2
        D(idx+0,idx*6+5) =  2.*(E1[idx]  + E2[idx])
                           -2.*(px1[idx] + px2[idx])*dpx2_dE2[idx]
                           -2.*(py1[idx] + py2[idx])*dpy2_dE2[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz2_dE2[idx]; // del E2   

        E(idx+0,0)  = -2.*(px1[idx] + px2[idx])*(dpx1_dvx[idx] + dpx2_dvx[idx])
                      -2.*(py1[idx] + py2[idx])*(dpy1_dvx[idx] + dpy2_dvx[idx])
                      -2.*(pz1[idx] + pz2[idx])*(dpz1_dvx[idx] + dpz2_dvx[idx]); // del vx
        E(idx+0,1)  = -2.*(px1[idx] + px2[idx])*(dpx1_dvy[idx] + dpx2_dvy[idx])
                      -2.*(py1[idx] + py2[idx])*(dpy1_dvy[idx] + dpy2_dvy[idx])
                      -2.*(pz1[idx] + pz2[idx])*(dpz1_dvy[idx] + dpz2_dvy[idx]); // del vy
        E(idx+0,2)  = -2.*(px1[idx] + px2[idx])*(dpx1_dvz[idx] + dpx2_dvz[idx])
                      -2.*(py1[idx] + py2[idx])*(dpy1_dvz[idx] + dpy2_dvz[idx])
                      -2.*(pz1[idx] + pz2[idx])*(dpz1_dvz[idx] + dpz2_dvz[idx]); // del vz
        }

        for( idx=0; idx<npi0s; idx++) {
        // d(npi0s+1) = klE*klE - klpx*klpx - klpy*klpy - klpz*klpz - MASS_KL*MASS_KL;
        D(npi0s-1  ,idx*6+0) = -2.* klpx * dpx1_dx1[idx]
                             -2.* klpy * dpy1_dx1[idx]
                             -2.* klpz * dpz1_dx1[idx]; // del x1
        D(npi0s-1  ,idx*6+1) = -2.* klpx * dpx1_dy1[idx]
                             -2.* klpy * dpy1_dy1[idx]
                             -2.* klpz * dpz1_dy1[idx]; // del y1
        D(npi0s-1  ,idx*6+2) =  2.* klE
                             -2.* klpx * dpx1_dE1[idx]
                             -2.* klpy * dpy1_dE1[idx]
                             -2.* klpz * dpz1_dE1[idx]; // del E1

        D(npi0s-1  ,idx*6+3) = -2.* klpx * dpx2_dx2[idx]
                             -2.* klpy * dpy2_dx2[idx]
                             -2.* klpz * dpz2_dx2[idx]; // del x2
        D(npi0s-1  ,idx*6+4) = -2.* klpx * dpx2_dy2[idx]
                             -2.* klpy * dpy2_dy2[idx]
                             -2.* klpz * dpz2_dy2[idx]; // del y2
        D(npi0s-1  ,idx*6+5) =  2.* klE
                             -2.* klpx * dpx2_dE2[idx]
                             -2.* klpy * dpy2_dE2[idx]
                             -2.* klpz * dpz2_dE2[idx]; // del E2

        E(npi0s-1  ,0)  = -2.* klpx * dklpx_dvx
                        -2.* klpy * dklpy_dvx
                        -2.* klpz * dklpz_dvx; // del vx
        E(npi0s-1 ,1)  = -2.* klpx * dklpx_dvy
                        -2.* klpy * dklpy_dvy
                        -2.* klpz * dklpz_dvy; // del vy
        E(npi0s-1  ,2)  = -2.* klpx * dklpx_dvz
                        -2.* klpy * dklpy_dvz
                        -2.* klpz * dklpz_dvz; // del vz






        //      d(npi0s+2) += sum(E1[idx]*x1[idx] + E2[idx]*x2[idx]) - vx*klE;
        //      d(npi0s+3) += sum(E1[idx]*y1[idx] + E2[idx]*y2[idx]) - vy*klE;





        D(npi0s,idx*6+0) = (vz-T1TargetZ)*E1[idx];            // del x1
        D(npi0s,idx*6+1) = 0.;                // del y1
        D(npi0s,idx*6+2) = (vz-T1TargetZ)*x1[idx] - (CSIZPosition - T1TargetZ)*vx;    // del E1

        D(npi0s,idx*6+3) = (vz-T1TargetZ)*E2[idx];            // del x2
        D(npi0s,idx*6+4) = 0.;                // del y2
        D(npi0s,idx*6+5) = (vz-T1TargetZ)*x2[idx] - (CSIZPosition - T1TargetZ)*vx;    // del E2

        E(npi0s,0)  = -(CSIZPosition - T1TargetZ)*klE;                        // del vx
        E(npi0s,1)  = 0.;                     // del vy
        E(npi0s,2)  = ce_x;                   // del vz

        D(npi0s+1,idx*6+0) = 0.;                // del x1
        D(npi0s+1,idx*6+1) = (vz-T1TargetZ)*E1[idx];            // del y1
        D(npi0s+1,idx*6+2) = (vz-T1TargetZ)*y1[idx] - (CSIZPosition - T1TargetZ)*vy;    // del E1

        D(npi0s+1,idx*6+3) = 0.;                // del x2
        D(npi0s+1,idx*6+4) = (vz-T1TargetZ)*E2[idx];            // del y2
        D(npi0s+1,idx*6+5) = (vz-T1TargetZ)*y2[idx] - (CSIZPosition - T1TargetZ)*vy;    // del E2

        E(npi0s+1,0)  = 0.;                     // del vx
        E(npi0s+1,1)  = -(CSIZPosition - T1TargetZ)*klE;                        // del vy
        E(npi0s+1,2)  = ce_y;                   // del vz

        }
        da0 = a0 - a;
        dv0 = v0 - v;

        TMatrixD Dt(npi0s*6,npi0s+2);
        D.T(); //transpose D
        Dt = D; //set holding matrix
        D.T(); //transpose back
    






        TMatrixD Et(3, npi0s+2);
        E.T();
        Et = E;
        E.T();
        
        VD = (D*Va0*Dt);
        //std::cout << " HERE IS VD PRE INVERT " << std::endl << std::endl;
        //for(int j = 0; j < npi0s + 2; j++){
        //   for(int k = 0; k < npi0s + 2; k++){
        //      std::cout << VD(j,k) << "  "; 

        //   }
        //   std::cout << std::endl;
        //} 
        VD.Invert();
        //std::cout << " HERE IS VD POST INVERT " << std::endl << std::endl;
        //for(int j = 0; j < npi0s + 2; j++){
        //   for(int k = 0; k < npi0s + 2; k++){
        //      std::cout << VD(j,k) << "  "; 
        //   }
        //   std::cout << std::endl;
        //}
        VE = (Et*VD*E);
        //std::cout << " HERE IS VE PRE INVERT " << std::endl << std::endl;
        //for(int j = 0; j < 3; j++){
        //   for(int k = 0; k < 3; k++){
        //      std::cout << VE(j,k) << "  "; 

        //   }
        //   std::cout << std::endl;
        //} 
        VE.Invert();
        //std::cout << " HERE IS VE POST INVERT " << std::endl << std::endl;
        //for(int j = 0; j < 3; j++){
        //   for(int k = 0; k < 3; k++){
        //      std::cout << VE(j,k) << "  "; 
        //   }
        //   std::cout << std::endl;
        //}
        Lambda0 = (VD*(D*da0 + d));
        //std::cout << " HERE IS LAMBDA VECTOR " << std::endl << std::endl;
        //for(int j = 0; j < npi0s + 2; j++){
        //   std::cout << Lambda0(j) << std::endl;
        //}       
        //std::cout << std::endl;
        TVectorD unit(2);
        unit(0) = 1;
        unit(1) = 1;
        TVectorD chisq_vec(1);

        //std::cout << std::endl << std::endl << "HERE IS THE D VECTOR" << std::endl;
        //for(int j = 0; j < npi0s+2; j++){
        //   std::cout << d(j) << std::endl;
        //}
 
        chisq_vec(0) = (Lambda0 * (D*da0 + E*dv0 + d));
        chisq = chisq_vec(0);
        TVectorD subv(3);
        subv = VE * Et * Lambda0;
        v = v-subv;
        if (chisq < chisq_keep_v && error_flag==0 && chisq>=0.) {
                chisq_keep_v    = chisq;
                v_keep_v        = v;
                VE_keep_v       = VE;
                VD_keep_v       = VD;
                Lambda0_keep_v  = Lambda0;
                E_keep_v        = E;
                D_keep_v        = D;
                fit_updated     = 1;
        }else {
                        chisq   = chisq_keep_v;
                        v       = v_keep_v;
                        VE      = VE_keep_v;
                        VD      = VD_keep_v;
                        Lambda0 = Lambda0_keep_v;
                        E       = E_keep_v;
                        D       = D_keep_v;
                        break;
        }


        // fill back KL vertex first


        vx = v(0);
        vy = v(1);
        vz = v(2);

        } // end of KL vertex minizing loop
        TMatrixD Dt(npi0s*6,npi0s+2);
        D.T();
        Dt = D;
        D.T();

        TMatrixD Et(3, npi0s+2);
        E.T();
        Et = E;
        E.T();

        TVectorD suba(npi0s*6);
        suba = Va0 * Dt * Lambda0;
        if (error_flag==0) {
                Lambda  = Lambda0 - VD * E * VE * Et * Lambda0;
                a = a0 - suba;
        }

        if ((it == 0 || chisq < chisq_keep) && error_flag==0 && chisq>=0.) {
                chisq_keep      = chisq;
                v_keep          = v;
                VE_keep         = VE;
                VD_keep         = VD;
                Lambda0_keep    = Lambda0;
                E_keep          = E;
                D_keep          = D;
                a_keep          = a;
                fit_updated     = 1;
        }else {
                chisq   = chisq_keep;
                v       = v_keep;
                VE      = VE_keep;
                VD      = VD_keep;
                Lambda0 = Lambda0_keep;
                E       = E_keep;
                D       = D_keep;
                a       = a_keep;
                break;
        }

        // fill back other parameters
        for( idx=0; idx<npi0s; idx++) {
                x1[idx] = a(idx*6 + 0);
                y1[idx] = a(idx*6 + 1);
                E1[idx] = a(idx*6 + 2);
                x2[idx] = a(idx*6 + 3);
                y2[idx] = a(idx*6 + 4);
                E2[idx] = a(idx*6 + 5);
        }
        } // end of global minizing loop

        if (fit_updated == 0) { // fit failed.
                fprintf(stderr,"[ERROR] : Fit failed (%d), return error code = %d.\n",ifail,error_flag);
                chisq = -1.;
                if (error_flag!=0) return (double)error_flag;
                else return -3.;
        }
        //reset val
        klpx = 0;
        klpy = 0;
        klpz = 0;
        klE = 0;       
        for( idx=0; idx<npi0s; idx++) {

                R1[idx] = sqrt(pow(x1[idx]-vx,2) + pow(y1[idx]-vy,2) + pow(z1[idx]-vz,2));
                R2[idx] = sqrt(pow(x2[idx]-vx,2) + pow(y2[idx]-vy,2) + pow(z2[idx]-vz,2));

                px1[idx] = (x1[idx]-vx)/R1[idx] * E1[idx];
                py1[idx] = (y1[idx]-vy)/R1[idx] * E1[idx];
                pz1[idx] = (z1[idx]-vz)/R1[idx] * E1[idx];

                px2[idx] = (x2[idx]-vx)/R2[idx] * E2[idx];
                py2[idx] = (y2[idx]-vy)/R2[idx] * E2[idx];
                pz2[idx] = (z2[idx]-vz)/R2[idx] * E2[idx];

                klpx += px1[idx] + px2[idx];
                klpy += py1[idx] + py2[idx];
                klpz += pz1[idx] + pz2[idx];
                klE  += E1[idx]  + E2[idx];
                if(idx == npi0s - 1){counter++;}
        }

        TMatrixD Dt(npi0s*6,npi0s+2);
        D.T();
        Dt = D;
        D.T();

        TMatrixD Et(3, npi0s+2);
        E.T();
        Et = E;
        E.T();
        // Final error matrix

        TMatrixD VDtu = VD - VD * E * VE * Et * VD;
        TMatrixD Va = Va0 - Va0 * Dt * VDtu * D * Va0;

        KL.setMass(sqrt(klE*klE-klpx*klpx-klpy*klpy-klpz*klpz));
        KL.setEnergy(klE);
        KL.setP3(klpx,klpy,klpz);
        KL.setVtx(vx,vy,vz);
        KL.setChisqZ(chisq);
      // npi0s =3 here, I had checked
        for( idx=0; idx<npi0s; idx++) {
                KL.pi0()[idx].setEnergy(E1[idx]+E2[idx]);
                KL.pi0()[idx].setMass(sqrt(pow(E1[idx]+E2[idx],2) -
                                           pow(px1[idx]+px2[idx],2) -
                                           pow(py1[idx]+py2[idx],2) -
                                           pow(pz1[idx]+pz2[idx],2)));

                KL.pi0()[idx].setVtx(vx,vy,vz);
                KL.pi0()[idx].setP3(px1[idx]+px2[idx],py1[idx]+py2[idx],pz1[idx]+pz2[idx]);
                KL.pi0()[idx].setRecZ(vz);
                KL.pi0()[idx].setRecZsig2(VE(2,2));

                KL.pi0()[idx].g1().setEnergy(E1[idx]);
                KL.pi0()[idx].g1().setPos(x1[idx],y1[idx],z1[idx]);
                KL.pi0()[idx].g1().setP3(px1[idx],py1[idx],pz1[idx]);

                KL.pi0()[idx].g2().setEnergy(E2[idx]);
                KL.pi0()[idx].g2().setPos(x2[idx],y2[idx],z2[idx]);
                KL.pi0()[idx].g2().setP3(px2[idx],py2[idx],pz2[idx]);
//std::cout << KL.pi0()[idx].g1().p3().mag() <<std::endl;
//std::cout << KL.pi0()[idx].g2().p3().mag() <<std::endl;
//
//
//              KL.pi0()[idx].setAngle(KL.pi0()[idx].g1().p3(),
//              KL.pi0()[idx].g2().p3());



        }

        return chisq;
}



double E14GNAnaFunction::cfit_kl3pi0(Klong &KL)
{
        double T1TargetZ = MTBP::T1TargetZ;
        double CSIZPosition = MTBP::CSIZPosition;
        double MASS_PI0 = MTBP::Pi0_MASS;
        double chisq_keep   = HUGE;
        double chisq_keep_v = HUGE;
        double chisq = -1.0;

        int ifail = 0;
        int error_flag = 0;
        int fit_updated = 0;
        int npi0s = 3; // really 2, but with 2 extra gammas


        double px1[3];
        double py1[3];
        double pz1[3];
        double E1[3];
        double x1[3];
        double y1[3];
        double z1[3];
        double sx1[3];
        double sy1[3];
        double sE1[3];
        double R1[3];

        double px2[3];
        double py2[3];
        double pz2[3];
        double E2[3];
        double x2[3];
        double y2[3];
        double z2[3];
        double sx2[3];
        double sy2[3];
        double sE2[3];
        double R2[3];

        double klpx, klpy, klpz, klE, klE0 = 0.;
        double vx, vy, vz;

        // Prepare initial values
        int idx;
        for( idx=0; idx<npi0s; idx++) {
                x1[idx] = KL.pi0()[idx].g1().x();
                y1[idx] = KL.pi0()[idx].g1().y();
                z1[idx] = KL.pi0()[idx].g1().z();
                E1[idx] = KL.pi0()[idx].g1().e();
                sx1[idx] = KL.pi0()[idx].g1().sigmaX();
                sy1[idx] = KL.pi0()[idx].g1().sigmaY();
                sE1[idx] = KL.pi0()[idx].g1().sigmaE();

                x2[idx] = KL.pi0()[idx].g2().x();
                y2[idx] = KL.pi0()[idx].g2().y();
                z2[idx] = KL.pi0()[idx].g2().z();
                E2[idx] = KL.pi0()[idx].g2().e();
                sx2[idx] = KL.pi0()[idx].g2().sigmaX();
                sy2[idx] = KL.pi0()[idx].g2().sigmaY();
                sE2[idx] = KL.pi0()[idx].g2().sigmaE();

                klE0 += E1[idx] + E2[idx];
        }
        vx = KL.vx();
        vy = KL.vy();
        vz = KL.vz();


        TVectorD a0(npi0s*6);
        TVectorD a(npi0s*6);
        TVectorD v0(3);
        TVectorD v(3);
        for( idx=0; idx<npi0s; idx++) {
                a0(idx*6 + 0)  = x1[idx]; 
                a0(idx*6 + 1)  = y1[idx];
                a0(idx*6 + 2)  = E1[idx];
                a0(idx*6 + 3)  = x2[idx];
                a0(idx*6 + 4)  = y2[idx];
                a0(idx*6 + 5)  = E2[idx];
        }
        v0(0) = vx;
        v0(1) = vy;
        v0(2) = vz;
        TMatrixDSym Va0(npi0s*6);  // (npi0s*6) x (npi0s*6)

        for( idx=0; idx<npi0s; idx++) {
                Va0(idx*6+0 ,idx*6+0) = pow(sx1[idx],2);
                Va0(idx*6+1 ,idx*6+1) = pow(sy1[idx],2);
                Va0(idx*6+2 ,idx*6+2) = pow(sE1[idx],2);
                Va0(idx*6+3 ,idx*6+3) = pow(sx2[idx],2);
                Va0(idx*6+4 ,idx*6+4) = pow(sy2[idx],2);
                Va0(idx*6+5 ,idx*6+5) = pow(sE2[idx],2);
        }
        a = a0;
        v = v0;

        TMatrixD D(npi0s+3,npi0s*6); // = del H/del alpha(alpha_A)
        TMatrixD E(npi0s+3,3);      // = del H/del alpha(v) //remove ,0
        TVectorD d(npi0s+3); // = H(alpha_A)

        TVectorD da0(npi0s*6);
        TVectorD dv0(3);

        TMatrixD VD(npi0s+3,npi0s+3);
        TMatrixD VE(3,3);
        TVectorD Lambda0(npi0s+3);
        TVectorD Lambda(npi0s+3);
        for (int k = 0; k < npi0s + 3; k++){ //initialize Lambda vectors to be 1 
           Lambda0(k) = 1;
           Lambda(k) = 1;
        }
 
        TVectorD v_keep(3);
        v_keep = v;
 
        TMatrixD VE_keep(3,3);
        VE_keep = VE;

        TMatrixD VD_keep(npi0s+3,npi0s+3);
        VD_keep = VD;

        TVectorD Lambda0_keep(npi0s+3);
        Lambda0_keep = Lambda0;

        TMatrixD E_keep(npi0s+3, 3);
        E_keep = E;

        TMatrixD D_keep(npi0s+3,npi0s*6);
        D_keep = D;

        TVectorD a_keep(npi0s*6);
        a_keep = a;

        TVectorD v_keep_v(3);
        v_keep_v = v;

        TMatrixD VE_keep_v(3,3);
        VE_keep_v = VE;

        TMatrixD VD_keep_v(npi0s+3,npi0s+3);
        VD_keep_v = VD;

        TVectorD Lambda0_keep_v(npi0s+3);
        Lambda0_keep_v = Lambda0;

        TMatrixD E_keep_v(npi0s+3, 3);
        E_keep_v = E;

        TMatrixD D_keep_v(npi0s+3,npi0s*6);
        D_keep_v = D;        

//=============================================================
//        // start minizing loop

        int it,it_v;
        for(it=0;it<MAX_ITERACTIONS;it++) {             // Global minizing loop
        chisq_keep_v = HUGE;                            // reset chisq_keep_v
        for(it_v=0;it_v<MAX_ITERACTIONS;it_v++) {       // KL vertex (v) minizing loop
        v0 = v;         // set initial vertex to be expanding point
        error_flag = 0; // reset error flag

        double dpx1_dx1[3], dpx2_dx2[3];
        double dpx1_dy1[3], dpx2_dy2[3];
        double dpx1_dE1[3], dpx2_dE2[3];

        double dpy1_dx1[3], dpy2_dx2[3];
        double dpy1_dy1[3], dpy2_dy2[3];
        double dpy1_dE1[3], dpy2_dE2[3];

        double dpz1_dx1[3], dpz2_dx2[3];
        double dpz1_dy1[3], dpz2_dy2[3];
        double dpz1_dE1[3], dpz2_dE2[3];

        double dR1_dx1[3],  dR2_dx2[3];
        double dR1_dy1[3],  dR2_dy2[3];
        double dR1_dE1[3],  dR2_dE2[3];

        double dpx1_dvx[3], dpx2_dvx[3];
        double dpx1_dvy[3], dpx2_dvy[3];
        double dpx1_dvz[3], dpx2_dvz[3];

        double dpy1_dvx[3], dpy2_dvx[3];
        double dpy1_dvy[3], dpy2_dvy[3];
        double dpy1_dvz[3], dpy2_dvz[3];

        double dpz1_dvx[3], dpz2_dvx[3];
        double dpz1_dvy[3], dpz2_dvy[3];
        double dpz1_dvz[3], dpz2_dvz[3];

        double dR1_dvx[3],  dR2_dvx[3];
        double dR1_dvy[3],  dR2_dvy[3];
        double dR1_dvz[3],  dR2_dvz[3];

        double dklpx_dvx, dklpy_dvx, dklpz_dvx;
        double dklpx_dvy, dklpy_dvy, dklpz_dvy;
        double dklpx_dvz, dklpy_dvz, dklpz_dvz;

        klpx = klpy = klpz = klE = 0.;
        dklpx_dvx = dklpy_dvx = dklpz_dvx = 0.;
        dklpx_dvy = dklpy_dvy = dklpz_dvy = 0.;
        dklpx_dvz = dklpy_dvz = dklpz_dvz = 0.;
        for( idx=0; idx<npi0s; idx++) {

                R1[idx] = sqrt(pow(x1[idx]-vx,2) + pow(y1[idx]-vy,2) + pow(z1[idx]-vz,2));
                R2[idx] = sqrt(pow(x2[idx]-vx,2) + pow(y2[idx]-vy,2) + pow(z2[idx]-vz,2));

                px1[idx] = (x1[idx]-vx)/R1[idx] * E1[idx];
                py1[idx] = (y1[idx]-vy)/R1[idx] * E1[idx];
                pz1[idx] = (z1[idx]-vz)/R1[idx] * E1[idx];

                px2[idx] = (x2[idx]-vx)/R2[idx] * E2[idx];
                py2[idx] = (y2[idx]-vy)/R2[idx] * E2[idx];
                pz2[idx] = (z2[idx]-vz)/R2[idx] * E2[idx];

                klpx += px1[idx] + px2[idx];
                klpy += py1[idx] + py2[idx];
                klpz += pz1[idx] + pz2[idx];
                klE  += E1[idx]  + E2[idx];

                dR1_dx1[idx] = (x1[idx] - vx) / R1[idx];
                dR1_dy1[idx] = (y1[idx] - vy) / R1[idx];
                dR1_dE1[idx] = 0;

                dpx1_dx1[idx] = (E1[idx] - px1[idx] * dR1_dx1[idx])/R1[idx];
                dpx1_dy1[idx] = (        - px1[idx] * dR1_dy1[idx])/R1[idx];
                dpx1_dE1[idx] = (x1[idx] - vx) / R1[idx];

                dpy1_dx1[idx] = (        - py1[idx] * dR1_dx1[idx])/R1[idx];
                dpy1_dy1[idx] = (E1[idx] - py1[idx] * dR1_dy1[idx])/R1[idx];
                dpy1_dE1[idx] = (y1[idx] - vy) / R1[idx];

                dpz1_dx1[idx] = (        - pz1[idx] * dR1_dx1[idx])/R1[idx];
                dpz1_dy1[idx] = (        - pz1[idx] * dR1_dy1[idx])/R1[idx];
                dpz1_dE1[idx] = (z1[idx] - vz) / R1[idx];

                dR2_dx2[idx] = (x2[idx] - vx) / R2[idx];
                dR2_dy2[idx] = (y2[idx] - vy) / R2[idx];
                dR2_dE2[idx] = 0;
               
                dpx2_dx2[idx] = (E2[idx] - px2[idx] * dR2_dx2[idx])/R2[idx];
                dpx2_dy2[idx] = (        - px2[idx] * dR2_dy2[idx])/R2[idx];
                dpx2_dE2[idx] = (x2[idx] - vx) / R2[idx];

                dpy2_dx2[idx] = (        - py2[idx] * dR2_dx2[idx])/R2[idx];
                dpy2_dy2[idx] = (E2[idx] - py2[idx] * dR2_dy2[idx])/R2[idx];
                dpy2_dE2[idx] = (y2[idx] - vy) / R2[idx];

                dpz2_dx2[idx] = (        - pz2[idx] * dR2_dx2[idx])/R2[idx];
                dpz2_dy2[idx] = (        - pz2[idx] * dR2_dy2[idx])/R2[idx];
                dpz2_dE2[idx] = (z2[idx] - vz) / R2[idx];

                dR1_dvx[idx] = -(x1[idx] - vx) / R1[idx];
                dR1_dvy[idx] = -(y1[idx] - vy) / R1[idx];
                dR1_dvz[idx] = -(z1[idx] - vz) / R1[idx];

                dpx1_dvx[idx] = (-E1[idx] - px1[idx] * dR1_dvx[idx])/R1[idx];
                dpx1_dvy[idx] = (         - px1[idx] * dR1_dvy[idx])/R1[idx];
                dpx1_dvz[idx] = (         - px1[idx] * dR1_dvz[idx])/R1[idx];

                dpy1_dvx[idx] = (         - py1[idx] * dR1_dvx[idx])/R1[idx];
                dpy1_dvy[idx] = (-E1[idx] - py1[idx] * dR1_dvy[idx])/R1[idx];
                dpy1_dvz[idx] = (         - py1[idx] * dR1_dvz[idx])/R1[idx];

                dpz1_dvx[idx] = (         - pz1[idx] * dR1_dvx[idx])/R1[idx];
                dpz1_dvy[idx] = (         - pz1[idx] * dR1_dvy[idx])/R1[idx];
                dpz1_dvz[idx] = (-E1[idx] - pz1[idx] * dR1_dvz[idx])/R1[idx];

                dR2_dvx[idx] = -(x2[idx] - vx) / R2[idx];
                dR2_dvy[idx] = -(y2[idx] - vy) / R2[idx];
                dR2_dvz[idx] = -(z2[idx] - vz) / R2[idx];

                dpx2_dvx[idx] = (-E2[idx] - px2[idx] * dR2_dvx[idx])/R2[idx];
                dpx2_dvy[idx] = (         - px2[idx] * dR2_dvy[idx])/R2[idx];
                dpx2_dvz[idx] = (         - px2[idx] * dR2_dvz[idx])/R2[idx];

                dpy2_dvx[idx] = (         - py2[idx] * dR2_dvx[idx])/R2[idx];
                dpy2_dvy[idx] = (-E2[idx] - py2[idx] * dR2_dvy[idx])/R2[idx];
                dpy2_dvz[idx] = (         - py2[idx] * dR2_dvz[idx])/R2[idx];

                dpz2_dvx[idx] = (         - pz2[idx] * dR2_dvx[idx])/R2[idx];
                dpz2_dvy[idx] = (         - pz2[idx] * dR2_dvy[idx])/R2[idx];
                dpz2_dvz[idx] = (-E2[idx] - pz2[idx] * dR2_dvz[idx])/R2[idx];

                dklpx_dvx += (dpx1_dvx[idx] + dpx2_dvx[idx]);
                dklpx_dvy += (dpx1_dvy[idx] + dpx2_dvy[idx]);
                dklpx_dvz += (dpx1_dvz[idx] + dpx2_dvz[idx]);

                dklpy_dvx += (dpy1_dvx[idx] + dpy2_dvx[idx]);
                dklpy_dvy += (dpy1_dvy[idx] + dpy2_dvy[idx]);
                dklpy_dvz += (dpy1_dvz[idx] + dpy2_dvz[idx]);

                dklpz_dvx += (dpz1_dvx[idx] + dpz2_dvx[idx]);
                dklpz_dvy += (dpz1_dvy[idx] + dpz2_dvy[idx]);
                dklpz_dvz += (dpz1_dvz[idx] + dpz2_dvz[idx]);
        }
        for( idx=0; idx<npi0s; idx++) { //change to 3pi0 constraint
                d(idx) =   pow(E1[idx]  + E2[idx] ,2)
                           - pow(px1[idx] + px2[idx],2)
                           - pow(py1[idx] + py2[idx],2)
                           - pow(pz1[idx] + pz2[idx],2)
                           - MASS_PI0*MASS_PI0;
        }
        d(npi0s+0) = klE*klE - klpx*klpx - klpy*klpy - klpz*klpz - MASS_KL*MASS_KL;

	d(npi0s+1) = - (CSIZPosition - T1TargetZ)*vx*klE; // length to Csi
        d(npi0s+2) = - (CSIZPosition - T1TargetZ)*vy*klE; //T1TargetZ is negative

        double ce_x = 0 ;
        double ce_y = 0 ;


        for( idx=0; idx<npi0s; idx++) {
                d(npi0s+1) += (vz-T1TargetZ)*(E1[idx]*x1[idx] + E2[idx]*x2[idx]);
                d(npi0s+2) += (vz-T1TargetZ)*(E1[idx]*y1[idx] + E2[idx]*y2[idx]);
                ce_x += E1[idx]*x1[idx] + E2[idx]*x2[idx];
                ce_y += E1[idx]*y1[idx] + E2[idx]*y2[idx];
        }
        //KL.setCx(ce_x/klE);
        //KL.setCy(ce_y/klE);
        //
        // Reset core matrix D & E


        for(int i=0;i<npi0s+3;i++) {
                for(int j=0;j<npi0s*6;j++)     D(i,j) = 0.;
                for(int j=0;j<3;j++)           E(i,j) = 0.;
        }
        // Calculate core matrix D & E
        for( idx=0; idx<npi0s; idx++) {

        //d(idx+1) =   pow(E1[idx]  + E2[idx] ,2)
        //           - pow(px1[idx] + px2[idx],2)
        //           - pow(py1[idx] + py2[idx],2)
        //           - pow(pz1[idx] + pz2[idx],2)
        //           - MASS_PI0*MASS_PI0;



        D(idx+0,idx*6+0) = -2.*(px1[idx] + px2[idx])*dpx1_dx1[idx]
                           -2.*(py1[idx] + py2[idx])*dpy1_dx1[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz1_dx1[idx]; // del x1
        D(idx+0,idx*6+1) = -2.*(px1[idx] + px2[idx])*dpx1_dy1[idx]
                           -2.*(py1[idx] + py2[idx])*dpy1_dy1[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz1_dy1[idx]; // del y1
        D(idx+0,idx*6+2) =  2.*(E1[idx]  + E2[idx])
                           -2.*(px1[idx] + px2[idx])*dpx1_dE1[idx]
                           -2.*(py1[idx] + py2[idx])*dpy1_dE1[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz1_dE1[idx]; // del E1

        D(idx+0,idx*6+3) = -2.*(px1[idx] + px2[idx])*dpx2_dx2[idx]
                           -2.*(py1[idx] + py2[idx])*dpy2_dx2[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz2_dx2[idx]; // del x2
        D(idx+0,idx*6+4) = -2.*(px1[idx] + px2[idx])*dpx2_dy2[idx]
                           -2.*(py1[idx] + py2[idx])*dpy2_dy2[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz2_dy2[idx]; // del y2
        D(idx+0,idx*6+5) =  2.*(E1[idx]  + E2[idx])
                           -2.*(px1[idx] + px2[idx])*dpx2_dE2[idx]
                           -2.*(py1[idx] + py2[idx])*dpy2_dE2[idx]
                           -2.*(pz1[idx] + pz2[idx])*dpz2_dE2[idx]; // del E2   

        E(idx+0,0)  = -2.*(px1[idx] + px2[idx])*(dpx1_dvx[idx] + dpx2_dvx[idx])
                      -2.*(py1[idx] + py2[idx])*(dpy1_dvx[idx] + dpy2_dvx[idx])
                      -2.*(pz1[idx] + pz2[idx])*(dpz1_dvx[idx] + dpz2_dvx[idx]); // del vx
        E(idx+0,1)  = -2.*(px1[idx] + px2[idx])*(dpx1_dvy[idx] + dpx2_dvy[idx])
                      -2.*(py1[idx] + py2[idx])*(dpy1_dvy[idx] + dpy2_dvy[idx])
                      -2.*(pz1[idx] + pz2[idx])*(dpz1_dvy[idx] + dpz2_dvy[idx]); // del vy
        E(idx+0,2)  = -2.*(px1[idx] + px2[idx])*(dpx1_dvz[idx] + dpx2_dvz[idx])
                      -2.*(py1[idx] + py2[idx])*(dpy1_dvz[idx] + dpy2_dvz[idx])
                      -2.*(pz1[idx] + pz2[idx])*(dpz1_dvz[idx] + dpz2_dvz[idx]); // del vz
        }

        for( idx=0; idx<npi0s; idx++) {
        // d(npi0s+1) = klE*klE - klpx*klpx - klpy*klpy - klpz*klpz - MASS_KL*MASS_KL;
        D(npi0s+0  ,idx*6+0) = -2.* klpx * dpx1_dx1[idx]
                             -2.* klpy * dpy1_dx1[idx]
                             -2.* klpz * dpz1_dx1[idx]; // del x1
        D(npi0s+0  ,idx*6+1) = -2.* klpx * dpx1_dy1[idx]
                             -2.* klpy * dpy1_dy1[idx]
                             -2.* klpz * dpz1_dy1[idx]; // del y1
        D(npi0s+0  ,idx*6+2) =  2.* klE
                             -2.* klpx * dpx1_dE1[idx]
                             -2.* klpy * dpy1_dE1[idx]
                             -2.* klpz * dpz1_dE1[idx]; // del E1

        D(npi0s+0  ,idx*6+3) = -2.* klpx * dpx2_dx2[idx]
                             -2.* klpy * dpy2_dx2[idx]
                             -2.* klpz * dpz2_dx2[idx]; // del x2
        D(npi0s+0  ,idx*6+4) = -2.* klpx * dpx2_dy2[idx]
                             -2.* klpy * dpy2_dy2[idx]
                             -2.* klpz * dpz2_dy2[idx]; // del y2
        D(npi0s+0  ,idx*6+5) =  2.* klE
                             -2.* klpx * dpx2_dE2[idx]
                             -2.* klpy * dpy2_dE2[idx]
                             -2.* klpz * dpz2_dE2[idx]; // del E2

        E(npi0s+0  ,0)  = -2.* klpx * dklpx_dvx
                        -2.* klpy * dklpy_dvx
                        -2.* klpz * dklpz_dvx; // del vx
        E(npi0s+0 ,1)  = -2.* klpx * dklpx_dvy
                        -2.* klpy * dklpy_dvy
                        -2.* klpz * dklpz_dvy; // del vy
        E(npi0s+0  ,2)  = -2.* klpx * dklpx_dvz
                        -2.* klpy * dklpy_dvz
                        -2.* klpz * dklpz_dvz; // del vz






        //      d(npi0s+2) += sum(E1[idx]*x1[idx] + E2[idx]*x2[idx]) - vx*klE;
        //      d(npi0s+3) += sum(E1[idx]*y1[idx] + E2[idx]*y2[idx]) - vy*klE;





        D(npi0s+1,idx*6+0) = (vz-T1TargetZ)*E1[idx];            // del x1
        D(npi0s+1,idx*6+1) = 0.;                // del y1
        D(npi0s+1,idx*6+2) = (vz-T1TargetZ)*x1[idx] - (CSIZPosition - T1TargetZ)*vx;    // del E1

        D(npi0s+1,idx*6+3) = (vz-T1TargetZ)*E2[idx];            // del x2
        D(npi0s+1,idx*6+4) = 0.;                // del y2
        D(npi0s+1,idx*6+5) = (vz-T1TargetZ)*x2[idx] - (CSIZPosition - T1TargetZ)*vx;    // del E2

        E(npi0s+1,0)  = -(CSIZPosition - T1TargetZ)*klE;                        // del vx
        E(npi0s+1,1)  = 0.;                     // del vy
        E(npi0s+1,2)  = ce_x;                   // del vz

        D(npi0s+2,idx*6+0) = 0.;                // del x1
        D(npi0s+2,idx*6+1) = (vz-T1TargetZ)*E1[idx];            // del y1
        D(npi0s+2,idx*6+2) = (vz-T1TargetZ)*y1[idx] - (CSIZPosition - T1TargetZ)*vy;    // del E1

        D(npi0s+2,idx*6+3) = 0.;                // del x2
        D(npi0s+2,idx*6+4) = (vz-T1TargetZ)*E2[idx];            // del y2
        D(npi0s+2,idx*6+5) = (vz-T1TargetZ)*y2[idx] - (CSIZPosition - T1TargetZ)*vy;    // del E2

        E(npi0s+2,0)  = 0.;                     // del vx
        E(npi0s+2,1)  = -(CSIZPosition - T1TargetZ)*klE;                        // del vy
        E(npi0s+2,2)  = ce_y;                   // del vz

        }
        da0 = a0 - a;
        dv0 = v0 - v;

        TMatrixD Dt(npi0s*6,npi0s+3);
        D.T(); //transpose D
        Dt = D; //set holding matrix
        D.T(); //transpose back
    






        TMatrixD Et(3, npi0s+3);
        E.T();
        Et = E;
        E.T();
        
        VD = (D*Va0*Dt);
        //std::cout << " HERE IS VD PRE INVERT " << std::endl << std::endl;
        //for(int j = 0; j < npi0s + 2; j++){
        //   for(int k = 0; k < npi0s + 2; k++){
        //      std::cout << VD(j,k) << "  "; 

        //   }
        //   std::cout << std::endl;
        //} 
        VD.Invert();
        //std::cout << " HERE IS VD POST INVERT " << std::endl << std::endl;
        //for(int j = 0; j < npi0s + 2; j++){
        //   for(int k = 0; k < npi0s + 2; k++){
        //      std::cout << VD(j,k) << "  "; 
        //   }
        //   std::cout << std::endl;
        //}
        VE = (Et*VD*E);
        //std::cout << " HERE IS VE PRE INVERT " << std::endl << std::endl;
        //for(int j = 0; j < 3; j++){
        //   for(int k = 0; k < 3; k++){
        //      std::cout << VE(j,k) << "  "; 

        //   }
        //   std::cout << std::endl;
        //} 
        VE.Invert();
        //std::cout << " HERE IS VE POST INVERT " << std::endl << std::endl;
        //for(int j = 0; j < 3; j++){
        //   for(int k = 0; k < 3; k++){
        //      std::cout << VE(j,k) << "  "; 
        //   }
        //   std::cout << std::endl;
        //}
        Lambda0 = (VD*(D*da0 + d));
        //std::cout << " HERE IS LAMBDA VECTOR " << std::endl << std::endl;
        //for(int j = 0; j < npi0s + 2; j++){
        //   std::cout << Lambda0(j) << std::endl;
        //}       
        //std::cout << std::endl;
        TVectorD unit(2);
        unit(0) = 1;
        unit(1) = 1;
        TVectorD chisq_vec(1);

        //std::cout << std::endl << std::endl << "HERE IS THE D VECTOR" << std::endl;
        //for(int j = 0; j < npi0s+2; j++){
        //   std::cout << d(j) << std::endl;
        //}
 
        chisq_vec(0) = (Lambda0 * (D*da0 + E*dv0 + d));
        chisq = chisq_vec(0);
        TVectorD subv(3);
        subv = VE * Et * Lambda0;
        v = v-subv;
        if (chisq < chisq_keep_v && error_flag==0 && chisq>=0.) {
                chisq_keep_v    = chisq;
                v_keep_v        = v;
                VE_keep_v       = VE;
                VD_keep_v       = VD;
                Lambda0_keep_v  = Lambda0;
                E_keep_v        = E;
                D_keep_v        = D;
                fit_updated     = 1;
        }else {
                        chisq   = chisq_keep_v;
                        v       = v_keep_v;
                        VE      = VE_keep_v;
                        VD      = VD_keep_v;
                        Lambda0 = Lambda0_keep_v;
                        E       = E_keep_v;
                        D       = D_keep_v;
                        break;
        }


        // fill back KL vertex first


        vx = v(0);
        vy = v(1);
        vz = v(2);

        } // end of KL vertex minizing loop
        TMatrixD Dt(npi0s*6,npi0s+3);
        D.T();
        Dt = D;
        D.T();

        TMatrixD Et(3, npi0s+3);
        E.T();
        Et = E;
        E.T();

        TVectorD suba(npi0s*6);
        suba = Va0 * Dt * Lambda0;
        if (error_flag==0) {
                Lambda  = Lambda0 - VD * E * VE * Et * Lambda0;
                a = a0 - suba;
        }

        if ((it == 0 || chisq < chisq_keep) && error_flag==0 && chisq>=0.) {
                chisq_keep      = chisq;
                v_keep          = v;
                VE_keep         = VE;
                VD_keep         = VD;
                Lambda0_keep    = Lambda0;
                E_keep          = E;
                D_keep          = D;
                a_keep          = a;
                fit_updated     = 1;
        }else {
                chisq   = chisq_keep;
                v       = v_keep;
                VE      = VE_keep;
                VD      = VD_keep;
                Lambda0 = Lambda0_keep;
                E       = E_keep;
                D       = D_keep;
                a       = a_keep;
                break;
        }

        // fill back other parameters
        for( idx=0; idx<npi0s; idx++) {
                x1[idx] = a(idx*6 + 0);
                y1[idx] = a(idx*6 + 1);
                E1[idx] = a(idx*6 + 2);
                x2[idx] = a(idx*6 + 3);
                y2[idx] = a(idx*6 + 4);
                E2[idx] = a(idx*6 + 5);
        }
        } // end of global minizing loop

        if (fit_updated == 0) { // fit failed.
                fprintf(stderr,"[ERROR] : Fit failed (%d), return error code = %d.\n",ifail,error_flag);
                chisq = -1.;
                if (error_flag!=0) return (double)error_flag;
                else return -3.;
        }

        for( idx=0; idx<npi0s; idx++) {

                R1[idx] = sqrt(pow(x1[idx]-vx,2) + pow(y1[idx]-vy,2) + pow(z1[idx]-vz,2));
                R2[idx] = sqrt(pow(x2[idx]-vx,2) + pow(y2[idx]-vy,2) + pow(z2[idx]-vz,2));

                px1[idx] = (x1[idx]-vx)/R1[idx] * E1[idx];
                py1[idx] = (y1[idx]-vy)/R1[idx] * E1[idx];
                pz1[idx] = (z1[idx]-vz)/R1[idx] * E1[idx];

                px2[idx] = (x2[idx]-vx)/R2[idx] * E2[idx];
                py2[idx] = (y2[idx]-vy)/R2[idx] * E2[idx];
                pz2[idx] = (z2[idx]-vz)/R2[idx] * E2[idx];

                klpx += px1[idx] + px2[idx];
                klpy += py1[idx] + py2[idx];
                klpz += pz1[idx] + pz2[idx];
                klE  += E1[idx]  + E2[idx];
        }


        TMatrixD Dt(npi0s*6,npi0s+3);
        D.T();
        Dt = D;
        D.T();

        TMatrixD Et(3, npi0s+3);
        E.T();
        Et = E;
        E.T();
        // Final error matrix

        TMatrixD VDtu = VD - VD * E * VE * Et * VD;
        TMatrixD Va = Va0 - Va0 * Dt * VDtu * D * Va0;

        KL.setMass(sqrt(klE*klE-klpx*klpx-klpy*klpy-klpz*klpz));
        KL.setEnergy(klE);
        KL.setP3(klpx,klpy,klpz);
        KL.setVtx(vx,vy,vz);
        KL.setChisqZ(chisq);

      // npi0s =3 here, I had checked
        for( idx=0; idx<npi0s; idx++) {
                KL.pi0()[idx].setEnergy(E1[idx]+E2[idx]);
                KL.pi0()[idx].setMass(sqrt(pow(E1[idx]+E2[idx],2) -
                                           pow(px1[idx]+px2[idx],2) -
                                           pow(py1[idx]+py2[idx],2) -
                                           pow(pz1[idx]+pz2[idx],2)));

                KL.pi0()[idx].setVtx(vx,vy,vz);
                KL.pi0()[idx].setP3(px1[idx]+px2[idx],py1[idx]+py2[idx],pz1[idx]+pz2[idx]);
                KL.pi0()[idx].setRecZ(vz);
                KL.pi0()[idx].setRecZsig2(VE(2,2));

                KL.pi0()[idx].g1().setEnergy(E1[idx]);
                KL.pi0()[idx].g1().setPos(x1[idx],y1[idx],z1[idx]);
                KL.pi0()[idx].g1().setP3(px1[idx],py1[idx],pz1[idx]);

                KL.pi0()[idx].g2().setEnergy(E2[idx]);
                KL.pi0()[idx].g2().setPos(x2[idx],y2[idx],z2[idx]);
                KL.pi0()[idx].g2().setP3(px2[idx],py2[idx],pz2[idx]);
//std::cout << KL.pi0()[idx].g1().p3().mag() <<std::endl;
//std::cout << KL.pi0()[idx].g2().p3().mag() <<std::endl;
//
//
//              KL.pi0()[idx].setAngle(KL.pi0()[idx].g1().p3(),
//              KL.pi0()[idx].g2().p3());



        }

        return chisq;
}











