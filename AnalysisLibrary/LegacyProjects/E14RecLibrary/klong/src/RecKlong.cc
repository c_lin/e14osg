// -*- C++ -*-
//
// RecKlong.cc
// Klong reconstruction 
//
// Author:  Ken Sakashita
// Created: Thu Nov  4 22:28:20 JST 2004
//
// $Id: RecKlong.cc,v 1.4 2006/04/24 12:43:05 toshi Exp $
//
// $Log: RecKlong.cc,v $
// Revision 1.4  2006/04/24 12:43:05  toshi
// g5anafuse function added by Gabe
//
// Revision 1.3  2005/10/03 05:30:51  toshi
// add function: recK2pi0g
//
// Revision 1.2  2005/09/11 22:50:07  kensh
// Add new vertex calculation method.
// In Klong container class, you can change the method by m_vertexFlag.
//
//    flag                 description
//  ---------------------------------------------------------------
//    VERTEX_COE_SCALE     x,y = scaled center-of-energy
//    VERTEX_FIX_XYZERO    x,y = fix to zero
//
// Also, you can change the method from RecKlong::recK3pi0, RecKlong::recK2pi0.
//
//  Old :
//   std::vector<Klong> kl = recklong.recK2pi0( g5list );
//
//  New :
//   std::vector<Klong> kl = recklong.recK2pi0( g5list,VERTEX_FIX_XYZERO );
//
//
// A default flag is VERTEX_COE_SCALE.
// Thus, if you call RecKlong::recK3pi0 and recK2pi0 without second argument,
// like
//
//   std::vector<Klong> kl = recklong.recK2pi0( g5list );
//
// , the vertex is calculated with the scaled-center-of-energy method.
//
// Revision 1.1  2004/11/04 13:26:35  kensh
// Klong class. Faster than recKL.
//
//
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "gamma/Gamma.h"
#include "pi0/Pi0.h"
#include "rec2g/Rec2g.h"
#include "klong/RecKlong.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "TArrayD.h"
//#include "TMatrixSymD.h"
#include "gnana/E14GNAnaFunction.h"
#include <iomanip>
#include <list>
                                                                                                         
#define MAX_ITERACTIONS 30
#define MASS_KL 0.497648
#define DEBUG_G5FUSE_LEVEL 0


/////
RecKlong::RecKlong( int debugLevel )
  : m_debugLevel( debugLevel )
{
  //

}

/////
RecKlong::~RecKlong()
{
  //
}

std::vector<Klong>
RecKlong::recKppipi0( const std::list<Gamma>& glist){

  const double mp0 = MTBP::Pi0_MASS;
  const double mp  = MTBP::CPi_MASS;
  const double ZCSI = MTBP::CSIZPosition;
  
  Rec2g rec2g;

  std::list<Pi0> pi0list = rec2g.recPi0withConstM( glist );

  std::vector<Klong> Kppipi0;


  for( std::list<Pi0>::const_iterator p1=pi0list.begin(); p1!=pi0list.end(); p1++ ) {
    for( std::list<Gamma>::const_iterator g3=glist.begin(); g3!=glist.end(); g3++ ) {
        
      if ( p1->g1().id() == g3->id() || p1->g2().id() == g3->id() )continue; //same gamma.

        Pi0 pi0 = *p1;
		Gamma gam = *g3;
        double recz_KL = pi0.recZ();
        
        CLHEP::HepLorentzVector Ppi0(pi0.p3(), pi0.e());
        
        CLHEP::Hep3Vector Posg[3];
        Posg[0].set(p1->g1().x(), p1->g1().y(), ZCSI);
        Posg[1].set(p1->g2().x(), p1->g2().y(), ZCSI);
        Posg[2].set(g3->x(), g3->y(), ZCSI);
        
        double Eg[3] = {p1->g1().e(), p1->g2().e(), g3->e()};
        
        double rT = g3->pos().perp();
        double ppipT = -p1->p3().perp();
        double ppip  = ppipT/sin(atan(rT/(ZCSI-p1->recZ())));
        double ppipz = sqrt( ppip*ppip - ppipT*ppipT );
        double Ep    = sqrt( ppip*ppip + mp*mp);
        double pkz   = p1->p3().z() + ppipz;
        double Ek    = p1->e() + Ep;
        double invM  = sqrt(Ek*Ek - pkz*pkz);

		CLHEP::Hep3Vector momg = Posg[2]-CLHEP::Hep3Vector(0,0,recz_KL);
		momg.setMag(g3->e());
		gam.setP3(momg);

		Klong Kp;
        Kp.setMass( invM );
        Kp.setEnergy( Ek );
        Kp.setP3(CLHEP::Hep3Vector(0,0,pkz));
        Kp.setVtx(CLHEP::Hep3Vector(0,0,recz_KL));
        Kp.addPi0(pi0);
		Kp.addGamma(gam);
		Kp.setSortForKpipi0();
		Kppipi0.push_back(Kp);

	}

  }

  return( Kppipi0 );
}






/////
std::vector<Klong> 
RecKlong::recK2pi0( const std::list<Gamma>& glist, int userFlag, int vtxflag, double pi0sig2cut )
{
  //
  Rec2g rec2g; // pi0 reconstruction 
  std::list<Pi0> pi0list = rec2g.recPi0withConstM( glist );

  return( recK2pi0( pi0list,userFlag,vtxflag,pi0sig2cut ) );
}

/////
std::vector<Klong> 
RecKlong::recK2pi0( const std::list<Pi0>& pi0list, int userFlag, int vtxflag, double pi0sig2cut )
{
  //
  std::vector<Klong> k2pi0;

  for( std::list<Pi0>::const_iterator p1=pi0list.begin();
       p1!=pi0list.end(); p1++ ) {
    for( std::list<Pi0>::const_iterator p2=p1;
	 p2!=pi0list.end(); p2++ ) {
      if ( p2->g1().id() == p1->g1().id() || p2->g1().id() == p1->g2().id() ||
	   p2->g2().id() == p1->g1().id() || p2->g2().id() == p1->g2().id()  ){
	continue; // same gamma.
      }
      else {
	if( p1->status() == 1 &&
	    p2->status() == 1 &&
	    p1->recZsig2() <= pi0sig2cut &&
	    p2->recZsig2() <= pi0sig2cut ) {
	  
	  Klong klong;
	  klong.setUserFlag( userFlag );
	  klong.setVertexFlag( vtxflag );
	  klong.setPi0( (*p1),(*p2) );
	  klong.setSortByChisqZ();
	  
	  k2pi0.push_back( klong );
	}
      }
    }
  } 

  // sort by ChisqZ
  sort( k2pi0.begin(), k2pi0.end() );

  // set id
  int id = 0;
  for(std::vector<Klong>::iterator i=k2pi0.begin();
      i!=k2pi0.end(); i++) {
    i->setId( id );

    id++;
  }

  return( k2pi0 );
}


/////
std::vector<Klong> 
RecKlong::recK3pi0( const std::list<Gamma>& glist, int userFlag, int vtxflag, double pi0sig2cut )
{
  
  Rec2g rec2g; // pi0 reconstruction 
  std::list<Pi0> pi0list = rec2g.recPi0withConstM( glist );

  return( recK3pi0( pi0list,userFlag,vtxflag,pi0sig2cut ) );
}

/////
std::vector<Klong> 
RecKlong::recK3pi0( const std::list<Pi0>& pi0list, int userFlag, int vtxflag, double pi0sig2cut )
{
  
  std::vector<Klong> k3pi0;

  for( std::list<Pi0>::const_iterator p1=pi0list.begin();
       p1!=pi0list.end(); p1++ ) {
    for( std::list<Pi0>::const_iterator p2=p1;
	 p2!=pi0list.end(); p2++ ) {
      if ( p2->g1().id() == p1->g1().id() || p2->g1().id() == p1->g2().id() ||
	   p2->g2().id() == p1->g1().id() || p2->g2().id() == p1->g2().id()  ){
	continue; // same gamma.
      }
      else{
	for( std::list<Pi0>::const_iterator p3=p2;
	     p3!=pi0list.end(); p3++ ){
	  if ( p3->g1().id() == p2->g1().id() || p3->g1().id() == p2->g2().id() ||
	       p3->g2().id() == p2->g1().id() || p3->g2().id() == p2->g2().id() ||
	       p3->g1().id() == p1->g1().id() || p3->g1().id() == p1->g2().id() ||
	       p3->g2().id() == p1->g1().id() || p3->g2().id() == p1->g2().id()  ){
	    continue; // same gamma.
	  }
	  else {
	    if( p1->status() == 1 &&
		p2->status() == 1 &&
		p3->status() == 1 &&
		p1->recZsig2() <= pi0sig2cut &&
		p2->recZsig2() <= pi0sig2cut &&
		p3->recZsig2() <= pi0sig2cut ) {
	      
	      Klong klong;
	      klong.setUserFlag( userFlag );
	      klong.setVertexFlag( vtxflag );
	      klong.setPi0( (*p1),(*p2),(*p3) );
	      klong.setSortByChisqZ();
	      
	      k3pi0.push_back( klong );
	    }
	  }
	}
      }
    }
  } 

  // sort by ChisqZ
  sort( k3pi0.begin(), k3pi0.end() );

  // set id
  int id = 0;
  for(std::vector<Klong>::iterator i=k3pi0.begin();
      i!=k3pi0.end(); i++) {
    i->setId( id );

    id++;
  }

  return( k3pi0 );
}


std::vector<Klong>
RecKlong::recK3pi0_wcfit( const std::list<Gamma>& glist, int userFlag, int vtxflag, double pi0sig2cut )
{

  Rec2g rec2g; // pi0 reconstruction
  std::list<Pi0> pi0list = rec2g.recPi0withConstM( glist );

  return( recK3pi0_wcfit( pi0list,userFlag,vtxflag,pi0sig2cut ) );
}

std::vector<Klong>
RecKlong::recK3pi0_wcfit( const std::list<Pi0>& pi0list, int userFlag, int vtxflag, double pi0sig2cut )
{

  std::vector<Klong> k3pi0;

  for( std::list<Pi0>::const_iterator p1=pi0list.begin();
       p1!=pi0list.end(); p1++ ) {
    for( std::list<Pi0>::const_iterator p2=p1;
         p2!=pi0list.end(); p2++ ) {
      if ( p2->g1().id() == p1->g1().id() || p2->g1().id() == p1->g2().id() ||
           p2->g2().id() == p1->g1().id() || p2->g2().id() == p1->g2().id()  ){
        continue; // same gamma.
      }
      else{
        for( std::list<Pi0>::const_iterator p3=p2;
             p3!=pi0list.end(); p3++ ){
          if ( p3->g1().id() == p2->g1().id() || p3->g1().id() == p2->g2().id() ||
               p3->g2().id() == p2->g1().id() || p3->g2().id() == p2->g2().id() ||
               p3->g1().id() == p1->g1().id() || p3->g1().id() == p1->g2().id() ||
               p3->g2().id() == p1->g1().id() || p3->g2().id() == p1->g2().id()  ){
            continue; // same gamma.
          }
          else {
            if( p1->status() == 1 &&
                p2->status() == 1 &&
                p3->status() == 1 &&
                p1->recZsig2() <= pi0sig2cut &&
                p2->recZsig2() <= pi0sig2cut &&
                p3->recZsig2() <= pi0sig2cut ) {

              Klong klong;
              klong.setUserFlag( userFlag );
              klong.setVertexFlag( vtxflag );
              klong.setPi0( (*p1),(*p2),(*p3) );
              klong.setSortByChisqZ();

              k3pi0.push_back( klong );
            }
          }
        }
      }
    }
  } /// all the same up to here, add constrained fit if vtxflag is coe
  if(vtxflag == VERTEX_FIX_XYZERO){
     sort( k3pi0.begin(), k3pi0.end() );    
  }
  else if(vtxflag == VERTEX_COE_SCALE){
  
     double chisq_temp;
     double chisq_best = 9999999;
     for(int j = 0; j < k3pi0.size(); j++){
        if(std::isnan(k3pi0[j].vz())){
           chisq_temp = 9999999;
           continue;
        }
        chisq_temp = E14GNAnaFunction::getFunction()->cfit_kl3pi0(k3pi0[j]);
       
        if(chisq_temp < chisq_best){
           chisq_best = chisq_temp;
        }
        k3pi0[j].setChisqZ(chisq_temp); //replace chisqZ as cfit_chisq
     }
     sort( k3pi0.begin(), k3pi0.end() ); 
  }

  int id = 0;
  for(std::vector<Klong>::iterator i=k3pi0.begin();
      i!=k3pi0.end(); i++) {
    i->setId( id );

    id++;
  }

  return( k3pi0 );
}

//Perdue 20050926
/////
std::vector<Klong> 
RecKlong::recK2pi0g( const std::list<Gamma>& glist, int vtxflag, double pi0sig2cut )
{


  //
  RecKlong recklong_inner;

  // k2pi0g - the "final" Kaon
  // kl[j]  - the jth Kaon, where j <-> glist4[j] and identifies the 
  // gamma removed from glist.
  //
  std::vector<Klong> k2pi0g;
  std::vector<Klong> kl[5];

  // 
  // Set up new gamma lists.
  // -------------------------
  // 4-gamma lists indexed by ij where i = # of gammas in list
  // and j = id # of gamma removed from glist.  These lists are 
  // used to make two pi0 reconstructions to find the vertex.
  std::list<Gamma> glist4[5];

  // Create 5 copies of glist4 -> glist4j.
  // Remove different gammas each time, label acoording to
  // missing gamma: 
  //    1, 2, 3, 4 - 40
  //    2, 3, 4, 0 - 41
  //    3, 4, 0, 1 - 42
  //    4, 0, 1, 2 - 43
  //    0, 1, 2, 3 - 44
  // For each copy, do a "2pi0" reconstruction.
  // Also store gamma (x,y,z) and E for future work on the spare gamma.
  double gx[5], gy[5], gz[5], gE[5];

  for ( unsigned int i = 0; i < 5; i++ ) {
    glist4[i] = glist;
    std::list<Gamma>::iterator oddgammaout = glist4[i].begin();
    for ( unsigned int j = 0; j < i; j++ ) {
      oddgammaout++;
    }
    gx[i] = oddgammaout->x();
    gy[i] = oddgammaout->y();
    gz[i] = oddgammaout->z();
    gE[i] = oddgammaout->e();
    glist4[i].erase( oddgammaout );
  }

  // Here, the four gammas from each set are used to try to 
  // reconstruct a pair of pi0's.  Begin with a fixed vertex for 
  // the 2pi0 (x,y) => update to include the 5th gamma after pairing.
  int userFlag = 20151101;//set 2015 flag temporary (by S.Shinohara, 20190617)
  for ( unsigned int i = 0; i < 5; i++) {
    kl[i] = recklong_inner.recK2pi0( glist4[i], 
				     userFlag,
				     VERTEX_FIX_XYZERO, 
				     pi0sig2cut );
    if (kl[i].size() > 0) { 
      for (std::vector<Klong>::iterator k = kl[i].begin();
	   k != kl[i].end();
	   k++) {
	k->setUserFlag( i );
	k2pi0g.push_back( (*k) );
      }
    }
  }

  // sort by ChisqZ
  sort( k2pi0g.begin(), k2pi0g.end() );

  // set id & update klong variables (account for 5th gamma)
  //****
  int id = 0;
  for(std::vector<Klong>::iterator i = k2pi0g.begin();
      i != k2pi0g.end(); 
      i++) {
    // Id
    i->setId( id );
    id++;

    // Construct Kaon for COE Vertex
    // 
    if ( vtxflag == VERTEX_COE_SCALE ) { 

      // reconstruct Vtx - Special Update Vars for 5 gamma events.
      double Etot = 0.0;
      double avrX = 0.0;
      double avrY = 0.0;
      double avrZ = i->vz();
      //double sig2tot = 0.0;
      for( unsigned int gnum = 0; gnum < 5; gnum++ ) {
	// center of energy
	avrX += gx[gnum]*gE[gnum];
	avrY += gy[gnum]*gE[gnum];
	
	// Etotal
	Etot += gE[gnum];
      }
      avrX = avrX/Etot;
      avrY = avrY/Etot;

      double TargetZ      = -1180.;
      double CalorimatorZ =   610.;
      double scaleFactor  = ( avrZ - TargetZ )/( CalorimatorZ - TargetZ );
  
      avrX = avrX*scaleFactor;
      avrY = avrY*scaleFactor;

      // update Pi0 vars      
      for( std::vector<Pi0>::iterator p=i->pi0().begin();
	   p!=i->pi0().end(); p++ ) {
	// 
	p->setVtx( avrX, avrY, avrZ );  // set Pi0 vertex
	p->updateVars();                // and update Pi0 vars
      }  

      // klong vars
      CLHEP::HepLorentzVector p_kl = CLHEP::HepLorentzVector( 0.,0.,0.,0. );
      CLHEP::Hep3Vector gspare_p3  = CLHEP::Hep3Vector( gx[i->userFlag()] - i->vx(), 
					  gy[i->userFlag()] - i->vy(), 
					  gz[i->userFlag()] - i->vz()
					  ); 
      gspare_p3.setMag( gE[i->userFlag()] );
      CLHEP::HepLorentzVector p_gspare( gspare_p3, gE[i->userFlag()] );
      p_kl += p_gspare;
      for( std::vector<Pi0>::iterator p=i->pi0().begin();
	   p!=i->pi0().end(); p++ ) {
	// 4 momentum
	CLHEP::HepLorentzVector p_pi0( p->p3(), p->e() );
	p_kl += p_pi0;
      }      
      i->setVtx( avrX,avrY,avrZ );
      i->setP3( p_kl.vect() );
      i->setEnergy( p_kl.e() );
      i->setMass( p_kl.m() );

    } else { // Construct Kaon for Fixed Vertex

    // klong vars
      CLHEP::Hep3Vector gspare_p3 = CLHEP::Hep3Vector( gx[i->userFlag()] - i->vx(), 
					 gy[i->userFlag()] - i->vy(), 
					 gz[i->userFlag()] - i->vz()
					 ); 
      gspare_p3.setMag( gE[i->userFlag()] );
      CLHEP::HepLorentzVector p_gspare( gspare_p3, gE[i->userFlag()] );
      CLHEP::HepLorentzVector p_kl = CLHEP::HepLorentzVector( i->p3(), i->e() );
      p_kl += p_gspare;
      i->setP3( p_kl.vect() );
      i->setEnergy( p_kl.e() );
      i->setMass( p_kl.m() );

    }
  }    

#if DEBUG_LEVEL > 0 
  {
    unsigned int i = 0;
    for(std::vector<Klong>::iterator k = k2pi0g.begin();
	k != k2pi0g.end(); 
	k++) {
      std::cout<<"id = "<<k2pi0g[i].id()<<std::endl;
      std::cout<<"userFlag = "<<k2pi0g[i].userFlag()<<std::endl;
      for( std::vector<Pi0>::const_iterator p=k2pi0g[i].pi0().begin();
	   p!=k2pi0g[i].pi0().end(); p++ ) {
	std::cout<<"pion id = "<<p->id()<<std::endl;
	std::cout<<"  g1 id = "<<p->g1().id()<<std::endl;
	std::cout<<"  g2 id = "<<p->g2().id()<<std::endl;
      }
      i++;
      std::cout<<std::endl;
    }
  }
#endif

  return( k2pi0g );
}


//Joseph 20230901 modified from k2pi0g
////

std::vector<Klong>
RecKlong::recK2pi0gg( const std::list<Gamma>& glist, int vtxflag, double pi0sig2cut )
{


  //
  RecKlong recklong_inner;

  // k2pi0gg - the "final" Kaon
  // kl[j]  - the jth Kaon, where j <-> glist4[j] and identifies the 2 gammas removed 
  // from glist.
  // j  |  removed_g1 | removed g2
  // 0  |    0        |    1
  // 1  |    0        |    2
  // 2  |    0        |    3
  // 3  |    0        |    4
  // 4  |    0        |    5
  // 5  |    1        |    2
  // 6  |    1        |    3
  // 7  |    1        |    4
  // 8  |    1        |    5
  // 9  |    2        |    3
  // 10 |    2        |    4
  // 11 |    2        |    5
  // 12 |    3        |    4
  // 13 |    3        |    5
  // 14 |    4        |    5
  std::vector<Klong> k2pi0gg;
  std::vector<Klong> kl[15];

  // 
  // Set up new gamma lists.
  // -------------------------
  //  These lists are 
  // used to make two pi0 reconstructions to find the vertex.
  std::list<Gamma> glist4[15];
  std::list<Gamma> glist_copy;
  // Create 15 copies of glist4 -> glist4j.
  // Remove 2 different gammas each time, label acoording to
  // missing gammas: 
  // For each copy, do a "2pi0" reconstruction.
  // Also store gammas (x,y,z) and E for future work on the gammas.
  double gx[6], gy[6], gz[6], gE[6], gt[6], sigx[6], sigy[6], sigE[6], gid[6], gcsq[6];  
  double cx[6], cy[6], cz[6], cE[6], ct[6]; 
  double g1Id[45];
  double g2Id[45];
  unsigned int it = 0; 
  unsigned int j = 0;
  unsigned int l = 0;
  unsigned int k;
  unsigned int stop = 1; 
  //save gamma info
  glist_copy = glist; 
  std::list<Gamma>::iterator gam = glist_copy.begin();
  
  int m = 0; 
  for(std::list<Gamma>::iterator gam = glist_copy.begin(); gam != glist_copy.end(); gam++) {
     gx[m] = gam->x();
     gy[m] = gam->y();
     gz[m] = gam->z();
     gE[m] = gam->e();
     gt[m] = gam->t();
     gid[m] = gam->id();
     sigx[m] = gam->sigmaX();
     sigy[m] = gam->sigmaY();
     sigE[m] = gam->sigmaE();
     gcsq[m] = gam->chisq();
     cx[m] = gam->cluster().x();
     cy[m] = gam->cluster().y();
     cz[m] = gam->cluster().z();
     cE[m] = gam->cluster().e();
     ct[m] = gam->cluster().t();
     //std::cout<<"This is gamma ID order "<< gam->id()<<std::endl; 
     m++;
  }
  
  //generate 2pi0 gamma lists
  while (it <= 4) {
    l = 0; 
    glist4[j] = glist;
    std::list<Gamma>::iterator oddgammaout1 = glist4[j].begin();
    std::list<Gamma>::iterator oddgammaout2 = glist4[j].begin();
    for (k = 0; k < it; k++){ //set first missing gamma
       oddgammaout1++;
       oddgammaout2++; ///2nd gamma index > 1st gamma index
       l++;  // set start for next one    
    }
    while (l < stop){
       l++;
       oddgammaout2++;
    }
    //std::cout << k << " : " << l << " :  " << it << std::endl; //test algorithm 
    stop++;
    if(stop == 6){ /// if reached end of gamma index
       it++; //increment i
       stop = it + 1; 
    }


    glist4[j].erase( oddgammaout1 );
    glist4[j].erase( oddgammaout2 );
    
    j++; ///g4list incrementor

  } // while loop i 
// std::cout << "good" << std::endl; 
  // Here, the four gammas from each set are used to try to 
  // reconstruct a pair of pi0's.  Begin with a fixed vertex for 
  // the 2pi0 (x,y) => update to include the 5th gamma after pairing.
  int userFlag = 20210101;//set 2021 flag temporary (by Joseph, 20210901)
  for ( unsigned int i = 0; i < 15; i++) {
    kl[i] = recklong_inner.recK2pi0( glist4[i], 
				     userFlag,
				     VERTEX_FIX_XYZERO, 
				     pi0sig2cut );
    if (kl[i].size() > 0) { 
      for (std::vector<Klong>::iterator k = kl[i].begin();
	   k != kl[i].end();
	   k++) {
	k->setUserFlag( i );
	k2pi0gg.push_back( (*k) );
      }
    }
  }
  sort( k2pi0gg.begin(), k2pi0gg.end() );
//  std::cout << "CHECKING SORT " << std::endl << "-------------------------------------------------------------" << std::endl << k2pi0gg[0].chisqZ() << "  :  " << chisq_best << std::endl;

  // set id & update klong variables (account for 5th gamma)
  //****
  int id = 0;
  Gamma   spare_gam1;
  Cluster spare_clus1;
  Gamma   spare_gam2;
  Cluster spare_clus2;
  Pi0     spare_pi0; 
  //spare_gam1.setId(4);
  //spare_gam2.setId(5);
  for(std::vector<Klong>::iterator i = k2pi0gg.begin();
      i != k2pi0gg.end(); 
      i++) {
    // Id
    //
    if(std::isnan(i->vz())){continue;}
    i->setId( id );
    

    // Construct Kaon for COE Vertex
    //
    bool ag[6] = {false, false, false, false, false, false}; 
    int temp_id1;
    int temp_id2;
    for( std::vector<Pi0>::const_iterator p=k2pi0gg[id].pi0().begin();
           p!=k2pi0gg[id].pi0().end(); p++ ) {
     //   std::cout<<"  pg1 id = "<<p->g1().id()<<std::endl;
      //  std::cout<<"  pg2 id = "<<p->g2().id()<<std::endl;
        temp_id1 = p->g1().id();
        temp_id2 = p->g2().id();
        ag[temp_id1] = true;
        ag[temp_id2] = true;
  
      }
    bool g1_done = false; 
    int g1_index = 0;
    int g2_index = 0;
    int g1_ind = 0;
    int g2_ind = 0;
    for(int ag_it = 0; ag_it < 6; ag_it++){
     //  std::cout << ag[ag_it] << " : "; 
       if(!ag[ag_it] && !g1_done){
          g1Id[id] = ag_it;
          ag[ag_it] = true;
          g1_done = true; 
          g1_ind = ag_it;
       }
       else if(!ag[ag_it]){
          g2Id[id] = ag_it;
          ag[ag_it] = true;
          g2_ind = ag_it;
       }
    }
    for(std::list<Gamma>::iterator gam = glist_copy.begin(); gam != glist_copy.end(); gam++) {
       if(gam->id() == g1_ind){
          spare_gam1 = *gam; 
       }
       if(gam->id() == g2_ind){
          spare_gam2 = *gam;
       }
    }
/*
    for(int ag = 0; ag < 6; ag++){
       if(gid[ag] == g1_ind){
          g1_index = ag;
       }
       if(gid[ag] == g2_ind){
          g2_index = ag;
       }
    }
   */
    //std::cout << std::endl;
    /*for(int mg_it = 0; mg_it < 45; mg_it++){
       if(id == 44){
          std::cout  << g1Id[mg_it] << "    :    " << g2Id[mg_it] << std::endl; 
       }
    }*/

    if ( vtxflag == VERTEX_COE_SCALE ) { 

      // reconstruct Vtx - Special Update Vars for 6 gamma events.
      double Etot = 0.0;
      double avrX = 0.0;
      double avrY = 0.0;
      double avrZ = i->vz();
      //double sig2tot = 0.0;
      for( unsigned int gnum = 0; gnum < 6; gnum++ ) {
	// center of energy
	avrX += gx[gnum]*gE[gnum];
	avrY += gy[gnum]*gE[gnum];
	
	// Etotal
	Etot += gE[gnum];
        Etot += gE[gnum];
      }
      avrX = avrX/Etot;
      avrY = avrY/Etot;

      double TargetZ      = MTBP::T1TargetZ;//-1180.;
      double CalorimatorZ = MTBP::CSIZPosition;  //610.;
      double scaleFactor  = ( avrZ - TargetZ )/( CalorimatorZ - TargetZ );
  
      avrX = avrX*scaleFactor;
      avrY = avrY*scaleFactor;
     
      // update Pi0 vars      
      for( std::vector<Pi0>::iterator p=i->pi0().begin();
	   p!=i->pi0().end(); p++ ) {
	// 
	p->setVtx( avrX, avrY, avrZ );  // set Pi0 vertex
	p->updateVars();                // and update Pi0 vars
      }  
      // klong vars
      // add 1st spare gamme
      CLHEP::HepLorentzVector p_kl = CLHEP::HepLorentzVector( 0.,0.,0.,0. );
      CLHEP::Hep3Vector g1spare_p3  = CLHEP::Hep3Vector( gx[g1_index] - i->vx(), gy[g1_index] - i->vy(), gz[g1_index] - i->vz()); 
      g1spare_p3.setMag( gE[g1_index] );
      CLHEP::HepLorentzVector p_g1spare( g1spare_p3, gE[g1_index] );
      //add 2nd spare gamma
//      CLHEP::Hep3Vector g2spare_p3  = CLHEP::Hep3Vector( g2x[i->userFlag()] - i->vx(), 
//					  g2y[i->userFlag()] - i->vy(), 
//					  g2z[i->userFlag()] - i->vz()
//					  ); 
      CLHEP::Hep3Vector g2spare_p3  = CLHEP::Hep3Vector( gx[g2_index] - i->vx(), gy[g2_index] - i->vy(), gz[g2_index] - i->vz()); 
      g2spare_p3.setMag( gE[g2_index] );
      CLHEP::HepLorentzVector p_g2spare( g2spare_p3, gE[g2_index] );
      
      p_kl += p_g1spare;
      p_kl += p_g2spare;
      for( std::vector<Pi0>::iterator p=i->pi0().begin();
	   p!=i->pi0().end(); p++ ) {
	// 4 momentum
	CLHEP::HepLorentzVector p_pi0( p->p3(), p->e() );
	p_kl += p_pi0;
      }
     /* spare_clus1.setPos(cx[g1_index], cy[g1_index], cz[g1_index]);
      spare_clus2.setPos(cx[g2_index], cy[g2_index], cz[g2_index]);

      spare_clus1.setEnergy(cE[g1_index]);
      spare_clus2.setEnergy(cE[g2_index]);
      
      spare_clus1.setTime(ct[g1_index]);
      spare_clus2.setTime(ct[g2_index]);

      spare_clus1.setId(g1Id[id]);
      spare_clus2.setId(g2Id[id]);
      
      spare_gam1.setP3(g1spare_p3);
      spare_gam2.setP3(g2spare_p3);
      
      spare_gam1.setPos(gx[g1_index], gy[g1_index], gz[g1_index]);
      spare_gam2.setPos(gx[g2_index], gy[g2_index], gz[g2_index]);

      spare_gam1.setEnergy(gE[g1_index]);
      spare_gam2.setEnergy(gE[g2_index]);
      
      spare_gam1.setTime(gt[g1_index]);
      spare_gam2.setTime(gt[g2_index]);
    
      spare_gam1.setSigmaX(sigx[g1_index]);
      spare_gam2.setSigmaX(sigx[g2_index]);

      spare_gam1.setSigmaY(sigy[g1_index]);
      spare_gam2.setSigmaY(sigy[g2_index]);

      spare_gam1.setSigmaE(sigE[g1_index]);
      spare_gam2.setSigmaE(sigE[g2_index]);

      spare_gam1.setId(g1Id[id]);
      spare_gam2.setId(g2Id[id]);
      
      spare_gam1.setCluster(spare_clus1);
      spare_gam2.setCluster(spare_clus2);
      */
      double spE = spare_gam1.e() + spare_gam2.e();
      double spx = g1spare_p3.x() + g2spare_p3.x();
      double spy = g1spare_p3.y() + g2spare_p3.y();
      double spz = g1spare_p3.z() + g2spare_p3.z();
      double pi0_invm = sqrt(pow(spE,2)-pow(spx,2)-pow(spy,2)-pow(spz,2));
  
      spare_pi0.setEnergy(spE);
      spare_pi0.setMass(pi0_invm);
      spare_pi0.setVtx(avrX,avrY,avrZ);
      spare_pi0.setP3(spx,spy,spz);
      spare_pi0.setId(2);
      spare_pi0.setGamma(spare_gam1,spare_gam2);
 
      //i->addGamma(spare_gam1);      
      //i->addGamma(spare_gam2);     /// try adding the two photons back  
      i->addPi0(spare_pi0);
      i->setVtx( avrX,avrY,avrZ );
      i->setP3( p_kl.vect() );
      i->setEnergy( p_kl.e() );
      i->setMass( p_kl.m() );
    } else { // Construct Kaon for Fixed Vertex

    // klong vars
      CLHEP::Hep3Vector g1spare_p3  = CLHEP::Hep3Vector( gx[g1_index] - i->vx(), gy[g1_index] - i->vy(), gz[g1_index] - i->vz()); 
      g1spare_p3.setMag( gE[g1_index] );
      CLHEP::HepLorentzVector p_g1spare( g1spare_p3, gE[g1_index] );

      CLHEP::Hep3Vector g2spare_p3  = CLHEP::Hep3Vector( gx[g2_index] - i->vx(), gy[g2_index] - i->vy(), gz[g2_index] - i->vz()); 
      g2spare_p3.setMag( gE[g2_index] );
      CLHEP::HepLorentzVector p_g2spare( g2spare_p3, gE[g2_index] );

      CLHEP::HepLorentzVector p_kl = CLHEP::HepLorentzVector( i->p3(), i->e() );
      p_kl += p_g1spare;
      p_kl += p_g2spare;
      /*spare_clus1.setPos(cx[g1_index], cy[g1_index], cz[g1_index]);
      spare_clus2.setPos(cx[g2_index], cy[g2_index], cz[g2_index]);

      spare_clus1.setEnergy(cE[g1_index]);
      spare_clus2.setEnergy(cE[g2_index]);
      
      spare_clus1.setTime(ct[g1_index]);
      spare_clus2.setTime(ct[g2_index]);

      spare_clus1.setId(g1Id[id]);
      spare_clus2.setId(g2Id[id]);
      
      spare_gam1.setP3(g1spare_p3);
      spare_gam2.setP3(g2spare_p3);

      spare_gam1.setPos(gx[g1_index], gy[g1_index], gz[g1_index]);
      spare_gam2.setPos(gx[g2_index], gy[g2_index], gz[g2_index]);
      
      spare_gam1.setEnergy(gE[g1_index]);
      spare_gam2.setEnergy(gE[g2_index]);
      
      spare_gam1.setTime(gt[g1_index]);
      spare_gam2.setTime(gt[g2_index]);
 
      spare_gam1.setSigmaX(sigx[g1_index]);
      spare_gam1.setSigmaX(sigx[g1_index]);

      spare_gam1.setSigmaY(sigy[g1_index]);
      spare_gam2.setSigmaY(sigy[g2_index]);

      spare_gam2.setSigmaE(sigE[g2_index]);
      spare_gam2.setSigmaE(sigE[g2_index]);

      spare_gam1.setId(g1Id[id]);
      spare_gam2.setId(g2Id[id]);
 
      spare_gam1.setCluster(spare_clus1);
      spare_gam2.setCluster(spare_clus2);
      */
      double spE = spare_gam1.e() + spare_gam2.e();
      double spx = g1spare_p3.x() + g2spare_p3.x();
      double spy = g1spare_p3.y() + g2spare_p3.y();
      double spz = g1spare_p3.z() + g2spare_p3.z();
      double pi0_invm = sqrt(pow(spE,2)-pow(spx,2)-pow(spy,2)-pow(spz,2));
  
      spare_pi0.setEnergy(spE);
      spare_pi0.setMass(pi0_invm);
      spare_pi0.setVtx(0,0,i->vz());
      spare_pi0.setP3(spx,spy,spz);
      spare_pi0.setId(2);
      spare_pi0.setGamma(spare_gam1,spare_gam2);
      
      //i->addGamma(spare_gam1);
      //i->addGamma(spare_gam2);
      i->addPi0(spare_pi0);
      i->setP3( p_kl.vect() );
      i->setEnergy( p_kl.e() );
      i->setMass( p_kl.m() );
      //std::cout << *i << std::endl;
 
      
    //  std::cout << *i << std::endl;

    }
 id++; 
  }
  /////  Get the Constrained fit chisq and resort     
  if ( vtxflag == VERTEX_COE_SCALE ) {
  double chisq_temp;
  double chisq_best = 9999999;
  for(int j = 0; j < k2pi0gg.size(); j++){
     if(std::isnan(k2pi0gg[j].vz())){
        chisq_temp = 9999999; 
        continue;
     }
     chisq_temp = E14GNAnaFunction::getFunction()->cfit_klpi0pi0X(k2pi0gg[j]);
     if(chisq_temp < chisq_best){
        chisq_best = chisq_temp;
     }
     //std::cout << chisq_temp << std::endl;
     k2pi0gg[j].setChisqZ(chisq_temp); //replace chisqZ as cfit_chisq
     //std::cout << k2pi0gg[j].chisqZ() << std::endl;
  }
  //std::cout << "good  " << k2pi0gg[0].sortFlag() << std::endl;
  // sort by Constrained_fit_Chisq
   //  for(int j = 0; j < k2pi0gg.size(); j++){
   //    std::cout << j << "  :  " << k2pi0gg[j].chisqZ() << std::endl;
   //  }
  sort( k2pi0gg.begin(), k2pi0gg.end() );
  }


#if DEBUG_LEVEL > 0 
  {
    unsigned int i = 0;
    for(std::vector<Klong>::iterator k = k2pi0gg.begin();
	k != k2pi0gg.end(); 
	k++) {
      std::cout<<"id = "<<k2pi0gg[i].id()<<std::endl;
      std::cout<<"userFlag = "<<k2pi0gg[i].userFlag()<<std::endl;
      for( std::vector<Pi0>::const_iterator p=k2pi0gg[i].pi0().begin();
	   p!=k2pi0gg[i].pi0().end(); p++ ) {
	std::cout<<"pion id = "<<p->id()<<std::endl;
	std::cout<<"  g1 id = "<<p->g1().id()<<std::endl;
	std::cout<<"  g2 id = "<<p->g2().id()<<std::endl;
      }
      i++;
      std::cout<<std::endl;
    }
  }
#endif
  return( k2pi0gg );
}





//Perdue 20060401
/////
std::vector<Klong> 
RecKlong::recK5gfuse( const std::list<Gamma>& glist, 
		      int vtxflag, 
		      double pi0sig2cut )
{
#if DEBUG_G5FUSE_LEVEL > 0
  std::cout<<"Entering recK5gfuse...\n";
#endif
  // Reconstruction object internal to recK5gfuse
  RecKlong recklong_inner;

  // k5g - the "final" Kaon
  // kl[k]  - the jth Kaon, where k <-> glist6[i,j] and identifies the 
  // "split" gamma and the way the energy was shared.
  //
  std::vector<Klong> k5g;
  std::vector<Klong> kl[25];

  // 
  // Set up new gamma lists.
  // -------------------------
  // "6"-gamma lists indexed by ij where i = identifies the "split" gamma 
  // and j identifies how the energy was shared.  These lists are used to 
  // make 3pi0 reconstructions.
  std::list<Gamma> glist6[5][5];

  // Create 5 copies of glist6 -> glist6i.
  // Split different gammas each time, & label acoording to
  // the split gamma id: 
  //    1, 2, 3, 4 - 40 => split 0
  //    2, 3, 4, 0 - 41 => split 1
  //    3, 4, 0, 1 - 42 => split 2
  //    4, 0, 1, 2 - 43 => split 3
  //    0, 1, 2, 3 - 44 => split 4
  // Do 5 different energy splittings (10%,90%), (20%,80%), (30%,70%), 
  // (40%,60%), and (50%,50%) => second index (j)
  // For each copy, do a "3pi0" reconstruction.

  for ( unsigned int i = 0; i < 5; i++ ) {
    for ( unsigned int j = 0; j < 5; j++ ) { 
      glist6[i][j] = glist;
      std::list<Gamma>::iterator splitgamma = glist6[i][j].begin();
      for ( unsigned int k = 0; k < i; k++ ) {
	splitgamma++;
      }
      Gamma gamma_new1, gamma_new2;
      //      gamma_new1 = GammaCopy( *splitgamma );
      //      gamma_new2 = GammaCopy( *splitgamma );
      gamma_new1 = *splitgamma ;
      gamma_new2 = *splitgamma ;
      glist6[i][j].erase( splitgamma );
      gamma_new1.setId( 5 );  
      double factor = ( (float) (j + 1) )/10.0;
      double tempe1 = factor * gamma_new1.e();
      double tempe2 = (1 - factor) * gamma_new2.e();
      gamma_new1.setEnergy( tempe1 );
      gamma_new2.setEnergy( tempe2 );
      glist6[i][j].push_back( gamma_new1 );
      glist6[i][j].push_back( gamma_new2 );
      glist6[i][j].sort();  // Sort by Energy
      {
	int k = 0;
	for ( std::list<Gamma>::iterator p = glist6[i][j].begin();
	      p != glist6[i][j].end(); 
	      p++ ) {
	  p->setId( k );   // Re-index
	  k++;
	}    
      }
    } 
  } // End gamma splitting & energy modification

#if DEBUG_G5FUSE_LEVEL > 0
  for ( unsigned int i = 0; i < 5; i++ ) {
    for ( unsigned int j = 0; j < 5; j++ ) { 
      std::cout<<"glist6["<<i<<"]["<<j<<"].e()'s: ";
      for ( std::list<Gamma>::iterator p = glist6[i][j].begin();
      	    p != glist6[i][j].end(); 
      	    p++ ) {
      	std::cout<<" "<<p->e();
      }    

      std::cout<<std::endl;
    } 
  } 
#endif

  // Here, the six gammas from each set are used to try to 
  // reconstruct 3pi0s.  
  int userFlag = 20151101;//set 2015 flag temporary (by S.Shinohara, 20190617)
  for ( unsigned int i = 0; i < 5; i++) {
    for ( unsigned int j = 0; j < 5; j++) {
      unsigned int index = 5*i + j;
      kl[index] = 
	recklong_inner.recK3pi0( glist6[i][j], userFlag, vtxflag, pi0sig2cut );
#if DEBUG_G5FUSE_LEVEL > 0
      std::cout<<std::endl;
      std::cout<<"glist6["<<i<<"]["<<j<<"].e()'s: ";
      for ( std::list<Gamma>::iterator p = glist6[i][j].begin();
	    p != glist6[i][j].end(); 
	    p++ ) {
	std::cout<<" "<<p->e();
      }    
      std::cout<<std::endl;
      std::cout<<"kl["<<index<<" = "<<i<<"]["<<j<<"].size() = "
	       <<kl[index].size()<<std::endl;
#endif
      if (kl[index].size() > 0) { 
#if DEBUG_G5FUSE_LEVEL > 0
	unsigned int count = 0;
#endif
	for (std::vector<Klong>::iterator kaon = kl[index].begin();
	     kaon != kl[index].end();
	     kaon++) {
	  kaon->setUserFlag( i );
#if DEBUG_G5FUSE_LEVEL > 0
	  std::cout<<"kl["<<index<<"],#"<<count<<".chisqZ() = "
		   <<kaon->chisqZ()<<std::endl;
	  count++;
#endif
	  k5g.push_back( (*kaon) );
	}
      }
    }
  } // End 3pi0 Recons
#if DEBUG_G5FUSE_LEVEL > 0
  std::cout<<"k5g.size() = "<<k5g.size()<<std::endl;
#endif

  // sort by ChisqZ
  sort( k5g.begin(), k5g.end() );

#if DEBUG_G5FUSE_LEVEL > 0
  if (k5g.size() > 0) { 
      unsigned int j = 0;
      for (std::vector<Klong>::iterator k = k5g.begin();
	   k != k5g.end();
	   k++) {
	std::cout<<"k5g#"<<j<<".chisqZ() = "<<k->chisqZ()<<std::endl;
	j++;
      }
    }
  std::cout<<"Exiting recK5gfuse...\n\n";
#endif

  return( k5g );
}



/////
std::vector<Klong> 
RecKlong::recK5gam(const std::list<Gamma>& glist, int vtxflag, double pi0sig2cut){


	double mkl = MTBP::KL_MASS;
	double mp0 = MTBP::Pi0_MASS;	
			
	Rec2g rec2g; // pi0 reconstruction 
	std::list<Pi0> pi0list = rec2g.recPi0withConstM( glist );

	std::vector<Klong> k5gam;
		

 	for(std::list<Pi0>::const_iterator p1=pi0list.begin(); p1!=pi0list.end(); p1++){
		for(std::list<Pi0>::const_iterator p2=pi0list.begin(); p2!=pi0list.end(); p2++){
				
			// discard overlap of photons
			if( p2->g1().id() == p1->g1().id() || p2->g1().id() == p1->g2().id() 
			 || p2->g2().id() == p1->g1().id() || p2->g2().id() == p1->g2().id()) continue;


			// discard bad pi0s
			if( p1->status() != 1 || p2->status() != 1 || 
				p1->recZsig2() > pi0sig2cut || p2->recZsig2() > pi0sig2cut)continue;
				
			double recz_KL = (p1->recZ()/p1->recZsig2()+p2->recZ()/p2->recZsig2())
				/(1.0/p1->recZsig2() + 1.0/p2->recZsig2());
			
			Pi0 pi1p = *p1;
			Pi0 pi2p = *p2;
			pi1p.setRecZ(recz_KL);
			pi1p.updateVars();
			pi2p.setRecZ(recz_KL);
			pi2p.updateVars();
			
			CLHEP::HepLorentzVector Ppi1(pi1p.p3(), pi1p.e());
			CLHEP::HepLorentzVector Ppi2(pi2p.p3(), pi2p.e());
			
			double M = (Ppi1+Ppi2).m();
			
			// scale pi0 so that mass of pi0 becomes PDG value
			Ppi1 *= mp0/Ppi1.m();
			Ppi2 *= mp0/Ppi2.m();
				
						
			double argsqrtp0 = (mkl*mkl-(M-mp0)*(M-mp0))*(mkl*mkl-(M+mp0)*(M+mp0));
			if(argsqrtp0<0) continue;
			
			double P0 = sqrt(argsqrtp0)/2/mkl;
			double E0 = sqrt(P0*P0+M*M);
			double bt0 = P0/E0;
			//double gam0 = 1/sqrt(1-bt0*bt0);
			
			double A = (Ppi1.e()+Ppi2.e())*(Ppi1.e()+Ppi2.e())-bt0*bt0*(Ppi1.z()+Ppi2.z())*(Ppi1.z()+Ppi2.z());
			double B = -2*(1-bt0*bt0)*(Ppi1.z()+Ppi2.z())*(Ppi1.e()+Ppi2.e());
			double C = (Ppi1.z()+Ppi2.z())*(Ppi1.z()+Ppi2.z())-bt0*bt0*(Ppi1.e()+Ppi2.e())*(Ppi1.e()+Ppi2.e());
			double D = (Ppi1.x()+Ppi2.x())*(Ppi1.x()+Ppi2.x())+(Ppi1.y()+Ppi2.y())*(Ppi1.y()+Ppi2.y());
			
			double det = B*B-4*(C+D)*(A-D);
			
			if(det<0) continue;
			double bt_sol_m = (-B-sqrt(det))/(2*(A-D));
			double bt_sol_p = (-B+sqrt(det))/(2*(A-D));

			double gam_m = 1/sqrt(1-bt_sol_m*bt_sol_m);
			double gam_p = 1/sqrt(1-bt_sol_p*bt_sol_p);


			double eKL_m = gam_m*mkl;
			double eKL_p = gam_p*mkl;
			
			double chisqZ = (p1->recZ()-recz_KL)*(p1->recZ()-recz_KL)/p1->recZsig2()
					  +(p2->recZ()-recz_KL)*(p2->recZ()-recz_KL)/p2->recZsig2();
			
  			double deltaZ = fabs(p1->recZ()-p2->recZ());
		  
			pi1p.setRecZsig2(p1->recZsig2());
			pi1p.setVtx(0.0, 0.0, p1->recZ());
      		pi1p.setP3(Ppi1.vect());
      		pi1p.setEnergy(Ppi1.e());
     		pi1p.setMass(p1->m());

			pi2p.setRecZsig2(p2->recZsig2());
			pi2p.setVtx(0.0, 0.0, p2->recZ());
      		pi2p.setP3(Ppi2.vect());
      		pi2p.setEnergy(Ppi2.e());
     		pi2p.setMass(p2->m());

			Klong klong;
			klong.setVertexFlag(vtxflag);
			klong.setVtx(CLHEP::Hep3Vector(0,0,recz_KL));
			klong.addPi0(pi1p);
			klong.addPi0(pi2p);

  			
			for( std::list<Gamma>::const_iterator g =glist.begin(); g!=glist.end(); g++){
				if ( p1->g1().id() == g->id() || p1->g2().id() == g->id() ||
				     p2->g1().id() == g->id() || p2->g2().id() == g->id()) continue; // same gamma.

			   	CLHEP::Hep3Vector gp = CLHEP::Hep3Vector(g->x(), g->y(), g->z()-recz_KL);
				gp.setMag(g->e());
				
				Gamma gam = *g;
				gam.setEnergy(g->e());
				gam.setP3(gp);

				klong.addGamma(gam);
			}

			klong.setChisqZ(chisqZ);
			klong.setMass(mkl);
			klong.setDeltaZ(deltaZ);
			klong.setEnergy(eKL_m);
			klong.setP3(0,0,sqrt(eKL_m*eKL_m-mkl*mkl));
		
			if(bt_sol_m>0) k5gam.push_back(klong);
			
			klong.setEnergy(eKL_p);
			klong.setP3(0,0,sqrt(eKL_p*eKL_p-mkl*mkl));
			if(bt_sol_p>0) k5gam.push_back(klong);

		}
	}

	// sort by ChisqZ
	sort( k5gam.begin(), k5gam.end() );
	// set id
  
	int id = 0;
	for(std::vector<Klong>::iterator i=k5gam.begin(); i!=k5gam.end(); i++){
		i->setId( id );
		id++;
	}


	return k5gam;
}


/////
std::vector<Klong> 
RecKlong::recK5gam_2pi0g(const std::list<Gamma>& glist, int vtxflag, double pi0sig2cut){
  
	double mkl = MTBP::KL_MASS;
	double mp0 = MTBP::Pi0_MASS;	
			
	Rec2g rec2g; // pi0 reconstruction 
	std::list<Pi0> pi0list = rec2g.recPi0withConstM( glist );

	std::vector<Klong> k5gam;
		

 	for(std::list<Pi0>::const_iterator p1=pi0list.begin(); p1!=pi0list.end(); p1++){
		for(std::list<Pi0>::const_iterator p2=pi0list.begin(); p2!=pi0list.end(); p2++){
			

			// discard overlap of photons
			if( p2->g1().id() == p1->g1().id() || p2->g1().id() == p1->g2().id() 
			 || p2->g2().id() == p1->g1().id() || p2->g2().id() == p1->g2().id()) continue;


			// discard bad pi0s
			if( p1->status() != 1 || p2->status() != 1 || 
				p1->recZsig2() > pi0sig2cut || p2->recZsig2() > pi0sig2cut)continue;
				
			double recz_KL = (p1->recZ()/p1->recZsig2()+p2->recZ()/p2->recZsig2())
				/(1.0/p1->recZsig2() + 1.0/p2->recZsig2());
			
			Pi0 pi1p = *p1;
			Pi0 pi2p = *p2;
			pi1p.setRecZ(recz_KL);
			pi1p.setVtx(0.0, 0.0, recz_KL);
			pi1p.updateVars();
			pi2p.setRecZ(recz_KL);
			pi2p.setVtx(0.0, 0.0, recz_KL);
			pi2p.updateVars();
			
			CLHEP::HepLorentzVector Ppi1(pi1p.p3(), pi1p.e());
			CLHEP::HepLorentzVector Ppi2(pi2p.p3(), pi2p.e());
			
			// scale pi0 so that mass of pi0 becomes PDG value
			Ppi1 *= mp0/Ppi1.m();
			Ppi2 *= mp0/Ppi2.m();
			
			for( std::list<Gamma>::const_iterator g =glist.begin(); g!=glist.end(); g++){
				if ( p1->g1().id() == g->id() || p1->g2().id() == g->id() ||
				     p2->g1().id() == g->id() || p2->g2().id() == g->id()) continue; // same gamma.

			   	CLHEP::Hep3Vector gp = CLHEP::Hep3Vector(g->x(), g->y(), g->z()-recz_KL);
				gp.setMag(g->e());
				
				Gamma gam = *g;
				gam.setEnergy(g->e());
				gam.setP3(gp);

				CLHEP::HepLorentzVector Pg(gp.x(), gp.y(), gp.z(), gp.mag());

				double mpipig = (Ppi1+Ppi2+Pg).m();


				if(fabs(mpipig-mkl)<100){
				
					pi1p.setRecZsig2(p1->recZsig2());
					pi1p.setVtx(0.0, 0.0, p1->recZ());
    		  		pi1p.setP3(Ppi1.vect());
    		  		pi1p.setEnergy(Ppi1.e());
    		 		pi1p.setMass(mp0);
    		 		pi1p.setRecZ(p1->recZ());

					pi2p.setRecZsig2(p2->recZsig2());
					pi2p.setVtx(0.0, 0.0, p2->recZ());
    		  		pi2p.setP3(Ppi2.vect());
    		  		pi2p.setEnergy(Ppi2.e());
    		 		pi2p.setMass(mp0);
    		 		pi2p.setRecZ(p2->recZ());


					double chisqZ = (p1->recZ()-recz_KL)*(p1->recZ()-recz_KL)/p1->recZsig2()
					  +(p2->recZ()-recz_KL)*(p2->recZ()-recz_KL)/p2->recZsig2();
			
					double deltaZ = fabs(p1->recZ()-p2->recZ());
		  

					Klong klong;
					klong.setVertexFlag(vtxflag);
					klong.setVtx(CLHEP::Hep3Vector(0,0,recz_KL));
					klong.addPi0(pi1p);
					klong.addPi0(pi2p);	
					klong.setChisqZ(chisqZ);
					klong.setMass(mpipig);
					klong.setDeltaZ(deltaZ);
					klong.addGamma(gam);
					
					double ek = (Ppi1+Ppi2+Pg).e();
					klong.setEnergy(ek);
					klong.setP3(pi1p.p3().x()+pi2p.p3().x()+gam.p3().x(),
								pi1p.p3().y()+pi2p.p3().y()+gam.p3().y(),
								pi1p.p3().z()+pi2p.p3().z()+gam.p3().z());
					
					k5gam.push_back(klong);
				}
			}


		}
	}

	// sort by ChisqZ
	sort( k5gam.begin(), k5gam.end() );
	// set id
  
	int id = 0;
	for(std::vector<Klong>::iterator i=k5gam.begin(); i!=k5gam.end(); i++){
		i->setId( id );
		id++;
	}


	return k5gam;
}

/*
double RecKlong::cfit_klpi0pi0X(Klong &KL)
{
        double T1TargetZ = MTBP::T1TargetZ;
        double CSIZPosition = MTBP::CSIZPosition; 
        double MASS_PI0 = MTBP::Pi0_MASS;
	double chisq_keep   = HUGE;
	double chisq_keep_v = HUGE;
	double chisq = -1.0;
	
	int ifail = 0;
	int error_flag = 0;
	int fit_updated = 0;
	int npi0s = 3; // really 2, but with 2 extra gammas


	double px1[3]; 
	double py1[3];
	double pz1[3]; 
	double E1[3];  
	double x1[3];  
	double y1[3];  
	double z1[3];
        double sx1[3];
        double sy1[3];
        double sE1[3];
	double R1[3];
	
	double px2[3]; 
	double py2[3];
	double pz2[3]; 
	double E2[3];  
	double x2[3];  
	double y2[3];  
	double z2[3];
        double sx2[3];
        double sy2[3];
        double sE2[3];
	double R2[3];
	
	double klpx, klpy, klpz, klE, klE0 = 0.;
	double vx, vy, vz;

	// Prepare initial values
	int idx;
	for( idx=0; idx<npi0s; idx++) { //grab the "3" pi0s
		x1[idx] = KL.pi0()[idx].g1().x();
		y1[idx] = KL.pi0()[idx].g1().y();
		z1[idx] = KL.pi0()[idx].g1().z();
		E1[idx] = KL.pi0()[idx].g1().e();
                sx1[idx] = KL.pi0()[idx].g1().sigmaX();
                sy1[idx] = KL.pi0()[idx].g1().sigmaY();
                sE1[idx] = KL.pi0()[idx].g1().sigmaE();

		x2[idx] = KL.pi0()[idx].g2().x();
		y2[idx] = KL.pi0()[idx].g2().y();
		z2[idx] = KL.pi0()[idx].g2().z();
		E2[idx] = KL.pi0()[idx].g2().e();
                sx2[idx] = KL.pi0()[idx].g2().sigmaX();
                sy2[idx] = KL.pi0()[idx].g2().sigmaY();
                sE2[idx] = KL.pi0()[idx].g2().sigmaE();
		
		klE0 += E1[idx] + E2[idx];
	}
	vx = KL.vx();
	vy = KL.vy();
	vz = KL.vz();
		
	// a() & v()	
	
	TVectorD a0(npi0s*6);
        TVectorD a(npi0s*6);
	TVectorD v0(3);
        TVectorD v(3);
	
	for( idx=0; idx<npi0s; idx++) {
		a0(idx*6 + 1)  = x1[idx];
		a0(idx*6 + 2)  = y1[idx];
		a0(idx*6 + 3)  = E1[idx];
		a0(idx*6 + 4)  = x2[idx];
		a0(idx*6 + 5)  = y2[idx];
		a0(idx*6 + 6)  = E2[idx];
	}
	
	v0(1) = vx;
	v0(2) = vy;
	v0(3) = vz;
	
	TMatrixDSym Va0(npi0s*6);  // (npi0s*6) x (npi0s*6)
	
	for( idx=0; idx<npi0s; idx++) {
		Va0(idx*6+1 ,idx*6+1) = pow(sx1[idx],2);
		Va0(idx*6+2 ,idx*6+2) = pow(sy1[idx],2);
		Va0(idx*6+3 ,idx*6+3) = pow(sE1[idx],2);
		Va0(idx*6+4 ,idx*6+4) = pow(sx2[idx],2);
		Va0(idx*6+5 ,idx*6+5) = pow(sy2[idx],2);
		Va0(idx*6+6 ,idx*6+6) = pow(sE2[idx],2);
	}
	
	a = a0;
	v = v0;		
	
	TMatrixD D(npi0s+2,npi0s*6,0); // = del H/del alpha(alpha_A)
	TMatrixD E(npi0s+2,3,0);      // = del H/del alpha(v)
	
	TVectorD d(npi0s+2); // = H(alpha_A)
 
	TVectorD da0(npi0s*6); 
	TVectorD dv0(3); 
	
	TMatrixD VD(npi0s+2,npi0s+2);	
	TMatrixD VE(3,3);
	
	TVectorD Lambda0(npi0s+2,1);
	TVectorD Lambda(npi0s+2,1);
	
	TVectorD v_keep     = v;
	TMatrixD VE_keep;
        VE_keep            = VE;
        TMatrixD VD_keep;
        VD    	           = VD;
	TVectorD Lambda0_keep;
        Lambda0_keep	   = Lambda0;
	TMatrixD E_keep;
        E_keep      	   = E;
        TMatrixD D_keep;
        D_keep      	   = D;
	TVectorD a_keep;
        a_keep 	    	   = a;
	
	TVectorD v_keep_v;
        v_keep_v  		= v;
	TMatrixD VE_keep_v;
        VE_keep_v    	        = VE;
	TMatrixD VD_keep_v;
        VD_keep_v     	        = VD;
	TVectorD Lambda0_keep_v;
        Lambda0_keep_v	        = Lambda0;
	TMatrixD E_keep_v;
        E_keep_v      	        = E;
	TMatrixD D_keep_v;
        D_keep_v      	        = D;

	//=============================================================	
	// start minizing loop
	
	int it,it_v;
	for(it=0;it<MAX_ITERACTIONS;it++) {		// Global minizing loop
	chisq_keep_v = HUGE;				// reset chisq_keep_v
	for(it_v=0;it_v<MAX_ITERACTIONS;it_v++) {	// KL vertex (v) minizing loop	
	
	v0 = v; 	// set initial vertex to be expanding point
	error_flag = 0;	// reset error flag
		
	double dpx1_dx1[3], dpx2_dx2[3];
	double dpx1_dy1[3], dpx2_dy2[3];
	double dpx1_dE1[3], dpx2_dE2[3];
					
	double dpy1_dx1[3], dpy2_dx2[3];
	double dpy1_dy1[3], dpy2_dy2[3];
	double dpy1_dE1[3], dpy2_dE2[3];
					
	double dpz1_dx1[3], dpz2_dx2[3];
	double dpz1_dy1[3], dpz2_dy2[3];
	double dpz1_dE1[3], dpz2_dE2[3];
					
	double dR1_dx1[3],  dR2_dx2[3];
	double dR1_dy1[3],  dR2_dy2[3];
	double dR1_dE1[3],  dR2_dE2[3];
	
	double dpx1_dvx[3], dpx2_dvx[3];
	double dpx1_dvy[3], dpx2_dvy[3];
	double dpx1_dvz[3], dpx2_dvz[3];
					
	double dpy1_dvx[3], dpy2_dvx[3];
	double dpy1_dvy[3], dpy2_dvy[3];
	double dpy1_dvz[3], dpy2_dvz[3];
					
	double dpz1_dvx[3], dpz2_dvx[3];
	double dpz1_dvy[3], dpz2_dvy[3];
	double dpz1_dvz[3], dpz2_dvz[3];
					
	double dR1_dvx[3],  dR2_dvx[3];
	double dR1_dvy[3],  dR2_dvy[3];
	double dR1_dvz[3],  dR2_dvz[3];         

	double dklpx_dvx, dklpy_dvx, dklpz_dvx;
	double dklpx_dvy, dklpy_dvy, dklpz_dvy;
	double dklpx_dvz, dklpy_dvz, dklpz_dvz;
		
	klpx = klpy = klpz = klE = 0.;	
	dklpx_dvx = dklpy_dvx = dklpz_dvx = 0.;
	dklpx_dvy = dklpy_dvy = dklpz_dvy = 0.;
	dklpx_dvz = dklpy_dvz = dklpz_dvz = 0.;
	
	for( idx=0; idx<npi0s; idx++) {
		
		R1[idx] = sqrt(pow(x1[idx]-vx,2) + pow(y1[idx]-vy,2) + pow(z1[idx]-vz,2));
		R2[idx] = sqrt(pow(x2[idx]-vx,2) + pow(y2[idx]-vy,2) + pow(z2[idx]-vz,2));
		
		px1[idx] = (x1[idx]-vx)/R1[idx] * E1[idx];
		py1[idx] = (y1[idx]-vy)/R1[idx] * E1[idx];
		pz1[idx] = (z1[idx]-vz)/R1[idx] * E1[idx];
		
		px2[idx] = (x2[idx]-vx)/R2[idx] * E2[idx];
		py2[idx] = (y2[idx]-vy)/R2[idx] * E2[idx];
		pz2[idx] = (z2[idx]-vz)/R2[idx] * E2[idx];
		
		klpx += px1[idx] + px2[idx];
		klpy += py1[idx] + py2[idx];
		klpz += pz1[idx] + pz2[idx];
		klE  += E1[idx]  + E2[idx];
		
		dR1_dx1[idx] = (x1[idx] - vx) / R1[idx];
		dR1_dy1[idx] = (y1[idx] - vy) / R1[idx];
		dR1_dE1[idx] = 0;
		
		dpx1_dx1[idx] = (E1[idx] - px1[idx] * dR1_dx1[idx])/R1[idx];
		dpx1_dy1[idx] = (        - px1[idx] * dR1_dy1[idx])/R1[idx];
		dpx1_dE1[idx] = (x1[idx] - vx) / R1[idx];
		
		dpy1_dx1[idx] = (        - py1[idx] * dR1_dx1[idx])/R1[idx];
		dpy1_dy1[idx] = (E1[idx] - py1[idx] * dR1_dy1[idx])/R1[idx];
		dpy1_dE1[idx] = (y1[idx] - vy) / R1[idx];	      
		
		dpz1_dx1[idx] = (        - pz1[idx] * dR1_dx1[idx])/R1[idx];
		dpz1_dy1[idx] = (	 - pz1[idx] * dR1_dy1[idx])/R1[idx];
		dpz1_dE1[idx] = (z1[idx] - vz) / R1[idx];
		
		dR2_dx2[idx] = (x2[idx] - vx) / R2[idx];
		dR2_dy2[idx] = (y2[idx] - vy) / R2[idx];
		dR2_dE2[idx] = 0;
		
		dpx2_dx2[idx] = (E2[idx] - px2[idx] * dR2_dx2[idx])/R2[idx];
		dpx2_dy2[idx] = (        - px2[idx] * dR2_dy2[idx])/R2[idx];
		dpx2_dE2[idx] = (x2[idx] - vx) / R2[idx];
		
		dpy2_dx2[idx] = (        - py2[idx] * dR2_dx2[idx])/R2[idx];
		dpy2_dy2[idx] = (E2[idx] - py2[idx] * dR2_dy2[idx])/R2[idx];
		dpy2_dE2[idx] = (y2[idx] - vy) / R2[idx];	      
		
		dpz2_dx2[idx] = (        - pz2[idx] * dR2_dx2[idx])/R2[idx];
		dpz2_dy2[idx] = (	 - pz2[idx] * dR2_dy2[idx])/R2[idx];
		dpz2_dE2[idx] = (z2[idx] - vz) / R2[idx];	
		
		dR1_dvx[idx] = -(x1[idx] - vx) / R1[idx];
		dR1_dvy[idx] = -(y1[idx] - vy) / R1[idx];
		dR1_dvz[idx] = -(z1[idx] - vz) / R1[idx];	      
		
		dpx1_dvx[idx] = (-E1[idx] - px1[idx] * dR1_dvx[idx])/R1[idx];
		dpx1_dvy[idx] = (         - px1[idx] * dR1_dvy[idx])/R1[idx];
		dpx1_dvz[idx] = (         - px1[idx] * dR1_dvz[idx])/R1[idx];

		dpy1_dvx[idx] = (         - py1[idx] * dR1_dvx[idx])/R1[idx];
		dpy1_dvy[idx] = (-E1[idx] - py1[idx] * dR1_dvy[idx])/R1[idx];
		dpy1_dvz[idx] = (         - py1[idx] * dR1_dvz[idx])/R1[idx];
		
		dpz1_dvx[idx] = (         - pz1[idx] * dR1_dvx[idx])/R1[idx];
		dpz1_dvy[idx] = (         - pz1[idx] * dR1_dvy[idx])/R1[idx];
		dpz1_dvz[idx] = (-E1[idx] - pz1[idx] * dR1_dvz[idx])/R1[idx];	
		
		dR2_dvx[idx] = -(x2[idx] - vx) / R2[idx];
		dR2_dvy[idx] = -(y2[idx] - vy) / R2[idx];
		dR2_dvz[idx] = -(z2[idx] - vz) / R2[idx];	      
		
		dpx2_dvx[idx] = (-E2[idx] - px2[idx] * dR2_dvx[idx])/R2[idx];
		dpx2_dvy[idx] = (         - px2[idx] * dR2_dvy[idx])/R2[idx];
		dpx2_dvz[idx] = (         - px2[idx] * dR2_dvz[idx])/R2[idx];

		dpy2_dvx[idx] = (         - py2[idx] * dR2_dvx[idx])/R2[idx];
		dpy2_dvy[idx] = (-E2[idx] - py2[idx] * dR2_dvy[idx])/R2[idx];
		dpy2_dvz[idx] = (         - py2[idx] * dR2_dvz[idx])/R2[idx];
		
		dpz2_dvx[idx] = (         - pz2[idx] * dR2_dvx[idx])/R2[idx];
		dpz2_dvy[idx] = (         - pz2[idx] * dR2_dvy[idx])/R2[idx];
		dpz2_dvz[idx] = (-E2[idx] - pz2[idx] * dR2_dvz[idx])/R2[idx];	
		
		dklpx_dvx += (dpx1_dvx[idx] + dpx2_dvx[idx]);
		dklpx_dvy += (dpx1_dvy[idx] + dpx2_dvy[idx]);
		dklpx_dvz += (dpx1_dvz[idx] + dpx2_dvz[idx]);
		
		dklpy_dvx += (dpy1_dvx[idx] + dpy2_dvx[idx]);
		dklpy_dvy += (dpy1_dvy[idx] + dpy2_dvy[idx]);
		dklpy_dvz += (dpy1_dvz[idx] + dpy2_dvz[idx]);
		
		dklpz_dvx += (dpz1_dvx[idx] + dpz2_dvx[idx]);
		dklpz_dvy += (dpz1_dvy[idx] + dpz2_dvy[idx]);
		dklpz_dvz += (dpz1_dvz[idx] + dpz2_dvz[idx]);
	}
	
	for( idx=0; idx<npi0s-1; idx++) {
		d(idx+1) =   pow(E1[idx]  + E2[idx] ,2) 
			   - pow(px1[idx] + px2[idx],2) 
			   - pow(py1[idx] + py2[idx],2) 
			   - pow(pz1[idx] + pz2[idx],2) 
			   - MASS_PI0*MASS_PI0;       
	}	
	d(npi0s) = klE*klE - klpx*klpx - klpy*klpy - klpz*klpz - MASS_KL*MASS_KL;
	
	d(npi0s+1) = - (CSIZPosition - T1TargetZ)*vx*klE; // length to Csi
	d(npi0s+2) = - (CSIZPosition - T1TargetZ)*vy*klE; //T1TargetZ is negative

        double ce_x = 0 ;
        double ce_y = 0 ;
 
 
	for( idx=0; idx<npi0s; idx++) {
		d(npi0s+1) += (vz-T1TargetZ)*(E1[idx]*x1[idx] + E2[idx]*x2[idx]);
		d(npi0s+2) += (vz-T1TargetZ)*(E1[idx]*y1[idx] + E2[idx]*y2[idx]);
                ce_x += E1[idx]*x1[idx] + E2[idx]*x2[idx];
                ce_y += E1[idx]*y1[idx] + E2[idx]*y2[idx];
	}

        //KL.setCx(ce_x/klE);	
        //KL.setCy(ce_y/klE);
	
	// Reset core matrix D & E
	for(int i=1;i<=npi0s+2;i++) {
		for(int j=1;j<=npi0s*6;j++) 	D(i,j) = 0.;
		for(int j=1;j<=3;j++)  		E(i,j) = 0.;	
	}
	
	// Calculate core matrix D & E
	for( idx=0; idx<npi0s-1; idx++) {
	
	//d(idx+1) =   pow(E1[idx]  + E2[idx] ,2)
	//	     - pow(px1[idx] + px2[idx],2)
	//	     - pow(py1[idx] + py2[idx],2)
	//	     - pow(pz1[idx] + pz2[idx],2)
	//	     - MASS_PI0*MASS_PI0;	
		
	D(idx+1,idx*6+1) = -2.*(px1[idx] + px2[idx])*dpx1_dx1[idx] 
			   -2.*(py1[idx] + py2[idx])*dpy1_dx1[idx] 
			   -2.*(pz1[idx] + pz2[idx])*dpz1_dx1[idx]; // del x1
	D(idx+1,idx*6+2) = -2.*(px1[idx] + px2[idx])*dpx1_dy1[idx] 
			   -2.*(py1[idx] + py2[idx])*dpy1_dy1[idx] 
			   -2.*(pz1[idx] + pz2[idx])*dpz1_dy1[idx]; // del y1
	D(idx+1,idx*6+3) =  2.*(E1[idx]  + E2[idx]) 
			   -2.*(px1[idx] + px2[idx])*dpx1_dE1[idx] 
			   -2.*(py1[idx] + py2[idx])*dpy1_dE1[idx] 
			   -2.*(pz1[idx] + pz2[idx])*dpz1_dE1[idx]; // del E1
			   
	D(idx+1,idx*6+4) = -2.*(px1[idx] + px2[idx])*dpx2_dx2[idx] 
			   -2.*(py1[idx] + py2[idx])*dpy2_dx2[idx] 
			   -2.*(pz1[idx] + pz2[idx])*dpz2_dx2[idx]; // del x2
	D(idx+1,idx*6+5) = -2.*(px1[idx] + px2[idx])*dpx2_dy2[idx] 
			   -2.*(py1[idx] + py2[idx])*dpy2_dy2[idx] 
			   -2.*(pz1[idx] + pz2[idx])*dpz2_dy2[idx]; // del y2
	D(idx+1,idx*6+6) =  2.*(E1[idx]  + E2[idx]) 
			   -2.*(px1[idx] + px2[idx])*dpx2_dE2[idx] 
			   -2.*(py1[idx] + py2[idx])*dpy2_dE2[idx] 
			   -2.*(pz1[idx] + pz2[idx])*dpz2_dE2[idx]; // del E2		   
	
	E(idx+1,1)  = -2.*(px1[idx] + px2[idx])*(dpx1_dvx[idx] + dpx2_dvx[idx]) 
		      -2.*(py1[idx] + py2[idx])*(dpy1_dvx[idx] + dpy2_dvx[idx]) 
		      -2.*(pz1[idx] + pz2[idx])*(dpz1_dvx[idx] + dpz2_dvx[idx]); // del vx
	E(idx+1,2)  = -2.*(px1[idx] + px2[idx])*(dpx1_dvy[idx] + dpx2_dvy[idx]) 
		      -2.*(py1[idx] + py2[idx])*(dpy1_dvy[idx] + dpy2_dvy[idx]) 
		      -2.*(pz1[idx] + pz2[idx])*(dpz1_dvy[idx] + dpz2_dvy[idx]); // del vy
	E(idx+1,3)  = -2.*(px1[idx] + px2[idx])*(dpx1_dvz[idx] + dpx2_dvz[idx]) 
		      -2.*(py1[idx] + py2[idx])*(dpy1_dvz[idx] + dpy2_dvz[idx]) 
		      -2.*(pz1[idx] + pz2[idx])*(dpz1_dvz[idx] + dpz2_dvz[idx]); // del vz
	}	      
		      
	for( idx=0; idx<npi0s; idx++) {	      
	// d(npi0s+1) = klE*klE - klpx*klpx - klpy*klpy - klpz*klpz - MASS_KL*MASS_KL;	      
	
	D(npi0s  ,idx*6+1) = -2.* klpx * dpx1_dx1[idx] 
			     -2.* klpy * dpy1_dx1[idx] 
			     -2.* klpz * dpz1_dx1[idx]; // del x1
	D(npi0s  ,idx*6+2) = -2.* klpx * dpx1_dy1[idx] 
			     -2.* klpy * dpy1_dy1[idx] 
			     -2.* klpz * dpz1_dy1[idx]; // del y1
	D(npi0s  ,idx*6+3) =  2.* klE
			     -2.* klpx * dpx1_dE1[idx] 
			     -2.* klpy * dpy1_dE1[idx] 
			     -2.* klpz * dpz1_dE1[idx]; // del E1   
			   
	D(npi0s  ,idx*6+4) = -2.* klpx * dpx2_dx2[idx] 
			     -2.* klpy * dpy2_dx2[idx] 
			     -2.* klpz * dpz2_dx2[idx]; // del x2
	D(npi0s  ,idx*6+5) = -2.* klpx * dpx2_dy2[idx] 
			     -2.* klpy * dpy2_dy2[idx] 
			     -2.* klpz * dpz2_dy2[idx]; // del y2
	D(npi0s  ,idx*6+6) =  2.* klE
			     -2.* klpx * dpx2_dE2[idx] 
			     -2.* klpy * dpy2_dE2[idx] 
			     -2.* klpz * dpz2_dE2[idx]; // del E2   
	
	E(npi0s  ,1)  = -2.* klpx * dklpx_dvx 
		        -2.* klpy * dklpy_dvx 
		        -2.* klpz * dklpz_dvx; // del vx
	E(npi0s  ,2)  = -2.* klpx * dklpx_dvy 
		        -2.* klpy * dklpy_dvy 
		        -2.* klpz * dklpz_dvy; // del vy
	E(npi0s  ,3)  = -2.* klpx * dklpx_dvz 
		        -2.* klpy * dklpy_dvz 
		        -2.* klpz * dklpz_dvz; // del vz	   
			
	//	d(npi0s+2) += sum(E1[idx]*x1[idx] + E2[idx]*x2[idx]) - vx*klE;
	//	d(npi0s+3) += sum(E1[idx]*y1[idx] + E2[idx]*y2[idx]) - vy*klE;	
	
	D(npi0s+1,idx*6+1) = (vz-T1TargetZ)*E1[idx]; 		// del x1
	D(npi0s+1,idx*6+2) = 0.; 		// del y1
	D(npi0s+1,idx*6+3) = (vz-T1TargetZ)*x1[idx] - (CSIZPosition - T1TargetZ)*vx; 	// del E1   
			   
	D(npi0s+1,idx*6+4) = (vz-T1TargetZ)*E2[idx]; 		// del x2
	D(npi0s+1,idx*6+5) = 0.; 		// del y2
	D(npi0s+1,idx*6+6) = (vz-T1TargetZ)*x2[idx] - (CSIZPosition - T1TargetZ)*vx; 	// del E2   
	
	E(npi0s+1,1)  = -(CSIZPosition - T1TargetZ)*klE; 			// del vx
	E(npi0s+1,2)  = 0.; 			// del vy
	E(npi0s+1,3)  = ce_x; 			// del vz
	
	D(npi0s+2,idx*6+1) = 0.; 		// del x1
	D(npi0s+2,idx*6+2) = (vz-T1TargetZ)*E1[idx]; 		// del y1
	D(npi0s+2,idx*6+3) = (vz-T1TargetZ)*y1[idx] - (CSIZPosition - T1TargetZ)*vy; 	// del E1   
			   
	D(npi0s+2,idx*6+4) = 0.; 		// del x2
	D(npi0s+2,idx*6+5) = (vz-T1TargetZ)*E2[idx]; 		// del y2
	D(npi0s+2,idx*6+6) = (vz-T1TargetZ)*y2[idx] - (CSIZPosition - T1TargetZ)*vy; 	// del E2   
	
	E(npi0s+2,1)  = 0.; 			// del vx
	E(npi0s+2,2)  = -(CSIZPosition - T1TargetZ)*klE; 			// del vy
	E(npi0s+2,3)  = ce_y; 			// del vz	    
	
	}

	da0 = a0 - a;
	dv0 = v0 - v;
	
        TMatrixD Dt;
        Dt = D.T();
        TVectorD Lambda0t = Lambda0;
        //Lambda0t.T();
        TMatrixD Et = E;
        Et.T();

	VD = (D*Va0*Dt);
        VD.Invert();	
	VE = (Et*VD*E);
        VE.Invert();
	
        TVectorD tempd(npi0s+2);
        tempd = (D*da0);
        tempd += d; // (D*da0 + d)
     
	Lambda0 = (VD*tempd);
        TVectorD unit(2,1);
        TVectorD chisq_vec(1); 
	chisq_vec = (Lambda0t * (D*da0 + E*dv0 + d))*unit;
        chisq = chisq_vec(0);
        TVectorD subv; 
        subv = VE * Et * Lambda0;		
	v = v-subv; 	 
	
	if (chisq < chisq_keep_v && error_flag==0 && chisq>=0.) { 
		chisq_keep_v 	= chisq;
        	v_keep_v    	= v;
        	VE_keep_v    	= VE;
        	VD_keep_v    	= VD;
        	Lambda0_keep_v	= Lambda0;
        	E_keep_v      	= E;
        	D_keep_v      	= D;
		fit_updated	= 1;
	}else {
			chisq 	= chisq_keep_v;
        		v    	= v_keep_v;
        		VE    	= VE_keep_v;
        		VD    	= VD_keep_v;
        		Lambda0	= Lambda0_keep_v;
        		E      	= E_keep_v;
        		D      	= D_keep_v;
			break;
	}
	
	// fill back KL vertex first
	vx = v(1);
	vy = v(2);
	vz = v(3);
	
	} // end of KL vertex minizing loop
	TMatrixD Dt = D;
        Dt.T();
        TMatrixD Et = E;
        Et.T();
        TVectorD suba;
        suba = Va0 * Dt * Lambda;
	if (error_flag==0) {
		Lambda  = Lambda0 - VD * E * VE * Et * Lambda0;
		a = a0 - suba;
	}		
	
	if ((it == 0 || chisq < chisq_keep) && error_flag==0 && chisq>=0.) {
		chisq_keep 	= chisq;
        	v_keep    	= v;
        	VE_keep    	= VE;
        	VD_keep    	= VD;
        	Lambda0_keep	= Lambda0;
        	E_keep      	= E;
        	D_keep      	= D;
		a_keep 		= a;	
		fit_updated	= 1;	
	}else {
		chisq 	= chisq_keep;
        	v    	= v_keep;
        	VE    	= VE_keep;
        	VD    	= VD_keep;
        	Lambda0	= Lambda0_keep;
        	E      	= E_keep;
        	D      	= D_keep;
		a	= a_keep;
	        break;
	}
	
	// fill back other parameters
	for( idx=0; idx<npi0s; idx++) {
		x1[idx] = a(idx*6 + 1);
		y1[idx] = a(idx*6 + 2);
		E1[idx] = a(idx*6 + 3);
		x2[idx] = a(idx*6 + 4);
		y2[idx] = a(idx*6 + 5);
		E2[idx] = a(idx*6 + 6);
	}
	
	} // end of global minizing loop	
	
	if (fit_updated == 0) { // fit failed.
		fprintf(stderr,"[ERROR] : Fit failed (%d), return error code = %d.\n",ifail,error_flag);
		chisq = -1.;
		if (error_flag!=0) return (double)error_flag;	
		else return -3.;
	}	

        for( idx=0; idx<npi0s; idx++) {

                R1[idx] = sqrt(pow(x1[idx]-vx,2) + pow(y1[idx]-vy,2) + pow(z1[idx]-vz,2));
                R2[idx] = sqrt(pow(x2[idx]-vx,2) + pow(y2[idx]-vy,2) + pow(z2[idx]-vz,2));

                px1[idx] = (x1[idx]-vx)/R1[idx] * E1[idx];
                py1[idx] = (y1[idx]-vy)/R1[idx] * E1[idx];
                pz1[idx] = (z1[idx]-vz)/R1[idx] * E1[idx];

                px2[idx] = (x2[idx]-vx)/R2[idx] * E2[idx];
                py2[idx] = (y2[idx]-vy)/R2[idx] * E2[idx];
                pz2[idx] = (z2[idx]-vz)/R2[idx] * E2[idx];

                klpx += px1[idx] + px2[idx];
                klpy += py1[idx] + py2[idx];
                klpz += pz1[idx] + pz2[idx];
                klE  += E1[idx]  + E2[idx];
        }

	
	TMatrixD Dt = D;
	Dt.T();
	TMatrixD Et = E;
	Et.T();
	
	// Final error matrix
	TMatrixD VDtu = VD - VD * E * VE * Et * VD;
	TMatrixD Va = Va0 - Va0 * Dt * VDtu * D * Va0;
	
    	KL.setMass(sqrt(klE*klE-klpx*klpx-klpy*klpy-klpz*klpz));
    	KL.setEnergy(klE);
	KL.setP3(klpx,klpy,klpz);
    	KL.setVtx(vx,vy,vz);
        KL.setChisqZ(chisq);



      // npi0s =3 here, I had checked
	for( idx=0; idx<npi0s; idx++) {
		KL.pi0()[idx].setEnergy(E1[idx]+E2[idx]);
		KL.pi0()[idx].setMass(sqrt(pow(E1[idx]+E2[idx],2) -
				    	   pow(px1[idx]+px2[idx],2) -
					   pow(py1[idx]+py2[idx],2) -
					   pow(pz1[idx]+pz2[idx],2)));
		
		KL.pi0()[idx].setVtx(vx,vy,vz);
		KL.pi0()[idx].setP3(px1[idx]+px2[idx],py1[idx]+py2[idx],pz1[idx]+pz2[idx]);
		KL.pi0()[idx].setRecZ(vz);
		KL.pi0()[idx].setRecZsig2(VE(3,3));
		
		KL.pi0()[idx].g1().setEnergy(E1[idx]);
		KL.pi0()[idx].g1().setPos(x1[idx],y1[idx],z1[idx]);
		KL.pi0()[idx].g1().setP3(px1[idx],py1[idx],pz1[idx]);
		
		KL.pi0()[idx].g2().setEnergy(E2[idx]);
		KL.pi0()[idx].g2().setPos(x2[idx],y2[idx],z2[idx]);
		KL.pi0()[idx].g2().setP3(px2[idx],py2[idx],pz2[idx]);

//std::cout << KL.pi0()[idx].g1().p3().mag() <<std::endl;
//std::cout << KL.pi0()[idx].g2().p3().mag() <<std::endl;
         

//	        KL.pi0()[idx].setAngle(KL.pi0()[idx].g1().p3(),
//                                       KL.pi0()[idx].g2().p3());
        }
      
        return chisq;     	
}
*/



