#include <iostream>
#include <list>
#include "rec2g/Rec2g.h"
#include "pi0/Pi0.h"
#include "gnana/E14GNAnaFunction.h"
//#include "MTAnalysisLibrary/MTFunction.h"
#include "ShapeChi2/ShapeChi2New.h"

#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

bool RecKL2gamma(std::list<Gamma> const &glist, std::list<Pi0>& piList);
bool RecKL2gamma2(std::list<Gamma> const &glist, std::list<Pi0>& piList, const double CalorimatorZ);

bool user_rec(std::list<Gamma> const &glist, std::list<Pi0>& piList,int recoption, const double CalorimatorZ)
{
  
  bool isRec;
  if(recoption==0) isRec = RecKL2gamma(glist,piList);
  else if(recoption==1) isRec = RecKL2gamma2(glist,piList,CalorimatorZ);

  return isRec;
}

bool RecKL2gamma(std::list<Gamma> const &glist, std::list<Pi0>& piList){
  
  ///// reconstruction 
  static Rec2g rec2g;
  piList = rec2g.recPi0withConstM( glist, Rec2g::M_KL);
  if(piList.size()!=1)
    return false;
  
  ///// position correction for angle dependency
  E14GNAnaFunction::getFunction()->correctPosition( piList.front() );  
  // enrgy correction for angle&energy dependency
  E14GNAnaFunction::getFunction()->correctEnergyWithAngle( piList.front() );  
  
  ///// re-reconstruction with corrected gamma
  std::list<Gamma> glist2;
  glist2.push_back(piList.front().g1());
  glist2.push_back(piList.front().g2());

  /*
  for( std::list<Gamma>::iterator it=glist2.begin(); it!=glist2.end(); it++ ){
    it->setSigmaE( MTFUNC::GetCSIEnergyResolution( it->e() ) );
    double resX = MTFUNC::GetCSIPositionResolution( it->e() );
    it->setSigmaPos( resX, resX, 0 );
  }
  */
  
  piList = rec2g.recPi0withConstM( glist2, Rec2g::M_KL );
  if(piList.size()!=1)
    return false;
  
  ///// shape chi2 evaluation 
  //E14GNAnaFunction::getFunction()->shapeChi2( piList.front() );///// obsolete ShapeChi2 method  
  static ShapeChi2New chi2Newcalc;
  for( Int_t iGamma = 0 ; iGamma < 2 ; ++iGamma ){
    Gamma& gam= ( (iGamma==0) ? 
		  piList.front().g1() :
		  piList.front().g2() );
    chi2Newcalc.shapeChi2(gam);
  }
  
  return true;
}

bool RecKL2gamma2(std::list<Gamma> const &glist, std::list<Pi0>& piList, const double CalorimatorZ){

  //double TargetZ      = -21507.; //mm
  const double TargetZ  = MTBP::T1TargetZ;
  double Etot, avrX, avrY, avrZ, scaleFactor;

  ///// reconstruction 
  static Rec2g rec2g;
  piList = rec2g.recPi0withConstM( glist, Rec2g::M_KL);

  if(piList.size()!=1)
    return false;
  std::list<Pi0>::iterator pi0;

  pi0 = piList.begin();
  avrX = pi0->g1().e() * pi0->g1().x() + pi0->g2().e() * pi0->g2().x();
  avrY = pi0->g1().e() * pi0->g1().y() + pi0->g2().e() * pi0->g2().y();
  Etot = pi0->g1().e() + pi0->g2().e();
  avrX /= Etot; avrY /= Etot;
  avrZ = pi0->recZ();
  scaleFactor = (avrZ-TargetZ)/(CalorimatorZ-TargetZ);
  avrX *= scaleFactor; avrY *= scaleFactor;
//std::cout<<avrX<<"\t"<<avrY<<"\t"<<avrZ<<"\n"; getchar();
  pi0->setVtx( avrX, avrY, avrZ );
  pi0->updateVars();
  
  ///// position correction for angle dependency
  E14GNAnaFunction::getFunction()->correctPosition( piList.front() );  
  // enrgy correction for angle&energy dependency
  E14GNAnaFunction::getFunction()->correctEnergyWithAngle( piList.front() );  
  
  ///// re-reconstruction with corrected gamma
  std::list<Gamma> glist2;
  glist2.push_back(piList.front().g1());
  glist2.push_back(piList.front().g2());

  /*
  for( std::list<Gamma>::iterator it=glist2.begin(); it!=glist2.end(); it++ ){
    it->setSigmaE( MTFUNC::GetCSIEnergyResolution( it->e() ) );
    double resX = MTFUNC::GetCSIPositionResolution( it->e() );
    it->setSigmaPos( resX, resX, 0 );
  }
  */
  
  piList = rec2g.recPi0withConstM( glist2, Rec2g::M_KL );
  if(piList.size()!=1)
    return false;

  pi0 = piList.begin();
  avrX = pi0->g1().e() * pi0->g1().x() + pi0->g2().e() * pi0->g2().x();
  avrY = pi0->g1().e() * pi0->g1().y() + pi0->g2().e() * pi0->g2().y();
  Etot = pi0->g1().e() + pi0->g2().e();
  avrX /= Etot; avrY /= Etot;
  avrZ = pi0->recZ();
  scaleFactor = (avrZ-TargetZ)/(CalorimatorZ-TargetZ);
  avrX *= scaleFactor; avrY *= scaleFactor;
//std::cout<<avrX<<"\t"<<avrY<<"\t"<<avrZ<<"\n"; getchar();
  pi0->setVtx( avrX, avrY, avrZ );
  pi0->updateVars();

  ///// shape chi2 evaluation 
  //E14GNAnaFunction::getFunction()->shapeChi2( piList.front() );///// obsolete ShapeChi2 method  
  static ShapeChi2New chi2Newcalc;
  for( Int_t iGamma = 0 ; iGamma < 2 ; ++iGamma ){
    Gamma& gam= ( (iGamma==0) ? 
		  piList.front().g1() :
		  piList.front().g2() );
    chi2Newcalc.shapeChi2(gam);
  }
  
  return true;
}
