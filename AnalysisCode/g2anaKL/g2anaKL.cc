#include <numeric>
#include <vector>
#include "TFile.h"
#include "TChain.h"
#include "TMath.h"
#include "gnana/E14GNAnaFunction.h"
#include "gnana/E14GNAnaDataContainer.h"
#include "gamma/GammaFinder.h"
#include "rec2g/Rec2g.h"
#include "CLHEP/Vector/ThreeVector.h"

#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "MTAnalysisLibrary/MTPositionHandler.h"
#include "MTAnalysisLibrary/MTFunction.h"
#include "MTAnalysisLibrary/MTVetoFBAR.h"
#include "MTAnalysisLibrary/MTVetoNCC.h"
#include "MTAnalysisLibrary/MTVetoCBAR.h"
#include "MTAnalysisLibrary/MTVetoBCV.h"
#include "MTAnalysisLibrary/MTVetoCV.h"
#include "MTAnalysisLibrary/MTVetoOEV.h"
#include "MTAnalysisLibrary/MTVetoCC03.h"
#include "MTAnalysisLibrary/MTVetoCSI.h"
#include "MTAnalysisLibrary/MTVetoLCV.h"
#include "MTAnalysisLibrary/MTVetoCC04.h"
#include "MTAnalysisLibrary/MTVetoCC05.h"
#include "MTAnalysisLibrary/MTVetoCC06.h"
#include "MTAnalysisLibrary/MTVetoBHCV.h"
#include "MTAnalysisLibrary/MTVetoBHPV.h"
#include "MTAnalysisLibrary/MTVetoBPCV.h"
#include "MTAnalysisLibrary/MTVetoBHGC.h"
#include "MTAnalysisLibrary/MTVetonewBHCV.h"
#include "MTAnalysisLibrary/MTVetoIB.h"
#include "MTAnalysisLibrary/MTVetoIBCV.h"
#include "MTAnalysisLibrary/MTVetoMBCV.h"
#include "MTAnalysisLibrary/MTVetoDCV.h"
#include "MTAnalysisLibrary/MTVetoUCV.h"
#include "MTAnalysisLibrary/MTVetoUCVLG.h"
#include "MTAnalysisLibrary/MTVetonewBHCV.h"
#include "MTAnalysisLibrary/MTVetoDetector.h"
#include "MTAnalysisLibrary/MTGenParticle.h"

#include "SshinoSuppConfInfoManager/SuppConfInfoManager.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
//#include "E14ProdInfo/E14ProdInfoWriter.h"
#include "LcFTT/LcFTTAnaDataHandler.h"
#include <UTCconfig/UTCconfig.h>

bool user_rec(std::list<Gamma> const &glist, std::list<Pi0>& piList, int recoption, const double CalorimeterZ);
void user_cut(E14GNAnaDataContainer &data,std::list<Pi0> const &piList);

/*
// my cut set (2013.6.6)
enum{ DELTA_VERTEX_TIME=0, DELTA_KL_MASS,   MIN_HALFET,    KL_PT,
      KL_CHISQZ,           DELTA_PI0_MASS,  DELTA_PI0_Z,   MIN_GAMMAE,
      MAX_GAMMAE,          MIN_FIDUCIAL_XY, MAX_FIDUCIALR, MIN_CLUSTER_DISTANCE,
      KL_Z,                KL_BEAMEXIT_XY,  MAX_SHAPECHISQ };
*/
// my cut set (2015.8.20)
enum{ DELTA_VERTEX_TIME=0, DELTA_KL_MASS,   CsIEt, KL_PT, 
      KL_CHISQZ,           DELTA_PI0_MASS,  DELTA_PI0_Z,   MIN_GAMMAE,
      MAX_GAMMAE,          MIN_FIDUCIAL_XY, MAX_FIDUCIALR, MIN_CLUSTER_DISTANCE, 
      KL_Z,                KL_BEAMEXIT_XY,  MAX_SHAPECHISQ };


double BERTFactor[10] = { -0.9103, 0.0117108, -3.81494e-5, 6.60306e-8,
			  -6.86366e-11, 4.49989e-14, -1.87667e-17, 4.83159e-21,
			  -7.00298e-25, 4.37142e-29 }; // 100-3000MeV
double BERTFactor1[2] = { 0.438016, -1.23899e-5}; // > 3000 MeV
//double BICFactor[10] =  { -0.895313, 0.0116593, -3.81083e-5, 6.75463e-8, -7.2311e-11, 4.87878e-14, -2.08719e-17, 5.49004e-21, -8.09606e-25, 5.12139e-29}; // 100-3000MeV
double BICFactor[10] =  { -0.8963656316, 0.0116663175, -3.81083e-5, 6.75463e-8,
			  -7.2311e-11, 4.87878e-14, -2.08719e-17, 5.49004e-21,
			  -8.09606e-25, 5.12139e-29}; // 100-3000MeV
double BICFactor1[2] =  { 0.567662, -4.13216e-5}; // > 3000 MeV


double GetCorrectedE( double TotalE, double DepE )
{
  if( TotalE < 100 ){
    return DepE;
  }
  if( TotalE < DepE ){
    return DepE;
  }

  double BERTFraction = DepE / TotalE;
  double BERTMean = 0;
  double BICMean = 0;

  if( TotalE < 3000 ){
    for( int i=0; i<10; i++ ){
      BERTMean += BERTFactor[i] * pow( TotalE, i);
      BICMean  += BICFactor[i] * pow( TotalE, i);
    }
  }else{
    for( int i=0; i<2; i++ ){
      BERTMean += BERTFactor1[i] * pow( TotalE, i);
      BICMean  += BICFactor1[i] * pow( TotalE, i);
    }
  }
  //  std::cout << TotalE << " " << BERTMean << " " << BICMean << std::endl;
  double alpha = BERTMean - BICMean;
  alpha /= BERTMean;
  alpha /= (1-BERTMean);

  double BICFraction = alpha * pow( BERTFraction, 2) + (1-alpha)*BERTFraction;
  return BICFraction * TotalE;
}

double GetEventStartTime( std::list<Pi0> &piList, double* vertexTime, const double CSIZPosition )
{
  double gammaHitTime[2] = {0.0};
  double gammaHitX[2]    = {0.0};
  double gammaHitY[2]    = {0.0};
  double gammaE[2]       = {0.0};
  double gammaTWeight[2]  = {0.0};

  for( int iclus=0; iclus<2; iclus++ ){
    Gamma const &gamma = (iclus==0) ? piList.front().g1() : piList.front().g2();
    gammaHitTime[ iclus ] = gamma.t();
    gammaHitX[ iclus ]    = gamma.pos().x();
    gammaHitY[ iclus ]    = gamma.pos().y();
    gammaE[ iclus ]       = gamma.e();
    gammaTWeight[ iclus ] = pow( MTFUNC::TResolutionOfCluster( gamma.e() ), -2 );
  }
  double vertexZ = piList.front().recZ();

  for( int igamma=0; igamma<2; igamma++ ){
    vertexTime[igamma] = gammaHitTime[igamma] - sqrt( pow( CSIZPosition-vertexZ, 2) + pow( gammaHitX[igamma], 2) + pow( gammaHitY[igamma], 2) ) / (TMath::C()/1E6);
  }

  return MTFUNC::WeightedAverage( 2, vertexTime, gammaTWeight );
}


int g2anaKL( char* inputFile, char* outputFile, int userFlag, int recoption, int argc, char** argv );


int main(int argc,char** argv)
{  
  std::cout << "g2anaKL starts." << std::endl;

  // read argument  
  if( argc != 3 && argc!=4 && argc!=5  ){
    std::cout << "Argument Error ! :" << std::endl
	      << "\t usage : " << argv[0] << " InputFileName OutputFileName userFlag recoption" << std::endl
	      << "\t input file should be the output of clusteringFromDst."  << std::endl;
    std::cout << "If there is no userFlag, default userFlag (20151101) will be set." << std::endl;
    return -1;
  }
  int UserFlag=20151101;
  if( argc==4 || argc==5 )UserFlag = atoi(argv[3]);
  std::cout<<"userFlag: "<<UserFlag<<std::endl;
  if( UserFlag<20150101 || 21120903<UserFlag ){
    std::cout<<"Unexpected UserFlag is used! The UserFlag format is yyyymmdd. Used userFlag: "<<UserFlag<<std::endl;
    return -1;
  }

  int recoption=0;
  if( argc==5 ) recoption= atoi(argv[4]);
  std::cout<<"recoption: "<<recoption<<std::endl;
  if ( recoption!=0 && recoption!=1 ){
    std::cout<<"Unexpected recoption is used!"<<std::endl;
    std::cout<<"recoption=0: Default rec. method. recoption=1: Yuting's rec. method."<<std::endl;
    std::cout<<"Used recoption is "<<recoption<<std::endl;
    return -1;
  }
  

  int result = g2anaKL( argv[1], argv[2], UserFlag, recoption, argc, argv );
  std::cout << "g2anaKL finished." << std::endl;
  std::cout << std::endl;
  return result;
}

int g2anaKL( char* inputFile, char* outputFile, int userFlag, int recoption, int argc, char** argv )
{

  bool isRealData = true; // Run:true Sim:false

  // set input file
  TChain *inputTree = new TChain( "clusteringTree" );
  inputTree->Add( inputFile );
  const long nEntries = inputTree->GetEntries();
  if( nEntries==0 ){
    std::cout << "This file has no events." << std::endl;
    return -1;
  }

  // E14BasicParam
  E14BasicParamManager::E14BasicParamManager *m_E14BP
    = new E14BasicParamManager::E14BasicParamManager();
  m_E14BP->SetDetBasicParam(userFlag,"CSI");
  const double CSIZPosition    = m_E14BP->GetDetZPosition();

  NsUTC::UTCconfig* utcconf;
  
  //CSIEt
  Double_t CSIEt = 0.0;
  Double_t CSIHalfEtR = 0.0;
  Double_t CSIHalfEtL = 0.0;
  inputTree->SetBranchAddress( "CSIEt", &CSIEt );
  inputTree->SetBranchAddress( "CSIHalfEtR", &CSIHalfEtR );
  inputTree->SetBranchAddress( "CSIHalfEtL", &CSIHalfEtL );

  // Run information
  Int_t RunID = 0;
  Int_t NodeID = 0;
  Int_t FileID = 0;
  Int_t DstEntryID = 0;
  Int_t ClusteringEntryID = 0;// additional branch

  Short_t SpillID = 0;
  Int_t   EventID = 0;
  Int_t   TimeStamp;
  UInt_t  L2TimeStamp;
  Short_t Error;
  Short_t TrigTagMismatch;
  Short_t L2AR;
  Short_t COE_Et_overflow;
  Short_t COE_L2_override;
  Int_t   COE_Esum;
  Int_t   COE_Ex;
  Int_t   COE_Ey;
  Bool_t  isGoodRun;
  Bool_t  isGoodSpill;
  Int_t   DetectorWFMCorruptBit;
  UInt_t  DetectorBit,ScaledTrigBit,RawTrigBit;
  Int_t   CDTNum;
  Short_t CDTData[3][64];
  Short_t CDTBit[38][38];
  Short_t CDTVetoData[4][64];
  Short_t CDTVetoBitSum[13];

  
  inputTree->SetBranchAddress( "RunID",      &RunID );
  inputTree->SetBranchAddress( "NodeID",     &NodeID );
  inputTree->SetBranchAddress( "FileID",     &FileID );
  inputTree->SetBranchAddress( "DstEntryID", &DstEntryID );

  if(inputTree->GetBranch("DetectorBit")){
    inputTree->SetBranchAddress("SpillID",        &SpillID );
    inputTree->SetBranchAddress("EventID",        &EventID );
    inputTree->SetBranchAddress("TimeStamp",      &TimeStamp);
    inputTree->SetBranchAddress("L2TimeStamp",    &L2TimeStamp);
    inputTree->SetBranchAddress("Error",          &Error);
    inputTree->SetBranchAddress("TrigTagMismatch",&TrigTagMismatch);
    inputTree->SetBranchAddress("L2AR",           &L2AR);
    inputTree->SetBranchAddress("COE_Et_overflow",&COE_Et_overflow);
    inputTree->SetBranchAddress("COE_L2_override",&COE_L2_override);
    inputTree->SetBranchAddress("COE_Esum",       &COE_Esum);
    inputTree->SetBranchAddress("COE_Ex",         &COE_Ex);
    inputTree->SetBranchAddress("COE_Ey",         &COE_Ey);
    inputTree->SetBranchAddress("DetectorBit",    &DetectorBit);
    inputTree->SetBranchAddress("ScaledTrigBit",  &ScaledTrigBit);
    inputTree->SetBranchAddress("RawTrigBit",     &RawTrigBit);
    inputTree->SetBranchAddress("isGoodRun",      &isGoodRun);
    inputTree->SetBranchAddress("isGoodSpill",    &isGoodSpill);
    if(inputTree->GetBranch("CDTVetoData")){
      inputTree->SetBranchAddress("CDTVetoData",  CDTVetoData);    
      inputTree->SetBranchAddress("CDTVetoBitSum",CDTVetoBitSum);    
    }
	
	if(inputTree->GetBranch("DetectorWFMCorruptBit")){
	  inputTree->SetBranchAddress("DetectorWFMCorruptBit",  &DetectorWFMCorruptBit);
	}
  }
  if(inputTree->GetBranch("CDTNum")){
     inputTree->SetBranchAddress("CDTNum",       &CDTNum);
     if( inputTree->GetBranch("CDTData")){
        inputTree->SetBranchAddress("CDTData",      CDTData);
        inputTree->SetBranchAddress("CDTBit",       CDTBit);
     }
  } 

 
  // check sim or data
  Int_t    TruePID = 0; // mother particle ID
  Int_t    TrueDecayMode = 0;
  Int_t    GsimGenEventID = 0;
  Int_t    GsimEntryID = 0;
  Int_t    MCEventID = 0;//obsolete
  Double_t TrueVertexZ;
  Double_t TrueVertexTime;
  Double_t TrueMom[3];
  Double_t TruePos[3];
  MTGenParticle* genP = new MTGenParticle();
  std::string *AccidentalFileName = new std::string;
  Int_t        AccidentalFileID;
  Long64_t     AccidentalEntryID;
  Bool_t       AccidentalOverlayFlag;
  if( inputTree->GetBranch( "TruePID" ) ){
    isRealData = false;
    inputTree->SetBranchAddress("TruePID",       &TruePID );
    inputTree->SetBranchAddress("TrueDecayMode", &TrueDecayMode );
    inputTree->SetBranchAddress("TrueVertexZ",   &TrueVertexZ );
    inputTree->SetBranchAddress("TrueVertexTime",&TrueVertexTime );
    inputTree->SetBranchAddress("TrueMom",        TrueMom );
    inputTree->SetBranchAddress("TruePos",        TruePos );
    inputTree->SetBranchAddress("GsimGenEventID",    &GsimGenEventID );
    inputTree->SetBranchAddress("GsimEntryID",       &GsimEntryID );
    inputTree->SetBranchAddress("MCEventID",         &MCEventID );
    inputTree->SetBranchAddress("AccidentalFileName",   &AccidentalFileName );
    inputTree->SetBranchAddress("AccidentalFileID",     &AccidentalFileID );
    inputTree->SetBranchAddress("AccidentalEntryID",    &AccidentalEntryID );
    inputTree->SetBranchAddress("AccidentalOverlayFlag",&AccidentalOverlayFlag );
    genP->SetBranchAddress( inputTree );
	utcconf = NsUTC::UTCconfig::GetInstance(NsUTC::RUN81);
  }else{
	inputTree->GetEntry(0);
	utcconf = NsUTC::UTCconfig::GetInstance(RunID);
  }


  E14GNAnaDataContainer data;
  data.setBranchAddress( inputTree );

  SshinoLib::SuppConfInfoManager *SuppConfInfoMan = NULL; 
  SuppConfInfoMan = new SshinoLib::SuppConfInfoManager( inputFile );

  // set output file
  TFile *otf = new TFile( outputFile, "RECREATE" );
  
  // add version information of E14 Analysis Suite
//  E14ProdLibrary::E14ProdInfoWriter::GetInstance()->Write(inputTree->GetFile(), otf, argc, argv);
  
  // add peakfind and zero-suppression information
  SuppConfInfoMan -> WriteSuppConfInfo( otf );

  TTree *otr = new TTree( "RecTree", "output from g2anaKL");  

  otr->Branch( "RunID",             &RunID,             "RunID/I" );
  otr->Branch( "NodeID",            &NodeID,            "NodeID/I" );
  otr->Branch( "FileID",            &FileID,            "FileID/I" );
  otr->Branch( "DstEntryID",        &DstEntryID,        "DstEntryID/I" );
  otr->Branch( "ClusteringEntryID", &ClusteringEntryID, "ClusteringEntryID/I" ); 
  otr->Branch( "userFlag",          &userFlag,          "userFlag/I");
  otr->Branch( "recoption",         &recoption,         "recoption/I");
  
  if(isRealData){
    otr->Branch("SpillID",        &SpillID,        "SpillID/S" );
    otr->Branch("EventID",        &EventID,        "EventID/I" );
    otr->Branch("DetectorBit",    &DetectorBit,    "DetectorBit/i");
    otr->Branch("ScaledTrigBit",  &ScaledTrigBit,  "ScaledTrigBit/i");  
    otr->Branch("RawTrigBit",     &RawTrigBit,     "RawTrigBit/i");
    otr->Branch("TimeStamp",      &TimeStamp,      "TimeStamp/I");
    otr->Branch("L2TimeStamp",    &L2TimeStamp,    "L2TimeStamp/i");
    otr->Branch("Error",          &Error,          "Error/S");
    otr->Branch("TrigTagMismatch",&TrigTagMismatch,"TrigTagMismatch/S");
    otr->Branch("L2AR",           &L2AR,           "L2AR/S");
    otr->Branch("COE_Et_overflow",&COE_Et_overflow,"COE_Et_overflow/S");
    otr->Branch("COE_L2_override",&COE_L2_override,"COE_L2_override/S");
    otr->Branch("COE_Esum",       &COE_Esum,       "COE_Esum/I");
    otr->Branch("COE_Ex",         &COE_Ex,         "COE_Ex/I");
    otr->Branch("COE_Ey",         &COE_Ey,         "COE_Ey/I");
    otr->Branch("isGoodRun",      &isGoodRun,      "isGoodRun/O");
    otr->Branch("isGoodSpill",    &isGoodSpill,    "isGoodSpill/O");
    if(inputTree->GetBranch("CDTVetoData")){
      otr->Branch("CDTVetoData",  CDTVetoData,     "CDTVetoData[4][64]/S");
      otr->Branch("CDTVetoBitSum",CDTVetoBitSum,   "CDTVetoBitSum[13]/S");
    }
    if(inputTree->GetBranch("DetectorWFMCorruptBit")){
		otr->Branch("DetectorWFMCorruptBit", &DetectorWFMCorruptBit,  "DetectorWFMCorruptBit/I");
	}
  }
  
  if(inputTree->GetBranch("CDTNum")){
      otr->Branch("CDTNum",       &CDTNum,         "CDTNum/I");
      if(inputTree->GetBranch("CDTData")){
         otr->Branch("CDTData",      CDTData,         "CDTData[3][64]/S");
         otr->Branch("CDTBit",       CDTBit,          "CDTBit[38][38]/S");
     }
  }
 
  data.branchOfPi0List( otr );
  
  // UTC branches
  float GammaUTCEne[120];
  float GammaUTCTime[120];
  float GammaUTCWeightedTime[120];
 
  float GammaUTCCorrectedTime[120];
  float GammaUTCCorrectedWeightedTime[120];

  if(userFlag>=20190101){  
  	otr->Branch("GammaUTCEne", &GammaUTCEne, "GammaUTCEne[120]");
  	otr->Branch("GammaUTCTime", &GammaUTCTime, "GammaUTCTime[120]");
  	otr->Branch("GammaUTCWeightedTime", &GammaUTCWeightedTime, "GammaUTCWeightedTime[120]");
  }

  
  Double_t EventStartTime = 0.0;
  Double_t EventStartZ    = 0.0;
  Double_t VertexTime[2]  = {0.0};
  // Original cluster information
  Int_t      OriginalClusterNumber  = 0;
  Double_t  *OriginalClusterE       = new Double_t [256];
  Double_t  *OriginalClusterT       = new Double_t [256];
  Double_t (*OriginalClusterPos)[3] = new Double_t [256][3];
  Double_t  *OriginalClusterVertexT = new Double_t [256];
  
  const int NclsMAX = 120;
  const int NUTCmodMAX = 20;
  int ClusterUTCModNumber[NclsMAX];
  int ClusterUTCModID[NclsMAX][NUTCmodMAX];
  float ClusterUTCModTime[NclsMAX][NUTCmodMAX];
  float ClusterUTCModEne[NclsMAX][NUTCmodMAX];

  for(int i=0;i<NclsMAX;i++){
	  ClusterUTCModNumber[i] = 0;
	  for(int j=0;j<NUTCmodMAX;j++){
		  ClusterUTCModID[i][j] = 0;
		  ClusterUTCModTime[i][j] = 0;
		  ClusterUTCModEne[i][j] = 0;
	  }
  }



  Int_t    UTCNumber;
  Int_t    UTCModID[256];
  Float_t  UTCEne[256];
  Float_t  UTCTime[256];
  Short_t  UTCPSBit[256];

  if(userFlag>=20190101){  
  	inputTree->SetBranchAddress("UTCNumber",&UTCNumber);
  	inputTree->SetBranchAddress("UTCModID",  UTCModID);
  	inputTree->SetBranchAddress("UTCEne",    UTCEne);
  	inputTree->SetBranchAddress("UTCTime",   UTCTime);
  	inputTree->SetBranchAddress("UTCPSBit",  UTCPSBit);
  	
  	otr->Branch("UTCNumber",&UTCNumber,"UTCNumber/I");
  	otr->Branch("UTCModID",  UTCModID, "UTCModID[UTCNumber]/I");
  	otr->Branch("UTCEne",    UTCEne,   "UTCEne[UTCNumber]/F");
  	otr->Branch("UTCTime",   UTCTime,  "UTCTime[UTCNumber]/F");
  	otr->Branch("UTCPSBit",   UTCPSBit,  "UTCPSBit[UTCNumber]/S");

  	inputTree->SetBranchAddress("ClusterUTCModNumber", ClusterUTCModNumber);
  	inputTree->SetBranchAddress("ClusterUTCModID", ClusterUTCModID);
  	inputTree->SetBranchAddress("ClusterUTCModTime", ClusterUTCModTime);
  	inputTree->SetBranchAddress("ClusterUTCModEne", ClusterUTCModEne);

  	otr->Branch("OriginalClusterUTCModNumber", ClusterUTCModNumber, "OriginalClusterUTCModNumber[120]/I");
  	otr->Branch("OriginalClusterUTCModID", ClusterUTCModID, "OriginalClusterUTCModID[120][20]/I");
  	otr->Branch("OriginalClusterUTCModTime", ClusterUTCModTime, "OriginalClusterUTCModTime[120][20]" );
  	otr->Branch("OriginalClusterUTCModEne", ClusterUTCModEne, "OriginalClusterUTCModEne[120][20]" );
  }

  
  
  
  // My cut set and veto set
  Int_t MyCutCondition = 0;
  Int_t MyVetoCondition = 0;
  // Cut parameters
  Double_t DeltaVertexTime = 0;
  Double_t TotalEt = 0;
  Double_t MinHalfEt = 0;
  Double_t MinGammaE = 0;
  Double_t MaxGammaE = 0;
  Double_t MaxFiducialR = 0;
  Double_t ClusterDistance = 0;
  Double_t MinFiducialXY = 0;
  Double_t MaxShapeChisq = 0;
  Double_t ERatio = 0;
  Double_t EAsymmetry = 0;
  Double_t ProjectionAngle = 0;
  Double_t MinETheta = 0;
  Double_t CSICoEX = 0;
  Double_t CSICoEY = 0;
  Double_t InterpolatedDistance = 0; // The distance btw. origin and the line connecting 2 clusters
  Double_t IntersectedSquare = 0; // The vertex of minimum square which intersects the line connecting 2 clusters
  Double_t ExtraClusterDeltaVertexTime = 0;
  Double_t ExtraClusterEne = 0;
  Double_t AverageClusterTime = 0;
  otr->Branch( "CSIEt", &CSIEt, "CSIEt/D" );
  otr->Branch( "CSIHalfEtR", &CSIHalfEtR, "CSIHalfEtR/D" );
  otr->Branch( "CSIHalfEtL", &CSIHalfEtL, "CSIHalfEtL/D" );
  otr->Branch( "EventStartTime", &EventStartTime, "EventStartTime/D");
  otr->Branch( "EventStartZ", &EventStartZ, "EventStartZ/D");
  otr->Branch( "VertexTime", VertexTime, "VertexTime[2]/D");
  otr->Branch( "OriginalClusterNumber", &OriginalClusterNumber, "OriginalClusterNumber/I" );
  otr->Branch( "OriginalClusterE", OriginalClusterE, "OriginalClusterE[OriginalClusterNumber]/D" );
  otr->Branch( "OriginalClusterT", OriginalClusterT, "OriginalClusterT[OriginalClusterNumber]/D" );
  otr->Branch( "OriginalClusterVertexT", OriginalClusterVertexT, "OriginalClusterVertexT[OriginalClusterNumber]/D" );
  otr->Branch( "MyCutCondition", &MyCutCondition, "MyCutCondition/I" );
  otr->Branch( "MyVetoCondition", &MyVetoCondition, "MyVetoCondition/I" );
  otr->Branch( "DeltaVertexTime", &DeltaVertexTime, "DeltaVertexTime/D" );
  otr->Branch( "TotalEt", &TotalEt, "TotalEt/D");
  otr->Branch( "MinHalfEt", &MinHalfEt, "MinHalfEt/D" );
  otr->Branch( "MinGammaE", &MinGammaE, "MinGammaE/D");
  otr->Branch( "MaxGammaE", &MaxGammaE, "MaxGammaE/D");
  otr->Branch( "MaxFiducialR", &MaxFiducialR, "MaxFiducialR/D");
  otr->Branch( "MinFiducialXY", &MinFiducialXY, "MinFiducialXY/D");
  otr->Branch( "ClusterDistance", &ClusterDistance, "ClusterDistance/D");
  otr->Branch( "MaxShapeChisq", &MaxShapeChisq, "MaxShapeChisq/D" );
  otr->Branch( "ERatio", &ERatio, "ERatio/D");
  otr->Branch( "EAsymmetry", &EAsymmetry, "EAsymmetry/D");
  otr->Branch( "ProjectionAngle", &ProjectionAngle, "ProjectionAngle/D");
  otr->Branch( "MinETheta", &MinETheta, "MinETheta/D");
  otr->Branch( "CSICoEX", &CSICoEX, "CSICoEX/D");
  otr->Branch( "CSICoEY", &CSICoEY, "CSICoEY/D");
  otr->Branch( "InterpolatedDistance", &InterpolatedDistance, "InterpolatedDistance/D" );
  otr->Branch( "IntersectedSquare", &IntersectedSquare, "IntersectedSquare/D");
  otr->Branch( "ExtraClusterDeltaVertexTime", &ExtraClusterDeltaVertexTime, "ExtraClusterDeltaVertexTime/D" );
  otr->Branch( "ExtraClusterEne", &ExtraClusterEne, "ExtraClusterEne/D" );
  otr->Branch( "AverageClusterTime", &AverageClusterTime, "AverageClusterTime/D" );
  if( !isRealData ){
    otr->Branch( "TruePID",          &TruePID,           "TruePID/I" );
    otr->Branch( "TrueDecayMode",    &TrueDecayMode,     "TrueDecayMode/I" );
    otr->Branch( "TrueVertexZ",      &TrueVertexZ,       "TrueVertexZ/D" );
    otr->Branch( "TrueVertexTime",   &TrueVertexTime,    "TrueVertexTime/D" );
    otr->Branch( "TrueMom",           TrueMom,           "TrueMom[3]/D" );
    otr->Branch( "TruePos",           TruePos,           "TruePos[3]/D" );
    otr->Branch( "GsimGenEventID",   &GsimGenEventID,    "GsimGenEventID/I" );
    otr->Branch( "GsimEntryID",      &GsimEntryID,       "GsimEntryID/I" );
    otr->Branch( "MCEventID",        &MCEventID,         "MCEventID/I" );
    otr->Branch( "AccidentalFileName",   "std::string",          &AccidentalFileName );
    otr->Branch( "AccidentalFileID",     &AccidentalFileID,      "AccidentalFileID/I" );
    otr->Branch( "AccidentalEntryID",    &AccidentalEntryID,     "AccidentalEntryID/L" );
    otr->Branch( "AccidentalOverlayFlag",&AccidentalOverlayFlag, "AccidentalOverlayFlag/O" );
    genP->Branch( otr );
  }

  GammaFinder gFinder;
  MTPositionHandler* pHandler = MTPositionHandler::GetInstance();

  // Veto detectors
  Bool_t  DetectorExistFlag[MTBP::nDetectors];
  MTVeto* vetoHandler[MTBP::nDetectors];
  vetoHandler[MTBP::FBAR] = new MTVetoFBAR( userFlag );
  vetoHandler[MTBP::NCC]  = new MTVetoNCC( userFlag );
  vetoHandler[MTBP::CBAR] = new MTVetoCBAR( userFlag );
  vetoHandler[MTBP::BCV]  = new MTVetoBCV( userFlag );
  vetoHandler[MTBP::CV]   = new MTVetoCV( userFlag );
  vetoHandler[MTBP::LCV]  = new MTVetoLCV( userFlag );
  vetoHandler[MTBP::CC03] = new MTVetoCC03( userFlag );
  vetoHandler[MTBP::OEV]  = new MTVetoOEV( userFlag );
  vetoHandler[MTBP::CSI]  = new MTVetoCSI( userFlag );
  vetoHandler[MTBP::CC04] = new MTVetoCC04( userFlag );
  vetoHandler[MTBP::CC05] = new MTVetoCC05( userFlag );
  vetoHandler[MTBP::CC06] = new MTVetoCC06( userFlag );
  vetoHandler[MTBP::BPCV] = new MTVetoBPCV( userFlag );
  vetoHandler[MTBP::IBCV] = new MTVetoIBCV( userFlag );
  vetoHandler[MTBP::MBCV] = new MTVetoMBCV( userFlag );
  vetoHandler[MTBP::DCV]  = new MTVetoDCV( userFlag);
  vetoHandler[MTBP::UCVLG]= new MTVetoUCVLG(userFlag);

  MTBHVeto* bhvetoHandler[6];
  bhvetoHandler[0] = new MTVetoBHCV( userFlag );
  bhvetoHandler[1] = new MTVetoBHPV( userFlag );
  bhvetoHandler[2] = new MTVetonewBHCV( userFlag );
  bhvetoHandler[3] = new MTVetoBHGC( userFlag );
  bhvetoHandler[4] = new MTVetoIB( userFlag );
  bhvetoHandler[5] = new MTVetoUCV(userFlag);

  /// FTT
  std::vector<LcLib::LcFTTAnaDataHandler*> fttDataVec;

  for( int idet=0; idet<MTBP::nDetectors; idet++ ){
    ///// Check the existence of the detector.
    ///// If it doesn't exist, DetectorHandler of the detector is not generated.
    if( inputTree->GetBranch(Form("%sNumber",MTBP::detectorName[idet].c_str())) 
	|| inputTree->GetBranch(Form("%sModuleNumber",MTBP::detectorName[idet].c_str())) ){
      DetectorExistFlag[idet] = true;
      std::cout << "Veto Detector : " << MTBP::detectorName[idet].c_str() << " exists." << std::endl;
    }else{
      DetectorExistFlag[idet] = false;
      std::cout<< "Veto Detector : " << MTBP::detectorName[idet].c_str() <<" doesn't exist." << std::endl;
      continue;
    }

    switch( idet ){
    case MTBP::FBAR:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      if( 20180101<=userFlag && userFlag<20180301 ) vetoHandler[idet]->BranchOriginalData( otr );              
      break;
    case MTBP::NCC:
    case MTBP::BCV:
    case MTBP::LCV:
    case MTBP::CSI:
    case MTBP::OEV:
    case MTBP::CC03:      
    case MTBP::CC04:
    case MTBP::CC05:
    case MTBP::CC06:
    case MTBP::BPCV:
    case MTBP::IBCV:
    case MTBP::MBCV:
	case MTBP::DCV:
	case MTBP::UCVLG:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      break;
    case MTBP::CV:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      vetoHandler[idet]->BranchOriginalData( otr );              
      break;
    case MTBP::CBAR:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      vetoHandler[idet]->BranchOriginalData( otr );              
      break;
    case MTBP::BHCV:
      bhvetoHandler[0]->SetBranchAddress( inputTree );
      bhvetoHandler[0]->Branch( otr );
      break;        
    case MTBP::BHPV:
      bhvetoHandler[1]->SetBranchAddress( inputTree );
      bhvetoHandler[1]->Branch( otr );
      break;
    case MTBP::newBHCV:
      bhvetoHandler[2]->SetBranchAddress( inputTree );
      bhvetoHandler[2]->Branch( otr );
      break;                      
    case MTBP::BHGC:
      bhvetoHandler[3]->SetBranchAddress( inputTree );
      bhvetoHandler[3]->Branch( otr );
      break;                            
    case MTBP::IB:
      bhvetoHandler[4]->SetBranchAddress( inputTree );
      bhvetoHandler[4]->Branch( otr );
      break;                      
	case MTBP::UCV:
      bhvetoHandler[5]->SetBranchAddress( inputTree );
      bhvetoHandler[5]->Branch( otr );
	  break;
    default:
      break;
    }

    /// FTT
    if( inputTree->GetBranch(Form("%sFTTChisq", MTBP::detectorName[idet].c_str() )) ){
       LcLib::LcFTTAnaDataHandler *ftt_data =
                            new LcLib::LcFTTAnaDataHandler( MTBP::detectorName[idet] );
       ftt_data->SetBranchAddresses( inputTree );
       ftt_data->AddBranches( otr );
       fttDataVec.push_back( ftt_data );
    }

  }
  
  //veto info. and MC timning correction
  vetoHandler[MTBP::CSI]->SetDataContainer( &data );
  
  for( Int_t iDet=0 ; iDet<MTBP::nDetectors ; iDet++ ){
    if( !DetectorExistFlag[iDet] )
      continue;

    if( !MTBP::is500MHzDetector[iDet] && MTBP::VetoWidth[iDet]>0 ){
      vetoHandler[iDet]->SetVetoThreshold( MTBP::VetoEneThre[iDet] );
      vetoHandler[iDet]->SetVetoWindow( MTBP::VetoWidth[iDet], MTBP::VetoTiming[iDet]  );
    }
  }
  bhvetoHandler[0]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::BHCV] );
  bhvetoHandler[0]->SetVetoWindow( MTBP::VetoWidth[MTBP::BHCV], MTBP::VetoTiming[MTBP::BHCV] );
  bhvetoHandler[1]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::BHPV] );

  bhvetoHandler[1]->SetVetoWindow( MTBP::VetoWidth[MTBP::BHPV], MTBP::VetoTiming[MTBP::BHPV] );

  bhvetoHandler[2]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::newBHCV] );
  bhvetoHandler[2]->SetVetoWindow( MTBP::VetoWidth[MTBP::newBHCV], MTBP::VetoTiming[MTBP::newBHCV] );

  bhvetoHandler[3]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::BHGC] );
  bhvetoHandler[3]->SetVetoWindow( MTBP::VetoWidth[MTBP::BHGC], MTBP::VetoTiming[MTBP::BHGC] );

  bhvetoHandler[4]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::IB] );
  bhvetoHandler[4]->SetVetoWindow( MTBP::VetoWidth[MTBP::IB], MTBP::VetoTiming[MTBP::IB] );

  bhvetoHandler[5]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::UCV] );
  bhvetoHandler[5]->SetVetoWindow( MTBP::VetoWidth[MTBP::UCV], MTBP::VetoTiming[MTBP::UCV] );
  
  // loop analysis
  for( int entry=0; entry<nEntries; entry++ ){
    if( nEntries>100 )
      if( entry%(nEntries/10)==0 )
	std::cout << entry/(nEntries/100) << "%" << std::endl;
    
    
    // Initialize
    MyCutCondition = 0;
    MyVetoCondition = 0;
    OriginalClusterNumber = 0;
    DeltaVertexTime = 0;
    MinHalfEt = 0;
    MinGammaE = 9999;
    MaxGammaE = 0;
    MaxFiducialR = 0;
    MinFiducialXY = 9999;
    ClusterDistance = 0;
    MaxShapeChisq = 0;
    ERatio = 0;
    EAsymmetry = 0;
    ProjectionAngle = 0;
    MinETheta = 0;
    CSICoEX = 0;
    CSICoEX = 0;
    InterpolatedDistance = 9999;
    IntersectedSquare = 9999;
    ExtraClusterDeltaVertexTime = 9999;
    ExtraClusterEne = 0;
    AverageClusterTime = -9999.;
    TotalEt=0;

    // read data
    inputTree->GetEntry( entry );

    ClusteringEntryID = entry;

    // Online trigger
    MinHalfEt = std::min( CSIHalfEtR, CSIHalfEtL );
    if( MinHalfEt < 250. ){
      //continue;
    }


    std::list<Cluster> clist;
    data.getData(clist);

    // Store original clusters information
    for( std::list<Cluster>::iterator it=clist.begin(); it!=clist.end(); it++ ){
      OriginalClusterE[OriginalClusterNumber] = it->e();
      OriginalClusterT[OriginalClusterNumber] = it->t();
      OriginalClusterVertexT[OriginalClusterNumber] = it->t();
      OriginalClusterPos[OriginalClusterNumber][0] = it->x();
      OriginalClusterPos[OriginalClusterNumber][1] = it->y();
      OriginalClusterPos[OriginalClusterNumber][2] = it->z();
      
      OriginalClusterNumber++;
    }
    bool* clusterUsageFlag = new bool[OriginalClusterNumber]();
    for( int icluster=0; icluster<OriginalClusterNumber; icluster++ ){
      clusterUsageFlag[icluster] = false;
    }
    
    // charged pion correction
    //if( true ){
    if( false ){      
      // charged pion search
      int cpiNumber = 0;
      double cpiPos[2][2] = {{0}};
      double cpiTotalE[2] = {0};
      for( int ip=0; ip<genP->GenParticleNumber; ip++ ){
	if( cpiNumber == 2 ) break;
	if( fabs( genP->GenParticlePid[ip]) != 211 ) continue;
	if( genP->GenParticleMother[ip] != 1 ) continue;
	if( genP->GenParticleEndPos[ip][2]<CSIZPosition || genP->GenParticleEndPos[ip][2]>CSIZPosition+MTBP::CSILength ) continue;
	if( sqrt( pow( genP->GenParticleEndPos[ip][0], 2) + pow( genP->GenParticleEndPos[ip][1], 2) ) > 900 ) continue;
	if( genP->GenParticleEndPos[ip][0]<120 && genP->GenParticleEndPos[ip][1]<120 ) continue;
	
	cpiPos[cpiNumber][0] = genP->GenParticleEndPos[ip][0];
	cpiPos[cpiNumber][1] = genP->GenParticleEndPos[ip][1];
	cpiTotalE[cpiNumber] = genP->GenParticleEk[ip] + MTBP::CPi_MASS;
	cpiNumber++;
      }

      // Energy correction for charged pion clusters ( Nearer than 100mm)
      for( int ip=0; ip<cpiNumber; ip++ ){
	for( std::list<Cluster>::iterator it=clist.begin(); it!=clist.end(); it++ ){
	  if( MTFUNC::Distance( cpiPos[ip][0], it->x(), cpiPos[ip][1], it->y(), 0, 0) < 200 ){

	    double originalEne = it->e();
	    double factor = GetCorrectedE( cpiTotalE[ip], originalEne ) / originalEne;
	    if( factor != 1 ){
	      // Half Et correction
	      std::vector<int> clusterIdVec = it->clusterIdVec();
	      std::vector<double> clusterEVec = it->clusterEVec();
	      std::vector<double> newClusterEVec;

	      for( int icsi=0; icsi<clusterIdVec.size(); icsi++ ){
		newClusterEVec.push_back( clusterEVec[icsi] * factor );
		
		if( pHandler->GetCSIXPosition( clusterIdVec[icsi] ) > 0 ){
		  CSIHalfEtR += clusterEVec[icsi] * (factor-1);
		}else{
		  CSIHalfEtL += clusterEVec[icsi] * (factor-1);
		}
	      }
	      
	      originalEne *= factor;
	      it->setEnergy( originalEne );
	      it->setClusterEVec( newClusterEVec );
	      //std::cout << " " << it->e() << std::endl;
	    }
	    break;
	  }
	}
      }
    }
	    
    // gamma finding
    std::list<Gamma> glist;
    gFinder.findGamma( clist, glist);


    // check number of gammas
    if( glist.size() < 2 ){
      data.KlongNumber   = 0;
      data.Pi0Number     = 0;
      data.GammaNumber   = 0;
      data.GamClusNumber = 0;
      //otr->Fill();
      data.eventID++;
      continue; 
    }


    // If there are more than 2 clusters, I select most near time 2 clusters
    std::vector< double > clusterTimeVec;
    for( std::list<Gamma>::iterator it=glist.begin(); it!=glist.end(); it++ ){
      clusterTimeVec.push_back( it->t() );
    }

    while( glist.size() != 2 ){
      std::sort( clusterTimeVec.begin(), clusterTimeVec.end() );
      while( clusterTimeVec.size() != 2 ){
	double averageTime = std::accumulate( clusterTimeVec.begin(), clusterTimeVec.end(), 0.0);
	averageTime /= clusterTimeVec.size();
	if( averageTime-clusterTimeVec[0] > clusterTimeVec[clusterTimeVec.size()-1]-averageTime ){
	  clusterTimeVec.erase( clusterTimeVec.begin() );
	}else{
	  clusterTimeVec.pop_back();
	}
      }

      for( std::list<Gamma>::iterator it=glist.begin(); it!=glist.end(); ){
	bool rejectFlag = true;
	for( unsigned int index=0; index<clusterTimeVec.size(); index++ ){
	  if( it->t() == clusterTimeVec[index] ){
	    rejectFlag = false;
	    break;
	  }
	}
	if( rejectFlag ){
	  it = glist.erase( it );
	  continue;
	}
	it++;
      }
    }


    std::list<Pi0> piList; 
    if( !user_rec( glist, piList, recoption,CSIZPosition ) ){
      data.Pi0Number     = 0;
      data.GammaNumber   = 0;
      data.GamClusNumber = 0;
      //otr->Fill();
      data.eventID++;
      continue;
    }

    {
      Pi0 const& pi0 = piList.front();
      Gamma const &g1 = pi0.g1();
      Gamma const &g2 = pi0.g2();
      clusterUsageFlag[g1.id()] = true;
      clusterUsageFlag[g2.id()] = true;
    }
    
    // Vertex timing reconstruction
    EventStartTime = GetEventStartTime( piList, VertexTime, CSIZPosition );
    EventStartZ = piList.front().recZ();


    bool Only2gFlag = true;
    for( int icluster=0; icluster<OriginalClusterNumber; icluster++ ){
      OriginalClusterVertexT[icluster] = OriginalClusterT[icluster] - sqrt( pow( CSIZPosition-EventStartZ, 2) + pow( OriginalClusterPos[icluster][0], 2) + pow( OriginalClusterPos[icluster][1], 2) ) / (TMath::C()/1E6);

      if( clusterUsageFlag[icluster] ) continue; // This cluster is included in KL
      if(fabs(OriginalClusterVertexT[icluster]-EventStartTime)<10 && OriginalClusterE[icluster]>20){
	Only2gFlag=false;
	//std::cout<<OriginalClusterVertexT[icluster]-EventStartTime<<" "<<OriginalClusterE[icluster]<<" "<<OriginalClusterNumber<<std::endl;
      }
    }

    if(!Only2gFlag){
      data.eventID++;
      continue;
    }

    //std::cout<<entry<<std::endl;
    
    // cuts
    user_cut( data, piList );
    
    // my cut condition
    MinHalfEt = std::min( CSIHalfEtR, CSIHalfEtL );
    double clusterPos[2][3]; // 3rd parameter is the distance from (0, 0)
    double eThetas[2] = {0};
    AverageClusterTime = 0;

    for( int igamma=0; igamma<2; igamma++ ){
      if( DeltaVertexTime < fabs( EventStartTime-VertexTime[igamma] ) ){
	DeltaVertexTime = fabs( EventStartTime-VertexTime[igamma] );
      }

      Gamma const &g = (igamma==0) ? piList.front().g1() : piList.front().g2();
      if( MinGammaE > g.e() ) MinGammaE = g.e();
      if( MaxGammaE < g.e() ) MaxGammaE = g.e();
      if( MaxFiducialR < g.pos().perp() )           MaxFiducialR = g.pos().perp();
      if( MinFiducialXY > std::max( fabs(g.x()), fabs(g.y())) ) MinFiducialXY = std::max( fabs(g.x()), fabs(g.y()) );
      if( MaxShapeChisq < g.chisq() ) MaxShapeChisq = g.chisq();
      clusterPos[igamma][0] = g.x();
      clusterPos[igamma][1] = g.y();
      AverageClusterTime += g.t();
      clusterPos[igamma][2] = g.pos().perp();

      eThetas[igamma] = g.e() * atan( clusterPos[igamma][2] / (g.z() - piList.front().recZ() ) ) * 180. / TMath::Pi();

      CSICoEX += g.e() * g.x();
      CSICoEY += g.e() * g.y();
      TotalEt+=g.e();
    }

    ClusterDistance = sqrt( pow( clusterPos[0][0]-clusterPos[1][0], 2) + pow( clusterPos[0][1]-clusterPos[1][1], 2) );

    AverageClusterTime /= 2;

    ProjectionAngle = pow( clusterPos[0][2], 2) + pow( clusterPos[1][2], 2) - pow( ClusterDistance, 2);
    ProjectionAngle /= (2 * clusterPos[0][2] * clusterPos[1][2] );
    ProjectionAngle = acos( ProjectionAngle ) * 180. / TMath::Pi();

    InterpolatedDistance = fabs( clusterPos[1][0]*clusterPos[0][1] - clusterPos[0][0]*clusterPos[1][1] );
    InterpolatedDistance /= ClusterDistance;

    IntersectedSquare = std::min( fabs( (clusterPos[1][0]*clusterPos[0][1]-clusterPos[0][0]*clusterPos[1][1])/(clusterPos[1][1]-clusterPos[0][1]+clusterPos[1][0]-clusterPos[0][0]) ),
				  fabs( (clusterPos[1][0]*clusterPos[0][1]-clusterPos[0][0]*clusterPos[1][1])/(clusterPos[1][1]-clusterPos[0][1]-clusterPos[1][0]+clusterPos[0][0]) ) );

    ERatio = MinGammaE / MaxGammaE;
    EAsymmetry = (MaxGammaE-MinGammaE) / (MaxGammaE+MinGammaE);

    MinETheta = std::min( eThetas[0], eThetas[1] );

    CSICoEX /= (MinGammaE + MaxGammaE);
    CSICoEY /= (MinGammaE + MaxGammaE);
    

    if( true ){
      if( DeltaVertexTime > 3. ){
	MyCutCondition |= (1<<DELTA_VERTEX_TIME);
      }
      if( TotalEt < 650. ){
	MyCutCondition |= (1<<CsIEt);
      }
      if( piList.front().p3().perp() > 50. ){
	MyCutCondition |= (1<<KL_PT);
      }
      if( MinGammaE < 50. ){
	MyCutCondition |= (1<<MIN_GAMMAE);
      }
      if( MinFiducialXY < 150. ){
	MyCutCondition |= (1<<MIN_FIDUCIAL_XY);
      }
      if( 850. < MaxFiducialR ){
	MyCutCondition |= (1<<MAX_FIDUCIALR);
      }
      if( ClusterDistance < 150. ){
	MyCutCondition |= (1<<MIN_CLUSTER_DISTANCE);
      }
      if( EventStartZ<3000 || 5000<EventStartZ ){
	MyCutCondition |= (1<<KL_Z);
      }
      if( MaxShapeChisq > 7. ){
	MyCutCondition |= (1<<MAX_SHAPECHISQ);
      }      
    }



    if( OriginalClusterNumber > 2 ){
      for( int icluster=0; icluster<OriginalClusterNumber; icluster++ ){
	if( clusterUsageFlag[icluster] ) continue; // This cluster is included in KL
	if( fabs( ExtraClusterDeltaVertexTime ) > fabs( OriginalClusterVertexT[icluster] - EventStartTime ) ){
	  ExtraClusterDeltaVertexTime = OriginalClusterVertexT[icluster] - EventStartTime;
	  ExtraClusterEne = OriginalClusterE[icluster];
	  
	}
      }
    }

    // veto detector check
    Pi0 pi0 = piList.front();
    vetoHandler[MTBP::CSI]->SetPi0Info( &pi0 );
    
    for( int idet=0; idet<MTBP::nDetectors; idet++ ){
      if( !DetectorExistFlag[idet] )
	continue;
      switch( idet ){
      case MTBP::FBAR:
      case MTBP::OEV:
      case MTBP::CBAR:
      case MTBP::CC03:
      case MTBP::CC04:
      case MTBP::CC05:
      case MTBP::CC06:
      case MTBP::BCV:
      case MTBP::LCV:
      case MTBP::NCC:
      case MTBP::CV:	
      case MTBP::DCV:
      case MTBP::UCVLG:
	vetoHandler[idet]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( vetoHandler[idet]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;
      case MTBP::IBCV:	
      case MTBP::MBCV:	
      case MTBP::BPCV:
	vetoHandler[idet]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( vetoHandler[idet]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;	
      case MTBP::CSI:
	vetoHandler[idet]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( vetoHandler[idet]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}	
	break;	


      case MTBP::BHCV:
	bhvetoHandler[0]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[0]->Process( 2 ) ){
	  if(userFlag<20160101)MyVetoCondition |= (1<<idet);
	}
	break;	
      case MTBP::BHPV:
	bhvetoHandler[1]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[1]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;

      case MTBP::newBHCV:
	bhvetoHandler[2]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[2]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;

      case MTBP::BHGC:
	bhvetoHandler[3]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[3]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;

      case MTBP::IB:
	bhvetoHandler[4]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[4]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;
	  case MTBP::UCV:
	bhvetoHandler[5]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[5]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;

      default:
	break;
      }
    }

    if( fabs(ExtraClusterDeltaVertexTime) < 10. ){
      MyVetoCondition |= (1<<(MTBP::nDetectors));
    }

    
    
    data.setData( piList );


  
	if(userFlag>=20190101){  

		for(int j=0;j<120;j++){
		  GammaUTCEne[j] = 0;
		  GammaUTCTime[j] = 0;
		  GammaUTCWeightedTime[j] = 0;
		  GammaUTCCorrectedTime[j] = 0;
		  GammaUTCCorrectedWeightedTime[j] = 0;
   		}
		
		int isbadUTCevent = 0;
		for(int j=0;j<data.GammaNumber;j++){

			int id = data.GammaId[j];

			double UTCEne = 0;
			double UTCTime = ClusterUTCModTime[id][0];
			double UTCWeightedTime = 0;

			for(int k=0;k<ClusterUTCModNumber[id];k++){
			
				int modid = ClusterUTCModID[id][k];
				if(utcconf->CheckHVcondition(modid)<0){
				   	isbadUTCevent = 1;
					break;
				}

				double e = ClusterUTCModEne[id][k];
				// use only 20 MeV clusters if more than two modules exist.
				if(e>20.0 || k==0){
					UTCEne += e;
					UTCWeightedTime += ClusterUTCModTime[id][k]*e;
				}
			}
		
			UTCWeightedTime /= UTCEne;
 
			GammaUTCEne[j] = UTCEne;
			GammaUTCTime[j] = UTCTime;
  			GammaUTCWeightedTime[j] = UTCWeightedTime;

		}

		if(isbadUTCevent) continue;

	}



    otr->Fill();
    data.eventID++;
    
    delete[] clusterUsageFlag;

  }

  otf->cd();
  otr->Write();
  otf->Close();

  return 0;

}

