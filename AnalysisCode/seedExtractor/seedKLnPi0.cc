#include <iostream>
#include <list>

#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TVector3.h"
#include "TMath.h"

#include "GsimData/GsimGenParticleData.h"
#include "GsimData/GsimTrackData.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "LcHEPEvt/LcHEPEvtParticle.h"
#include "LcHEPEvt/LcHEPEvtHandler.h"

int main( int argc, char** argv )
{
   // read arg
   if( argc!=4 && argc!=5 ){
      std::cout<<" arg err: (bin) (input) (output) (userflag)"
               <<" [optional: requirement of #gamma hits in CSI, 1 by default]" << std::endl;
      return 0;
   }

   const std::string ifname = argv[1];
   const std::string ofname = argv[2];
   const Int_t userflag = std::atoi( argv[3] );
   const Int_t min_ngamma =  (argc==5) ? std::atoi(argv[4]) : 1;

   std::cout<<" Corrected version: v0.04.00 (2023.02.19)" << std::endl;
   std::cout<<" input file: " << ifname << std::endl;
   std::cout<<" ouptut file: " << ofname << std::endl;
   std::cout<<" userflag: " << userflag << std::endl;
   std::cout<<" #gamma hits in CSI" << min_ngamma << std::endl;

   // prepare input
   TChain* itree = new TChain("eventTree00");   
   itree->Add( ifname.c_str() );
   const Long64_t nentry = itree->GetEntries();
   std::cout<<" #entry = " << nentry << std::endl;
   if( nentry==0 ) return 0;

   GsimGenParticleData* gp = new GsimGenParticleData();
   itree->SetBranchAddress("GenParticle.", &gp );

   // prepare output
   LcLib::LcHEPEvtHandler hepevt;
   TFile* ofile = new TFile( ofname.c_str(), "RECREATE");
   TTree* otree = hepevt.HEPEvtTree();
   hepevt.AddBranches( otree );

   E14BasicParamManager::E14BasicParamManager e14bp;
   e14bp.SetDetBasicParam( userflag, "CSI");
   const Double_t k_CsiZ = e14bp.GetDetZPosition();
   const Double_t k_MaxCsiHitR  = 900.;
   const Double_t k_MinCsiHitXY = 100.;
   const Double_t k_MinTotalE   = 400.;
   std::cout<<" CSI z = " << k_CsiZ << std::endl;

   // loop analysis
   for( Long64_t ientry=0; ientry<nentry; ++ientry )
   {
      if( nentry>100 && ientry%(nentry/10)==0 )
         std::cout<<" " << 10*ientry/(nentry/10) <<"%" << std::endl;

      itree->GetEntry(ientry);
      TClonesArray* gp_arr  = gp->briefTracks;
      Int_t ngp = gp_arr->GetEntries();

      std::list<LcLib::LcHEPEvtParticle> pi0list;
      std::list<LcLib::LcHEPEvtParticle> fplist;

      // loop over all particles of this event and classify them.
      for( Int_t igp=0; igp<ngp; ++igp )
      {
         GsimTrackData* gtrack = dynamic_cast<GsimTrackData*>(gp_arr->At(igp) );
         if( !gtrack ){
            std::cout<<"Error: null pointer is given from GsimTrackData*." << std::endl;
            return 0;
         }

         // first particle is always KL
         if( gtrack->track==1 ) continue; 

         LcLib::LcHEPEvtParticle particle( gtrack );

         if( particle.GetMother()==1 && particle.GetPid()==MTBP::pi0_PDGCode ){
            pi0list.push_back( particle );
         }else if( particle.GetMother()>1 ){
            fplist.push_back( particle );
         }
      }

      // remove all the final particles that are not from kaon's daughter pions.
      std::list<LcLib::LcHEPEvtParticle>::iterator ifp = fplist.begin();
      while( ifp != fplist.end() )
      {
         bool isFromPi0 = false;
         for(std::list<LcLib::LcHEPEvtParticle>::const_iterator ipi0 = pi0list.begin();
             ipi0 != pi0list.end(); ++ipi0  )
         {
            if( ipi0->GetTrack()==ifp->GetMother() ){
               isFromPi0 = true;
               break;
            }
         }

         if( isFromPi0 ) ifp++;
         else            ifp = fplist.erase( ifp );
      }

      // further analysis to keep this event or not.
      Double_t esum = 0;
      Int_t    ngamma = 0;
      for(std::list<LcLib::LcHEPEvtParticle>::const_iterator ifp = fplist.begin(); 
          ifp != fplist.end(); ++ifp )
      {
         if( ifp->GetPid()!=MTBP::gamma_PDGCode ) continue;
         TVector3 const& vtx = ifp->GetVertex();
         TVector3 const& p3  = ifp->GetP3(); 

         const Double_t delz = k_CsiZ - vtx.z();
         if( delz<0 ) break;        // decay behind CSI
         if( p3.z()<1.e-3 ) continue;  // fly backward or theta = ~90 degreee.

         const Double_t scale = delz / p3.z();
         const Double_t csi_x = vtx.x() + p3.x() * scale;
         const Double_t csi_y = vtx.y() + p3.y() * scale;
         const Double_t csi_r = TMath::Hypot( csi_x, csi_y );
        
         if( TMath::Abs(csi_x)<k_MinCsiHitXY && TMath::Abs(csi_y)<k_MinCsiHitXY ) continue;
         if( csi_r>k_MaxCsiHitR ) continue;

         ngamma++;
         esum += ifp->GetEnergy();
      }

      if( ngamma>=min_ngamma && esum>k_MinTotalE ){
         hepevt.Init();
         for(std::list<LcLib::LcHEPEvtParticle>::const_iterator ifp = fplist.begin();
             ifp != fplist.end(); ++ifp )
         {
            hepevt.AddParticle( *ifp );
         }
         otree->Fill();
      }

   } // end of entry loop

   // end of analysis   
   otree->Write();
   ofile->Close();

   return 1;
}
