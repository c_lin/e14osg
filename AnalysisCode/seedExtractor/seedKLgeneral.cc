/*
 *  seedKLgeneral.cc
 *  2023.03.23 Chieh Lin
 *
 *  This is a general code to generate any KL decays by HEPEvt file.
 *  All the daughter particles of a KL are collected and saved as an event.
 *  Moreover, if a KL decay Z < 0  or > CSI Z, this event is discarded.
 *
 */

#include <iostream>
#include <list>

#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TVector3.h"
#include "TMath.h"

#include "GsimData/GsimGenParticleData.h"
#include "GsimData/GsimTrackData.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "LcHEPEvt/LcHEPEvtParticle.h"
#include "LcHEPEvt/LcHEPEvtHandler.h"

int main( int argc, char** argv )
{
   // read arg
   if( argc!=4 ){
      std::cout<<" arg err: (bin) (input) (output) (userflag)" << std::endl;
      return 0;
   }

   const std::string ifname = argv[1];
   const std::string ofname = argv[2];
   const Int_t userflag = std::atoi( argv[3] );

   std::cout<<" Version: v0.04.01 (2023.03.22)" << std::endl;
   std::cout<<" input file: " << ifname << std::endl;
   std::cout<<" ouptut file: " << ofname << std::endl;
   std::cout<<" userflag: " << userflag << std::endl;

   // prepare input
   TChain* itree = new TChain("eventTree00");   
   itree->Add( ifname.c_str() );
   const Long64_t nentry = itree->GetEntries();
   std::cout<<" #entry = " << nentry << std::endl;
   if( nentry==0 ) return 0;

   GsimGenParticleData* gp = new GsimGenParticleData();
   itree->SetBranchAddress("GenParticle.", &gp );

   // prepare output
   LcLib::LcHEPEvtHandler hepevt;
   TFile* ofile = new TFile( ofname.c_str(), "RECREATE");
   TTree* otree = hepevt.HEPEvtTree();
   hepevt.AddBranches( otree );

   E14BasicParamManager::E14BasicParamManager e14bp;
   e14bp.SetDetBasicParam( userflag, "CSI");
   const Double_t k_CsiZ = e14bp.GetDetZPosition();
   std::cout<<" CSI z = " << k_CsiZ << std::endl;

   // loop analysis
   for( Long64_t ientry=0; ientry<nentry; ++ientry )
   {
      if( nentry>100 && ientry%(nentry/10)==0 )
         std::cout<<" " << 10*ientry/(nentry/10) <<"%" << std::endl;

      itree->GetEntry(ientry);
      TClonesArray* gp_arr  = gp->briefTracks;
      Int_t ngp = gp_arr->GetEntries();

      std::list<LcLib::LcHEPEvtParticle> dplist;

      Double_t decay_z = -9999.;
      bool is_bad_decay = false;

      // loop over all particles of this event and classify them.
      for( Int_t igp=0; igp<ngp; ++igp )
      {
         GsimTrackData* gtrack = dynamic_cast<GsimTrackData*>(gp_arr->At(igp) );
         if( !gtrack ){
            std::cout<<"Error: null pointer is given from GsimTrackData*." << std::endl;
            return 0;
         }

         // first particle is always KL
         if( gtrack->track==1 ){
            decay_z = gtrack->end_v.z();

            if( decay_z < 0. || decay_z > k_CsiZ ){
               is_bad_decay = true;
               break;
            }else{
               continue;
            }
         }

         if( gtrack->mother==1 ){
            LcLib::LcHEPEvtParticle particle( gtrack );
            dplist.push_back( particle );
         }
        
      }

      if( is_bad_decay ) continue;

      // convert into HEPEvt tree
      hepevt.Init();
      for(std::list<LcLib::LcHEPEvtParticle>::const_iterator ifp = dplist.begin();
             ifp != dplist.end(); ++ifp )
      {
         hepevt.AddParticle( *ifp );
      }

      otree->Fill(); 

   } // end of entry loop

   // end of analysis   
   otree->Write();
   ofile->Close();

   return 1;
}
