#ifndef LCCSIVETOHANDLER_H
#define LCCSIVETOHANDLER_H

#include "TTree.h"

#include "LcDetPar/LcDetParCsiEneSuppThreshold.h"
#include "LcRecData/LcRecData.h"

class LcCsiVetoHandler
{
 public:
   /// constructor ///
   LcCsiVetoHandler( const Int_t userflag = 20180601 );

   /// destructor ///
   ~LcCsiVetoHandler();

   /// method
   void        Reset();
   bool        AddBranches( TTree *tr ); 
   Double_t    GetSuppEneThreshold( const Int_t modId ) const;
   static bool EvalCsiVeto( const Double_t dist, 
                            const Double_t ene, 
                            const Double_t ene_thr = 3. ); 
   void UpdateVars( const LcLib::LcRecData *data );

   /// getter ///
   Bool_t IsFitSigmaCsiVeto()      const { return m_isFitSigmaCsiVeto; }
   Bool_t IsTightFitSigmaCsiVeto() const { return m_isTightFitSigmaCsiVeto; }

 private:
   LcLib::LcDetParCsiEneSuppThreshold m_esupp;

   Bool_t m_isFitSigmaCsiVeto;
   Bool_t m_isTightFitSigmaCsiVeto;
   Int_t  m_minDistModId;
   Int_t  m_maxEneModId;

};

#endif /* LcCsiVetoHandler.h guard */
