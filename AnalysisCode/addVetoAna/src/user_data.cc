#include <string>

#include "TObject.h"
#include "TTree.h"

#include "LcAna/LcAnaVeto.h"
#include "LcAna/LcAnaVetoFBAR.h"
#include "LcAna/LcAnaVetoNCC.h"
#include "LcAna/LcAnaVetoCBAR.h"
#include "LcAna/LcAnaVetoIB.h"
#include "LcAna/LcAnaVetoCV.h"
#include "LcAna/LcAnaVetoCC06.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

void user_data( TTree* itree, std::vector<LcLib::LcAnaVeto*> &vetoVec, const Int_t userflag )
{
   /// inactivate the branch before CloneTree if already exists ///
   const UInt_t ndet = 6;
   const std::string DetName[ndet]  = {"FBAR","NCC","CBAR","IB","CV","CC06"};
   const Double_t FTTChisqThr[ndet] = { 20.  , 40. , 27.  , 47., 12., 47.  };   
   for( UInt_t idet=0; idet<ndet; ++idet )
   {
      if( itree->GetBranch( Form("%sProperTime",DetName[idet].c_str() ) ) ){
         itree->SetBranchStatus( Form("%sProper*"       ,DetName[idet].c_str() ), 0);
         itree->SetBranchStatus( Form("%sMaskingProper*",DetName[idet].c_str() ), 0);
      }
   }

   /// construct objects ///
   E14BasicParamManager::E14BasicParamManager e14bp;
   for( UInt_t idet=0; idet<ndet; ++idet )
   {
      LcLib::LcAnaVeto* veto = NULL;
      if( DetName[idet]=="FBAR" ){
         veto = new LcLib::LcAnaVetoFBAR; 
      }else if( DetName[idet]=="NCC" ){
         veto = new LcLib::LcAnaVetoNCC;
      }else if( DetName[idet]=="CBAR" ){
         veto = new LcLib::LcAnaVetoCBAR;
      }else if( DetName[idet]=="IB" ){
         veto = new LcLib::LcAnaVetoIB;
      }else if( DetName[idet]=="CV" ){
         veto = new LcLib::LcAnaVetoCV;
      }else if( DetName[idet]=="CC06" ){
         veto = new LcLib::LcAnaVetoCC06;
      }

      /// FTT threshold
      veto->SetFTTChisqThreshold(FTTChisqThr[idet]);

      /// dead channel
      e14bp.SetDetBasicParam(userflag,DetName[idet]);
      std::vector<Int_t> deadIdVec = e14bp.GetDeadModuleIDVec();
      veto->SetDeadModuleVec(deadIdVec);

      vetoVec.push_back(veto);
   }
   
}
