#include <vector>

#include "TObject.h"
#include "TMath.h"

#include "LcRecData/LcRecData.h"
#include "addVetoAna/LcCsiVetoHandler.h"
#include "LcAna/LcAnaVeto.h"

UInt_t user_cut( const LcLib::LcRecData *data,
                 const LcCsiVetoHandler *csiVeto,
                 const std::vector<LcLib::LcAnaVeto*> &vetoVec )
{
   /// prepare container for convention ///
   LcLib::LcAnaVeto *FBAR = NULL;
   LcLib::LcAnaVeto *NCC  = NULL;
   LcLib::LcAnaVeto *CBAR = NULL;
   LcLib::LcAnaVeto *IB   = NULL;
   LcLib::LcAnaVeto *CV   = NULL;

   for(std::vector<LcLib::LcAnaVeto*>::const_iterator it=vetoVec.begin(); it!=vetoVec.end(); ++it )
   {
      if     ( (*it)->GetDetectorName()=="FBAR" ) FBAR = *it;
      else if( (*it)->GetDetectorName()=="NCC"  ) NCC  = *it;
      else if( (*it)->GetDetectorName()=="CBAR" ) CBAR = *it;
      else if( (*it)->GetDetectorName()=="IB"   ) IB   = *it;
      else if( (*it)->GetDetectorName()=="CV"   ) CV   = *it;
   }

   //////////////////////////////////////
   // start NewVetoCondition judgement //
   //////////////////////////////////////

   UInt_t cutval = 0;

   /// 0x1 : CC03
   if( data->CC03VetoEne>3. ) cutval += 0x1;

   /// 0x2 : CC04
   if( data->CC04E391VetoEne>3. || data->CC04KTeVVetoEne>3.) cutval += 0x2;

   /// 0x4 : CC05
   if( data->CC05E391VetoEne>3. ) cutval += 0x4;

   /// 0x8 : CC06
   if( data->CC06E391VetoEne>3. ) cutval += 0x8;

   /// 0x10 : FBAR
   if( FBAR->GetProperTime()>15.1 && FBAR->GetProperTime()<66. ) cutval += 0x10;

   /// 0x20 : NCC
   if( data->NCCVetoEne>1. ) cutval += 0x20;

   /// 0x40 : CBAR [30ns]
   if( CBAR->GetProperTime()>32.6 && CBAR->GetProperTime()<62.6 ) cutval += 0x40;

   /// 0x80 : IB [30ns]
   if( IB->GetProperTime()>-50.5 && IB->GetProperTime()<-20.5 ) cutval += 0x80;

   /// 0x100 : CV [20ns]
   if( CV->GetProperTime()>44.1 && CV->GetProperTime()<64.1 ) cutval += 0x100;

   /// 0x200 : OEV
   if( data->OEVVetoEne>1. ) cutval += 0x200; 

   /// 0x400 : CSI (isolated crystal), tight version
   if( csiVeto->IsTightFitSigmaCsiVeto() ) cutval += 0x400;

   /// 0x800 : CSI (extra cluster)
   if( TMath::Abs(data->ExtraClusterDeltaVertexTime)<=10. ) cutval += 0x800;

   /// 0x1000 : LCV
   if( data->LCVVetoEne>0.6 ) cutval += 0x1000;

   /// 0x2000 : BPCV
   if( data->BPCVVetoEne>1. ) cutval += 0x2000;

   /// 0x4000 : BHPV
   if( (data->MyVetoCondition&0x200)==0x200 ) cutval += 0x4000;

   /// 0x8000 : IBCV
   if( data->IBCVVetoEne>0.5 ) cutval += 0x8000;

   /// 0x10000 : MBCV
   if( data->MBCVVetoEne>0.5 ) cutval += 0x10000;

   /// 0x20000 : newBHCV
   if( (data->newBHCVModHitCount>1 && data->newBHCVVetoEne>884.e-6/4.) ) cutval += 0x20000;

   /// 0x40000 : BHGC
   if( data->BHGCVetoEne>2.5 ) cutval += 0x40000; 

   /// 0x80000 : CC04 scintillator
   if( data->CC04ScintiVetoEne>1. ) cutval += 0x80000;

   /// 0x100000 : CC05 scintillator
   if( data->CC05ScintiVetoEne>1. ) cutval += 0x100000;

   /// 0x200000 : CC06 scintillator
   if( data->CC06ScintiVetoEne>1. ) cutval += 0x200000;

   /// 0x400000 : HINEMOS
   if( data->NCCScintiVetoEne>1. ) cutval += 0x400000;

   /// 0x800000 : FBAR (FTT, wide window)
   if( FBAR->GetMaskingProperTime()>-50.4 && FBAR->GetMaskingProperTime()<120.4 ) cutval += 0x800000;

   /// 0x1000000 : NCC (FTT, wide window)
   if( NCC->GetMaskingProperTime()>-40.9 && NCC->GetMaskingProperTime()<59.1 ) cutval += 0x1000000;

   /// 0x2000000 : CV (FTT, wide window)
   if( CV->GetMaskingProperTime()>14.1 && CV->GetMaskingProperTime()<94.1 ) cutval += 0x2000000;

   return cutval;
}
