#include "addVetoAna/LcCsiVetoHandler.h"

LcCsiVetoHandler::LcCsiVetoHandler( const Int_t userflag )
   : m_esupp(userflag)
{
   Reset();
}

LcCsiVetoHandler::~LcCsiVetoHandler()
{
   ;
}

void LcCsiVetoHandler::Reset()
{
   m_isFitSigmaCsiVeto = false;
   m_isTightFitSigmaCsiVeto = false;
}

bool LcCsiVetoHandler::AddBranches( TTree *tr )
{
   if( tr==NULL ){
      Error("AddBranches","null pointer is given.");
      return false;
   } 

   tr->Branch("isFitSigmaCSIVeto"       , &m_isFitSigmaCsiVeto      ,"isFitSigmaCSIVeto/O"); 
   tr->Branch("isTightFitSigmaCSIVeto"  , &m_isTightFitSigmaCsiVeto ,"isTightFitSigmaCSIVeto/O"); 
   tr->Branch("TightCSIVetoMinDistModID", &m_minDistModId           ,"TightCSIVetoMinDistModID/I");
   tr->Branch("TightCSIVetoMaxEneModID" , &m_maxEneModId            ,"TightCSIVetoMaxEneModID/I");

   return true;
}

Double_t LcCsiVetoHandler::GetSuppEneThreshold( const Int_t modId ) const
{
   return m_esupp.GetEneSuppThreshold(modId);
}

bool LcCsiVetoHandler::EvalCsiVeto( const Double_t dist, 
                                    const Double_t ene, 
                                    const Double_t ene_thr )
{
   const Double_t dist_UL = 400 * (10 - ene_thr) / 7. + 200;

   if( dist < 200. ){
     //if( ene > 10. ) return true;
     if(ene>ene_thr && ene> 10. ) return true;
   }else if( dist > 200. && dist < dist_UL ){
     if( ene > 10. - (dist - 200. ) / 400. * 7 ) return true;
   }else{
     if( ene > ene_thr ) return true;
   }

   return false;
}

void LcCsiVetoHandler::UpdateVars( const LcLib::LcRecData *data )
{
   const Double_t k_MaxDeltaTime = 10.;

   m_isFitSigmaCsiVeto = false;
   m_isTightFitSigmaCsiVeto = false;

   m_minDistModId = -1;
   m_maxEneModId = -1;

   Double_t minDist = 950.;
   Double_t maxEne  = -1.;

   for( Int_t imod=0; imod<data->CSIVetoNumber; ++imod )
   {
      /// ignore delta time > 10 ns
      if( TMath::Abs(data->CSIVetoDeltaTime[imod]) > k_MaxDeltaTime ) continue;

      /// original fit sigma CSI veto
      const Double_t supp_thr = GetSuppEneThreshold(data->CSIVetoModID[imod]);
      const Double_t veto_thr = (supp_thr < 3. ) ? 3. : supp_thr;

      if( EvalCsiVeto( data->CSIVetoDistance[imod], 
                       data->CSIVetoEne[imod],
                       veto_thr                     ) ) m_isFitSigmaCsiVeto = true;

      if( EvalCsiVeto( data->CSIVetoDistance[imod],
                       data->CSIVetoEne[imod],
                       supp_thr                     ) )
      {
         m_isTightFitSigmaCsiVeto = true;

         /// min distance ///
         if( data->CSIVetoDistance[imod] < minDist ){
            minDist = data->CSIVetoDistance[imod];
            m_minDistModId = data->CSIVetoModID[imod];
         }

         /// max energy ///
         if( data->CSIVetoEne[imod] > maxEne ){
            maxEne = data->CSIVetoDistance[imod];
            m_maxEneModId = data->CSIVetoModID[imod];
         }
      }

   }
}
