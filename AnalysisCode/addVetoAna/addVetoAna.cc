#include <iostream>
#include <string>
#include <cstdlib>

#include "TObject.h"
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"

#include "LcRecData/LcRecData.h"
#include "LcAna/LcAnaVeto.h"

#include "addVetoAna/LcCsiVetoHandler.h"

void   user_data( TTree *itree, 
                  std::vector<LcLib::LcAnaVeto*> &vetoVec,
                  const Int_t userflag );

UInt_t user_cut ( const LcLib::LcRecData *data,
                  const LcCsiVetoHandler *csiVeto,
                  const std::vector<LcLib::LcAnaVeto*> &vetoVec );

/*************************************************************************************************/

int main( int argc, char** argv )
{
   /// read arg
   if( argc!=4 ){
      std::cout << " arg err : (bin) (input) (output) (userflag)" << std::endl;
      return 0;
   }

   const std::string ifname = argv[1];
   const std::string ofname = argv[2];
   const Int_t userflag = std::atoi( argv[3] );

   std::cout<<" input file  : " << ifname << std::endl;
   std::cout<<" output file : " << ofname << std::endl;
   std::cout<<" userflag    : " << userflag << std::endl;

   /// read tree ///
   TChain *itree = new TChain("RecTree");
   itree->Add( ifname.c_str() );
   const Long64_t nentry = itree->GetEntries();
   std::cout<<" #input entry = " << nentry << std::endl;
   if( nentry==0 ) return 0;

   /// prepare data container ///
   std::vector<LcLib::LcAnaVeto*> vetoVec;
   user_data(itree, vetoVec, userflag);
   LcCsiVetoHandler *csiVeto = new LcCsiVetoHandler(userflag);

   /// Load input ///
   LcLib::LcRecData *data = new LcLib::LcRecData( itree );

   /// prepare output ///
   TFile *ofile = new TFile( ofname.c_str(), "RECREATE" );
   TTree *otree = itree->CloneTree(0);

   csiVeto->AddBranches(otree);
   for(std::vector<LcLib::LcAnaVeto*>::iterator it=vetoVec.begin(); it!=vetoVec.end(); ++it )
      (*it)->AddBranches(otree);

   UInt_t NewVetoCondition = 0;
   otree->Branch("NewVetoCondition",&NewVetoCondition,"NewVetoCondition/i");
 
   /// loop ///
   for( Long64_t ientry=0; ientry<nentry; ++ientry )
   {
      if( nentry>100 && (ientry%(nentry/10)==0) )
         std::cout<<" " << 10*ientry/(nentry/10) <<"%" << std::endl;

      itree->GetEntry(ientry);

      csiVeto->UpdateVars(data);
      for(std::vector<LcLib::LcAnaVeto*>::iterator it=vetoVec.begin(); it!=vetoVec.end(); ++it )
      {
         (*it)->Reset();
         (*it)->UpdateVars(data);
      }

      NewVetoCondition = user_cut(data,csiVeto,vetoVec);
      otree->Fill();
   }

   /// 
   ofile->cd();
   otree->Write();
   ofile->Close();

   std::cout<<" END! " << std::endl;
   return 1;
}
