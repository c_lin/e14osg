#include <iostream>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TCut.h"

int main( int argc, char** argv )
{
   /// read arg
   if( argc!=4 ){
      std::cout<<"arg err: " << argv[0] 
                <<" (input) (output) (cut)"
               << std::endl;
      return 0;
   }

   const std::string ifname  = argv[1];
   const std::string ofname  = argv[2];
   const std::string cutname = argv[3];

   std::cout<<" input: " << ifname << std::endl;
   std::cout<<" output: " << ofname << std::endl;
   std::cout<<" cut: " << cutname << std::endl;

   /// set input file
   TChain *itree = new TChain("RecTree");
   itree->Add( ifname.c_str() );
   std::cout<<" #entries in input file: " << itree->GetEntries() << std::endl;

   /// set output file
   TFile *ofile = new TFile( ofname.c_str(), "recreate");
   
   /// event selection
   const TCut user_cut( cutname.c_str() );
   TTree *otree = itree->CopyTree(user_cut);

   std::cout<<" #entries after selection: " << otree->GetEntries() << std::endl;

   otree->Write();
   ofile->Close();

   return 1;
}
