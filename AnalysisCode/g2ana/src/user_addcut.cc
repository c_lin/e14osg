#include "gnana/E14GNAnaFunction.h"
#include "gnana/E14GNAnaDataContainer.h"
#include "csimap/CsiMap.h"
#include "TMath.h"
#include <iostream>
#include <list>

#include <numeric>
#include <TMultiLayerPerceptron.h>
#include <TFile.h>
#include "TMath.h"

//int user_addcut(E14GNAnaDataContainer &data,double CSICoE, double VertexTimeDiff, double* AddCutVal){
int user_addcut(E14GNAnaDataContainer &data,double CSICoE, double VertexTimeDiff, double* AddCutVal,TMultiLayerPerceptron* mlp, TMultiLayerPerceptron* mlpkine, int userFlag, const int CSINDeadChannel, const std::vector<int> CSIDeadChannelIDVec){

  /*
  TFile* tmlp = new TFile("/home/had/shiomi/work2/beam2013_05/MasudaAnalysisCode/MyAnaPro4/ann/testangle/simu500.root");
  TMultiLayerPerceptron* mlp = (TMultiLayerPerceptron*)tmlp->Get("TMultiLayerPerceptron");

  TFile* tmlpkine = new TFile("/home/had/shiomi/work2/beam2013_05/MasudaAnalysisCode/MyAnaPro4/ann/test0814_2/simu1030.root");
  TMultiLayerPerceptron* mlpkine = (TMultiLayerPerceptron*)tmlpkine->Get("TMultiLayerPerceptron");
  */
  
  int AddCutCondition=0;

  double params[12];
  
  params[0]=data.GamClusSize[0];
  params[1]=data.GamClusSize[1];

  double RMS1,RMS2,RMSVert1,RMSVert2,RMSHori1,RMSHori2;
    
  for(int i=0; i<2; i++){
    double rms=0;
    double rms_hori=0;
    double rms_vert=0;
    double csitote=0;
    
    double ax=data.GammaPos[i][1]/data.GammaPos[i][0];
    double ay=-1/ax;
    double y0=data.GammaPos[i][1]-ay*data.GammaPos[i][0];
      
      for(int j=0; j<data.GamClusSize[i]; j++){
	double xpos=CsiMap::getCsiMap()->getX(data.GamClusCsiId[i][j]);
	double ypos=CsiMap::getCsiMap()->getY(data.GamClusCsiId[i][j]);	
	double r2 = pow(data.GamClusCoePos[i][0]-xpos,2)+pow(data.GamClusCoePos[i][1]-ypos,2);
	rms+=r2*data.GamClusCsiE[i][j];
	csitote+=data.GamClusCsiE[i][j];
	
	rms_hori += pow(fabs(ypos-ax*xpos)/sqrt(pow(ax,2)+pow(1,2)),2)*data.GamClusCsiE[i][j];
	rms_vert += pow(fabs(ypos-ay*xpos-y0)/sqrt(pow(ay,2)+pow(1,2)),2)*data.GamClusCsiE[i][j];

      }
      rms=rms/csitote;
      rms_hori=rms_hori/csitote;
      rms_vert=rms_vert/csitote;
      
      if(i==0){
	RMS1=sqrt(rms);
	RMSVert1=sqrt(rms_vert);
	RMSHori1=sqrt(rms_hori);
	//Size1=GamClusSize[0];
      } else {
	RMS2=sqrt(rms);
	RMSVert2=sqrt(rms_vert);
	RMSHori2=sqrt(rms_hori);
	//Size2=GamClusSize[1];
      }
      
    }

    params[2]=RMS1;
    params[3]=RMS2;
    params[4]=RMSVert1;
    params[5]=RMSVert2;
    params[6]=RMSHori1;
    params[7]=RMSHori2;
    params[8]=data.GammaE[0];
    params[9]=data.GammaE[1];

    params[10]=TMath::ATan(sqrt(pow(data.GammaMom[0][0],2)+pow(data.GammaMom[0][1],2))/data.GammaMom[0][2]);
    params[11]=TMath::ATan(sqrt(pow(data.GammaMom[1][0],2)+pow(data.GammaMom[1][1],2))/data.GammaMom[1][2]);

    
    double NNOutput=mlp->Evaluate(0, params);


    double params2[5];
    params2[0]=sqrt(pow(data.GammaPos[0][0],2)+pow(data.GammaPos[0][1],2));

    double ERatio = data.GammaE[1]/data.GammaE[0];
    
    params2[1]=ERatio;
    double r1=sqrt(pow(data.GammaPos[0][0],2)+pow(data.GammaPos[0][1],2));
    double r2=sqrt(pow(data.GammaPos[1][0],2)+pow(data.GammaPos[1][1],2));


    double ClusterDistance = sqrt(pow(data.GammaPos[0][0]-data.GammaPos[1][0],2)+
				  pow(data.GammaPos[0][1]-data.GammaPos[1][1],2));
    
    if(r1<r2) params2[2]=ClusterDistance;
    else  params2[2]=-ClusterDistance;

    double ProjectionAngle = pow( r1, 2) + pow( r2, 2) - pow( ClusterDistance, 2);
    ProjectionAngle /= (2 * r1 * r2 );
    ProjectionAngle = acos( ProjectionAngle ) * 180. / TMath::Pi();

    
    params2[3]=ProjectionAngle;
    params2[4]=data.GammaTime[0]-data.GammaTime[1];       

    double NNOutputKine=mlpkine->Evaluate(0, params2);

    double GamClusRMS[100];
    
    for(int i=0; i<data.GamClusNumber; i++){
      double rms=0;
      double csitote=0;
      for(int j=0; j<data.GamClusSize[i]; j++){
	double xpos=CsiMap::getCsiMap()->getX(data.GamClusCsiId[i][j]);
	double ypos=CsiMap::getCsiMap()->getY(data.GamClusCsiId[i][j]);	
	double r2 = pow(data.GamClusCoePos[i][0]-xpos,2)+pow(data.GamClusCoePos[i][1]-ypos,2);	
	rms+=r2*data.GamClusCsiE[i][j];
	csitote+=data.GamClusCsiE[i][j];
      }
      rms=rms/csitote;
      GamClusRMS[i]=sqrt(rms);
    }
    
    // dead channel treatment
    double MinDeadChR = -9999;
    if(CSINDeadChannel>0){
      double    min[CSINDeadChannel];
      for(int ideadch=0;ideadch<CSINDeadChannel;ideadch++){ //Initialize
	min[ideadch]       = 1e5;
      }
      
      for(int igam=0; igam<2; igam++){
	for(int ideadch=0; ideadch<CSINDeadChannel; ideadch++){
	  double xpos = CsiMap::getCsiMap()->getX( CSIDeadChannelIDVec.at(ideadch) );
	  double ypos = CsiMap::getCsiMap()->getY( CSIDeadChannelIDVec.at(ideadch) );
	  double r = sqrt( pow( data.GammaPos[igam][0] - xpos, 2 ) + 
			   pow( data.GammaPos[igam][1] - ypos, 2) );
	  if( min[igam] > r )
	    min[igam] = r;
	}
      }
      MinDeadChR = TMath::MinElement(CSINDeadChannel,min);
    }else{
		MinDeadChR = 9999;
	}
    
    AddCutCondition=0;
    if( data.GammaE[0]+data.GammaE[1]<650) AddCutCondition |= ( 1<< 0);
    if( CSICoE<200) AddCutCondition |= ( 1<< 1);
    if( TMath::Min(GamClusRMS[0],GamClusRMS[1])<10) AddCutCondition |= ( 1<< 2);
    if( TMath::Min(data.GamClusSize[0],data.GamClusSize[1])<5) AddCutCondition |= ( 1<< 3);
    if( fabs(VertexTimeDiff)>2) AddCutCondition |= ( 1<< 4);
    if( MinDeadChR<53 ) AddCutCondition |= ( 1<< 5);
    //    if( NNOutputKine<0.67 ) AddCutCondition |= ( 1<< 6); //obsolete (2015~)
    //    if( NNOutput<0.8 ) AddCutCondition |= ( 1<< 7); //obsolete (2015~)

    int MinClusSize=TMath::Min(data.GamClusSize[0],data.GamClusSize[1]);
    double MinClusRMS=TMath::Min(GamClusRMS[0],GamClusRMS[1]);

    AddCutVal[0]=NNOutput;
    AddCutVal[1]=NNOutputKine;
    AddCutVal[2]=MinClusRMS;
    AddCutVal[3]=MinDeadChR;
    
    //tmlp->Close();
    //tmlpkine->Close();

    //delete tmlp;
    //delete tmlpkine;

    return AddCutCondition;
}
  
  




