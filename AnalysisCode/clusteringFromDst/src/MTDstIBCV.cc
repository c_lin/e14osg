#include "clusteringFromDst/MTDstIBCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDstIBCV::MTDstIBCV( Int_t runID, Int_t userFlag ) : MTDstVeto( CFDP::IBCV, runID, userFlag )
{
  SetNumFromModID();
  SetModIDToChannel();  
  Wfm_ProdDer2threshold = -200;
  Wfm_Smoothnessthreshold = 35;
  Initialize();
}



void MTDstIBCV::Process2()
{

  ApplyEnergyCorrection();
  ApplyTimingCorrection();

  detnumber = DetectorNCH;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    detmodid[ich]    = 0;
    detene[ich]      = 0;
    dettime[ich]     = 0;
    detptime[ich]    = 0;
    detfalltime[ich] = 0;
    detsmoothness[ich] = 0;
    detprodder2[ich] = 0;
    detareaR[ich] = 0;
    detwfmcorrectnumber[ich] = -1;
  }

  Wfm_corrupted = 0;
  for(int i=0; i<DetNumber; i++){
    int num = NumFromModID[DetModID[i]];
    detmodid[num]    = DetModID[i];
    detene[num]      = DetEne[i];
    dettime[num]     = DetTime[i]*8;  // [clock]->[ns]
    detptime[num]    = DetPTime[i]*8; // [clock]->[ns]
    detfalltime[num] = DetFallTime[i]*8;
	detsmoothness[num] = DetSmoothness[i];
    detprodder2[num] = DetProdDer2[i];
    detareaR[num]    = DetAreaR[i];
    detwfmcorrectnumber[num]    = DetWfmCorrectNumber[i];
	if(fabs(DetSmoothness[i])>Wfm_Smoothnessthreshold) Wfm_corrupted++;
  	if(DetProdDer2[i]<Wfm_ProdDer2threshold)           Wfm_corrupted += 10000;
  }

  DetModuleNumber = 0;
  for(int imod=0; imod<DetectorNMod; imod++){
    if(detene[imod]>0){
      DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[imod]];
      DetModuleHitTime[DetModuleNumber] = GetHitTime( detptime[imod], detptime[imod+32] );
      DetModuleHitZ[DetModuleNumber]    = GetHitZ(    detptime[imod], detptime[imod+32] );
      DetModuleEne[DetModuleNumber]     = GetEne(     detene[imod],   detene[imod+32],
						      detptime[imod], detptime[imod+32] );
	  DetModuleSmoothness[DetModuleNumber] = ((fabs(detsmoothness[imod])>fabs(detsmoothness[imod+32])) 
			? detsmoothness[imod] : detsmoothness[imod+32] );
	  DetModuleProdDer2[DetModuleNumber] = ((detprodder2[imod]<detprodder2[imod+32]) 
			? detprodder2[imod] : detprodder2[imod+32] );
	  DetModuleAreaR[DetModuleNumber] = ((detareaR[imod]<detareaR[imod+32]) 
			? detareaR[imod] : detareaR[imod+32] );

	  DetModuleWfmCorrectNumber[DetModuleNumber] = -1; 
	  int modcorrectnumber = 0;
	  if(detwfmcorrectnumber[imod]>=0)   modcorrectnumber += detwfmcorrectnumber[imod];
	  if(detwfmcorrectnumber[imod+32]>=0) modcorrectnumber += detwfmcorrectnumber[imod+32]*100;
	  if(detwfmcorrectnumber[imod]>=0||detwfmcorrectnumber[imod+32]>=0) DetModuleWfmCorrectNumber[DetModuleNumber] = modcorrectnumber;

      // dead channel treatment
      if( m_userFlag>20160401 && m_userFlag<20190101 ){
		  if( imod==27 ){
			  DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[27]];
			  DetModuleHitTime[DetModuleNumber] = detptime[27];
			  DetModuleHitZ[DetModuleNumber]    = 0;
			  DetModuleEne[DetModuleNumber]     = detene[27];
			  DetModuleSmoothness[DetModuleNumber] = detsmoothness[27]; 
			  DetModuleProdDer2[DetModuleNumber] = detprodder2[27]; 
			  DetModuleAreaR[DetModuleNumber] = detareaR[27]; 
		  }
	  }
      
	  ++DetModuleNumber;
	}
  }
}

void MTDstIBCV::SetNumFromModID()
{
  int count = 0;
  for(int i=0; i<32; i++){
    NumFromModID[i] = count;
    ++count;
  }
  for(int i=32; i<100; i++){
    NumFromModID[i] = -1;
  }  
  for(int i=100; i<132; i++){
    NumFromModID[i] = count;
    ++count;
  }
}

void MTDstIBCV::SetModIDToChannel()
{
  for(int i=0; i<32; i++)    ModIDFromChannel[i] = i;
  for(int i=32; i<100; i++)  ModIDFromChannel[i] = -1;
  for(int i=100; i<132; i++) ModIDFromChannel[i] = i-100;
}

float MTDstIBCV::GetHitTime( float frontTime, float rearTime )
{
  return ( frontTime + rearTime ) / 2.;
}

float MTDstIBCV::GetHitZ( float frontTime, float rearTime )
{
  return ( frontTime - rearTime ) / 2. * MTBP::IBCVPropVelo;
}

float MTDstIBCV::GetEne( float frontEne, float rearEne, float frontTime, float rearTime)
{
  float hitZ = GetHitZ( frontTime, rearTime );
  if( hitZ > MTBP::IBCVLength / 2. )   // downstream overflow
    return rearEne;
  if( hitZ < - MTBP::IBCVLength / 2. ) // upstream overflow
    return frontEne;
  
  float e = frontEne / exp( -hitZ / ( MTBP::IBCVLAMBDA_U + MTBP::IBCVALPHA_U * hitZ ) );
  e      += rearEne  / exp(  hitZ / ( MTBP::IBCVLAMBDA_D - MTBP::IBCVALPHA_D * hitZ ) );
  return e;
}

