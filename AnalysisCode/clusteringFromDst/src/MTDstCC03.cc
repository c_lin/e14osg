#include "clusteringFromDst/MTDstCC03.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDstCC03::MTDstCC03( Int_t runID, Int_t userFlag ) : MTDstVeto( CFDP::CC03, runID, userFlag )
{
  
  Wfm_ProdDer2threshold = -200;
  Wfm_Smoothnessthreshold = 30;
	
  SetModIDToChannel();
  Initialize();
}

void MTDstCC03::Initialize()
{
  DetNumber = DetectorNCH;
  for( int imod=0; imod<DetNumber; imod++ ){
    DetModID[imod]    = 0;
    DetEne[imod]      = 0;
    DetTime[imod]     = 0;
    DetPTime[imod]    = 0;
  }
  
  DetModuleNumber = DetectorNMod;
  for( int imod=0; imod<DetModuleNumber; imod++ ){
    DetModuleModID[imod]   = imod*2;
    DetModuleHitTime[imod] = 0;
    DetModuleEne[imod]     = 0;
  }
}  

void MTDstCC03::Process2()
{
  ApplyEnergyCorrection();
  ApplyTimingCorrection();
    
  detnumber = DetectorNCH;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    detmodid[ich]    = 0;
    detene[ich]      = 0;
    dettime[ich]     = 0;
    detptime[ich]    = 0;
    detfalltime[ich] = 0;
    detsmoothness[ich] = 0;
    detprodder2[ich] = 0;
    detareaR[ich] = 0;
    detwfmcorrectnumber[ich] = -1;
  }

  Wfm_corrupted = 0;
  for(int i=0; i<DetNumber; i++){
    int num = NumFromModID[DetModID[i]];
    detmodid[num]    = DetModID[i];
    detene[num]      = DetEne[i];
    dettime[num]     = DetTime[i]*8;  // [clock]->[ns]
    detptime[num]    = DetPTime[i]*8; // [clock]->[ns]
    detfalltime[num] = DetFallTime[i]*8;
    detsmoothness[num] = DetSmoothness[i];
    detprodder2[num] = DetProdDer2[i];
    detareaR[num] = DetAreaR[i];
    detwfmcorrectnumber[num] = DetWfmCorrectNumber[i];
	if(fabs(DetSmoothness[i])>Wfm_Smoothnessthreshold) Wfm_corrupted++;
  	if(DetProdDer2[i]<Wfm_ProdDer2threshold)           Wfm_corrupted += 10000;
  }

  DetModuleNumber = 0;
  for(int i=0; i<detnumber; i=i+2){
    if(detene[i]>0){
      // dead channel treatment
      if( m_userFlag<20160101 ){ //2015 run
	if(i==16){
	  DetModuleModID[DetModuleNumber]   = ModIDFromChannel[i];
	  DetModuleHitTime[DetModuleNumber] = detptime[i];
	  DetModuleEne[DetModuleNumber]     = detene[i];
	}else{
	  DetModuleModID[DetModuleNumber]   = ModIDFromChannel[i];
	  DetModuleHitTime[DetModuleNumber] = GetHitTime( detptime[i], detptime[i+1] );
	  DetModuleEne[DetModuleNumber]     = GetEne(     detene[i],   detene[i+1] );
	}
	++DetModuleNumber;

      }else{ // 2016run~
	DetModuleModID[DetModuleNumber]   = ModIDFromChannel[i];
	DetModuleHitTime[DetModuleNumber] = GetHitTime( detptime[i], detptime[i+1] );
	DetModuleEne[DetModuleNumber]     = GetEne(     detene[i],   detene[i+1] );

	DetModuleSmoothness[DetModuleNumber] = ((fabs(detsmoothness[i])>fabs(detsmoothness[i+1])) 
			? detsmoothness[i] : detsmoothness[i+1] );
  	DetModuleProdDer2[DetModuleNumber] = ((detprodder2[i]<detprodder2[i+1]) 
			? detprodder2[i] : detprodder2[i+1] );
  	DetModuleAreaR[DetModuleNumber] = ((detareaR[i]<detareaR[i+1]) 
			? detareaR[i] : detareaR[i+1] );

	DetModuleWfmCorrectNumber[DetModuleNumber] = -1; 
	int modcorrectnumber = 0;
	if(detwfmcorrectnumber[i  ]>=0) modcorrectnumber += detwfmcorrectnumber[i  ];
	if(detwfmcorrectnumber[i+1]>=0) modcorrectnumber += detwfmcorrectnumber[i+1]*100;
	if(detwfmcorrectnumber[i]>=0||detwfmcorrectnumber[i+1]>=0) DetModuleWfmCorrectNumber[DetModuleNumber] = modcorrectnumber;


	++DetModuleNumber;
      }
    }
  }

}

void MTDstCC03::SetModIDToChannel()
{
  for(int i=0; i<32; i++){
    if(i%2==0) ModIDFromChannel[i] = i;
    else       ModIDFromChannel[i] = i-1;
  }
}

