#include "clusteringFromDst/MTDstBCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDstBCV::MTDstBCV( Int_t runID, Int_t userFlag ) : MTDstVeto( CFDP::BCV, runID, userFlag ), m_userFlag( userFlag )
{
  SetNumFromModID();
  SetModIDToChannel();
}


void MTDstBCV::Process2()
{
  ApplyEnergyCorrection();
  ApplyTimingCorrection();

  detnumber = DetectorNCH;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    detmodid[ich]    = 0;
    detene[ich]      = 0;
    dettime[ich]     = 0;
    detptime[ich]    = 0;
  }

  for(int i=0; i<DetNumber; i++){
    int num = NumFromModID[DetModID[i]];
    detmodid[num]    = DetModID[i];
    detene[num]      = DetEne[i];
    dettime[num]     = DetTime[i]*8;  // [clock]->[ns]
    detptime[num]    = DetPTime[i]*8; // [clock]->[ns]
  }

  DetModuleNumber = 0;
  for(int imod=0; imod<DetectorNMod; imod++){
    if(detene[imod]>0){
      DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[imod]];
      DetModuleHitTime[DetModuleNumber] = GetHitTime( detptime[imod], detptime[imod+32] );
      DetModuleHitZ[DetModuleNumber]    = GetHitZ(    detptime[imod], detptime[imod+32] );
      DetModuleEne[DetModuleNumber]     = GetEne(     detene[imod],   detene[imod+32],
						      detptime[imod], detptime[imod+32] );
      
      ++DetModuleNumber;
    }
  }
}

void MTDstBCV::SetNumFromModID()
{
  int count = 0;
  for(int i=0; i<32; i++){
    NumFromModID[i] = count;
    ++count;
  }
  for(int i=32; i<100; i++){
    NumFromModID[i] = -1;
  }  
  for(int i=100; i<132; i++){
    NumFromModID[i] = count;
    ++count;
  }
}

void MTDstBCV::SetModIDToChannel()
{
  for(int i=0; i<32; i++)    ModIDFromChannel[i] = i;
  for(int i=32; i<100; i++)  ModIDFromChannel[i] = -1;
  for(int i=100; i<132; i++) ModIDFromChannel[i] = i-100;
}

float MTDstBCV::GetHitTime( float frontTime, float rearTime )
{
  return ( frontTime + rearTime ) / 2.;
}

float MTDstBCV::GetHitZ( float frontTime, float rearTime )
{
  return ( frontTime - rearTime ) / 2. * MTBP::BCVPropVelo;
}

float MTDstBCV::GetEne( float frontEne, float rearEne, float frontTime, float rearTime)
{
  float hitZ = GetHitZ( frontTime, rearTime );
  if( hitZ > MTBP::BCVLength / 2. )   // downstream overflow
    return rearEne;
  if( hitZ < - MTBP::BCVLength / 2. ) // upstream overflow
    return frontEne;
  
  float e = frontEne / exp( -hitZ / ( MTBP::BCVLAMBDA + MTBP::BCVALPHA * hitZ ) );
  e      += rearEne  / exp(  hitZ / ( MTBP::BCVLAMBDA - MTBP::BCVALPHA * hitZ ) );
  return e;
}

