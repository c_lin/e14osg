#include "clusteringFromDst/MTDstCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDstCV::MTDstCV( Int_t runID, Int_t userFlag ) : MTDstVeto( CFDP::CV, runID, userFlag)
{
  CVModuleRiseTime = new Float_t[DetectorNCH][2];
  CVModuleFallTime = new Float_t[DetectorNCH][2];
  Wfm_ProdDer2threshold = -250;
  Wfm_Smoothnessthreshold = 40;
  SetNumFromModID();
  SetModIDToChannel();
  Initialize();
}

MTDstCV::~MTDstCV()
{
  delete[] CVModuleRiseTime;
  delete[] CVModuleFallTime;
}

void MTDstCV::Branch( TTree* tr )
{
  
  MTDstDetector::Branch(tr);
  
  ClusterTree->Branch( "CVModuleRiseTime", CVModuleRiseTime, "CVModuleRiseTime[CVModuleNumber][2]/F" );
  ClusterTree->Branch( "CVModuleFallTime", CVModuleFallTime, "CVModuleFallTime[CVModuleNumber][2]/F" );
}

void MTDstCV::Initialize()
{
  MTDstVeto::Initialize(); 

  DetModuleNumber = DetectorNMod;
  for( int imod=0; imod<DetectorNMod; imod++ ){
    if( imod<48 ){
      // front 0, 2, 4, ..., 92, 94
      DetModuleModID[imod] = imod*2;
    }else{
      // rear 100, 102, 104, ..., 284, 186
      DetModuleModID[imod] = (imod+2)*2;
    }
    
    DetModuleHitTime[imod] = 0;
    DetModuleEne[imod]     = 0;

    CVModuleRiseTime[imod][0] = MTBP::Invalid;
    CVModuleRiseTime[imod][1] = MTBP::Invalid;
    CVModuleFallTime[imod][0] = MTBP::Invalid;
    CVModuleFallTime[imod][1] = MTBP::Invalid;
  }
}  



void MTDstCV::Process2()
{

  
  ApplyEnergyCorrection();
  ApplyTimingCorrection();

  detnumber = DetectorNCH;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    detmodid[ich]        = 0;
    detene[ich]          = 0;
    dettime[ich]         = 0;
    detptime[ich]        = 0;
    detfalltime[ich]     = 0;
	detsmoothness[ich] = 0;
    detprodder2[ich] = 0;
    detareaR[ich] = 0;
    detwfmcorrectnumber[ich] = -1;
  }
  
  Wfm_corrupted = 0;
  for(int i=0; i<DetNumber; i++){
    int num = NumFromModID[DetModID[i]];
    detmodid[num]       = DetModID[i];
    detene[num]         = DetEne[i];
    dettime[num]        = DetTime[i];
    detptime[num]       = DetPTime[i];
	detsmoothness[num] = DetSmoothness[i];
    detprodder2[num] = DetProdDer2[i];
    detareaR[num] = DetAreaR[i];
    detwfmcorrectnumber[num] = DetWfmCorrectNumber[i];
	if(fabs(DetSmoothness[i])>Wfm_Smoothnessthreshold) Wfm_corrupted++;
  	if(DetProdDer2[i]<Wfm_ProdDer2threshold)           Wfm_corrupted += 10000;
  }

  DetModuleNumber = 0;

  // dead channel treatment
  if( m_userFlag<20160101 ){
    for(int i=0; i<detnumber; i=i+2){
      if(i==168) continue;
      
      if(detene[i]>0){
	if( i!=110 ){
	  DetModuleHitTime[DetModuleNumber] = GetHitTime( detptime[i], detptime[i+1] ) * 8.; // [clock] -> [ns]
	  DetModuleEne[DetModuleNumber]     = GetEne( detene[i], detene[i+1] );
	  DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[i]];
	  
	  CVModuleRiseTime[DetModuleNumber][0] = dettime[i]*8.;
	  CVModuleRiseTime[DetModuleNumber][1] = dettime[i+1]*8.;
	}else{
	  // dead channel
	  DetModuleHitTime[DetModuleNumber] = detptime[110]*8;
	  DetModuleEne[DetModuleNumber]     = detene[110];
	  DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[i]];
	}
	++DetModuleNumber;
      }
    }
    
    // dead channel
    if(detene[169]>0){
      DetModuleHitTime[DetModuleNumber] = detptime[169] * 8.;
      DetModuleEne[DetModuleNumber]     = detene[169];
      DetModuleModID[DetModuleNumber]   = 172;    
      ++DetModuleNumber;
    }

  } else {
    for(int i=0; i<detnumber; i=i+2){
      if(detene[i]>0){
	DetModuleHitTime[DetModuleNumber] = GetHitTime( detptime[i], detptime[i+1] ) * 8.; // [clock] -> [ns]
	DetModuleEne[DetModuleNumber]     = GetEne( detene[i], detene[i+1] );
	DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[i]];
	
		
	DetModuleSmoothness[DetModuleNumber] = ((fabs(detsmoothness[i])>fabs(detsmoothness[i+1])) 
			? detsmoothness[i] : detsmoothness[i+1] );
	DetModuleProdDer2[DetModuleNumber] = ((detprodder2[i]<detprodder2[i+1]) 
			? detprodder2[i] : detprodder2[i+1] );
  	DetModuleAreaR[DetModuleNumber] = ((detareaR[i]<detareaR[i+1]) 
			? detareaR[i] : detareaR[i+1] );

	DetModuleWfmCorrectNumber[DetModuleNumber] = -1; 
	int modcorrectnumber = 0;
	if(detwfmcorrectnumber[i]>=0)   modcorrectnumber += detwfmcorrectnumber[i  ];
	if(detwfmcorrectnumber[i+1]>=0) modcorrectnumber += detwfmcorrectnumber[i+1]*100;
	if(detwfmcorrectnumber[i]>=0||detwfmcorrectnumber[i+1]>=0) DetModuleWfmCorrectNumber[DetModuleNumber] = modcorrectnumber;



	CVModuleRiseTime[DetModuleNumber][0] = dettime[i]*8.;
	CVModuleRiseTime[DetModuleNumber][1] = dettime[i+1]*8.;
	CVModuleFallTime[DetModuleNumber][0] = detfalltime[i]*8.;
	CVModuleFallTime[DetModuleNumber][1] = dettime[i+1]*8.;
	++DetModuleNumber;
      }
    }
  }

}

void MTDstCV::SetNumFromModID()
{
  int count = 0;
  for(int i=0; i<96; i++){
    NumFromModID[i] = count;
    ++count;
  }
  for(int i=96; i<100; i++){
    NumFromModID[i] = -1;
  }
  for(int i=100; i<188; i++){
    NumFromModID[i] = count;
    ++count;
  }
}

void MTDstCV::SetModIDToChannel()
{
  for(int i=0; i<96; i++){
    if(i%2==0)
      ModIDFromChannel[i] = i;
    else
      ModIDFromChannel[i] = i-1;
  }
  for(int i=96; i<100; i++){
    ModIDFromChannel[i] = -1;
  }
  for(int i=100; i<188; i++){
    if(i%2==0)
      ModIDFromChannel[i] = i;
    else
      ModIDFromChannel[i] = i-1; 
  }
}

