#include "clusteringFromDst/MTDst500MHzDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDst500MHzDetector::MTDst500MHzDetector( Int_t detID, Int_t runID, Int_t userFlag ) : MTDstDetector( detID, runID ), m_userFlag( userFlag )
{  
  Initialize();
}

void MTDst500MHzDetector::Initialize()
{
  int maxHit = 20;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    DetModID[ich]    = 0;
    DetNHit500[ich]  = 0;
    for(int i=0; i<maxHit; i++){    
      DetEne500[ich][i]  = 0;
      DetTime500[ich][i] = 0;
    }
  }
}  


void MTDst500MHzDetector::Process()
{
	Process2();
}

void MTDst500MHzDetector::Process2()
{
  

  ApplyEnergyCorrection();
  ApplyTimingCorrection();

  DetModuleNumber = DetNumber;
  int maxhit = 20;
  for(int ich=0; ich<DetModuleNumber; ich++){
    DetModuleModID[ich]    = 0;
    DetModulenHits500[ich] = 0;
    for(int ihit=0; ihit<maxhit; ihit++){
      DetModuleEne500[ich][ihit]     = 0;
      DetModuleHitTime500[ich][ihit] = 0;
    }
  }
  for(int ich=0; ich<DetModuleNumber; ich++){
    DetModuleModID[ich]    = DetModID[ich];
    DetModulenHits500[ich] = DetNHit500[ich];
    for(int ihit=0; ihit<DetNHit500[ich]; ihit++){
      DetModuleEne500[ich][ihit]     = DetEne500[ich][ihit];
      DetModuleHitTime500[ich][ihit] = DetTime500[ich][ihit]*2;
	  DetModuleAreaR500[ich][ihit]   = DetAreaR500[ich][ihit];
    }
  }

  /*
	
  ApplyEnergyCorrection();
  ApplyTimingCorrection();
  
  int maxhit = 20;
  detnumber = DetectorNCH;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    detmodid[ich]    = 0;
    detnhit500[ich]  = 0;
    for(int ihit=0; ihit<maxhit; ihit++){
      detene500[ich][ihit]  = 0;
      dettime500[ich][ihit] = 0;
    }    
  }

  for(int ich=0; ich<DetNumber; ich++){    
    int num = NumFromModID[DetModID[ich]];
    
	detmodid[num]    = DetModID[ich];
    detnhit500[num]  = DetNHit500[ich];
    for(int ihit=0; ihit<DetNHit500[ich]; ihit++){
      detene500[num][ihit]  = DetEne500[ich][ihit];
      dettime500[num][ihit] = DetTime500[ich][ihit]*2; // [clock] -> [ns]
    }
  }
 
  DetModuleNumber = 0;
  for(int imod=0; imod<DetectorNCH; imod++){
	  //std::cout << "imod=" << imod << "/" << DetectorNMod << ", ModIDFromChannel[" << detmodid[imod]<< "]=" << ModIDFromChannel[detmodid[imod]] << "\n";
  
	  if(detnhit500[imod]>0){      
		  DetModuleModID[DetModuleNumber]    = ModIDFromChannel[detmodid[imod]];
		  DetModulenHits500[DetModuleNumber] = detnhit500[imod];
		  //std::cout << "imod=" << imod << "/" << DetectorNMod << ", Nhit=" << detnhit500[imod] << "\n";
      
		  for(int ihit=0; ihit<detnhit500[imod]; ihit++){
			  DetModuleEne500[DetModuleNumber][ihit]     = detene500[imod][ihit];
			  DetModuleHitTime500[DetModuleNumber][ihit] = dettime500[imod][ihit];
		  }
		  ++DetModuleNumber;
	  }
  }
  */
}


