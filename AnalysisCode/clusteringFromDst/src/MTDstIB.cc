#include "clusteringFromDst/MTDstIB.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDstIB::MTDstIB( Int_t runID, Int_t userFlag ) : MTDst500MHzDetector( CFDP::IB, runID, userFlag )
{
  SetNumFromModID();
  SetModIDToChannel();
  Initialize();
}

void MTDstIB::SetNumFromModID(){
  int count = 0;
  for(int i=0; i<32; i++){
    NumFromModID[i] = count;
    ++count;
  }

  for(int i=32; i<100; i++){
    NumFromModID[i] = -1;
  }

  for(int i=100; i<132; i++){
    NumFromModID[i] = count;
    ++count;
  }

}

void MTDstIB::SetModIDToChannel(){

  for(int i=0; i<32; i++){
    ModIDFromChannel[i]=i;
  }

  for(int i=32; i<100; i++){
    ModIDFromChannel[i]=-1;
  }

  for(int i=100; i<132; i++){
    ModIDFromChannel[i]=i-100;
  }

}
