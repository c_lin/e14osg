#include "clusteringFromDst/MTDstVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <TMath.h>

#include <iostream>
#include <fstream>

MTDstVeto::MTDstVeto( Int_t detID, Int_t runID, Int_t userFlag ) : MTDstDetector( detID, runID ), m_userFlag( userFlag )
{
}

void MTDstVeto::Initialize()
{
	
  DetNumber = DetectorNCH;
  for( int imod=0; imod<DetNumber; imod++ ){
    DetModID[imod]    = 0;
    DetEne[imod]      = 0;
    DetTime[imod]     = 0;
    DetPTime[imod]    = 0;
    DetFallTime[imod] = 0;
    DetSmoothness[imod] = 0;
    DetProdDer2[imod] = 0;
    DetAreaR[imod] = 0;
    DetWfmCorrectNumber[imod] = -1;
  }
  
  DetModuleNumber = DetectorNMod;
  for( int imod=0; imod<DetModuleNumber; imod++ ){
    DetModuleModID[imod]   = imod;
    DetModuleHitTime[imod] = 0;
    DetModuleEne[imod]     = 0;
    DetModuleHitZ[imod]    = 0;
    DetModuleSmoothness[imod] = 0;
    DetModuleProdDer2[imod] = 0;
    DetModuleAreaR[imod] = 0;
    DetModuleWfmCorrectNumber[imod] = -1;
  }
}  




void MTDstVeto::Process2()
{
 
	
  ApplyEnergyCorrection();
  ApplyTimingCorrection();


  detnumber = DetectorNCH;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    detmodid[ich]    = 0;
    detene[ich]      = 0;
    dettime[ich]     = 0;
    detptime[ich]    = 0;
    detfalltime[ich] = 0;
    detsmoothness[ich] = 0;
    detprodder2[ich] = 0;
    detareaR[ich] = 0;
    detwfmcorrectnumber[ich] = -1;
  }

  Wfm_corrupted = 0;
  for(int i=0; i<DetNumber; i++){
    int num = NumFromModID[DetModID[i]];
    detmodid[num]    = DetModID[i];
    detene[num]      = DetEne[i];
    dettime[num]     = DetTime[i]*8;  // [clock]->[ns]
    detptime[num]    = DetPTime[i]*8; // [clock]->[ns]
    detfalltime[num] = DetFallTime[i]*8;
    detsmoothness[num] = DetSmoothness[i];
    detprodder2[num] = DetProdDer2[i];
    detareaR[num] = DetAreaR[i];
    detwfmcorrectnumber[num] = DetWfmCorrectNumber[i];
	if(fabs(DetSmoothness[i])>Wfm_Smoothnessthreshold) Wfm_corrupted++;
  	if(DetProdDer2[i]<Wfm_ProdDer2threshold)           Wfm_corrupted += 10000;
  }
  
  DetModuleNumber = 0;
  for(int i=0; i<detnumber; i++){
    if(detene[i]>0){
		DetModuleModID[DetModuleNumber]   = ModIDFromChannel[i];
		DetModuleHitTime[DetModuleNumber] = detptime[i];
		DetModuleEne[DetModuleNumber]     = detene[i];
		DetModuleSmoothness[DetModuleNumber] = detsmoothness[i];
		DetModuleProdDer2[DetModuleNumber] = detprodder2[i];
		DetModuleAreaR[DetModuleNumber] = detareaR[i];
		DetModuleWfmCorrectNumber[DetModuleNumber] = detwfmcorrectnumber[i];
		++DetModuleNumber;
    }
  }  


}


