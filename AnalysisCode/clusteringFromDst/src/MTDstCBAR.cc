#include "clusteringFromDst/MTDstCBAR.h"

MTDstCBAR::MTDstCBAR( Int_t runID, Int_t userFlag) : MTDstVeto( CFDP::CBAR, runID, userFlag )
{
  Wfm_ProdDer2threshold = -230;
  Wfm_Smoothnessthreshold = 100;
  SetNumFromModID();
  SetModIDToChannel();
}

void MTDstCBAR::Process2()
{
  ApplyEnergyCorrection();
  ApplyTimingCorrection();

  detnumber = DetectorNCH;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    detmodid[ich]    = 0;
    detene[ich]      = 0;
    dettime[ich]     = 0;
    detptime[ich]    = 0;
    detfalltime[ich] = 0;
    detsmoothness[ich] = 0;
    detprodder2[ich] = 0;
    detareaR[ich] = 0;
    detwfmcorrectnumber[ich] = -1;
  }

  for(int i=0; i<DetNumber; i++){
    int num = NumFromModID[DetModID[i]];
    detmodid[num]    = DetModID[i];
    detene[num]      = DetEne[i];
    dettime[num]     = DetTime[i]*8;  // [clock]->[ns]
    detptime[num]    = DetPTime[i]*8; // [clock]->[ns]
    detfalltime[num] = DetFallTime[i];
    detsmoothness[num] = DetSmoothness[i];
    detprodder2[num] = DetProdDer2[i];
    detareaR[num] = DetAreaR[i];
    detwfmcorrectnumber[num] = DetWfmCorrectNumber[i];
  }

  Wfm_corrupted = 0;
  DetModuleNumber = 0;
  for(int imod=0; imod<DetectorNMod; imod++){
    if(detene[imod]>0){
	  
	  if(fabs(DetSmoothness[imod])>Wfm_Smoothnessthreshold) Wfm_corrupted++;
	  if(DetProdDer2[imod]<Wfm_ProdDer2threshold)                    Wfm_corrupted += 10000;

      DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[imod]];
      DetModuleHitTime[DetModuleNumber] = GetHitTime( detptime[imod], detptime[imod+64] );
      DetModuleHitZ[DetModuleNumber]    = GetHitZ(    detptime[imod], detptime[imod+64] );
      DetModuleEne[DetModuleNumber]     = GetEne(     detene[imod],   detene[imod+64],
							detptime[imod], detptime[imod+64] );

	  DetModuleSmoothness[DetModuleNumber] = ((fabs(detsmoothness[imod])>fabs(detsmoothness[imod+64])) 
			? detsmoothness[imod] : detsmoothness[imod+64] );
	  DetModuleProdDer2[DetModuleNumber] = ((detprodder2[imod]<detprodder2[imod+64]) 
			? detprodder2[imod] : detprodder2[imod+64] );
	  DetModuleAreaR[DetModuleNumber] = ((detareaR[imod]<detareaR[imod+64]) 
			? detareaR[imod] : detareaR[imod+64] );

	  DetModuleWfmCorrectNumber[DetModuleNumber] = -1; 
	  int modcorrectnumber = 0;
	  if(detwfmcorrectnumber[imod   ]>=0) modcorrectnumber += detwfmcorrectnumber[imod   ];
	  if(detwfmcorrectnumber[imod+64]>=0) modcorrectnumber += detwfmcorrectnumber[imod+64]*100;
	  if(detwfmcorrectnumber[imod]>=0||detwfmcorrectnumber[imod+64]>=0) DetModuleWfmCorrectNumber[DetModuleNumber] = modcorrectnumber;

      ++DetModuleNumber;
    }
  }
}

void MTDstCBAR::Branch( TTree* tree ){
	MTDstDetector::Branch(tree);

	if( 20190101 < m_userFlag && m_userFlag < 20190701){  // Run81 and Run82
	  ClusterTree->Branch("CBARCH21Ene",&detene[21], "CBARCH21Ene/F");
	  ClusterTree->Branch("CBARCH21Time",&detptime[21], "CBARCH21Time/F");
	}
}


void MTDstCBAR::SetNumFromModID()
{
  int count = 0;
  for(int i=0; i<64; i++){
    NumFromModID[i] = count;
    ++count;
  }
  for(int i=64; i<100; i++){
    NumFromModID[i] = -1;
  }
  for(int i=100; i<164; i++){
    NumFromModID[i] = count;
    ++count;
  }
}

void MTDstCBAR::SetModIDToChannel()
{
  for(int i=0; i<64; i++)    ModIDFromChannel[i] = i;
  for(int i=64; i<100; i++)  ModIDFromChannel[i] = -1;
  for(int i=100; i<164; i++) ModIDFromChannel[i] = i-100;
}

float MTDstCBAR::GetHitTime( float frontTime, float rearTime )
{
  return ( frontTime + rearTime ) / 2.;
}

float MTDstCBAR::GetHitZ( float frontTime, float rearTime )
{
  return ( frontTime - rearTime ) / 2. * MTBP::CBARPropVelo;
}

float MTDstCBAR::GetEne( float frontEne, float rearEne, float frontTime, float rearTime)
{
  float hitZ = GetHitZ( frontTime, rearTime );

  if( hitZ >   MTBP::CBARLength / 2. )   // downstream overflow
    return rearEne;
  if( hitZ < - MTBP::CBARLength / 2. ) // upstream overflow
    return frontEne;

  float e = frontEne / exp( -hitZ / ( MTBP::CBARLAMBDA + MTBP::CBARALPHA * hitZ ) );
  e      += rearEne  / exp(  hitZ / ( MTBP::CBARLAMBDA - MTBP::CBARALPHA * hitZ ) );  
  return e;
}
