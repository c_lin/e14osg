#include "clusteringFromDst/MTDstDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDstDetector::MTDstDetector( Int_t detID, Int_t runID ):RunID( runID ),
							 DetectorID( detID ),
							 DetectorName( CFDP::DetName[detID].c_str() ),
							 DetectorNCH(  CFDP::DetNChannels[detID] ),
							 DetectorNMod( CFDP::DetNModules[detID] ),
							 is125MHz(     CFDP::Is125MHz[detID] )
{
  DetNumber = 0;
  DetModID    = new Int_t[DetectorNCH];
  DetFTTChisq = new Float_t [DetectorNCH];
  DetFTTTime  = new Float_t [DetectorNCH];
  m_isGetFTTBranch = false;
  m_isSetFTTBranch = false;
  if( is125MHz ){
    DetIntADC   = new Float_t[DetectorNCH];
    DetEne      = new Float_t[DetectorNCH];
    DetTime     = new Float_t[DetectorNCH];
    DetPTime    = new Float_t[DetectorNCH];
    DetFallTime = new Float_t[DetectorNCH];
    DetSmoothness = new Float_t[DetectorNCH];
    DetProdDer2 = new Float_t[DetectorNCH];
    ClockShift  = new Int_t  [DetectorNCH];
	DetAreaR    = new Float_t[DetectorNCH];
	DetWfmCorrectNumber = new short[DetectorNCH];
  }else{
    DetNHit500  = new Short_t[DetectorNCH];
    DetEne500   = new Float_t[DetectorNCH][20];
    DetTime500  = new Float_t[DetectorNCH][20];
    DetAreaR500  = new Float_t[DetectorNCH][20];
  }
    

  DetPSbit = new Short_t[DetectorNCH];
  DetPSbit_rearranged = new Short_t[DetectorNCH];
  
  detnumber = 0;
  detmodid  = new Int_t[DetectorNCH];
  if( is125MHz ){
    detene      = new Float_t[DetectorNCH];
    detintadc   = new Float_t[DetectorNCH];
    dettime     = new Float_t[DetectorNCH];
    detptime    = new Float_t[DetectorNCH];
    detfalltime = new Float_t[DetectorNCH];
    detprodder2 = new Float_t[DetectorNCH];
    detsmoothness = new Float_t[DetectorNCH];
    detwfmcorrectnumber = new short[DetectorNCH];
  }else{
    detnhit500  = new Short_t[DetectorNCH];
    detene500   = new Float_t[DetectorNCH][20];
    dettime500  = new Float_t[DetectorNCH][20];
    detareaR500 = new Float_t[DetectorNCH][20];
  }
    
  detareaR      = new Float_t[DetectorNCH];

  DetModuleNumber  = 0;
  if( is125MHz ){
    DetModuleModID      = new Int_t[DetectorNMod];
    DetModuleEne        = new Float_t[DetectorNMod];
    DetModuleHitTime    = new Float_t[DetectorNMod];
    DetModuleHitZ       = new Float_t[DetectorNMod];
    DetModuleSmoothness = new Float_t[DetectorNMod];
    DetModuleProdDer2   = new Float_t[DetectorNMod];
	DetModuleWfmCorrectNumber = new short[DetectorNMod];
	DetSuppWfm125       = new Float_t[DetectorNCH][MTBP::nSample125];
	DetSuppWfm125S      = new short[DetectorNCH][MTBP::nSample125];
	DetModuleAreaR      = new Float_t[DetectorNCH];
	
  }else{
    DetModuleModID      = new Int_t[DetectorNCH];
    DetModulenHits500   = new Short_t[DetectorNCH];
    DetModuleEne500     = new Float_t[DetectorNCH][20];
    DetModuleHitTime500 = new Float_t[DetectorNCH][20];
	DetModuleAreaR500   = new Float_t[DetectorNCH][20];

	DetSuppWfm500       = new Float_t[DetectorNCH][MTBP::nSample500];
	DetSuppWfm500S      = new short[DetectorNCH][MTBP::nSample500];
  }
    
	
  DetSuppWfmModID = new Int_t[DetectorNCH];
  DetSuppWfmNumber = 0;

  /*
  DetTrueNumber = 0;
  DetTrueModID  = new Int_t  [DetectorNCH];
  DetTrueEne    = new Float_t[DetectorNCH];
  DetTrueTime   = new Float_t[DetectorNCH];
  */
  
  //// for IB wfm MC ////
  AccidentalDetECalibConst = new Float_t [DetectorNCH];
  AccidentalDetTCalibConst = new Float_t [DetectorNCH]; 

  ///// DetectorID in EnergyCorrector is given from MTBP.
  ///// That in MTDstDetector is, on the other hand, given from CFDP.
  ///// DetectorID matching is done below.
  Int_t MTDetectorID = -1;
  for(Int_t i = 0 ; i < MTBP::nDetectors ; ++i){
    if( strcmp( MTBP::detectorName[i].c_str(), DetectorName ) == 0 )
      MTDetectorID = i;
  }
  ECorrector  = new EnergyCorrector( MTDetectorID, RunID );
  TSCorrector = new TimingShiftCorrector( MTDetectorID, RunID);

  /// FTT suppressed ///
  m_outFTTNumber = 0;
  m_outFTTModId = new Int_t   [DetectorNCH];
  m_outFTTChisq = new Float_t [DetectorNCH];
  m_outFTTTime  = new Float_t [DetectorNCH];

  if(detID==CFDP::CC04||detID==CFDP::CC05||detID==CFDP::CC06) ModIDFromChannel = new Int_t[66];
  else if(detID==CFDP::IBCV||detID==CFDP::IB||detID==CFDP::BCV) ModIDFromChannel = new Int_t[132];
  else if(detID==CFDP::CBAR) ModIDFromChannel = new Int_t[164];
  else if(detID==CFDP::CV) ModIDFromChannel = new Int_t[188];
  else if(detID==CFDP::BHPV) ModIDFromChannel = new Int_t[42];
  else ModIDFromChannel = new Int_t[DetectorNCH];

  NumFromModID = new Int_t[4096];
  for(int i=0;i<4096;i++) NumFromModID[i] = -1;

  SetNumFromModID();
  SetModIDToChannel();  
  
  if(DetectorID==MTBP::NCC){
	  Wfm_ProdDer2threshold = -180;
	  Wfm_Smoothnessthreshold = 40;
  }else if(DetectorID==MTBP::FBAR){
	  Wfm_ProdDer2threshold = -180;
	  Wfm_Smoothnessthreshold = 40;
  }else if(DetectorID==MTBP::MBCV){
	  Wfm_ProdDer2threshold = -190;
	  Wfm_Smoothnessthreshold = 30;
  }else if(DetectorID==MTBP::DCV){
	  Wfm_ProdDer2threshold = -190;
	  Wfm_Smoothnessthreshold = 45;
  }else if(DetectorID==MTBP::UCVLG){
	  Wfm_ProdDer2threshold = -190;
	  Wfm_Smoothnessthreshold = 45;
  }else{
	  Wfm_ProdDer2threshold = -200;
	  Wfm_Smoothnessthreshold = 40;
  }
  
  Initialize();
}

MTDstDetector::~MTDstDetector()
{
  delete[] DetModID;
  delete[] DetFTTChisq;
  delete[] DetFTTTime;
  if( is125MHz ){
    delete[] DetIntADC;
    delete[] DetEne;
    delete[] DetTime;
    delete[] DetPTime;
    delete[] DetFallTime;
    delete[] DetSmoothness;
    delete[] DetProdDer2;
    delete[] ClockShift;
	delete[] DetAreaR;
	delete[] DetWfmCorrectNumber;
  }else{
    delete[] DetNHit500;
    delete[] DetEne500;
    delete[] DetTime500;
	delete[] DetAreaR500;
  }

  delete[] detmodid;
  if( is125MHz ){
    delete[] detene;
    delete[] detintadc;
    delete[] dettime;
    delete[] detptime;
    delete[] detfalltime;
    delete[] detsmoothness;
    delete[] detprodder2;
	delete[] detareaR;
	delete[] detwfmcorrectnumber;
  }else{
    delete[] detnhit500;
    delete[] detene500;
    delete[] dettime500;
	delete[] detareaR500;
  }

    
  delete[] DetModuleModID;
  if( is125MHz ){
    delete[] DetModuleEne;
    delete[] DetModuleHitTime;
    delete[] DetModuleHitZ;
    delete[] DetModuleSmoothness;
    delete[] DetModuleProdDer2;
	delete[] DetSuppWfm125;
	delete[] DetSuppWfm125S;
	delete[] DetModuleAreaR;
	delete[] DetModuleWfmCorrectNumber;
  }else{
    delete[] DetModulenHits500;
    delete[] DetModuleEne500;
    delete[] DetModuleHitTime500;
	delete[] DetSuppWfm500;
	delete[] DetSuppWfm500S;
	delete[] DetModuleAreaR500;
  }

	
  delete [] DetSuppWfmModID;

  delete[] AccidentalDetECalibConst;
  delete[] AccidentalDetTCalibConst;
  
  delete ECorrector;
  delete TSCorrector;

  delete [] m_outFTTModId;
  delete [] m_outFTTChisq;
  delete [] m_outFTTTime;

  delete [] NumFromModID;
  delete [] ModIDFromChannel;

}

void MTDstDetector::Apply_PS(int option){

	detnumber = DetNumber;

	// once copy
	for( int ich=0; ich<detnumber; ich++ ){
		detmodid[ich] = DetModID[ich];
		if(is125MHz){
			detintadc[ich]   = DetIntADC[ich];
			detene[ich]      = DetEne[ich];
			dettime[ich]     = DetTime[ich];
			detptime[ich]    = DetPTime[ich];
			detfalltime[ich] = DetFallTime[ich];
			detprodder2[ich] = DetProdDer2[ich];
			detsmoothness[ich] = DetSmoothness[ich];
			detareaR[ich]      = DetAreaR[ich];
			detwfmcorrectnumber[ich] = DetWfmCorrectNumber[ich];
		}else{
			detnhit500[ich]  = DetNHit500[ich];
			for( int ihit=0; ihit<maxhit; ihit++){
				detene500[ich][ihit]  = DetEne500[ich][ihit];
				dettime500[ich][ihit] = DetTime500[ich][ihit];
				detareaR500[ich][ihit] = DetAreaR500[ich][ihit];
			}
		}
    }
 
   // reset	
	DetNumber     = 0;
	for( int ich=0; ich<DetectorNCH; ich++ ){
		DetModID[ich]    = 0;
		if( is125MHz ){
			DetIntADC[ich]   = -9999;
			DetEne[ich]      = -9999;
			DetTime[ich]     = -9999;
			DetPTime[ich]    = -9999;
			DetFallTime[ich] = -9999;
			DetSmoothness[ich] = 0;
			DetProdDer2[ich] = 0;
			DetAreaR[ich] = 0;
			DetWfmCorrectNumber[ich] = 0;
		}else{
			DetNHit500[ich]  = 0;
			for( int ihit=0; ihit<maxhit; ihit++){      
				DetEne500[ich][ihit]  = -9999;
				DetTime500[ich][ihit] = -9999;
				DetAreaR500[ich][ihit] = -9999;
		  	}
	   	}
	}


	// refill
	for( int ich=0; ich<detnumber; ich++ ){
		int modid = detmodid[ich];
		int index = ModID2Index(DetectorID, modid);
	
		//std::cout << "dID=" << DetectorID << "\t" << modid << "\t" << index << "\t" << DetPSbit[index] << "\n";	

		if(ReArrangeArray(DetectorID)){
			if(!((option>>DetectorID)&0x01)){
				if(DetPSbit[index]==0) continue;
			}
		}else{

			if(!((option>>DetectorID)&0x01)){
				if(DetPSbit[index]==0){

					if( is125MHz ){
						detintadc[ich] = -9999;
						detene[ich]    = -9999;
						dettime[ich]   = -9999;
						detptime[ich]  = -9999;
						detfalltime[ich] = -9999;
						detprodder2[ich] = 0;
						detsmoothness[ich] = 0;
						detareaR[ich] = -9999;
						detwfmcorrectnumber[ich] = -1;
					}else{
						detnhit500[ich] = 0;
						for( int ihit=0; ihit<maxhit; ihit++){
							detene500[ich][ihit] = -9999;
							dettime500[ich][ihit] = -9999;
							detareaR500[ich][ihit] = -9999;
		  				}
	   				}
				}
			}
		}

		int c = DetNumber;
		DetModID[c] = detmodid[ich];
		if( is125MHz ){
			DetIntADC[c]   = detintadc[ich];
			DetEne[c]      = detene[ich];
			DetTime[c]     = dettime[ich];
			DetPTime[c]    = detptime[ich];
			DetFallTime[c] = detfalltime[ich];
			DetSmoothness[c] = detsmoothness[ich];
			DetProdDer2[c] = detprodder2[ich];
			DetAreaR[c] = detareaR[ich];
			DetWfmCorrectNumber[c] = detwfmcorrectnumber[ich];
		}else{
			DetNHit500[c]  = detnhit500[ich];
			for( int ihit=0; ihit<maxhit; ihit++){
				DetEne500[c][ihit]  = detene500[ich][ihit];
				DetTime500[c][ihit] = dettime500[ich][ihit];
				DetAreaR500[c][ihit] = detareaR500[ich][ihit];
		  	}
	   	}
		DetNumber++;
	}
}


void MTDstDetector::FillPSBit(){

	for(int c=0;c<DetNumber;c++){
		int modid = DetModID[c];
		int index = ModID2Index(DetectorID, modid);
		DetPSbit_rearranged[c] = DetPSbit[index];
	}

}

void MTDstDetector::SetNodeFileID( const Int_t nodeid, const Int_t fileid )
{
  ECorrector->SetNodeFileID(nodeid,fileid);
  TSCorrector->SetNodeFileID(nodeid,fileid);
}

void MTDstDetector::Initialize()
{
  
	
	
  Int_t maxhit = 20;

  Wfm_corrupted = 0;
  DetNumber     = 0;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    DetModID[ich]    = 0;
    DetFTTChisq[ich] = -5.;
    DetFTTChisq[ich] = -1.;
	DetPSbit[ich] = 0;
	DetPSbit_rearranged[ich] = 0;
    if( is125MHz ){
      DetIntADC[ich]   = 0;
      DetEne[ich]      = 0;
      DetTime[ich]     = 0;
      DetPTime[ich]    = 0;
      DetFallTime[ich] = 0;
      DetSmoothness[ich] = 0;
      DetProdDer2[ich] = 0;
	  DetAreaR[ich] = 0;
	  DetWfmCorrectNumber[ich] = -1;
    }else{
      DetNHit500[ich]  = 0;
      for( int ihit=0; ihit<maxhit; ihit++){      
	DetEne500[ich][ihit]  = 0;
	DetTime500[ich][ihit] = 0;
	DetAreaR500[ich][ihit]  = 0;
      }
    }
      
  }
  
  detnumber = DetectorNCH;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    detmodid[ich]    = 0;
    if( is125MHz ){
      detene[ich]      = 0;
      detintadc[ich]      = 0;
      dettime[ich]     = 0;
      detptime[ich]    = 0;
      detfalltime[ich] = 0;
      detsmoothness[ich] = 0;
      detprodder2[ich] = 0;
	  detareaR[ich] = 0;
	  detwfmcorrectnumber[ich] = -1;
    }else{
      detnhit500[ich]  = 0;
      for( int ihit=0; ihit<maxhit; ihit++){      
	detene500[ich][ihit]  = 0;
	dettime500[ich][ihit] = 0;
	detareaR500[ich][ihit] = 0;
      }
    }
      
  }
  
  DetModuleNumber = 0;
  if( is125MHz ){
    for( int ich=0; ich<DetectorNMod; ich++ ){
      DetModuleModID[ich]   = -1;
      DetModuleEne[ich]     = 0;
      DetModuleHitTime[ich] = 0;
      DetModuleHitZ[ich]    = 0;
      DetModuleSmoothness[ich]    = 0;
      DetModuleProdDer2[ich]    = 0;
      DetModuleAreaR[ich]    = 0;
      DetModuleWfmCorrectNumber[ich]    = -1;
    }
  }else{
    for( int ich=0; ich<DetectorNCH; ich++ ){
      DetModuleModID[ich]   = -1;
      DetModulenHits500[ich]  = 0;
      for( int ihit=0; ihit<maxhit; ihit++){
	DetModuleEne500[ich][ihit]     = 0;
	DetModuleHitTime500[ich][ihit] = 0;
	DetModuleAreaR500[ich][ihit]   = 0;
      }
    }
  }

  m_outFTTNumber = 0;
  for( int ich=0; ich<DetectorNCH; ich++ ){
     m_outFTTModId[ich] = -1;
     m_outFTTChisq[ich] = -5.;
  }
 
}

void MTDstDetector::SetBranchAddress( TTree* tr )
{
  
  DstTree = tr;
  DstTree->SetBranchAddress(Form("%sNumber",     DetectorName), &DetNumber);
  DstTree->SetBranchAddress(Form("%sModID",      DetectorName),  DetModID);
  
  if(DstTree->GetBranch(Form("%sPSBit",    DetectorName))){
  	DstTree->SetBranchAddress(Form("%sPSBit",    DetectorName),  DetPSbit);
  }
  
  if( is125MHz ){
    if( DstTree->GetBranch(Form("%sIntegratedADC",DetectorName)) )
      DstTree->SetBranchAddress(Form("%sIntegratedADC", DetectorName),  DetIntADC);
    DstTree->SetBranchAddress(Form("%sEne",           DetectorName),  DetEne);
    DstTree->SetBranchAddress(Form("%sTime",          DetectorName),  DetTime);
    DstTree->SetBranchAddress(Form("%sPTime",         DetectorName),  DetPTime);
    if( DstTree->GetBranch(Form("%sFallTime",DetectorName)) )
      DstTree->SetBranchAddress(Form("%sFallTime",    DetectorName),  DetFallTime);

	if( DstTree->GetBranch(Form("%sSmoothness",DetectorName))!=0 ){
      DstTree->SetBranchAddress(Form("%sSmoothness",    DetectorName),  DetSmoothness);
	}
    if( DstTree->GetBranch(Form("%sProdDer2",DetectorName))!=0 )
      DstTree->SetBranchAddress(Form("%sProdDer2",    DetectorName),  DetProdDer2);

	if( DstTree->GetBranch(Form("%sWfmCorrectNumber",DetectorName))!=0 ){
      DstTree->SetBranchAddress(Form("%sWfmCorrectNumber",    DetectorName),  DetWfmCorrectNumber);
	}

    if( DstTree->GetBranch(Form("%sWfm",DetectorName))!=0 ){
		DstTree->SetBranchAddress(Form( "%sWfmNumber", DetectorName ), &DetSuppWfmNumber);
		DstTree->SetBranchAddress(Form( "%sWfmModID",  DetectorName ),  DetSuppWfmModID);
		if(RunID>=0)
			DstTree->SetBranchAddress(Form( "%sWfm",  DetectorName ),  DetSuppWfm125S);
		else
			DstTree->SetBranchAddress(Form( "%sWfm",  DetectorName ),  DetSuppWfm125);
	}

  
	if( DstTree->GetBranch(Form("%sAreaR",DetectorName))!=0 ){
  	  DstTree->SetBranchAddress(Form("%sAreaR",    DetectorName),  DetAreaR);
	}


  }else{
    DstTree->SetBranchAddress(Form("%snHits",    DetectorName ), DetNHit500 );
    DstTree->SetBranchAddress(Form("%sEne",      DetectorName ), DetEne500 );
    DstTree->SetBranchAddress(Form("%sTime",     DetectorName ), DetTime500 );

	if( DstTree->GetBranch(Form("%sWfm",DetectorName))!=0 ){
		DstTree->SetBranchAddress(Form( "%sWfmNumber", DetectorName ), &DetSuppWfmNumber);
		DstTree->SetBranchAddress(Form( "%sWfmModID",  DetectorName ),  DetSuppWfmModID);
		if(RunID>=0)
			DstTree->SetBranchAddress(Form( "%sWfm",  DetectorName ),  DetSuppWfm500S);
		else
			DstTree->SetBranchAddress(Form( "%sWfm",  DetectorName ),  DetSuppWfm500);
	}


	
	if( DstTree->GetBranch(Form("%sAreaR",DetectorName))!=0 ){
		DstTree->SetBranchAddress(Form("%sAreaR",     DetectorName ), DetAreaR500 );
	}

  }

  if( DstTree->GetBranch(Form("%sFTTChisq",DetectorName) ) ){
     m_isGetFTTBranch = true;
     DstTree->SetBranchAddress(Form("%sFTTChisq",DetectorName), DetFTTChisq );
     DstTree->SetBranchAddress(Form("%sFTTTime",DetectorName), DetFTTTime );
  }

	

}


void MTDstDetector::DstBranch( TTree* tr )
{
  ClusterTree = tr;
  ClusterTree->Branch(Form("%sNumber",     DetectorName), &DetNumber ,  Form("%sNumber/I",             DetectorName));
  ClusterTree->Branch(Form("%sModID",      DetectorName),  DetModID ,   Form("%sModID[%sNumber]/I",    DetectorName, DetectorName));
  if( is125MHz ){
 
    if( DstTree->GetBranch(Form("%sIntegratedADC",DetectorName)) )
	  ClusterTree->Branch(Form("%sIntegratedADC", DetectorName),  DetIntADC,   Form("%sIntegratedADC[%sNumber]/F", DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sEne",           DetectorName),  DetEne,      Form("%sEne[%sNumber]/F",           DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sTime",          DetectorName),  DetTime,     Form("%sTime[%sNumber]/F",          DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sPTime",         DetectorName),  DetPTime,    Form("%sPTime[%sNumber]/F",         DetectorName, DetectorName));
    
    if( DstTree->GetBranch(Form("%sFallTime",DetectorName)) )
		ClusterTree->Branch(Form("%sFallTime",      DetectorName),  DetFallTime, Form("%sFallTime[%sNumber]/F",      DetectorName, DetectorName));
	if( DstTree->GetBranch(Form("%sSmoothness",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sSmoothness",    DetectorName),  DetSmoothness, Form("%sSmoothness[%sNumber]/F",  DetectorName, DetectorName));
    if( DstTree->GetBranch(Form("%sProdDer2",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sProdDer2",      DetectorName),  DetProdDer2, Form("%sProdDer2[%sNumber]/F",      DetectorName, DetectorName));
    if( DstTree->GetBranch(Form("%sWfmCorrectNumber",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sWfmCorrectNumber",      DetectorName),  DetWfmCorrectNumber, Form("%sWfmCorrectNumber[%sNumber]/S", DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sWfmCorrupted",  DetectorName),  &Wfm_corrupted, Form("%sWfmCorrupted/I",        DetectorName));

	if(DstTree->GetBranch( Form( "%sWfm",   DetectorName ))){
	  ClusterTree->Branch( Form( "%sWfmNumber", DetectorName ), &DetSuppWfmNumber, Form( "%sWfmNumber/I",  DetectorName ));
	  ClusterTree->Branch( Form( "%sWfmModID", DetectorName ),   DetSuppWfmModID,  Form( "%sWfmModID[%sWfmNumber]/I",  DetectorName, DetectorName ));
	  ClusterTree->Branch( Form( "%sWfm",   DetectorName ),      DetSuppWfm125,    Form( "%sWfm[%sWfmNumber][%d]", DetectorName, DetectorName, MTBP::nSample125 ));
	}

  
	if( DstTree->GetBranch(Form("%sAreaR",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sAreaR",    DetectorName),  DetAreaR, Form("%sAreaR[%sNumber]/F",  DetectorName, DetectorName));


  }else{
    ClusterTree->Branch(Form("%snHits",    DetectorName ), DetNHit500,  Form("%snHits[%sNumber]/S",    DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sEne",      DetectorName ), DetEne500,   Form("%sEne[%sNumber][20]/F",  DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sTime",     DetectorName ), DetTime500,  Form("%sTime[%sNumber][20]/F", DetectorName, DetectorName));

	if(DstTree->GetBranch( Form( "%sWfm",   DetectorName ))){
	  ClusterTree->Branch( Form( "%sWfmNumber", DetectorName ), &DetSuppWfmNumber, Form( "%sWfmNumber/I",  DetectorName ));
	  ClusterTree->Branch( Form( "%sWfmModID", DetectorName ),   DetSuppWfmModID,  Form( "%sWfmModID[%sWfmNumber]/I",  DetectorName, DetectorName ));
	  ClusterTree->Branch( Form( "%sWfm",   DetectorName ),      DetSuppWfm500,    Form( "%sWfm[%sWfmNumber][%d]", DetectorName, DetectorName, MTBP::nSample500 ));
	}

	if( DstTree->GetBranch(Form("%sAreaR",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sAreaR", DetectorName ), DetAreaR500,   Form("%sAreaR[%sNumber][20]/F",  DetectorName, DetectorName));
  }
  
	
  if(DstTree->GetBranch(Form("%sPSBit",    DetectorName)))
	  ClusterTree->Branch(Form("%sPSBit",      DetectorName),  DetPSbit_rearranged ,   Form("%sPSBit[%sNumber]/S", DetectorName, DetectorName));

}

void MTDstDetector::Branch( TTree* tr )
{
  ClusterTree = tr;
  if( is125MHz ){
    ClusterTree->Branch(Form("%sModuleNumber",  DetectorName), &DetModuleNumber ,    Form("%sModuleNumber/I",                  DetectorName));
    ClusterTree->Branch(Form("%sModuleModID",   DetectorName),  DetModuleModID ,     Form("%sModuleModID[%sModuleNumber]/I",   DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sModuleEne",     DetectorName),  DetModuleEne ,       Form("%sModuleEne[%sModuleNumber]/F",     DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sModuleHitTime", DetectorName),  DetModuleHitTime,    Form("%sModuleHitTime[%sModuleNumber]/F", DetectorName, DetectorName));
	
	if(DstTree->GetBranch(Form("%sPSBit",    DetectorName)))
		ClusterTree->Branch(Form("%sModulePSBit",   DetectorName),  DetPSbit_rearranged, Form("%sModulePSBit[%sModuleNumber]/S", DetectorName, DetectorName));
	if( DstTree->GetBranch(Form("%sSmoothness",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sModuleSmoothness", DetectorName),  DetModuleSmoothness, Form("%sModuleSmoothness[%sModuleNumber]", DetectorName, DetectorName));
    if( DstTree->GetBranch(Form("%sProdDer2",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sModuleProdDer2", DetectorName),  DetModuleProdDer2, Form("%sModuleProdDer2[%sModuleNumber]", DetectorName, DetectorName));
    if( DstTree->GetBranch(Form("%sWfmCorrectNumber",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sModuleWfmCorrectNumber", DetectorName),  DetModuleWfmCorrectNumber, Form("%sModuleWfmCorrectNumber[%sModuleNumber]/S", DetectorName, DetectorName));
    
	if( DstTree->GetBranch(Form("%sAreaR",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sModuleAreaR", DetectorName),  DetModuleAreaR, Form("%sModuleAreaR[%sModuleNumber]", DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sWfmCorrupted",  DetectorName),  &Wfm_corrupted,     Form("%sWfmCorrupted/I",        DetectorName));


	if(DstTree->GetBranch( Form( "%sWfm",   DetectorName ))){
	  ClusterTree->Branch( Form( "%sWfmNumber", DetectorName ), &DetSuppWfmNumber, Form( "%sWfmNumber/I",  DetectorName ));
	  ClusterTree->Branch( Form( "%sWfmModID", DetectorName ),   DetSuppWfmModID,  Form( "%sWfmModID[%sWfmNumber]/I",  DetectorName, DetectorName ));
	  ClusterTree->Branch( Form( "%sWfm",   DetectorName ),      DetSuppWfm125,    Form( "%sWfm[%sWfmNumber][%d]", DetectorName, DetectorName, MTBP::nSample125 ));
	}


  }else{
    ClusterTree->Branch(Form("%sNumber",        DetectorName ),&DetModuleNumber,     Form("%sNumber/I",             DetectorName));
    ClusterTree->Branch(Form("%sModID",         DetectorName ), DetModuleModID,      Form("%sModID[%sNumber]/I",    DetectorName, DetectorName));
    ClusterTree->Branch(Form("%snHits",         DetectorName ), DetModulenHits500,   Form("%snHits[%sNumber]/S",    DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sEne",           DetectorName ), DetModuleEne500,     Form("%sEne[%sNumber][20]/F",  DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sTime",          DetectorName ), DetModuleHitTime500, Form("%sTime[%sNumber][20]/F", DetectorName, DetectorName));

	if( DstTree->GetBranch(Form("%sAreaR",DetectorName))!=0 )
		ClusterTree->Branch(Form("%sAreaR",    DetectorName ), DetModuleAreaR500,     Form("%sAreaR[%sNumber][20]/F",  DetectorName, DetectorName));

	if(DstTree->GetBranch( Form( "%sWfm",   DetectorName ))){
	  ClusterTree->Branch( Form( "%sWfmNumber", DetectorName ), &DetSuppWfmNumber, Form( "%sWfmNumber/I",  DetectorName ));
	  ClusterTree->Branch( Form( "%sWfmModID", DetectorName ),   DetSuppWfmModID,  Form( "%sWfmModID[%sWfmNumber]/I",  DetectorName, DetectorName ));
	  ClusterTree->Branch( Form( "%sWfm",   DetectorName ),      DetSuppWfm500,    Form( "%sWfm[%sWfmNumber][%d]", DetectorName, DetectorName, MTBP::nSample500 ));
	}


  }

  if( DetectorID==CFDP::BCV || DetectorID==CFDP::CBAR || DetectorID==CFDP::IBCV )
    ClusterTree->Branch(Form("%sModuleHitZ", DetectorName), DetModuleHitZ, Form("%sModuleHitZ[%sModuleNumber]/F", DetectorName, DetectorName));  

}


void MTDstDetector::AddSuppFTTBranch( TTree *otree )
{
   if( m_isGetFTTBranch ){
      m_isSetFTTBranch=true;
      otree->Branch(Form("%sFTTNumber",DetectorName), &m_outFTTNumber, Form("%sFTTNumber/I",DetectorName) );
      otree->Branch(Form("%sFTTModID",DetectorName), m_outFTTModId, Form("%sFTTModID[%sFTTNumber]/I",DetectorName, DetectorName) );
      otree->Branch(Form("%sFTTChisq",DetectorName), m_outFTTChisq, Form("%sFTTChisq[%sFTTNumber]/F",DetectorName, DetectorName) );
      otree->Branch(Form("%sFTTTime",DetectorName), m_outFTTTime, Form("%sFTTTime[%sFTTNumber]/F",DetectorName, DetectorName) );     
   }
}

void MTDstDetector::FillWfm(){
	
	if(RunID<0) return;

	if( is125MHz ){
		for(Int_t i = 0 ; i < DetSuppWfmNumber ; ++i ){
			for(Int_t j = 0 ; j < MTBP::nSample125;j++)
				DetSuppWfm125[i][j] = DetSuppWfm125S[i][j];
		}
	}else{
		for(Int_t i = 0 ; i < DetSuppWfmNumber ; ++i ){
			for(Int_t j = 0 ; j < MTBP::nSample500;j++)
				DetSuppWfm500[i][j] = DetSuppWfm500S[i][j];
		}
    }
}

void MTDstDetector::ApplyEnergyCorrection()
{
  if( is125MHz ){
    for(Int_t i = 0 ; i < DetNumber ; ++i ){
      Int_t modid = DetModID[i];
      Float_t CorrectionFactor = ECorrector->GetCorrFactor(modid);
      DetEne[i] *= CorrectionFactor;
    }
  }else{
    for(Int_t i = 0 ; i < DetNumber ; ++i ){
      Int_t modid = DetModID[i];
      Float_t CorrectionFactor = ECorrector->GetCorrFactor(modid);
      for(Int_t j = 0 ; j < DetNHit500[i] ; ++j )
	DetEne500[i][j] *= CorrectionFactor;
    }
  }
}

void MTDstDetector::ApplyTimingCorrection()
{
  if( is125MHz ){
    for(Int_t i = 0 ; i < DetNumber ; ++i ){
      Int_t modid = DetModID[i];
      Float_t CorrectionValue = TSCorrector->GetCorrValue(modid,"clock");
      DetTime[i]     += CorrectionValue;
      DetPTime[i]    += CorrectionValue;
    }
  }else{
    for(Int_t i = 0 ; i < DetNumber ; ++i ){
      Int_t modid = DetModID[i];
      Float_t CorrectionValue = TSCorrector->GetCorrValue(modid,"clock");
      for(Int_t j = 0 ; j < DetNHit500[i] ; ++j )
	DetTime500[i][j] += CorrectionValue;
    }
  }  
}

void MTDstDetector::FillSuppFTTVars()
{
   if( !m_isSetFTTBranch ) return;
   m_outFTTNumber = 0;
   for( Int_t ich=0; ich<DetNumber ; ++ich )
   {
      if( DetFTTChisq[ich]>0. ){
         m_outFTTModId[ m_outFTTNumber ] = DetModID[ich];
         m_outFTTChisq[ m_outFTTNumber ] = DetFTTChisq[ich];
         m_outFTTTime [ m_outFTTNumber ] = DetFTTTime[ich];
         m_outFTTNumber++;
      }
   }   

}

void MTDstDetector::SetNumFromModID()
{
  int count = 0;
  for(int i=0; i<DetectorNCH; i++){
    NumFromModID[i] = count;
    ++count;
  } 
}

void MTDstDetector::SetModIDToChannel()
{
  for(int i=0; i<DetectorNCH; i++){
    ModIDFromChannel[i] = i;
  }
}

int MTDstDetector::ModID2Index(int detID, int ModID){

	if(detID==CFDP::CC03   ||
	   detID==CFDP::FBAR   ||
	   detID==CFDP::CSI    ||
	   detID==CFDP::BHPV   ||
	   detID==CFDP::BHGC   ||
	   detID==CFDP::DCV    ||
	   detID==CFDP::LCV    ||
	   detID==CFDP::BPCV   ||
	   detID==CFDP::MBCV   ||
	   detID==CFDP::newBHCV||
	   detID==CFDP::OEV) return ModID;


	if(detID==CFDP::CV){
		if(ModID<100)return ModID;
		else return ModID-100+96;
	}

	if(detID==CFDP::CC04){
		if(ModID<58) return ModID;
		else return ModID-60+58;
	}
	if(detID==CFDP::CC05 || detID==CFDP::CC06){
		if(ModID<54) return ModID;
		else return ModID-60+54;
	}
	if(detID==CFDP::IB   ||
	   detID==CFDP::IBCV){
		if(ModID<100) return ModID;
		else return ModID-100+32;
	}
	if(detID==CFDP::CBAR){
		if(ModID<100) return ModID;
		else return ModID-100+64;
	}
	
	if(detID==CFDP::NCC){
		if(ModID<500){
			int comid = ModID/10;
			int indid = ModID%10;
			return comid*4+indid;
		}else if(ModID<600){
			return (ModID-500)/10 + 192;
		}else{
			return (ModID-600)/10 + 200;
		}
	}

	return ModID;
}


int MTDstDetector::ReArrangeArray(int detID){
	if(detID==CFDP::CSI) return true;
	else if(detID==CFDP::NCC) return true;
	else if(detID==CFDP::CBAR) return true;
	else if(detID==CFDP::OEV)  return true;
	else if(detID==CFDP::CV)  return true;
	else if(detID==CFDP::LCV)  return true;
	else if(detID==CFDP::CC03)  return true;
	else if(detID==CFDP::CC04)  return true;
	else if(detID==CFDP::CC05)  return true;
	else if(detID==CFDP::CC06)  return true;
	else return false;
}


