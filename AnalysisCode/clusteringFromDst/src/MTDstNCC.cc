#include "clusteringFromDst/MTDstNCC.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <TMath.h>

#include <iostream>
#include <fstream>

MTDstNCC::MTDstNCC( Int_t runID, Int_t userFlag ) : MTDstVeto( CFDP::NCC, runID , userFlag)
{

  std::string NCCTimeCorrectionFile = std::string( std::getenv("E14ANA_EXTERNAL_DATA") );
  if( m_userFlag<20160101){
    NCCTimeCorrectionFile += std::string("/clusteringFromDst/data/NCC/NCCTimeCorrection_2015.dat");
  }else{
    NCCTimeCorrectionFile += std::string("/clusteringFromDst/data/NCC/NCCTimeCorrection_2016.dat");
  }

  std::ifstream ifs(NCCTimeCorrectionFile.c_str());
  {
    int modid;
    float p0,p1,p2,p3,p4;
    
    while(ifs>>modid>>p0>>p1>>p2>>p3>>p4){
      NCCCorrectionParameter[modid][0]=p0;
      NCCCorrectionParameter[modid][1]=p1;
      NCCCorrectionParameter[modid][2]=p2;
      NCCCorrectionParameter[modid][3]=p3;
      NCCCorrectionParameter[modid][4]=p4;
    }
  }
  
}

void MTDstNCC::Process2()
{
  ApplyEnergyCorrection();
  ApplyTimingCorrection();

  DetModuleNumber=0;
  for( int imod=0; imod<DetectorNMod; imod++ ){
    DetModuleModID[imod]   = 0;    
    DetModuleHitTime[imod] = 0;
    DetModuleEne[imod]     = 0;
    DetModuleSmoothness[imod] = 0;
    DetModuleProdDer2[imod] = 0;
    DetModuleAreaR[imod] = 0;
    DetModuleWfmCorrectNumber[imod] = -1;
  }


  Wfm_corrupted = 0;
  for(int imod=0; imod<DetNumber; imod++){
    if( DetEne[imod]>0 || DetectorID==CFDP::BPCV ){
      DetModuleModID[DetModuleNumber]   = DetModID[imod];
      DetModuleEne[DetModuleNumber]     = DetEne[imod];

	  DetModuleSmoothness[DetModuleNumber] = DetSmoothness[imod];
	  DetModuleProdDer2[DetModuleNumber]   = DetProdDer2[imod];
	  DetModuleAreaR[DetModuleNumber]      = DetAreaR[imod];
	  DetModuleWfmCorrectNumber[DetModuleNumber]      = DetWfmCorrectNumber[imod];
      
	  if(fabs(DetSmoothness[imod])>Wfm_Smoothnessthreshold) Wfm_corrupted++;
	  if(DetProdDer2[imod]<Wfm_ProdDer2threshold)           Wfm_corrupted += 10000;
	  
      DetModuleHitTime[DetModuleNumber] = GetNCCTime(DetPTime[imod],DetEne[imod],DetModID[imod]);
      
      ++DetModuleNumber;
    }
  }
}

Float_t MTDstNCC::GetNCCTime(float time,float ene, int modID){

  float correctval=0;
  
  if(isRealData){
    if(modID%10==0 && modID<600){
      int id=modID/10;
      
      if(ene>600) ene=600;	
      correctval=NCCCorrectionParameter[id][1]*(TMath::Max(ene,NCCCorrectionParameter[id][2])-NCCCorrectionParameter[id][2])
	+NCCCorrectionParameter[id][3]*pow(ene,2)
	+NCCCorrectionParameter[id][4]*pow(ene,3);
    }	 
  }
	  
  return time*8-correctval;  
}
