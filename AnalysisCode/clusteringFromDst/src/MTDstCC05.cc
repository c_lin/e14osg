#include "clusteringFromDst/MTDstCC05.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDstCC05::MTDstCC05( Int_t runID, Int_t userFlag ) : MTDstVeto( CFDP::CC05, runID, userFlag )
{
  SetNumFromModID();
  SetModIDToChannel();
  Wfm_ProdDer2threshold = -150;
  Wfm_Smoothnessthreshold = 35;
  Initialize();
}

void MTDstCC05::Initialize()
{
  MTDstVeto::Initialize();
  DetModuleModID[54]=60;
  DetModuleModID[55]=62;
  DetModuleModID[56]=63;
  DetModuleModID[57]=64;  
}

void MTDstCC05::Process2()
{
  ApplyEnergyCorrection();
  ApplyTimingCorrection();
  
  detnumber=DetectorNCH;
  for( int ich=0; ich<DetectorNCH; ich++ ){
    detmodid[ich]    = 0;
    detene[ich]      = 0;
    dettime[ich]     = 0;
    detptime[ich]    = 0;
    detfalltime[ich] = 0;
	detsmoothness[ich] = 0;
    detprodder2[ich] = 0;
    detareaR[ich] = 0;
    detwfmcorrectnumber[ich] = -1;
    /*
      DetTrueModID[ich] = 0;
      DetTrueEne[ich]   = 0;
      DetTrueTime[ich]  = 0;
    */
  }

  Wfm_corrupted = 0;
  for(int i=0; i<DetNumber; i++){
    int num = NumFromModID[DetModID[i]];
    detmodid[num]    = DetModID[i];
    detene[num]      = DetEne[i];
    dettime[num]     = DetTime[i]*8;  // [clock]->[ns]
    detptime[num]    = DetPTime[i]*8; // [clock]->[ns]
	detfalltime[num] = DetFallTime[i]*8;
	detsmoothness[num] = DetSmoothness[i];
    detprodder2[num] = DetProdDer2[i];
    detareaR[num]    = DetAreaR[i];
    detwfmcorrectnumber[num] = DetWfmCorrectNumber[i];
	if(fabs(DetSmoothness[i])>Wfm_Smoothnessthreshold) Wfm_corrupted++;
  	if(DetProdDer2[i]<Wfm_ProdDer2threshold)           Wfm_corrupted += 10000;
  }

  DetModuleNumber = 0;
  int crystalnum = MTBP::CC05E391NModules;
  
  for(int i=0; i<crystalnum; i++){
    if(detene[i]>0){
      DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[i]];
      DetModuleHitTime[DetModuleNumber] = detptime[i];
      DetModuleEne[DetModuleNumber]     = detene[i];
      DetModuleSmoothness[DetModuleNumber] = detsmoothness[i];
      DetModuleProdDer2[DetModuleNumber] = detprodder2[i];
      DetModuleAreaR[DetModuleNumber] = detareaR[i];
      DetModuleWfmCorrectNumber[DetModuleNumber] = detwfmcorrectnumber[i];
      ++DetModuleNumber;
    }
  }
  
  for(int ich=crystalnum; ich<DetectorNCH; ich++){  
    if(detene[ich]>0){
      
      if(ich==55) continue;
      if(ich==59) continue;
      
      if( ModIDFromChannel[detmodid[ich]]==60 || ModIDFromChannel[detmodid[ich]]==64 ){
	DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[ich]];
	DetModuleHitTime[DetModuleNumber] = ( detptime[ich] + detptime[ich+1] ) / 2.;
	DetModuleEne[DetModuleNumber]     = ( detene[ich]   + detene[ich+1] )   / 2.;
	DetModuleSmoothness[DetModuleNumber] = ((fabs(detsmoothness[ich])>fabs(detsmoothness[ich+1])) 
			? detsmoothness[ich] : detsmoothness[ich+1] );
  	DetModuleProdDer2[DetModuleNumber] = ((detprodder2[ich]<detprodder2[ich+1]) 
			? detprodder2[ich] : detprodder2[ich+1] );
  	DetModuleAreaR[DetModuleNumber] = ((detareaR[ich]<detareaR[ich+1]) 
			? detareaR[ich] : detareaR[ich+1] );

	DetModuleWfmCorrectNumber[DetModuleNumber] = -1; 
	int modcorrectnumber = 0;
	if(detwfmcorrectnumber[ich  ]>=0) modcorrectnumber += detwfmcorrectnumber[ich  ];
	if(detwfmcorrectnumber[ich+1]>=0) modcorrectnumber += detwfmcorrectnumber[ich+1]*100;
	if(detwfmcorrectnumber[ich]>=0||detwfmcorrectnumber[ich+1]>=0) DetModuleWfmCorrectNumber[DetModuleNumber] = modcorrectnumber;



	++ich;
      }else{
	DetModuleModID[DetModuleNumber]   = ModIDFromChannel[detmodid[ich]];
	DetModuleHitTime[DetModuleNumber] = detptime[ich];
	DetModuleEne[DetModuleNumber]     = detene[ich];	
	DetModuleSmoothness[DetModuleNumber] = detsmoothness[ich];
  	DetModuleProdDer2[DetModuleNumber] = detprodder2[ich];
  	DetModuleAreaR[DetModuleNumber] = detareaR[ich];
  	DetModuleWfmCorrectNumber[DetModuleNumber] = detwfmcorrectnumber[ich];
      }
      ++DetModuleNumber;
    }
  }
}

void MTDstCC05::SetNumFromModID()
{
  int count = 0;
  for(int i=0; i<54; i++){
    NumFromModID[i] = count;
    ++count;
  }
  for(int i=54; i<60; i++){
    NumFromModID[i] = -1;
  }  
  for(int i=60; i<66; i++){
    NumFromModID[i] = count;
    ++count;
  }
}

void MTDstCC05::SetModIDToChannel()
{
  for(int i=0; i<54; i++)  ModIDFromChannel[i] = i;
  for(int i=54; i<60; i++) ModIDFromChannel[i] = -1;
  ModIDFromChannel[60]=60;
  ModIDFromChannel[61]=60;
  ModIDFromChannel[62]=62;
  ModIDFromChannel[63]=63;
  ModIDFromChannel[64]=64;
  ModIDFromChannel[65]=64;
}

