#include "clusteringFromDst/MTDstUCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDstUCV::MTDstUCV( Int_t runID, Int_t userFlag ) : MTDst500MHzDetector( CFDP::UCV, runID, userFlag )
{
  SetNumFromModID();
  SetModIDToChannel();
  DetFallTime500  = new Float_t[DetectorNCH][20];
  DetPeakTime500  = new Float_t[DetectorNCH][20];
  Initialize();
}

MTDstUCV::~MTDstUCV(){
    delete[] DetFallTime500;
    delete[] DetPeakTime500;
}

void MTDstUCV::SetBranchAddress(TTree* tree){
	MTDstDetector::SetBranchAddress(tree);
    DstTree->SetBranchAddress(Form("%sFallTime",     DetectorName ), DetFallTime500 );
    DstTree->SetBranchAddress(Form("%sPeakTime",     DetectorName ), DetPeakTime500 );
}

void MTDstUCV::Branch( TTree* tree ){
	MTDstDetector::Branch(tree);
    ClusterTree->Branch(Form("%sFallTime",  DetectorName ), DetFallTime500, Form("%sFallTime[%sNumber][20]/F", DetectorName, DetectorName));
    ClusterTree->Branch(Form("%sPeakTime",  DetectorName ), DetPeakTime500, Form("%sPeakTime[%sNumber][20]/F", DetectorName, DetectorName));
}



