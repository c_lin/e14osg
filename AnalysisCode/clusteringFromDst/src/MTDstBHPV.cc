#include "clusteringFromDst/MTDstBHPV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTDstBHPV::MTDstBHPV( Int_t runID, Int_t userFlag ) : MTDst500MHzDetector( CFDP::BHPV, runID, userFlag )
{
  SetNumFromModID();
  SetModIDToChannel();
  Initialize();
}

void MTDstBHPV::SetNumFromModID(){
  int count = 0;
  for(int i=0; i<32; i++){
    NumFromModID[i] = count;
    ++count;
  }

  for(int i=32; i<40; i++){
    NumFromModID[i] = -1;
  }

  NumFromModID[40]=32;
  NumFromModID[41]=33;

}

void MTDstBHPV::SetModIDToChannel(){

  for(int i=0; i<32; i++){
    ModIDFromChannel[i]=i;
  }

  for(int i=32; i<40; i++){
    ModIDFromChannel[i]=-1;
  }

  ModIDFromChannel[32]=40;
  ModIDFromChannel[33]=41;

}
