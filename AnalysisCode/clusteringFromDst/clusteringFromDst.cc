#include "clusteringFromDst.h"

bool user_rec(std::list<Gamma> const &glist, std::list<Pi0>& piList);

struct csiHitInfo{
  int    id;
  double timeChisq;
  static bool largerTimeChisq( const csiHitInfo& chi0, const csiHitInfo& chi1 ){ return chi0.timeChisq > chi1.timeChisq;}
};

bool sort_hitlist(const std::pair<int, double>& left, const std::pair<int, double>& right){
	if(left.second==right.second) return left.first < right.first;
	else return left.second > right.second;	
}

Int_t clusteringFromDst( const Char_t* inputFile, const Char_t* outputFile,
			 Int_t RunID, Int_t NodeID, Int_t FileID,
			 const Char_t *GoodRunSpillFile, const Char_t* CorrectionDataDir, const Int_t BlindFlag, Int_t userFlag,
			 int argc, char** argv, int option );

double getTimeSigma( double e )
{
  double sigmat = 0; //[ns]
  for( int index = 0 ; index < 3 ; ++index )
    sigmat += pow( MTBP::CSITimeResolutionCoeff[index], 2) / pow( e, 2-index );
  return sqrt( sigmat );
}

void FillWeightedAverageTimeInClusters( std::list<Cluster> &clist )
{
  for( std::list<Cluster>::iterator it=clist.begin(); it!=clist.end(); it++ ){
    std::vector<double> clusterEVec    = it->clusterEVec();
    std::vector<double> clusterTimeVec = it->clusterTimeVec();
    std::vector<double> clusterTimeWeightVec;
    for( unsigned int icsi=0; icsi<clusterTimeVec.size(); icsi++ ){
      if( clusterEVec[icsi] > 0 )
	clusterTimeWeightVec.push_back( pow( getTimeSigma( clusterEVec[icsi]), -2) );
      else
	clusterTimeWeightVec.push_back( 0 );
    }
    it->setTime( MTFUNC::WeightedAverage( clusterTimeVec.size(), &(clusterTimeVec[0]), &(clusterTimeWeightVec[0]) ) );
  }
}

double GetEventStartTime( std::list<Pi0> & piList, double* vertexTime, const double CSIZPosition )
{
  double gammaHitTime[2] = {0.0};
  double gammaHitX[2]    = {0.0};
  double gammaHitY[2]    = {0.0};
  double gammaE[2]       = {0.0};
  double gammaTWeight[2]  = {0.0};

  for( int iclus = 0 ; iclus < 2 ; ++iclus ){
    Gamma const &gamma = (iclus==0) ? piList.front().g1() : piList.front().g2();
    gammaHitTime[ iclus ] = gamma.t();
    gammaHitX[ iclus ]    = gamma.pos().x();
    gammaHitY[ iclus ]    = gamma.pos().y();
    gammaE[ iclus ]       = gamma.e();
    gammaTWeight[iclus ]  = pow( MTFUNC::TResolutionOfCluster( gamma.e() ), -2 );
  }
  double vertexZ = piList.front().recZ();
  for( int igamma = 0 ; igamma < 2 ; ++igamma )
    vertexTime[igamma] = gammaHitTime[igamma] - sqrt( pow( CSIZPosition - vertexZ, 2 ) + 
						      pow( gammaHitX[igamma], 2)             + 
						      pow( gammaHitY[igamma], 2)               ) / (TMath::C()/1E6);
  return MTFUNC::WeightedAverage( 2, vertexTime, gammaTWeight );
}

Int_t main(Int_t argc,char** argv)
{
  std::cout << "clusteringFromDst starts." << std::endl;
  if( !( argc==9 || argc==4 || argc==10 || argc==5 || argc==11 || argc==6 ) ){
    std::cout << "Argument error!" << std::endl;
    std::cout << "for Data : " << std::endl;
    std::cout << "\t" << argv[0] << " DstFileName ClusteringFileName RunID NodeID FileID" 
	      <<                    " GoodRunSpillFileName CorrectionDataDir BlindFlag(1:blind(physics), 0:unblind(Z0,etc..) ) userFlag(default: 20151101) " 
	      <<                    " Option "
	      << std::endl;
    std::cout << "for MC : " << std::endl;
    std::cout << "\t" << argv[0] << " DstFileName ClusteringFileName FileID userFlag(default: 20151101)"
	      <<                    " Option "
	      << std::endl;

    return -1;
  }
 //SetForceOldCsiZposition(defalut(false) Z=6168, if true Z=6148)

  Bool_t isData = ( argc==9 || argc==10 || argc==11 ) ? true : false;

  const Char_t *InputFileName  = argv[1];
  const Char_t *OutputFileName = argv[2];
  Int_t   RunID          = ( isData )? atoi(argv[3]) : -1;// Data : MC
  Int_t   NodeID         = ( isData )? atoi(argv[4]) : -1;// Data : MC
  Int_t   FileID         = ( isData )? atoi(argv[5]) : atoi(argv[3]);// Data : MC
  const Char_t *GoodRunSpillFileName = ( isData )? argv[6] : "";// Data : MC
  const Char_t *CorrectionDataDir    = ( isData )? argv[7] : "";// Data : MC
  const Int_t   BlindFlag            = ( isData )? atoi(argv[8]) : 0;// Data : MC
  const Int_t   userFlag             = (argc==10||argc==11)? atoi(argv[9]) : ( (argc==5||argc==6)? atoi(argv[4]) : 20151101 );// Data : MC : default(20151101)
  Int_t   option = 0;
  if(argc==11) option = atoi(argv[10]);
  if(argc==6)  option = atoi(argv[5]);

  if( userFlag<20150101 || 21120903<userFlag ){
    std::cout<<"Unexpected userFlag is used! The userFlag format is yyyymmdd. Used userFlag: "<<userFlag<<std::endl;
    return -1;
  }

  Int_t result = clusteringFromDst( InputFileName, OutputFileName, RunID, NodeID, FileID,
				    GoodRunSpillFileName, CorrectionDataDir, BlindFlag, userFlag,
				    argc, argv, option );
  
  
  
  std::cout << "clusteringFromDst finished." << std::endl;
  std::cout << std::endl;
  return result;
}


Int_t clusteringFromDst( const Char_t* inputFile, const Char_t* outputFile, 
			 Int_t RunID, Int_t NodeID, Int_t FileID,
			 const Char_t *GoodRunSpillFile, const Char_t *CorrectionDataDir, const Int_t BlindFlag, const Int_t userFlag,
			 int argc, char** argv, int option )
{
  std::cout << "RunID  : " << RunID  << std::endl;
  std::cout << "NodeID : " << NodeID << std::endl;
  std::cout << "FileID : " << FileID << std::endl;
  std::cout << "userFlag : " << userFlag << std::endl;
  bool isRealData = true; // Run:true, Sim:false


  float calibT0[4096];
  float GainMean[4096];
  float MeVCoef[4096];
  
  TFile *dst_tree_calib = new TFile( inputFile );
  TTree* calibtree = (TTree*) dst_tree_calib->Get("calib");
  TTree* calibtreeout = nullptr;
  if(calibtree!=nullptr){
	calibtree->SetMakeClass(1);

	calibtree->SetBranchAddress(Form("CSI.T0Mean[4096]"), calibT0);
	calibtree->SetBranchAddress(Form("CSI.GainMean[4096]"), GainMean);
	calibtree->SetBranchAddress(Form("CSI.MeVCoeff[4096]"), MeVCoef);
	calibtree->GetEntry(0);
	delete calibtree;
	dst_tree_calib->Close();

	calibtreeout = new TTree("calibCSI", "calibCSI");
	calibtreeout->Branch("CSIT0", calibT0,        "CSIT0[4096]");
	calibtreeout->Branch("CSIGainMean", GainMean, "CSIGainMean[4096]");
	calibtreeout->Branch("CSIMeVCoef",  MeVCoef,  "CSIMeVCoef[4096]");
	calibtreeout->Fill();
  }

  NsUTC::UTCconfig* utcconf = nullptr;
 
  if(userFlag>=20190101) utcconf = NsUTC::UTCconfig::GetInstance(RunID);
  
  // set input file
  TChain *itr = new TChain( "T" );
  itr->Add( inputFile );
 

    
  long nEntries = itr->GetEntries();
  if( nEntries==0 ){
    std::cout << "This file has no events." << std::endl;
    return -1;
  }else{
    std::cout << "This file has " << nEntries << " events." << std::endl;
  }
  if( itr->GetBranch( "TruePID" ) ){
    std::cout << "This is simulation file." << std::endl;
    isRealData = false;
  }else{
    std::cout << "This is data file." << std::endl;
  }

			 
  Bool_t SetForceOldCsiZPosition = false;
  int useinitialdata = 0;
  int turnoffEcorr = 0;
  int turnoffTcorr = 0;
  int applyPS = 0;
  int PSdet = 0;
  if(option!=0){
  	if(!isRealData){
		std::cout << "This option can be used only for real data! option is ignored.\n";
	}else{
		if((option>>0)&0x01){
			std::cout << "For CSI and UTC, Integrated ADC values are filled instead of energy.\n";
		}
	  	if((option>>1)&0x01){
			std::cout << "For CSI and UTC, Initial times are used instead of corrected ones.\n";
		}
		useinitialdata = option&0x3;

		if((option>>2)&0x01){
			std::cout << "For CSI, energy correction is not applied.\n";
			turnoffEcorr = 1;
		}
		if((option>>3)&0x01){
			std::cout << "For CSI, timing correction is not applied.\n";
			turnoffTcorr = 1;
		}
		if((option>>4)&0x01){
			std::cout << "Apply pedestal suppression only for CSI based on PSbit.\n";
			applyPS = 2;
		}
		if((option>>5)&0x01){
			std::cout << "Apply pedestal suppression for all detectors based on PSbit.\n";
			applyPS = 1;
			PSdet = option>>6;

		  	for(int i=0;i<CFDP::nDetectors;i++){
				if(i==CFDP::BCV||i==CFDP::BPCV||i==CFDP::BHCV)continue;

				if((PSdet>>i)&0x01) std::cout << CFDP::DetName[i] << ": PS not applied\n"; 
				else std::cout << CFDP::DetName[i] << ": PS applied\n"; 
			}
			std::cout << "-----------------------------------------------------\n";

		}

		if((option>>31)&0x01){
			SetForceOldCsiZPosition = true;
			std::cout << "Force CSI position to be previous value of 6148 mm.\n";
		}

	}
  }


  // E14BasicParam
  E14BasicParamManager::E14BasicParamManager *m_E14BP
    = new E14BasicParamManager::E14BasicParamManager();
  m_E14BP->SetDetBasicParam(userFlag,"CSI");
  const double CSIZPosition    = m_E14BP->GetDetZPosition();

  ///// if you want to store Laser/LED data, you should activate the following line.
  //itr->SetBranchStatus( "TRIGGERTAG*",0 );

  MTDstTree* dst = new MTDstTree( itr ,isRealData, userFlag);  
  dst->SetBranchAddress();

  // for simulation data
  Int_t    TruePID       = 0; // mother particle ID
  Int_t    TrueDecayMode = 0;
  Double_t TrueVertexZ;
  Double_t TrueVertexTime;
  Double_t TrueMom[3];
  Double_t TruePos[3];
  Int_t    MCEventID;///// obsolete. replaced by GsimEntryID
  Int_t    GsimGenEventID;
  Int_t    GsimEntryID;
  MTGenParticle* genP = new MTGenParticle();
  std::string *AccidentalFileName = new std::string;
  Int_t    AccidentalFileID  = -1;
  Long64_t AccidentalEntryID = -1;
  Bool_t   AccidentalOverlayFlag;
  if( !isRealData ){
    itr->SetBranchAddress( "TruePID",       &TruePID );    
    itr->SetBranchAddress( "TrueDecayMode", &TrueDecayMode );
    itr->SetBranchAddress( "TrueVertexZ",   &TrueVertexZ );
    itr->SetBranchAddress( "TrueVertexTime",&TrueVertexTime );
    itr->SetBranchAddress( "TrueMom",        TrueMom );
    itr->SetBranchAddress( "TruePos",        TruePos );
    itr->SetBranchAddress( "GsimGenEventID",&GsimGenEventID);
    itr->SetBranchAddress( "GsimEntryID",   &GsimEntryID);
    itr->SetBranchAddress( "MCEventID",     &MCEventID);
    genP->SetBranchAddress( itr );
    itr->SetBranchAddress( "AccidentalFileName",   &AccidentalFileName );
    itr->SetBranchAddress( "AccidentalFileID",     &AccidentalFileID );
    itr->SetBranchAddress( "AccidentalEntryID",    &AccidentalEntryID );
    itr->SetBranchAddress( "AccidentalOverlayFlag",&AccidentalOverlayFlag );
  }

  GiriLib::GoodRunSpillManager *GRGSMan = NULL;
  CSIEnergyCorrector   *CSIECalibCorrector = NULL;
  EnergyCorrector      *CSIAddEneCorrector = NULL;///// additional correction if it exists.
  TimingShiftCorrector *CSITimingCorrector = NULL;
  if( isRealData ){
    GRGSMan = new GiriLib::GoodRunSpillManager(GoodRunSpillFile);
    GRGSMan->SetRunID(RunID);

    std::string CSIECalibCorrDataDir = std::string(CorrectionDataDir) + "/CSICalib";
    CSIECalibCorrector = new CSIEnergyCorrector( CSIECalibCorrDataDir.c_str() );
    //CSIECalibCorrector->ForceReady();/// activate this line if some correction files are not prepared
    CSIECalibCorrector->SetRunID(RunID);

    EnergyCorrector::SetDataDirName(CorrectionDataDir);
    TimingShiftCorrector::SetDataDirName(CorrectionDataDir);
    CSIAddEneCorrector = new EnergyCorrector(MTBP::CSI,RunID);
    CSITimingCorrector = new TimingShiftCorrector(MTBP::CSI,RunID);
  }
  
  
  ///// Internal cluster timing limits
  std::string dirName    = std::string( std::getenv(MTBP::EnvName.c_str()) )+"/AnalysisCode/clusteringFromDst";
  TFile*      icttf      = new TFile( Form("%s/data/InternalClusterLimit.root",dirName.c_str() ));
  TSpline3*   upperLimit = (TSpline3*)icttf->Get( "upper" );
  TSpline3*   lowerLimit = (TSpline3*)icttf->Get( "lower" );
  

  SshinoLib::SuppConfInfoManager *SuppConfInfoMan = NULL; 
  SuppConfInfoMan = new SshinoLib::SuppConfInfoManager( inputFile );
  
  ///// set output file
  TFile   *otf = new TFile( outputFile, "RECREATE" );
  
  // add version information of E14 Analysis Suite
//  E14ProdLibrary::E14ProdInfoWriter::GetInstance()->Write(itr->GetFile(), otf, argc, argv);

  // add peakfind and zero-suppression information
  SuppConfInfoMan -> WriteSuppConfInfo( otf );

  TTree   *otr = new TTree( "clusteringTree", "output from clusteringFromDst");  
  Int_t    DstEntryID = 0;
  otr->Branch( "RunID",         &RunID,          "RunID/I" );
  otr->Branch( "NodeID",        &NodeID,         "NodeID/I" );
  otr->Branch( "FileID",        &FileID,         "FileID/I" );  
  otr->Branch( "DstEntryID",    &DstEntryID,     "DstEntryID/I" );

  Int_t DetectorWFMCorruptBit; 
  otr->Branch("DetectorWFMCorruptBit", &DetectorWFMCorruptBit, "DetectorWFMCorruptBit/I");
  
  E14GNAnaDataContainer data;
  data.branchOfClusterList( otr );
  data.branchOfDigi( otr );


  const int NclsMAX = 120;
  const int NUTCmodMAX = 20;
  int ClusterUTCModNumber[NclsMAX];
  int ClusterUTCModID[NclsMAX][NUTCmodMAX];
  float ClusterUTCModTime[NclsMAX][NUTCmodMAX];
  float ClusterUTCModEne[NclsMAX][NUTCmodMAX];

  if(userFlag>20190101){

  	for(int i=0;i<NclsMAX;i++){
  	    ClusterUTCModNumber[i] = 0;
  	    for(int j=0;j<NUTCmodMAX;j++){
  	  	  ClusterUTCModID[i][j] = 0;
  	  	  ClusterUTCModTime[i][j] = 0;
  	  	  ClusterUTCModEne[i][j] = 0;
  	    }
  	}
  	
  	otr->Branch("ClusterUTCModNumber", ClusterUTCModNumber, "ClusterUTCModNumber[120]/I");
  	otr->Branch("ClusterUTCModID", ClusterUTCModID, "ClusterUTCModID[120][20]/I");
  	otr->Branch("ClusterUTCModTime", ClusterUTCModTime, "ClusterUTCModTime[120][20]" );
  	otr->Branch("ClusterUTCModEne", ClusterUTCModEne, "ClusterUTCModEne[120][20]" );
  }




  Bool_t isGoodRun;
  Bool_t isGoodSpill;

  if( !isRealData ){
    otr->Branch("GsimGenEventID", &GsimGenEventID, "GsimGenEventID/I" );
    otr->Branch("GsimEntryID",    &GsimEntryID,    "GsimEntryID/I" );
    otr->Branch("MCEventID",      &MCEventID,      "MCEventID/I");
    otr->Branch("TruePID",        &TruePID,        "TruePID/I" );
    otr->Branch("TrueDecayMode",  &TrueDecayMode,  "TrueDecayMode/I" );
    otr->Branch("TrueVertexZ",    &TrueVertexZ,    "TrueVertexZ/D" );
    otr->Branch("TrueVertexTime", &TrueVertexTime, "TrueVertexTime/D" );
    otr->Branch("TrueMom",         TrueMom,        "TrueMom[3]/D" );
    otr->Branch("TruePos",         TruePos,        "TruePos[3]/D" );
    genP->Branch( otr );
    otr->Branch("AccidentalFileName",   "std::string",          &AccidentalFileName );
    otr->Branch("AccidentalFileID",     &AccidentalFileID,      "AccidentalFileID/I" );
    otr->Branch("AccidentalEntryID",    &AccidentalEntryID,     "AccidentalEntryID/L" );
    otr->Branch("AccidentalOverlayFlag",&AccidentalOverlayFlag, "AccidentalOverlayFlag/O" );
  }else{
    
	  
	std::string strL2trig;
	std::string strSpillNo;
  
	void* ptrL2Trig =  dst->GetL2TrigPtr(strL2trig);
	void* ptrSpillNo = dst->GetSpillNoPtr(strSpillNo);
	  
	otr->Branch("SpillID",        ptrSpillNo,             strSpillNo.c_str());
    otr->Branch("EventID",        &(dst->EventNo),        "EventID/I" );    
    otr->Branch("TimeStamp",      &(dst->TimeStamp),      "TimeStamp/I");
    otr->Branch("L2TimeStamp",    &(dst->L2TimeStamp),    "L2TimeStamp/i");
    otr->Branch("L1TrigNo",       &(dst->L1TrigNo),       "L1TrigNo/I");
    otr->Branch("L2TrigNo",       ptrL2Trig,              strL2trig.c_str());
    otr->Branch("COE_Esum",       &(dst->COE_Esum),       "COE_Esum/I");
    otr->Branch("COE_Ex",         &(dst->COE_Ex),         "COE_Ex/I");
    otr->Branch("COE_Ey",         &(dst->COE_Ey),         "COE_Ey/I");
    otr->Branch("DetectorBit",    &(dst->DetectorBit),    "DetectorBit/i");
    otr->Branch("ScaledTrigBit",  &(dst->ScaledTrigBit),  "ScaledTrigBit/i");
    otr->Branch("RawTrigBit",     &(dst->RawTrigBit),     "RawTrigBit/i");
    otr->Branch("Error",          &(dst->Error),          "Error/S");
    otr->Branch("TrigTagMismatch",&(dst->TrigTagMismatch),"TrigTagMismatch/S");
    otr->Branch("L2AR",           &(dst->L2AR),           "L2AR/S");
    otr->Branch("COE_Et_overflow",&(dst->COE_Et_overflow),"COE_Et_overflow/S");
    otr->Branch("COE_L2_override",&(dst->COE_L2_override),"COE_L2_override/S");
    otr->Branch("isGoodRun",      &isGoodRun,             "isGoodRun/O");
    otr->Branch("isGoodSpill",    &isGoodSpill,           "isGoodSpill/O");
/*
    if( itr->GetBranch("CDTNum") ){
      otr->Branch("CDTNum",       &(dst->CDTNum),         "CDTNum/I");
      otr->Branch("CDTData",      (dst->CDTData),         "CDTData[3][64]/S");
      otr->Branch("CDTBit",       (dst->CDTBit),          "CDTBit[38][38]/S");
    }
*/
    if( itr->GetBranch("CDTVetoData") ){
      otr->Branch("CDTVetoData",  (dst->CDTVetoData),     "CDTVetoData[4][64]/S");
      otr->Branch("CDTVetoBitSum",(dst->CDTVetoBitSum),   "CDTVetoBitSum[13]/S");
    }
  }
  if( itr->GetBranch("CDTNum") ){
     otr->Branch("CDTNum",       &(dst->CDTNum),         "CDTNum/I");
  }
  if( itr->GetBranch("CDTData") ){
     otr->Branch("CDTData",      (dst->CDTData),         "CDTData[3][64]/S");
     otr->Branch("CDTBit",       (dst->CDTBit),          "CDTBit[38][38]/S");
  }
 
  Double_t CSIEt      = 0;
  Double_t CSIHalfEtR = 0;
  Double_t CSIHalfEtL = 0;
  otr->Branch( "CSIEt",      &CSIEt,      "CSIEt/D" );
  otr->Branch( "CSIHalfEtR", &CSIHalfEtR, "CSIHalfEtR/D" );
  otr->Branch( "CSIHalfEtL", &CSIHalfEtL, "CSIHalfEtL/D" );
  
  Int_t    CSIWfmCorrupted; 
  Int_t    CSINumber;
  Int_t    CSIModID[2716];
  Float_t  CSIEne[2716];
  Float_t  CSIProdDer2[2716];
  Float_t  CSISmoothness[2716];
  Float_t  CSITime[2716];
  Long64_t CSIEtSum[64];
  Short_t  CSIPSBit[2716];
  otr->Branch("CSINumber",&CSINumber,"CSINumber/I");
  otr->Branch("CSIModID",  CSIModID, "CSIModID[CSINumber]/I");
  otr->Branch("CSIEne",    CSIEne,   "CSIEne[CSINumber]/F");
  otr->Branch("CSITime",   CSITime,  "CSITime[CSINumber]/F");
  //otr->Branch("CSIEtSum",  CSIEtSum, "CSIEtSum[64]/L");
  
  
  if(itr->GetBranch("CSIPSBit")) otr->Branch("CSIPSBit",  CSIPSBit, "CSIPSBit[CSINumber]/S");
  if(itr->GetBranch("CSIProdDer2")) otr->Branch("CSIProdDer2", CSIProdDer2, "CSIProdDer2[CSINumber]");
  if(itr->GetBranch("CSISmoothness")) otr->Branch("CSISmoothness", CSISmoothness, "CSISmoothness[CSINumber]");
  if(itr->GetBranch("CSISmoothness") || itr->GetBranch("CSIProdDer2") ){
	  otr->Branch("CSIWfmCorrupted", &CSIWfmCorrupted, "CSIWfmCorrupted/I");
  }


  Int_t    UTCWfmCorrupted; 
  Int_t    UTCNumber;
  Int_t    UTCModID[256];
  Float_t  UTCEne[256];
  Float_t  UTCTime[256];
  Short_t  UTCPSBit[256];
  Float_t  UTCProdDer2[256];
  Float_t  UTCSmoothness[256];
 
  if(userFlag>20190101){
  	otr->Branch("UTCNumber",&UTCNumber,"UTCNumber/I");
  	otr->Branch("UTCModID",  UTCModID, "UTCModID[UTCNumber]/I");
  	otr->Branch("UTCEne",    UTCEne,   "UTCEne[UTCNumber]/F");
  	otr->Branch("UTCTime",   UTCTime,  "UTCTime[UTCNumber]/F");
  	if(itr->GetBranch("CSIPSBit")) otr->Branch("UTCPSBit",  UTCPSBit, "UTCPSBit[UTCNumber]/S");
  	if(itr->GetBranch("CSIProdDer2")) otr->Branch("UTCProdDer2",  UTCProdDer2, "UTCProdDer2[UTCNumber]");
  	if(itr->GetBranch("CSISmoothness")) otr->Branch("UTCSmoothness",  UTCSmoothness, "UTCSmoothness[UTCNumber]");
  	if(itr->GetBranch("CSIWfmCorrupted")) otr->Branch("UTCWfmCorrupted", &UTCWfmCorrupted, "UTCWfmCorrupted/I");
  }

  ///// CSI Energy Correction Tree
  TTree *csitr = new TTree( "CSICorrectionTree", "csi gain correction");
  Double_t CSIAlOverall, CSIK3pi0Calib[2716], CSILaserCorr[2716],CSITemperatureCorr[2716], CSITimeDepGain;
  if( isRealData ){
    csitr->Branch("AlOverall",      &CSIAlOverall,       "AlOverall/D");
    csitr->Branch("K3pi0Calib",      CSIK3pi0Calib,      "K3pi0Calib[2716]/D");
    csitr->Branch("LaserCorr",       CSILaserCorr,       "LaserCorr[2716]/D");
    csitr->Branch("TemperatureCorr", CSITemperatureCorr, "TemperatureCorr[2716]/D");
    csitr->Branch("TimeDependCorr", &CSITimeDepGain,     "TimeDependCorr/D");

    CSIAlOverall   = CSIECalibCorrector->GetSubEnergyCorrectionFactor( CSIEnergyCorrector::Overall, 0 );
    CSITimeDepGain = CSIECalibCorrector->GetSubEnergyCorrectionFactor( CSIEnergyCorrector::Time,    0 );
    for(Int_t ich = 0 ; ich < 2716 ; ++ich ){
      CSIK3pi0Calib[ich]      = CSIECalibCorrector->GetSubEnergyCorrectionFactor( CSIEnergyCorrector::K3pi0,       ich );
      CSILaserCorr[ich]       = CSIECalibCorrector->GetSubEnergyCorrectionFactor( CSIEnergyCorrector::Laser,       ich );
      CSITemperatureCorr[ich] = CSIECalibCorrector->GetSubEnergyCorrectionFactor( CSIEnergyCorrector::Temperature, ich );
    }
    csitr->Fill();
  }
  
  ///// Clock Shift Correction Tree  
  //  TTree *clocktr = new TTree( "ClockCorrectionTree", "clock correction");

  // declare  ClusterFinder and variables
  ClusterFinder clusterFinder;
  if( SetForceOldCsiZPosition ){
    clusterFinder.setCsiZsurface( 6148 );
  }else{
    clusterFinder.setCsiZsurface(CSIZPosition);
  }
  int   nCSIDigi           = 0;
  int    CSIDigiID[3000]   = {0};
  double CSIDigiE[3000]    = {0};
  double CSIDigiTime[3000] = {0};
  int   nCSIDigiForClustering           = 0;
  int    CSIDigiIDForClustering[3000]   = {0};
  double CSIDigiEForClustering[3000]    = {0};
  double CSIDigiTimeForClustering[3000] = {0};

  MTPositionHandler* pHandler = MTPositionHandler::GetInstance();  

  std::cout<<"start"<<std::endl;
  std::cout<<"nDet:"<<CFDP::nDetectors<<std::endl;
  
  ///// declare veto detector classes
  MTDstDetector* DetectorHandler[CFDP::nDetectors]={ NULL };
  Bool_t         DetectorExistFlag[CFDP::nDetectors];
 

  int MTBPIndex[CFDP::nDetectors];
  MTBPIndex[CFDP::CC03] = MTBP::CC03;
  MTBPIndex[CFDP::CC04] = MTBP::CC04;
  MTBPIndex[CFDP::CC05] = MTBP::CC05;
  MTBPIndex[CFDP::CC06] = MTBP::CC06;
  MTBPIndex[CFDP::CBAR] = MTBP::CBAR;
  MTBPIndex[CFDP::FBAR] = MTBP::FBAR;
  MTBPIndex[CFDP::CSI]  = MTBP::CSI;
  MTBPIndex[CFDP::CV]   = MTBP::CV;
  MTBPIndex[CFDP::BCV]  = MTBP::BCV;
  MTBPIndex[CFDP::NCC]  = MTBP::NCC;
  MTBPIndex[CFDP::OEV]  = MTBP::OEV;
  MTBPIndex[CFDP::LCV]  = MTBP::LCV;
  MTBPIndex[CFDP::BPCV] = MTBP::BPCV;
  MTBPIndex[CFDP::BHPV] = MTBP::BHPV;
  MTBPIndex[CFDP::BHCV] = MTBP::BHCV;
  MTBPIndex[CFDP::newBHCV] = MTBP::newBHCV;
  MTBPIndex[CFDP::BHGC] = MTBP::BHGC;
  MTBPIndex[CFDP::IB] = MTBP::IB;
  MTBPIndex[CFDP::IBCV] = MTBP::IBCV;
  MTBPIndex[CFDP::MBCV] = MTBP::MBCV;
  MTBPIndex[CFDP::DCV]  = MTBP::DCV;
  MTBPIndex[CFDP::UCV]  = MTBP::UCV;
  MTBPIndex[CFDP::UCVLG]= MTBP::UCVLG;



  for( int idet=0; idet<CFDP::nDetectors; idet++ ){

    ///// Check the existence of the detector. ( for CSI, the flag is always unset )
    ///// If it doesn't exist, DetectorHandler of the detector is not generated.
    if( itr->GetBranch(Form("%sNumber",CFDP::DetName[idet].c_str())) && idet!=CFDP::CSI ){
      DetectorExistFlag[idet] = true;
      std::cout << "Veto Detector : " << CFDP::DetName[idet].c_str() << " exists." << std::endl;
    }else{
      DetectorExistFlag[idet] = false;
      std::cout<< "Veto Detector : " << CFDP::DetName[idet].c_str() <<" doesn't exist." << std::endl;
      continue;
    }
    
    switch( idet ){
    case CFDP::CV:      DetectorHandler[idet] = new MTDstCV(  RunID, userFlag);      break;
    case CFDP::CBAR:    DetectorHandler[idet] = new MTDstCBAR(RunID, userFlag);      break;
    case CFDP::BCV:     DetectorHandler[idet] = new MTDstBCV( RunID, userFlag);      break;
    case CFDP::CC03:    DetectorHandler[idet] = new MTDstCC03(RunID, userFlag);      break;
    case CFDP::CC04:    DetectorHandler[idet] = new MTDstCC04(RunID, userFlag);      break;
    case CFDP::CC05:    DetectorHandler[idet] = new MTDstCC05(RunID, userFlag);      break;
    case CFDP::CC06:    DetectorHandler[idet] = new MTDstCC06(RunID, userFlag);      break;
    case CFDP::IB:      DetectorHandler[idet] = new MTDstIB(  RunID, userFlag);      break;
    case CFDP::IBCV:    DetectorHandler[idet] = new MTDstIBCV(RunID, userFlag);      break;
    case CFDP::NCC:     DetectorHandler[idet] = new MTDstNCC (RunID, userFlag);      break;
    case CFDP::UCV:     DetectorHandler[idet] = new MTDstUCV( RunID, userFlag);       break;
    case CFDP::FBAR:
	case CFDP::UCVLG:
    case CFDP::LCV:
    case CFDP::DCV:
    case CFDP::OEV:
    case CFDP::BPCV:
    case CFDP::MBCV:
      DetectorHandler[idet] = new MTDstVeto(idet, RunID, userFlag); break;
	case CFDP::BHCV:  
    case CFDP::newBHCV:
    case CFDP::BHGC:
      DetectorHandler[idet] = new MTDst500MHzDetector(idet, RunID, userFlag); break;//
	case CFDP::BHPV:
      DetectorHandler[idet] = new MTDstBHPV(RunID, userFlag); break;//
    }
    
    DetectorHandler[idet]->SetDataMCFlag(isRealData);

    DetectorHandler[idet]->SetBranchAddress( itr );
    DetectorHandler[idet]->Branch( otr );

    if( idet==CFDP::CBAR && userFlag<20160101 )
      DetectorHandler[idet]->DstBranch( otr );///// special treatment for 2015 run (CBAR39 problem)
    if( idet==CFDP::FBAR && 20180101<=userFlag && userFlag<20180301 )
      DetectorHandler[idet]->DstBranch( otr );///// special treatment for RUN78 (FBAR ch5 was almost dead)
    
    DetectorHandler[idet]->AddSuppFTTBranch( otr );
  }
  
  ///// stopwatch

  TStopwatch sw;
  std::cout << "Stopwatch START ! " << std::endl;
  sw.Start();


  //////////////////////////////
  ////// Event Loop Start //////
  //////////////////////////////
  std::cout<<"loop start"<<std::endl;  
  std::map< int, int > IDIndexMap;
  for( int entry = 0 ; entry < nEntries ; entry++ ){

    if( nEntries>100 )
      if( entry%(nEntries/10)==0 )
	std::cout << entry/(nEntries/100) << "%" << std::endl;
    
    dst->GetEntry(entry, applyPS);
    DstEntryID = entry;

	DetectorWFMCorruptBit = 0;
	
	if(dst->CSIWfm_corrupted) DetectorWFMCorruptBit |= (1<<MTBP::CSI);
	if(dst->UTCWfm_corrupted) DetectorWFMCorruptBit |= (1<<MTBP::CSI);

    if( isRealData ){
      isGoodRun   = GRGSMan->IsGoodRun();
      
	  EnergyCorrector::SetL2TrigNo( dst->GetL2TrigNo() );
      TimingShiftCorrector::SetL2TrigNo( dst->GetL2TrigNo() );

  	  isGoodSpill = GRGSMan->IsGoodSpill( (short) dst->GetSpillNo() );
	  TimingShiftCorrector::SetSpillID( (short) dst->GetSpillNo() );
  	  EnergyCorrector::SetSpillID( (short) dst->GetSpillNo() );
	 
    }

    // Initialize
    CSIEt                 = 0;
    CSIHalfEtR            = 0;
    CSIHalfEtL            = 0;
    data.CsiNumber        = 0;
    data.ClusterNumber    = 0;
    nCSIDigiForClustering = 0;
    CSINumber             = 0;
    
    // Neglect TRIGGERTAG events
    if( dst->IsLaser() || dst->IsLED() ){
      data.eventID++;
      continue;
    }
    
    // Get data for clustering from dst
    dst->GetCSIDataWithoutDeadChannels( nCSIDigi, CSIDigiID, CSIDigiE, CSIDigiTime, useinitialdata ); 
    //    dst->GetCSIEtSum(CSIEtSum);
    
		
	int nUTCDigi=0;
	if(userFlag<20190101){
    	IDIndexMap.clear();
    	for( int imod=0; imod<nCSIDigi; imod++ ){
    	  IDIndexMap.insert( std::map< int, int >::value_type( CSIDigiID[imod], imod ) );
    	}
	}else{
		IDIndexMap.clear();
    	int nCSIDigi_woUTC = nCSIDigi;
    	for( int imod=0; imod<nCSIDigi; imod++ ){
			if(CSIDigiID[imod]>=2716){
				nCSIDigi_woUTC = imod;
				nUTCDigi = nCSIDigi - nCSIDigi_woUTC;
				break;
			}
    	    IDIndexMap.insert( std::map< int, int >::value_type( CSIDigiID[imod], imod ) );
    	}
    	nCSIDigi = nCSIDigi_woUTC;
	}

    ///// CSI Energy Correction for real data. (not for MC)
    ///// Absolute energy correction(Al target run), Temperature correction, 3pi0 correction.
    if( isRealData ){
      CSIAddEneCorrector->SetNodeFileID(NodeID,FileID);
      CSITimingCorrector->SetNodeFileID(NodeID,FileID);
      for( int ich=0; ich<nCSIDigi; ich++ ){

		  if(!turnoffEcorr){
			  CSIDigiE[ich]    *= CSIECalibCorrector->GetEnergyCorrectionFactor(CSIDigiID[ich]);
			  CSIDigiE[ich]    *= CSIAddEneCorrector->GetCorrFactor(CSIDigiID[ich]);
		  }
		  if(!turnoffTcorr){
			  CSIDigiTime[ich] += CSITimingCorrector->GetCorrValue(CSIDigiID[ich],"clock");
		  }
      }
    }
    
    // Convert FADC clock to nsec.
    for( int ich=0; ich<nCSIDigi; ich++ )
      CSIDigiTime[ich] *= 8.;
    
    // Correct time delay over 400MeV crystals. ( data only )
    if( isRealData )
      for( int ich = 0 ; ich < nCSIDigi ; ++ich )
	if( CSIDigiE[ich] > 400 )
	  for( int index = 1 ; index < 3 ; ++index )
	    CSIDigiTime[ich] -= MTBP::CSITimeDelayCoeff[index] * ( pow( CSIDigiE[ich], index) - pow( 400, index) );
    
    


    // Filling digi data. Neglect minus energy crystals
    // For clustering, I use only 150-300ns hit crystal ( data only )
    // Filling CsIEt. Neglect minus energy crystals.
    // I use only 150-300ns hit crystal for data and only 0-100ns hit for sim. -> change -20 - 130ns
    for( int imod=0; imod<nCSIDigi; imod++ ){  
      CSIModID[imod] = CSIDigiID[imod];
      CSIEne[imod]   = (float)CSIDigiE[imod];
      CSITime[imod]  = (float)CSIDigiTime[imod];
      CSINumber      = nCSIDigi;
     
	  CSIPSBit[imod]  =  dst->GetCSIPSBit(CSIModID[imod]);
      CSIProdDer2[imod]    =  dst->GetCSIProdDer2(CSIModID[imod]);
      CSISmoothness[imod]  =  dst->GetCSISmoothness(CSIModID[imod]);
	  if(imod==0){
		  CSIWfmCorrupted = dst->CSIWfm_corrupted;
		  UTCWfmCorrupted = dst->UTCWfm_corrupted;
	  }


      if( CSIDigiE[imod] < 0 || CSIDigiE[imod] < 2 ) continue;
      data.CsiModID[data.CsiNumber] = CSIDigiID[imod];
      data.CsiEne[data.CsiNumber]   = CSIDigiE[imod];
      data.CsiTime[data.CsiNumber]  = CSIDigiTime[imod];
      data.CsiNumber++;
      
      CSIDigiIDForClustering[nCSIDigiForClustering]  = CSIDigiID[imod];
      //if( (isRealData && 150<CSIDigiTime[imod] && CSIDigiTime[imod]<300) || (!isRealData && CSIDigiTime[imod]<100) ){
      if(  150 < CSIDigiTime[imod] && CSIDigiTime[imod] < 300 ) {
	CSIDigiEForClustering[nCSIDigiForClustering]    = CSIDigiE[imod];
	CSIDigiTimeForClustering[nCSIDigiForClustering] = CSIDigiTime[imod];
	// CsIEt
	CSIEt += CSIDigiE[imod];
	if( pHandler->GetCSIXPosition( CSIDigiID[imod] ) > 0 ) CSIHalfEtR += CSIDigiE[imod];
	else                                                   CSIHalfEtL += CSIDigiE[imod];
      }else{
	CSIDigiEForClustering[nCSIDigiForClustering]    = 0;
	CSIDigiTimeForClustering[nCSIDigiForClustering] = 0;
      }
      nCSIDigiForClustering++;
    }
    
    //////////////////////////////
    ///// Clustering Process /////
    //////////////////////////////
    std::list<Cluster> clist = clusterFinder.findCluster( nCSIDigiForClustering, CSIDigiIDForClustering, CSIDigiEForClustering, CSIDigiTimeForClustering );
    FillWeightedAverageTimeInClusters( clist );
    
	bool iterationFlag = true;
    while( iterationFlag ){
      iterationFlag = false;
      std::set<int> rejectSet;
      csiHitInfo hitInfo = { 0, 0 };
      std::vector<csiHitInfo> timeChisqVec;
      for( std::list<Cluster>::iterator it=clist.begin(); it!=clist.end(); it++ ){
	std::vector<int>    clusterIdVec   = it->clusterIdVec();
	std::vector<double> clusterEVec    = it->clusterEVec();
	std::vector<double> clusterTimeVec = it->clusterTimeVec();
	for( unsigned int icsi = 0 ; icsi<clusterIdVec.size() ; icsi++ ){
	  if( clusterTimeVec[icsi] - it->t() > upperLimit->Eval( std::min( 2000., clusterEVec[icsi]) ) || 
	      clusterTimeVec[icsi] - it->t() < lowerLimit->Eval( std::min( 2000., clusterEVec[icsi]) )   ){
	    //rejectSet.insert( clusterIdVec[icsi] );
	    iterationFlag = true;
	    //break;
	    hitInfo.id         = clusterIdVec[icsi];
	    hitInfo.timeChisq  = fabs(clusterTimeVec[icsi] - it->t());
	    hitInfo.timeChisq /= getTimeSigma( clusterEVec[icsi] );
	    timeChisqVec.push_back( hitInfo );
	  }
	}
      }
      if( iterationFlag==false ) break;
      
      // Largest timeChisq ( csiTime - clusterTime )/ sigma is ignored.
      std::sort( timeChisqVec.begin(), timeChisqVec.end(), csiHitInfo::largerTimeChisq );
      for( int icsi=0; icsi<nCSIDigiForClustering; icsi++ ){
	if( CSIDigiIDForClustering[icsi] == timeChisqVec[0].id ){
	  CSIDigiEForClustering[icsi] = 0;
	}
      }
      clist.clear();
      clist = clusterFinder.findCluster( nCSIDigiForClustering, CSIDigiIDForClustering, CSIDigiEForClustering, CSIDigiTimeForClustering );
      FillWeightedAverageTimeInClusters( clist );
    }
    
    // If some clusters exist, check cluster timing difference each other. Threshold is 30ns so far.(2013.4.25)
    if( clist.size() > 0 ){
      std::vector< double > clusterTimeVec;
      for( std::list<Cluster>::iterator it=clist.begin(); it!=clist.end(); it++ ){
	clusterTimeVec.push_back( it->t() );
      }
      
      std::sort( clusterTimeVec.begin(), clusterTimeVec.end() );
      while( true ){
	if( clusterTimeVec[clusterTimeVec.size()-1] - clusterTimeVec[0] < 30 )
	  break;
	double averageTime = std::accumulate( clusterTimeVec.begin(), clusterTimeVec.end(), 0.0 );
	averageTime /= clusterTimeVec.size();
	if( averageTime-clusterTimeVec[0] > clusterTimeVec[clusterTimeVec.size()-1] - averageTime )
	  clusterTimeVec.erase( clusterTimeVec.begin() );
	else
	  clusterTimeVec.pop_back();
      }
      
      for( std::list<Cluster>::iterator it=clist.begin(); it!=clist.end();){
	bool rejectFlag = true;
	for( unsigned int index=0; index<clusterTimeVec.size(); index++ ){
	  if( it->t() == clusterTimeVec[index] ){
	    rejectFlag = false;
	    break;
	  }
	}
	
	if( rejectFlag ){
	  it = clist.erase( it );
	  continue;
	}
	it++;
      }
    }
    ///// End of Clustering Process /////
    /////////////////////////////////////


    ////////////////////
    ///// Blinding /////
    ////////////////////
    // if this is a 2-gamma event in a physics run
    // in which pi0 is reconstructed in the blind box,
    // this event is not stored in the output. 
    if( clist.size() > 1 && isRealData && BlindFlag ){
      int    OriginalClusterNumber = 0;
      double OriginalClusterE[100];
      double OriginalClusterT[100];
      double OriginalClusterVertexT[100];
      double OriginalClusterPos[100][3];
      
      ///// Store original clusters information
      for( std::list<Cluster>::iterator it = clist.begin() ; it!=clist.end() ; it++ ){
	OriginalClusterE[OriginalClusterNumber]       = it->e();
	OriginalClusterT[OriginalClusterNumber]       = it->t();
	OriginalClusterVertexT[OriginalClusterNumber] = it->t();
	OriginalClusterPos[OriginalClusterNumber][0]  = it->x();
	OriginalClusterPos[OriginalClusterNumber][1]  = it->y();
	OriginalClusterPos[OriginalClusterNumber][2]  = it->z();
	OriginalClusterNumber++;
      }
      bool* clusterUsageFlag = new bool[OriginalClusterNumber]();
      for( int icluster=0; icluster<OriginalClusterNumber; icluster++ )
	clusterUsageFlag[icluster] = false;
      
      ///// gamma finding
      GammaFinder gFinder;
      std::list<Gamma> glist;
      gFinder.findGamma( clist, glist); // gamma difinition ( > 20 MeV)      
      std::vector< double > clusterTimeVec;
      for( std::list<Gamma>::iterator it=glist.begin(); it!=glist.end(); it++ )
	clusterTimeVec.push_back( it->t() );
      
      if(glist.size() > 1 ){
	while( glist.size() != 2 ){
	  std::sort( clusterTimeVec.begin(), clusterTimeVec.end() );
	  while( clusterTimeVec.size() != 2 ){
	    double averageTime = std::accumulate( clusterTimeVec.begin(), clusterTimeVec.end(), 0.0);
	    averageTime /= clusterTimeVec.size();
	    if( averageTime-clusterTimeVec[0] > clusterTimeVec[clusterTimeVec.size()-1]-averageTime )
	      clusterTimeVec.erase( clusterTimeVec.begin() );
	    else
	      clusterTimeVec.pop_back();
	  }
	  
	  for( std::list<Gamma>::iterator it=glist.begin(); it!=glist.end(); ){
	    bool rejectFlag = true;
	    for( unsigned int index=0; index<clusterTimeVec.size(); index++ ){
	      if( it->t() == clusterTimeVec[index] ){
		rejectFlag = false;
		break;
	      }
	    }
	    if( rejectFlag ){
	      it = glist.erase( it );
	      continue;
	    }
	    it++;
	  }
	}
	
	  
	std::list<Pi0> piList;      
	if( user_rec(glist,piList) ){
	  Pi0   const &pi0 = piList.front();
	  Gamma const &g1  = pi0.g1();
	  Gamma const &g2  = pi0.g2();
	  clusterUsageFlag[g1.id()] = true;
	  clusterUsageFlag[g2.id()] = true;
	  
	  double VertexTime[2]  = {0};
	  double EventStartTime = GetEventStartTime(piList, VertexTime, CSIZPosition );
	  double EventStartZ    = piList.front().recZ();
	  
	  for( int icluster=0; icluster<OriginalClusterNumber; icluster++ ){
	    OriginalClusterVertexT[icluster] = OriginalClusterT[icluster] - sqrt( pow( CSIZPosition-EventStartZ,  2) + 
										  pow( OriginalClusterPos[icluster][0], 2) +
										  pow( OriginalClusterPos[icluster][1], 2)  ) / (TMath::C()/1E6);
	  }
	 

	  bool Only2gFlag = true;
	  if( OriginalClusterNumber > 2 ){	    
	    for( int icluster=0; icluster < OriginalClusterNumber; icluster++ ){
	      OriginalClusterVertexT[icluster] = OriginalClusterT[icluster] - sqrt( pow( CSIZPosition-EventStartZ,  2) +
										    pow( OriginalClusterPos[icluster][0], 2) + 
										    pow( OriginalClusterPos[icluster][1], 2)  ) / (TMath::C()/1E6);
	      
	      if( clusterUsageFlag[icluster] ) continue; // This cluster is included in KL
	      if( fabs(OriginalClusterVertexT[icluster]-EventStartTime)<10 && OriginalClusterE[icluster]>20) Only2gFlag=false;
	    }
	  }
	  
	  if( Only2gFlag && (RunID>=28537 || (dst->ScaledTrigBit & 0x100)) ) { // From RUN79 (28537~), we don't have on-spill bit.
	    double Pi0Pt = pi0.p3().perp();
	    double RecZ  = pi0.recZ();
	
		if( 120 < Pi0Pt && Pi0Pt < 260 && 2900 < RecZ && RecZ < 5100 ){
	      data.eventID++;
	      continue;
	    }
	  }
	}
      }
      delete[] clusterUsageFlag;
    }//// end of the blinding 
   

	if(userFlag>=20190101){

		memset(ClusterUTCModNumber, 0, sizeof(ClusterUTCModNumber));
		memset(ClusterUTCModID, 0, sizeof(ClusterUTCModID));
		
		for(int i=0;i<NclsMAX;i++){
			for(int j=0;j<NUTCmodMAX;j++){
				ClusterUTCModTime[i][j] = 0.0;
				ClusterUTCModEne[i][j] = 0.0;
			}
		}
	
		// UTC fill
	    for( int imod=0; imod<nUTCDigi; imod++ ){
	      UTCModID[imod] = CSIDigiID[imod+nCSIDigi]-2716;
	      UTCEne[imod]   = (float)CSIDigiE[imod+nCSIDigi];
	      UTCPSBit[imod]  =  dst->GetUTCPSBit(UTCModID[imod]);
	      UTCTime[imod]  = 8.0*(float)CSIDigiTime[imod+nCSIDigi];
	      UTCProdDer2[imod]    =  dst->GetUTCProdDer2(UTCModID[imod]);
	      UTCSmoothness[imod]  =  dst->GetUTCSmoothness(UTCModID[imod]);
		} 
	    UTCNumber        = nUTCDigi;
	
	
		// UTC cluster info fill
		float tbl_UTCEne[256];
		float tbl_UTCTime[256];
	
		for(int i=0;i<256;i++){
			tbl_UTCEne[i] = -1;
			tbl_UTCTime[i] = -1;
		}
	
		for(int i=0;i<UTCNumber;i++){
			int imod = UTCModID[i];
			tbl_UTCEne[imod] = UTCEne[i];
			tbl_UTCTime[imod] = UTCTime[i];
		}
	
		int Ncls = 0;
	    for( std::list<Cluster>::iterator it=clist.begin(); it!=clist.end(); it++ ){
	
			const std::vector<int> clsIDvec = it->clusterIdVec();
			std::vector<std::pair<int, double> > hitlist;
	
			for(unsigned int i=0;i<clsIDvec.size();i++){
				int modID = clsIDvec[i];
				int sumID = utcconf->GetSumID(modID); 
				if(sumID<0) continue;
	
				double Esum = tbl_UTCEne[sumID];
				if(Esum<0) continue;
	

		
				if(useinitialdata==0){
					// we exclude accidentals hit only for MPPC
					if(isRealData){
						if(tbl_UTCTime[sumID]<215 || tbl_UTCTime[sumID]>255) continue;  		
					}else{
						if(tbl_UTCTime[sumID]<230 || tbl_UTCTime[sumID]>270) continue;  		
					}
				}

				hitlist.push_back(std::pair<int, double>(sumID, Esum));
			}
	
	
			std::sort(hitlist.begin(), hitlist.end(), sort_hitlist);
			
			int IDprev = -1;
			std::vector<int> merged_list;
		 	
			for(unsigned int i=0;i<hitlist.size();i++){
				if(IDprev==hitlist[i].first)continue;
				IDprev = hitlist[i].first;
				merged_list.push_back(IDprev);
			}	
			
			if(merged_list.size()==0){
				ClusterUTCModNumber[Ncls]= 0;
				continue;
			}
		
			if(Ncls>=NclsMAX){
				std::cout << "Error! MAX of cluster number is " << NclsMAX << "!\n";
				continue;
			}	
	
			int nsave = 0;
			for(unsigned int i=0;i<merged_list.size();i++){
	  			if(i>=(unsigned int)NUTCmodMAX) continue;
				//std::cout << "merged_list[" << i << "]=" << merged_list[i] << ", E = " << tbl_UTCEne[merged_list[i]] << "\n"; 
				ClusterUTCModID[Ncls][i]   = merged_list[i];
				ClusterUTCModTime[Ncls][i] = tbl_UTCTime[merged_list[i]];
				ClusterUTCModEne[Ncls][i]  = tbl_UTCEne[merged_list[i]];
				nsave++;
			}
			ClusterUTCModNumber[Ncls]= nsave; 
			Ncls++;
		}
	
	}

    
    // veto detector process
    for( int idet=0; idet<CFDP::nDetectors; idet++ ){
      
	  if( !DetectorExistFlag[idet] )
		  continue;
      switch( idet ){
      case CFDP::CV:
      case CFDP::CBAR:
      case CFDP::BCV:
      case CFDP::CC03:
      case CFDP::CC04:
      case CFDP::CC05:
      case CFDP::CC06:
      case CFDP::FBAR:
      case CFDP::LCV:
      case CFDP::OEV:
      case CFDP::NCC:
      case CFDP::BPCV:
      case CFDP::IBCV:
      case CFDP::MBCV:
      case CFDP::UCVLG:
      case CFDP::DCV:
	DetectorHandler[idet]->SetNodeFileID(NodeID,FileID);
	if(applyPS==1) DetectorHandler[idet]->Apply_PS(PSdet);
	DetectorHandler[idet]->FillPSBit();
	DetectorHandler[idet]->Process2();
	if(DetectorHandler[idet]->GetFlagWfmCorrupted()) DetectorWFMCorruptBit |= (1<<MTBPIndex[idet]);
	break;
      case CFDP::BHCV:
      case CFDP::BHPV:
      case CFDP::newBHCV:
      case CFDP::BHGC:
      case CFDP::IB:
      case CFDP::UCV:
	DetectorHandler[idet]->SetNodeFileID(NodeID,FileID);
	if(applyPS==1) DetectorHandler[idet]->Apply_PS(PSdet);
	DetectorHandler[idet]->FillPSBit();
	DetectorHandler[idet]->Process();
	break;
      default:
	break;
      }
      DetectorHandler[idet]->FillSuppFTTVars();
      DetectorHandler[idet]->FillWfm();
    }
    
    ///// filling digi-data and Cluster infomation in TTree
    data.setData( clist );
    otr->Fill();
    data.eventID++; 
  }// end of event loop 
  ///// stopwatch
  sw.Stop();
  std::cout << "--- End of event loop: "; sw.Print();
  
  if(isRealData){
    otf->cd();
    csitr->Write();
    //    clocktr->Write();
  }
  
  
  otf->cd();
  otr->Write();  
  if(calibtreeout!=nullptr){
	calibtreeout->Write();
  }
  
  otf->Close();


  return 0;
}
