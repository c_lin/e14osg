#ifndef __MTDSTIB_H__
#define __MTDSTIB_H__

#include <iostream>
#include "MTDst500MHzDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

class MTDstIB : public MTDst500MHzDetector
{
public:
  MTDstIB( Int_t runID, Int_t userFlag );
  void SetNumFromModID();
  void SetModIDToChannel();

private:
};

#endif //__MTDSTIB_H__
