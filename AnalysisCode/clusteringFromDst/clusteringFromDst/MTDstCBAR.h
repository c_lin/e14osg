#ifndef __MTDSTCBAR_H__
#define __MTDSTCBAR_H__

#include <cmath>
#include <fstream>
#include <iostream>
#include "MTDstVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

class MTDstCBAR : public MTDstVeto
{
public:
  MTDstCBAR( Int_t runID, Int_t userFlag );

  void Branch( TTree* tree );
  
  void SetNumFromModID();
  void SetModIDToChannel();
  void Process() {Process2();}
  void Process2();
private:
  float GetHitTime( float frontTime, float rearTime );
  float GetHitZ(    float frontTime, float rearTime );
  float GetEne(     float frontEne,  float rearEne, float frontTime, float rearTime);
};

#endif //__MTDSTCBAR_H__
