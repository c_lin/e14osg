#ifndef __MTDSTCC03_H__
#define __MTDSTCC03_H__

#include <iostream>
#include "MTDstVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTDstCC03 : public MTDstVeto
{
public:
  MTDstCC03( Int_t runID, Int_t userFlag );
    
  void SetModIDToChannel();
  void Process2();
  void Initialize();
private:
  float GetHitTime( float PMTTime1, float PMTTime2 ){ return (PMTTime1 + PMTTime2)/2.; }
  float GetEne(     float PMTEne1,  float PMTEne2 ){  return (PMTEne1  + PMTEne2 )/2.; }
};

#endif //__MTDSTCC03_H__
