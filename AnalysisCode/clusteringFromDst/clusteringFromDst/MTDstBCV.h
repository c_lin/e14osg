#ifndef __MTDSTBCV_H__
#define __MTDSTBCV_H__

#include <cmath>
#include <iostream>
#include "MTDstVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

class MTDstBCV : public MTDstVeto
{
public:
  MTDstBCV( Int_t runID, Int_t userFlag );
  ~MTDstBCV();
  void SetNumFromModID();
  void SetModIDToChannel();
  void Process() {Process2();}
  void Process2();
private:
  float GetHitTime( float frontTime, float rearTime );
  float GetHitZ(    float frontTime, float rearTime );
  float GetEne(     float frontEne,  float rearEne, 
		    float frontTime, float rearTime);
  const Int_t m_userFlag;
};

#endif //__MTDSTBCV_H__
