#ifndef __MTDSTIBCV_H__
#define __MTDSTIBCV_H__

#include <cmath>
#include <iostream>
#include "MTDstVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

class MTDstIBCV : public MTDstVeto
{
 public:
  MTDstIBCV( Int_t runID, Int_t userFlag );
  ~MTDstIBCV();
  void SetNumFromModID();
  void SetModIDToChannel();
  void Process() {Process2();}
  void Process2();

 private:
  float GetHitTime( float frontTime, float rearTime );
  float GetHitZ( float frontTime, float rearTime );
  float GetEne( float frontEne, float rearEne, float frontTime, float rearTime);
};

#endif //__MTDSTIBCV_H__
