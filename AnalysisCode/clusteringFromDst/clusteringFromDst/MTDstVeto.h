#ifndef __MTDSTVeto_H__
#define __MTDSTVeto_H__

#include <iostream>
#include "MTDstDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTDstVeto : public MTDstDetector
{
  
 public:
  MTDstVeto( Int_t detID, Int_t runID, Int_t userFlag );
  virtual void Process(){Process2();}
  virtual void Process2();
  virtual void Initialize();

 protected:
  const Int_t m_userFlag;
  

 
};

#endif //__MTDSTVeto_H__
