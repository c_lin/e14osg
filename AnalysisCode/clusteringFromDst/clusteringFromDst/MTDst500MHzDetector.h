#ifndef __MTDST500MHzDetector_H__
#define __MTDST500MHzDetector_H__

#include <iostream>
#include "MTDstDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTDst500MHzDetector : public MTDstDetector
{
public:
  MTDst500MHzDetector(Int_t detID, Int_t runID, Int_t userFlag );

  virtual void Initialize();
  virtual void Process();
  virtual void Process2();

private:
   
  const Int_t m_userFlag;
};

#endif //__MTDST500MHzDetector_H__
