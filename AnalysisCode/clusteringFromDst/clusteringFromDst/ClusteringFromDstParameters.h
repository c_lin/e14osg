#ifndef __CLUSTERINGFROMDSTPARAMETERS_H__
#define __CLUSTERINGFROMDSTPARAMETERS_H__
#include <iostream>
#include <string>
#include <TMath.h>

namespace CFDP{

  enum { CC03,    CC04, CC05, CC06, CBAR, 
	 FBAR,    CSI,  CV,   BCV,  NCC,
	 OEV,     LCV,  BPCV, BHPV, BHCV,
	 newBHCV, BHGC, IB,   IBCV, MBCV,
         DCV, UCV, UCVLG};
  
  const Int_t nDetectors = 23;

  const std::string DetName[nDetectors] = 
    { "CC03",    "CC04", "CC05", "CC06", "CBAR", 
      "FBAR",    "CSI",  "CV",   "BCV",  "NCC",
      "OEV",     "LCV",  "BPCV", "BHPV", "BHCV",
      "newBHCV", "BHGC", "IB",   "IBCV", "MBCV",
      "DCV", "UCV", "UCVLG" };

  const Int_t DetNChannels[nDetectors] = 
    { 32,   64,   60,  60, 128,
      32, 2716,  184,  64, 204,
      44,    4,    4,  34,   8,
      48,    8,   64,  64,  16,
      32, 12, 2};
  const Int_t DetNModules[nDetectors] = 
    { 16,   62,   58,  58,   64,
      32, 2716,   92,  32,  204,
      44,   4,     4,  34,    8,
      48,   8,    32,  32,   16,
      32, 12, 2}; ///// special treatment for BHGC (actually it should be "4" for BHGC)

  const Bool_t Is125MHz[nDetectors] =
    { true,  true,  true,  true,  true,
      true,  true,  true,  true,  true,
      true,  true,  true,  false, false,
      false, false, false, true,  true,
      true, false, true};
  const Bool_t Is500MHz[nDetectors] =
    { false, false, false, false, false,
      false, false, false, false, false,
      false, false, false, true,  true,
      true,  true,  true,  false, false,
      true, true, false};
  
  const Int_t MaxnHit500 = 21;

}
#endif
