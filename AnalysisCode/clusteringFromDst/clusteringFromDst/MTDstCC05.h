#ifndef __MTDSTCC05_H__
#define __MTDSTCC05_H__

#include <iostream>
#include "MTDstVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

class MTDstCC05 : public MTDstVeto
{
public:
  MTDstCC05( Int_t runID, Int_t userFlag );
  void SetNumFromModID();
  void SetModIDToChannel();
  void Process() {Process2();}
  void Process2();
  void Initialize();
  
private:
  float GetHitTime( float PMTTime1, float PMTTime2 ){ return ( PMTTime1 + PMTTime2 ) / 2.; }
  float GetEne(     float PMTEne1,  float PMTEne2 ){  return ( PMTEne1  + PMTEne2  ) / 2.; }
};

#endif //__MTDSTCC05_H__
