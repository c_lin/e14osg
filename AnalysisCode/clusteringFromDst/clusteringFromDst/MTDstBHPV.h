#ifndef __MTDSTBHPV_H__
#define __MTDSTBHPV_H__

#include <iostream>
#include "MTDst500MHzDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

class MTDstBHPV : public MTDst500MHzDetector
{
public:
  MTDstBHPV( Int_t runID, Int_t userFlag );
  void SetNumFromModID();
  void SetModIDToChannel();

private:
};

#endif //__MTDSTBHPV_H__
