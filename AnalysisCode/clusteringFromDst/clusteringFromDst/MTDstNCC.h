#ifndef __MTDSTNCC_H__
#define __MTDSTNCC_H__

#include <iostream>
#include "MTDstVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTDstNCC : public MTDstVeto
{
public:
  MTDstNCC( Int_t runID, Int_t userFlag );
    
  float GetNCCTime(float time, float ene, int modID);
  void Process(){Process2();}
  void Process2();
private:
  
 protected:
  float NCCCorrectionParameter[600][5];  
};

#endif //__MTDSTNCC_H__
