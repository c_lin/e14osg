#ifndef __MTDSTUCV_H__
#define __MTDSTUCV_H__

#include <iostream>
#include "MTDst500MHzDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

class MTDstUCV : public MTDst500MHzDetector
{
public:
  MTDstUCV( Int_t runID, Int_t userFlag );
  ~MTDstUCV();

  void SetBranchAddress(TTree* tree);
  void Branch( TTree* tree );

  Float_t (*DetFallTime500)[20];
  Float_t (*DetPeakTime500)[20];
private:
};

#endif //__MTDSTUCV_H__
