#ifndef __MTDSTCV_H__
#define __MTDSTCV_H__

#include <fstream>
#include <iostream>
#include "MTDstVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

class MTDstCV : public MTDstVeto
{
public:
  MTDstCV( Int_t runID, Int_t userFlag );
  ~MTDstCV();
  void Branch( TTree* tr );
  Float_t (*CVModuleRiseTime)[2];
  Float_t (*CVModuleFallTime)[2];
  void SetNumFromModID();
  void SetModIDToChannel();
  void Process() {Process2();}
  void Process2();
  void Initialize();
  
private:
  float GetHitTime( float shortSideTime, float longSideTime ){ return ( shortSideTime + longSideTime ) / 2.; }
  float GetEne(     float shortSideEne,  float longSideEne ){  return    shortSideEne + longSideEne; }
 
};

#endif //__MTDSTCV_H__
