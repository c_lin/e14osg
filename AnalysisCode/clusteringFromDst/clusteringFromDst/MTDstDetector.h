#ifndef __MTDSTDETECTOR_H__
#define __MTDSTDETECTOR_H__

#include <iostream>
#include <TROOT.h>
#include <TTree.h>
#include <TString.h>

#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "ClusteringFromDstParameters.h"
#include "DSTETCorrector/EnergyCorrector.h"
#include "DSTETCorrector/TimingShiftCorrector.h"

class MTDstDetector
{
 public:
  MTDstDetector( Int_t detID, Int_t runID );
  ~MTDstDetector();

  static const Int_t maxhit = 20;
  virtual void SetBranchAddress( TTree* tree );
  void DstBranch( TTree* tree );
  virtual void Branch( TTree* tree );
  virtual void Initialize();
  virtual void Process() = 0;
  virtual void Process2() = 0;
  void Apply_PS(int option);
  void FillPSBit();
  
  void  SetDataMCFlag(Bool_t isDataFlag){ isRealData = isDataFlag; }
  void  SetNodeFileID( const Int_t nodeid, const Int_t fileid );

  void AddSuppFTTBranch( TTree *otree );
  void FillSuppFTTVars();
  void FillWfm();


  // MC true information 
  /* 
     For single readout detectors : same as MC.
     For dual readout detectors : oposite channel has 0.
     For OEV : sum of 2 modules output which are read by 1 PMT.
     For CC04 : not decided yet.
   */
  Int_t    DetTrueNumber;
  Int_t*   DetTrueModID;
  Int_t*   ClockShift;
  Float_t* DetTrueEne;
  Float_t* DetTrueTime;
  
  Int_t    GetFlagWfmCorrupted(void){return Wfm_corrupted;} 
private:
  
  int ModID2Index(int detID, int ModID);
  int ReArrangeArray(int detID);
  
protected:
  TTree* DstTree;
  const Int_t   RunID;
  const Int_t   DetectorID;
  const Char_t *DetectorName;
  const Int_t   DetectorNCH;
  const Int_t   DetectorNMod;
  const Bool_t  is125MHz;
  
  Int_t   Wfm_corrupted;
  Float_t Wfm_ProdDer2threshold;
  Float_t Wfm_Smoothnessthreshold;
  
  void  ApplyEnergyCorrection();
  EnergyCorrector *ECorrector;
  
  void  ApplyTimingCorrection();
  TimingShiftCorrector *TSCorrector;

  TTree *ClusterTree;
  Bool_t isRealData;

  ///// for DST branches
  Int_t    DetNumber;
  Int_t   *DetModID;
  Float_t *DetIntADC;
  Float_t *DetEne;
  Float_t *DetTime;
  Float_t *DetFallTime;
  Float_t *DetPTime;
  Float_t *DetFTTChisq;
  Float_t *DetFTTTime;
  Float_t *DetSmoothness;
  Float_t *DetProdDer2;
  Float_t *DetAreaR;
  short   *DetWfmCorrectNumber;
  Short_t *DetPSbit;
  Short_t *DetPSbit_rearranged;


  /// for 500MHz detectors
  Short_t  *DetNHit500;
  Float_t (*DetEne500)[20];
  Float_t (*DetTime500)[20];
  Float_t (*DetAreaR500)[20];

  
  // for wfm data
  short (*DetSuppWfm125S)[MTBP::nSample125];
  short (*DetSuppWfm500S)[MTBP::nSample500];
  
  //// for wfm MC ////
  Float_t (*DetSuppWfm125)[MTBP::nSample125];
  Float_t (*DetSuppWfm500)[MTBP::nSample500];
  int      DetSuppWfmNumber;
  int     *DetSuppWfmModID;
  
  Float_t  *AccidentalDetECalibConst;
  Float_t  *AccidentalDetTCalibConst;

  ///// temporary variables
  Int_t    detnumber;
  Int_t   *detmodid;
  Float_t *detintadc;
  Float_t *detene;
  Float_t *dettime;
  Float_t *detptime;
  Float_t *detfalltime;
  Float_t *detareaR;
  Float_t *detsmoothness;
  Float_t *detprodder2;
  short   *detwfmcorrectnumber;
  /// for 500MHz detectors
  Short_t  *detnhit500;
  Float_t (*detene500)[20];
  Float_t (*dettime500)[20];
  Float_t (*detareaR500)[20];

  ///// for Clustering branches
  Int_t    DetModuleNumber;
  Int_t   *DetModuleModID;
  Float_t *DetModuleEne;
  Float_t *DetModuleHitTime;
  Float_t *DetModuleHitZ;
  Float_t *DetModuleSmoothness;
  Float_t *DetModuleProdDer2;
  Float_t *DetModuleAreaR;
  short   *DetModuleWfmCorrectNumber;
  /// for 500MHz detectors
  Short_t  *DetModulenHits500;
  Float_t (*DetModuleEne500)[20];
  Float_t (*DetModuleHitTime500)[20];
  Float_t (*DetModuleAreaR500)[20];

  ///
  bool m_isGetFTTBranch;  
  bool m_isSetFTTBranch;

  Int_t    m_outFTTNumber;
  Int_t   *m_outFTTModId;
  Float_t *m_outFTTChisq;
  Float_t *m_outFTTTime;

  virtual void SetNumFromModID();
  virtual void SetModIDToChannel();

  Int_t* NumFromModID;
  Int_t* ModIDFromChannel;


};

#endif //__MTDSTDETECTOR_H__
