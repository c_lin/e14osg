#include <fstream>
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <cstdlib>
#include <algorithm>
#include <string>

#include <TRandom3.h>

#include "gsim2dst/MTConvertLCV.h"

const Char_t* PositionDependenceDataFile[] = { "LCVPositionCorrection/north.dat",
					       "LCVPositionCorrection/top.dat",
					       "LCVPositionCorrection/south.dat",
					       "LCVPositionCorrection/bottom.dat" };
//const Char_t* LocationOfPosDepDataFile = "/home/had/maeday/work/analysis/AnalysisLibrary1212/gsim2dst/src/";

MTConvertLCV::MTConvertLCV( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::LCV, userFlag )
{
  m_detname="LCV";
  for( unsigned int index=0; index<(UInt_t)MTBP::DetNChannels[MTBP::LCV]; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index ) );
    IndexIDArray[index] = index;
  }
  
  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = ich;
    DetTrueModID[ich] = ich;
  }

  WfmTimeShift = MTBP::LCVWfmTimeShift;
  WfmSigma0 = MTBP::LCVWfmSigma0;
  WfmAsymPar = MTBP::LCVWfmAsymPar;

  pulseGenerator = new MTPulseGenerator();
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  Initialize();
  
  m_PositionCorrectionVec = new std::vector<LCVPositionCorrectionData>[DetNumber];
  for( Int_t iCh=0 ; iCh<DetNumber ; iCh++ ){
    std::ifstream ifs( std::string( std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
				    + std::string("/gsim2dst/LCV/")
				    + std::string(PositionDependenceDataFile[iCh]) ).c_str(), std::ios::in );
    LCVPositionCorrectionData PositionCorrectionData;
    while( ifs.good() ){
      ifs >> PositionCorrectionData.m_zPos >> PositionCorrectionData.m_RelOutput
	  >> PositionCorrectionData.m_RelOutputError;
      if( ifs.eof() ) break;
      m_PositionCorrectionVec[iCh].push_back( PositionCorrectionData );
    }    
    
    if( m_PositionCorrectionVec[iCh].size()<2 ){
      std::cout << "ERROR in MTConvertLCV : fail to set position correction data for ch"
		<< IndexIDArray[iCh] << std::endl;
      m_PositionCorrectionVec[iCh].clear();
      PositionCorrectionData.m_zPos = 0.;
      PositionCorrectionData.m_RelOutput = 1.;
      PositionCorrectionData.m_RelOutputError = 0.;
      m_PositionCorrectionVec[iCh].push_back( PositionCorrectionData );      
      PositionCorrectionData.m_zPos = 1.;
      m_PositionCorrectionVec[iCh].push_back( PositionCorrectionData );      
    }

    std::sort( m_PositionCorrectionVec[iCh].begin(),
	       m_PositionCorrectionVec[iCh].end() );
  }
}

MTConvertLCV::~MTConvertLCV(){
  
  delete   pulseGenerator;
  delete[] m_PositionCorrectionVec;
  delete   m_E14BP;
}

void MTConvertLCV::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}

void MTConvertLCV::ConvertFromMTimeWithResponse( void )//added at 26th Dec, 2013
{
  GetFromMTimeWithResponse( DetNumber, DetModID, DetEne, DetTime );
}


void MTConvertLCV::GetTrueInfo( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();
  
  number = DetectorNCH;
  for( Int_t ich = 0 ; ich < number ; ++ich ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }
  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    ene[digi->modID]   = (float)digi->energy;
    time[digi->modID]  = (float)digi->time /8.; // [ns]->[clock]
  }
}

void MTConvertLCV::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )//added at 26th Dec, 2013
{
  TClonesArray *digiArray = GetDigiArray();
  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector<pEnergyTime> propagatedVec;
  
  number = DetectorNCH;
  for( int iCh=0; iCh<number ; iCh++ ) modID[iCh] = IndexIDArray[iCh];
  
  // mtime loop
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t iDigi=0 ; iDigi<nDigi ; iDigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( iDigi );
    propagatedVec.clear();
    
    Int_t DigiModID = digi->modID;
    Int_t index = IDIndexMap[DigiModID];
    
    for( UInt_t imtime=digi->mtimeEntry ; imtime<digi->mtimeEntry+digi->mtimeSize ; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      Double_t zPosFromUpstreamEdge = mtime->r.z()-MTBP::LCVUpstreamZ;
      Double_t PropLength = MTBP::LCVZLength - zPosFromUpstreamEdge;
      Double_t AttenEne = mtime->energy * GetPositionCorrection( DigiModID, zPosFromUpstreamEdge );
      Double_t CorrectedEne = AttenEne*(1+MTBP::BirksConst*MTBP::BHCVMIPEnergy/MTBP::BHCVModThickness)/(1+MTBP::BirksConst*AttenEne/MTBP::BHCVModThickness);//add quenching effect;currently set to be 0 to disable this effect
      Double_t PropTime = mtime->time + PropLength/MTBP::LCVLightPropSpeed;
      
      energyTime.energy = CorrectedEne;
      energyTime.time = PropTime;
      propagatedVec.push_back( energyTime );
    }
    
    if( !propagatedVec.empty() ){
      pulseGenerator->SetEnergyTime( propagatedVec );
      
     Float_t SmearedEnergy = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy();
      if( MTBP::LCVLightYieldPerMeV[index]>0 ){
	Int_t nPE = gRandom->Poisson( SmearedEnergy*MTBP::LCVLightYieldPerMeV[index] );
	SmearedEnergy = (Float_t)(nPE)/MTBP::LCVLightYieldPerMeV[index];
	//Float_t OnePhotonSmearing = gRandom->Gaus( 0, TMath::Sqrt(nPE)*MTBP::LCVOnePhotonSigma ); 
	//SmearedEnergy += OnePhotonSmearing;
      }
      
      ene[index] = SmearedEnergy;
      time[index] = (Float_t)pulseGenerator->GetPseudoConstantFractionTime()/8.;//[ns]-->[clock]
      
      Float_t EnergyForTimeSmearing = ( ene[index]<MTBP::LCVMinEnergyTimeSmearing
					? MTBP::LCVMinEnergyTimeSmearing : ene[index] );
      Float_t TimeSmearing = gRandom->Gaus( 0, TMath::Hypot( MTBP::LCVTimeRes_a,
							     MTBP::LCVTimeRes_b/TMath::Sqrt(EnergyForTimeSmearing) ) );
      time[index] += TimeSmearing;
    }
  }
  
  return;
}

void MTConvertLCV::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
//added at 15th Mar, 2014
//updated at 4th May, 2014 (PTime and FallTime added)
{
  number = DetectorNCH;

  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();
  
  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  
  WfmFunc->SetParameter( 2, WfmSigma0 );
  WfmFunc->SetParameter( 3, WfmAsymPar );

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    propagatedVec.clear();
    
    //modIndex = pHandler->GetCVModIndex( digi->modID );
    
    Int_t ModID = digi->modID;
    Int_t iMod = IDIndexMap[ModID];
    
    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry ; imtime<digi->mtimeEntry+digi->mtimeSize ; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Ignore over 1us hit
      if( mtime->time > 1000 ) continue;
      
      Double_t zPosFromUpstreamEdge = mtime->r.z()-MTBP::LCVUpstreamZ;
      Double_t PropLength = MTBP::LCVZLength - zPosFromUpstreamEdge;
      Double_t AttenEne = mtime->energy * GetPositionCorrection( ModID, zPosFromUpstreamEdge );
      Double_t CorrectedEne = AttenEne*(1+MTBP::BirksConst*MTBP::BHCVMIPEnergy/MTBP::BHCVModThickness)/(1+MTBP::BirksConst*AttenEne/MTBP::BHCVModThickness);//add quenching effect;currently set to be 0 to disable this effect
      Int_t nPE = gRandom->Poisson( CorrectedEne*MTBP::LCVLightYieldPerMeV[iMod] );
      CorrectedEne = (Float_t)(nPE)/MTBP::LCVLightYieldPerMeV[iMod];//consider photon statistics
      //Float_t OnePhotonSmearing = gRandom->Gaus( 0, TMath::Sqrt(nPE)*MTBP::LCVOnePhotonSigma ); 
      //CorrectedEnergy += OnePhotonSmearing;
      
      Double_t PropTime = mtime->time + PropLength/MTBP::LCVLightPropSpeed;
      Float_t EnergyForTimeSmearing = ( CorrectedEne<MTBP::LCVMinEnergyTimeSmearing
					? MTBP::LCVMinEnergyTimeSmearing : CorrectedEne );
      Float_t TimeSmearing = gRandom->Gaus( 0, TMath::Hypot( MTBP::LCVTimeRes_a,
							     MTBP::LCVTimeRes_b/TMath::Sqrt(EnergyForTimeSmearing) ) );
      PropTime += (TimeSmearing*8.);//timing resolution
      
      energyTime.energy = CorrectedEne;
      energyTime.time = PropTime;
      propagatedVec.push_back( energyTime );
    }//mtime

    GenerateWfmFromMTimeVec( iMod, propagatedVec );
  }// idigi
  
  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<number; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
    }
  }
  
  return;
}

Double_t MTConvertLCV::GetPositionCorrection( Int_t modID, Double_t zPosFromUpstream ){//added at 26th Dec, 2013
  
  Int_t index = IDIndexMap[modID];
  
  std::vector<LCVPositionCorrectionData>::iterator itUp = m_PositionCorrectionVec[index].begin();
  std::vector<LCVPositionCorrectionData>::iterator itTmp = itUp;
  std::vector<LCVPositionCorrectionData>::iterator itDown = ++itTmp;
  while( itDown!=m_PositionCorrectionVec[index].end() ){
    if( (itDown->m_zPos)>zPosFromUpstream ) break;
    else{
      itUp++;
      itDown++;
    }
  }
  
  Double_t PositionCorrection = ( itUp->m_RelOutput + ((itDown->m_RelOutput-itUp->m_RelOutput)
						       *(zPosFromUpstream-itUp->m_zPos)
						       /(itDown->m_zPos-itUp->m_zPos)) );
  return PositionCorrection;
}

