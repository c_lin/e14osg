#include "gsim2dst/MTConvertIBAR.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTConvertIBAR::MTConvertIBAR( Int_t userFlag ) : MTConvert500MHzDetector( MTBP::IB, userFlag )
{
   m_detname="IB";
  for( int index=0; index<MTBP::DetNChannels[MTBP::IB]/2; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index) );
    IndexIDArray[index] = index;
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index+100, index+32) );
    IndexIDArray[index+32] = index+100;
  }
  
  DetNumber        = DetectorNCH;
  DetUpdatedNumber = DetectorNCH;
  DetTrueNumber    = DetectorNCH;

  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich]        = IndexIDArray[ich];
    DetUpdatedModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich]    = IndexIDArray[ich];
  }

  pulseGenerator = new MTPulseGenerator();
  
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  Initialize();
  //  MCEventTiming = 0;

}

MTConvertIBAR::~MTConvertIBAR()
{
  delete pulseGenerator;
  delete m_E14BP;
}

void MTConvertIBAR::Initialize( void )
{
  int maxhit=MTBP::NMaxHitPerEvent;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetNHit[ich]        = 0;
    DetUpdatedNHit[ich] = 0;
    for(int ihit=0; ihit<maxhit; ihit++){
      DetEne[ich][ihit]         = MTBP::Invalid;
      DetTime[ich][ihit]        = MTBP::Invalid;
      DetUpdatedEne[ich][ihit]  = MTBP::Invalid;
      DetUpdatedTime[ich][ihit] = MTBP::Invalid;

      DetTrueEneTmp[ich][ihit] = 0;//added at 16th Mar, 2016
      DetTrueTimeTmp[ich][ihit] = 0;//added at 16th Mar, 2016
    }
    for( Int_t iSample=0 ; iSample<MTBP::nSample500 ; iSample++ )
      DetWfm[ich][iSample] = 0;
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
    
    DetTrueNHit[ich] = 0;
  }
}

void MTConvertIBAR::ConvertFromDigiWithResponse( void ){//added at 25th Nov, 2013
  
  GetFromDigiWithResponse( DetNumber, DetModID, DetNHit, DetEne, DetTime );
}

void MTConvertIBAR::GetTrueInfo( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] )
{
  TClonesArray *digiArray = GetDigiArray();
  
  number = DetectorNCH;
  
  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    Int_t index = IDIndexMap[digi->modID];
    Int_t iHit = nhit[index];
    ene[index][iHit] = (float)digi->energy;
    time[index][iHit] = (float)digi->time /MTBP::SampleInterval500; //[ns]->[clock]
    nhit[index]++;
  } 
}

void MTConvertIBAR::GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] ){
  TClonesArray *digiArray = GetDigiArray();
  
  number = DetectorNCH;
  for( Int_t iCh=0 ; iCh<number ; iCh++ ) modID[iCh] = IndexIDArray[iCh];
  
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t idigi=0 ; idigi<nDigi ; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    Int_t ModID = digi->modID;
    Int_t index = IDIndexMap[ModID];
    Float_t TimeSmearing = 0;//[clock]

    ene[index][0] = digi->energy;
    time[index][0] = digi->time/MTBP::SampleInterval500 + TimeSmearing;//[ns]->[clock]
    time[index][0] += AccidentalTimeShift;
    nhit[index]++;
  }
  
  return;
}

void MTConvertIBAR::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] ){

  number = DetNumber;
  int maxhit=MTBP::NMaxHitPerEvent;
  for( int ich=0; ich<DetNumber; ich++ ){
    modID[ich] = IndexIDArray[ich];
    for( int ihit=0; ihit<maxhit; ihit++ ){
      ene[ich][ihit] = 0;
      time[ich][ihit] = 0;
    }
  }

  TClonesArray *digiArray = GetDigiArray();
  TClonesArray *mtimeArray = GetMTimeArray();

  // digi loop
  Int_t nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );

    Int_t ModID = digi->modID;
    Int_t index_U = IDIndexMap[ModID];
    Int_t index_D = IDIndexMap[ModID+100];
    Int_t iHit  = nhit[ModID];
    //    Float_t TimeSmearing = 0;//[clock]

    double Energy_U = 0;
    double Energy_D = 0;
    double Time_U = 0;
    double Time_D = 0;

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // Ignore over 1us hit
      if( mtime->time > 1000 ) continue;

      // front
      double TmpEnergy = GetFrontVisibleEnergy( mtime->r.z(), mtime->energy );
      Energy_U += TmpEnergy;
      Time_U   += GetFrontVisibleTime( mtime->r.z(), mtime->time ) * TmpEnergy;

      // rear
      TmpEnergy = GetRearVisibleEnergy( mtime->r.z(), mtime->energy );
      Energy_D += TmpEnergy;
      Time_D   += GetRearVisibleTime( mtime->r.z(), mtime->time ) * TmpEnergy;

    } // imtime

    ene[index_U][iHit]  = Energy_U;
    time[index_U][iHit] = Time_U / Energy_U / 2; // [clock]    
    time[index_U][iHit] += AccidentalTimeShift;
    nhit[index_U]++;
    
    ene[index_D][iHit] = Energy_D;
    time[index_D][iHit] = Time_D / Energy_D / 2; // [clock]
    time[index_D][iHit] += AccidentalTimeShift;
    nhit[index_D]++;

    // dead channel
    if( (20170401 < m_userFlag && m_userFlag < 20190201) || (m_userFlag>20210101) ){ // Run69-Run79 || >=Run86
      for(int ideadch=0;ideadch<MTBP::IBNDeadChannels;ideadch++){
	if(index_U==MTBP::IBDeadChannelID[ideadch] || index_D==MTBP::IBDeadChannelID[ideadch]){
	  ene[MTBP::IBDeadChannelID[ideadch]][iHit]=0;
	  time[MTBP::IBDeadChannelID[ideadch]][iHit]=0;
	  nhit[MTBP::IBDeadChannelID[ideadch]]=0;
	}
      }
    }

  } // idigi
  
  return;
}

void MTConvertIBAR::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Short_t* nhit,
						Float_t (*ene)[MTBP::NMaxHitPerEvent], 
						Float_t (*time)[MTBP::NMaxHitPerEvent],
						Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit,
						Float_t (*updatedene)[MTBP::NMaxHitPerEvent], 
						Float_t (*updatedtime)[MTBP::NMaxHitPerEvent] )
{  
  number        = DetNumber;
  updatednumber = DetNumber;
  int maxhit=MTBP::NMaxHitPerEvent;
  for( int ich=0; ich<DetNumber; ich++ ){
    modID[ich]        = IndexIDArray[ich];
    updatedmodID[ich] = IndexIDArray[ich];
    for( int ihit=0; ihit<maxhit; ihit++ ){
      ene[ich][ihit]         = 0;
      time[ich][ihit]        = 0;
      updatedene[ich][ihit]  = 0;
      updatedtime[ich][ihit] = 0;
    }
  }

  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedFrontVec;
  std::vector< pEnergyTime > propagatedRearVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    propagatedFrontVec.clear();
    propagatedRearVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;

      // front
      Double_t CorrectedEne_Front = GetFrontVisibleEnergy( mtime->r.z(), mtime->energy );
      //// photo-static fluctuation ////
      Int_t nPE_Front = gRandom->Poisson( CorrectedEne_Front * MTBP::IBLightYieldPerMeV_Front );
      CorrectedEne_Front = (Float_t)(nPE_Front)/MTBP::IBLightYieldPerMeV_Front;
      if( nPE_Front==0 ) CorrectedEne_Front = 1e-6; // set small value to avoid TSpline3 Eval error

      energyTime.energy = CorrectedEne_Front;
      energyTime.time   = GetFrontVisibleTime( mtime->r.z(), mtime->time );
      propagatedFrontVec.push_back( energyTime );
      
      // rear
      Double_t CorrectedEne_Rear = GetRearVisibleEnergy( mtime->r.z(), mtime->energy );
      //// photo-static fluctuation ////
      Int_t nPE_Rear = gRandom->Poisson( CorrectedEne_Rear * MTBP::IBLightYieldPerMeV_Rear );
      CorrectedEne_Rear = (Float_t)(nPE_Rear)/MTBP::IBLightYieldPerMeV_Rear;
      if( nPE_Rear==0 ) CorrectedEne_Rear = 1e-6; // set small value to avoid TSpline3 Eval error

      energyTime.energy = CorrectedEne_Rear;
      energyTime.time   = GetRearVisibleTime( mtime->r.z(), mtime->time );
      propagatedRearVec.push_back( energyTime );
    }

    // timing resolution (2019/06/15 added by S.Shinohara based on Murayama-san's PhD study) //
    Float_t TimeSmearing_Front = 0;
    Float_t TimeSmearing_Rear  = 0;
    if( !propagatedFrontVec.empty() ){
      pulseGenerator->SetEnergyTime( propagatedFrontVec );
      Float_t EnergyForTimeSmearing = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy(-1,1000);
      if( EnergyForTimeSmearing<0.1 ) EnergyForTimeSmearing = 0.1;
      Float_t SmearingSigma = sqrt( 
				   TMath::Power( MTBP::IBTimeResPar_a/(EnergyForTimeSmearing*MTBP::IBLightYieldPerMeV_Front), 2) 
				   + TMath::Power(  MTBP::IBTimeResPar_b/sqrt(EnergyForTimeSmearing*MTBP::IBLightYieldPerMeV_Front), 2) 
				   + TMath::Power(  MTBP::IBTimeResPar_c, 2) );
      TimeSmearing_Front = gRandom->Gaus( 0, SmearingSigma );
    }
    if( !propagatedRearVec.empty() ){
      pulseGenerator->SetEnergyTime( propagatedRearVec );
      Float_t EnergyForTimeSmearing = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy(-1,1000);
      if( EnergyForTimeSmearing<0.1 ) EnergyForTimeSmearing = 0.1;
      Float_t SmearingSigma = sqrt( 
				   TMath::Power( MTBP::IBTimeResPar_a/(EnergyForTimeSmearing*MTBP::IBLightYieldPerMeV_Rear), 2) 
				   + TMath::Power(  MTBP::IBTimeResPar_b/sqrt(EnergyForTimeSmearing*MTBP::IBLightYieldPerMeV_Rear), 2) 
				   + TMath::Power(  MTBP::IBTimeResPar_c, 2) );
      TimeSmearing_Rear = gRandom->Gaus( 0, SmearingSigma );
    }

    // front
    Float_t TimeShiftCorrection_Front = TimeSmearing_Front;
    WfmFunc->SetParameter( 0, MTBP::IBWfmPeakHeight_U );
    WfmFunc->SetParameter( 2, MTBP::IBWfmSigma0_U );
    WfmFunc->SetParameter( 3, MTBP::IBWfmAsymPar_U );
    propagatedClusteredVec.clear();

    const Double_t k_Pitch = (m_dbWfmGenerator) ? 2. : 0.1;    

    if( !propagatedFrontVec.empty() ){
      ClusteringMtime( propagatedFrontVec, propagatedClusteredVec, k_Pitch );
      if( m_dbWfmGenerator )
	GenerateDbWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec, TimeShiftCorrection_Front);
      else
	GenerateWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec);
    }

    // rear
    Float_t TimeShiftCorrection_Rear = TimeSmearing_Rear;
    WfmFunc->SetParameter( 0, MTBP::IBWfmPeakHeight_D );
    WfmFunc->SetParameter( 2, MTBP::IBWfmSigma0_D );
    WfmFunc->SetParameter( 3, MTBP::IBWfmAsymPar_D );
    propagatedClusteredVec.clear();

    if( !propagatedRearVec.empty() ){    
      ClusteringMtime( propagatedRearVec, propagatedClusteredVec, k_Pitch );
      if( m_dbWfmGenerator )
	GenerateDbWfmFromMTimeVec( IDIndexMap[digi->modID+100], propagatedClusteredVec, TimeShiftCorrection_Rear);
      else
	GenerateWfmFromMTimeVec( IDIndexMap[digi->modID+100], propagatedClusteredVec);
    }

  }// idigi

  if( !EnableAccidentalOverlay ){
    for( Int_t iCh=0 ; iCh<number ; iCh++ ){
      /* //obsolete (20190517)
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], nhit[iCh], 
			    &ene[iCh][0], &time[iCh][0], iCh );
      */
      GetEnergyTimeFromAveragedWfm( &DetWfm[iCh][0], updatednhit[iCh], 
				    &updatedene[iCh][0], &updatedtime[iCh][0], iCh, &DetPTime[iCh][0]);
      EvalFTTChisq(iCh, &DetWfm[iCh][0], DetFTTChisq[iCh], DetFTTTime[iCh]);
    }
  }    

  return;
}


double MTConvertIBAR::GetFrontVisibleEnergy( double z, double e)
{
  double deltaZ = z - ( 4098 + 277.5 ); // z is absolute position. delta z is normalized at the center of IB ( - MB + IB ).  
  return e / 2. * exp( -deltaZ / ( MTBP::IBLAMBDA_U + MTBP::IBALPHA_U * deltaZ ) );
}


double MTConvertIBAR::GetRearVisibleEnergy( double z, double e)
{
  double deltaZ = z - ( 4098 + 277.5 );
  return e / 2. * exp( deltaZ / ( MTBP::IBLAMBDA_D - MTBP::IBALPHA_D * deltaZ ) );
}


double MTConvertIBAR::GetFrontVisibleTime( double z, double t )
{
  double deltaZ = z - ( 4098 + 277.5 );
  return t + (MTBP::IBLength/2+deltaZ)/MTBP::IBPropVelo - MTBP::IBLength/2/MTBP::IBPropVelo;
}

double MTConvertIBAR::GetRearVisibleTime( double z, double t )
{
  double deltaZ = z - ( 4098 + 277.5 );
  return t + (MTBP::IBLength/2-deltaZ)/MTBP::IBPropVelo - MTBP::IBLength/2/MTBP::IBPropVelo;
}


