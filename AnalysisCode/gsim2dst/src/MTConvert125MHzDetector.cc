#include "gsim2dst/MTConvert125MHzDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

#include "LcWfm/LcWfm.h"

MTConvert125MHzDetector::MTConvert125MHzDetector( Int_t detID, Int_t userFlag ):DetectorID( detID ),
										DetectorName( MTBP::detectorName[detID].c_str() ),
										DetectorNCH(  MTBP::DetNChannels[detID] ),
										m_userFlag( userFlag )
{
  
  if(userFlag>=20190101 && DetectorID==MTBP::CSI){
	  DetectorNCH = 2716 + 256;
  }

  if(userFlag>=20200101 && DetectorID==MTBP::UCV) DetectorNCH = 12;
  if(userFlag>=20200101 && DetectorID==MTBP::UCVLG) DetectorNCH = 2;

	
	
  DetectorEventData = new GsimDetectorEventData();
  
  DetNumber   = 0;
  DetModID    = new Int_t  [DetectorNCH];
  DetEne      = new Float_t[DetectorNCH];
  DetTime     = new Float_t[DetectorNCH];
  DetPTime    = new Float_t[DetectorNCH];
  DetFallTime = new Float_t[DetectorNCH];
  DetAreaR 	  = new Float_t[DetectorNCH];
  DetSmoothness = new Float_t[DetectorNCH];
  DetWfm      = new Float_t[DetectorNCH][MTBP::nSample125];
  DetWfmCorrectNumber = new short[DetectorNCH];
  DetFTTChisq = new Float_t[DetectorNCH]; 
  DetFTTTime  = new Float_t[DetectorNCH]; 

  DetSuppWfm = new Float_t[DetectorNCH][MTBP::nSample125];
  DetSuppWfmModID = new Int_t[DetectorNCH];
  DetSuppWfmNumber = 0;

  nSampleAverage = 5; //needs to be odd number
  nSampleOneSide = (nSampleAverage-1)/2;
  
  DetTrueNumber = 0;
  DetTrueModID  = new Int_t  [DetectorNCH];
  DetTrueEne    = new Float_t[DetectorNCH];
  DetTrueTime   = new Float_t[DetectorNCH];

  IndexIDArray  = new Int_t  [DetectorNCH];

  EnableAccidentalOverlay = true;//default
  AccidentalEntryID       = 0;
  AccidentalTimeShift     = 0;
  
  //for waveform analysis 
  WfmFunc = new TF1( std::string( std::string("WfmFunc") + MTBP::detectorName[detID] ).c_str(),
		     "[0]*exp(-TMath::Power((x-[1])/([2]+[3]*(x-[1])),2.))" );
  DetPeakHeightThre  = MTBP::CSIMinPeakHeight;//default value
  DetNominalPeakTime = MTBP::CSINominalPeakTime;//default value

  AccidentalDetModID       = new Int_t   [DetectorNCH];
  AccidentalDetEne         = new Float_t [DetectorNCH];
  AccidentalDetTime        = new Float_t [DetectorNCH];
  AccidentalPedestal       = new Float_t [DetectorNCH];
  AccidentalWfm            = new Short_t [DetectorNCH][MTBP::nSample125];
  AccidentalDetECalibConst = new Float_t [DetectorNCH];
  AccidentalDetTCalibConst = new Float_t [DetectorNCH]; 

  EtSumOnlineTrig = 0;

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();  

  E14BPConfFile = "";
  NominalAverageClusterTime_Data = 215; //[ns], default
  NominalAverageClusterTime_MC   = 215; //[ns], default
  DetZPosition = -9999;
  NDeadChannel = 0;
  DeadChannelIDVec.clear();
  NDeadModule  = 0;
  DeadModuleIDVec.clear();
  DetSmearingSigma = 0;
  
  for(int ich=0;ich<4096;ich++){
    DetChByChNominalTime[ich]=9999;
    DetChByChMinPeakHeight[ich]=9999;
  }  
	
}


MTConvert125MHzDetector::~MTConvert125MHzDetector()
{
  delete DetectorEventData;
  
  delete[] DetModID;
  delete[] DetEne;
  delete[] DetTime;
  delete[] DetPTime;
  delete[] DetFallTime;
  delete[] DetAreaR;
  delete[] DetSmoothness;
  delete[] DetWfm;
  delete[] DetWfmCorrectNumber;
  delete   WfmFunc;
  delete [] DetSuppWfm;
  delete [] DetSuppWfmModID;
  
  delete[] DetTrueModID;
  delete[] DetTrueEne;
  delete[] DetTrueTime;
  
  delete[] AccidentalDetModID;
  delete[] AccidentalDetEne;
  delete[] AccidentalDetTime;
  delete[] AccidentalPedestal;
  delete[] AccidentalWfm;
  delete[] AccidentalDetECalibConst;
  delete[] AccidentalDetTCalibConst;
    
  delete[] IndexIDArray;

  delete[] DetChByChNominalTime;
  delete[] DetChByChMinPeakHeight;

  delete   m_E14BP;
}


void MTConvert125MHzDetector::Initialize()
{
  DetNumber     = 0;
  DetTrueNumber = 0;
  for( int ich = 0 ; ich < DetectorNCH ; ich++ ){
    DetModID[ich]     = 0;
    DetEne[ich]       = 0;
    DetTime[ich]      = 0;
    DetPTime[ich]     = MTBP::Invalid;
    DetFallTime[ich]  = MTBP::Invalid;
    DetAreaR[ich] 	  = -9999.;
    DetSmoothness[ich] = 0;
    DetWfmCorrectNumber[ich] = -1;
    DetFTTChisq[ich]  = -4.;
    DetFTTTime[ich]   = -1.;
    for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0;
    
    DetTrueModID[ich] = 0;
    DetTrueEne[ich]   = 0;
    DetTrueTime[ich]  = 0;
  }
  for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; iSample++ )
    DetEtSumWfm[iSample] = 0;
}


TClonesArray* MTConvert125MHzDetector::GetDigiArray(){
  return DetectorEventData->digi;
}

TClonesArray* MTConvert125MHzDetector::GetMTimeArray(){
  return DetectorEventData->mtime;
}

void MTConvert125MHzDetector::SetDetectorEventData(GsimDetectorEventData *DED){
	DetectorEventData = DED;
}

void MTConvert125MHzDetector::SetBranchAddress(TTree* tree)
{
	GsimEventTree = tree;
	if(DetectorID!=MTBP::UCVLG)	GsimEventTree->SetBranchAddress(Form("%s.", DetectorName), &DetectorEventData);
}


void MTConvert125MHzDetector::SetAccidentalBranchAddress( TTree* tree )
{
  
  AccidentalTree = tree;
  if(!AccidentalTree->GetBranch( Form("%sNumber", DetectorName))){
	  std::cout << "Accidental Wfm of " << DetectorName << " was not found!!\n";
	  EnableAccidentalOverlay = false; 
	  for(int i=0;i<DetectorNCH;i++){
		  AccidentalDetECalibConst[i] = 1;
		  AccidentalDetTCalibConst[i] = 0;
	  }
	  return;
  }
  

  if(AccidentalRunID_fillflag==0){
	  std::cout << "Branch of the RunID of the accidental file is set\n"; 
	  AccidentalTree->SetBranchAddress( Form( "RunID",         DetectorName ), &AccidentalRunID );
	  AccidentalRunID_fillflag++;
  }
  AccidentalTree->SetBranchAddress( Form( "%sNumber",      DetectorName ), &AccidentalDetNumber );
  AccidentalTree->SetBranchAddress( Form( "%sModID",       DetectorName ),  AccidentalDetModID );
  AccidentalTree->SetBranchAddress( Form( "%sEne",         DetectorName ),  AccidentalDetEne );
  AccidentalTree->SetBranchAddress( Form( "%sTime",        DetectorName ),  AccidentalDetTime );

  AccidentalTree->SetBranchAddress( Form( "%sPedestal",    DetectorName ),  AccidentalPedestal );
  AccidentalTree->SetBranchAddress( Form( "%sWfm",         DetectorName ),  AccidentalWfm );
  AccidentalTree->SetBranchAddress( Form( "%sECalibConst", DetectorName ),  AccidentalDetECalibConst );
  AccidentalTree->SetBranchAddress( Form( "%sTCalibConst", DetectorName ),  AccidentalDetTCalibConst );
  
  return;
}

void MTConvert125MHzDetector::Branch( TTree* tree )
{
  DstTree = tree;

  DstTree->Branch( Form( "%sNumber",     DetectorName ), &DetNumber,     Form( "%sNumber/I",                  DetectorName ) );
  DstTree->Branch( Form( "%sModID",      DetectorName ),  DetModID,      Form( "%sModID[%sNumber]/I",         DetectorName, DetectorName ) );
  DstTree->Branch( Form( "%sEne",        DetectorName ),  DetEne,        Form( "%sEne[%sNumber]/F",           DetectorName, DetectorName ) );
  DstTree->Branch( Form( "%sTime",       DetectorName ),  DetTime,       Form( "%sTime[%sNumber]/F",          DetectorName, DetectorName ) );
  DstTree->Branch( Form( "%sPTime",      DetectorName ),  DetPTime,      Form( "%sPTime[%sNumber]/F",         DetectorName, DetectorName ) );
  DstTree->Branch( Form( "%sFallTime",   DetectorName ),  DetFallTime,   Form( "%sFallTime[%sNumber]/F",      DetectorName, DetectorName ) );
  DstTree->Branch( Form( "%sSmoothness", DetectorName ),  DetSmoothness, Form( "%sSmoothness[%sNumber]/F",    DetectorName, DetectorName ) );
  DstTree->Branch( Form( "%sWfmCorrectNumber", DetectorName ),  DetWfmCorrectNumber, Form( "%sWfmCorrectNumber[%sNumber]/S", DetectorName, DetectorName ) );
  
  DstTree->Branch( Form( "%sTrueNumber", DetectorName ), &DetTrueNumber, Form( "%sTrueNumber/I",              DetectorName ) );
  DstTree->Branch( Form( "%sTrueModID",  DetectorName ),  DetTrueModID,  Form( "%sTrueModID[%sTrueNumber]/I", DetectorName, DetectorName ) );
  DstTree->Branch( Form( "%sTrueEne",    DetectorName ),  DetTrueEne,    Form( "%sTrueEne[%sTrueNumber]/F",   DetectorName, DetectorName ) );
  DstTree->Branch( Form( "%sTrueTime",   DetectorName ),  DetTrueTime,   Form( "%sTrueTime[%sTrueNumber]/F",  DetectorName, DetectorName ) );

  DstTree->Branch( Form( "%sEtSum",      DetectorName ),  DetEtSumWfm,   Form( "%sEtSum[%d]/L",               DetectorName, MTBP::nSample125 ) );
 
  if(m_WFMsave){
	  DstTree->Branch( Form( "%sWfmNumber", DetectorName ), &DetSuppWfmNumber, Form( "%sWfmNumber/I",  DetectorName ));
	  DstTree->Branch( Form( "%sWfmModID", DetectorName ),   DetSuppWfmModID,  Form( "%sWfmModID[%sWfmNumber]/I",  DetectorName, DetectorName ));
	  DstTree->Branch( Form( "%sWfm",   DetectorName ),      DetSuppWfm,       Form( "%sWfm[%sWfmNumber][%d]", DetectorName, DetectorName, MTBP::nSample125 ));
  }

}

void MTConvert125MHzDetector::AddFTTBranch( TTree* tree )
{
   tree->Branch(Form("%sFTTChisq",DetectorName),DetFTTChisq,Form("%sFTTChisq[%sNumber]/F",DetectorName,DetectorName) );  
   tree->Branch(Form("%sFTTTime",DetectorName),DetFTTTime,Form("%sFTTTime[%sNumber]/F",DetectorName,DetectorName) );  
}

void MTConvert125MHzDetector::AddAreaRBranch( TTree* tree )
{
   tree->Branch(Form("%sAreaR",   DetectorName), DetAreaR, Form( "%sAreaR[%sNumber]/F", DetectorName, DetectorName ) );
}


void MTConvert125MHzDetector::ConvertFromDigi()
{  GetFromDigi(              DetNumber, DetModID, DetEne, DetTime ); }

void MTConvert125MHzDetector::ConvertFromDigiWithResponse()
{  GetFromDigiWithResponse(  DetNumber, DetModID, DetEne, DetTime ); }

void MTConvert125MHzDetector::ConvertFromMTime()
{  GetFromMTime(             DetNumber, DetModID, DetEne, DetTime ); }

void MTConvert125MHzDetector::ConvertFromMTimeWithResponse()
{  GetFromMTimeWithResponse( DetNumber, DetModID, DetEne, DetTime ); }

void MTConvert125MHzDetector::ConvertFromMTimeWithPulseShape()
{  GetFromMTimeWithPulseShape( DetNumber, DetModID, DetEne,
			       DetTime, DetPTime, DetFallTime ); }



void MTConvert125MHzDetector::ConvertTrueInfo()
{  GetTrueInfo( DetTrueNumber, DetTrueModID, DetTrueEne, DetTrueTime ); }

void MTConvert125MHzDetector::GetFromDigi(              Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{  GetTrueInfo( number, modID, ene, time ); }

void MTConvert125MHzDetector::GetFromDigiWithResponse(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{  GetFromDigi( number, modID, ene, time ); }

void MTConvert125MHzDetector::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{  GetFromMTime( number, modID, ene, time ); }


void MTConvert125MHzDetector::AddAccidental(){
  AddAccidental( DetNumber, DetModID, DetEne, DetTime ); 
}
void MTConvert125MHzDetector::AddAccidental( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  if( !EnableAccidentalOverlay ) return;
  //  AccidentalTree->GetEntry( AccidentalEntryID );

  for( Int_t ich = 0 ; ich < DetectorNCH ; ++ich ){
    Float_t TrueEne  = ene[ich];
    Float_t AcciEne  = AccidentalDetEne[ich];
    
    if( TrueEne==0 || TrueEne < AcciEne ){
      time[ich] = AccidentalDetTime[ich] + AccidentalTimeShift;
    }
    ene[ich] += AccidentalDetEne[ich];
  }
}

void MTConvert125MHzDetector::AddAccidentalWfm(){  
  AddAccidentalWfm( DetNumber, DetModID, DetEne,DetTime,
		    DetPTime, DetFallTime, DetWfm );
}
void MTConvert125MHzDetector::AddAccidentalWfm( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, 
					  Float_t* PTime, Float_t* FallTime, Float_t (*wfm)[MTBP::nSample125] )
{ 
	
		
  if( !EnableAccidentalOverlay ) return;  
  ///// in case of CsI, do not apply accidental when the event is not online-triggered
  if( DetectorID == MTBP::CSI && (EtSumOnlineTrig < MTBP::OnlineThreshold) ) return;
 
  if(DetectorID==MTBP::CSI && m_userFlag>=20190101){
	  number += 256;
  }


  //  AccidentalTree->GetEntry( AccidentalEntryID );
 
  /// reset online cluster simulation before filling cluster bit
  if( m_onlineClusSimulator ) m_onlineClusSimulator->Reset();
 
  for( Int_t iCh = 0 ; iCh < number ; ++iCh ){
    modID[iCh] = IndexIDArray[iCh];// for channels without true hit
    if( modID[iCh] != AccidentalDetModID[iCh] ){
      ///// Accidental data should be in E14ID order.
      ///// If not, error message is issued.
      std::cout << "DetName " << DetectorName << ", modID = " << modID[iCh] << ", AccidentalDetModID = "<< AccidentalDetModID[iCh] << std::endl;
      std::cout << "Mismatch between IndexIDArray and AccidentalModID." << std::endl;
      return;	
    }
   
		
    for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; iSample++ )
      wfm[iCh][iSample] += (AccidentalWfm[iCh][iSample] - AccidentalPedestal[iCh]) * AccidentalDetECalibConst[iCh];


	// correct corrupted wfm
	std::vector<short> waveform;
	for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; iSample++ ){
		waveform.push_back(wfm[iCh][iSample]/AccidentalDetECalibConst[iCh]+0.5);	
	}
	if(ApplyWfmCorrection){
		DetWfmCorrectNumber[iCh]
			= CorrectCorruptedWfm(waveform, AccidentalRunID, DetectorName, DetModID[iCh] );
		for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; iSample++ ){
			wfm[iCh][iSample] = waveform[iSample]*AccidentalDetECalibConst[iCh];
		}
	}else{
		DetWfmCorrectNumber[iCh] = -1;
	}
	
	short wfms[64];
	for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; iSample++ ){
		wfms[iSample] = waveform[iSample];
	}


	float smoothness = 0;
	float prod_der2;
	float derivative3;
	//E14ProdLibrary::E14CrateData::CalcSmoothness(wfms, 64, smoothness, prod_der2, derivative3 );
	
	DetSmoothness[iCh] = smoothness;

    ///// pedestal calculation for merged pulse is done before energy and timing calculation 
    Double_t StdDev[2] = { TMath::RMS( 10, &wfm[iCh][0] ), TMath::RMS( 10, &wfm[iCh][MTBP::nSample125-10] )};
	Float_t  pedestal;
	// DCV and UCV always use the first samples to calculate pedestal
	if(DetectorID==MTBP::DCV || DetectorID==MTBP::UCVLG) pedestal = StdDev[0];
	else pedestal = (StdDev[0] < StdDev[1]) ? TMath::Mean(10, &wfm[iCh][0]) : TMath::Mean(10, &wfm[iCh][MTBP::nSample125 - 10]);
	
	for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; ++iSample )
      wfm[iCh][iSample] -= pedestal;
    
    GetEnergyTimeFromWfm( &wfm[iCh][0], ene[iCh], time[iCh],
			  PTime[iCh], FallTime[iCh], iCh );
    EvalFTTChisq( iCh, &DetWfm[iCh][0], DetFTTChisq[iCh], DetFTTTime[iCh] );
	
	if( m_onlineClusSimulator && m_detname=="CSI" )
       FillClusterBit( iCh, &DetWfm[iCh][0]);
  }

  if( m_onlineClusSimulator && m_detname=="CSI" )
     m_CDTNum = GetOnlineClusterNumber();

}

void MTConvert125MHzDetector::SetAccidentalEntryNumber( Long64_t entry )
{  AccidentalEntryID = entry; }

void MTConvert125MHzDetector::SetAccidentalTimeShift( float t ) // [ns]
{
  AccidentalTimeShift = t / 8.; // [ns] -> [clock]
  AccidentalTimeShift += MTBP::MCCommonTimeShift / 8.; // Sim hit time and Data hit time have 185ns difference.a for May run  2013/06/07 by shiomi
}

float MTConvert125MHzDetector::GetAccidentalTimeShift() const
{  return AccidentalTimeShift * 8.;}

int MTConvert125MHzDetector::GetNDigi()
{
  TClonesArray *digiArray = GetDigiArray();
  return digiArray->GetEntries();  
}

//added at 21st Feb, 2014 by Maeda Yosuke for waveform analysis
void MTConvert125MHzDetector::AddWfmFromFunc( Float_t* wfm, TF1* func )
{  
  for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; ++iSample ){
    wfm[iSample] += func->Eval( iSample*8. );
  }
  return;
}

Float_t MTConvert125MHzDetector::GetIntegralFromFunc(TF1* func)
{
	
	if(DetectorID==MTBP::UCVLG){
		static int init = 1;
			
		double maxwfm = -1e5;
		if(init){
			const int Ntry = 2*10000;
			for (int i = 0; i<Ntry; i++){
				double t = 2*512.0*i/Ntry;
				double y = func->Eval(t);
				if(maxwfm<y) maxwfm = y;
			}
			init = 0;
		}
		static double norm = maxwfm;
		return norm;
	}

	if(DetectorID==MTBP::DCV){
	  static int init = 1;

	  double orig_time = func->GetParameter(1);
	  func->SetParameter(1,512);
	  
	  double maxwfm = -1e5;
	  if(init){
	    const int Ntry = 2*10000;
	    for (int i = 0; i<Ntry; i++){
	      double t = 2*512.0*i/Ntry;
	      double y = func->Eval(t);
	      if(maxwfm<y) maxwfm = y;
	    }
	    init = 0;
	  }
	  static double norm = maxwfm;
	  func->SetParameter(1,orig_time);
	  return norm;
	}
	
	Float_t SampleIntegral = 0;
	Int_t StartTiming = (Int_t)(func->GetParameter(1) / 8. - MTBP::nSample125 / 2 + 1);
	for (Int_t iSample = 0; iSample < MTBP::nSample125; iSample++)
		SampleIntegral += func->Eval((StartTiming + iSample)*8.);
	
	return SampleIntegral;

	
}



void MTConvert125MHzDetector::GenerateWfmFromMTimeVec(Int_t iCh, const std::vector<pEnergyTime>& propagatedVec,
	Float_t AdditionalTimeShift, TF1* fnc)
{

	TF1* FillFunc;
	if (fnc == NULL)  FillFunc = WfmFunc;
	else FillFunc = fnc;

	for (std::vector<pEnergyTime>::const_iterator it = propagatedVec.begin(); it != propagatedVec.end(); it++) {
		Double_t ChTime = it->time + AdditionalTimeShift + AccidentalTimeShift * 8 - AccidentalDetTCalibConst[iCh] * 8;//in [ns] unit
		FillFunc->SetParameter(0, 1.);
		FillFunc->SetParameter(1, ChTime);

		Double_t IntegralNormalizedFunc = GetIntegralFromFunc(FillFunc);
		if (IntegralNormalizedFunc < 0.01) FillFunc->SetParameter(0, 0);
		else FillFunc->SetParameter(0, it->energy / IntegralNormalizedFunc);

		Int_t nanflag = 0;
		if (isnan(FillFunc->Eval(0)))
			nanflag = 1;

		AddWfmFromFunc(&DetWfm[iCh][0], FillFunc);
	}

	return;
}

/// data-base waveform generator
void MTConvert125MHzDetector::GenerateDbWfmFromMTimeVec( Int_t iCh, const std::vector<pEnergyTime>& propagatedVec,
                                                 Float_t AdditionalTimeShift )
{
  if( m_dbWfmGenerator==NULL ) return;

  LcLib::LcWfm wfm(MTBP::nSample125);

  for( std::vector<pEnergyTime>::const_iterator it = propagatedVec.begin() ; it!=propagatedVec.end() ; it++ ){
    Double_t ChTime = it->time + AdditionalTimeShift +  AccidentalTimeShift*8 - AccidentalDetTCalibConst[iCh]*8;//in [ns] unit

    LcLib::LcWfm sub_wfm = m_dbWfmGenerator->GenerateWfm( IndexIDArray[iCh], it->energy, ChTime/8.); 
    wfm += sub_wfm;
  }

  for( Int_t is=0; is<MTBP::nSample125; ++is )
     DetWfm[iCh][is] = wfm[is];

  return;
}

//handle all forward and backward CFTime and Parabola Time 
void MTConvert125MHzDetector::GetEnergyTimeFromWfm( Float_t* wfm, Float_t& energy,
					      Float_t& time, Float_t& PTime, Float_t& FallTime,
					      Int_t ChIndex, Bool_t ApplyT0Calib ){
  
  energy   = 0;
  time     = 0;
  PTime    = MTBP::Invalid;
  FallTime = MTBP::Invalid;
  
  //energy calculation : just take integral of 64 samples
  for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
    energy += wfm[iSample];
    
  //timing calculation
  Int_t    PeakTime   = TMath::LocMax( MTBP::nSample125, wfm );
  //Int_t StartSample = ( DetectorID==NCC ? 20 : 24 );
  //Int_t PeakTime    = TMath::LocMax( 20, wfm+StartSample ) + StartSample;//limit peak region +/-10cnts :: for CFTime w/ peak region limited
  Double_t PeakHeight = wfm[PeakTime];
  Double_t thre       = PeakHeight/2.;//chaged at 1st May, 2014 
  
  if(DetectorID==MTBP::DCV || DetectorID==MTBP::UCVLG){
	  energy = PeakHeight;
  }
  
  //rise edge
  for( Int_t iSample = PeakTime ; iSample > 0 ; iSample-- ){
    if( wfm[iSample-1] < thre && wfm[iSample] >= thre ){
      time = iSample - 1 + (thre-wfm[iSample-1]) / (wfm[iSample]-wfm[iSample-1]);
      break;
    }
  }


  //fall edge
  for( Int_t iSample = PeakTime ; iSample < MTBP::nSample125-1 ; iSample++ ){
    if( wfm[iSample] > thre && wfm[iSample+1] <= thre ){
      FallTime = iSample + (wfm[iSample]-thre) / (wfm[iSample]-wfm[iSample+1]);
      break;
    }
  }
  
  Float_t EneThre     = DetPeakHeightThre;  // default
  Float_t NominalTime = DetNominalPeakTime; // default
  GetEneThreAndNominalTime( ChIndex, EneThre, NominalTime );
  
  std::vector<MYParabolaTimeContainer> ParabolaTimeVec;
  Int_t iBestPeak = PeakSearch( wfm, ParabolaTimeVec, EneThre, NominalTime );
  if( iBestPeak < 0 ) PTime = MTBP::Invalid;
  else PTime = ParabolaTimeVec[iBestPeak].m_ParabolaParam[1];
  //set region-limited CFTime (20 clock)
  //Int_t StartSample = ( DetectorID==NCC ? 20 : 24 );
  //PeakTime = TMath::LocMax( 20, wfm+StartSample ) + StartSample;//limit peak region +/-10cnts :: for CFTime w/ peak region limited
  //for( Int_t iSample=PeakTime ; iSample>0 ; iSample-- ){
  //  if( wfm[iSample-1]<thre && wfm[iSample]>=thre ){
  //    PTime = iSample - 1 + (thre-wfm[iSample-1])/(wfm[iSample]-wfm[iSample-1]);
  //    PTime += WfmTimeShift/8.;
  //    break;
  //  }
  //}
  //PTime = time+WfmTimeShift/8.;//conventional 64 sample CFTime
  
  if( ApplyT0Calib ){
    Float_t TimeOffset = AccidentalDetTCalibConst[ChIndex] ;//[clock]
    time     += TimeOffset;
    PTime    += TimeOffset;
    FallTime += TimeOffset;
  }  
  
  // for dead channels
  Bool_t isDead=false;
  if(20160401<m_userFlag && m_userFlag<20190201 ){ // Run69-Run79
    if(DetectorID==MTBP::IBCV){
      isDead=false;
      for(int ideadch=0;ideadch<MTBP::IBCVNDeadChannels;ideadch++){
	if(ChIndex==MTBP::IBCVDeadChannelIndex[ideadch]) isDead=true;
      }
    }
  }

  if(isDead){
    energy   = 0;
    time     = 0;
    PTime    = MTBP::Invalid;
    FallTime = MTBP::Invalid;
  }
  
  return;
}

void MTConvert125MHzDetector::GetAreaRFromWfm()
{
	
	if (DetectorID!=MTBP::NCC)
	{
		for (int ichannel = 0; ichannel<DetNumber; ichannel++)
		{
			GetAreaRFromWfm( &DetWfm[ichannel][0], DetAreaR[ichannel]);	
		}
	}
}

void MTConvert125MHzDetector::GetAreaRFromWfm( const Float_t* wfm, Float_t& areaR ){

	  int n_smple = 63;

	  Float_t max = 0;
	  Float_t min = 0;
	  Float_t all_sum = 0;
	  
	  for ( Int_t ismple = 1; ismple < n_smple; ismple++ )
	  {
		  if ( ismple == 1 || ( wfm[ismple] > max ) ) 
		  {
			  max = wfm[ismple];
		  }
		  if ( ismple == 1 || ( wfm[ismple] < min ) )
		  {
			  min  = wfm[ismple];
		  }
		  all_sum += wfm[ismple];
	  }

	  double max_d = (Double_t) max;
	  double min_d = (Double_t) min;
	  double all_sum_d =  (Double_t) all_sum;

	  if (max == min)	areaR = -9999; // There is no waveform
	  else  				areaR = (all_sum_d/(n_smple-1)-min_d)/(max_d-min_d); // Normal AreaR calculation
}

void MTConvert125MHzDetector::EvalFTTChisq( const Int_t ChIndex, const Float_t *wfm, Float_t &FTTChisq, Float_t &FTTTime )
{
   if( m_fttCalculator==NULL ) return;
   LcLib::LcWfm test_wfm(MTBP::nSample125, wfm );

   Float_t EneThre     = DetPeakHeightThre;
   Float_t NominalTime = DetNominalPeakTime;
   GetEneThreAndNominalTime( ChIndex, EneThre, NominalTime );

   m_fttCalculator->SetNominalTime( NominalTime );
  
   FTTChisq = m_fttCalculator->EvalFTTChisq( IndexIDArray[ChIndex], test_wfm);   
   FTTTime  = m_fttCalculator->GetTimeToEvalFTT(test_wfm);
}

void MTConvert125MHzDetector::FillClusterBit( const Int_t ChIndex, const Float_t *wfm )
{
   if( m_detname!="CSI" || m_onlineClusSimulator==NULL ) return;

   if( m_clusPeakThresholdVec.size()!=DetectorNCH ){
      std::cout<<" ERROR : m_clusPeakThresholdVec.size()!=DetectorNCH " << std::endl;
      return;
   }

   LcLib::LcWfm test_wfm(MTBP::nSample125, wfm);
   const Int_t modId = IndexIDArray[ChIndex];

   m_onlineClusSimulator->FillByWfm( modId, test_wfm, 
                                     m_clusPeakThresholdVec[ChIndex]);
}

void MTConvert125MHzDetector::SetOnlineClusNominalSample()
{
   if( m_detname=="CSI" && m_onlineClusSimulator ) 
      m_onlineClusSimulator->SetNominalSample( TMath::Nint(DetNominalPeakTime) ); 
}

void MTConvert125MHzDetector::SetOnlineClusPeakThreshold()
{
   if( m_detname!="CSI" || m_onlineClusSimulator==NULL ) return;
 
   const Double_t kNominalTime = DetNominalPeakTime; // [clock]
   const Int_t kNominalSample = TMath::Nint(kNominalTime);
     
   for( Int_t imod=0; imod<DetectorNCH; ++imod )
   {
      /// cluster bit threshold :
      /// small : 22 MeV, large : 44 MeV
      const Int_t modId = IndexIDArray[imod];
      const Double_t kEneThreshold = (imod<2240) ? 22. : 44. ;
      Double_t PeakThreshold = 9999.;

      if( m_dbWfmGenerator ){ 
          LcLib::LcWfm stdwfm = m_dbWfmGenerator->GenerateWfm(modId, kEneThreshold, kNominalTime);
          if( stdwfm.GetNsample()==MTBP::nSample125 ){
             const Int_t kNominalSample = TMath::Nint(kNominalTime);
             PeakThreshold = stdwfm[kNominalSample];
          } 
      }else{
         /// parameters for the original waveform simulation
         PeakThreshold = (modId<2240) ? 3.518 : 7.045 ;
      }
      m_clusPeakThresholdVec.push_back(PeakThreshold);
   }  
}

Int_t MTConvert125MHzDetector::GetOnlineClusterNumber() const
{
   if( m_onlineClusSimulator==NULL ) return -1;
   /// before run79, jointing nearby crystal algorithm is used.
   if( m_userFlag < 20180601 ){
      m_onlineClusSimulator->JointNearbyCrystal();
      return m_onlineClusSimulator->GetNcluster();
   }else{
      return m_onlineClusSimulator->GetNcluster();
   }
}

void MTConvert125MHzDetector::GetEtWfm()//added at 12th Mar, 2014
{
  for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
    DetEtSumWfm[iSample] = 0;
  
  for( Int_t iCh=0 ; iCh<DetectorNCH ; iCh++ ){
    if( AccidentalDetECalibConst[iCh]>0 ){
      for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
	if( ! std::isnan(DetWfm[iCh][iSample]) )
	  DetEtSumWfm[iSample] += (Long64_t)(DetWfm[iCh][iSample]/AccidentalDetECalibConst[iCh]);
    }
  }
  return;
}


Int_t MTConvert125MHzDetector::PeakSearch( Float_t* wfm, std::vector<MYParabolaTimeContainer>& ParabolaTimeVec, 
				     Float_t EneThre, Float_t NominalTime )
{
  static Double_t PeakHeightCorrection = 1.1; //for 5 sample moving average
  //static Double_t PeakHeightCorrection = 1.25; //for 9 sample moving average
  EneThre /= PeakHeightCorrection;
  
  MYParabolaTimeContainer ParabolaTime;
  Float_t FilteredWfm[MTBP::nSample125];
  for( Int_t iSample=nSampleOneSide ; iSample<MTBP::nSample125-nSampleOneSide ; iSample++ )
    FilteredWfm[iSample] = TMath::Mean( nSampleAverage, wfm+iSample-nSampleOneSide );
  
  for( Int_t iSample=nSampleOneSide+1 ; iSample<MTBP::nSample125-nSampleOneSide-1 ; iSample++ ){
    if( FilteredWfm[iSample]*PeakHeightCorrection>EneThre
	&& FilteredWfm[iSample-1] < FilteredWfm[iSample]
	&& FilteredWfm[iSample]  >= FilteredWfm[iSample+1] ){
      ParabolaTime.m_PeakTime = iSample;
      //GetParabolaParam( &wfm[iSample-1], ParabolaTime.m_ParabolaParam );
      GetParabolaParam( &FilteredWfm[iSample-1], ParabolaTime.m_ParabolaParam );
      ParabolaTime.m_ParabolaParam[1] += (ParabolaTime.m_PeakTime-1);
      ParabolaTimeVec.push_back( ParabolaTime );
    }    
  }
  
  Int_t iBestParabolaTime = ( ParabolaTimeVec.empty() ? -1 : 0 );
  Int_t iPeak = 0;
  Double_t MinDiff = (Double_t)MTBP::nSample125;
  for( std::vector<MYParabolaTimeContainer>::iterator it=ParabolaTimeVec.begin()
	 ; it!=ParabolaTimeVec.end() ; it++ ){
    Double_t diff = TMath::Abs(it->m_ParabolaParam[1]-NominalTime);//updated to use detector by detector NominalTime (19th Jul, 2014)
    //Double_t diff = TMath::Abs(it->m_ParabolaParam[1]-33.0);//updated to fit pro3 data (13th Jun, 2014, MY)
    if( MinDiff>diff ){
      iBestParabolaTime = iPeak;
      MinDiff = diff;
    }
    iPeak++;
  }
  
  return iBestParabolaTime;
}

//"EneThre" and "NominalTime" are needed to be initilized with standard the standard value.
//converted to [MeV/count] unit in the last of this function
void MTConvert125MHzDetector::GetEneThreAndNominalTime( Int_t ChIndex, Float_t& EneThre, Float_t& NominalTime )
{  

  int imod    = IndexIDArray[ChIndex];
  NominalTime = DetChByChNominalTime[imod];
  EneThre     = DetChByChMinPeakHeight[imod];

  double NominalTimeCorrection = (NominalAverageClusterTime_Data - NominalAverageClusterTime_MC)/8.;

  NominalTime -= AccidentalDetTCalibConst[ChIndex]; // To be the same treatment as ChTime
  NominalTime -= NominalTimeCorrection; // To treat AverageClusterTime difference btw data and MC

  EneThre *= AccidentalDetECalibConst[ChIndex];
  return;
}

void MTConvert125MHzDetector::GetParabolaParam( Float_t* wfm, Double_t* param )
{
  param[0] = ( wfm[0]-2*wfm[1]+wfm[2] )/2.;
  param[1] = (3*wfm[0]-4*wfm[1]+wfm[2])/(2*(wfm[0]-2*wfm[1]+wfm[2]));
  param[2] = wfm[0] - param[0]*param[1]*param[1];

  return;
}

Int_t MTConvert125MHzDetector::ClusteringMtime( std::vector< pEnergyTime >& inputEtVec, 
					  std::vector< pEnergyTime >& outputEtVec, double pitch )
{
  int startIndex=0;
  
  double weightedTime=0.0, totalWeight=0.0;

  const Double_t k_EnergyThre = 1.e-3;

  for( UInt_t index=0; index<inputEtVec.size(); index++ ){
    if( inputEtVec[index].time - inputEtVec[startIndex].time < pitch ){
      weightedTime += inputEtVec[index].time * inputEtVec[index].energy;
      totalWeight  += inputEtVec[index].energy;
    }else{
      energyTime.energy = totalWeight;
      
      /// protection : if energy is too low, ignore it from 2016 analysis
      if( totalWeight>k_EnergyThre ||  !(m_dbWfmGenerator) ){
	energyTime.time   = weightedTime / totalWeight;
	outputEtVec.push_back( energyTime );
      }
      weightedTime = inputEtVec[index].time * inputEtVec[index].energy;
      totalWeight  = inputEtVec[index].energy;
      startIndex = index;
    }
  }
  energyTime.energy = totalWeight;
  
  /// protection : if energy is too low, ignore it from 2016 analysis
  if( totalWeight>k_EnergyThre ||  !(m_dbWfmGenerator) ){
    energyTime.time   = weightedTime / totalWeight;
    outputEtVec.push_back( energyTime );
  }

  return outputEtVec.size();
}

Int_t MTConvert125MHzDetector::ClusteringMtime(std::vector< EnergyTimePos >& inputEtVec,
	std::vector< EnergyTimePos >& outputEtVec, double pitch)
{
	int startIndex = 0;

	double weightedTime = 0.0,  totalWeight = 0.0;
	double weightedPos[3] = {0.0,0.0, 0.0};

	EnergyTimePos etp;

	for (UInt_t index = 0; index < inputEtVec.size(); index++) {
		if (inputEtVec[index].time - inputEtVec[startIndex].time < pitch) {
			weightedTime += inputEtVec[index].time * inputEtVec[index].energy;
			weightedPos[0]  += inputEtVec[index].pos[0] * inputEtVec[index].energy;
			weightedPos[1]  += inputEtVec[index].pos[1] * inputEtVec[index].energy;
			weightedPos[2]  += inputEtVec[index].pos[2] * inputEtVec[index].energy;
			totalWeight += inputEtVec[index].energy;
		}
		else {
			
			etp.energy = totalWeight;
			etp.time = weightedTime / totalWeight;
			etp.pos[0] = weightedPos[0] / totalWeight;
			etp.pos[1] = weightedPos[1] / totalWeight;
			etp.pos[2] = weightedPos[2] / totalWeight;
			
			outputEtVec.push_back(etp);
			weightedTime = inputEtVec[index].time * inputEtVec[index].energy;
			weightedPos[0] = inputEtVec[index].pos[0] * inputEtVec[index].energy;
			weightedPos[1] = inputEtVec[index].pos[1] * inputEtVec[index].energy;
			weightedPos[2] = inputEtVec[index].pos[2] * inputEtVec[index].energy;
			totalWeight = inputEtVec[index].energy;
			startIndex = index;
		}
	}
	
	etp.energy = totalWeight;
	etp.time = weightedTime / totalWeight;
	etp.pos[0] = weightedPos[0] / totalWeight;
	etp.pos[1] = weightedPos[1] / totalWeight;
	etp.pos[2] = weightedPos[2] / totalWeight;
	outputEtVec.push_back(etp);

	return outputEtVec.size();
}


void MTConvert125MHzDetector::SuppressWfm(){
	
	if(!m_WFMsave) return;

	DetSuppWfmNumber = 0;
	for( int ich = 0; ich < DetectorNCH ; ich++ ){
		double maxy = -1e10;
		for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; iSample++ ){
			if(maxy<DetWfm[ich][iSample]) maxy = DetWfm[ich][iSample];
		}
		maxy /= AccidentalDetECalibConst[ich];
		if(maxy>30){
			DetSuppWfmModID[DetSuppWfmNumber] = DetModID[ich];
			for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; iSample++ ){
				DetSuppWfm[DetSuppWfmNumber][iSample] = DetWfm[ich][iSample];
			}
			DetSuppWfmNumber++;
		}
	}
}


void MTConvert125MHzDetector::ZeroSuppress( Float_t threshold ){
  ZeroSuppress( DetNumber, DetModID, DetEne, DetTime, DetPTime, DetFallTime, DetWfm, threshold );
  return;
}

void MTConvert125MHzDetector::ZeroSuppress( Int_t& number, Int_t* modID, Float_t* ene, Float_t*time, 
				      Float_t* PTime, Float_t* FallTime, Float_t (*wfm)[MTBP::nSample125], 
				      Float_t threshold )
{
  number = 0;
  for( Int_t iCh = 0 ; iCh < DetectorNCH ; iCh++ ){
    if( TMath::Abs(ene[iCh]) > threshold ){
      if( iCh != number ){
	modID[number]    = IndexIDArray[iCh];
	ene[number]      = ene[iCh];
	time[number]     = time[iCh];
	PTime[number]    = PTime[iCh];
	FallTime[number] = FallTime[iCh];
      }
      number++;
    }
  } 
  return;
}

void MTConvert125MHzDetector::SetFileNameE14BasicParam( std::string InputFileName ){

  if( (access( InputFileName.c_str(), R_OK ) != 0) ){
    std::cerr << "Error!! E14BP conf file is not found." << std::endl;
    std::exit(1);
  }

  E14BPConfFile = InputFileName;
  return;
}

void MTConvert125MHzDetector::SetE14BasicParamFromRootFile( std::string InputFileName ){

  if( (access( InputFileName.c_str(), R_OK ) != 0) ){
    std::cerr << "Error!! No such E14BP conf file." << std::endl;
    std::cerr << "InputFileName: "<< InputFileName.c_str() << std::endl;
    std::exit(1);
  }

  E14BPConfFile = InputFileName;

  // basic parameter from channel 0 in a detector
  m_E14BP->SetE14BasicParamFromRootFile( E14BPConfFile, m_userFlag, DetectorName, 0);
  NominalAverageClusterTime_Data = m_E14BP->GetNominalAverageClusterTime_Data();
  NominalAverageClusterTime_MC   = m_E14BP->GetNominalAverageClusterTime_MC();
  DetZPosition     = m_E14BP->GetDetZPosition();
  NDeadChannel     = m_E14BP->GetNDeadChannels();
  DeadChannelIDVec = m_E14BP->GetDeadChannelIDVec();
  NDeadModule      = m_E14BP->GetNDeadModules();
  DeadModuleIDVec  = m_E14BP->GetDeadModuleIDVec();
  DetSmearingSigma = m_E14BP->GetG2DPSmearingSigma();
  DetNominalPeakTime = m_E14BP->GetG2DPNominalPeakTime();
  DetPeakHeightThre  = m_E14BP->GetG2DPMinPeakHeight();

  // basic parameter for channel by channel
  m_E14BP->GetArrayNominalPeakTime( DetChByChNominalTime   );
  m_E14BP->GetArrayMinPeakHeight(   DetChByChMinPeakHeight );

  return;
}

void MTConvert125MHzDetector::SetE14BasicParamFromCode(){

  E14BPConfFile = "";

  // basic parameter derived from channel 0 in a detector
  m_E14BP->SetDetBasicParam( m_userFlag, DetectorName, 0);
  NominalAverageClusterTime_Data = m_E14BP->GetNominalAverageClusterTime_Data();
  NominalAverageClusterTime_MC   = m_E14BP->GetNominalAverageClusterTime_MC();
  DetZPosition     = m_E14BP->GetDetZPosition();
  NDeadChannel     = m_E14BP->GetNDeadChannels();
  DeadChannelIDVec = m_E14BP->GetDeadChannelIDVec();
  NDeadModule      = m_E14BP->GetNDeadModules();
  DeadModuleIDVec  = m_E14BP->GetDeadModuleIDVec();
  DetSmearingSigma = m_E14BP->GetG2DPSmearingSigma();
  DetNominalPeakTime = m_E14BP->GetG2DPNominalPeakTime();
  DetPeakHeightThre  = m_E14BP->GetG2DPMinPeakHeight();

  // basic parameter for channel by channel
  for(int ich=0;ich<DetectorNCH;ich++){
    int imod = IndexIDArray[ich];
    m_E14BP->SetDetBasicParam( m_userFlag, DetectorName, imod);
    DetChByChNominalTime[imod]   = m_E14BP->GetG2DPNominalPeakTime();
    DetChByChMinPeakHeight[imod] = m_E14BP->GetG2DPMinPeakHeight();
  }

  return;
}



short MTConvert125MHzDetector::CorrectCorruptedWfm(std::vector<short>& data, int runID, std::string detname, int chID ){

          if(29979<=runID && runID<=31532){
                // run81
                  if(detname=="CBAR" && chID==121)  return CorrectCorruptedWfm_type2(data, 8);
                  else if(detname=="CBAR" && (chID==51||chID==135||chID==148))  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="NCC" && (chID==10||chID==20))  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="CC04" && chID==32)  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="CC05" && 16<=chID && chID<=31)  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="OEV"  && chID==16)  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="DCV"  && chID==21)  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="MBCV" && chID==3)   return CorrectCorruptedWfm_type1(data);
                  else if(detname=="CSI" && chID==2931)  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="CSI" && chID==1884)  return CorrectCorruptedWfm_type2(data, 8);
                  else if(detname=="CSI" &&
                                  (chID==0||chID==2||chID==4||chID==42||chID==188||chID==367||chID==381
                                   ||chID==644||chID==648||chID==772||chID==796||chID==810||chID==860
                                   ||chID==1404||chID==1427||chID==1433||chID==1439||chID==1452||chID==1588
                                   ||chID==1589||chID==1776||chID==2007||chID==2195||chID==2448
                                   ||chID==2450||chID==2472||chID==2495||chID==2654)) return CorrectCorruptedWfm_type1(data);

          }else if(31532<=runID && runID<=32220){
                // run82
                  if(detname=="CBAR" && chID==121)  return CorrectCorruptedWfm_type2(data, 8);
                  else if(detname=="CBAR" && (chID==51||chID==135||chID==148))  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="NCC" && chID==10)  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="CC04" && chID==32)  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="CC05" && 16<=chID && chID<=31)  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="OEV"  && chID==16)  return CorrectCorruptedWfm_type1(data);
                  else if(detname=="MBCV" && chID==3)   return CorrectCorruptedWfm_type1(data);
                  else if(detname=="CSI" && chID==1884)  return CorrectCorruptedWfm_type2(data, 8);
                  else if(detname=="CSI" &&
                                  (chID==2||chID==4||chID==42||chID==644||chID==648||chID==772||chID==796
                                   ||chID==810||chID==860||chID==1404||chID==1427||chID==1433
                                   ||chID==1439||chID==1452||chID==1588||chID==1589||chID==1776||chID==1874
                                   ||chID==2007||chID==2195||chID==2448
                                   ||chID==2450||chID==2472||chID==2495||chID==2654)) return CorrectCorruptedWfm_type1(data);

          }

          // no correction
          return -1;

  }

 short MTConvert125MHzDetector::CorrectCorruptedWfm_type1(std::vector<short>& Data){

                // constant used for correction

                int nTry = 1;
                const int noiseScalar = 3;
                int sample_offset =  1;

                int n_samples = 64;

                const int n_used_samples = 10-sample_offset;

                //---------------------------------------------------
                // calculation of pedestals

        float Ped1 = 0; // First n_used_samples
        float Ped2 = 0; // last n_used_samples
        float RMS1 = 0; // First n_used_samples
        float RMS2 = 0; // last n_used_samples

                float pedestal;
                float pedestal_sigma;

        for (int i_sample=sample_offset, n=sample_offset+n_used_samples; i_sample<n; i_sample++) {
          Ped1 += (float)Data[i_sample]/(double)n_used_samples;
          Ped2 += (float)Data[n_samples-1-i_sample]/(double)n_used_samples;
        }
        for (int i_sample=sample_offset, n=sample_offset+n_used_samples; i_sample<n; i_sample++) {
          RMS1 += pow( (float)Data[i_sample] - Ped1, 2 );
          RMS2 += pow( (float)Data[n_samples-1-i_sample] - Ped2, 2 );
        }
        RMS1 = sqrt( RMS1 );
        RMS2 = sqrt( RMS2 );

        pedestal       = Ped1;
        pedestal_sigma = RMS1;
        if (RMS2 < RMS1) {
          pedestal       = Ped2;
          pedestal_sigma = RMS2;
        }


                double pedsgScalar  = pedestal_sigma;

                //---------------------------------------------------
                // correction of waveform


                // Driving parameters
                //      const Double_t noiseScalar = 3;  / noiseScalar; --> make this an input
                //####   temporary   #########
                Double_t pedSigma = 0;
                //############################
                const Int_t satuLevel = 16383;
                const Int_t DIRAClevel = 10000;
                Short_t correctionCounter = 0;
                const Int_t ntime = Data.size() ;  // 64
                Int_t yy[ntime];
                //      Bool_t pastPeak = false; // if this clock is after the peak,

                Int_t maxH = 0;
                Bool_t isDIRAC = false;
                Bool_t isSaturation = false;
                Int_t diff = 0;
                Int_t maxDiff = 0;

                Int_t satuCounter = 0;
                Int_t ShiftedSatuLevel = satuLevel;
                for ( Int_t itime = 0; itime < ntime; itime ++ ) { //clock loop
                        yy[itime] = (Double_t) Data[itime] ;
                        if ( Data[itime] == ShiftedSatuLevel ) {
                                satuCounter ++;
                                yy[itime] = satuLevel;
                        }
                        maxH = std::max( maxH, yy[itime] );
                        if ( itime > 1 ) {
                                diff = std::abs( yy[itime] - yy[itime-1] );
                                maxDiff = std::max( diff, maxDiff );
                        }
                } // loop of itime

                if ( satuCounter > 1 ) isSaturation = true;
                if ( maxDiff == DIRAClevel && yy[0]==0 && yy[1]==0 ) {
                        isDIRAC = true;
                }
                Int_t absProblem[ntime];

                if ( nTry != 0 ) {
                        absProblem[0] = 4;
                        absProblem[1] = 2;
                        for ( Int_t itime = 2; itime < ntime-1; itime++ ) {
                                absProblem[itime] = 0;
                                Int_t preD   = yy[itime+1] - yy[itime+0];
                                Int_t pstD   = yy[itime+0] - yy[itime-1]; // This is the point. start from third
                                Int_t DD                 = preD - pstD;
                                Int_t Dside      = yy[itime+1] - yy[itime-1];
                                Int_t preDD  = 0;
                                if ( itime < ntime-2 ) { // only use when itmime+2 is in data.
                                        preDD = (yy[itime+2] - yy[itime+1]) - Dside/2.;
                                }
                                Int_t pstDD  = 0;
                                if ( itime > 2 ) { // avoid tracking data
                                        pstDD = Dside/2. - (yy[itime-1] - yy[itime-2]);
                                }
                                if ( std::abs(DD) > std::max( std::abs(preDD), std::abs(pstDD) ) * noiseScalar
                                                        && std::abs( DD ) >  std::abs( Dside )
                                                        && std::abs( DD ) > pedSigma * pedsgScalar
                                                        && !isDIRAC ) {
                                        absProblem[itime] = std::abs( DD/std::abs( DD ) );
                                }
                        }
                        absProblem[ntime-1] = 8;

                        if ( absProblem[2] == 1
                                                                                        && absProblem[1] != 1
                                                                                        && absProblem[3] != 1
                                                                                        && absProblem[4] != 1) {
                                Double_t correctionPre = ( -yy[2+2] + 3*yy[2+1] + yy[2-1] )/3.;
                                yy[2] = correctionPre;
                                correctionCounter ++;
                        }

                       for ( Int_t itime = 3; itime < ntime -2; ++ itime ) {
                                //Assuming acceralation is constant, so
                                Double_t correctionPre = ( -yy[itime+2] + 3*yy[itime+1] + yy[itime-1] )/3.;
                                Double_t correctionPst = ( yy[itime+1] + 3*yy[itime-1] - yy[itime-2] )/3.;
                                Double_t correction = ( correctionPre + correctionPst ) /2.;

                                if ( absProblem[itime]  ==1 ) {
                                        if (
                                                                absProblem[itime-2] !=1
                                                 && absProblem[itime-1] !=1
                                                 && absProblem[itime+1] !=1
                                                 && absProblem[itime+2] ==1 ){
                                                yy[itime] = correctionPst;
                                                correctionCounter ++;
                                        } else if (
                                                                absProblem[itime-2] !=1
                                                 && absProblem[itime-1] !=1
                                                 && absProblem[itime+1] !=1
                                                 && absProblem[itime+2] !=1 ){
                                                yy[itime] = correction;
                                                correctionCounter ++;
                                        } else if (
                                                          absProblem[itime-2] ==1
                                                 && absProblem[itime-1] !=1
                                                 && absProblem[itime+1] !=1
                                                 && absProblem[itime+2] !=1 ){
                                                yy[itime] = correctionPre;
                                                correctionCounter ++;
                                        }
                                }
                        }
                        if ( absProblem[ntime-2] == 1
                                && absProblem[ntime-2-2] !=1
                                && absProblem[ntime-2-1] !=1
                                && absProblem[ntime+2+1] !=1 ) {
                                Double_t correctionPst = ( yy[ntime-2+1] + 3*yy[ntime-2-1] - yy[ntime-2-2] )/3.;
                                yy[ntime-2] = correctionPst;
                        }
                }
                // compiling from yy[] to Data[].
                for ( Int_t itime = 0; itime < ntime; itime ++ ) {
                        Data[itime] =  yy[itime];
                }

                if ( isDIRAC ) correctionCounter = 1981;
                if ( isSaturation       ) correctionCounter = 2020;
                return  correctionCounter;


  }

short MTConvert125MHzDetector::CorrectCorruptedWfm_type2(std::vector<short>& data, int maxbitloss){

          short n_correct = 0;



          return n_correct;

  }
