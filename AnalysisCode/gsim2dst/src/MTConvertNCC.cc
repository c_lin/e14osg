#include "gsim2dst/MTConvertNCC.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

#include <TRandom3.h>

MTConvertNCC::MTConvertNCC( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::NCC, userFlag )
{
    m_detname="NCC";
    unsigned int index = 0;
    // 3 body module
    for( int commonID=0; commonID<48; commonID++ ){
        for( int cfmr=0; cfmr<4; cfmr++ ){
            IDIndexMap.insert( std::pair< Int_t, unsigned int>( commonID*10+cfmr, index ) );
            IndexIDArray[index] = commonID*10+cfmr;
            index++;
        }
    }
    // outer module
    for( int commonID=50; commonID<58; commonID++ ){
        IDIndexMap.insert( std::pair< Int_t, unsigned int>( commonID*10, index ) );
        IndexIDArray[index] = commonID*10;
        index++;
    }
    // HINEMOS
    for( int commonID=60; commonID<64; commonID++ ){
        IDIndexMap.insert( std::pair< Int_t, unsigned int>( commonID*10, index ) );
        IndexIDArray[index] = commonID*10;
        index++;
    }


    DetNumber = DetectorNCH;
    DetTrueNumber = DetectorNCH;
    for( int ich=0; ich<DetNumber; ich++ ){
        DetModID[ich] = IndexIDArray[ich];
        DetTrueModID[ich] = IndexIDArray[ich];
    }

    pulseGenerator = new MTPulseGenerator();

    WfmTimeShift = MTBP::NCCWfmTimeShift;
    WfmSigma0 = MTBP::NCCWfmSigma0;
    WfmAsymPar = MTBP::NCCWfmAsymPar;

    m_E14BP = new E14BasicParamManager::E14BasicParamManager();

    Initialize();
}

MTConvertNCC::~MTConvertNCC()
{
    delete pulseGenerator;
    delete m_E14BP;
}

void MTConvertNCC::Initialize( void )
{
    for( int ich=0; ich<DetNumber; ich++ ){
        DetEne[ich] = 0;
        DetTime[ich] = 0;
        DetPTime[ich] = MTBP::Invalid;
        DetFallTime[ich] = MTBP::Invalid;
        for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
            DetWfm[ich][iSample] = 0;

        DetTrueEne[ich] = 0;
        DetTrueTime[ich] = 0;
    }
}


void MTConvertNCC::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
    TClonesArray *digiArray = GetDigiArray();

    number = DetectorNCH;
    for( int ich=0; ich<number; ich++ ){
        modID[ich] = IndexIDArray[ich];
        ene[ich]   = 0;
        time[ich]  = 0;
    }

    int nDigi = digiArray->GetEntries();
    for( int idigi=0; idigi<nDigi; idigi++ ){
        GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
        ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
        time[IDIndexMap[digi->modID]]  = (float)digi->time /8.; // [ns]->[clock]
    }
}

void MTConvertNCC::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
//added th28th Feb, 2014 by Maeda Yosuke
//updated at 4th May, 2014 (different wfm param for outer & hinemos, still same threshold and nominal time)
{
    number = DetectorNCH;

    TClonesArray *digiArray = GetDigiArray();
    int nDigi = digiArray->GetEntries();

    TClonesArray *mtimeArray = GetMTimeArray();
    std::vector< pEnergyTime > propagatedVecCommon[MTBP::NCCnCommonChannel];
    std::vector< pEnergyTime > propagatedVecIndividuals[MTBP::NCCnCommonChannel][3];
    std::vector< pEnergyTime > propagatedVecOthers;
    std::vector< pEnergyTime > propagatedClusteredVec;

    const Double_t k_Pitch = (m_dbWfmGenerator) ? 8. : 0.1;

    // digi loop
    for( int idigi=0; idigi<nDigi; idigi++ ){
        GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
        Int_t ChIndex = digi->modID/10;
        Bool_t IsNormalModule = (ChIndex<50);
        std::vector< pEnergyTime >& propagatedVec =
            ( IsNormalModule ? propagatedVecCommon[ChIndex] : propagatedVecOthers );

        // mtime loop    
        for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
            GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

            //no detector response is implemented
            energyTime.energy = mtime->energy;//GetVisibleEnergy( mtime->r.z(), mtime->energy );
            energyTime.time   = mtime->time;//GetVisibleTime( mtime->r.z(), mtime->time );
            propagatedVec.push_back( energyTime );
			if(IsNormalModule){
	            int IndividualsIndex = (digi->modID%4)-1;
				if(IndividualsIndex>=0)
					propagatedVecIndividuals[ChIndex][IndividualsIndex].push_back( energyTime );
			}
        }

        // timing resolution (2019/06/15 added by S.Shinohara based on g4ana study) //
        // same parameter used to common and outer//
        Float_t TimeSmearingOuter = 0;
        if( m_dbWfmGenerator ){
            if( !propagatedVec.empty() ){
                pulseGenerator->SetEnergyTime( propagatedVec );

                Float_t EnergyForTimeSmearing = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy(-1,1000);
                if( EnergyForTimeSmearing<0.1 ) EnergyForTimeSmearing = 0.1;
                Float_t SmearingSigma = sqrt( DetSmearingSigma / EnergyForTimeSmearing );
                TimeSmearingOuter = gRandom->Gaus( 0, SmearingSigma );
            }
        }

        if( !IsNormalModule ){//outer crystal and Hinemos  
            Double_t TimeShiftCorrection = (-1)*WfmTimeShift;
            if( ChIndex<60 ){//IsOuterCrystal
                TimeShiftCorrection += MTBP::NCCWfmTimeShift;
                TimeShiftCorrection += TimeSmearingOuter;
                WfmFunc->SetParameter( 2, MTBP::NCCOuterWfmSigma0 );
                WfmFunc->SetParameter( 3, MTBP::NCCOuterWfmAsymPar );
            }
            else{//IsHinemos
                TimeShiftCorrection += MTBP::HinemosWfmTimeShift+MTBP::TimeDiffCrystalAndScintiForNCC;
                WfmFunc->SetParameter( 2, MTBP::HinemosWfmSigma0 );
                Double_t AsymPar = gRandom->Gaus( MTBP::HinemosWfmAsymPar,
                    MTBP::HinemosWfmAsymParSigma );
                WfmFunc->SetParameter( 3, AsymPar );
            }

            ClusteringMtime( propagatedVec, propagatedClusteredVec, k_Pitch );
            if( m_dbWfmGenerator && ChIndex<60 ) // outer modules
                GenerateDbWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec, TimeShiftCorrection );
            else
                GenerateWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec,
                    TimeShiftCorrection );

            propagatedVec.clear();
            propagatedClusteredVec.clear();
        }

    }

    //common channel
    WfmFunc->SetParameter( 2, WfmSigma0 );
    WfmFunc->SetParameter( 3, WfmAsymPar );

    for( Int_t iCh=0 ; iCh<MTBP::NCCnCommonChannel ; iCh++ ){

        if( !propagatedVecCommon[iCh].empty() ){

			propagatedClusteredVec.clear();
            // timing resolution (2019/06/15 added by S.Shinohara based on g4ana study) //
            // same parameter used to common and outer//
            Float_t TimeShiftCorrection = 0;
            Float_t TimeSmearingCommon = 0;
            if( m_dbWfmGenerator ){
                pulseGenerator->SetEnergyTime( propagatedVecCommon[iCh] );
                Float_t EnergyForTimeSmearing = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy(-1,1000);
                Float_t SmearingSigma = sqrt( DetSmearingSigma / EnergyForTimeSmearing );
                TimeSmearingCommon = gRandom->Gaus( 0, SmearingSigma );
                TimeShiftCorrection = TimeSmearingCommon;
            }

            ClusteringMtime( propagatedVecCommon[iCh], propagatedClusteredVec, k_Pitch );
            if( m_dbWfmGenerator )
                GenerateDbWfmFromMTimeVec( IDIndexMap[iCh*10], propagatedClusteredVec, TimeShiftCorrection );
            else
                GenerateWfmFromMTimeVec( IDIndexMap[iCh*10], propagatedClusteredVec );
        }

    	for( Int_t iind=0 ; iind<3 ; iind++ ){
			if( !propagatedVecIndividuals[iCh][iind].empty() ){
				propagatedClusteredVec.clear();

        	    ClusteringMtime( propagatedVecIndividuals[iCh][iind], propagatedClusteredVec, k_Pitch );
				GenerateWfmFromMTimeVec( IDIndexMap[iCh*10+1+iind], propagatedClusteredVec );
        	}
		}

    }

    if( !EnableAccidentalOverlay ){
        for( Int_t iCh=0 ; iCh<number ; iCh++ ){//do not process for individual channel
            if( iCh>4*MTBP::NCCnCommonChannel || (iCh/4)<MTBP::NCCnCommonChannel )
                GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
                    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014

            EvalFTTChisq( iCh, &DetWfm[iCh][0], DetFTTChisq[iCh], DetFTTTime[iCh] );
        }
    }

    return;
}

void MTConvertNCC::GetEtWfm()
{
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
        DetEtSumWfm[iSample] = 0;

    for( Int_t iCh=0; iCh<DetectorNCH; iCh++ )
        if( DetModID[iCh]<500 && (DetModID[iCh]%10)==0 && AccidentalDetECalibConst[iCh]>0 ){//sum up only common channel
            for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
                if( ! std::isnan(DetWfm[iCh][iSample]) )
                    DetEtSumWfm[iSample] += (Long64_t)(DetWfm[iCh][iSample]/AccidentalDetECalibConst[iCh]);
        }

    return;
}

