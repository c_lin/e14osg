#include "gsim2dst/MTConvertBHPV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


MTConvertBHPV::MTConvertBHPV( Int_t userFlag ) : MTConvert500MHzDetector( MTBP::BHPV, userFlag )
{
   m_detname="BHPV";
  for( Int_t index = 0 ; index < 2*MTBP::nModBHPV ; ++index ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index) );
    IndexIDArray[index] = index;
  }
  IDIndexMap.insert( std::pair< Int_t, unsigned int>( MTBP::BHTSModuleID, 2*MTBP::nModBHPV ) );
  IndexIDArray[2*MTBP::nModBHPV] = MTBP::BHTSModuleID;
  
  IDIndexMap.insert( std::pair< Int_t, unsigned int>( MTBP::BHTSModuleID+1, 2*MTBP::nModBHPV+1 ) );
  IndexIDArray[2*MTBP::nModBHPV+1] = MTBP::BHTSModuleID+1;
  
  for( Int_t ich = 0 ; ich < DetectorNCH ; ++ich ){
    DetModID[ich]     = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  Initialize();
}

MTConvertBHPV::~MTConvertBHPV()
{
  delete m_E14BP;
}

void MTConvertBHPV::ConvertFromDigiWithResponse(){
  GetFromDigiWithResponse( DetNumber, DetModID, DetNHit, DetEne, DetTime );
}

void MTConvertBHPV::GetTrueInfo( Int_t& number, Int_t* modID, Short_t* nhit,
				 Float_t (*ene) [MTBP::NMaxHitPerEvent],
				 Float_t (*time)[MTBP::NMaxHitPerEvent] )
{
  TClonesArray *digiArray = GetDigiArray();
  number = DetectorNCH;
  for( Int_t iCh = 0 ; iCh < number ; ++iCh )
    modID[iCh] = IndexIDArray[iCh];
  
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t idigi = 0 ; idigi < nDigi ; ++idigi ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    Int_t index = IDIndexMap[digi->modID];
    Int_t iHit  = nhit[index];
    ene[index][iHit]  = (float)digi->energy;
    time[index][iHit] = (float)digi->time / MTBP::SampleInterval500; //[ns]->[clock]
    ++nhit[index];
  } 
}

void MTConvertBHPV::GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit,
					     Float_t (*ene) [MTBP::NMaxHitPerEvent], 
					     Float_t (*time)[MTBP::NMaxHitPerEvent] )
{
  TClonesArray *digiArray = GetDigiArray(); 
  number = DetectorNCH;
  for( Int_t iCh = 0 ; iCh < number ; ++iCh ) 
    modID[iCh] = IndexIDArray[iCh];
  
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t idigi = 0 ; idigi < nDigi ; ++idigi ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At(idigi);
    Int_t ModID = digi->modID;
    Int_t index = IDIndexMap[ModID];
    Float_t TimeSmearing = 0;//[clock]
    if( ModID<2*MTBP::nModBHPV ){
      Float_t nPE       = (Float_t)(digi->energy);
      Float_t SmearedLY = gRandom->Gaus( nPE, MTBP::BHPV1peSigma*TMath::Sqrt(nPE) );
      ene[index][0] = SmearedLY;
      if( SmearedLY < MTBP::BHPVTimeRes_MinLY ) 
	SmearedLY = MTBP::BHPVTimeRes_MinLY;
      TimeSmearing = gRandom->Gaus( 0, TMath::Hypot( MTBP::BHPVTimeRes_b / TMath::Sqrt(SmearedLY),
						     MTBP::BHPVTimeRes_c / SmearedLY ) );
    }else{
      ene[index][0] = digi->energy;
    }
    time[index][0] = digi->time / MTBP::SampleInterval500 + TimeSmearing;//[ns]->[clock]
    time[index][0] += AccidentalTimeShift;
    ++nhit[index];
  }
  
  return;
}

