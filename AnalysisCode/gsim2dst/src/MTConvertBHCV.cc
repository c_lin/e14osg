#include "gsim2dst/MTConvertBHCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTConvertBHCV::MTConvertBHCV( Int_t userFlag ) : MTConvert500MHzDetector( MTBP::BHCV, userFlag )
{  
   m_detname="BHCV";
  for( Int_t index = 0; index < DetectorNCH ; ++index ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index ) );
    IndexIDArray[index] = index;
  }
  
  for( Int_t ich = 0 ; ich < DetectorNCH ; ++ich ){
    DetModID[ich]     = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  Initialize();  
}

MTConvertBHCV::~MTConvertBHCV()
{
  delete m_E14BP;
}

void MTConvertBHCV::ConvertFromMTimeWithResponse(){
  GetFromMTimeWithResponse( DetNumber, DetModID, DetNHit, DetEne, DetTime );
}

void MTConvertBHCV::GetTrueInfo( Int_t& number, Int_t* modID, Short_t* nhit, 
				 Float_t (*ene) [MTBP::NMaxHitPerEvent],
				 Float_t (*time)[MTBP::NMaxHitPerEvent] )
{
  TClonesArray *digiArray = GetDigiArray();
  number = DetectorNCH;
  for( Int_t iCh = 0 ; iCh < number ; ++iCh )
    modID[iCh] = IndexIDArray[iCh];
  
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t idigi = 0 ; idigi < nDigi ; ++idigi ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At(idigi);
    Int_t index = IDIndexMap[digi->modID];
    Int_t iHit  = nhit[index];
    ene[index][iHit]  = (Float_t)digi->energy;
    time[index][iHit] = (Float_t)digi->time / MTBP::SampleInterval500; //[ns]->[clock]
    ++nhit[index];
  } 
}

void MTConvertBHCV::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, 
					      Float_t (*ene) [MTBP::NMaxHitPerEvent], 
					      Float_t (*time)[MTBP::NMaxHitPerEvent] )
{
  TClonesArray *digiArray  = GetDigiArray();
  TClonesArray *mtimeArray = GetMTimeArray();
  
  number = DetectorNCH;
  for( Int_t iCh = 0 ; iCh < number ; ++iCh )
    modID[iCh] = IndexIDArray[iCh];
  
  // mtime loop
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t iDigi = 0 ; iDigi < nDigi ; ++iDigi ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( iDigi );
    
    Int_t  DigiModID = digi->modID;
    Int_t  index     = IDIndexMap[DigiModID];
    Bool_t IsNorth   = ( DigiModID < MTBP::BHCVSouthModStartID );//North side is positive direction of X axisx
    
    std::vector<pEnergyTime> etVec;
    for( UInt_t imtime = digi->mtimeEntry ; imtime < digi->mtimeEntry + digi->mtimeSize ; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      Double_t HitXFromEdge = TMath::Abs( mtime->r.x()
					  + (IsNorth?1:(-1)) * MTBP::BHCVCentralOverlap/2. );
      Double_t PropLength = MTBP::BHCVModSizeX-HitXFromEdge;
      Double_t AttenEne   = ( mtime->energy * MTBP::BHCVEdgeCalibFactor
			      *TMath::Exp( (HitXFromEdge - MTBP::BHCVCentralOverlap/2.)
					   / MTBP::BHCVAttenLength ) );

      /////add quenching effect, (but kB is set to be 0 to ignore this effect forawhile )
      Double_t CorrectedEne = AttenEne * (1 + MTBP::BirksConst * MTBP::BHCVMIPEnergy / MTBP::BHCVModThickness ) / ( 1 + MTBP::BirksConst * AttenEne / MTBP::BHCVModThickness);
      Double_t PropTime     = mtime->time + PropLength /MTBP::BHCVLightPropSpeed;
       
      m_EnergyTime.energy = CorrectedEne;
      m_EnergyTime.time   = PropTime;
      etVec.push_back( m_EnergyTime );
    }
    
    if( !etVec.empty() ){
      Double_t MTimeEnergy = -9999;
      Double_t MTimeTime   = -9999;
      GetModEnergyTimeFromMTime( etVec, MTimeEnergy, MTimeTime );
      ene[index][nhit[index]]  = (Float_t)MTimeEnergy;
      time[index][nhit[index]] = (Float_t)MTimeTime / MTBP::SampleInterval500;
      time[index][nhit[index]] += AccidentalTimeShift;
      if( TMath::Abs(MTimeEnergy)>0 ){
	Double_t TimeSmearing = gRandom->Gaus( 0, TMath::Hypot( MTBP::BHCVTimeRes_a,
								MTBP::BHCVTimeRes_c/MTimeEnergy) );
	time[index][nhit[index]] += TimeSmearing;
      }
      ++nhit[index];
    }
  }
  return;
}

void MTConvertBHCV::GetModEnergyTimeFromMTime( std::vector<pEnergyTime>& etVec, Double_t& ene, Double_t& time )
{  
  if( etVec.empty() ) return;
  std::sort( etVec.begin(), etVec.end(), pEnergyTime::fasterHit );
  
  Double_t EarliestTime = etVec[0].time;
  Double_t EneSum = 0;
  for( std::vector<pEnergyTime>::iterator it = etVec.begin() ; it!=etVec.end() ; it++ ){
    if( it->time - EarliestTime < MTBP::BHCVCutOffTime ) EneSum += it->energy;
    else break;
  }
  ene  = EneSum;
  time = EarliestTime;

  
  return;
}

