#include "gsim2dst/MTConvertNewBHCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTConvertNewBHCV::MTConvertNewBHCV( Int_t userFlag ) : MTConvert500MHzDetector( MTBP::newBHCV, userFlag )
{
  m_detname="newBHCV";
  for( Int_t index=0; index < DetectorNCH ; ++index ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index ) );
    IndexIDArray[index] = index;
  }
  
  for( Int_t ich = 0 ; ich < DetectorNCH ; ++ich ){
    DetModID[ich]     = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  Initialize();
  
  std::string JitterDataFile = std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
    + std::string("/gsim2dst/newBHCV/TimeJitter.root");
  
  f_Jitter = new TFile(JitterDataFile.c_str());
  for( Int_t i = 0 ; i < 3 ; ++i ) 
    h_TimeJitter[i]=(TH1F*)f_Jitter->Get(Form("h_TimeJitter%d",i));  
}

MTConvertNewBHCV::~MTConvertNewBHCV()
{
  f_Jitter->Close();
  delete m_E14BP;
}

void MTConvertNewBHCV::ConvertFromDigiWithResponse(){
  GetFromDigiWithResponse( DetNumber, DetModID, DetNHit, DetEne, DetTime );
}

void MTConvertNewBHCV::GetTrueInfo( Int_t& number, Int_t* modID, Short_t* nhit, 
				    Float_t (*ene) [MTBP::NMaxHitPerEvent], 
				    Float_t (*time)[MTBP::NMaxHitPerEvent] )
{
  TClonesArray *digiArray = GetDigiArray();
  number = DetectorNCH;
  for( Int_t iCh = 0 ; iCh < number ; ++iCh )
    modID[iCh] = IndexIDArray[iCh];
  
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t idigi = 0; idigi < nDigi ; ++idigi ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    Int_t index = IDIndexMap[digi->modID];
    Int_t iHit  = nhit[index];

    ene[index][iHit]  = (float)digi->energy;
    time[index][iHit] = (float)digi->time / MTBP::SampleInterval500; //[ns]->[clock]
    ++nhit[index];
  }
}

void MTConvertNewBHCV::GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit,
						Float_t (*ene) [MTBP::NMaxHitPerEvent],
						Float_t (*time)[MTBP::NMaxHitPerEvent] )
{  
  TClonesArray *digiArray = GetDigiArray();
  number = DetectorNCH;
 
 for( Int_t iCh = 0 ; iCh < number ; ++iCh )
    modID[iCh] = IndexIDArray[iCh];
  
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t idigi = 0 ; idigi < nDigi ; ++idigi ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At(idigi);
    Int_t ModID = digi->modID;
    Int_t index = IDIndexMap[ModID];
    
    Float_t TimeSmearing = h_TimeJitter[ModID/16]->GetRandom(); //[clock]
    
    ene[index][0]  = digi->energy;
    time[index][0] = digi->time / MTBP::SampleInterval500 + TimeSmearing;//[ns]->[clock]
    time[index][0] += AccidentalTimeShift;
    ++nhit[index];
  }
  return;
}


