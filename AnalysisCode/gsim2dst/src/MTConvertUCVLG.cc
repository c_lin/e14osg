#include "gsim2dst/MTConvertUCVLG.h"
#include <fstream>
#include <sstream>
#include <cstdlib>

Double_t ucvlgfunc(Double_t *x, Double_t* par){
	double t = x[0]-par[1];
	if(t<0) return 0;

	double y = par[0]*sin(t/par[2])*exp(-(t-par[4])/par[3]);
	return y;	
}

MTConvertUCVLG::MTConvertUCVLG( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::UCVLG, userFlag )
{

  
  for( int ich=0; ich<MTBP::DetNChannels[MTBP::UCVLG]; ich++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( ich, ich ) );
    IndexIDArray[ich] = ich;
  }

  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }
  
  pulseGenerator = new MTPulseGenerator();

  WfmTimeShift = 0;
  WfmSigma0 = MTBP::UCVLGWfmSigma0;
  WfmAsymPar = MTBP::UCVLGWfmAsymPar;
  
  Initialize();
  
  ran = new TRandom3(1);
  
  func = new TF1("UCVLGFunc", ucvlgfunc, -1000, 1000, 5);

}

MTConvertUCVLG::~MTConvertUCVLG()
{
	delete ran;
	delete pulseGenerator;
}


void MTConvertUCVLG::Initialize( void )
{
	
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}



void MTConvertUCVLG::GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    for( int sl=0; sl<2; sl++ ){
      ene[IDIndexMap[digi->modID+sl]]   = (float)digi->energy;
      time[IDIndexMap[digi->modID+sl]]  = (float)digi->time /8.; // [ns]->[clock]
    }
  }

}

void MTConvertUCVLG::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  
  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedVec.clear();
    propagatedClusteredVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      energyTime.energy = mtime->energy; // attenuated energy      
      energyTime.time   = GetVisibleTime( mtime->r.y(), mtime->time, mtime->energy);
      propagatedVec.push_back( energyTime );


      
    } // imtime
    
    // If there is no hit before 1us, it continues.
    if( propagatedVec.size() == 0 ) continue;
    
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );

    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    modID[digi->modID] = digi->modID;
    ene[digi->modID]   = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[digi->modID]  = (float)pulseGenerator->GetPseudoConstantFractionTime(); // [ns]

    // [ns] -> [clock]
    time[digi->modID] /= 8.;
    
  }
  
}

void MTConvertUCVLG::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
{
  number = DetectorNCH;
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedVec.clear();
    propagatedClusteredVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      energyTime.energy = GetVisibleEnergy( mtime->r.y(), mtime->energy );
      energyTime.time   = GetVisibleTime( mtime->r.y(), mtime->time , mtime->energy);
      propagatedVec.push_back( energyTime );


    } // imtime
    
    // If there is no hit before 1us, it continues.
    if( propagatedVec.size() == 0 ) continue;



	static int init = 0;
	if(init==0){	
		func->SetParameter( 0, 1 );
	   	func->SetParameter( 1, 0 );
		func->SetParameter( 2, 100.0 );
		func->SetParameter( 3, 77.55 );
		func->SetParameter( 4, -1.4 );
		init++;
	}
	
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );

    GenerateWfmFromMTimeVec( digi->modID/6, propagatedClusteredVec, 0 , func);
  }
  
  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<number; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
	}
  }

  return;
}




double MTConvertUCVLG::GetVisibleEnergy( double y, double e)
{
	const double npe_MeV = 30.0/0.19;
	double npe_ave = e*npe_MeV*exp(y/300.0);
	int npe = ran->Poisson(npe_ave);
	return npe/npe_MeV;
}


double MTConvertUCVLG::GetVisibleTime( double y, double t , double e)
{
	const double tres = 2.0;
   	return t + (120.0-y)/100.0 + ran->Gaus(0, tres);
}



void MTConvertUCVLG::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
    time[IDIndexMap[digi->modID]]  = (float)digi->time /8.; // [ns]->[clock]
  }
}


