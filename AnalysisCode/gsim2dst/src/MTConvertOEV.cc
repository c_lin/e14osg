#include "gsim2dst/MTConvertOEV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <fstream>
#include <cstdlib>

#include <TRandom3.h>

MTConvertOEV::MTConvertOEV( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::OEV, userFlag )
{
   m_detname="OEV";
  for( unsigned int index=0; index<(UInt_t)MTBP::DetNChannels[MTBP::OEV]; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index ) );
    IndexIDArray[index] = index;
  }
  
  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = ich;
    DetTrueModID[ich] = ich;
  }
  Initialize();

  // Read map file
  SimID2ModIDArray = new Int_t [64]();
  std::string headerLine;
  double dummy = 0.0;
  unsigned int simID = 0;
  unsigned int modID = 0;
  //relative path since 18th May, 2014
  //use env. var. since 29th May, 2014
  std::string filename = std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
    + std::string("/gsim2dst/OEV/OEV_map.txt");
  std::ifstream ifs( filename.c_str() );
  getline( ifs, headerLine );
  while( ifs >> simID >> modID >> dummy >> dummy >> dummy >> dummy ){
    SimID2ModIDArray[simID] = modID;
  }

  pulseGenerator = new MTPulseGenerator();

  WfmTimeShift = MTBP::OEVWfmTimeShift;
  WfmSigma0 = MTBP::OEVWfmSigma0;
  WfmAsymPar = MTBP::OEVWfmAsymPar;

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

}


MTConvertOEV::~MTConvertOEV()
{
  delete[] SimID2ModIDArray;
  delete pulseGenerator;
  delete m_E14BP;
}


void MTConvertOEV::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 

    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}

void MTConvertOEV::ConvertFromMTimeWithResponse( void )//added at 4th Jan, 2014
{
  GetFromMTimeWithResponse( DetNumber, DetModID, DetEne, DetTime );
}

void MTConvertOEV::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    if( ene[SimID2ModIDArray[digi->modID]] == 0 ){
      time[SimID2ModIDArray[digi->modID]] = (float)digi->time /8.; // [ns]->[clock]
    }else{
      if( time[SimID2ModIDArray[digi->modID]] > (float)digi->time ){
	time[SimID2ModIDArray[digi->modID]] = (float)digi->time /8.; // [ns]->[clock]
      }
    }

    ene[SimID2ModIDArray[digi->modID]] += (float)digi->energy;
  }
}

void MTConvertOEV::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )//added at 26th Dec, 2013
{
  static const double s_OEVZPosition = DetZPosition;

  TClonesArray *digiArray = GetDigiArray();
  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector<pEnergyTime> propagatedVec;
  
  number = DetectorNCH;
  for( int iCh=0; iCh<number ; iCh++ ) modID[iCh] = IndexIDArray[iCh];
  
  // mtime loop
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t iDigi=0 ; iDigi<nDigi ; iDigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( iDigi );
    propagatedVec.clear();
    
    //Int_t DigiModID = digi->modID;
    Int_t index = SimID2ModIDArray[digi->modID];
    
    for( UInt_t imtime=digi->mtimeEntry ; imtime<digi->mtimeEntry+digi->mtimeSize ; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      Double_t zPosFromUpstreamEdge = mtime->r.z()-s_OEVZPosition;
      Double_t PropLength = MTBP::OEVZLength - zPosFromUpstreamEdge;
      Double_t CorrectedEne = mtime->energy;
      Double_t PropTime = mtime->time + PropLength/MTBP::LCVLightPropSpeed;
      
      energyTime.energy = CorrectedEne;
      energyTime.time = PropTime;
      propagatedVec.push_back( energyTime );
    }
    
    if( !propagatedVec.empty() ){
      pulseGenerator->SetEnergyTime( propagatedVec );
      
      Float_t SmearedEnergy = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy();
      if( MTBP::OEVLightYieldPerMeV>0 ){
	Int_t nPE = gRandom->Poisson( SmearedEnergy*MTBP::OEVLightYieldPerMeV );
	SmearedEnergy = (Float_t)(nPE)/MTBP::OEVLightYieldPerMeV;
	Float_t OnePhotonSmearing = gRandom->Gaus( 0, TMath::Sqrt(nPE)*MTBP::OEVOnePhotonSigma ); 
	SmearedEnergy += OnePhotonSmearing;
      }
      
      ene[index] = SmearedEnergy;
      time[index] = (Float_t)pulseGenerator->GetPseudoConstantFractionTime()/8.;//[ns]-->[clock]
      
      Float_t EnergyForTimeSmearing = ( ene[index]<MTBP::OEVMinEnergyTimeSmearing
					? MTBP::OEVMinEnergyTimeSmearing : ene[index] );
      Float_t TimeSmearing = gRandom->Gaus( 0, TMath::Hypot( MTBP::OEVTimeRes_a,
							     MTBP::OEVTimeRes_b/TMath::Sqrt(EnergyForTimeSmearing) ) );
      time[index] += TimeSmearing;
    }
  }
  
  return;
}

void MTConvertOEV::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
//added at 4th May, 2014 (PTime and FallTime added)
{
  static const double s_OEVZPosition = DetZPosition;

  number = DetectorNCH;
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();
  
  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  //always use this value, added at 14th Feb, 2014 by Maeda Yosuke
  WfmFunc->SetParameter( 2, WfmSigma0 );
  
  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    propagatedVec.clear();
    propagatedClusteredVec.clear();
    Int_t iCh = SimID2ModIDArray[digi->modID];//dst channel index
    
    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      Double_t zPosFromUpstreamEdge = mtime->r.z()-s_OEVZPosition;
      Double_t PropLength = MTBP::OEVZLength - zPosFromUpstreamEdge;
      Double_t CorrectedEne = mtime->energy;
      Double_t PropTime = mtime->time + PropLength/MTBP::LCVLightPropSpeed;
      
      energyTime.energy = CorrectedEne;
      energyTime.time = PropTime;
      propagatedVec.push_back( energyTime );
    }
    
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );
    WfmFunc->SetParameter( 3, gRandom->Gaus( WfmAsymPar, MTBP::OEVWfmAsymParSigma ) );
    GenerateWfmFromMTimeVec( iCh, propagatedClusteredVec );
    //GenerateWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec );
  }
  
  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<number; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
    }
  }
  
  return;
}

