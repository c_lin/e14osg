#include <cstdlib>
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "gsim2dst/MTConvertCV.h"

const double MTConvertCV::defaultLightYield = 150.; // /MeV


MTConvertCV::MTConvertCV( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::CV, userFlag )
{
   m_detname="CV";  
  for( int ich=0; ich<MTBP::DetNChannels[MTBP::CV]; ich++ ){
    if( ich < 96 ){
      // front plane
      IDIndexMap.insert( std::pair< Int_t, unsigned int>( ich, ich ) );
      IndexIDArray[ich] = ich;
    }else{
      // rear plane
      IDIndexMap.insert( std::pair< Int_t, unsigned int>( ich+4, ich ) );
      IndexIDArray[ich] = ich+4;
    }
  }

  
  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  pHandler = MTPositionHandler::GetInstance();
  pulseGenerator   = new MTPulseGenerator();
  lightYieldNumber = new int    [MTBP::CVNModules]();
  lightYield       = new double [MTBP::CVNModules][3][20]();
  meanLightYield   = new double [MTBP::CVNModules]();
  m_1peIntADC      = new double [MTBP::DetNChannels[MTBP::CV]]();//added at 24th Jun, 2014 (MY)
  LoadLightYield( userFlag );
  
  m_reCalibrationFactor = new double[MTBP::CVNModules]();
  LoadReCalibrationFactor( userFlag );
  
  m_pedestalWidth         = new double[MTBP::DetNChannels[MTBP::CV]]();
  m_temporalPedestalWidth = new double[MTBP::DetNChannels[MTBP::CV]]();
  LoadPedestalWidth( userFlag );

// 2022/0104/ added to use Ecalib estimated with MC by coterra: see
// 2021 Dec. collaboration meeting 
// https://kds.kek.jp/event/38781/contributions/201279/attachments/149915/188005/0Lc04_collab_CVvalidCoterra.pdf
	m_EstimatedEcalibFromMC = new double[MTBP::DetNChannels[MTBP::CV]]();
  LoadEstimatedEcalibFromMC( userFlag );
////////////////////////////////////////////

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();  

  WfmTimeShift       = MTBP::CVWfmTimeShift;
  WfmSigma0          = MTBP::CVWfmSigma0;
  WfmAsymPar         = MTBP::CVWfmAsymPar;

  Initialize();
}


MTConvertCV::~MTConvertCV()
{
  delete pulseGenerator;
  delete[] lightYieldNumber;
  delete[] lightYield;
  delete[] meanLightYield;
  delete[] m_1peIntADC;
  delete[] m_reCalibrationFactor;
  delete[] m_pedestalWidth;
  delete[] m_temporalPedestalWidth;
// 2022/0104/ added to use Ecalib estimated with MC by coterra: see
// 2021 Dec. collaboration meeting 
// https://kds.kek.jp/event/38781/contributions/201279/attachments/149915/188005/0Lc04_collab_CVvalidCoterra.pdf
	delete[] m_EstimatedEcalibFromMC;
  delete   m_E14BP;
}



void MTConvertCV::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;

    m_temporalPedestalWidth[ich] = 0;
  }
}


void MTConvertCV::GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    for( int sl=0; sl<2; sl++ ){
      ene[IDIndexMap[digi->modID+sl]]   = (float)digi->energy/2;
      time[IDIndexMap[digi->modID+sl]]  = (float)digi->time /8.; // [ns]->[clock]
    }
  }

}


void MTConvertCV::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
    time[IDIndexMap[digi->modID]]  = (float)digi->time / 8.; // [ns]->[clock]
  }
}

//updated at 11th Mar, 2014
//same light yield smearing with GetFromMTimeWithPulseShape is available
void MTConvertCV::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();
  
  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVecShort;
  std::vector< pEnergyTime > propagatedVecLong;
  //std::vector< pEnergyTime > propagatedClusteredVec;//do not apply mtime clustering 

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    propagatedVecShort.clear();
    propagatedVecLong.clear();
    
    //modIndex = pHandler->GetCVModIndex( digi->modID );
    
    Int_t ModID = digi->modID;
    Int_t iMod = IDIndexMap[ModID];
    
    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry ; imtime<digi->mtimeEntry+digi->mtimeSize ; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
			if( mtime->time > 1000 ) continue;
      
      Double_t MtimeEnergy = mtime->energy;
      Double_t MtimeTime = mtime->time;
      Double_t ModHitPosFromXYAxis = GetModHitPos( ModID, mtime->r.x(), mtime->r.y() );
      Double_t CommonDelayTime = 0;//hits in regions with lifted fibers have some delay
      Double_t DeltaPropTime = GetLightPropTime( ModID, ModHitPosFromXYAxis, CommonDelayTime );
      Double_t LYPerMeV[2] = {};//for short and long side
      GetLightYield( ModID, ModHitPosFromXYAxis, LYPerMeV[0], LYPerMeV[1] );
      
      for( Int_t sl=0 ; sl<2 ; sl++ ){

// 2022/0104/ added to use Ecalib estimated with MC by coterra: see
// 2021 Dec. collaboration meeting 
// https://kds.kek.jp/event/38781/contributions/201279/attachments/149915/188005/0Lc04_collab_CVvalidCoterra.pdf
	if (m_userFlag<20190200){
		energyTime.energy = gRandom->Poisson( MtimeEnergy*LYPerMeV[sl] )*(AccidentalDetECalibConst[iMod+sl]*m_1peIntADC[iMod+sl]);
	} else {
		energyTime.energy = gRandom->Poisson( MtimeEnergy*LYPerMeV[sl] )*(m_EstimatedEcalibFromMC[iMod+sl]*m_1peIntADC[iMod+sl]);
	}

	energyTime.time = MtimeTime + CommonDelayTime ;//in [ns] unit
	energyTime.time += ( (sl==0 ? (-1) : (1))*DeltaPropTime );
	energyTime.time += gRandom->Gaus( 0, MTBP::CVSigmaT*sqrt(2) );//add timing resolution
	
	if( sl==0 ) propagatedVecShort.push_back( energyTime );
	else propagatedVecLong.push_back( energyTime );
      }
    }
    
    if( !propagatedVecShort.empty() && !propagatedVecLong.empty() ){ 
      //apply pseudo waveform analysis
      for( Int_t sl=0 ; sl<2 ; sl++ ){
	pulseGenerator->SetEnergyTime( ((sl==0) ? propagatedVecShort : propagatedVecLong) );
	ene[iMod+sl] = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy();
	time[iMod+sl] = (Float_t)pulseGenerator->GetPseudoConstantFractionTime() / 8.; // [ns]
	m_temporalPedestalWidth[iMod+sl] = gRandom->Gaus( 0, m_pedestalWidth[iMod+sl] );
	ene[iMod+sl] += (Float_t)m_temporalPedestalWidth[iMod+sl];
      }
    }
  }// idigi
  
  return;
}

void MTConvertCV::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
//added at 20th Feb, 2014 by Maeda Yosuke
//updated at 4th May, 2014 (PTime and FallTime added), 
{
  
  number = DetectorNCH;
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();
  
  TClonesArray *mtimeArray = GetMTimeArray();
  
  WfmFunc->SetParameter( 2, WfmSigma0 );//always use this value, added at 14th Feb, 2014 by Maeda Yosuke 
  std::vector< pEnergyTime > propagatedVec_S, propagatedVec_L;
  std::vector< pEnergyTime > propagatedClusteredVec_S, propagatedClusteredVec_L;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    
    Int_t ModID = digi->modID;
    Int_t iMod = IDIndexMap[ModID];

    /// for data-base waveform
    if( m_dbWfmGenerator )
      {
	propagatedVec_S.clear();
	propagatedVec_L.clear();
	propagatedClusteredVec_S.clear();
	propagatedClusteredVec_L.clear();
      }

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry ; imtime<digi->mtimeEntry+digi->mtimeSize ; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
			if( mtime->time > 1000 ) continue;
      
      Double_t MtimeEnergy = mtime->energy;
      Double_t MtimeTime = mtime->time;
      Double_t ModHitPosFromXYAxis = GetModHitPos( ModID, mtime->r.x(), mtime->r.y() );
      Double_t CommonDelayTime = 0;//hits in regions with lifted fibers have some delay
      Double_t DeltaPropTime = GetLightPropTime( ModID, ModHitPosFromXYAxis, CommonDelayTime );
      Double_t LYPerMeV[2] = {};//for short and long side
      GetLightYield( ModID, ModHitPosFromXYAxis, LYPerMeV[0], LYPerMeV[1] );
      //Double_t LYPerMeVSum = meanLightYield[iMod/2];
      //update in the way to convert light yield from p.e. to energy (24th Jun, 2014)
      
      for( Int_t sl=0 ; sl<2 ; sl++ ){
// 2022/0104/ added to use Ecalib estimated with MC by coterra: see
// 2021 Dec. collaboration meeting 
// https://kds.kek.jp/event/38781/contributions/201279/attachments/149915/188005/0Lc04_collab_CVvalidCoterra.pdf
	Double_t ChEnergy = 0;
	if (m_userFlag<20190200){
		ChEnergy = gRandom->Poisson( MtimeEnergy*LYPerMeV[sl] )*(AccidentalDetECalibConst[iMod+sl]*m_1peIntADC[iMod+sl]);
	} else {
		ChEnergy = gRandom->Poisson( MtimeEnergy*LYPerMeV[sl] )*(m_EstimatedEcalibFromMC[iMod+sl]*m_1peIntADC[iMod+sl]);
	}
	Double_t ChTime = MtimeTime + CommonDelayTime;
	ChTime += ( (sl==0 ? (-1) : (1))*DeltaPropTime );
	ChTime += gRandom->Gaus( 0, MTBP::CVSigmaT*sqrt(2));//add timing resolution
	
        //// new waveform generation
        if( m_dbWfmGenerator ){
	  if( sl==0 ){
	    /// Short side
	    energyTime.energy = ChEnergy;
	    energyTime.time   = ChTime;
	    propagatedVec_S.push_back( energyTime );
	  }else{
	    /// Long side
	    energyTime.energy = ChEnergy;
	    energyTime.time   = ChTime;
	    propagatedVec_L.push_back( energyTime );
	  }
        }else{
	  //// original waveform generation
	  ChTime += AccidentalTimeShift*8 - static_cast<Double_t>(AccidentalDetTCalibConst[iMod+sl]*8);
	  WfmFunc->SetParameter( 0, 1. );
	  WfmFunc->SetParameter( 1, ChTime );
	  WfmFunc->SetParameter( 3, gRandom->Gaus( WfmAsymPar, MTBP::CVWfmAsymParSigma ) );
	
	  Double_t IntegralNormalizedFunc = GetIntegralFromFunc( WfmFunc );
	  if( IntegralNormalizedFunc<0.01 ) WfmFunc->SetParameter( 0, 0 );
	  else WfmFunc->SetParameter( 0, ChEnergy/IntegralNormalizedFunc );
	
	  AddWfmFromFunc( &DetWfm[iMod+sl][0], WfmFunc );
        }
      } // sl loop
    } // MTIME loop

    if( m_dbWfmGenerator )
      {
	const Double_t k_Pitch = 8.;
	/// mtime clustering
	ClusteringMtime( propagatedVec_S, propagatedClusteredVec_S, k_Pitch);
	ClusteringMtime( propagatedVec_L, propagatedClusteredVec_L, k_Pitch);

	/// data-base waveform generation
	GenerateDbWfmFromMTimeVec( iMod   , propagatedClusteredVec_S ); 
	GenerateDbWfmFromMTimeVec( iMod+1 , propagatedClusteredVec_L ); 
      }
  } // DIGI loop
  
  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<number; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
      // Add pedestal fluctuation
      m_temporalPedestalWidth[iCh] = gRandom->Gaus( 0, m_pedestalWidth[iCh] );
      ene[iCh] += (float)m_temporalPedestalWidth[iCh];
      // Add timing resolution : done in generating waveform
      //time[ich]  += (float)gRandom->Gaus( 0, MTBP::CVSigmaT*sqrt(2)) / 8.;
      
      EvalFTTChisq( iCh, &DetWfm[iCh][0], DetFTTChisq[iCh], DetFTTTime[iCh] );
    }
  }
  
  return;
}

bool MTConvertCV::LoadLightYield( Int_t userFlag )
//updated at 25th Feb, 2014 to get LY/MeV for each readout side (short & long)
//file format for light yield data is changed
{
  Int_t ModID = 0;
  Double_t ModPos = -1;
  Double_t LYShort = -1;
  Double_t LYLong = -1;
  Double_t LYSum = -1;
  
  for( int kndex=0; kndex<MTBP::CVNModules; kndex++ ){
    lightYieldNumber[kndex] = 0;
    meanLightYield[kndex] = 0;
    for( int index=0; index<20; index++ ){
      for( int jndex=0; jndex<3; jndex++ ){
	lightYield[kndex][jndex][index] = 0;
      }
    }
  }

  //use env. E14ANA_EXTERNAL_DATA
  std::string CVLYDataFile = std::string( std::getenv("E14ANA_EXTERNAL_DATA") );
  std::string CVDeltaVCorrectionDataFile = std::string( std::getenv("E14ANA_EXTERNAL_DATA") );
  std::string CV1peGainHistoryFile = std::string( std::getenv("E14ANA_EXTERNAL_DATA") );
  if(userFlag<20160101){
    CVLYDataFile += std::string("/gsim2dst/CV/CVLightYield.txt");//relative path since 18th May, 2014
    CVDeltaVCorrectionDataFile += std::string("/gsim2dst/CV/CVDeltaVCorrection.txt");
    CV1peGainHistoryFile += std::string("/gsim2dst/CV/CV1peHistory.txt"); //added at 24th Jun, 2014
  }else{
    CVLYDataFile += std::string("/gsim2dst/CV/CVLightYield_2016.txt");
    CVDeltaVCorrectionDataFile += std::string("/gsim2dst/CV/CVDeltaVCorrection_2016.txt");
    CV1peGainHistoryFile += std::string("/gsim2dst/CV/CV1peHistory_2016.txt");
  }
  
  if( (access( CVLYDataFile.c_str(), R_OK ) != 0)
      || (access( CVDeltaVCorrectionDataFile.c_str(), R_OK ) != 0)
      || (access( CV1peGainHistoryFile.c_str(), R_OK ) != 0) ){
    std::cerr << "CV light yield file is not found." << std::endl;
    std::exit(1);
  }
  std::ifstream ifs( CVLYDataFile.c_str() );
  std::string title;
  getline( ifs, title );

  std::ifstream ifs_cor( CVDeltaVCorrectionDataFile.c_str() );
  getline( ifs_cor, title );
  Int_t ChID = -1;
  Double_t correction = 0;
  std::map<Int_t,Double_t> GainMap;
  while( ifs_cor >> ChID >> correction ){
    GainMap[ChID] = correction;
  }
  ifs_cor.close();
  
  while( ifs >> ModID >> ModPos >> LYShort >> LYLong >> LYSum ){

    Int_t index = IDIndexMap[ModID]/2;
    if( GainMap.count( ModID )!=1 || GainMap.count( ModID+1 )!=1 ){
      std::cout << "ERROR in MTConvertCV : No gain correction data for module "
		<< ModID << std::endl;
      exit( 1 );
    }
    LYShort *= GainMap[ModID];
    LYLong *= GainMap[ModID+1];
    if( GainMap[ModID]<0 ) LYSum *= GainMap[ModID+1];//when short side is dead
    else if( GainMap[ModID+1]<0 ) LYSum *= GainMap[ModID];//when long side is dead
    else LYSum *= ((GainMap[ModID]+GainMap[ModID+1])/2.);
    
    //dead channels : change the light yield per MeV to 0
    Bool_t IsDeadMod = false;
    if( userFlag<20160101 ){
      for( Int_t iMod=0 ; iMod<MTBP::CVNDeadChannels ; iMod++ ){
	if( ModID==(MTBP::CVDeadModuleID[iMod]*2) ){
	  IsDeadMod = true;
	  if( (MTBP::CVDeadChannelID[iMod]%2)==0 ) LYShort = 0;
	  else LYLong = 0;
	}
      }
    }
    
    lightYield[index][0][lightYieldNumber[index]] = ModPos;
    lightYield[index][1][lightYieldNumber[index]] = LYShort*10;
    lightYield[index][2][lightYieldNumber[index]] = LYLong*10; // [/100keV] -> [/MeV]
    meanLightYield[index] += ( IsDeadMod ? (LYShort+LYLong) : LYSum )*10; // [/100keV] -> [/MeV]
    lightYieldNumber[index]++;
  }
  
  ifs.close();


  for( int imod=0; imod<MTBP::CVNModules; imod++ ){
    if( lightYieldNumber[imod] > 0 ){
      meanLightYield[imod] /= lightYieldNumber[imod];
    }else{
      meanLightYield[imod] = defaultLightYield;
    }
  }
  
  std::ifstream ifs_1pe( CV1peGainHistoryFile.c_str() );
  ChID = -1;
  Double_t OnePEIntADC = 0;
  while( ifs_1pe >> ChID >> OnePEIntADC ){
    m_1peIntADC[IDIndexMap[ChID]] = OnePEIntADC;
  }
  ifs_1pe.close();
  
  return true;
}

Double_t MTConvertCV::GetModHitPos( Int_t modID, Double_t x, Double_t y )
//calculated longitudinal hit position from x- or y- axis (not from the edge of the long fiber side)
//added at 25th 2014 Feb by Maeda Yosuke
{
  Double_t ModHitPos = 0;
  
  if( modID<100 ){//in front plane
    if( modID<MTBP::CVFrontnModQuadrant*2 ) ModHitPos = x;
    else if( modID<MTBP::CVFrontnModQuadrant*4 ) ModHitPos = y;
    else if( modID<MTBP::CVFrontnModQuadrant*6 ) ModHitPos = (-1)*x;
    else ModHitPos = (-1)*y;
  }
  else{
    if( modID<MTBP::CVRearnModQuadrant*2+100 ) ModHitPos = y;
    else if( modID<MTBP::CVRearnModQuadrant*4+100 ) ModHitPos = (-1)*x;
    else if( modID<MTBP::CVRearnModQuadrant*6+100 ) ModHitPos = (-1)*y;
    else  ModHitPos = x;
  }

  return ModHitPos;
} 

void MTConvertCV::GetLightYield( Int_t modID, Double_t ModHitPosFromXYAxis,
				 Double_t& LYPerMeVShort, Double_t& LYPerMeVLong ) const
//updated at 25th Feb, 2014
{
  
  Double_t PosFromEdge = ModHitPosFromXYAxis + ( modID<100 ? MTBP::CVFrontBeamHole/2.
						 : MTBP::CVRearBeamHole/2. );
  if( modID<100 && ((modID/2+1)%MTBP::CVFrontnModQuadrant)==0 ){//for FCV11
    PosFromEdge = ModHitPosFromXYAxis + MTBP::CVFrontMod11EdgeLength;
  }
  LYPerMeVShort = defaultLightYield;
  LYPerMeVLong = defaultLightYield;
  
  Int_t iMod = -1;
  std::map<Int_t,unsigned int>::const_iterator it = IDIndexMap.find(modID);
  if( (it==IDIndexMap.end()) ){
    std::cout << "ERROR : invalid ModID : " << modID << std::endl; 
    LYPerMeVShort = -1;
    LYPerMeVLong = -1;
    return;
  }
  else iMod = it->second/2;
  for( Int_t iPos=0; iPos<lightYieldNumber[iMod] ; iPos++ ){
    if( TMath::Abs( lightYield[iMod][0][iPos] - PosFromEdge ) <= 25.
	|| ((iPos==0)&&(PosFromEdge<0))
	|| ((iPos==(lightYieldNumber[iMod]-1))&&(PosFromEdge>1000)) ){//modified at 23rd Jun, 2014  
      LYPerMeVShort = lightYield[iMod][1][iPos];
      LYPerMeVLong = lightYield[iMod][2][iPos];
      break;
    }
  }
  
  if( lightYieldNumber[iMod]>0 ){
    if( PosFromEdge<lightYield[iMod][0][0] ){
      LYPerMeVShort = lightYield[iMod][1][0];
      LYPerMeVLong = lightYield[iMod][2][0];
    } 
    else if( PosFromEdge>lightYield[iMod][0][lightYieldNumber[iMod]-1] ){
      LYPerMeVShort = lightYield[iMod][1][lightYieldNumber[iMod]-1];
      LYPerMeVLong = lightYield[iMod][2][lightYieldNumber[iMod]-1];
    } 
  }
  
  return;
}


Double_t MTConvertCV::GetLightPropTime( Int_t modID, Double_t ModHitPosFromXYAxis, Double_t& CommonDelayTime )
//added at 22th Feb, 2014 by Maeda Yosuke 
//propagation is added so that hits on x- or y-axis have the same timing in the both sides
//return value : (long-short)/2 in [ns]
//effect of fiber lift region is cosidered
{
  static Double_t FiberLiftPosFront = (-1)*MTBP::CVFrontBeamHole/2.+MTBP::CVFiberLiftLongSide; 
  static Double_t FiberLiftPosRear = (-1)*MTBP::CVRearBeamHole/2.+MTBP::CVFiberLiftLongSide; 
  Double_t DeltaPropLength = ModHitPosFromXYAxis;//will be 0 when x=0 or y=0
  Double_t ScintiPropLength = 0;
  
  if( modID<100 ){//front plane
    Int_t ModNoInQuadrant = ((modID/2)%MTBP::CVFrontnModQuadrant);
    if( DeltaPropLength<FiberLiftPosFront ){
      ScintiPropLength = FiberLiftPosFront - DeltaPropLength;
      DeltaPropLength = FiberLiftPosFront;
    }
    else if( DeltaPropLength>MTBP::CVFrontFiberLiftShortSide[ModNoInQuadrant] ){
      ScintiPropLength = DeltaPropLength - MTBP::CVFrontFiberLiftShortSide[ModNoInQuadrant];
      DeltaPropLength = MTBP::CVFrontFiberLiftShortSide[ModNoInQuadrant]; 
    }
  }
  else{//front plane
    if( DeltaPropLength<FiberLiftPosRear ){
      ScintiPropLength = FiberLiftPosRear - DeltaPropLength;
      DeltaPropLength = FiberLiftPosRear;
    }
  }
  
  CommonDelayTime = ScintiPropLength/MTBP::CVLightPropSpeed;
  
  return DeltaPropLength/MTBP::CVLightPropSpeed;
}

void MTConvertCV::LoadReCalibrationFactor( Int_t userFlag )
{

  //use env. E14ANA_EXTERNAL_DATA
  std::string filename = std::string( std::getenv("E14ANA_EXTERNAL_DATA") );
  if(userFlag<20160101){
    filename += std::string("/gsim2dst/CV/CVReCalibrationFactor.txt");
  }else{
    filename += std::string("/gsim2dst/CV/CVReCalibrationFactor_2016.txt");
  }
  if( access( filename.c_str(), R_OK) != 0 ){
    std::cerr << filename << " is not found." << std::endl;
    std::exit( 1 );
  }

  std::ifstream ifs( filename.c_str() );

  int id;

  for( int imod=0; imod<MTBP::CVNModules; imod++ ){
    ifs >> id >> m_reCalibrationFactor[imod];
  }
  ifs.close();

}



void MTConvertCV::AddAccidental()
{
  AddAccidental( DetNumber, DetModID, DetEne, DetTime );
}

void MTConvertCV::AddAccidental( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{

  if( !EnableAccidentalOverlay ) return;
  //  AccidentalTree->GetEntry( AccidentalEntryID );

  for( int ich=0; ich<DetectorNCH; ich++ ){

    modID[ich] = IndexIDArray[ich];
    // If simulation does not have any hit or accidental energy is larger than simulation energy
    if( ene[ich]==0 || ene[ich]<AccidentalDetEne[ich]*m_reCalibrationFactor[ich/2] ){
      time[ich] = AccidentalDetTime[ich] + AccidentalTimeShift;
    }

    ene[ich] -= m_temporalPedestalWidth[ich];
    ene[ich] += AccidentalDetEne[ich] * (float)m_reCalibrationFactor[ich/2];

}

}

void MTConvertCV::LoadPedestalWidth( Int_t userFlag )
{
  //use env. E14ANA_EXTERNAL_DATA
  std::string filename = std::string( std::getenv("E14ANA_EXTERNAL_DATA") );
  if(userFlag<20160101){
    filename += std::string("/gsim2dst/CV/CVPedestal.txt");
  }else{
    filename += std::string("/gsim2dst/CV/CVPedestal_2016.txt");
  }
  if( access( filename.c_str(), R_OK) != 0 ){
    std::cerr << filename << " is not found." << std::endl;
    std::exit( 1 );
  }
  
  std::ifstream ifs( filename.c_str() );

  int id;

  for( int imod=0; imod<MTBP::CVNChannels; imod++ ){
    ifs >> id >> m_pedestalWidth[imod];
    //std::cout << id << " " << m_pedestalWidth[imod] << std::endl;
  }
  ifs.close();
}

void  MTConvertCV::LoadEstimatedEcalibFromMC( Int_t userFlag )
{
	std::string filename = std::string( std::getenv("E14ANA_EXTERNAL_DATA") );
	if (userFlag<20190200){ // temporarily same
			filename += std::string("/gsim2dst/CV/expectedHistoricalADCmipLG7.dat");
  }else{
			filename += std::string("/gsim2dst/CV/expectedHistoricalADCmipLG7.dat");
  }

	if( access( filename.c_str(), R_OK) != 0 ){
    std::cerr << filename << " is not found." << std::endl;
    std::exit( 1 );
	}
  
  std::ifstream ifs( filename.c_str() );

  int id;

	double EstimatedEcalibFromMCsigma[MTBP::CVNChannels];
	// added to use Ecalib estimated with MC by coterra: see
	// 2021 Dec. collaboration meeting 
	// https://kds.kek.jp/event/38781/contributions/201279/attachments/149915/188005/0Lc04_collab_CVvalidCoterra.pdf
	for( int imod=0; imod<MTBP::CVNChannels; imod++ ){
    ifs >> id >> m_EstimatedEcalibFromMC[imod] >> EstimatedEcalibFromMCsigma[imod] ;
  }
  ifs.close();

}
