#include "gsim2dst/MTConvertGenParticle.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <cmath>

const Double_t g_DefaultMomentum = 2000;

MTConvertGenParticle::MTConvertGenParticle( Int_t userFlag ) : m_userFlag( userFlag )
{
  m_genParticleData = new GsimGenParticleData();
  TrueGammaPosOnCSISurface = new Double_t [32][3]();

  GenParticleTrack     = new Int_t [MaxGenParticleN]();
  GenParticleMother    = new Int_t [MaxGenParticleN]();
  GenParticlePid       = new Int_t [MaxGenParticleN]();
  //GenParticleMotherPid = new Int_t [MaxGenParticleN]();
  GenParticleMom       = new Double_t [MaxGenParticleN][3]();
  GenParticleEk        = new Double_t [MaxGenParticleN]();
  GenParticleMass      = new Double_t [MaxGenParticleN]();
  GenParticleTime      = new Double_t [MaxGenParticleN]();
  GenParticlePos       = new Double_t [MaxGenParticleN][3]();
  GenParticleEndMom    = new Double_t [MaxGenParticleN][3]();
  GenParticleEndEk     = new Double_t [MaxGenParticleN]();
  GenParticleEndTime   = new Double_t [MaxGenParticleN]();
  GenParticleEndPos    = new Double_t [MaxGenParticleN][3]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  CSIZPosition = GetDetZPosition("CSI"); // from E14BP code

  Initialize();
}


MTConvertGenParticle::~MTConvertGenParticle()
{
  delete m_genParticleData;
  delete[] TrueGammaPosOnCSISurface;

  delete[] GenParticleTrack;
  delete[] GenParticleMother;
  delete[] GenParticlePid;
  //delete[] GenParticleMotherPid;
  delete[] GenParticleMom;
  delete[] GenParticleEk;
  delete[] GenParticleMass;
  delete[] GenParticleTime;
  delete[] GenParticlePos;
  delete[] GenParticleEndMom;
  delete[] GenParticleEndEk;
  delete[] GenParticleEndTime;
  delete[] GenParticleEndPos;

  delete   m_E14BP;
}



void MTConvertGenParticle::Initialize( void )
{
  TruePID = 0;
  TrueDecayMode = 999;
  TrueVertexZ = 0;
  TrueVertexTime = 0;
  for( int index=0; index<3; index++ ){
    TrueMom[index] = 0;
    TruePos[index] = 0;
  }
  TrueNGammas = 0;
  TrueNGammasOnCSI = 0;
  for( int index=0; index<32; index++ ){
    for( int jndex=0; jndex<3; jndex++ ){
      TrueGammaPosOnCSISurface[index][jndex] = 0;
    }
  }
  TrueHitsOnCSI = 0;

  GenParticleNumber = 0;
  for( int index=0; index<MaxGenParticleN; index++ ){
    GenParticleTrack[index] = -1;
    GenParticleMother[index] = -1;
    GenParticlePid[index] = -1;
    //GenParticleMotherPid[index] = -1;
    GenParticleEk[index] = -1;
    GenParticleMass[index] = -1;
    GenParticleTime[index] = -1;
    GenParticleEndEk[index] = -1;
    GenParticleEndTime[index] = -1;
    for( int xyz=0; xyz<3; xyz++ ){
      GenParticleMom[index][xyz] = -1;
      GenParticlePos[index][xyz] = -1;
      GenParticleEndMom[index][xyz] = -1;
      GenParticleEndPos[index][xyz] = -1;
    }
  }

}




void MTConvertGenParticle::SetBranchAddress( TTree* tr )
{
  m_eventTree = tr;
  m_eventTree->SetBranchAddress( "GenParticle.", &m_genParticleData );
}
  

void MTConvertGenParticle::Branch( TTree* tr )
{
  m_dstTree = tr;
  
  m_dstTree->Branch( "TruePID", &TruePID, "TruePID/I" );
  m_dstTree->Branch( "TrueDecayMode", &TrueDecayMode, "TrueDecayMode/I" );
  m_dstTree->Branch( "TrueVertexZ", &TrueVertexZ, "TrueVertexZ/D" );
  m_dstTree->Branch( "TrueVertexTime", &TrueVertexTime, "TrueVertexTime/D" );
  m_dstTree->Branch( "TrueMom", TrueMom, "TrueMom[3]/D" );
  m_dstTree->Branch( "TruePos", TruePos, "TruePos[3]/D" );

  m_dstTree->Branch( "TrueNGammas", &TrueNGammas, "TrueNGammas/I" );
  m_dstTree->Branch( "TrueNGammasOnCSI", &TrueNGammasOnCSI, "TrueNGammasOnCSI/I" );
  m_dstTree->Branch( "TrueGammaPosOnCSISurface", TrueGammaPosOnCSISurface, "TrueGammaPosOnCSISurface[TrueNGammas][3]/D" );
  m_dstTree->Branch( "TrueHitsOnCSI", &TrueHitsOnCSI, "TrueHitsOnCSI/I" );

  m_dstTree->Branch( "GenParticleNumber", &GenParticleNumber, "GenParticleNumber/I" );
  m_dstTree->Branch( "GenParticleTrack", GenParticleTrack, "GenParticleTrack[GenParticleNumber]/I" );
  m_dstTree->Branch( "GenParticleMother", GenParticleMother, "GenParticleMother[GenParticleNumber]/I" );
  m_dstTree->Branch( "GenParticlePid", GenParticlePid, "GenParticlePid[GenParticleNumber]/I" );
  //m_dstTree->Branch( "GenParticleMotherPid", GenParticleMotherPid, "GenParticleMotherPid[GenParticleNumber]/I" );
  m_dstTree->Branch( "GenParticleMom", GenParticleMom, "GenParticleMom[GenParticleNumber][3]/D" );
  m_dstTree->Branch( "GenParticleEk", GenParticleEk, "GenParticleEk[GenParticleNumber]/D" );
  m_dstTree->Branch( "GenParticleMass", GenParticleMass, "GenParticleMass[GenParticleNumber]/D" );
  m_dstTree->Branch( "GenParticleTime", GenParticleTime, "GenParticleTime[GenParticleNumber]/D" );
  m_dstTree->Branch( "GenParticlePos", GenParticlePos, "GenParticlePos[GenParticleNumber][3]/D" );
  m_dstTree->Branch( "GenParticleEndMom", GenParticleEndMom, "GenParticleEndMom[GenParticleNumber][3]/D" );
  m_dstTree->Branch( "GenParticleEndEk", GenParticleEndEk, "GenParticleEndEk[GenParticleNumber]/D" );
  m_dstTree->Branch( "GenParticleEndTime", GenParticleEndTime, "GenParticleEndTime[GenParticleNumber]/D" );
  m_dstTree->Branch( "GenParticleEndPos", GenParticleEndPos, "GenParticleEndPos[GenParticleNumber][3]/D" );

}


Int_t MTConvertGenParticle::Convert( void )
{
  
  Initialize();

  static const double s_CSIZPosition = CSIZPosition;

  TClonesArray* briefTracks = m_genParticleData->briefTracks;


  for( int index=0; index<briefTracks->GetEntries(); index++ ){
    GsimTrackData* track = (GsimTrackData*)briefTracks->At( index );
    // GenParticle store
    if( index < MaxGenParticleN ){
      GenParticleTrack[index]  = track->track;
      GenParticleMother[index] = track->mother;
      GenParticlePid[index]    = track->pid;
      GenParticleMom[index][0] = track->p.x();
      GenParticleMom[index][1] = track->p.y();
      GenParticleMom[index][2] = track->p.z();
      GenParticleEk[index]     = track->ek;
      GenParticleMass[index]   = track->mass;
      GenParticleTime[index]   = track->time;
      GenParticlePos[index][0] = track->v.x();
      GenParticlePos[index][1] = track->v.y();
      GenParticlePos[index][2] = track->v.z();
      GenParticleEndMom[index][0] = track->end_p.x();
      GenParticleEndMom[index][1] = track->end_p.y();
      GenParticleEndMom[index][2] = track->end_p.z();
      GenParticleEndEk[index]  = track->end_ek;
      GenParticleEndTime[index]  = track->end_time;
      GenParticleEndPos[index][0] = track->end_v.x();
      GenParticleEndPos[index][1] = track->end_v.y();
      GenParticleEndPos[index][2] = track->end_v.z();
      GenParticleNumber++;
    }


    //mother PID check
    if( track->mother == -1 ){ // mother particle
      TruePID = track->pid;
      TrueVertexZ = track->end_v.z();
      TrueVertexTime = track->end_time;
      TrueMom[0] = track->end_p.x();
      TrueMom[1] = track->end_p.y();
      TrueMom[2] = track->end_p.z();
      TruePos[0] = track->end_v.x();
      TruePos[1] = track->end_v.y();
      TruePos[2] = track->end_v.z();
      //break;
    }
  }

  // decay mode check
  if( TruePID != KLPID ){
    TrueDecayMode = -1;
    return TrueDecayMode;
  }
  
  std::vector< int > secondaryPIDVec;
  int dalitzCount = 0;
  double x=0, y=0, z=0;
  for( int index=0; index<briefTracks->GetEntries(); index++ ){
    GsimTrackData* track = (GsimTrackData*)briefTracks->At( index );
    // Check this track penetrate CsI surface
    if( GetCSIHitPosition( track->v, track->p, x, y, z ) ){
      if( track->end_v.z() > s_CSIZPosition ){
	TrueHitsOnCSI++;
      }
    }

    if( track->pid == GAMMAPID ){
      if( TrueNGammas < 32 ){
	if( GetCSIHitPosition( track->v, track->p, TrueGammaPosOnCSISurface[TrueNGammas][0], TrueGammaPosOnCSISurface[TrueNGammas][1], TrueGammaPosOnCSISurface[TrueNGammas][2] ) ){
	  if( track->end_v.z() > s_CSIZPosition ){
	    TrueNGammasOnCSI++;
	  }
	}
	TrueNGammas++;

	if( TrueNGammas == 32 ){
	  std::cout << "MTConvertGenParticle : TrueNGammas is 32." << std::endl;
	}
      }
    }
    
    if( track->mother == 1 ){
      secondaryPIDVec.push_back( track->pid );
      // if secondary particle is pi0, check pi0 decays to 2gamma or e+e-gamma
      if( track->pid == PI0PID ){
	std::vector< int > thirdPIDVec;
	for( int index=0; index<briefTracks->GetEntries(); index++ ){
	  GsimTrackData* track3 = (GsimTrackData*)briefTracks->At( index );
	  if( track3->mother == (Int_t)track->track ){
	    thirdPIDVec.push_back( track3->pid );
	  }
	}
	if( CheckDalitz( thirdPIDVec ) ){
	  dalitzCount++;
	}
      }
    }
    if( secondaryPIDVec.size() > 3 ){
      TrueDecayMode = 0;
      return TrueDecayMode;
    }
  }

  TrueDecayMode = GetDecayMode( secondaryPIDVec );
  TrueDecayMode += dalitzCount*10;
  return TrueDecayMode;
}

Float_t MTConvertGenParticle::GetInitialDeltaTOF( Float_t ParticleMass, Float_t ParticleMom, Bool_t ForceToUseSpecifiedMom ){
  //last updated at 24th Jun, 2014
  //added at 27th May, 2014 (MY)
  //DeltaTOF including correction for MC event timing due to variation of MC start z position
  //basically, given particle mass and momentum obtained from GenParticle is used to calculate particle beta
  //to use particle mass given by GenParticle in single particle incidence, set negative mass
  //  (in case of multiparticle incidence but with negative mass, KL mass is used) 
  //Only when incident particle momentum cann't be computed, value specified by ParticleMom is used
  //  (in case of multiparticle incidence )
  
  Float_t MomSquared = 0;
  Float_t MassSquared = ( ParticleMass>0 ? ParticleMass*ParticleMass : -1 );
  Float_t InitParticleStartZ = 0;
  Float_t InitParticleStartTime = 0;
  Float_t BetaInv = 1;
  
  std::vector<Int_t> InitialParticleIndexVec;
  Float_t InitialParticleTotalMom[3] = { 0, 0, 0 };
  Float_t InitialParticleTotalE = 0;
  for( Int_t iGen=0 ; iGen<GenParticleNumber ; iGen++ ){
    if( GenParticleMother[iGen]<0 ){
      InitialParticleIndexVec.push_back( iGen );
      InitialParticleTotalE += (GenParticleEk[iGen]+GenParticleMass[iGen]); 
      for( Int_t xyz=0 ; xyz<3 ; xyz++ )
	InitialParticleTotalMom[xyz] += GenParticleMom[iGen][xyz];
    }
  }
  
  MomSquared = ( TMath::Power( InitialParticleTotalMom[0], 2. )
		 + TMath::Power( InitialParticleTotalMom[1], 2. )
		 + TMath::Power( InitialParticleTotalMom[2], 2. ) );
  if( InitialParticleIndexVec.empty() || TMath::Abs(MomSquared)<1e-3 ){
    std::cout << "WARNING : MTConvertGenParticle::GetInitialDeltaTOF()"
	      << " :: no incident particles or 0 incident particle momentum."
	      << std::endl;
    return MTBP::Invalid;
  }
  
  Int_t index = InitialParticleIndexVec[0];
  InitParticleStartZ = GenParticlePos[index][2];
  InitParticleStartTime = GenParticleTime[index];
  
  //nominal MCTiming (defined in MTConvertBHXX.cc) : gamma timing
  Float_t MCTimingCorrection = InitParticleStartZ/(TMath::C()*1e-6)-InitParticleStartTime;
  
  //when there is only one initial particle and particle mass is not specified
  if( InitialParticleIndexVec.size()==1 && MassSquared<0 )
    MassSquared = TMath::Power( GenParticleMass[index], 2. );
  else{//check the consistentcy of initial particle infomation with given particle mass
    if( TMath::Abs( TMath::Sqrt( InitialParticleTotalE*InitialParticleTotalE - MomSquared ) - ParticleMass )>0.1 )
      MomSquared = -1;
  }
  
  if( ForceToUseSpecifiedMom || MomSquared<0 ) MomSquared = TMath::Power( ParticleMom, 2. );
  if( MassSquared<0 ) MassSquared = TMath::Power( MTBP::KL_MASS, 2. );
  BetaInv = TMath::Sqrt(1+MassSquared/MomSquared);
  
  //BeamLineLength : distance between target and z=0 (=21507mm for May2013) 
  Float_t MCInitialDeltaTOF = (MTBP::BeamLineLength+InitParticleStartZ)/(TMath::C()*1e-6)*( BetaInv - 1 );
  
  return (MCInitialDeltaTOF + MCTimingCorrection);
}

Int_t MTConvertGenParticle::GetDecayMode( std::vector< int > pidVec )
{
  std::sort( pidVec.begin(), pidVec.end() );

  if( pidVec.size() == 3 ){
    if( pidVec[0]==-211 && pidVec[1]==-11 && pidVec[2]==12 ) return KE3; // pi-e+nu
    if( pidVec[0]==-12 && pidVec[1]==11 && pidVec[2]==211 )  return KE3; // pi+e-nu

    if( pidVec[0]==-211 && pidVec[1]==-13 && pidVec[2]==14 ) return KMU3; // pi-mu+nu
    if( pidVec[0]==-14 && pidVec[1]==13 && pidVec[2]==211 )  return KMU3; // pi+mu-nu

    if( pidVec[0]==PI0PID && pidVec[1]==PI0PID && pidVec[2]==PI0PID )  return K3PI0;  // 3pi0
    if( pidVec[0]==-211 && pidVec[1]==111 && pidVec[2]==211 ) return KCHARGED3PI; // pi+pi-pi0
  }

  if( pidVec.size() == 2 ){
    if( pidVec[0]==PI0PID && pidVec[1]==PI0PID )     return K2PI0;  // 2pi0
    if( pidVec[0]==GAMMAPID && pidVec[1]==GAMMAPID ) return K2GAMMA;  // 2gamma
  }

  return 0;
}


    
bool MTConvertGenParticle::CheckDalitz( std::vector< int > pidVec )
{

  if( pidVec.size() != 3 ) return false;

  std::sort( pidVec.begin(), pidVec.end() );
  if( pidVec[0]==-11 && pidVec[1]==11 && pidVec[2]==GAMMAPID ) return true;
  return false;
}


bool MTConvertGenParticle::IsOnCSISurface( double* pos )
{
  static const double s_CSIZPosition = CSIZPosition;

  if( pos[2] != s_CSIZPosition ) return false;
  if( fabs(pos[0])<100 && fabs(pos[1])<100 ) return false;
  if( pow( pos[0], 2) + pow( pos[1], 2) > pow( 900, 2) ) return false;
  return true;
}


bool MTConvertGenParticle::GetCSIHitPosition( TVector3 v, TVector3 p, Double_t &x, Double_t &y, Double_t &z )
{
  static const double s_CSIZPosition = CSIZPosition;

  if( v.z() > s_CSIZPosition ){
    x = v.x();
    y = v.y();
    z = v.z();
    return false;
  }

  x = (s_CSIZPosition - v.z()) * p.x() / p.z() + v.x();
  y = (s_CSIZPosition - v.z()) * p.y() / p.z() + v.y();
  z = s_CSIZPosition;

  double pos[3] = { x, y, z};
  return IsOnCSISurface( pos );
}

double MTConvertGenParticle::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}
