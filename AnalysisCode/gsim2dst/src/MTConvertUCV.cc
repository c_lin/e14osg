#include "gsim2dst/MTConvertUCV.h"

bool sort_et(const pEnergyTime& l, const pEnergyTime&r){
	return l.time<r.time;
}	

Double_t ucvfunc(Double_t *x, Double_t* par){
	double t = x[0]-par[1];
	if(t<0) return 0;

	double y = par[0]*t*exp(-(t-par[2])/par[3]);
	return y;	
}



void MTConvertUCV::Branch( TTree* tree )
{
	MTConvert500MHzDetector::Branch(tree);
	DstTree->Branch( Form("%sFallTime",   DetectorName), DetFallTime,  Form("%sFallTime[%sNumber][%d]/F",DetectorName,DetectorName, MTBP::NMaxHitPerEvent ) );
	DstTree->Branch( Form("%sPeakTime",   DetectorName), DetPeakTime,  Form("%sPeakTime[%sNumber][%d]/F",DetectorName,DetectorName, MTBP::NMaxHitPerEvent ) );
}



MTConvertUCV::MTConvertUCV( Int_t userFlag ) : MTConvert500MHzDetector( MTBP::UCV, userFlag )
{
  for( int index=0; index<MTBP::DetNChannels[MTBP::UCV]; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index) );
    IndexIDArray[index] = index;
  }
  
  DetNumber        = DetectorNCH;
  DetUpdatedNumber = DetectorNCH;
  DetTrueNumber    = DetectorNCH;

  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich]        = IndexIDArray[ich];
    DetUpdatedModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich]    = IndexIDArray[ich];
  }
 
  DetFallTime = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];
  DetPeakTime = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];
  
  ran = new TRandom3(1);
  func = new TF1("UCVFunc",	ucvfunc, -1000, 1000, 4);

  Initialize();

}

MTConvertUCV::~MTConvertUCV(){
	
	delete[] DetFallTime;
	delete[] DetPeakTime;
	delete ran;
	delete func;
}

void MTConvertUCV::Initialize( void )
{
  int maxhit=MTBP::NMaxHitPerEvent;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetNHit[ich]        = 0;
    DetUpdatedNHit[ich] = 0;
    for(int ihit=0; ihit<maxhit; ihit++){
      DetEne[ich][ihit]         = MTBP::Invalid;
      DetTime[ich][ihit]        = MTBP::Invalid;
      DetFallTime[ich][ihit]    = MTBP::Invalid;
      DetPeakTime[ich][ihit]    = MTBP::Invalid;
      DetUpdatedEne[ich][ihit]  = MTBP::Invalid;
      DetUpdatedTime[ich][ihit] = MTBP::Invalid;

      DetTrueEneTmp[ich][ihit] = 0;//added at 16th Mar, 2016
      DetTrueTimeTmp[ich][ihit] = 0;//added at 16th Mar, 2016
    }
    for( Int_t iSample=0 ; iSample<MTBP::nSample500 ; iSample++ )
      DetWfm[ich][iSample] = 0;
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
    
    DetTrueNHit[ich] = 0;



  }
}

void MTConvertUCV::ConvertFromDigiWithResponse( void ){//added at 25th Nov, 2013
  
  GetFromDigiWithResponse( DetNumber, DetModID, DetNHit, DetEne, DetTime );
}

void MTConvertUCV::GetTrueInfo( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] )
{
  TClonesArray *digiArray = GetDigiArray();
  
  number = DetectorNCH;
  
  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    Int_t index = IDIndexMap[digi->modID];
    Int_t iHit = nhit[index];
    ene[index][iHit] = (float)digi->energy;
    time[index][iHit] = (float)digi->time /MTBP::SampleInterval500; //[ns]->[clock]
    nhit[index]++;
  } 
}

void MTConvertUCV::GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] ){
  TClonesArray *digiArray = GetDigiArray();
  
  number = DetectorNCH;
  for( Int_t iCh=0 ; iCh<number ; iCh++ ) modID[iCh] = IndexIDArray[iCh];
  
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t idigi=0 ; idigi<nDigi ; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    Int_t ModID = digi->modID;
    Int_t index = IDIndexMap[ModID];
    Float_t TimeSmearing = 0;//[clock]

    ene[index][0] = digi->energy;
    time[index][0] = digi->time/MTBP::SampleInterval500 + TimeSmearing;//[ns]->[clock]
    time[index][0] += AccidentalTimeShift;
    DetFallTime[index][0] = time[index][0];
    DetPeakTime[index][0] = time[index][0];
    nhit[index]++;
  }
  
  return;
}

void MTConvertUCV::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] ){

  number = DetNumber;
  int maxhit=MTBP::NMaxHitPerEvent;
  for( int ich=0; ich<DetNumber; ich++ ){
    modID[ich] = IndexIDArray[ich];
    for( int ihit=0; ihit<maxhit; ihit++ ){
      ene[ich][ihit] = 0;
      time[ich][ihit] = 0;
      DetFallTime[ich][ihit] = 0;
      DetPeakTime[ich][ihit] = 0;
    }
  }

  TClonesArray *digiArray = GetDigiArray();
  TClonesArray *mtimeArray = GetMTimeArray();

  // digi loop
  Int_t nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );

    Int_t ModID = digi->modID;
    Int_t index = IDIndexMap[ModID];
    Int_t iHit  = nhit[ModID];

    double Energy = 0;
    double Time = 0;

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // Ignore over 1us hit
      if( mtime->time > 1000 ) continue;

      double TmpEnergy = GetVisibleEnergy( digi->modID,mtime->r.y(), mtime->energy );
      
      Energy += TmpEnergy;
      Time   += GetVisibleTime( digi->modID,mtime->r.z(), mtime->time, mtime->energy ) * TmpEnergy;


    } // imtime

    ene[index][iHit]  = Energy;
    time[index][iHit] = Time /Energy; // [clock]    
    time[index][iHit] += AccidentalTimeShift;
	DetFallTime[index][iHit] = time[index][iHit];
	DetPeakTime[index][iHit] = time[index][iHit];
    nhit[index]++;
    


  } // idigi
  
  return;
}

void MTConvertUCV::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Short_t* nhit,
						Float_t (*ene)[MTBP::NMaxHitPerEvent], 
						Float_t (*time)[MTBP::NMaxHitPerEvent],
						Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit,
						Float_t (*updatedene)[MTBP::NMaxHitPerEvent], 
						Float_t (*updatedtime)[MTBP::NMaxHitPerEvent] )
{  
  
  t00 = AccidentalDetTCalibConst[0];
  number        = DetNumber;
  updatednumber = DetNumber;
  int maxhit=MTBP::NMaxHitPerEvent;
  for( int ich=0; ich<DetNumber; ich++ ){
    modID[ich]        = IndexIDArray[ich];
    updatedmodID[ich] = IndexIDArray[ich];

	// calibration offset is canceled here
	AccidentalDetTCalibConst[ich] -= t00; 
    for( int ihit=0; ihit<maxhit; ihit++ ){
      ene[ich][ihit]         = 0;
      time[ich][ihit]        = MTBP::Invalid;
      DetFallTime[ich][ihit] = MTBP::Invalid;
      DetPeakTime[ich][ihit] = MTBP::Invalid;
      updatedene[ich][ihit]  = 0;
      updatedtime[ich][ihit] = 0;
    }
  }
	

  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedFrontVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    propagatedFrontVec.clear();

	// mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );


      if(m_userFlag<20210201){      
	energyTime.energy = GetVisibleEnergy( digi->modID, mtime->r.y(), mtime->energy );
	energyTime.time   = GetVisibleTime( digi->modID, mtime->r.y(), mtime->time , mtime->energy);
      } else {
	energyTime.energy = GetVisibleEnergy( digi->modID, mtime->r.x(), mtime->energy );
	energyTime.time   = GetVisibleTime( digi->modID, mtime->r.x(), mtime->time , mtime->energy);
      }
      propagatedFrontVec.push_back( energyTime );
    
    }
//#define MPPCNOISE
#ifdef MPPCNOISE

	const double Noise_freq = 100e6;
	const double Ave_hit = 700.0*1e-9*Noise_freq;
	int Naddnoise = ran->Poisson(Ave_hit);
	
	std::vector<double> v_noise;
	for(int i=0;i<Naddnoise;i++){
		double t = -200+700*ran->Rndm();
		v_noise.push_back(t);
	}
	std::sort(v_noise.begin(), v_noise.end());

	for(int i=0;i<v_noise.size();i++){
		double e1pe = 0.19/30; // MeV
		energyTime.energy = e1pe;
		energyTime.time = v_noise[i];
		propagatedFrontVec.push_back( energyTime );
	}

	std::sort(propagatedFrontVec.begin(), propagatedFrontVec.end(), sort_et);
#endif

	

	static int init = 0;
	if(init==0){
	  if(m_userFlag<20210201){
	    func->SetParameter( 0, 1 );
	    func->SetParameter( 1, 0 );
	    func->SetParameter( 2, 9.47737 );
	    func->SetParameter( 3, 6.512 );
	    
	    double peak = -1e5;
	    int Ncal = 10000;
	    for(int i=0;i<Ncal;i++){
	      double t = -50 + 400.0*i/Ncal;
	      double p = func->Eval(t/2);
	      if(p>peak) peak = p;
	    }
	    
	    norm_wfm = 1.0/peak;
	    init++;
	  } else{
	    WfmFunc->SetParameter( 0, 1 );
	    WfmFunc->SetParameter( 1, 0 );
	    WfmFunc->SetParameter( 2, 5.681);
	    WfmFunc->SetParameter( 3, 0.2748);

	    double peak = -1e5;
	    int Ncal = 10000;
	    for(int i=0;i<Ncal;i++){
	      double t = -50 + 400.0*i/Ncal;
	      double p = WfmFunc->Eval(t/2);
	      if(p>peak) peak = p;
	    }	    
	    norm_wfm = 1.0/peak;
	    init++;	    
	  }
	}
	if(m_userFlag<20210201) func->SetParameter(0, norm_wfm);
	else WfmFunc->SetParameter(0,norm_wfm);
	  	
	propagatedClusteredVec.clear();
	ClusteringMtime( propagatedFrontVec, propagatedClusteredVec );
  
	if(m_userFlag<20210201) GenerateWfmFromMTimeVec( digi->modID, propagatedClusteredVec, 0, func);
	else GenerateWfmFromMTimeVec( digi->modID, propagatedClusteredVec, 82.4/2. );
  
	


  }// idigi


	  
  if( !EnableAccidentalOverlay ){
    for( Int_t iCh=0 ; iCh<number ; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], nhit[iCh], 
			    &ene[iCh][0], &time[iCh][0], iCh );
      GetEnergyTimeFromAveragedWfm( &DetWfm[iCh][0], updatednhit[iCh], 
				    &updatedene[iCh][0], &updatedtime[iCh][0], iCh, &DetPTime[iCh][0]);
      
    }
  }    

  return;
}


void MTConvertUCV::GetEnergyTimeFromWfm( Float_t* wfm, Short_t& nhit, 
	Float_t* ene, Float_t* time,
	Int_t ChIndex, Bool_t ApplyT0Calib )
{



  
	
	Short_t nHit=0;
	Int_t SampleStart = 6;
	Int_t SampleEnd = 250;
	Double_t threshold = 0.007;
	Int_t nSampleIntegral = 40;
	Int_t nSampleBeforePeak = 20;
	Int_t nSampleAfterPeak = 20;
	



	double pedestal = 0;

	
	static const Int_t FADCMaxCount = 4095;

	Short_t nSaturated = 0;

	//// calculation of averaged waveform ////
	const int k_nSamples = 256;
	Float_t AveWfm[k_nSamples];
	for(int iSample=0;iSample<k_nSamples;iSample++){
		AveWfm[iSample]=0;
		if(iSample<5||250<iSample){
			AveWfm[iSample] = (Float_t)wfm[iSample];
		}else{
			for(int jSample=iSample-2;jSample<=iSample+2;jSample++){
				AveWfm[iSample] += (Float_t)wfm[jSample]/5.;
			}
		}
	}

	for( Int_t iSample=SampleStart ; iSample<SampleEnd ; iSample++ ){
		Float_t PrePreDiff = AveWfm[iSample-1]-AveWfm[iSample-2];
		Float_t PreDiff    = AveWfm[iSample]  -AveWfm[iSample-1];
		Float_t Diff       = AveWfm[iSample+1]-AveWfm[iSample];
		Float_t PostDiff   = AveWfm[iSample+2]-AveWfm[iSample+1];
		if( wfm[iSample]==FADCMaxCount ) nSaturated++;

		if( (AveWfm[iSample]>(pedestal+threshold*0.5)) // half of the peak finding threshold
			&& ( ((Diff<=0)&&(PostDiff<0)) && (((PreDiff>0)&&(PrePreDiff>0))||(nSaturated>1)) ) ){

			Short_t tmpPeakTime = 0;
			Float_t tmpPeakHeight = 0;
			//// find maximum peak ////
			for(int jSample=iSample-6;jSample<iSample+4;jSample++){
				if( tmpPeakHeight < wfm[jSample]-pedestal ){
					tmpPeakHeight = wfm[jSample]-pedestal;
					tmpPeakTime   = ((nSaturated>1) ? (jSample+1-nSaturated+(nSaturated-1)/2) : jSample);
				}
			}

			Bool_t  IsPileUp = false;
			Short_t PeakTime = tmpPeakTime;
			Float_t PeakHeight = wfm[PeakTime]-pedestal;
			Float_t CFTimeThre = PeakHeight/2.+pedestal;
			Float_t FallTimeThre = 2.0*PeakHeight/3.+pedestal;
			//Float_t FallTimeThre = PeakHeight/2.+pedestal;
			Float_t CFTime    = -10;
			Float_t FallTime    = -10;
			Bool_t  FindCFThre = false;
			Bool_t  FindFallTimeThre = false;

			if( PeakHeight<threshold ) continue; //skip if original peakheight bellow the threshold

			/*
			for( Int_t iClock=0 ; iClock<nSampleBeforePeak ; iClock++ ) {
				if( PeakTime-iClock-1<0 ) break;
				if( wfm[PeakTime-iClock-1]-pedestal>PeakHeight ) IsPileUp = true;
			}

			for( Int_t iClock=0 ; iClock<nSampleAfterPeak ; iClock++ ) {
				if( PeakTime+iClock>=256 ) break;
				if( wfm[PeakTime+iClock]-pedestal>PeakHeight ) IsPileUp = true;
			}
			*/


			//// find CFTime ////
			for( Int_t iClock=0 ; iClock<=PeakTime ; iClock++ ){
				if( PeakTime-iClock-1<0 ) break;
				if( !FindCFThre && (wfm[PeakTime-iClock]>CFTimeThre
					&& wfm[PeakTime-iClock-1]<=CFTimeThre) ){ // updated to include wfm equal to CFTimeThre
					CFTime = PeakTime-iClock-1+( (CFTimeThre-wfm[PeakTime-iClock-1])
						/(wfm[PeakTime-iClock]-wfm[PeakTime-iClock-1]) );
					FindCFThre = true;
				}
			}
			//// find FallTime ////
			for( Int_t iClock=0 ; iClock<256-PeakTime ; iClock++ ){
				if( PeakTime+iClock+1>=256 ) break;
				if( !FindFallTimeThre && (wfm[PeakTime+iClock]>FallTimeThre
					&& wfm[PeakTime+iClock+1]<=FallTimeThre) ){ // updated to include wfm equal to FallTimeThre
					FallTime = PeakTime+iClock+( (wfm[PeakTime+iClock]-FallTimeThre)
						/(wfm[PeakTime+iClock]-wfm[PeakTime+iClock+1]) );
					FindFallTimeThre = true;
				}
			}

			if( (FindCFThre||FindFallTimeThre) && !IsPileUp ){
				if( nHit< MTBP::NMaxHitPerEvent ){
					
					ene[nHit]  = PeakHeight;
					DetPeakTime[ChIndex][nHit] = PeakTime;
					
					if(FindCFThre) time[nHit] = CFTime;
					else time[nHit] = -9999;

					if(FindFallTimeThre) DetFallTime[ChIndex][nHit] = FallTime;
					else DetFallTime[ChIndex][nHit] = -9999;

					nHit++;
				}
			}
		}
	}



	nhit=nHit;




	if( ApplyT0Calib ){
		Float_t TimeOffset = AccidentalDetTCalibConst[ChIndex] ;//[clock]
		for(int ihit=0;ihit<MTBP::NMaxHitPerEvent;ihit++){
		   	time[ihit] += TimeOffset;
		   	DetFallTime[ChIndex][ihit] += TimeOffset;
		  
			time[ihit] += t00;
		  	DetFallTime[ChIndex][ihit] += t00;
			DetPeakTime[ChIndex][ihit] += t00;
		}
	}  

	return;
}

void MTConvertUCV::AddAccidentalWfm( Int_t& number, Int_t* modID, Short_t* nhit, 
		Float_t (*ene) [MTBP::NMaxHitPerEvent], 
		Float_t (*time)[MTBP::NMaxHitPerEvent],
		Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit,
		Float_t (*updatedene)[MTBP::NMaxHitPerEvent], 
		Float_t (*updatedtime)[MTBP::NMaxHitPerEvent],
		Float_t (*wfm)[MTBP::nSample500] )
{ 
	if( !EnableAccidentalOverlay ) return;  

	
	
	for( Int_t iCh = 0 ; iCh < number ; ++iCh ){
		modID[iCh] = IndexIDArray[iCh];// for channels without true hit
		if( modID[iCh] != AccidentalDetModID[iCh] ){
			///// Accidental data should be in E14ID order.
			///// If not, error message is issued.
			std::cout << "DetName " << DetectorName << ", modID = " << modID[iCh] << ", AccidentalDetModID = "<< AccidentalDetModID[iCh] << std::endl;
			std::cout << "Mismatch between IndexIDArray and AccidentalModID." << std::endl;
			return;	
		}

		for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; iSample++ ){
			wfm[iCh][iSample] += (AccidentalDetWfm[iCh][iSample] - AccidentalDetPedestal[iCh]) * AccidentalDetECalibConst[iCh];
			//std::cout << "Wfm[" << iCh << "][" << iSample << "]=" << wfm[iCh][iSample] << "\n";
		}
		///// pedestal calculation for merged pulse is done before energy and timing calculation 
		Double_t StdDev[2] = { TMath::RMS( 10, &wfm[iCh][0] ), TMath::RMS( 10, &wfm[iCh][MTBP::nSample500-10] )};


		Float_t  pedestal = TMath::Mean( 10, &wfm[iCh][1] );

		for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; ++iSample )
			wfm[iCh][iSample] -= pedestal;

		//std::cout << "ped=" << pedestal << "\n";
		

		/* //obsolete (20190517)
		   GetEnergyTimeFromWfm( &wfm[iCh][0], nhit[iCh], 
		   &ene[iCh][0], &time[iCh][0], iCh );
		   */

      
		GetEnergyTimeFromWfm( &DetWfm[iCh][0], nhit[iCh], 
			    &ene[iCh][0], &time[iCh][0], iCh );

		EvalFTTChisq(iCh, &DetWfm[iCh][0], DetFTTChisq[iCh], DetFTTTime[iCh]);
	}
}




double MTConvertUCV::GetVisibleEnergy(int modid, double y, double e)
{

  if(m_userFlag<20210201){  // prototype UCV
    double npe_MeV = 30.0/0.19;
    if(ran->Rndm()<1.0/7) npe_MeV = 20.0/0.19;
    
    double npe_ave = e*npe_MeV*exp(y/300.0);
    int npe = ran->Poisson(npe_ave);
    return npe/npe_MeV;
  } else{                // fiber UCV

    const double LY[12]={11.7,12.3,11.7,13.3,12.8,
			 12.8,11.9,11.9,13.0,12.2,
			 12.4,11.1};

    const double AttenuationLength=5671;
    
    double npe_ave;
    double npe_MeV;
    if(modid<=8){
      npe_MeV=LY[modid]/0.08827;
      npe_ave = e*npe_MeV*exp(-(170.75-y)/AttenuationLength);
      // For core channels,  the location of MPPC is 170.75mm
    } else {
      npe_MeV=LY[modid]/0.08513;
      npe_ave = e*npe_MeV*exp(-(170.75+y)/AttenuationLength);
      // For halo channels,  the location of MPPC is -170.75mm
    }
    int npe = ran->Poisson(npe_ave);
    return npe/npe_MeV;
  }
  
}

double MTConvertUCV::GetVisibleTime( int modid, double y, double t , double e)
{  
  const double tres = 2.0;

  if(m_userFlag<20210201){    // prototype UCV
    return t + (120.0-y)/100.0 + ran->Gaus(0, tres);
  } else{

    if(modid<=8){    
      return t + (170.75-y)/100.0 + ran->Gaus(0, tres);
    } else {
      return t + (170.75+y)/100.0 + ran->Gaus(0, tres);
    }    
  }
}


