#include "gsim2dst/MTConvertBHGC.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTConvertBHGC::MTConvertBHGC( Int_t userFlag ) : MTConvert500MHzDetector( MTBP::BHGC, userFlag )
{
   m_detname="BHGC";
  for( Int_t index = 0; index < 2*MTBP::nModBHGC ; ++index ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index) );
    IndexIDArray[index] = index;
  }
  //  IDIndexMap.insert( std::pair< Int_t, unsigned int>( MTBP::BHTSModuleID, 2*MTBP::nModBHGC ) );
  //  IndexIDArray[2*MTBP::nModBHGC] = MTBP::BHTSModuleID;
  
  for( Int_t ich = 0 ; ich < DetectorNCH ; ++ich ){
    DetModID[ich]     = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  Initialize();
}

MTConvertBHGC::~MTConvertBHGC()
{
  delete m_E14BP;
}

void MTConvertBHGC::ConvertFromDigiWithResponse(){
  GetFromDigiWithResponse( DetNumber, DetModID, DetNHit, DetEne, DetTime );
}

void MTConvertBHGC::GetTrueInfo( Int_t& number, Int_t* modID, Short_t* nhit,
				 Float_t (*ene) [MTBP::NMaxHitPerEvent], 
				 Float_t (*time)[MTBP::NMaxHitPerEvent] )
{
  TClonesArray *digiArray = GetDigiArray();
  number = DetectorNCH;
  for( Int_t iCh = 0 ; iCh < number ; ++iCh )
    modID[iCh] = IndexIDArray[iCh];
  
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t idigi = 0 ; idigi < nDigi ; ++idigi ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At(idigi);
    Int_t index = IDIndexMap[digi->modID];
    Int_t iHit  = nhit[index];
    ene[index][iHit]  = (float)digi->energy;
    time[index][iHit] = (float)digi->time / MTBP::SampleInterval500; //[ns]->[clock]
    ++nhit[index];
  } 
}

void MTConvertBHGC::GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit,
					     Float_t (*ene) [MTBP::NMaxHitPerEvent],
					     Float_t (*time)[MTBP::NMaxHitPerEvent] )
{  
  TClonesArray *digiArray = GetDigiArray(); 
  number = DetectorNCH;

  for( Int_t iCh = 0 ; iCh < number ; ++iCh )
    modID[iCh] = IndexIDArray[iCh];
  
  Int_t nDigi = digiArray->GetEntries();
  for( Int_t idigi = 0 ; idigi < nDigi ; ++idigi ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    Int_t ModID = digi->modID;
    Int_t index = IDIndexMap[ModID];
    Float_t TimeSmearing = gRandom->Gaus( 0, MTBP::BHGCTimeSmear );//[clock]
    /*
    if( ModID<2*MTBP::nModBHGC ){
      Float_t nPE = (Float_t)(digi->energy);
      Float_t SmearedLY = gRandom->Gaus( nPE, MTBP::BHGC1peSigma*TMath::Sqrt(nPE) );
      ene[index][0] = SmearedLY;
      if( SmearedLY<MTBP::BHGCTimeRes_MinLY ) SmearedLY = MTBP::BHGCTimeRes_MinLY;
      TimeSmearing = gRandom->Gaus( 0, TMath::Hypot( MTBP::BHGCTimeRes_b/TMath::Sqrt(SmearedLY),
      MTBP::BHGCTimeRes_c/SmearedLY ) );
    }
    else
    ene[index][0] = digi->energy;
    */
    Float_t nPE = (Float_t)(digi->energy);
    Float_t SmearedLY = gRandom->Gaus( nPE, MTBP::BHGC1peSigma*TMath::Sqrt(nPE) );
    ene[index][0]  = SmearedLY;
    time[index][0] = digi->time / MTBP::SampleInterval500 + TimeSmearing;//[ns]->[clock]
    time[index][0] += AccidentalTimeShift;
    ++nhit[index];
  }
  
  return;
}

