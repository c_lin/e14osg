#include "gsim2dst/MTConvertDetector.h"

Int_t MTConvertDetector::AccidentalRunID = 0;
Int_t MTConvertDetector::AccidentalRunID_fillflag = 0;
/// constructor
MTConvertDetector::MTConvertDetector()
   : m_dbWfmGenerator(NULL),
     m_fttCalculator(NULL),
     m_onlineClusSimulator(NULL)
{
   m_detname = "";
   m_WFMsave = false;
   ApplyWfmCorrection = 0;
}

/// destructor
MTConvertDetector::~MTConvertDetector()
{
   delete m_dbWfmGenerator;
   delete m_fttCalculator;
   delete m_onlineClusSimulator;
}

bool MTConvertDetector::SetDbWfmDir( const std::string dbWfmFileDir )
{
   if( m_dbWfmGenerator ) return false;
   LcLib::LcDbWfmGenerator *dbWfmGenerator = new LcLib::LcDbWfmGenerator( m_detname );
   const std::string dbfile = dbWfmFileDir + "/" + m_detname + ".root";
   if( !dbWfmGenerator->SetDbWfmFile(dbfile) ){
      std::cout<<" Warning : Set Db waveform for " << m_detname << " but fails. " << std::endl;
      delete dbWfmGenerator;
      return false;
   }else{
      m_dbWfmGenerator = dbWfmGenerator;
      m_dbWfmGenerator->SetFastMode();
      return true;
   } 
}

bool MTConvertDetector::SetFTTDir( const std::string fttFileDir )
{
   if( m_fttCalculator ) return false;
   LcLib::LcFTT *fttCalculator = new LcLib::LcFTT( m_detname );
   const std::string fttfile = fttFileDir + "/" + m_detname + ".root";
   if( !fttCalculator->SetFTTFile(fttfile) ){
      std::cout<<" Warning : Set FTT waveform for " << m_detname << " but fails. " << std::endl;
      delete fttCalculator;
      return false;
   }else{
      m_fttCalculator = fttCalculator;
      return true;
   }
}

void MTConvertDetector::EnableOnlineClusteringSimulation( const Int_t userflag )
{ 
   /// No CDT trigger system before run74
   if( userflag<20170401 ) return; 

   /// 
   m_onlineClusSimulator = new LcLib::LcTrigSimCluster;
   m_onlineClusSimulator->SetMaskingByUserflag(userflag);
   SetOnlineClusNominalSample();
   SetOnlineClusPeakThreshold();
}

bool  MTConvertDetector::AddOnlineClusterBranch( TTree *tr )
{
   if( tr==NULL ) return false;
   tr->Branch("CDTNum",&m_CDTNum,"CDTNum/I");
   return true;
}
 
void  MTConvertDetector::SetCDTBitMap( Short_t *CDTBitMap )
{
  TH2D *h_CDTMap;// = new TH2D("", "", 38,0,38, 38,0,38 );
  h_CDTMap = m_onlineClusSimulator->GetClusterBitMap();
  for(int ibitx=0; ibitx<38; ibitx++){
   for(int ibity=0; ibity<38; ibity++){
      if( h_CDTMap->GetBinContent(ibitx+1,ibity+1) ) {CDTBitMap[ibitx*38+ibity]=1;}
      else {CDTBitMap[ibitx*38+ibity]=0;}
    }
  }
  delete h_CDTMap;
  return ;
}

