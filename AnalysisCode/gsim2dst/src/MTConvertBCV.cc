#include "gsim2dst/MTConvertBCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <cmath>

MTConvertBCV::MTConvertBCV( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::BCV, userFlag )
{
  m_detname="BCV";
  for( unsigned int index=0; index<(UInt_t)MTBP::DetNChannels[MTBP::BCV]; index++ ){
    if( index < 32 ){
      IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index ) );
      IndexIDArray[index] = index;
    }else if( index < 64 ){
      IDIndexMap.insert( std::pair< Int_t, unsigned int>( index+68, index ) );
      IndexIDArray[index] = index+68;
    }
  }

  DetNumber     = DetectorNCH;
  DetTrueNumber = DetectorNCH;

  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  pulseGenerator = new MTPulseGenerator();
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  WfmTimeShift = MTBP::BCVWfmTimeShift;
  WfmSigma0 = MTBP::BCVWfmSigma0;
  WfmAsymPar = MTBP::BCVWfmAsymPar;
  
  Initialize(); 
}
      
MTConvertBCV::~MTConvertBCV()
{
  delete pulseGenerator;
  delete m_E14BP;
}		 

void MTConvertBCV::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }  
}

void MTConvertBCV::GetFromMTime( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  number = DetNumber;
  for( int ich=0; ich<DetNumber; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich] = 0;
    time[ich] = 0;
  }

  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedFrontVec;
  std::vector< pEnergyTime > propagatedRearVec;
  std::vector< pEnergyTime > propagatedClusteredVec;
  
  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedFrontVec.clear();
    propagatedRearVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;

      // front
      energyTime.energy = GetFrontVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetFrontVisibleTime( mtime->r.z(), mtime->time );
      propagatedFrontVec.push_back( energyTime );

      // rear
      energyTime.energy = GetRearVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetRearVisibleTime( mtime->r.z(), mtime->time );
      propagatedRearVec.push_back( energyTime );

    }

    // If there is no hit before 1us, it continues.
    if( propagatedFrontVec.size() == 0 ) continue;

    // front
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedFrontVec, propagatedClusteredVec );
    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    ene[ IDIndexMap[digi->modID]] = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[ IDIndexMap[digi->modID]] = (float)pulseGenerator->GetPseudoConstantFractionTime() / 8.; // [ns]->[clock]

    // rear
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedRearVec, propagatedClusteredVec );
    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    ene[ IDIndexMap[digi->modID+100]]  = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[ IDIndexMap[digi->modID+100]] = (float)pulseGenerator->GetPseudoConstantFractionTime() / 8.; // [ns]->[clock]

  }// idigi

}
  
void MTConvertBCV::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
//updated at 15th Mar, 2014
//updated at 4th May, 2014 (PTime and FallTime added)
{
  number = DetNumber;
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();


  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedFrontVec;
  std::vector< pEnergyTime > propagatedRearVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  WfmFunc->SetParameter( 2, WfmSigma0 );
  WfmFunc->SetParameter( 3, WfmAsymPar );
  
  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedFrontVec.clear();
    propagatedRearVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // front
      energyTime.energy = GetFrontVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetFrontVisibleTime( mtime->r.z(), mtime->time );
      propagatedFrontVec.push_back( energyTime );

      // rear
      energyTime.energy = GetRearVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetRearVisibleTime( mtime->r.z(), mtime->time );
      propagatedRearVec.push_back( energyTime );

    }

    // front
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedFrontVec, propagatedClusteredVec );
    GenerateWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec );

    // rear
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedRearVec, propagatedClusteredVec );
    GenerateWfmFromMTimeVec( IDIndexMap[digi->modID+100], propagatedClusteredVec );
  }// idigi
  
  if( !EnableAccidentalOverlay ){
    for( Int_t iCh=0 ; iCh<number ; iCh++ )
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
  }  
  return;
}


void MTConvertBCV::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();
  
  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ){
      std::cerr << "MTConvertBCV::GetTrueInfo : ModID" << digi->modID << " does not exist." << std::endl;
    }else{
      ene[ IDIndexMap[digi->modID]] = (float)digi->energy;
      time[ IDIndexMap[digi->modID]] = (float)digi->time /8.; // [ns]->[clock]
    }
  }
}

double MTConvertBCV::GetFrontVisibleEnergy( double z, double e)
{
  double deltaZ = z - 4098; // z is absolute position. delta z is normalized at the center of BCV.  
  return e / 2. * exp( -deltaZ / ( MTBP::CBARLAMBDA + MTBP::CBARALPHA * deltaZ ) );
}

double MTConvertBCV::GetRearVisibleEnergy( double z, double e)
{
  double deltaZ = z - 4098;
  return e / 2. * exp( deltaZ / ( MTBP::CBARLAMBDA - MTBP::CBARALPHA * deltaZ ) );
}

double MTConvertBCV::GetFrontVisibleTime( double z, double t )
{
  double deltaZ = z - 4098;
  return t + (MTBP::CBARLength/2+deltaZ)/MTBP::CBARPropVelo - MTBP::CBARLength/2/MTBP::CBARPropVelo;
}

double MTConvertBCV::GetRearVisibleTime( double z, double t )
{
  double deltaZ = z - 4098;
  return t + (MTBP::CBARLength/2-deltaZ)/MTBP::CBARPropVelo - MTBP::CBARLength/2/MTBP::CBARPropVelo;
}
