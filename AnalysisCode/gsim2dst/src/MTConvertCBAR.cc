#include "gsim2dst/MTConvertCBAR.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <cmath>
#include <fstream>



MTConvertCBAR::MTConvertCBAR( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::CBAR, userFlag )
{
   m_detname="CBAR";
  for( unsigned int index=0; index<(UInt_t)MTBP::DetNChannels[MTBP::CBAR]; index++ ){
    if( index < 64 ){
      IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index ) );
      IndexIDArray[index] = index;
    }else if( index < 128 ){
      IDIndexMap.insert( std::pair< Int_t, unsigned int>( index+36, index ) );
      IndexIDArray[index] = index+36;
    }
  }

  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;

  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  pulseGenerator = new MTPulseGenerator();
  WfmTimeShift = MTBP::CBARWfmTimeShift;
  WfmSigma0 = MTBP::CBARWfmSigma0;
  WfmAsymPar = MTBP::CBARWfmAsymPar;

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  //noiseParams = new double[64]();
  Initialize();
  //LoadAccidentalParams();
}      

MTConvertCBAR::~MTConvertCBAR()
{
  delete pulseGenerator;
  delete m_E14BP;
  //delete[] noiseParams;
}


void MTConvertCBAR::Initialize( void )
{

  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
  
}



void MTConvertCBAR::GetFromMTime( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{

  number = DetNumber;
  for( int ich=0; ich<DetNumber; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich] = 0;
    time[ich] = 0;
  }


  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();


  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedFrontVec;
  std::vector< pEnergyTime > propagatedRearVec;
  std::vector< pEnergyTime > propagatedClusteredVec;



  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedFrontVec.clear();
    propagatedRearVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // Ignore over 1us hit
      if( mtime->time > 1000 ) continue;

      // front
      energyTime.energy = GetFrontVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetFrontVisibleTime( mtime->r.z(), mtime->time );
      propagatedFrontVec.push_back( energyTime );

      // rear
      energyTime.energy = GetRearVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetRearVisibleTime( mtime->r.z(), mtime->time );
      propagatedRearVec.push_back( energyTime );
    }

    // If there is no hit before 1us, it continues.
    if( propagatedFrontVec.size() == 0 ) continue;

    // front
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedFrontVec, propagatedClusteredVec );
    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    ene[ IDIndexMap[digi->modID]] = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[ IDIndexMap[digi->modID]] = (float)pulseGenerator->GetPseudoConstantFractionTime() / 8.; // [ns]->[clock]


    // rear
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedRearVec, propagatedClusteredVec );
    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    ene[ IDIndexMap[digi->modID+100]]  = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[ IDIndexMap[digi->modID+100]] = (float)pulseGenerator->GetPseudoConstantFractionTime() / 8.; // [ns]->[clock]


  }// idigi

}


/*
void MTConvertCBAR::GetFromMTimeWithResponse(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{

  number = DetNumber;
  for( int ich=0; ich<DetNumber; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich] = 0;
    time[ich] = 0;
  }


  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();


  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedFrontVec;
  std::vector< pEnergyTime > propagatedRearVec;
  std::vector< pEnergyTime > propagatedClusteredVec;
  

  float accidentalE=0, accidentalZ=0, accidentalT=0;

  // Hitless module loop
  // If there is no digi info in this module, add accidental only 60ns. (and noise to be added)
  for( int ich=0; ich<DetNumber; ich++ ){
    bool hitFlag = false;
    for( int idigi=0; idigi<nDigi; idigi++ ){
      GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
      if( digi->modID == ich ){
	hitFlag = true;
	break;
      }
    }

    if( !hitFlag ){
      propagatedFrontVec.clear();
      propagatedRearVec.clear();
      GetAccidentalInfo( ich, accidentalE, accidentalZ, accidentalT );
      if( accidentalT < 60 ){
	// front
	energyTime.energy = GetFrontVisibleEnergy( accidentalZ+4098, accidentalE );
	energyTime.time   = GetFrontVisibleTime( accidentalZ+4098, accidentalT );
	propagatedFrontVec.push_back( energyTime );
	pulseGenerator->SetEnergyTime( propagatedFrontVec );
	ene[ IDIndexMap[ich]] = pulseGenerator->GetPseudoIntegratedEnergy();
	time[IDIndexMap[ich]] = pulseGenerator->GetPseudoConstantFractionTime() / 8.;
	// rear
	energyTime.energy = GetRearVisibleEnergy( accidentalZ+4098, accidentalE );
	energyTime.time   = GetRearVisibleTime( accidentalZ+4098, accidentalT );
	propagatedRearVec.push_back( energyTime );
	pulseGenerator->SetEnergyTime( propagatedRearVec );
	ene[ IDIndexMap[ich+100]] = pulseGenerator->GetPseudoIntegratedEnergy();
	time[IDIndexMap[ich+100]] = pulseGenerator->GetPseudoConstantFractionTime() / 8.;
      }
    }
  }



  // digi loop ( Hit module loop)
  for( int idigi=0; idigi<nDigi; idigi++ ){
    propagatedFrontVec.clear();
    propagatedRearVec.clear();
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);

    // mtime loop
    for( int imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      // front
      energyTime.energy = GetFrontVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetFrontVisibleTime( mtime->r.z(), mtime->time );
      propagatedFrontVec.push_back( energyTime );
      
      // rear
      energyTime.energy = GetRearVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetRearVisibleTime( mtime->r.z(), mtime->time );
      propagatedRearVec.push_back( energyTime );
    }

    // Add accidental only 0-60ns window
    GetAccidentalInfo( digi->modID, accidentalE, accidentalZ, accidentalT );
    if( accidentalT < 60 ){
      // front
      energyTime.energy = GetFrontVisibleEnergy( accidentalZ+4098, accidentalE );
      energyTime.time   = GetFrontVisibleTime( accidentalZ+4098, accidentalT );
      propagatedFrontVec.push_back( energyTime );
      // rear
      energyTime.energy = GetRearVisibleEnergy( accidentalZ+4098, accidentalE );
      energyTime.time   = GetRearVisibleTime( accidentalZ+4098, accidentalT );
      propagatedRearVec.push_back( energyTime );
    }

    // If there is no hit before 1us, it continues.
    if( propagatedFrontVec.size() == 0 ) continue;    

    
    // front
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedFrontVec, propagatedClusteredVec );
    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    ene[ IDIndexMap[digi->modID]] = pulseGenerator->GetPseudoIntegratedEnergy();
    time[ IDIndexMap[digi->modID]] = pulseGenerator->GetPseudoConstantFractionTime() / 8.; // [ns]->[clock]
    
    // rear
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedRearVec, propagatedClusteredVec );
    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    ene[ IDIndexMap[digi->modID+100]]  = pulseGenerator->GetPseudoIntegratedEnergy();
    time[ IDIndexMap[digi->modID+100]] = pulseGenerator->GetPseudoConstantFractionTime() / 8.; // [ns]->[clock]

  }// idigi



}
*/

  
void MTConvertCBAR::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
//updated at 27th Feb, 2014 by Maeda Yosuke
//updated at 4th May, 2014 by Maeda Yosuke (PTime and FallTime added)
{
  
  number = DetNumber;
  for( int ich=0; ich<DetNumber; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich] = 0;
    time[ich] = 0;
  }


  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedFrontVec;
  std::vector< pEnergyTime > propagatedRearVec;
  std::vector< pEnergyTime > propagatedModuleVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  //always use this value, added at 14th Feb, 2014 by Maeda Yosuke
  WfmFunc->SetParameter( 2, WfmSigma0 );
  WfmFunc->SetParameter( 3, WfmAsymPar );
  
  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    propagatedFrontVec.clear();
    propagatedRearVec.clear();
    propagatedModuleVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;

      // front
      Double_t CorrectedEne_Front = GetFrontVisibleEnergy( mtime->r.z(), mtime->energy );
      //// photo-static fluctuation ////
      if( m_dbWfmGenerator ){
	Int_t nPE_Front = gRandom->Poisson( CorrectedEne_Front * MTBP::CBARLightYieldPerMeV );
	CorrectedEne_Front = (Float_t)(nPE_Front)/MTBP::CBARLightYieldPerMeV;
	if( nPE_Front==0 ) CorrectedEne_Front = 1e-6; // set small value to avoid TSpline3 Eval error
      }
      energyTime.energy = CorrectedEne_Front;
      energyTime.time   = GetFrontVisibleTime( mtime->r.z(), mtime->time );
      propagatedFrontVec.push_back( energyTime );
      
      // rear
      //// photo-static fluctuation ////
      Double_t CorrectedEne_Rear = GetRearVisibleEnergy( mtime->r.z(), mtime->energy );
      if( m_dbWfmGenerator ){
	Int_t nPE_Rear = gRandom->Poisson( CorrectedEne_Rear * MTBP::CBARLightYieldPerMeV );
	CorrectedEne_Rear = (Float_t)(nPE_Rear)/MTBP::CBARLightYieldPerMeV;
	if( nPE_Rear==0 ) CorrectedEne_Rear = 1e-6; // set small value to avoid TSpline3 Eval error
      }
      energyTime.energy = CorrectedEne_Rear;
      energyTime.time   = GetRearVisibleTime( mtime->r.z(), mtime->time );
      propagatedRearVec.push_back( energyTime );

      // module info. (for timing smearing) //
      energyTime.energy = mtime->energy;
      energyTime.time   = mtime->time;
      propagatedModuleVec.push_back( energyTime );      
    }

    // timing resolution (2019/06/15 added by S.Shinohara based on g4ana study) //
    Float_t TimeSmearing_Front = 0;
    Float_t TimeSmearing_Rear = 0;
    if( m_dbWfmGenerator ){
      if( !propagatedModuleVec.empty() ){
	pulseGenerator->SetEnergyTime( propagatedModuleVec );
	
	Float_t EnergyForTimeSmearing = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy(-1,1000);
	if( EnergyForTimeSmearing<0.1 ) EnergyForTimeSmearing = 0.1;
	Float_t SmearingSigma = sqrt( DetSmearingSigma / EnergyForTimeSmearing );
	TimeSmearing_Front = sqrt(2)*gRandom->Gaus( 0, SmearingSigma );
	TimeSmearing_Rear  = sqrt(2)*gRandom->Gaus( 0, SmearingSigma );
      }
    }

    /*
      //// result from Murayama-san's study. Due to the over-smearing, not using for 2017analysis ///
    if( !propagatedFrontVec.empty() ){
      pulseGenerator->SetEnergyTime( propagatedFrontVec );
      Float_t EnergyForTimeSmearing = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy(-1,1000);
      if( EnergyForTimeSmearing<0.1 ) EnergyForTimeSmearing = 0.1;
      Float_t SmearingSigma = sqrt( 
				   TMath::Power( 36.8121/(EnergyForTimeSmearing*10.39), 2) 
				   + TMath::Power( 9.65511/sqrt(EnergyForTimeSmearing*10.39), 2) 
				   + TMath::Power( 0.0000001, 2) );
      TimeSmearing_Front = gRandom->Gaus( 0, SmearingSigma );
    }
    if( !propagatedRearVec.empty() ){
      pulseGenerator->SetEnergyTime( propagatedRearVec );
      Float_t EnergyForTimeSmearing = (Float_t)pulseGenerator->GetPseudoIntegratedEnergy(-1,1000);
      if( EnergyForTimeSmearing<0.1 ) EnergyForTimeSmearing = 0.1;
      Float_t SmearingSigma = sqrt(
				   TMath::Power( 36.8121/(EnergyForTimeSmearing*10.39), 2) 
				   + TMath::Power( 9.65511/sqrt(EnergyForTimeSmearing*10.39), 2) 
				   + TMath::Power( 0.0000001, 2) );
      TimeSmearing_Rear = gRandom->Gaus( 0, SmearingSigma );
    }
    */

    // front
    Float_t TimeShiftCorrection_Front = TimeSmearing_Front;
    propagatedClusteredVec.clear();
    const Double_t k_Pitch = (m_dbWfmGenerator) ? 8. : 0.1;
    if( !propagatedFrontVec.empty() ){
      ClusteringMtime( propagatedFrontVec, propagatedClusteredVec, k_Pitch );
      if( m_dbWfmGenerator )
	GenerateDbWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec, TimeShiftCorrection_Front);
      else
	GenerateWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec);
    }
    //pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    //pulseGenerator->GeneratePulseShape();
    //ene[ IDIndexMap[digi->modID]] = (float)pulseGenerator->GetIntegrated();
    //time[ IDIndexMap[digi->modID]] = (float)pulseGenerator->GetConstantFractionTime() / 8.; // [ns]->[clock]
    
    // rear
    Float_t TimeShiftCorrection_Rear = TimeSmearing_Rear;
    propagatedClusteredVec.clear();
    if( !propagatedRearVec.empty() ){
      ClusteringMtime( propagatedRearVec, propagatedClusteredVec, k_Pitch );
      if( m_dbWfmGenerator )
	GenerateDbWfmFromMTimeVec( IDIndexMap[digi->modID+100], propagatedClusteredVec, TimeShiftCorrection_Rear);
      else
	GenerateWfmFromMTimeVec( IDIndexMap[digi->modID+100], propagatedClusteredVec);
    }
    //pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    //pulseGenerator->GeneratePulseShape();
    //ene[ IDIndexMap[digi->modID+100]]  = (float)pulseGenerator->GetIntegrated();
    //time[ IDIndexMap[digi->modID+100]] = (float)pulseGenerator->GetConstantFractionTime() / 8.; // [ns]->[clock]
  }// idigi
  
  if( !EnableAccidentalOverlay ){
    for( Int_t iCh=0 ; iCh<number ; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
      EvalFTTChisq( iCh, &DetWfm[iCh][0], DetFTTChisq[iCh], DetFTTTime[iCh] );
    }
  }
  
  return;
}


void MTConvertCBAR::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();
  
  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ){
      std::cerr << "MTConvertCBAR::GetTrueInfo : ModID" << digi->modID << " does not exist." << std::endl;
    }else{
      ene[ IDIndexMap[digi->modID]]  = (float)digi->energy;
      time[ IDIndexMap[digi->modID]] = (float)digi->time / 8.; // [ns]->[clock]
    }
  }
}



double MTConvertCBAR::GetFrontVisibleEnergy( double z, double e)
{
  double deltaZ = z - 4098; // z is absolute position. delta z is normalized at the center of CBAR.
  
  return e / 2. * exp( -deltaZ / ( MTBP::CBARLAMBDA + MTBP::CBARALPHA * deltaZ ) );
}


double MTConvertCBAR::GetRearVisibleEnergy( double z, double e)
{
  double deltaZ = z - 4098;
  return e / 2. * exp( deltaZ / ( MTBP::CBARLAMBDA - MTBP::CBARALPHA * deltaZ ) );
}


double MTConvertCBAR::GetFrontVisibleTime( double z, double t )
{
  double deltaZ = z - 4098;
  return t + (MTBP::CBARLength/2+deltaZ)/MTBP::CBARPropVelo - MTBP::CBARLength/2/MTBP::CBARPropVelo;
}

double MTConvertCBAR::GetRearVisibleTime( double z, double t )
{
  double deltaZ = z - 4098;
  return t + (MTBP::CBARLength/2-deltaZ)/MTBP::CBARPropVelo - MTBP::CBARLength/2/MTBP::CBARPropVelo;
}

/*
  int MTConvertCBAR::ClusteringMtime( std::vector< pEnergyTime >& inputEtVec, std::vector< pEnergyTime >& outputEtVec, double pitch )
  {
  int startIndex=0;

  double weightedTime=0.0, totalWeight=0.0;

  for( UInt_t index=0; index<inputEtVec.size(); index++ ){
  if( inputEtVec[index].time - inputEtVec[startIndex].time < pitch ){
  weightedTime += inputEtVec[index].time * inputEtVec[index].energy;
  totalWeight  += inputEtVec[index].energy;
  }else{
  energyTime.energy = totalWeight;
  energyTime.time   = weightedTime / totalWeight;
  outputEtVec.push_back( energyTime );
  weightedTime = inputEtVec[index].time * inputEtVec[index].energy;
  totalWeight  = inputEtVec[index].energy;
  startIndex = index;
  }
  }
  energyTime.energy = totalWeight;
  energyTime.time   = weightedTime / totalWeight;
  outputEtVec.push_back( energyTime );

  return outputEtVec.size();
  }
*/

/*
  void MTConvertCBAR::LoadAccidentalParams( void )
  {
  int dummy = 0;
  std::string filename = std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
  + std::string("/gsim2dst/CBAR/obsolete/CBARAccidentalParameters.txt");
  ifstream ifs( filename.c_str() );
  for( int imod=0; imod<64; imod++ ){
  ifs >> dummy;
  ifs >> accidentalRate[imod];
  for( int ipar=0; ipar<4; ipar++ ){
  ifs >> accidentalEnergyParams[imod][ipar];
  }
  for( int ipos=0; ipos<22; ipos++ ){
  ifs >> accidentalPositionParams[imod][ipos];
  }
  }

  ifs.close();

  // Integral of position histogram
  for( int imod=0; imod<64; imod++ ){
  for( int ipos=0; ipos<22; ipos++ ){
  accidentalPositionParamsIntegral[imod][ipos] = 0;
  }
  for( int ipos=1; ipos<22; ipos++ ){
  for( int jpos=0; jpos<ipos; jpos++ ){
  accidentalPositionParamsIntegral[imod][ipos] += accidentalPositionParams[imod][jpos];
  }
  }
  }


  }


  void MTConvertCBAR::GetAccidentalInfo( int modID, double& energy, double& position, double& time )
  {

  energy   = GetAccidentalEnergy( modID );
  position = GetAccidentalDeltaZ( modID );
  time     = GetAccidentalTime( modID );

  }


  double MTConvertCBAR::GetAccidentalEnergy( int modID )
  {
  double r0=0, r1=0;
  double comparisonX=0, comparisonY=0;
  double threshold=0;

  while( true ){
  r0 = gRandom->Rndm();
  r1 = gRandom->Rndm();
  comparisonX = -1./0.069 * log( r0 ); 
  if( comparisonX < 1 ) continue; // neglect less than 1MeV
  comparisonY = r1 * exp( -0.069 * comparisonX ); // comparison function for rejection method

  threshold  = exp( accidentalEnergyParams[modID][0] + accidentalEnergyParams[modID][1]*comparisonX );
  threshold += exp( accidentalEnergyParams[modID][2] + accidentalEnergyParams[modID][3]*comparisonX );
  if( comparisonY < threshold ){
  return comparisonX;
  }
  }
  }



  double MTConvertCBAR::GetAccidentalDeltaZ( int modID )
  {
  double r0 = gRandom->Rndm();
  double r1 = gRandom->Rndm();
  int region = -1;
  
  for( int iregion=0; iregion<21; iregion++ ){
  if( accidentalPositionParamsIntegral[modID][iregion]<=r0 && r0<accidentalPositionParamsIntegral[modID][iregion+1] ){
  region = iregion;
  break;
  }
  }
  if( region == -1 ) region = 21;
  
  double position = region + r1;
  position *= 250.; // mm from upstream surface
  position -= 5500./2; // mm from center
  return position;
  }


  double MTConvertCBAR::GetAccidentalTime( int modID )
  {
  double t = 1./accidentalRate[modID]; // [nsec]
  t *= gRandom->Rndm();
  return t;
  }

*/  




