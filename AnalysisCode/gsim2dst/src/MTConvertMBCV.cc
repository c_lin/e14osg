#include "gsim2dst/MTConvertMBCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTConvertMBCV::MTConvertMBCV( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::MBCV, userFlag )
{
  m_detname = "MBCV";
  for( int index=0; index<16; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index) );
    IndexIDArray[index] = index;
  }

  DetNumber     = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  WfmTimeShift = MTBP::MBCVWfmTimeShift;
  WfmSigma0 = MTBP::MBCVWfmSigma0;
  WfmAsymPar = MTBP::MBCVWfmAsymPar;

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  Initialize();
}

MTConvertMBCV::~MTConvertMBCV()
{
  delete m_E14BP;
}

void MTConvertMBCV::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}


void MTConvertMBCV::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ) continue;//ignore no dead materials
    ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
    time[IDIndexMap[digi->modID]]  = (float)digi->time /8.; //[ns]->[clock]
  }
}

void MTConvertMBCV::GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ) continue;//ignore no dead materials
    if(digi->modID==60||digi->modID==64){
      for( int sl=0; sl<2; sl++ ){
	ene[IDIndexMap[digi->modID]+sl]   = (float)digi->energy;
	time[IDIndexMap[digi->modID]+sl]  = (float)digi->time /8.; // [ns]->[clock]
      }
    }
    else{
      ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
      time[IDIndexMap[digi->modID]]  = (float)digi->time /8.; // [ns]->[clock]
    }    
  }
}

void MTConvertMBCV::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
{
  number = DetectorNCH;
  
  //use digi information to generate waveform
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    Int_t ModID = digi->modID;
    if( IDIndexMap.count( ModID )==0 ) continue;//ignore no dead materials
    propagatedVec.clear();
    propagatedClusteredVec.clear();
    Int_t iCh = IDIndexMap[ModID];
    
    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      //No detector response
      energyTime.energy = mtime->energy;
      energyTime.time   = mtime->time;
      propagatedVec.push_back( energyTime );
    }
    
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );
    
    WfmFunc->SetParameter( 2, WfmSigma0 );
    WfmFunc->SetParameter( 3, WfmAsymPar );
    
    GenerateWfmFromMTimeVec( iCh, propagatedClusteredVec);
  }
  
  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<number; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );
    }
  }
  
  return;
}

