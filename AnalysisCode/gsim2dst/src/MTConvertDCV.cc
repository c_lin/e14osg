#include "gsim2dst/MTConvertDCV.h"

MTConvertDCV::MTConvertDCV( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::DCV, userFlag )
{

  propagatedVec = new std::vector< pEnergyTime >[DetectorNCH];
  propagatedClusteredVec = new std::vector< pEnergyTime >[DetectorNCH];

  for( int index=0; index<DetectorNCH; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index) );
    IndexIDArray[index] = index;
  }

  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  Initialize();

	
  // Npe/MeV at the center of module per 1 MPPC
  // 0.777 corresponds to MIP energy of 5-mm-thick plastic scintilator
  Npe_MeV = 30/0.777; 

  ran = new TRandom3(1);
}

MTConvertDCV::~MTConvertDCV(){
	delete ran;
	delete [] propagatedVec;
	delete [] propagatedClusteredVec;
}

void MTConvertDCV::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 

    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}


void MTConvertDCV::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ) continue;
	for(int j=0;j<4;j++){
    	ene[IDIndexMap[4*digi->modID+j]]   = (float)digi->energy/4;
    	time[IDIndexMap[4*digi->modID+j]]  = (float)digi->time /8.; //[ns]->[clock]
	}
  }
}

void MTConvertDCV::GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{

  // This function is no more used.	
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ) continue;//ignore no dead materials
    for(int i=0;i<4;i++){  
		ene[IDIndexMap[digi->modID]*4+i]   = (float)digi->energy/4;
      	time[IDIndexMap[digi->modID]*4+i]  = (float)digi->time /8.; // [ns]->[clock]
  	}
  }
}


void MTConvertDCV::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, 
		Float_t* time, Float_t* PTime, Float_t* FallTime ){

  number = DetectorNCH;

  //use digi information to generate waveform
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();

  for( int ich=0; ich<DetNumber; ich++ ){
	  propagatedVec[ich].clear();
	  propagatedClusteredVec[ich].clear();
  }	  


  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    Int_t ModID = digi->modID;
    Float_t ene_digi = digi->energy;
    Float_t time_digi = digi->time;
    if( IDIndexMap.count( ModID )==0 ) continue;//ignore no dead materials
    Int_t iCh = IDIndexMap[ModID];
    
    Int_t n_mtime=0;
    
    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      Double_t X_mtime= mtime->r.X();
      Double_t Y_mtime= mtime->r.Y();
      Double_t Z_mtime= mtime->r.Z();
      Float_t energy_mtime=mtime->energy;
      Float_t time_mtime=mtime->time;
      Int_t ModID_mtime = mtime->modID;

	  for(int i_readoutch=0;i_readoutch<4;i_readoutch++){ 
		  
		  if(energy_mtime>0){
			  energyTime.energy= GetEnergy(energy_mtime,Z_mtime, i_readoutch, ModID_mtime);

			  // factor of four comes from the definition of energy. See GetEnergy().
			  energyTime.time  = GetTime(time_mtime,Z_mtime, 4*energyTime.energy, i_readoutch, ModID_mtime);
			  
			  if(energyTime.energy>1e-5 && energyTime.time<1000){
				  propagatedVec[ModID_mtime*4+i_readoutch].push_back( energyTime );
			  }
		 
		  }
	  }
    }

  }

  WfmFunc->SetParameter( 0, 1 );
  WfmFunc->SetParameter( 1, 0 );
  WfmFunc->SetParameter( 2, MTBP::DCVWfmSigma0 );
  WfmFunc->SetParameter( 3, MTBP::DCVWfmAsymPar );
  for( int iCh=0; iCh<DetNumber; iCh++ ){
  	  if(propagatedVec[iCh].size()==0)continue;

	  // mtime clustering
	  ClusteringMtime( propagatedVec[iCh], propagatedClusteredVec[iCh] );

	  double E = 0;
	  for (std::vector<pEnergyTime>::iterator it = propagatedClusteredVec[iCh].begin();
			  it != propagatedClusteredVec[iCh].end(); it++){
		  E += it->energy;
	  }	

	  // Smear the timing based on the simple gaussian function
	  // This can basically include everything such as
	  // 1. optical photon propagation, 2. deformation of shape due to DAQ, 
	  // 3. MPPC timing resolution, 4. DAQ jitter, and 5. time constant of scintilation light.
	  // ---- Important! ----
	  // step by step fluctuation like 1, 3, 5 should be taken into account at every step, 
	  // i.e., GetTime() should be responsible for this implementation.
	  // On the other hand, 2, 4 are sources of fluctuation 
	  // once the accumulation of steps has been finished.
	  // Here, the latter categories (like 2 and 4) should be included.
	  
	  const double a = MTBP::DCVSigmaT; // constant term 
	  const double b = 0.0; // energy term
	  double sigma_t = sqrt(a*a + b*b/E);
	  double delT = ran->Gaus(0, sigma_t);
		
	  for(std::vector<pEnergyTime>::iterator it = propagatedClusteredVec[iCh].begin()
	  		  ; it != propagatedClusteredVec[iCh].end(); it++) {
  		  it->time += delT;
	  }

	  GenerateWfmFromMTimeVec(iCh, propagatedClusteredVec[iCh]);

  }

  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<DetNumber; iCh++ ){
	  if(propagatedClusteredVec[iCh].size()==0)continue;

      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );
	}
  }

  return;
}

Float_t MTConvertDCV::GetEnergy(Float_t energy_mtime, Double_t Z_mtime, Int_t chID, Int_t ModID){

	const double z_DCV1 = 7820.2;
	const double z_DCV2 = 9353.2;
	double len_DCV;
	
	double z;
	if(ModID<4){
		len_DCV = 1410;
		z = Z_mtime - z_DCV1 + len_DCV/2;
	}else{
		len_DCV = 1460;
	   	z = Z_mtime - z_DCV2 + len_DCV/2;
	}


	const double att = MTBP::DCV_attenuation;

	double Npe;
	if(chID<2){
		// upstream MPPCs
		double zprop = z;
		Npe = energy_mtime*Npe_MeV*exp(-(zprop-len_DCV/2)/att);
	}else{
		// downstream MPPCs
		double zprop = len_DCV-z;
		Npe = energy_mtime*Npe_MeV*exp(-(zprop-len_DCV/2)/att);
	}
	double Npe_smeared;
	
	if(Npe<0)Npe_smeared = 0;
	else Npe_smeared = ran->Poisson(Npe);

	// the energy is divided by four so that sum of four MPPC energies becomes 
	// the energy of module (definition of DCV calibration).
	double energy = Npe_smeared/Npe_MeV/4;
	return energy;
}
    

Float_t MTConvertDCV::GetTime(Float_t time_mtime, Double_t Z_mtime, Double_t E_mtime, Int_t chID, Int_t ModID){


  const double z_DCV1 = 7820.2;
  const double z_DCV2 = 9353.2;
  double len_DCV;
  
  double z;
  if(ModID<4){
	  len_DCV = 1410;
	  z = Z_mtime - z_DCV1 + len_DCV/2;
  }else{
	  len_DCV = 1460;
	  z = Z_mtime - z_DCV2 + len_DCV/2;
  }
  const double propagation_v = 173.7; // mm/ns	
  
  Float_t time;
  if(chID<2){
		// upstream MPPCs
	  time = time_mtime + z/propagation_v;
  }else{
		// downstream MPPCs
	  time = time_mtime + (len_DCV-z)/propagation_v;
  }

  
  // Distribution of the arrival time of photon at MPPC is assumed to be
  // determined by the time constant of plastic scintilator.
  // Here, tentative value of 2ns is used.
  // (It may be worth to consider the difference of propagation time 
  // depending on the direction of emission of scintilated photons.
  // Now, this contribution is ignored).
  
  // Note that this implementation is not based on any reasoning from
  // real measurement!
  // This simply inspires ones to consider the intrinsic 
  // distribution of arrival time of photons. 
  
  const double sigma_intrinsic = 2.0;   

  int Npe = E_mtime*Npe_MeV;
  if(Npe<1e-4) return 9999;
  double sigma_Npe = sigma_intrinsic/sqrt(Npe);
  time += ran->Gaus(0, sigma_Npe);
  
  return time;

}






