#include "gsim2dst/MTConvertIBCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <cmath>

MTConvertIBCV::MTConvertIBCV( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::IBCV, userFlag )
{
   m_detname="IBCV";
  for( unsigned int index=0; index<(UInt_t)MTBP::DetNChannels[MTBP::IBCV]; index++ ){
    if( index < 32 ){
      IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index ) );
      IndexIDArray[index] = index;
    }else if( index < 64 ){
      IDIndexMap.insert( std::pair< Int_t, unsigned int>( index+68, index ) );
      IndexIDArray[index] = index+68;
    }
  }

  DetNumber     = DetectorNCH;
  DetTrueNumber = DetectorNCH;

  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  pulseGenerator = new MTPulseGenerator();
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  //  LoadCalibConst();
  //LoadCalibConstFromDB();
  WfmTimeShift = MTBP::IBCVWfmTimeShift;
  WfmSigma0 = MTBP::IBCVWfmSigma0;
  WfmAsymPar = MTBP::IBCVWfmAsymPar;

  Initialize();
  
}

MTConvertIBCV::~MTConvertIBCV()
{
  delete pulseGenerator;
  delete m_E14BP;
}

void MTConvertIBCV::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}

void MTConvertIBCV::GetFromMTime( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  number = DetNumber;
  for( int ich=0; ich<DetNumber; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich] = 0;
    time[ich] = 0;
  }

  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedFrontVec;
  std::vector< pEnergyTime > propagatedRearVec;
  std::vector< pEnergyTime > propagatedClusteredVec;
  
  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedFrontVec.clear();
    propagatedRearVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;

      // front
      energyTime.energy = GetFrontVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetFrontVisibleTime( mtime->r.z(), mtime->time );
      propagatedFrontVec.push_back( energyTime );

      // rear
      energyTime.energy = GetRearVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetRearVisibleTime( mtime->r.z(), mtime->time );
      propagatedRearVec.push_back( energyTime );

    }

    // If there is no hit before 1us, it continues.
    if( propagatedFrontVec.size() == 0 ) continue;

    // front
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedFrontVec, propagatedClusteredVec );
    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    ene[ IDIndexMap[digi->modID]] = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[ IDIndexMap[digi->modID]] = (float)pulseGenerator->GetPseudoConstantFractionTime() / 8.; // [ns]->[clock]

    // rear
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedRearVec, propagatedClusteredVec );
    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    ene[ IDIndexMap[digi->modID+100]]  = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[ IDIndexMap[digi->modID+100]] = (float)pulseGenerator->GetPseudoConstantFractionTime() / 8.; // [ns]->[clock]

  }// idigi

}
  
void MTConvertIBCV::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
{
  number = DetNumber;
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();


  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedFrontVec;
  std::vector< pEnergyTime > propagatedRearVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  WfmFunc->SetParameter( 2, WfmSigma0 );
  WfmFunc->SetParameter( 3, WfmAsymPar );
  
  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedFrontVec.clear();
    propagatedRearVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // front
      energyTime.energy = GetFrontVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetFrontVisibleTime( mtime->r.z(), mtime->time );
      propagatedFrontVec.push_back( energyTime );

      // rear
      energyTime.energy = GetRearVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetRearVisibleTime( mtime->r.z(), mtime->time );
      propagatedRearVec.push_back( energyTime );

    }

    // front
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedFrontVec, propagatedClusteredVec );
    GenerateWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec );

    // rear
    propagatedClusteredVec.clear();
    ClusteringMtime( propagatedRearVec, propagatedClusteredVec );
    GenerateWfmFromMTimeVec( IDIndexMap[digi->modID+100], propagatedClusteredVec );
  }// idigi
  
  if( !EnableAccidentalOverlay ){
    for( Int_t iCh=0 ; iCh<number ; iCh++ )
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );
  }

  return;
}


void MTConvertIBCV::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();
  
  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ){
      std::cerr << "MTConvertIBCV::GetTrueInfo : ModID" << digi->modID << " does not exist." << std::endl;
    }else{
      ene[ IDIndexMap[digi->modID]] = (float)digi->energy;
      time[ IDIndexMap[digi->modID]] = (float)digi->time /8.; // [ns]->[clock]
    }
  }
}



double MTConvertIBCV::GetFrontVisibleEnergy( double z, double e)
{
  static const double s_IBCVZPosition = DetZPosition;

  double deltaZ = z - (s_IBCVZPosition + MTBP::IBCVLength/2. ); // z is absolute position. delta z is normalized at the center of IBCV.
  return e / 2. * exp( -deltaZ / ( MTBP::IBCVLAMBDA_U + MTBP::IBCVALPHA_U * deltaZ ) );
}


double MTConvertIBCV::GetRearVisibleEnergy( double z, double e)
{
  static const double s_IBCVZPosition = DetZPosition;

  double deltaZ = z - (s_IBCVZPosition + MTBP::IBCVLength/2. ); // z is absolute position. delta z is normalized at the center of IBCV.
  return e / 2. * exp( deltaZ / ( MTBP::IBCVLAMBDA_D - MTBP::IBCVALPHA_D * deltaZ ) );
}


double MTConvertIBCV::GetFrontVisibleTime( double z, double t )
{
  static const double s_IBCVZPosition = DetZPosition;

  double deltaZ = z - (s_IBCVZPosition + MTBP::IBCVLength/2. ); // z is absolute position. delta z is normalized at the center of IBCV.
  return t + (MTBP::IBCVLength/2+deltaZ)/MTBP::IBCVPropVelo - MTBP::IBCVLength/2/MTBP::IBCVPropVelo;
}

double MTConvertIBCV::GetRearVisibleTime( double z, double t )
{
  static const double s_IBCVZPosition = DetZPosition;

  double deltaZ = z - (s_IBCVZPosition + MTBP::IBCVLength/2. ); // z is absolute position. delta z is normalized at the center of IBCV.
  return t + (MTBP::IBCVLength/2-deltaZ)/MTBP::IBCVPropVelo - MTBP::IBCVLength/2/MTBP::IBCVPropVelo;
}

