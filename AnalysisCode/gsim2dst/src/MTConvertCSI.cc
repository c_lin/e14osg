#include "gsim2dst/MTConvertCSI.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

Double_t myfunc(Double_t *x, Double_t* par){
	
	double term1 = par[0]*par[3]*(exp(-(x[0]-par[1]-par[2]*par[2]/2/par[5])/par[5])
			*TMath::Erfc((x[0]-par[1]-par[2]*par[2]/par[5])/1.414/par[2]) - exp(-(x[0]-par[1]-par[2]*par[2]/2/par[7])/par[7])*TMath::Erfc((x[0]-par[1]-par[2]*par[2]/par[7])/1.414/par[2]));
	double term2 = par[0]*par[4]*(exp(-(x[0]-par[1]-par[2]*par[2]/2/par[6])/par[6])
			*TMath::Erfc((x[0]-par[1]-par[2]*par[2]/par[6])/1.414/par[2]) - exp(-(x[0]-par[1]-par[2]*par[2]/2/par[7])/par[7])*TMath::Erfc((x[0]-par[1]-par[2]*par[2]/par[7])/1.414/par[2]));

	return term1 + term2;	
}


MTConvertCSI::MTConvertCSI( Int_t userFlag, Int_t BremsstrahlungFlag ) : MTConvert125MHzDetector( MTBP::CSI, userFlag )
{

 
  if(m_userFlag>=20190101){	
	    DetectorNCHout = Ncry+Nsum;
	    for (UInt_t index = 0; index < Ncry + Nsum; index++) {
			IDIndexMap.insert(std::pair< Int_t, unsigned int>(index, index));
			IndexIDArray[index] = index;
	    } 

		for (int i = 0; i < Ncry; i++) {
			par_ztMPPC[i] = new double[Nparzt];
			par_ztPMT[i] = new double[Nparzt];
			par_LYMPPC[i] = new double[4];
			par_WfmMPPC[i] = new double[NparWfmMPPC*NbinzWfm];
		}
	
	
		std::string str_externaldata = std::getenv("E14ANA_EXTERNAL_DATA");
		std::string sumidaslocid_file;
		
		if(userFlag<=20190101){	
			sumidaslocid_file = str_externaldata + "/UTCsumData/config/sumIDaslocID.txt";
		}else if(20190101<=userFlag){
			sumidaslocid_file = str_externaldata + "/UTCsumData/config/sumIDaslocID_run79.txt";
		}else{
			std::cout << "[UTCconfig]: Warning! invalid runID is specified. Default run79 settings is used\n";
			sumidaslocid_file = str_externaldata + "/UTCsumData/config/sumIDaslocID_run79.txt";
		}
		
		std::ifstream fin(sumidaslocid_file.c_str());
		for (int i = 0; i < Ncry; i++) {
			int modID, sumID;
			fin >> modID >> sumID;
			sumIndex_as_modID[modID] = sumID;
		}
		fin.close();
//
// for OSG, fake MPPC data in this section
//


/*	
		std::string str_csilib = str_externaldata;
		str_csilib += "/UTCsumData/cosmicrayData/csiDB_v3.0.root";
		TFile* rfile = new TFile(str_csilib.c_str());
		if (rfile == NULL) {
			std::cout << "Error! csiDB root file was not found!\n";
			exit(0);
		}
		TTree* tr = (TTree*)rfile->Get("csiDB");
*/	
		static const int NparWfmMPPC = 7;

		double bufparWfmMPPC[36][7] = {};
		double bufparPMT[Nparzt] = {};
		double bufparMPPC[Nparzt] = {};
		double bufpartDz[Nparzt] = {};
		double bufMPPCADCoffset = {};
		double bufMPPCfVop = {};
		double bufMPPCLYpar[4] = {};
/*
		tr->SetBranchAddress("PMT.zt.par", bufparPMT);
		tr->SetBranchAddress("MPPC.zt.par", bufparMPPC);
		tr->SetBranchAddress("MPPC.E2G.par", bufparWfmMPPC);
		tr->SetBranchAddress("MPPC.fVop", &bufMPPCfVop);
		tr->SetBranchAddress("MPPC.LY.par", bufMPPCLYpar);
		tr->SetBranchAddress("MPPC.ADC.t_ofs2", &bufMPPCADCoffset);
*/	
	
	
	
		int isData[Ncry];
		std::vector<int> list_exist_s;
		std::vector<int> list_exist_l;
		for (int i = 0; i < Ncry; i++) {
		//	tr->GetEntry(i);

// for OSG, always isvalid = 0
//
			int isvalid = 0;

/*
			if(    i==2251 || i==2240 || i==2252 || i==2267 || i==2386 || i==2395 // MPPC not attached
				|| i==2406 || i==2417 || i==2538 || i==2549 || i==2560 || i==2569 // MPPC not attached
				|| i==2688 || i==2703 || i==2704 || i==2715 // MPPC not attached
				|| i==27   || i==190 || i==191 || i==2000 || i==2334 || i==2359 || i==2550 // lost chs.
				) isvalid = 0;

	
	
			if(fabs(bufparWfmMPPC[0][0])<1e-5){
				if(isvalid==1) std::cout << "Warning! MPPC Waveform data is not filled for locID = " << i << "\n";
				isvalid = 0;
			}
*/
	
			for(int j=0;j<NbinzWfm;j++){
				bufparWfmMPPC[7*j][0] *= 8.0;
				bufparWfmMPPC[7*j][1] *= 8.0;
				bufparWfmMPPC[7*j][4] *= 8.0;
				bufparWfmMPPC[7*j][5] *= 8.0;
				bufparWfmMPPC[7*j][6] *= 8.0;
			}
	
			if (isvalid == 0){
				for (int j = 0; j < Nparzt; j++) {
					par_ztMPPC[i][j] = -1000;
					par_ztPMT[i][j] = -1000;
					par_ztPMT[i][j] = -1000;
				}
				
				par_LYMPPC[i][0] = -1000;
				par_LYMPPC[i][1] = -1000;
				par_LYMPPC[i][2] = -1000;
				par_LYMPPC[i][3] = -1000;
				
				isData[i] = 0;
			}else{
				for (int j = 0; j < Nparzt; j++) {
					par_ztMPPC[i][j] = bufparMPPC[j];
					par_ztPMT[i][j] = bufparPMT[j];
				}
	
				for (int j = 0; j < NparWfmMPPC; j++) {
					for (int k = 0; k < NbinzWfm; k++) {
						par_WfmMPPC[i][j*NbinzWfm+k] = bufparWfmMPPC[7*k][j];
					}
				}
				for (int j = 0; j < 4; j++) par_LYMPPC[i][j] = bufMPPCLYpar[j];
				
				if(fabs(bufMPPCfVop)<1e-7) fVop[i] = 1;
				else fVop[i] = bufMPPCfVop;
			
	
				isData[i] = 1;
				if (i < 2240)list_exist_s.push_back(i);
				else list_exist_l.push_back(i);
			
				MPPCtADCoffset[i] = bufMPPCADCoffset;
			}
	
	  
		}
	
//		delete tr;
//		rfile->Close();

	
		TRandom3 ran(1);
/*
		for (int i = 0; i < Ncry; i++) {

			if (isData[i] == 1)continue;
			int ind;
			if (i < 2240) ind = list_exist_s[list_exist_s.size()*ran.Rndm()];
			else ind = list_exist_l[list_exist_l.size()*ran.Rndm()];
			for (int j = 0; j < Nparzt; j++) {
				par_ztMPPC[i][j] = par_ztMPPC[ind][j];
				par_ztPMT[i][j] = par_ztPMT[ind][j];
			}
			for (int j = 0; j < NparWfmMPPC; j++) {
				for (int k = 0; k < NbinzWfm ; k++) {
					par_WfmMPPC[i][j*NbinzWfm+k] = par_WfmMPPC[ind][j*NbinzWfm+k];
				}
			}
	
			for (int j = 0; j < 4; j++) par_LYMPPC[i][j] = par_LYMPPC[ind][j];
			
			fVop[i] = fVop[ind];
			MPPCtADCoffset[i] = MPPCtADCoffset[ind];
	
		}
*/
		MPPCFunc = new TF1("MPPCFunc",	myfunc, -1000, 1000, 8);
	
		double Ncount[Nsum];
		double Ncount2[Nsum*4];
		memset(Ncount, 0, sizeof(Ncount));
		memset(Ncount2, 0, sizeof(Ncount2));
		for (int i = 0; i < Nsum; i++){
		   	sumtoffset[i] = 0.0;
			Ncount[i] = 0.0;
			for(int j=0;j<4;j++){
		   		MPPCtoffset[4*i+j] = 0.0;
				Ncount2[4*i+j] = 0.0;
			}
		}
	
		for (int i = 0; i < Ncry; i++){
			
			// def of zero is determined by t(z=-250 mm)
			int indsum = sumIndex_as_modID[i];
			if (indsum < 0)continue;
			
		
			double offsetADC = MPPCtADCoffset[i];
			double offset = par_ztMPPC[i][0] + par_ztMPPC[i][1] * (-250) - offsetADC;
			double LY = GetRelativeLYMPPC(i, -250);
				
			if(fabs(LY)<1e-4 || isnan(LY) || LY<0){  
				std::cout << "LY data is not stored for i="  << i << "\n"; 
			}
			MPPCtoffset[indsum] += offset;
			Ncount2[indsum] += 1;
	
			indsum /= 4;
			sumtoffset[indsum] += offset*LY;
			Ncount[indsum] += LY;
		}
	
		for (int i=0;i<Nsum;i++){
			if (Ncount[i] == 0){
			   	sumtoffset[i] = -1000;
			}else{
			   	sumtoffset[i] /= Ncount[i];
			}
		}
	
		for(int i=0;i<Nsum*4;i++){
			if (Ncount2[i] == 0){
				MPPCtoffset[i] = -1000;
			}else{
				MPPCtoffset[i] /= Ncount2[i];
			}
		}
	
/*	  
		for(int i=0;i<Ncry;i++){
			for(int j=0;j<NbinzWfm;j++){
	
				MPPCFunc->SetParameter(0, 1);
				MPPCFunc->SetParameter(1, par_WfmMPPC[i][NbinzWfm*0+j]);
				MPPCFunc->SetParameter(2, par_WfmMPPC[i][NbinzWfm*1+j]);
				MPPCFunc->SetParameter(3, par_WfmMPPC[i][NbinzWfm*2+j]);
				MPPCFunc->SetParameter(4, par_WfmMPPC[i][NbinzWfm*3+j]);
				MPPCFunc->SetParameter(5, par_WfmMPPC[i][NbinzWfm*4+j]);
				MPPCFunc->SetParameter(6, par_WfmMPPC[i][NbinzWfm*5+j]);
				MPPCFunc->SetParameter(7, par_WfmMPPC[i][NbinzWfm*6+j]);
		
				double xp, yp;
				int ret = solv_peak(0, 400, xp, yp, MPPCFunc);
				if(ret<0){
					std::cout << "failed to calc. peak for MPPC (locID=" << i << ", posID=" <<  j << ")\n";
					offset_Wfm[i][j] = 0;
					continue;
				}
				double xcft;
				ret = solv_CFT(0, xp, yp/2, xcft, MPPCFunc);
				
				if(ret<0){
					std::cout << "failed to calc. cft for MPPC (locID=" << i << "), posID=" <<  j << "\n";
					offset_Wfm[i][j] = 0;
					continue;
				}
	
				double chk1 = MPPCFunc->Eval(-200);
				double chk2 = MPPCFunc->Eval(0);
				double chk3 = MPPCFunc->Eval(700);
	
				if(isnan(chk1*chk2*chk3)){
					std::cout << "failed to eval for MPPCFunc (locID=" << i << "), posID=" <<  j << "\n";
					continue;
				}
	
				if(yp<0){
					std::cout << "Waveform peak height became negative at locID = " << i << ", posID=" << j << "\n";
					std::cout << "peak=" << yp << "\n";
					std::cout << "f(-200)=" << chk1 << ", f(0)=" << chk2 << ", f(700)=" << chk3 << "\n";
					std::cout << "tcft=" << xcft << "\n";
				}
				offset_Wfm[i][j] = xcft;
			}
		}
*/	
	   	sumADCtopeakratio = 0.0632;
	    ampoffset_grad = -3.68272e-005;
  }else{
	  for( UInt_t index = 0 ; index < (UInt_t)DetectorNCH; index++ ){
		  IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index ) );
		  IndexIDArray[index] = index;
	  }
	  DetectorNCHout = Ncry;
  }


  m_detname="CSI";

  pulseGenerator       = new MTPulseGenerator();
  groundNoise          = new double[DetectorNCH]();
  temporalGroundNoise  = new double[DetectorNCH]();
  misCalibrationFactor = new double[DetectorNCH]();
  lightYield           = new double[DetectorNCH][20]();
  meanLightYield       = new double[DetectorNCH]();

  pHandler = MTPositionHandler::GetInstance();
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  WfmTimeShift       = MTBP::CSIWfmTimeShift;
  WfmSigma0          = MTBP::CSIWfmSigma0;
  WfmAsymPar         = MTBP::CSIWfmAsymPar;

  Initialize();
  LoadGroundNoise();// for GetFromMTimeWithResponse
  LoadMisCalibrationFactor( BremsstrahlungFlag );
  LoadLightYield();
}

MTConvertCSI::~MTConvertCSI()
{
  delete   pulseGenerator;
  delete[] misCalibrationFactor;
  delete[] groundNoise;
  delete[] temporalGroundNoise;
  delete[] lightYield;
  delete[] meanLightYield;
  delete   m_E14BP;

  if(m_userFlag>=20190101){	
	delete MPPCFunc;

	for (int i = 0; i < Ncry; i++) {
		delete[] par_ztMPPC[i];
		delete[] par_ztPMT[i];
		delete[] par_LYMPPC[i];
		delete[] par_WfmMPPC[i];
	}
  }
}


void MTConvertCSI::Initialize()
{
  DetNumber     = 0;
  DetTrueNumber = 0;
  for( int ich = 0; ich < DetectorNCHout ; ich++ ){
    DetModID[ich]    = 0;
    DetEne[ich]      = 0;
    DetTime[ich]     = 0;
    DetPTime[ich]    = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0;
    
    DetTrueModID[ich] = 0;
    DetTrueEne[ich]   = 0;
    DetTrueTime[ich]  = 0;

    temporalGroundNoise[ich] = 0;
  }
  for( Int_t iSample = 0 ; iSample < MTBP::nSample125 ; iSample++ )
    DetEtSumWfm[iSample] = 0;

  m_CDTNum = -1;
}


void MTConvertCSI::GetFromMTime(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  number = 0;

  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedVec.clear();
    propagatedClusteredVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;

      // Attenuation and propagation is applied for each mtime entry
      energyTime.energy = mtime->energy; // no attenuation
      energyTime.time   = GetVisibleTime( mtime->r.z(), mtime->time, mtime->modID<2240 );
      propagatedVec.push_back( energyTime );
    }

    // If there is no hit before 1us, it continues.
    if( propagatedVec.size() == 0 ) continue;
    
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );

    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    modID[number] = digi->modID;
    ene[number]   = pulseGenerator->GetPseudoIntegratedEnergy();
    time[number]  = pulseGenerator->GetPseudoConstantFractionTime() / 8.;

    number++;
  }  
}


void MTConvertCSI::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{

  static const double s_CSIZPosition = DetZPosition;

  number = 0;

  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedVec.clear();
    propagatedClusteredVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      

      // Attenuation and propagation is applied for each mtime entry
      //energyTime.energy = mtime->energy; // no attenuation
      // with non uniformity
      int posID = (int)((s_CSIZPosition + MTBP::CSILength - mtime->r.z() ) / 25.);
      if( posID==20 ) posID--;//modified at 28th May, 2014
      energyTime.energy = mtime->energy * lightYield[mtime->modID][posID] / meanLightYield[mtime->modID]; // attenuated energy
      energyTime.time   = GetVisibleTime( mtime->r.z(), mtime->time, mtime->modID<2240 );
      propagatedVec.push_back( energyTime );
    } // imtime
    
    // If there is no hit before 1us, it continues.
    if( propagatedVec.size() == 0 ) continue;
    
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );

    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    modID[number] = digi->modID;
    ene[number]   = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[number]  = (float)pulseGenerator->GetPseudoConstantFractionTime(); // [ns]

    // Light yield smearing. If smearing result is less than 0.1 MeV, this channel is neglected.
    //ene[number] = gRandom->Poisson( ene[number]*MTBP::CSILightYield ) / MTBP::CSILightYield;
    ene[number] *= (float)(meanLightYield[digi->modID] * MTBP::CSILightYield); // light yield
    ene[number] =  (float)gRandom->Poisson( ene[number] ); // smeared light yield
    ene[number] /= (float)(meanLightYield[digi->modID] * MTBP::CSILightYield); // smeared energy
    
    // Add ground noise fractuation
    temporalGroundNoise[digi->modID] = gRandom->Gaus( 0, groundNoise[digi->modID] );
    ene[number] += (float)temporalGroundNoise[digi->modID];

    // Mis calibration effect.
    ene[number] *= (float)misCalibrationFactor[digi->modID];
    
    // Time smearing with resolution
    time[number] = GetSmearedTime( ene[number], time[number] );

    // Large - Small time difference correction
    if( modID[number] > 2239 ){
      time[number] += 0.2094;
    }

    // T-Q correction (not fixed yet)
    time[number] = GetTQCorrectedTime( ene[number], time[number] );

    // [ns] -> [clock]
    time[number] /= 8.;


    number++;
  }
  
}

//updated by Maeda Yosuke at 4th May, 2014
void MTConvertCSI::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, 
					       Float_t* PTime, Float_t* FallTime )
{
  static const double s_CSIZPosition = DetZPosition;

  if( m_onlineClusSimulator ) m_onlineClusSimulator->Reset();

  number = MTBP::CSINChannels;
  for( Int_t iCh=0 ; iCh<number ; iCh++ ) modID[iCh] = IndexIDArray[iCh];
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();
  
  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;
  
  std::vector<EnergyTimePos> propageted_sumVec[Ncry];
  
  //always use this value, added at 14th Feb, 2014 by Maeda Yosuke
  WfmFunc->SetParameter( 2, WfmSigma0 );
  WfmFunc->SetParameter( 3, WfmAsymPar );
  
  EtSumOnlineTrig = 0;//added at 19th Jul, 2014

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedVec.clear();
    propagatedClusteredVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      // Attenuation and propagation is applied for each mtime entry
      int posID = (int)((s_CSIZPosition + MTBP::CSILength - mtime->r.z() ) / 25.);
      if( posID==20 ) posID--;//modified at 28th May, 2014
      energyTime.energy = mtime->energy * lightYield[mtime->modID][posID] / meanLightYield[mtime->modID]; // attenuated energy
      energyTime.time   = GetVisibleTime( mtime->r.z(), mtime->time, mtime->modID<2240 );
      propagatedVec.push_back( energyTime );



	  // MPPC 

	  if (mtime->modID >= 2716) {
		  continue;
	  }

  
	  if(m_userFlag<20190101) continue;

	  EnergyTimePos etp;
	  etp.energy = mtime->energy * GetRelativeLYMPPC(mtime->modID, mtime->r.z()-6148-250);
	  etp.energy = 1.;
	  etp.pos[0] = mtime->r.x();
	  etp.pos[1] = mtime->r.y();
	  etp.pos[2] = mtime->r.z();
	  etp.time = 0.;
          GetVisibleTimeMPPC(mtime->r.z(), mtime->time, mtime->modID);
	  if(etp.time<-9000)continue;
	  propageted_sumVec[mtime->modID].push_back(etp);


    } // imtime
    
    // If there is no hit before 1us, it continues.
    if( propagatedVec.size() == 0 ) continue;
    
    // mtime clustering
    const Double_t k_Pitch = (m_dbWfmGenerator) ? 8. : 0.1;
    ClusteringMtime( propagatedVec, propagatedClusteredVec, k_Pitch );
    
    Int_t iCh = IDIndexMap[digi->modID];
    Float_t EnergyForTimingSmearing = 0;
    for( std::vector<pEnergyTime>::iterator it = propagatedClusteredVec.begin()
	   ; it!=propagatedClusteredVec.end() ; it++ ){
      it->energy *= (Float_t)(meanLightYield[digi->modID] * MTBP::CSILightYield); // light yield
      it->energy  = (Float_t)gRandom->Poisson( it->energy ); // smeared light yield
      it->energy /= (Float_t)(meanLightYield[digi->modID] * MTBP::CSILightYield); // smeared energy
      
      // Mis calibration effect.
      it->energy *= (Float_t)misCalibrationFactor[digi->modID];

      // Large - Small time difference correction
      if( digi->modID > 2239 ) it->time += 0.2094;//[ns]
      
      if( it->time<130 ) EnergyForTimingSmearing += it->energy; //modified at 19th Jul, 2014
      //if( it->time<100 ) EnergyForTimingSmearing += it->time;      
    }

    EtSumOnlineTrig += EnergyForTimingSmearing;

    // Time smearing with resolution
    Float_t TimeSmearing = GetSmearedTime( EnergyForTimingSmearing, 0 );
    for( std::vector<pEnergyTime>::iterator it = propagatedClusteredVec.begin()
	   ; it!=propagatedClusteredVec.end() ; it++ ){
      it->time += TimeSmearing;
    }    
    // T-Q correction (not fixed yet) ; not applied in waveform simulation
    //time[number] = GetTQCorrectedTime( ene[number], time[number] );
    
    // Add ground noise fractuation : not applied in waveform simulation
    //temporalGroundNoise[digi->modID] = gRandom->Gaus( 0, groundNoise[digi->modID] );
    //ene[number] += (float)temporalGroundNoise[digi->modID];
    
    if( m_dbWfmGenerator )
       GenerateDbWfmFromMTimeVec( iCh, propagatedClusteredVec );
    else
       GenerateWfmFromMTimeVec( iCh, propagatedClusteredVec );
    
    if( !EnableAccidentalOverlay ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
      if( m_onlineClusSimulator ) FillClusterBit( iCh, &DetWfm[iCh][0] );
    }
    int Nanflag=0;
    if( isnan(ene[iCh]) || isnan(time[iCh]) ){
      //std::cout<<" In CsI response func Nan "<<std::endl;
    } 
    else if( isinf(ene[iCh]) || isinf(time[iCh]) ){
      //std::cout<<" In CsI response func ICH "<<std::endl;
    }
    
  }


  if(m_userFlag>=20190101){	

  
	std::vector<EnergyTimePos> propageted_Clustered_sumVec;
	std::vector<pEnergyTime>   propageted_Clustered_sumVec_ET;

	// ---------------- old implementation  -------------
	const double wfm_parMPPC[2] = { 31.7, 0.187 };
	WfmFunc->SetParameter(2, wfm_parMPPC[0]);
	WfmFunc->SetParameter(3, wfm_parMPPC[1]);
	//---------------------------------------------------

	int isfilled[Nsum];
	memset(isfilled, 0, sizeof(isfilled));
	for (int i = 0; i < Ncry; i++) {
		if(propageted_sumVec[i].size() == 0)continue;

		int indsum = sumIndex_as_modID[i];
		if (indsum < 0) {
			continue;
		}


// for calibration runs
//#define CALIBS3
//#define CALIBL3


#ifdef CALIBS0
		if(indsum<560 && indsum%4!=0)continue;
#endif
#ifdef CALIBS1
		if(indsum<560 && indsum%4!=1)continue;
#endif
#ifdef CALIBS2
		if(indsum<560 && indsum%4!=2)continue;
#endif
#ifdef CALIBS3
		if(indsum<560 && indsum%4!=3)continue;
#endif

#ifdef CALIBL0
		if(indsum>=560 && indsum%4!=0)continue;
#endif
#ifdef CALIBL1
		if(indsum>=560 && indsum%4!=1)continue;
#endif
#ifdef CALIBL2
		if(indsum>=560 && indsum%4!=2)continue;
#endif
#ifdef CALIBL3
		if(indsum>=560 && indsum%4!=3)continue;
#endif

		indsum /= 4;

		propageted_Clustered_sumVec.clear();
		ClusteringMtime(propageted_sumVec[i], propageted_Clustered_sumVec);
	
		for(int j=0;j<propageted_Clustered_sumVec.size();j++){
			energyTime.energy = propageted_Clustered_sumVec[j].energy;
			energyTime.time   = propageted_Clustered_sumVec[j].time;
		
			double posz  = propageted_Clustered_sumVec[j].pos[2];
			int iz = NbinzWfm*(posz-6148)/500.0;
	
			if(iz<0 || iz>=NbinzWfm) continue;

			MPPCFunc->SetParameter(1, par_WfmMPPC[i][NbinzWfm*0+iz]);
			MPPCFunc->SetParameter(2, par_WfmMPPC[i][NbinzWfm*1+iz]);
			MPPCFunc->SetParameter(3, par_WfmMPPC[i][NbinzWfm*2+iz]);
			MPPCFunc->SetParameter(4, par_WfmMPPC[i][NbinzWfm*3+iz]);
			MPPCFunc->SetParameter(5, par_WfmMPPC[i][NbinzWfm*4+iz]);
			MPPCFunc->SetParameter(6, par_WfmMPPC[i][NbinzWfm*5+iz]);
			MPPCFunc->SetParameter(7, par_WfmMPPC[i][NbinzWfm*6+iz]);
		
			propageted_Clustered_sumVec_ET.clear();
			propageted_Clustered_sumVec_ET.push_back(energyTime);

			// This value is used to adjust the consistency of time between MPPC and PMT.
			// For the data, MPPC t0 is adjusted such that peak of T_MPPC-T_PMT becomes 30ns for gamma.
			// For MC, this relative timing is shifted here. 
			const double relative_ToffsetMPPC = 10.09;

			GenerateWfmFromMTimeVec(indsum + Ncry, propageted_Clustered_sumVec_ET, 
					par_WfmMPPC[i][NbinzWfm*0+iz]-offset_Wfm[i][iz] + relative_ToffsetMPPC, MPPCFunc); // new
		}
		
	
		isfilled[indsum]++;
	}



	if (!EnableAccidentalOverlay){
		for (int indsum = 0; indsum < Nsum; indsum++) {
			if(isfilled[indsum]==0)continue;
			GetEnergyTimeFromWfm(&DetWfm[indsum + Ncry][0], ene[indsum + Ncry], time[indsum + Ncry],
				PTime[indsum + Ncry], FallTime[indsum + Ncry], indsum + Ncry);

		}
	}



  }


  if( !EnableAccidentalOverlay ) m_CDTNum = GetOnlineClusterNumber();

  

}

void MTConvertCSI::ApplyUTCAmpEffect(){

	for (int indsum = 0; indsum < Nsum; indsum++) {
	    // AMP saturation effect	
		double peakADCcount = DetEne[indsum+Ncry]/28.0*factor_LYADCtypical*sumADCtopeakratio;
		double Toffset_amp = ampoffset_grad*peakADCcount;
		DetTime[indsum+Ncry]     += Toffset_amp;
		DetPTime[indsum+Ncry]    += Toffset_amp;
		DetFallTime[indsum+Ncry] += Toffset_amp;
	}
}

void MTConvertCSI::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  number = 0;
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    modID[number] = digi->modID;
    ene[number]   = (float)digi->energy;
    time[number]  = (float)digi->time / 8.; // [ns]->[clock]
    number++;
  }
}

double MTConvertCSI::GetTfromZ(double z, double* par) {

	if (z <= par[4]) return par[0] + par[1] * (z+250);
	else return par[0] + par[3] + par[1]*(par[4]+250) + par[2]*(z - par[4]);
}



double MTConvertCSI::GetVisibleTime( double z, double t, bool sl )
{
  static const double s_CSIZPosition = DetZPosition;
  double propagationLength = s_CSIZPosition + MTBP::CSILength - z;

  if( sl ){
    //small
    return t + propagationLength / MTBP::CSISPropVelo;
  }else{
    //large
    return t + propagationLength / MTBP::CSILPropVelo;
  }
}

double MTConvertCSI::GetVisibleTimeMPPC(double z, double t, int modID)
{

	double z_incry = z - MTBP::CSIZPosition - MTBP::CSILength / 2;

	if (z_incry<-MTBP::CSILength / 2 || z_incry>MTBP::CSILength / 2) return -10000;

	int indsum = sumIndex_as_modID[modID];
	if (indsum < 0) return -10000;

	int indhybrid = indsum;
	double offsetADC = MPPCtADCoffset[modID];

	indsum /= 4;

	double* par = par_ztMPPC[modID];

//#define IDEAL_T0calib
#ifdef IDEAL_T0calib
	return  t + GetTfromZ(z_incry, par) - MPPCtoffset[indhybrid] - offsetADC;
#else
	return  t + GetTfromZ(z_incry, par) - sumtoffset[indsum] - offsetADC;
#endif

}

bool MTConvertCSI::LoadGroundNoise()
{
  int id=0;
  double noise = 0;
  double factor = 0;

  // Load noise
  //relative path since 18th May, 2014
  //use env. var. since 29th May, 2014
  std::string filename = std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
    + std::string("/gsim2dst/CSI/obsolete/CSINoise_run16860.txt");
  if( access( filename.c_str(), R_OK) != 0){
    std::cout << filename << " is not found." << std::endl;
    std::exit( 1 );
  }
  
  std::ifstream ifs( filename.c_str() );
  while( ifs >> id >> noise ){
    groundNoise[id] = noise;
  }
  ifs.close();
  
  // Load 3pi0 calibration factor
  //relative path since 18th May, 2014
  //use env. var. since 29th May, 2014
  std::string filename1 = std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
    + std::string("/gsim2dst/CSI/obsolete/CalibrationFactorADV_10.dat");
  if( access( filename1.c_str(), R_OK) != 0){
    std::cout << filename1 << " is not found." << std::endl;
    exit(1);
  }
  std::ifstream ifs1( filename1.c_str() );
  while( ifs1 >> id >> factor ){
    groundNoise[id] *= factor;
  }
  ifs1.close();


  // Absolute correction factor
  for( int ich=0; ich<DetectorNCH; ich++){
    groundNoise[ich] *= MTBP::CSIAbsoluteECoeff;
  }

  return true;
}


bool MTConvertCSI::LoadMisCalibrationFactor( Int_t BremsFlag )
{
  int id = 0;
  double factor = 0;
  double absolute_factor=1.00674; // Seltzer-Berger brems. model (default)

  std::string filename = std::string( std::getenv("E14ANA_EXTERNAL_DATA") );
  std::string file_AbsoluteFactor = std::string( std::getenv("E14ANA_EXTERNAL_DATA") );
  if( BremsFlag==0 ){           // Seltzer-Berger brems. model (default)
    filename            += std::string("/gsim2dst/CSI/CalibrationFactorADV_MC_CosmicCalib_9.dat");
    file_AbsoluteFactor += std::string("/gsim2dst/CSI/CalibrationFactorADV_MC_CosmicCalib_9_AbsoluteFactor.dat");
  }else if( BremsFlag==1 ){     // eBrems model from K.Shiomi (2018-10-26)
    filename += std::string("/gsim2dst/CSI/CalibrationFactorADV_MC_eBrems_CosmicCalib_9.dat");
    file_AbsoluteFactor += std::string("/gsim2dst/CSI/CalibrationFactorADV_MC_eBrems_CosmicCalib_9_AbsoluteFactor.dat");
  }else{                        // Seltzer-Berger brems. model (default)
    filename += std::string("/gsim2dst/CSI/CalibrationFactorADV_MC_CosmicCalib_9.dat");
    file_AbsoluteFactor += std::string("/gsim2dst/CSI/CalibrationFactorADV_MC_CosmicCalib_9_AbsoluteFactor.dat");
  }

  std::string fileEdgeCh = std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
    + std::string("/gsim2dst/CSI/edgeregion.dat");

  if( access( filename.c_str(), R_OK ) != 0
      || access( file_AbsoluteFactor.c_str(), R_OK ) != 0
      || access( fileEdgeCh.c_str(), R_OK ) != 0 ){
    std::cerr << filename << " or " 
	      << file_AbsoluteFactor << " or " 
	      << fileEdgeCh
	      << " is not found." << std::endl;
    exit(1);
  }

  std::ifstream ifs_AbsFac( file_AbsoluteFactor.c_str() );
  ifs_AbsFac >> absolute_factor;
  ifs_AbsFac.close();

  std::ifstream ifs( filename.c_str() );
  while( ifs >> id >> factor ){
    //misCalibrationFactor[id] = factor*1.00437953; // Absolute factor correction (2013.5.9)
    misCalibrationFactor[id] = factor*absolute_factor;
    //misCalibrationFactor[id] = factor; // Absolute factor correction   1+gRandom->Gaus(0,0.03286)
  }
  Int_t regionflag;
  std::ifstream ifs_edge( fileEdgeCh.c_str() );
  while( ifs_edge >> id >> regionflag ){
    //if( regionflag==1 ) misCalibrationFactor[id] = 1*1.00437953;
  }
  ifs_edge.close();
  ifs.close();

  return true;
}

double MTConvertCSI::GetRelativeLYMPPC( int modID, double z){

	double p0 = par_LYMPPC[modID][0];
	double p1 = par_LYMPPC[modID][1];
	double p2 = par_LYMPPC[modID][2];
	double p3 = par_LYMPPC[modID][3];

	double gainfactor = 1.0/fVop[modID];

	return gainfactor*(p0 + p1*atan(p2*(z-p3)))/factor_LYADCtypical;
}

bool MTConvertCSI::LoadLightYield()
{
  int id = 0;

  std::string filename = std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
    + std::string("/gsim2dst/CSI/CSIUniformity.txt");
  if( access( filename.c_str(), R_OK) != 0 ){
    std::cout << filename << " is not found." << std::endl;
    exit(1);
  }

  std::ifstream ifs( filename.c_str() );

  for( int ich=0; ich<DetectorNCH; ich++ ){
    ifs >> id;
    meanLightYield[id] = 0;

    for( int ipos=0; ipos<20; ipos++ ){
      ifs >> lightYield[id][ipos];
      meanLightYield[id] += lightYield[id][ipos] / 20.;
    }
  }

  return true;//added at 15th May, 2014 
}

///// old method
void  MTConvertCSI::AddAccidental(){
  AddAccidental( DetNumber, DetModID, DetEne, DetTime );}
///// old method
void  MTConvertCSI::AddAccidental( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  if( !EnableAccidentalOverlay ) return;

  // If simulation hits are not above online threshold, accidental overlay is not used.
  if( !OnlineTrigger() ) return; 

  //  AccidentalTree->GetEntry( AccidentalEntryID );
  
  pHitInfo hitInfo = { 0, 0, 0};
  std::vector< pHitInfo > hitInfoVec;

  int simIndex=0, accIndex=0;
  bool finishFlags[2] = { false, false};
  
  for( int ich=0; ich< DetectorNCH ; ich++ ){
    
    if( modID[simIndex]!=ich && AccidentalDetModID[accIndex]!=ich ){
      // This channel has no sim hit and accidental hit.
      continue;
    }else if( modID[simIndex]==ich && AccidentalDetModID[accIndex]!=ich ){
      // This channel has sim hit and does not have accidental hit.
      hitInfo.modID  = ich;
      hitInfo.energy = ene[simIndex];
      hitInfo.time   = time[simIndex]; // [clock]
      simIndex++;
    }else if( modID[simIndex]!=ich && AccidentalDetModID[accIndex]==ich ){
      // This channel does not have sim hit and has accidental hit.
      hitInfo.modID = ich;
      hitInfo.energy   = AccidentalDetEne[accIndex] * MTBP::CSIAbsoluteECoeff;
      hitInfo.time  = AccidentalDetTime[accIndex] + AccidentalTimeShift; // [clock]
      accIndex++;
    }else{
      // This channel has sim hit and accidental hit.
      hitInfo.modID = ich;
      hitInfo.energy   = ene[simIndex] - temporalGroundNoise[ich] + AccidentalDetEne[accIndex] * MTBP::CSIAbsoluteECoeff;
      if( ene[simIndex] >= AccidentalDetEne[accIndex]*MTBP::CSIAbsoluteECoeff ){
	hitInfo.time = time[simIndex]; // [clock]
      }else{
	hitInfo.time = AccidentalDetTime[accIndex] + AccidentalTimeShift; // [clock]
      }
      simIndex++;
      accIndex++;
    }

    // energy threshold
    //if( hitInfo.energy > 0.5 ){
      hitInfoVec.push_back( hitInfo );
      //}

    if( simIndex==number ){
      finishFlags[0] = true;
      simIndex--;
    }
    if( accIndex==AccidentalDetNumber ){
      finishFlags[1] = true;
      accIndex--;
    }

    if( finishFlags[0] && finishFlags[1] ){
      break;
    }
  }
  // Refill
  number = hitInfoVec.size();
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = hitInfoVec[ich].modID;
    ene[ich]   = hitInfoVec[ich].energy;
    time[ich]  = hitInfoVec[ich].time;
  }
}

float MTConvertCSI::GetSmearedTime( float e, float t ) const
{
  return t + gRandom->Gaus( 0, sqrt( pow((float)MTBP::CSITimeResolutionCoeff[0]*2/e, 2) + pow((float)MTBP::CSITimeResolutionCoeff[1]/sqrt(e), 2)/* + pow(0.07791,2)*/ ) );
}


float MTConvertCSI::GetTQCorrectedTime( float e, float t ) const
{
  return t - ( -0.1687*exp(-0.003311*e) - 0.6024*exp(-0.02733*e) - 0.1252*exp(-0.00486*e) + 0.04844 + 0.2089 );
}

bool MTConvertCSI::OnlineTrigger( int runNo )
{
  if( runNo == 46 ){ // January run (default)
    double halfEtR=0.0, halfEtL=0.0;

    for( int imod=0; imod<DetNumber; imod++ ){
      if( -20<DetTime[imod]*8 && DetTime[imod]*8<130 ){
	if( pHandler->GetCSIXPosition( DetModID[imod] ) > 0 ){
	  halfEtR += DetEne[imod];
	}else{
	  halfEtL += DetEne[imod];
	}
      }
    }

    if( halfEtR>MTBP::OnlineThreshold && halfEtL>MTBP::OnlineThreshold ){
      return true;
    }else{
      return false;
    }
  }
  else if( runNo == 49) {//updated at 19th Jul, 2014
    
    return (EtSumOnlineTrig>MTBP::OnlineThreshold);
    /*
    double Et=0.0;

    for( int imod=0; imod<DetNumber; imod++ ){
      if( -20<DetTime[imod]*8 && DetTime[imod]*8<130 ){
	Et+= DetEne[imod];
      }
    }

    if( Et>MTBP::OnlineThreshold){
      return true;
    } else {
      return false;
    }
    */
  }

  return true;
}

int MTConvertCSI::solv_peak(double x0, double x1, double& xp, double& yp, TF1* MPPCFunc){
		
	int c = 0;
	while(1){
		double xlw = (2*x0+x1)/3;
		double xup = (x0+2*x1)/3;

		double ylw = MPPCFunc->Eval(xlw);
		double yup = MPPCFunc->Eval(xup);

		if(ylw<yup){
			x0 = xlw;
		}else{
			x1 = xup;
		}

		if(fabs(xlw-xup)<1e-7 || c>=2000){
			xp = (xlw+xup)/2.0;
			yp = MPPCFunc->Eval(xp);
			break;
		}
		c++;

	}
	if(c==2000)return -1;
	else return 0;
}


int MTConvertCSI::solv_CFT(double x0, double x1, double yhalf, double& xp, TF1* MPPCFunc){

	int c = 0;
		
	while(1){
				
		double xtry = (x0+x1)/2;
		double y0 = MPPCFunc->Eval(x0)-yhalf;
		double y1 = MPPCFunc->Eval(x1)-yhalf;
		double ytry = MPPCFunc->Eval(xtry)-yhalf;

		if(ytry*y0>0) x0 = xtry;
		else if(ytry*y1>0) x1 = xtry;

		if(fabs(x1-x0)<1e-7 || c>=2000){
			xp = (x1+x0)/2.0;
			break;
		}

		c++;
	}

	if(c==2000)return -1;
	else return 0;
}


