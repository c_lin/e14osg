#include "gsim2dst/MTConvertCC03.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <fstream>
#include <sstream>
#include <cstdlib>

MTConvertCC03::MTConvertCC03( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::CC03, userFlag )
{
  m_detname="CC03";
  for( int ich=0; ich<MTBP::DetNChannels[MTBP::CC03]; ich++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( ich, ich ) );
    IndexIDArray[ich] = ich;
  }

  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }
  
  pulseGenerator = new MTPulseGenerator();
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  WfmTimeShift = MTBP::CC03WfmTimeShift;
  WfmSigma0 = MTBP::CC03WfmSigma0;
  WfmAsymPar = MTBP::CC03WfmAsymPar;

  Initialize();
  LoadLightYield();
}

MTConvertCC03::~MTConvertCC03()
{
  delete pulseGenerator;
  delete m_E14BP;
}


void MTConvertCC03::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}


bool MTConvertCC03::LoadLightYield( void )
//slightly update at 18th May, 2014 (by M.Y. ; process itself is not chaged)
{
  //relative path since 18th May, 2014
  //use env. var. since 29th May, 2014
  std::string dirname = std::string( std::getenv("E14ANA_EXTERNAL_DATA") )
    + std::string("/gsim2dst/CC03/Uniformity");
  
  for(int i=0; i<16; i++){
    std::ostringstream ch_filename;
    ch_filename << dirname << "/CC03_Uniformity_ch" << (i*2) << ".dat";
    
    if( access( ch_filename.str().c_str(), R_OK) != 0 ){
      std::cout << ch_filename.str() << " is not found." << std::endl;
      exit(1);
    }
    std::ifstream ifs( ch_filename.str().c_str() );
    int count=0;
    
    int ich,pos;
    double ratio,ratio_e;
    
    while(ifs>>ich>>pos>>ratio>>ratio_e){
      //std::cout<<i<<" "<<count<<" "<<ratio<<std::endl;
      lightYield[i][count]=ratio;
      count+=1;
    }
  }
  
  return true;//added at 14th May, 2014 (M.Y.)
}

void MTConvertCC03::GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    for( int sl=0; sl<2; sl++ ){
      ene[IDIndexMap[digi->modID+sl]]   = (float)digi->energy;
      time[IDIndexMap[digi->modID+sl]]  = (float)digi->time /8.; // [ns]->[clock]
    }
  }

}

void MTConvertCC03::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{

  static const double s_CC03ZPosition = DetZPosition;
  
  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedVec.clear();
    propagatedClusteredVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      // Attenuation and propagation is applied for each mtime entry
      //energyTime.energy = mtime->energy; // no attenuation
      // with non uniformity
      int posID;
      double hitz = mtime->r.z() - s_CC03ZPosition;
      if( hitz< 75) posID=0;
      else if( 75<=hitz && hitz <=425) posID=(int)(hitz-25)/50;
      else posID=8;      
      energyTime.energy = mtime->energy * lightYield[mtime->modID/2][posID]; // attenuated energy      
      energyTime.time   = GetVisibleTime( mtime->r.z(), mtime->time);
      propagatedVec.push_back( energyTime );


      //std::cout<<hitz<<" "<<posID<<" "<<mtime->energy<<" "<<mtime->energy * lightYield[mtime->modID/2][posID] <<std::endl;
      //std::cout<<mtime->time<<" "<<GetVisibleTime( mtime->r.z(), mtime->time)<<std::endl;
      //std::cout<<energyTime.energy/mtime->energy;
      //std::getchar();
      
    } // imtime
    
    // If there is no hit before 1us, it continues.
    if( propagatedVec.size() == 0 ) continue;
    
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );

    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    modID[digi->modID] = digi->modID;
    ene[digi->modID]   = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[digi->modID]  = (float)pulseGenerator->GetPseudoConstantFractionTime(); // [ns]

    modID[digi->modID+1] = digi->modID+1;
    ene[digi->modID+1]   = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[digi->modID+1]  = (float)pulseGenerator->GetPseudoConstantFractionTime(); // [ns]
    
    // Light yield smearing. If smearing result is less than 0.1 MeV, this channel is neglected.
    //ene[number] = gRandom->Poisson( ene[number]*MTBP::CSILightYield ) / MTBP::CSILightYield;
    //ene[number] *= (meanLightYield[digi->modID] * MTBP::CSILightYield); // light yield
    //ene[number] =  gRandom->Poisson( ene[number] ); // smeared light yield
    //ene[number] /= (meanLightYield[digi->modID] * MTBP::CSILightYield); // smeared energy
    
    // Add ground noise fractuation
    //temporalGroundNoise[digi->modID] = gRandom->Gaus( 0, groundNoise[digi->modID] );
    //ene[number] += temporalGroundNoise[digi->modID];

    
    // Time smearing with resolution
    //time[number] = GetSmearedTime( ene[number], time[number] );


    // T-Q correction (not fixed yet)
    //time[number] = GetTQCorrectedTime( ene[number], time[number] );

    // [ns] -> [clock]
    time[digi->modID] /= 8.;
    time[digi->modID+1] /= 8.;
    
  }
  
}

//added at 13th Mar, 2014
void MTConvertCC03::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
//added at 13th Mar, 2014
//updated at 4th May, 2014 (PTime and FallTime added) 
{

  static const double s_CC03ZPosition = DetZPosition;

  number = DetectorNCH;
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  WfmFunc->SetParameter( 2, WfmSigma0 );
  WfmFunc->SetParameter( 3, WfmAsymPar );

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi);
    propagatedVec.clear();
    propagatedClusteredVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      // Attenuation and propagation is applied for each mtime entry
      //energyTime.energy = mtime->energy; // no attenuation
      // with non uniformity
      int posID;
      double hitz = mtime->r.z() - s_CC03ZPosition;
      if( hitz< 75) posID=0;
      else if( 75<=hitz && hitz <=425) posID=(int)(hitz-25)/50;
      else posID=8;      
      energyTime.energy = mtime->energy * lightYield[mtime->modID/2][posID]; // attenuated energy      
      energyTime.time   = GetVisibleTime( mtime->r.z(), mtime->time);
      propagatedVec.push_back( energyTime );
    } // imtime
    
    // If there is no hit before 1us, it continues.
    if( propagatedVec.size() == 0 ) continue;
    
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );
    GenerateWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec );
    GenerateWfmFromMTimeVec( IDIndexMap[digi->modID]+1, propagatedClusteredVec );
  }
  
  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<number; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
      
      //m_temporalPedestalWidth[iCh] = gRandom->Gaus( 0, m_pedestalWidth[iCh] );
      //ene[iCh] += (float)m_temporalPedestalWidth[iCh];
    }
  }

  return;
}



double MTConvertCC03::GetVisibleTime( double z, double t)
{
  static const double s_CC03ZPosition = DetZPosition;
  double propagationLength = s_CC03ZPosition + MTBP::CC03Length - z;
  return t + propagationLength / MTBP::CC03PropVelo;
}


void MTConvertCC03::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
    time[IDIndexMap[digi->modID]]  = (float)digi->time /8.; // [ns]->[clock]
  }
}



