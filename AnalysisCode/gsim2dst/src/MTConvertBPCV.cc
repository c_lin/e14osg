#include "gsim2dst/MTConvertBPCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

 /////////Miyazaki definition/////////////
MTConvertBPCV::MTConvertBPCV( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::BPCV, userFlag )
{
  m_detname="BPCV";
  attflag     = true;  //include attenuation effect=true
  poissonflag = true; //include poisson effect=true

  // BPCV scintillator//
  for( int index=0; index<4; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index) );
    IndexIDArray[index] = index;
  }

  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  WfmTimeShift = MTBP::BPCVWfmTimeShift;
  WfmSigma0 = MTBP::BPCVWfmSigma0;
  WfmAsymPar = MTBP::BPCVWfmAsymPar;

  Initialize();

}

MTConvertBPCV::~MTConvertBPCV()
{
  delete m_E14BP;
}

void MTConvertBPCV::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )//added at 4th May, 2014
      DetWfm[ich][iSample] = 0; 

    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}


void MTConvertBPCV::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ) continue;//ignore no dead materials
    ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
    time[IDIndexMap[digi->modID]]  = (float)digi->time /8.; //[ns]->[clock]
  }
}

void MTConvertBPCV::GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ) continue;//ignore no dead materials
    if(digi->modID==4){
      for( int sl=0; sl<2; sl++ ){
	ene[IDIndexMap[digi->modID]+sl]   = (float)digi->energy;
	time[IDIndexMap[digi->modID]+sl]  = (float)digi->time /8.; // [ns]->[clock]
      }
    }
    else{
      ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
      time[IDIndexMap[digi->modID]]  = (float)digi->time /8.; // [ns]->[clock]
    }    
  }
}

int MTConvertBPCV::Rpois(Double_t lamda){
  Double_t r;
  Int_t irpois=0;
  r=(Double_t)rand()/RAND_MAX;
  lamda = TMath::Exp(lamda)*r;

  while(lamda>1){
    r=(Double_t)rand()/RAND_MAX;
    lamda*=r;
    irpois++;

    if(irpois>10000)
      return 9999;
  }
  return irpois;
}


//////////Miyazaki Koichi Definition///////////////////
/////attenuation is normalized at z = 9903 where I measured Light yield using cosmic-ray///////

void MTConvertBPCV::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime ){

  number = DetectorNCH;


  //use digi information to generate waveform
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  std::vector<Double_t> Vm_time;
  std::vector<Double_t> Vm_energy;
  std::vector<Double_t> Vm_Z;//to calculate propagetion time 
    

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    Int_t ModID = digi->modID;
    Float_t ene_digi = digi->energy;
    Float_t time_digi = digi->time;
    if( IDIndexMap.count( ModID )==0 ) continue;//ignore no dead materials
    propagatedVec.clear();
    propagatedClusteredVec.clear();
    Int_t iCh = IDIndexMap[ModID];
    
    Vm_time.clear();
    Vm_energy.clear();
    Vm_Z.clear();

    Int_t n_mtime=0;
    
    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
      
      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;
      
      Vm_time.push_back(mtime->time);
      Vm_energy.push_back(mtime->energy);
      Vm_Z.push_back(mtime->r.Z());      
      
      Double_t X_mtime= mtime->r.X();
      Double_t Y_mtime= mtime->r.Y();
      Double_t Z_mtime= mtime->r.Z();
      Float_t energy_mtime=mtime->energy;
      Float_t time_mtime=mtime->time;
      Int_t ModID_mtime = mtime->modID;
      
      energyTime.energy= GetEnergy(energy_mtime,Z_mtime,ModID_mtime);
      energyTime.time  = GetTime(time_mtime,Z_mtime);
      propagatedVec.push_back( energyTime );
    }

    // If there is no hit before 1us, it continues.
    if( propagatedVec.size() == 0 ) continue;
    
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );


    WfmFunc->SetParameter( 2, MTBP::BPCVWfmSigma0 );
    WfmFunc->SetParameter( 3, MTBP::BPCVWfmAsymPar );

    GenerateWfmFromMTimeVec( iCh, propagatedClusteredVec);
    
  }

  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<number; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
    }
  }


  return;
}

Float_t MTConvertBPCV::GetEnergy(Float_t energy_mtime, Double_t Z_mtime,Int_t ModID){

  static const double s_BPCVZPosition = DetZPosition;

  Float_t energy;

  const Double_t attenuation_ver = 14.3*TMath::Exp(-(80+50)/368.4)+5.06*TMath::Exp(-(80+50)/60.98);
  const Double_t attenuation_hor = 14.3*TMath::Exp(-(80+30)/368.4)+5.06*TMath::Exp(-(80+30)/60.98);
  const Double_t light_yield[2] ={ 6.2, 5.8}; //6.2p.e/MeV @110cm from PMT,5.8p.e/MeV @130cm from PMT

  Double_t attenuation;     

  energy=energy_mtime;
  
  ///Attenuation effect///
  if(attflag){
    //for ModID==1or3, assumed that total length of fibers are 130cm
    if(ModID==1||ModID==3){
      attenuation=14.3*TMath::Exp(-((Z_mtime/10-s_BPCVZPosition/10)+30)/368.4)+5.06*TMath::Exp(-((Z_mtime/10-s_BPCVZPosition/10)+30)/60.98);
      energy=energy*attenuation/attenuation_hor;
      
    }else{
      //for ModID==0or2, assumed that total length of fibers are 150cm
      attenuation=14.3*TMath::Exp(-((Z_mtime/10-s_BPCVZPosition/10)+50)/368.4)+5.06*TMath::Exp(-((Z_mtime/10-s_BPCVZPosition/10)+50)/60.98);
      energy=energy*attenuation/attenuation_ver;
    }
  }
  //std::cout<<"energy: att:"<<energy_mtime<<" "<<energy<<std::endl;

  if(poissonflag){
    ///calculate poisson effect

    Double_t rnd=(Double_t)rand()/RAND_MAX;;
    double LY;
    double npe;
    
    if(ModID==0 || ModID==2)
      LY=light_yield[0];
    else
      LY=light_yield[1];
    
    npe=Rpois(energy_mtime*LY);


      
    Double_t y_try=0;
    Double_t y_ref=-1;
    Double_t x0;
    Double_t sigma;
    if(npe==0)
      sigma=0.2001;//sigma of the pedestal[unit=#p.e]
    else
      sigma=0.3501*sqrt(npe); //sigma of i[p.e]
    while(y_try>y_ref){
      x0 = ((double)rand()/RAND_MAX-0.5) * 20 * sigma + npe;
      y_try=(double)rand()/RAND_MAX;
      y_ref = TMath::Gaus(x0, npe, sigma);
    }
    energy=x0/LY ;
  }
  
  return energy;
}
    

Float_t MTConvertBPCV::GetTime(Float_t time_mtime, Double_t Z_mtime){
  
  static const double s_BPCVZPosition = DetZPosition;

  Float_t time;
  
  const Double_t propagation_v=173.7;//[mm/nsec] from ToYoDa's Master thesis
  time = time_mtime + (Z_mtime - s_BPCVZPosition + 400)/propagation_v;
  time = gRandom->Gaus( time, MTBP::BPCVSigmaT );//add timing resolution by shiomi 2017/11/30
  
  
  return time;

  //std::cout<<"time:"<<time<<" "<<time_mtime<<std::endl;
}

