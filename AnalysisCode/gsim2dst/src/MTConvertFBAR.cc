#include "gsim2dst/MTConvertFBAR.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include <cmath>


MTConvertFBAR::MTConvertFBAR( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::FBAR, userFlag )
{
   m_detname="FBAR";
  for( unsigned int index = 0; index<(UInt_t)MTBP::DetNChannels[MTBP::FBAR]; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index ) );
    IndexIDArray[index] = index;
  }

  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = ich;
    DetTrueModID[ich] = ich;
  }

  pulseGenerator = new MTPulseGenerator();

  //same waveform parameter w/ CBAR
  WfmTimeShift = MTBP::CBARWfmTimeShift;
  WfmSigma0 = MTBP::CBARWfmSigma0;
  WfmAsymPar = MTBP::CBARWfmAsymPar;

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  Initialize();
  
}


MTConvertFBAR::~MTConvertFBAR()
{
  delete pulseGenerator;
  delete m_E14BP;
}


void MTConvertFBAR::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
      DetWfm[ich][iSample] = 0; 

    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}



void MTConvertFBAR::GetFromMTime( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{

  number = DetectorNCH;

  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;


  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    propagatedVec.clear();
    propagatedClusteredVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // Neglect over 1us hit
      if( mtime->time > 1000 ) continue;

      // Attenuation and propagation is applied for each mtime entry
      energyTime.energy = GetVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetVisibleTime( mtime->r.z(), mtime->time );
      propagatedVec.push_back( energyTime );
    }

    // If there is no hit before 1us, it continues.
    if( propagatedVec.size() == 0 ) continue;

    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec);
    
    pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    ene[digi->modID]  = (float)pulseGenerator->GetPseudoIntegratedEnergy();
    time[digi->modID] = (float)pulseGenerator->GetPseudoConstantFractionTime() /8.; // [ns]->[clock]
  }
}



void MTConvertFBAR::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
{
  
  number = DetectorNCH;
  
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();
  
  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  //always use this value, added at 14th Feb, 2014 by Maeda Yosuke
  WfmFunc->SetParameter( 2, WfmSigma0 );
  WfmFunc->SetParameter( 3, WfmAsymPar );
  
  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    propagatedVec.clear();
    propagatedClusteredVec.clear();

    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );

      // Attenuation and propagation is applied for each mtime entry
      energyTime.energy = GetVisibleEnergy( mtime->r.z(), mtime->energy );
      energyTime.time   = GetVisibleTime( mtime->r.z(), mtime->time );
      propagatedVec.push_back( energyTime );
    }

    // mtime clustering
    const Double_t k_Pitch = (m_dbWfmGenerator) ? 8. : 0.1;
    ClusteringMtime( propagatedVec, propagatedClusteredVec, k_Pitch );

    if(m_dbWfmGenerator)
       GenerateDbWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec );  
    else 
       GenerateWfmFromMTimeVec( IDIndexMap[digi->modID], propagatedClusteredVec );



    // pulse shape
    //pulseGenerator->SetEnergyTime( propagatedClusteredVec );
    //pulseGenerator->GeneratePulseShape();
    //ene[digi->modID]  = (float)pulseGenerator->GetIntegrated();
    //time[digi->modID] = (float)pulseGenerator->GetConstantFractionTime() /8.; // [ns]->[clock]
  }

  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<number; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
      EvalFTTChisq( iCh, &DetWfm[iCh][0], DetFTTChisq[iCh], DetFTTTime[iCh] );
    }
  }
  
  return;
}


void MTConvertFBAR::GetTrueInfo( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( Int_t ich = 0 ; ich < number ; ++ich ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }
  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    ene[digi->modID]   = (float)digi->energy;
    time[digi->modID]  = (float)digi->time /8.; // [ns]->[clock]
  }
}


double MTConvertFBAR::GetVisibleEnergy( double z, double e)
{
  static const double s_FBARZPosition = DetZPosition;

  // FBAR is calibrated at z=2388 ( NCC rear coincidence)
  double propagationLength = z - s_FBARZPosition;
  return e * exp( -propagationLength / MTBP::FBARAttenuationL ) / exp( -2388 / MTBP::FBARAttenuationL );
}
  

double MTConvertFBAR::GetVisibleTime( double z, double t)
{
  static const double s_FBARZPosition = DetZPosition;

  double propagationLength = z - s_FBARZPosition;
  return t + propagationLength / MTBP::FBARPropVelo;
}
