#include "gsim2dst/MTConvert500MHzDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "gsim2dst/MTConvertUCV.h"

	// Delete later // **********8
int areaRCounter = 0;
int areaRFunctionCounter = 0;

MTConvert500MHzDetector::MTConvert500MHzDetector( Int_t detID, Int_t userFlag ):DetectorID( detID ),
	DetectorName(MTBP::detectorName[detID].c_str() ),
	DetectorNCH( MTBP::DetNChannels[detID] ),
	m_userFlag( userFlag )
{
	DetectorEventData = new GsimDetectorEventData();

	DetNumber      = 0;
	DetModID       = new Int_t  [DetectorNCH];
	DetNHit        = new Short_t[DetectorNCH];  
	DetPTime   	   = new Short_t[DetectorNCH][MTBP::NMaxHitPerEvent]; 
	DetEne         = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];
	DetTime        = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];
	DetFTTChisq    = new Float_t[DetectorNCH]; 
	DetFTTTime     = new Float_t[DetectorNCH]; 
	DetAreaR       = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent]; 

	IndexIDArray   = new Int_t  [DetectorNCH];

	DetTrueNumber  = 0;
	DetTrueModID   = new Int_t  [DetectorNCH];
	DetTrueEne     = new Float_t[DetectorNCH];
	DetTrueTime    = new Float_t[DetectorNCH];

	DetTrueNHit    = new Short_t[DetectorNCH];
	DetTrueEneTmp  = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];
	DetTrueTimeTmp = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];

	EnableAccidentalOverlay = true;//default
	AccidentalEntryID       = 0;
	AccidentalTimeShift     = 0;

	AccidentalDetModID  = new Int_t  [DetectorNCH];
	AccidentalDetNHit   = new Short_t[DetectorNCH];
	AccidentalDetEne    = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];
	AccidentalDetTime   = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];
	AccidentalDetPedestal = new Float_t[DetectorNCH];
	AccidentalDetWfm      = new Short_t[DetectorNCH][MTBP::nSample500];
	AccidentalDetECalibConst = new Float_t [DetectorNCH];
	AccidentalDetTCalibConst = new Float_t [DetectorNCH]; 

	//for waveform analysis
	DetWfm  = new Float_t[DetectorNCH][MTBP::nSample500];
	WfmFunc = new TF1( std::string( std::string("WfmFunc") + MTBP::detectorName[detID] ).c_str(),
			"[0]*exp(-TMath::Power((x-[1])/([2]+[3]*(x-[1])),2.))" );

	DetSuppWfm = new Float_t[DetectorNCH][MTBP::nSample500];
	DetSuppWfmModID = new Int_t[DetectorNCH];
	DetSuppWfmNumber = 0;

	//output of updated peak finding method
	DetUpdatedNumber = 0;
	DetUpdatedModID  = new Int_t  [DetectorNCH];
	DetUpdatedNHit   = new Short_t[DetectorNCH];  
	DetUpdatedEne    = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];
	DetUpdatedTime   = new Float_t[DetectorNCH][MTBP::NMaxHitPerEvent];

	m_E14BP = new E14BasicParamManager::E14BasicParamManager();

	E14BPConfFile = "";
	NominalAverageClusterTime_Data = 215; //[ns], default
	NominalAverageClusterTime_MC   = 215; //[ns], default
	DetZPosition = -9999;
	NDeadChannel = 0;
	DeadChannelIDVec.clear();
	NDeadModule  = 0;
	DeadModuleIDVec.clear();
	DetSmearingSigma = 0;

	for(int ich=0;ich<4096;ich++){
		DetChByChNominalTime[ich]=9999;
		DetChByChMinPeakHeight[ich]=9999;
	}  

}


MTConvert500MHzDetector::~MTConvert500MHzDetector()
{
	delete DetectorEventData;

	delete[] DetModID;
	delete[] DetNHit;
	delete[] DetPTime;
	delete[] DetEne;
	delete[] DetTime;
	delete[] DetWfm;
	delete[] DetFTTChisq;
	delete[] DetFTTTime;
	delete[] DetAreaR;
	delete   WfmFunc;
	delete [] DetSuppWfm;
	delete [] DetSuppWfmModID;

	delete[] DetTrueModID;
	delete[] DetTrueEne;
	delete[] DetTrueTime;

	delete[] DetTrueNHit;
	delete[] DetTrueEneTmp;
	delete[] DetTrueTimeTmp;

	delete[] AccidentalDetModID;
	delete[] AccidentalDetNHit;
	delete[] AccidentalDetEne;
	delete[] AccidentalDetTime;
	delete[] AccidentalDetPedestal;
	delete[] AccidentalDetWfm;
	delete[] AccidentalDetECalibConst;
	delete[] AccidentalDetTCalibConst;

	delete[] DetUpdatedModID;
	delete[] DetUpdatedNHit;
	delete[] DetUpdatedEne;
	delete[] DetUpdatedTime;

	delete[] IndexIDArray;

	delete[] DetChByChNominalTime;
	delete[] DetChByChMinPeakHeight;

	delete   m_E14BP;
}

void MTConvert500MHzDetector::Initialize()
{
	DetNumber        = 0;
	DetUpdatedNumber = 0;
	DetTrueNumber    = 0;

	for( int ich = 0; ich < DetectorNCH ; ich++ ){
		DetModID[ich]        = 0;
		DetNHit[ich]         = 0;
		DetUpdatedModID[ich] = 0;
		DetUpdatedNHit[ich]  = 0;
		DetFTTChisq[ich]     = -5.; 
		DetFTTTime[ich]      = -1.; 

		DetTrueModID[ich] = 0;
		DetTrueNHit[ich]  = 0;
		DetTrueEne[ich]   = 0;
		DetTrueTime[ich]  = 0;

		for( int ihit = 0; ihit < MTBP::NMaxHitPerEvent ; ihit++ ){
			DetEne[ich][ihit]         = MTBP::Invalid;
			DetTime[ich][ihit]        = MTBP::Invalid;
			DetUpdatedEne[ich][ihit]  = MTBP::Invalid;
			DetUpdatedTime[ich][ihit] = MTBP::Invalid;

			DetAreaR[ich][ihit]	      = MTBP::Invalid; 
			DetPTime[ich][ihit]	      = 0; 
			DetTrueEneTmp[ich][ihit]  = 0;
			DetTrueTimeTmp[ich][ihit] = 0;
		}

		for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; iSample++ )
			DetWfm[ich][iSample] = 0;
	}
}

void MTConvert500MHzDetector::SetBranchAddress( TTree *tree )
{
	GsimEventTree = tree;
	GsimEventTree->SetBranchAddress( Form( "%s.", MTBP::GsimE14DetName[DetectorID].c_str() ), &DetectorEventData );
}

void MTConvert500MHzDetector::SetAccidentalBranchAddress( TTree *tree )
{

	AccidentalTree = tree;
	if(!AccidentalTree->GetBranch( Form("%sNumber", DetectorName))){

		std::cout << "Accidental Wfm of " << DetectorName << " was not found!!\n";
		EnableAccidentalOverlay = false; 
		for(int i=0;i<DetectorNCH;i++){
			AccidentalDetECalibConst[i] = 1;
			AccidentalDetTCalibConst[i] = 0;
		}
		return;
	}

	AccidentalTree->SetBranchAddress( Form("%sNumber", DetectorName),  &AccidentalDetNumber );
	AccidentalTree->SetBranchAddress( Form("%sModID",  DetectorName),   AccidentalDetModID );
	AccidentalTree->SetBranchAddress( Form("%snHits",  DetectorName),   AccidentalDetNHit );
	AccidentalTree->SetBranchAddress( Form("%sEne",    DetectorName),   AccidentalDetEne );
	AccidentalTree->SetBranchAddress( Form("%sTime",   DetectorName),   AccidentalDetTime );
	AccidentalTree->SetBranchAddress( Form("%sPedestal", DetectorName), AccidentalDetPedestal );
	AccidentalTree->SetBranchAddress( Form("%sWfm",      DetectorName), AccidentalDetWfm );
	AccidentalTree->SetBranchAddress( Form( "%sECalibConst", DetectorName ), AccidentalDetECalibConst );
	AccidentalTree->SetBranchAddress( Form( "%sTCalibConst", DetectorName ), AccidentalDetTCalibConst );

}

void MTConvert500MHzDetector::Branch( TTree* tree )
{
	DstTree = tree;

	if( DetectorID==MTBP::IB ){
		// updated method: using moving average to search the peak position
		DstTree->Branch( Form("%sNumber", DetectorName),&DetUpdatedNumber,Form("%sNumber/I",            DetectorName ) );
		DstTree->Branch( Form("%sModID",  DetectorName), DetUpdatedModID, Form("%sModID[%sNumber]/I",   DetectorName,DetectorName ) );
		DstTree->Branch( Form("%snHits",  DetectorName), DetUpdatedNHit,  Form("%snHits[%sNumber]/S",   DetectorName,DetectorName ) );
		DstTree->Branch( Form("%sEne",    DetectorName), DetUpdatedEne,   Form("%sEne[%sNumber][%d]/F", DetectorName,DetectorName, MTBP::NMaxHitPerEvent ) );
		DstTree->Branch( Form("%sTime",   DetectorName), DetUpdatedTime,  Form("%sTime[%sNumber][%d]/F",DetectorName,DetectorName, MTBP::NMaxHitPerEvent ) );
	}else{  
		DstTree->Branch( Form("%sNumber", DetectorName),&DetNumber,Form("%sNumber/I",            DetectorName ) );
		DstTree->Branch( Form("%sModID",  DetectorName), DetModID, Form("%sModID[%sNumber]/I",   DetectorName,DetectorName ) );
		DstTree->Branch( Form("%snHits",  DetectorName), DetNHit,  Form("%snHits[%sNumber]/S",   DetectorName,DetectorName ) );
		DstTree->Branch( Form("%sEne",    DetectorName), DetEne,   Form("%sEne[%sNumber][%d]/F", DetectorName,DetectorName, MTBP::NMaxHitPerEvent ) );
		DstTree->Branch( Form("%sTime",   DetectorName), DetTime,  Form("%sTime[%sNumber][%d]/F",DetectorName,DetectorName, MTBP::NMaxHitPerEvent ) );
	}

	if(m_WFMsave){
		DstTree->Branch( Form( "%sWfmNumber", DetectorName ), &DetSuppWfmNumber, Form( "%sWfmNumber/I",  DetectorName ));
		DstTree->Branch( Form( "%sWfmModID", DetectorName ),   DetSuppWfmModID,  Form( "%sWfmModID[%sWfmNumber]/I",  DetectorName, DetectorName ));
		DstTree->Branch( Form( "%sWfm",   DetectorName ),      DetSuppWfm,       Form( "%sWfm[%sWfmNumber][%d]", DetectorName, DetectorName, MTBP::nSample500 ));
	}
	DstTree->Branch( Form("%sTrueNumber", DetectorName ), &DetTrueNumber, Form( "%sTrueNumber/I",              DetectorName ) );
	DstTree->Branch( Form("%sTrueModID",  DetectorName ),  DetTrueModID,  Form( "%sTrueModID[%sTrueNumber]/I", DetectorName, DetectorName ) );
	DstTree->Branch( Form("%sTrueEne",    DetectorName ),  DetTrueEne,    Form( "%sTrueEne[%sTrueNumber]/F",   DetectorName, DetectorName ) );
	DstTree->Branch( Form("%sTrueTime",   DetectorName ),  DetTrueTime,   Form( "%sTrueTime[%sTrueNumber]/F",  DetectorName, DetectorName ) );

	/*
	   if( DetectorID==MTBP::IB ){
	   DstTree->Branch( Form("%sWfm",  DetectorName), DetWfm, Form("%sWfm[%sNumber][%d]/F", DetectorName,DetectorName,MTBP::nSample500 ) );
	   DstTree->Branch( Form("Accidental%sECalibConst", DetectorName ), AccidentalDetECalibConst, Form("Accidental%sECalibConst[%sNumber]/F", DetectorName, DetectorName ) );
	   DstTree->Branch( Form("Accidental%sTCalibConst", DetectorName ), AccidentalDetTCalibConst, Form("Accidental%sTCalibConst[%sNumber]/F", DetectorName, DetectorName ) );
	   }
	   */

}


void MTConvert500MHzDetector::AddFTTBranch( TTree *tree)
{
	tree->Branch(Form("%sFTTChisq",DetectorName),DetFTTChisq,Form("%sFTTChisq[%sNumber]/F",DetectorName,DetectorName) );
	tree->Branch(Form("%sFTTTime",DetectorName),DetFTTTime,Form("%sFTTTime[%sNumber]/F",DetectorName,DetectorName) );
}
void MTConvert500MHzDetector::AddAreaRBranch( TTree* tree )
{
	tree->Branch(Form("%sAreaR",   DetectorName), DetAreaR, Form( "%sAreaR[%sNumber][%d]/F", DetectorName, DetectorName, MTBP::NMaxHitPerEvent ) );
}
void MTConvert500MHzDetector::Convert()
{
	if( DetectorID==MTBP::BHCV )
		ConvertFromMTimeWithResponse();

	if( DetectorID==MTBP::IB )
		ConvertFromMTimeWithPulseShape();

	if( DetectorID==MTBP::UCV )
		ConvertFromMTimeWithPulseShape();

	if( DetectorID==MTBP::newBHCV || DetectorID==MTBP::BHPV || DetectorID==MTBP::BHGC )
		ConvertFromDigiWithResponse();
}

void MTConvert500MHzDetector::AccidentalOverlay()
{
	if( DetectorID==MTBP::BHCV || DetectorID==MTBP::newBHCV || DetectorID==MTBP::BHPV || DetectorID==MTBP::BHGC )
		AddAccidental();

	if( DetectorID==MTBP::IB || DetectorID==MTBP::UCV )
		AddAccidentalWfm();
}

void MTConvert500MHzDetector::ConvertTrueInfo()
{
	GetTrueInfo( DetTrueNumber, DetTrueModID, DetTrueNHit, DetTrueEneTmp, DetTrueTimeTmp );
	for( Int_t iCh = 0 ; iCh < DetTrueNumber ; ++iCh ){
		DetTrueEne[iCh]  = DetTrueEneTmp[iCh][0];
		DetTrueTime[iCh] = DetTrueTimeTmp[iCh][0];
	}
}

void MTConvert500MHzDetector::ConvertFromDigi()
{  GetFromDigi( DetNumber, DetModID, DetNHit, DetEne, DetTime );}

void MTConvert500MHzDetector::ConvertFromDigiWithResponse()
{  GetFromDigiWithResponse( DetNumber, DetModID, DetNHit, DetEne, DetTime ); }

void MTConvert500MHzDetector::ConvertFromMTimeWithResponse()
{  GetFromMTimeWithResponse( DetNumber, DetModID, DetNHit, DetEne, DetTime ); }

void MTConvert500MHzDetector::ConvertFromMTimeWithPulseShape()
{ GetFromMTimeWithPulseShape( DetNumber, DetModID, DetNHit, DetEne, DetTime,
		DetUpdatedNumber, DetUpdatedModID, DetUpdatedNHit, DetUpdatedEne, DetUpdatedTime ); }

void MTConvert500MHzDetector::GetFromDigi(              Int_t& number, Int_t* modID, Short_t* nhit, 
		Float_t (*ene) [MTBP::NMaxHitPerEvent], 
		Float_t (*time)[MTBP::NMaxHitPerEvent] )
{  GetTrueInfo( number, modID, nhit, ene, time ); }

void MTConvert500MHzDetector::GetFromDigiWithResponse(  Int_t& number, Int_t* modID, Short_t* nhit,
		Float_t (*ene) [MTBP::NMaxHitPerEvent], 
		Float_t (*time)[MTBP::NMaxHitPerEvent] )
{  GetTrueInfo( number, modID, nhit, ene, time ); }

void MTConvert500MHzDetector::GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, 
		Float_t (*ene) [MTBP::NMaxHitPerEvent], 
		Float_t (*time)[MTBP::NMaxHitPerEvent] )

{  GetTrueInfo( number, modID, nhit, ene, time ); }

void MTConvert500MHzDetector::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Short_t* nhit, 
		Float_t (*ene) [MTBP::NMaxHitPerEvent], 
		Float_t (*time)[MTBP::NMaxHitPerEvent],
		Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit,
		Float_t (*updatedene) [MTBP::NMaxHitPerEvent],
		Float_t (*updatedtime)[MTBP::NMaxHitPerEvent] )
{  GetTrueInfo( number, modID, nhit, ene, time ); }

void MTConvert500MHzDetector::AddAccidental()
{  AddAccidental( DetNumber, DetModID, DetNHit, DetEne, DetTime );}

void MTConvert500MHzDetector::AddAccidental( Int_t& number, Int_t* modID, Short_t* nhit, 
		Float_t (*ene) [MTBP::NMaxHitPerEvent], 
		Float_t (*time)[MTBP::NMaxHitPerEvent] )
{
	if( !EnableAccidentalOverlay ) return;
	//  AccidentalTree->GetEntry( AccidentalEntryID );

	for( Int_t ich = 0; ich < DetectorNCH ; ++ich ){
		modID[ich] = IndexIDArray[ich];// for channels without true hit
		if( modID[ich] != AccidentalDetModID[ich] ){
			///// Accidental data should be in E14ID order.
			///// If not, error message is issued.
			std::cout << "DetName " << DetectorName << ", modID = " << modID[ich] << ", AccidentalDetModID = "<< AccidentalDetModID[ich] << std::endl;
			std::cout << "Mismatch between IndexIDArray and AccidentalModID." << std::endl;
			return;
		}
		Int_t  nAccHit      = AccidentalDetNHit[ich];
		Bool_t TrueHitExist = ( nhit[ich] > 0 );
		Int_t  nTooManyHits = 0;
		for( Int_t ihit = 0 ; ihit < nAccHit ; ++ihit ){      
			Float_t AccTime = AccidentalDetTime[ich][ihit];//[clock]

			if( TrueHitExist && TMath::Abs( AccTime - time[ich][0] ) < MTBP::PulseMergeTimingWindow ){
				ene[ich][0] += AccidentalDetEne[ich][ihit];
				time[ich][0] = TMath::Min(AccTime,time[ich][0]);
			}else{
				if( nhit[ich]<MTBP::NMaxHitPerEvent ){
					ene[ich][nhit[ich]]  = AccidentalDetEne[ich][ihit];
					time[ich][nhit[ich]] = AccTime;
					nhit[ich] += 1;
				}
				else ++nTooManyHits;
			}
		}

		if( nTooManyHits>0 )
			std::cout << "Too many accidental hits in " << DetectorName
				<< " ch" << ich << " : " << nTooManyHits << " hits are not saved."
				<< std::endl;
	}

}

void MTConvert500MHzDetector::AddAccidentalWfm(){  
	AddAccidentalWfm( DetNumber, DetModID, DetNHit, DetEne, DetTime,
			DetUpdatedNumber, DetUpdatedModID, DetUpdatedNHit, DetUpdatedEne, DetUpdatedTime,
			DetWfm ); }

void MTConvert500MHzDetector::AddAccidentalWfm( Int_t& number, Int_t* modID, Short_t* nhit, 
		Float_t (*ene) [MTBP::NMaxHitPerEvent], 
		Float_t (*time)[MTBP::NMaxHitPerEvent],
		Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit,
		Float_t (*updatedene)[MTBP::NMaxHitPerEvent], 
		Float_t (*updatedtime)[MTBP::NMaxHitPerEvent],
		Float_t (*wfm)[MTBP::nSample500] )
{ 
	if( !EnableAccidentalOverlay ) return;  

	
	
	for( Int_t iCh = 0 ; iCh < number ; ++iCh ){
		modID[iCh] = IndexIDArray[iCh];// for channels without true hit
		if( modID[iCh] != AccidentalDetModID[iCh] ){
			///// Accidental data should be in E14ID order.
			///// If not, error message is issued.
			std::cout << "DetName " << DetectorName << ", modID = " << modID[iCh] << ", AccidentalDetModID = "<< AccidentalDetModID[iCh] << std::endl;
			std::cout << "Mismatch between IndexIDArray and AccidentalModID." << std::endl;
			return;	
		}

		for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; iSample++ )
			wfm[iCh][iSample] += (AccidentalDetWfm[iCh][iSample] - AccidentalDetPedestal[iCh]) * AccidentalDetECalibConst[iCh];

		///// pedestal calculation for merged pulse is done before energy and timing calculation 
		Double_t StdDev[2] = { TMath::RMS( 10, &wfm[iCh][0] ), TMath::RMS( 10, &wfm[iCh][MTBP::nSample500-10] )};


		Float_t  pedestal = ( StdDev[0]<StdDev[1] ) ?  TMath::Mean( 10, &wfm[iCh][0] ) : TMath::Mean( 10, &wfm[iCh][MTBP::nSample500-10] ) ;

		for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; ++iSample )
			wfm[iCh][iSample] -= pedestal;

		

		/* //obsolete (20190517)
		   GetEnergyTimeFromWfm( &wfm[iCh][0], nhit[iCh], 
		   &ene[iCh][0], &time[iCh][0], iCh );
		   */

		GetEnergyTimeFromAveragedWfm( &wfm[iCh][0], updatednhit[iCh], 
				&updatedene[iCh][0], &updatedtime[iCh][0], iCh, &DetPTime[iCh][0]);
		EvalFTTChisq(iCh, &DetWfm[iCh][0], DetFTTChisq[iCh], DetFTTTime[iCh]);
	}
}

void MTConvert500MHzDetector::SetAccidentalTimeShift( Float_t t ) // [ns]
{
	AccidentalTimeShift  = t / MTBP::SampleInterval500; // [ns] -> [clock]  
	AccidentalTimeShift += MTBP::MCCommonTimeShift / MTBP::SampleInterval500;
}
Float_t MTConvert500MHzDetector::GetAccidentalTimeShift() const
{  return AccidentalTimeShift * MTBP::SampleInterval500;}

Int_t MTConvert500MHzDetector::GetNDigi()
{
	TClonesArray *digiArray = GetDigiArray();
	return digiArray->GetEntries();  
}

void MTConvert500MHzDetector::AddWfmFromFunc( Float_t* wfm, TF1* func )
{  
	for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; ++iSample ){
		wfm[iSample] += func->Eval( iSample ); //WfmFunc's unit is clock
	}
	return;
}

Float_t MTConvert500MHzDetector::GetIntegralFromFunc( TF1* func )
{  
	Float_t SampleIntegral = 0;
	Int_t StartSample = (Int_t)(func->GetParameter( 1 )-MTBP::nSample500/2 + 1);
	for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; iSample++ )
		SampleIntegral += func->Eval( (StartSample+iSample) ); //The unit of WfmFunc is clock

	return SampleIntegral;
}

void MTConvert500MHzDetector::GenerateWfmFromMTimeVec( Int_t iCh, const std::vector<pEnergyTime>& propagatedVec, 
		Float_t AdditionalTimeShift, TF1* fnc )
{

	TF1* FillFunc;
	if (fnc == NULL)  FillFunc = WfmFunc;
	else FillFunc = fnc;


	Double_t WfmNormConst=FillFunc->GetParameter( 0 );

	for( std::vector<pEnergyTime>::const_iterator it = propagatedVec.begin() ; it!=propagatedVec.end() ; it++ ){
	        Double_t ChTime = it->time/2. + AccidentalTimeShift + AdditionalTimeShift - AccidentalDetTCalibConst[iCh]; //clock

		FillFunc->SetParameter( 0, it->energy * WfmNormConst );
		FillFunc->SetParameter( 1, ChTime );

		Double_t IntegralNormalizedFunc = GetIntegralFromFunc( FillFunc );
		if( IntegralNormalizedFunc<0.01 ) FillFunc->SetParameter( 0, 0 );
		else FillFunc->SetParameter( 0, it->energy * WfmNormConst );

		Int_t nanflag = 0;
		if( isnan( FillFunc->Eval(0) ) )
			nanflag=1;

		AddWfmFromFunc( &DetWfm[iCh][0], FillFunc );
	}

	return;
}

void MTConvert500MHzDetector::GenerateDbWfmFromMTimeVec( Int_t iCh, const std::vector<pEnergyTime>& propagatedVec, Float_t AdditionalTimeShift )
{
	if( m_dbWfmGenerator==NULL ) return;

	LcLib::LcWfm wfm(MTBP::nSample500);

	for( std::vector<pEnergyTime>::const_iterator it = propagatedVec.begin() ; it!=propagatedVec.end() ; it++ ){
		Double_t ChTime = it->time/2. + AdditionalTimeShift/2. +  AccidentalTimeShift - AccidentalDetTCalibConst[iCh]; // clock

		const Double_t k_WfmFlucSigma = 0.5 * AccidentalDetECalibConst[iCh];
		LcLib::LcWfm sub_wfm = m_dbWfmGenerator->GenerateWfm( IndexIDArray[iCh], it->energy, ChTime, k_WfmFlucSigma);
		wfm += sub_wfm;
	}

	for( Int_t is=0; is<MTBP::nSample500; ++is )
		DetWfm[iCh][is] = wfm[is];

	return;
}

void MTConvert500MHzDetector::GetEnergyTimeFromWfm( Float_t* wfm, Short_t& nhit, 
		Float_t* ene, Float_t* time,
		Int_t ChIndex, Bool_t ApplyT0Calib )
{
	//Peak finding for 500MHz detectors
	Short_t nHit=0;
	Int_t SampleStart = 10;
	Int_t SampleEnd = 200;
	Double_t threshold = 0;
	Int_t nSampleIntegral = 0;
	Int_t nSampleBeforePeak = 0;
	if( DetectorID==MTBP::IB ){
		threshold = 
			MTBP::IBPeakFindingThreshold*(MTBP::IBWfmPeakHeight_U+MTBP::IBWfmPeakHeight_D)/2.; //0.3 MeV threshold, mean of U and D
		nSampleIntegral   = MTBP::IBnSampleIntegral;
		nSampleBeforePeak = MTBP::IBnSampleIntegralBeforePeak;
	}


	if(DetectorID==MTBP::UCV){
		threshold = 0.001; 
		nSampleIntegral   = 60;
		nSampleBeforePeak = 40;
	}

	for( Int_t iSample=SampleStart ; iSample<SampleEnd ; iSample++ ){
		Float_t PrePreDiff = wfm[iSample-1]-wfm[iSample-2];
		Float_t PreDiff    = wfm[iSample]  -wfm[iSample-1];
		Float_t Diff       = wfm[iSample+1]-wfm[iSample];
		Float_t PostDiff   = wfm[iSample+2]-wfm[iSample+1];

		if( (wfm[iSample]>(threshold))
				&& ( ((Diff<=0)&&(PostDiff<0)) && ((PreDiff>0)&&(PrePreDiff>0)) ) ){
			Bool_t  IsPileUp = false;
			Short_t PeakTime = iSample;
			Float_t PeakHeight = wfm[PeakTime];
			Float_t CFTimeThre = PeakHeight/2.;
			Float_t CFTime    = -10;
			Bool_t  FindCFThre = false;

			for( Int_t iClock=0 ; iClock<nSampleBeforePeak ; iClock++ ){
				if( wfm[PeakTime-iClock-1]>PeakHeight ) IsPileUp = true;
				if( !FindCFThre && (wfm[PeakTime-iClock]>CFTimeThre
							&& wfm[PeakTime-iClock-1]<CFTimeThre) ){
					CFTime = PeakTime-iClock-1+( (CFTimeThre-wfm[PeakTime-iClock-1])
							/(wfm[PeakTime-iClock]-wfm[PeakTime-iClock-1]) );
					FindCFThre = true;
				}
			}



			if( FindCFThre && !IsPileUp ){
				if( nHit<MTBP::NMaxHitPerEvent ){
					time[nHit]   = CFTime;
					ene[nHit] = 0;
					for( Int_t iIntegral=0 ; iIntegral<nSampleIntegral ; iIntegral++ )
						if( 0<=PeakTime-nSampleBeforePeak+iIntegral && PeakTime-nSampleBeforePeak+iIntegral<MTBP::nSample500 )
							ene[nHit] += (wfm[PeakTime-nSampleBeforePeak+iIntegral]);


					if(DetectorID!=MTBP::UCV){
						for( Int_t iIntegral=0 ; iIntegral<nSampleIntegral ; iIntegral++ )
							if( 0<=PeakTime-nSampleBeforePeak+iIntegral && PeakTime-nSampleBeforePeak+iIntegral<MTBP::nSample500 )
								ene[nHit] += (wfm[PeakTime-nSampleBeforePeak+iIntegral]);
					}else{
						ene[nHit] = wfm[PeakTime];
					}

					nHit++;

				}

			}
		}
	}
	nhit=nHit;

	if( ApplyT0Calib ){
		Float_t TimeOffset = AccidentalDetTCalibConst[ChIndex] ;//[clock]
		for(int ihit=0;ihit<MTBP::NMaxHitPerEvent;ihit++) time[ihit] += TimeOffset;
	}  

	// dead channel
	if( (20170401 < m_userFlag && m_userFlag < 20190201) || (m_userFlag>20210101) ){ // Run69-Run79 || >=Run86
		for(int ideadch=0;ideadch<MTBP::IBNDeadChannels;ideadch++){
			if( ChIndex!=MTBP::IBDeadChannelID[ideadch] ) continue;
			for(int ihit=0;ihit<MTBP::NMaxHitPerEvent;ihit++){
				ene[ihit]=0;
				time[ihit]=0;
			}
			nhit=0;
		}
	}

	return;
}

void MTConvert500MHzDetector::GetEnergyTimeFromAveragedWfm( Float_t* wfm, Short_t& nhit, 
		Float_t* ene, Float_t* time,
		Int_t ChIndex, Short_t* peakTime, Bool_t ApplyT0Calib )
{
	//Peak finding with smoothing waveform for 500MHz detectors
	Short_t nHit=0;
	Int_t SampleStart = 10;
	Int_t SampleEnd = 200;
	Int_t nSampleCFSearch = 15;
	Float_t threshold = 0;
	Float_t AveragedWfmThreshold = 0;
	Int_t nSampleIntegral = 0;
	Int_t nSampleBeforePeak = 0;
	if( DetectorID==MTBP::IB ){
		threshold = MTBP::IBPeakFindingThreshold*(MTBP::IBWfmPeakHeight_U+MTBP::IBWfmPeakHeight_D)/2.; //0.3 MeV threshold, mean of U and D
		AveragedWfmThreshold = 1/2.*MTBP::IBPeakFindingThreshold*(MTBP::IBWfmPeakHeight_U+MTBP::IBWfmPeakHeight_D)/2.; //0.2 MeV threshold, mean of U and D
		nSampleIntegral   = MTBP::IBnSampleIntegral;
		nSampleBeforePeak = MTBP::IBnSampleIntegralBeforePeak;
	}

	Float_t AveWfm[256];
	for(int isam=0;isam<256;isam++){
		AveWfm[isam]=0;
		if(isam<5||250<isam){
			AveWfm[isam]=wfm[isam];
		}else{
			for(int jsam=isam-2;jsam<=isam+2;jsam++){
				AveWfm[isam]+=wfm[jsam]/5.;
			}
		}
	}

	for( Int_t iSample=SampleStart ; iSample<SampleEnd ; iSample++ ){
		Float_t PrePreDiff = AveWfm[iSample-1]-AveWfm[iSample-2];
		Float_t PreDiff    = AveWfm[iSample]  -AveWfm[iSample-1];
		Float_t Diff       = AveWfm[iSample+1]-AveWfm[iSample];
		Float_t PostDiff   = AveWfm[iSample+2]-AveWfm[iSample+1];

		if( (AveWfm[iSample]>(AveragedWfmThreshold))
				&& ( ((Diff<=0)&&(PostDiff<0)) && ((PreDiff>0)&&(PrePreDiff>0)) ) ){

			Short_t tmpPeakTime = 0;
			Float_t tmpPeakHeight = 0;
			// find maximum peak //
			for(int jsam=iSample-6;jsam<iSample+4;jsam++){
				if( tmpPeakHeight < wfm[jsam] ){
					tmpPeakHeight = wfm[jsam];
					tmpPeakTime   = jsam;
				}
			}

			Bool_t  IsPileUp = false;
			Short_t PeakTime = tmpPeakTime;
			Float_t PeakHeight = wfm[PeakTime];
			Float_t CFTimeThre = PeakHeight/2.;
			Float_t CFTime    = -10;
			Bool_t  FindCFThre = false;

			if( PeakHeight<threshold ) continue;

			for( Int_t iClock=0 ; iClock<nSampleBeforePeak ; iClock++ )
				if( wfm[PeakTime-iClock-1]>PeakHeight ) IsPileUp = true;

			// find CFTime //
			for( Int_t iClock=0 ; iClock<nSampleCFSearch ; iClock++ ){
				if( !FindCFThre && (wfm[PeakTime-iClock]>CFTimeThre
							&& wfm[PeakTime-iClock-1]<CFTimeThre) ){
					CFTime = PeakTime-iClock-1+( (CFTimeThre-wfm[PeakTime-iClock-1])
							/(wfm[PeakTime-iClock]-wfm[PeakTime-iClock-1]) );
					FindCFThre = true;
				}
			}

			if( FindCFThre && !IsPileUp ){
				if( nHit<MTBP::NMaxHitPerEvent ){
					peakTime[nHit]  = PeakTime;
					time[nHit]   	= CFTime;
					ene[nHit] 		= 0;
					for( Int_t iIntegral=0 ; iIntegral<nSampleIntegral ; iIntegral++ )
						if( 0<=PeakTime-nSampleBeforePeak+iIntegral && PeakTime-nSampleBeforePeak+iIntegral<MTBP::nSample500 )
							ene[nHit] += (wfm[PeakTime-nSampleBeforePeak+iIntegral]);

					nHit++;
				}
			}
		}
	}
	nhit=nHit;

	Float_t TimeOffset = AccidentalDetTCalibConst[ChIndex] ;//[clock]
	for(int ihit=0;ihit<MTBP::NMaxHitPerEvent;ihit++){
		time[ihit] += TimeOffset;
	}

	// dead channel
	if( (20170401 < m_userFlag && m_userFlag < 20190201) || (m_userFlag>20210101) ){ // Run69-Run79 || >=Run86
	  for(int ideadch=0;ideadch<MTBP::IBNDeadChannels;ideadch++){
			if( ChIndex!=MTBP::IBDeadChannelID[ideadch] ) continue;
			for(int ihit=0;ihit<MTBP::NMaxHitPerEvent;ihit++){
				ene[ihit]=0;
				time[ihit]=0;
			}
			nhit=0;
		}
	}

	return;
}

void MTConvert500MHzDetector::SuppressWfm(){

	if(!m_WFMsave) return;

	DetSuppWfmNumber = 0;
	for( int ich = 0; ich < DetectorNCH ; ich++ ){
		double maxy = -1e10;
		for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; iSample++ ){
			if(maxy<DetWfm[ich][iSample]) maxy = DetWfm[ich][iSample];
		}
		maxy /= AccidentalDetECalibConst[ich];
		if(maxy>30){
			DetSuppWfmModID[DetSuppWfmNumber] = DetModID[ich];
			for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; iSample++ ){
				DetSuppWfm[DetSuppWfmNumber][iSample] = DetWfm[ich][iSample];
			}
			DetSuppWfmNumber++;
		}
	}	

}

void MTConvert500MHzDetector::GetAreaRFromWfm()
{
	Int_t calcInterval = 20;
	if (DetectorID==MTBP::BHPV) calcInterval = 30 ;
	if (DetectorID==MTBP::IB)  calcInterval = 50;

	if (DetectorID==MTBP::IB)
	{
		if (DetNumber==0) return;
		for (int ichannel = 0; ichannel<DetNumber; ichannel++)
		{
			if(DetUpdatedNHit[ichannel]==0) continue;
			for( int ihit = 0; ihit < DetUpdatedNHit[ichannel] ; ihit++ )
			{
				GetAreaRFromWfm( &DetWfm[ichannel][0], DetAreaR[ichannel][ihit], DetPTime[ichannel][ihit], calcInterval);
			}
		}
	}
	else if (DetectorID!=MTBP::newBHCV)
	{
		for (int ichannel = 0; ichannel<DetNumber; ichannel++)
		{
			if(DetNHit[ichannel]==0) continue;
			for( int ihit = 0; ihit < DetNHit[ichannel] ; ihit++ )
			{
				GetAreaRFromWfm( &DetWfm[ichannel][0], DetAreaR[ichannel][ihit], DetPTime[ichannel][ihit], calcInterval);
			}
		}
	}
}

void MTConvert500MHzDetector::GetAreaRFromWfm( const Float_t* wfm, Float_t& areaR, const Short_t peakTime, const Int_t calcInterval)
{
	float max = 0;
	float min = 0;
	float all_sum = 0;
	int centerTime = peakTime;
	int intervalCounter = 0;

	for ( Int_t ismple = centerTime-calcInterval/2; ismple < centerTime+calcInterval/2; ismple++ )
	{
		if (ismple>0 && ismple<256)
		{
			if ( intervalCounter==0 || ( wfm[ismple] > max ) ) 
			{
				max = wfm[ismple];
			}
			if ( intervalCounter==0 || ( wfm[ismple] < min ) )
			{
				min  = wfm[ismple];
			}
			all_sum += wfm[ismple];
			intervalCounter++;
		}
	}

	Float_t max_d = max;
	Float_t min_d = min;
	Float_t all_sum_d = all_sum;
	Float_t areaWidth = intervalCounter;
	
	Float_t tmpAreaR;
	if ((abs(max_d - min_d)<.001)) return;
	areaR 	= (all_sum_d/areaWidth-min_d)/(max_d-min_d);;
	tmpAreaR = (all_sum_d/areaWidth-min_d)/(max_d-min_d);
}


void MTConvert500MHzDetector::EvalFTTChisq( const Int_t ChIndex, const Float_t *wfm, Float_t &FTTChisq , Float_t &FTTTime )
{
	if( m_fttCalculator==NULL ) return;
	LcLib::LcWfm test_wfm(MTBP::nSample500, wfm );

	/// Assuming only IBAR is used : 
	const Double_t NominalTime = 130.; // [clock], the nominal time in the view of window

	m_fttCalculator->SetNominalTime( NominalTime );

	FTTChisq = m_fttCalculator->EvalFTTChisq( IndexIDArray[ChIndex], test_wfm);
	FTTTime  = m_fttCalculator->GetTimeToEvalFTT( test_wfm );
}


Int_t MTConvert500MHzDetector::ClusteringMtime( std::vector< pEnergyTime >& inputEtVec, 
		std::vector< pEnergyTime >& outputEtVec, double pitch )
{
	int startIndex=0;

	double weightedTime=0.0, totalWeight=0.0;

	for( UInt_t index=0; index<inputEtVec.size(); index++ ){
		if( inputEtVec[index].time - inputEtVec[startIndex].time < pitch ){
			weightedTime += inputEtVec[index].time * inputEtVec[index].energy;
			totalWeight  += inputEtVec[index].energy;
		}else{
			energyTime.energy = totalWeight;
			energyTime.time   = weightedTime / totalWeight;
			outputEtVec.push_back( energyTime );
			weightedTime = inputEtVec[index].time * inputEtVec[index].energy;
			totalWeight  = inputEtVec[index].energy;
			startIndex = index;
		}
	}
	energyTime.energy = totalWeight;
	energyTime.time   = weightedTime / totalWeight;
	outputEtVec.push_back( energyTime );

	return outputEtVec.size();
}

void MTConvert500MHzDetector::SetFileNameE14BasicParam( std::string InputFileName ){

	if( (access( InputFileName.c_str(), R_OK ) != 0) ){
		std::cerr << "Error!! E14BP conf file is not found." << std::endl;
		std::exit(1);
	}

	E14BPConfFile = InputFileName;
	return;
}

void MTConvert500MHzDetector::SetE14BasicParamFromRootFile( std::string InputFileName ){

	if( (access( InputFileName.c_str(), R_OK ) != 0) ){
		std::cerr << "Error!! E14BP conf file is not found." << std::endl;
		std::exit(1);
	}

	E14BPConfFile = InputFileName;

	// basic parameter derived from channel 0 in a detector
	m_E14BP->SetE14BasicParamFromRootFile( E14BPConfFile, m_userFlag, DetectorName, 0);
	NominalAverageClusterTime_Data = m_E14BP->GetNominalAverageClusterTime_Data();
	NominalAverageClusterTime_MC   = m_E14BP->GetNominalAverageClusterTime_MC();
	DetZPosition     = m_E14BP->GetDetZPosition();
	NDeadChannel     = m_E14BP->GetNDeadChannels();
	DeadChannelIDVec = m_E14BP->GetDeadChannelIDVec();
	NDeadModule      = m_E14BP->GetNDeadModules();
	DeadModuleIDVec  = m_E14BP->GetDeadModuleIDVec();
	DetSmearingSigma = m_E14BP->GetG2DPSmearingSigma();

	// basic parameter for channel by channel
	m_E14BP->GetArrayNominalPeakTime( DetChByChNominalTime   );
	m_E14BP->GetArrayMinPeakHeight(   DetChByChMinPeakHeight );

	return;
}

void MTConvert500MHzDetector::SetE14BasicParamFromCode(){

	E14BPConfFile = "";

	// basic parameter derived from channel 0 in a detector
	m_E14BP->SetDetBasicParam( m_userFlag, DetectorName, 0);
	NominalAverageClusterTime_Data = m_E14BP->GetNominalAverageClusterTime_Data();
	NominalAverageClusterTime_MC   = m_E14BP->GetNominalAverageClusterTime_MC();
	DetZPosition     = m_E14BP->GetDetZPosition();
	NDeadChannel     = m_E14BP->GetNDeadChannels();
	DeadChannelIDVec = m_E14BP->GetDeadChannelIDVec();
	NDeadModule      = m_E14BP->GetNDeadModules();
	DeadModuleIDVec  = m_E14BP->GetDeadModuleIDVec();
	DetSmearingSigma = m_E14BP->GetG2DPSmearingSigma();

	// basic parameter for channel by channel 
	for(int ich=0;ich<DetectorNCH;ich++){
		int imod = IndexIDArray[ich];
		m_E14BP->SetDetBasicParam( m_userFlag, DetectorName, imod);
		DetChByChNominalTime[imod] = m_E14BP->GetG2DPNominalPeakTime();
		DetChByChMinPeakHeight[imod] = m_E14BP->GetG2DPMinPeakHeight();
	}

	return;
}
