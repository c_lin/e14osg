#include "gsim2dst/MTConvertCC04.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


MTConvertCC04::MTConvertCC04( Int_t userFlag ) : MTConvert125MHzDetector( MTBP::CC04, userFlag )
{
   m_detname="CC04";
  // CsI crystal
  for( int index=0; index<58; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index, index) );
    IndexIDArray[index] = index;
  }
  // Plastic scintillator
  for( int index=58; index<64; index++ ){
    IDIndexMap.insert( std::pair< Int_t, unsigned int>( index+2, index) );
    IndexIDArray[index] = index+2;
  }

  DetNumber = DetectorNCH;
  DetTrueNumber = DetectorNCH;
  for( int ich=0; ich<DetNumber; ich++ ){
    DetModID[ich] = IndexIDArray[ich];
    DetTrueModID[ich] = IndexIDArray[ich];
  }

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  WfmTimeShift = MTBP::CC04WfmTimeShift;
  WfmSigma0 = MTBP::CC04WfmSigma0;
  WfmAsymPar = MTBP::CC04WfmAsymPar;

  ran = new TRandom3(userFlag);
  Initialize();
}

MTConvertCC04::~MTConvertCC04()
{
  delete ran;
  delete m_E14BP;
}

void MTConvertCC04::Initialize( void )
{
  for( int ich=0; ich<DetNumber; ich++ ){
    DetEne[ich] = 0;
    DetTime[ich] = 0;
    DetPTime[ich] = MTBP::Invalid;
    DetFallTime[ich] = MTBP::Invalid;
    for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )//added at 4th May, 2014
      DetWfm[ich][iSample] = 0; 
    
    DetTrueEne[ich] = 0;
    DetTrueTime[ich] = 0;
  }
}


void MTConvertCC04::GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ) continue;//ignore no dead materials
    ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
    time[IDIndexMap[digi->modID]]  = (float)digi->time /8.; //[ns]->[clock]
  }
}

void MTConvertCC04::GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time )
{
  TClonesArray *digiArray = GetDigiArray();

  number = DetectorNCH;
  for( int ich=0; ich<number; ich++ ){
    modID[ich] = IndexIDArray[ich];
    ene[ich]   = 0;
    time[ich]  = 0;
  }

  int nDigi = digiArray->GetEntries();
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*) digiArray->At(idigi);
    if( IDIndexMap.count( digi->modID )==0 ) continue;//ignore no dead materials
    if(digi->modID==60||digi->modID==64){
      for( int sl=0; sl<2; sl++ ){
	ene[IDIndexMap[digi->modID]+sl]   = (float)digi->energy;
	time[IDIndexMap[digi->modID]+sl]  = (float)digi->time /8.; // [ns]->[clock]
      }
    }
    else{
      ene[IDIndexMap[digi->modID]]   = (float)digi->energy;
      time[IDIndexMap[digi->modID]]  = (float)digi->time /8.; // [ns]->[clock]
    }    
  }
}

void MTConvertCC04::GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime )
//added at 4th May, 2014 by Maeda Yosuke
{
  number = DetectorNCH;
  
  //use digi information to generate waveform
  TClonesArray *digiArray = GetDigiArray();
  int nDigi = digiArray->GetEntries();

  TClonesArray *mtimeArray = GetMTimeArray();
  std::vector< pEnergyTime > propagatedVec;
  std::vector< pEnergyTime > propagatedClusteredVec;

  // digi loop
  for( int idigi=0; idigi<nDigi; idigi++ ){
    GsimDigiData* digi = (GsimDigiData*)digiArray->At( idigi );
    Int_t ModID = digi->modID;
    if( IDIndexMap.count( ModID )==0 ) continue;//ignore no dead materials
    propagatedVec.clear();
    propagatedClusteredVec.clear();
    Int_t iCh = IDIndexMap[ModID];
    Bool_t IsCsI = (ModID<60);
    
    // mtime loop
    for( UInt_t imtime=digi->mtimeEntry; imtime<digi->mtimeEntry+digi->mtimeSize; imtime++ ){
      GsimTimeData* mtime = (GsimTimeData*)mtimeArray->At( imtime );
  
	  if(m_userFlag<20190101){
		  //No detector response
		  energyTime.energy = mtime->energy;
		  energyTime.time   = mtime->time;
		  propagatedVec.push_back( energyTime );
	  }else{
		  Double_t X_mtime= mtime->r.X();
		  Double_t Y_mtime= mtime->r.Y();
		  Double_t Z_mtime= mtime->r.Z();
		  Float_t energy_mtime=mtime->energy;
		  Float_t time_mtime=mtime->time;
		  Int_t ModID_mtime = mtime->modID;

		  energyTime.energy = GetVisibleEnergy(energy_mtime, X_mtime, ModID_mtime);
		  energyTime.time   = GetVisibleTime(time_mtime, X_mtime, energyTime.energy , ModID_mtime);
	  	
		  if(energyTime.energy>1e-5 && energyTime.time<1000 ){
			  propagatedVec.push_back( energyTime );
		  }
	  }


    }
    
    // mtime clustering
    ClusteringMtime( propagatedVec, propagatedClusteredVec );
        
    Float_t TimeShiftCorrection = 0;
    if( IsCsI ){
      WfmFunc->SetParameter( 2, WfmSigma0 );
      WfmFunc->SetParameter( 3, WfmAsymPar );
    }  
    else{//scintillator
      WfmFunc->SetParameter( 2, MTBP::CC04ScintiWfmSigma0 );
      WfmFunc->SetParameter( 3, MTBP::CC04ScintiWfmAsymPar );
      TimeShiftCorrection = MTBP::TimeDiffCrystalAndScintiForCC04_CC06;
    }

    if(m_userFlag>20190101){   
      if(0<= ModID && ModID <= 41) TimeShiftCorrection-= 0.7; // ns E391 cyrstal
      else if(42<= ModID && ModID < 60) TimeShiftCorrection-= 2.4; // ns KteV cystal
    }
    
    GenerateWfmFromMTimeVec( iCh, propagatedClusteredVec,
			     TimeShiftCorrection );
    
    if(ModID==60||ModID==64){
      for( Int_t iSample=0 ; iSample<MTBP::nSample125 ; iSample++ )
	DetWfm[iCh+1][iSample] = DetWfm[iCh][iSample];
    }
  }
  
  if( !EnableAccidentalOverlay ){
    for( int iCh=0; iCh<number; iCh++ ){
      GetEnergyTimeFromWfm( &DetWfm[iCh][0], ene[iCh], time[iCh],
			    PTime[iCh], FallTime[iCh], iCh );//modified at 19th Jul, 2014
    }
  }
  
  return;
}

Float_t MTConvertCC04::GetVisibleEnergy(Float_t energy_mtime, Double_t X_mtime, Int_t ModID){

	// for scintilator, we do not consider the response
	if(ModID>=60) return energy_mtime;

	double len_x;
	double Npe_MeV; // p.e per MeV
	if(ModID%2==0){
		// north side
		len_x = 300-X_mtime;
		if((42<=ModID && ModID<=57) 
			|| ModID==6 || ModID==20 || ModID==34) len_x += 85; 
	}else{
		// south side
		len_x = X_mtime+300.0;
		if((42<=ModID && ModID<=57) 
			|| ModID==7 || ModID==21 || ModID==35) len_x += 85;
	}

	// 42~57 are KTeV crystals.
	// Values are taken from Banno's thesis
	if(42<=ModID && ModID<=57) Npe_MeV = 83.3;
	else Npe_MeV = 40.7;


	// currently, attenuation is not taken into account.
	const double att = 1e8;

	double Npe = energy_mtime*Npe_MeV*exp(-len_x/att);

	double Npe_smeared;
	if(Npe<0)Npe_smeared = 0;
	else Npe_smeared = ran->Poisson(Npe);

	double energy = Npe_smeared/Npe_MeV;
	return energy;
}
    

Float_t MTConvertCC04::GetVisibleTime(Float_t time_mtime, Double_t X_mtime, Double_t E_mtime, Int_t ModID){

	// for scintilator, we do not consider the response
	if(ModID>=60) return time_mtime;

	double len_x;
	double Npe_MeV; // p.e per MeV
	if(ModID%2==0){
		// north side
		len_x = 300-X_mtime;
		if((42<=ModID && ModID<=57) 
			|| ModID==6 || ModID==20 || ModID==34) len_x += 85; 
	}else{
		// south side
		len_x = X_mtime+300.0;
		if((42<=ModID && ModID<=57) 
			|| ModID==7 || ModID==21 || ModID==35) len_x += 85;
	}

	// 42~57 are KTeV crystals.
	// Values are taken from Banno's thesis
	if(42<=ModID && ModID<=57) Npe_MeV = 83.3;
	else Npe_MeV = 40.7;


	// since this value is difficult to measure from existing data 
	// (2019 June) we assume the propagation is same as 5cmx5cm KOTO CSI.
	const double propagation_v = MTBP::CSILPropVelo;

	Float_t time = time_mtime + (len_x-300)/propagation_v;

	// time constant of pure CsI
	const double sigma_intrinsic = 6.0;   

	int Npe = (E_mtime+1e-6)*Npe_MeV;
	if(Npe==0) return 9999;
	double sigma_Npe = sigma_intrinsic/sqrt(Npe);
	time += ran->Gaus(0, sigma_Npe);
  
	return time;

}




