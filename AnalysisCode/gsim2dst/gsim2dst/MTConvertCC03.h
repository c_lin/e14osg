#ifndef __MTCONVERTCC03_H__
#define __MTCONVERTCC03_H__


#include <iostream>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTPositionHandler.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTConvertCC03 : public MTConvert125MHzDetector
{
public:
  MTConvertCC03( Int_t userFlag );
  ~MTConvertCC03();
  void Initialize( void );
  void GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetTrueInfo( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//added at 13th Mar, 2014, updated at 4th May, 2014
  
private:
  MTPulseGenerator* pulseGenerator;
  MTPositionHandler* pHandler;
  double lightYield[16][9];
  
  double GetVisibleTime( double z, double t);
  bool LoadLightYield( void );

  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTCC03_H__
