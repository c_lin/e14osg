#ifndef __MTCONVERTGENPARTICLE_H__
#define __MTCONVERTGENPARTICLE_H__


#include <iostream>
#include "TROOT.h"
#include "TTree.h"
#include "TClonesArray.h"

#include "GsimData/GsimGenParticleData.h"
#include "GsimData/GsimTrackData.h"

#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTConvertGenParticle
{
 public:
  MTConvertGenParticle( Int_t userFlag );
  ~MTConvertGenParticle();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  Int_t Convert( void );
  Float_t GetInitialDeltaTOF( Float_t ParticleMass = MTBP::KL_MASS, Float_t ParticleMom = 2000, Bool_t ForceToUseSpecifiedMom = false );
  //added at 27th May, 2014, updated at 13th Jun, 2014
  
  TClonesArray* GetGenParticleArray( void );


  Int_t TruePID; // mother particle PID
  Int_t TrueDecayMode;
  enum { KE3=1, KMU3, K3PI0, KCHARGED3PI, K2PI0, K2GAMMA};
  /*
    -1 : Not KL
    1  : Ke3
    2  : Kmu3
    3  : 3pi0
    4  : pi+pi-pi0
    5  : 2pi0
    6  : 2gamma
    0  : other decay mode

    +10 for each pi0 dalitz decay
   */
  Double_t TrueVertexZ;
  Double_t TrueVertexTime;
  Double_t TrueMom[3];
  Double_t TruePos[3];


  // GenParticle information
  static const int MaxGenParticleN = 256;
  Int_t GenParticleNumber;
  Int_t* GenParticleTrack;
  Int_t* GenParticleMother;
  Int_t* GenParticlePid;
  //Int_t* GenParticleMotherPid;
  Double_t (*GenParticleMom)[3];
  Double_t* GenParticleEk;
  Double_t* GenParticleMass;
  Double_t* GenParticleTime;
  Double_t (*GenParticlePos)[3];
  Double_t (*GenParticleEndMom)[3];
  Double_t* GenParticleEndEk;
  Double_t* GenParticleEndTime;
  Double_t (*GenParticleEndPos)[3];


  // Gamma information
  Int_t    TrueNGammas;
  Int_t    TrueNGammasOnCSI;
  Double_t (*TrueGammaPosOnCSISurface)[3]; // A gamma generated at downstream of CsI surface, set to Decay point.

  // On the CsI surface
  Int_t    TrueHitsOnCSI;

  
  static const int KLPID = 130;
  static const int PI0PID = 111;
  static const int GAMMAPID = 22;
  

  

 private:
  TTree* m_eventTree;
  TTree* m_dstTree;
  GsimGenParticleData* m_genParticleData;
  Int_t GetDecayMode( std::vector< int > pidVec );
  bool CheckDalitz( std::vector< int > pidVec );
  bool IsOnCSISurface( double* pos );
  bool GetCSIHitPosition( TVector3 v, TVector3 p, Double_t &x, Double_t &y, Double_t &z );

  const Int_t m_userFlag;
  
  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  Double_t CSIZPosition;
  double GetDetZPosition( char* detName );

};





#endif //__MTCONVERTGENPARTICLE_H__
