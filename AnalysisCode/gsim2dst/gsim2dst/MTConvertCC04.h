#ifndef __MTCONVERTCC04_H__
#define __MTCONVERTCC04_H__


#include <iostream>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertCC04 : public MTConvert125MHzDetector
{
 public:
  MTConvertCC04( Int_t userFlag );
  ~MTConvertCC04();
  void Initialize( void );
  void GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//added at 4th May, 2014 by Maeda Yosuke


 private:

  TRandom3* ran;
  Float_t GetVisibleTime(Float_t time_mtime, Double_t X_mtime, Double_t E_mtime, Int_t ModID);
  Float_t GetVisibleEnergy(Float_t energy_mtime, Double_t X_mtime, Int_t ModID);
  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};
#endif //__MTCONVERTCC04_H__
