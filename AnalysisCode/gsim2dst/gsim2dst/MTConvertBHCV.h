#ifndef __MTCONVERTBHCV_H__
#define __MTCONVERTBHCV_H__

#include <vector>

#include <TROOT.h>
#include <TRandom3.h>
#include <TMath.h>

#include "MTConvert500MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertBHCV : public MTConvert500MHzDetector
{
public:
  MTConvertBHCV( Int_t userFlag );
  ~MTConvertBHCV();
  
  void ConvertFromMTimeWithResponse();
  void GetTrueInfo(              Int_t& number, Int_t* modID, Short_t* nhit,
				 Float_t (*ene) [MTBP::NMaxHitPerEvent],
				 Float_t (*time)[MTBP::NMaxHitPerEvent] );
  void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Short_t* nhit,
				 Float_t (*ene) [MTBP::NMaxHitPerEvent],
				 Float_t (*time)[MTBP::NMaxHitPerEvent] );
  
private:
  pEnergyTime m_EnergyTime;
  void GetModEnergyTimeFromMTime( std::vector<pEnergyTime>& etVec, Double_t& ene, Double_t& time ); 

  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTBHCV_H__
