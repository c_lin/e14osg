#ifndef __MTCONVERTCBAR_H__
#define __MTCONVERTCBAR_H__

#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertCBAR : public MTConvert125MHzDetector
{
public:
  MTConvertCBAR( Int_t userFlag );
  ~MTConvertCBAR();
  void Initialize( void );
  void GetFromMTime( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//updated at 4th May, 2014 
  //void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetTrueInfo( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );


private:
  MTPulseGenerator* pulseGenerator;

  static const float dataSimTimeOffset;// = 4.446; // [ns] defined in.cc file

  // Noise parameters
  double* noiseParams; // [64]

  double GetFrontVisibleEnergy( double z, double e );
  double GetRearVisibleEnergy( double z, double e );
  double GetFrontVisibleTime( double z, double t );
  double GetRearVisibleTime( double z, double t );

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  
};

#endif //__MTCONVERTCBAR_H__
