#ifndef __MTCONVERTNCC_H__
#define __MTCONVERTNCC_H__


#include <iostream>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertNCC : public MTConvert125MHzDetector
{
public:
  MTConvertNCC( Int_t userFlag );
  ~MTConvertNCC();
  void Initialize( void );
  void GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//added at 28th Feb, 2014 by Maeda Yosuke, updated at 4th May, 2014
  void GetEtWfm();//added at 12th Mar, 2014

 private:
  MTPulseGenerator* pulseGenerator;
  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTNCC_H__
