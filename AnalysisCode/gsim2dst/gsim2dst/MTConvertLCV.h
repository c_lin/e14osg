#ifndef __MTCONVERTLCV_H__
#define __MTCONVERTLCV_H__

#include <iostream>
#include <vector>
#include <TROOT.h>

#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class LCVPositionCorrectionData{//added at 26th Dec, 2013
public:
  LCVPositionCorrectionData() : m_zPos(0), m_RelOutput(0), m_RelOutputError(0){};
  Double_t m_zPos;//measured from upstream edge
  Double_t m_RelOutput;//relative output
  Double_t m_RelOutputError;//error of relative output
  
  friend Bool_t operator<( const LCVPositionCorrectionData& data1, const LCVPositionCorrectionData& data2 )
  { return ( data1.m_zPos < data2.m_zPos ); };
};

class MTConvertLCV : public MTConvert125MHzDetector
{
 public:
  MTConvertLCV( Int_t userFlag );
  ~MTConvertLCV();
  void Initialize( void );
  void GetTrueInfo( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  
  void ConvertFromMTimeWithResponse( void );//added at 26th Dec, 2013
  void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );//added at 26th Dec, 2013
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//added at 15th Mar, 2014, updated at 4th May, 2014
  
  std::vector<LCVPositionCorrectionData>* m_PositionCorrectionVec;
  Double_t GetPositionCorrection( Int_t modID, Double_t zPosFromUpstream );//added at 26th Dec, 2013
  
 private:
  MTPulseGenerator* pulseGenerator;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  
};

#endif //__MTCONVERTLCV_H__
