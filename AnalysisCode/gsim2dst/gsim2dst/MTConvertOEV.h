#ifndef __MTCONVERTOEV_H__
#define __MTCONVERTOEV_H__


#include <iostream>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertOEV : public MTConvert125MHzDetector
{
public:
  MTConvertOEV( Int_t userFlag );
  ~MTConvertOEV();
  void Initialize( void );
  void GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  
  void ConvertFromMTimeWithResponse( void );//added at 4th Jan, 2014
  void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );//added at 4th Jan, 2014
  void GetFromMTimeWithPulseShape(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//added at 4th May, 2014

private:
  Int_t* SimID2ModIDArray;
  MTPulseGenerator* pulseGenerator;//added at 4th Jan, 2014

  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTOEV_H__
