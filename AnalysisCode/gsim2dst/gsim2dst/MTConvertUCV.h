#ifndef __MTCONVERTUCV_H__
#define __MTCONVERTUCV_H__

#include <iostream>

#include <TROOT.h>
#include <TRandom3.h>
#include <TMath.h>

#include "MTConvert500MHzDetector.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
//#include "AnalysisLibrary/MTPulseGenerator.h"

class MTConvertUCV : public MTConvert500MHzDetector
{
 public:
  MTConvertUCV( Int_t userFlag );
  ~MTConvertUCV();

  void Initialize( void );
  
  void ConvertFromDigiWithResponse( void );
  void GetTrueInfo(  Int_t& number, Int_t* modID, Short_t* nhit ,Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] );
  void GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] );
  void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Short_t* nhit,
				   Float_t (*ene)[MTBP::NMaxHitPerEvent], 
				   Float_t (*time)[MTBP::NMaxHitPerEvent],
				   Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit, 
				   Float_t (*updatedene)[MTBP::NMaxHitPerEvent], 
				   Float_t (*updatedtime)[MTBP::NMaxHitPerEvent] );


  void AddAccidentalWfm( Int_t& number, Int_t* modID, Short_t* nhit,
				 Float_t (*ene) [MTBP::NMaxHitPerEvent], 
				 Float_t (*time)[MTBP::NMaxHitPerEvent],
				 Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit,
				 Float_t (*updatedene) [MTBP::NMaxHitPerEvent],
				 Float_t (*updatedtime)[MTBP::NMaxHitPerEvent],
				 Float_t (*wfm)[MTBP::nSample500] );


  void GetEnergyTimeFromWfm( Float_t* wfm, Short_t& nhit, Float_t* ene, Float_t* time, Int_t ChIndex, 
		  Bool_t ApplyT0Calib = true );

  void Branch( TTree *tree );
 protected:
  Float_t (*DetFallTime)[MTBP::NMaxHitPerEvent];
  Float_t (*DetPeakTime)[MTBP::NMaxHitPerEvent];
 
 private:
  double GetVisibleEnergy( int modid, double y, double e );
  double GetVisibleTime( int modid, double y, double t , double e);
  
  TRandom3* ran;

  TF1* func;
  double norm_wfm;

  // calibration constant of ch0. Used to cancel
  // offset.
  double t00;
};

#endif //__MTCONVERTUCV_H__
