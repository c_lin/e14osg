#ifndef __MTCONVERTBHGC_H__
#define __MTCONVERTBHGC_H__

#include <iostream>

#include <TROOT.h>
#include <TRandom3.h>
#include <TMath.h>

#include "MTConvert500MHzDetector.h"


class MTConvertBHGC : public MTConvert500MHzDetector
{
public:
  MTConvertBHGC( Int_t userFlag );
  ~MTConvertBHGC();
  
  void ConvertFromDigiWithResponse();
  void GetTrueInfo(             Int_t& number, Int_t* modID, Short_t* nhit,
				Float_t (*ene) [MTBP::NMaxHitPerEvent], 
				Float_t (*time)[MTBP::NMaxHitPerEvent] );
  void GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit,
				Float_t (*ene) [MTBP::NMaxHitPerEvent],
				Float_t (*time)[MTBP::NMaxHitPerEvent] );

 private:
  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTBHGC_H__
