#ifndef __MTCONVERTFBAR_H__
#define __MTCONVERTFBAR_H__


#include <iostream>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertFBAR : public MTConvert125MHzDetector
{
 public:
  MTConvertFBAR( Int_t userFlag );
  ~MTConvertFBAR();
  void Initialize( void );
  void GetFromMTime(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );
  void GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );


 private:
  MTPulseGenerator* pulseGenerator;

  double GetVisibleEnergy( double z, double e);
  double GetVisibleTime( double z, double t);

  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTFBAR_H__
