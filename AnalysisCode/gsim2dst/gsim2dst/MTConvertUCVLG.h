#ifndef __MTCONVERTUCVLG_H__
#define __MTCONVERTUCVLG_H__


#include <iostream>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTPositionHandler.h"


class MTConvertUCVLG : public MTConvert125MHzDetector
{
public:
  MTConvertUCVLG( Int_t userFlag );
  ~MTConvertUCVLG();
  void Initialize( void );
  void GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetTrueInfo( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//added at 13th Mar, 2014, updated at 4th May, 2014
  
private:
  MTPulseGenerator* pulseGenerator;
  MTPositionHandler* pHandler;
  
  double GetVisibleTime( double y, double t , double e);
  double GetVisibleEnergy( double y, double e );
  double norm_wfm;
  
  TRandom3* ran;
  TF1* func;

};

#endif //__MTCONVERTUCVLG_H__
