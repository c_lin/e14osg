#ifndef __MTCONVERTDCV_H__
#define __MTCONVERTDCV_H__


#include <iostream>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "TRandom3.h"

class MTConvertDCV : public MTConvert125MHzDetector
{
public:
  MTConvertDCV( Int_t userFlag );
  ~MTConvertDCV();
  void Initialize( void );
  void GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );
  
private:
  TF1* m_WfmFunc;

  double Npe_MeV; // Npe/MeV at the center of module
  TRandom3* ran;
  std::vector< pEnergyTime >* propagatedVec;
  std::vector< pEnergyTime >* propagatedClusteredVec;

  // In geant4, the ModID varies from 0 to 7
  // the chID is 0, 1, 2, 3
  Float_t GetEnergy(Float_t energy, Double_t Z, Int_t chID, Int_t ModID);
  Float_t GetTime( Float_t time, Double_t Z, Double_t E, Int_t chID, Int_t ModID);

};
#endif //__MTCONVERTDCV_H__
