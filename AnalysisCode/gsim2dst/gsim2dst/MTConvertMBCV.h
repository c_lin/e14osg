#ifndef __MTCONVERTMBCV_H__
#define __MTCONVERTMBCV_H__

#include <iostream>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertMBCV : public MTConvert125MHzDetector
{
 public:
  MTConvertMBCV( Int_t userFlag );
  ~MTConvertMBCV();
  void Initialize( void );
  void GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//added at 4th May, 2014 by Maeda Yosuke
  
 private:
  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};
#endif //__MTCONVERTMBCV_H__
