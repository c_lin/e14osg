#ifndef __MTCONVERTBHPV_H__
#define __MTCONVERTBHPV_H__

#include <iostream>

#include <TROOT.h>
#include <TRandom3.h>
#include <TMath.h>

#include "MTConvert500MHzDetector.h"

class MTConvertBHPV : public MTConvert500MHzDetector
{
public:
  MTConvertBHPV( Int_t userFlag );
  ~MTConvertBHPV();
  
  void ConvertFromDigiWithResponse();

  void GetTrueInfo(             Int_t& number, Int_t* modID, Short_t* nhit ,
				Float_t (*ene) [MTBP::NMaxHitPerEvent], 
				Float_t (*time)[MTBP::NMaxHitPerEvent] );
  
  void GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, 
				Float_t (*ene) [MTBP::NMaxHitPerEvent], 
				Float_t (*time)[MTBP::NMaxHitPerEvent] );

 private:
  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTBHPV_H__
