#ifndef __MTCONVERTBCV_H__
#define __MTCONVERTBCV_H__

#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertBCV : public MTConvert125MHzDetector
{
public:
  MTConvertBCV( Int_t userFlag );
  ~MTConvertBCV();
  void Initialize( void );
  void GetFromMTime( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//updated at 15th Mar, 2014, again 4th May, 2014
  void GetTrueInfo( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );

private:
  MTPulseGenerator* pulseGenerator;

  double GetFrontVisibleEnergy( double z, double e );
  double GetRearVisibleEnergy( double z, double e );
  double GetFrontVisibleTime( double z, double t );
  double GetRearVisibleTime( double z, double t );

  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTBCV_H__
