#ifndef __MTCONVERTIBAR_H__
#define __MTCONVERTIBAR_H__

#include <iostream>

#include <TROOT.h>
#include <TRandom3.h>
#include <TMath.h>

#include "MTConvert500MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
//#include "AnalysisLibrary/MTPulseGenerator.h"

class MTConvertIBAR : public MTConvert500MHzDetector
{
 public:
  MTConvertIBAR( Int_t userFlag );
  ~MTConvertIBAR();
  void Initialize( void );
  
  void ConvertFromDigiWithResponse( void );
  void GetTrueInfo(  Int_t& number, Int_t* modID, Short_t* nhit ,Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] );
  void GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] );
  void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Short_t* nhit, Float_t (*ene)[MTBP::NMaxHitPerEvent], Float_t (*time)[MTBP::NMaxHitPerEvent] );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Short_t* nhit,
				   Float_t (*ene)[MTBP::NMaxHitPerEvent], 
				   Float_t (*time)[MTBP::NMaxHitPerEvent],
				   Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit, 
				   Float_t (*updatedene)[MTBP::NMaxHitPerEvent], 
				   Float_t (*updatedtime)[MTBP::NMaxHitPerEvent] );
 private:
  double GetFrontVisibleEnergy( double z, double e );
  double GetRearVisibleEnergy( double z, double e );
  double GetFrontVisibleTime( double z, double t );
  double GetRearVisibleTime( double z, double t );

  MTPulseGenerator* pulseGenerator;
  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTIBAR_H__
