#ifndef __MTCONVERTBPCV_H__
#define __MTCONVERTBPCV_H__


#include <iostream>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertBPCV : public MTConvert125MHzDetector
{
public:
  MTConvertBPCV( Int_t userFlag );
  ~MTConvertBPCV();

  void Initialize( void );
  void GetTrueInfo(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );
  
private:
  TF1* m_WfmFunc;
  int Rpois(Double_t lamda);

  Bool_t attflag;
  Bool_t poissonflag;
  
  Float_t GetEnergy(Float_t energy_mtime, Double_t Z_mtime, Int_t ModID);
  Float_t GetTime( Float_t time_mtime, Double_t Z_mtime);

  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};
#endif //__MTCONVERTBPCV_H__
