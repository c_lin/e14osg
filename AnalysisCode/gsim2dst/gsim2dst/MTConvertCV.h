#ifndef __MTCONVERTCV_H__
#define __MTCONVERTCV_H__


#include <iostream>
#include <fstream>
#include <cmath>

#include "TRandom.h"

#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTPositionHandler.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertCV : public MTConvert125MHzDetector
{
public:
  MTConvertCV( Int_t userFlag );
  ~MTConvertCV();
  void Initialize( void );
  void GetFromDigi( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
	void GetTrueInfo( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );//updated at 11th Mar, 2014
	void GetFromMTimeWithPulseShape
					( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, Float_t* PTime, Float_t* FallTime );//updated at 4th May, 2014 (PTime and FallTime added) 
  void AddAccidental();
  void AddAccidental( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
    
private:
  MTPositionHandler* pHandler;
  MTPulseGenerator* pulseGenerator;
  int* lightYieldNumber;
  double (*lightYield)[3][20]; // x, y, light yield
  double* meanLightYield;
  double* m_1peIntADC;//added at 24th Jun, 2014 (MY)
  double* m_reCalibrationFactor;
  double* m_pedestalWidth;
  double* m_temporalPedestalWidth;
  static const double defaultLightYield;// = 150.; // /MeV, define in .cc file
  static const double dataSimTimeOffset;// = 2.433; // [ns], define in .cc file

  bool LoadLightYield( Int_t userFlag );//updated at 25th Feb, 2014 to get LY/MeV for each readout side (short & long)
  //double GetLightYield( int modID, double x, double y ) const;//odsolete, should not be used 
  void GetLightYield( Int_t modID, Double_t ModHitPosFromXYAxis,
		      Double_t& LYPerMeVShort, Double_t& LYPerMeVLong ) const;//added at 25th Feb, 2014
  void LoadReCalibrationFactor( Int_t userFlag );
  void LoadPedestalWidth( Int_t userFlag );
	// added to use Ecalib estimated with MC by coterra //
  void LoadEstimatedEcalibFromMC( Int_t userFlag );
	double* m_EstimatedEcalibFromMC;
	//////////////////////////////////////////////////////
  
  Double_t GetModHitPos( Int_t modID, Double_t x, Double_t y );//added at 25th 2014 Feb by Maeda Yosuke 
  Double_t GetLightPropTime( Int_t modID, Double_t ModHitPosFromXYAxis, Double_t& CommonDelayTime );//added at 22nd 2014 Feb by Maeda Yosuke 

  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTCV_H__
