#ifndef __MTCONVERTDETECTOR_H__
#define __MTCONVERTDETECTOR_H__

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <math.h>
#include <string>

#include <TROOT.h>
#include <TStyle.h>
#include <TString.h>
#include <TTree.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TClonesArray.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TArrow.h>
#include <TRandom3.h>

#include "GsimData/GsimDetectorEventData.h"
#include "MTAnalysisLibrary/MTFunction.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "LcDbWfm/LcDbWfmGenerator.h"
#include "LcFTT/LcFTT.h"
#include "LcTrigSim/LcTrigSimCluster.h"

class MYParabolaTimeContainer{
 public:
  Int_t    m_PeakTime;
  Double_t m_ParabolaParam[3];
  MYParabolaTimeContainer(){
    for( Int_t iPar=0 ; iPar<3 ; iPar++ ) m_ParabolaParam[iPar] = 0;
  };
};

class MTConvertDetector
{
public:

  /// constructor
  MTConvertDetector();

  /// destructor
  ~MTConvertDetector();  

  virtual void  SetBranchAddress( TTree* tree ) = 0;
  virtual void  SetAccidentalOverlayDisable() = 0;
  virtual void  SetAccidentalBranchAddress( TTree* tree ) = 0;
  virtual void  Branch( TTree* tree ) = 0;
 
  /// data-base
  bool  SetDbWfmDir( const std::string dbWfmFileDir ); 
  
  /// FTT
  bool  SetFTTDir( const std::string fttFileDir );
  virtual void  AddFTTBranch( TTree *tree ) = 0;
  virtual void  AddAreaRBranch( TTree *tree ) = 0;

  /// CDT clustering simulation
  void  EnableOnlineClusteringSimulation( const Int_t userflag );
  bool  AddOnlineClusterBranch( TTree *tr );
  void  SetCDTBitMap( Short_t *CDTBitMap );

  /// common
  virtual void  Initialize() = 0;
  virtual void  Convert() = 0;
  virtual void  AccidentalOverlay() = 0;

  virtual void  ConvertTrueInfo() = 0;
  virtual void  SetAccidentalTimeShift( Float_t t ) = 0; // [ns]

  virtual Int_t GetNDigi() = 0;
  virtual void  ZeroSuppress( Float_t threshold = 1. ) = 0;
  virtual void  GetEtWfm() = 0;
  
  virtual void  SetFileNameE14BasicParam( std::string ) = 0;
  virtual void  SetE14BasicParamFromRootFile( std::string ) = 0;
  virtual void  SetE14BasicParamFromCode() = 0;

  virtual void SuppressWfm() = 0;
  virtual void GetAreaRFromWfm() = 0;

  void  SetWFMSaveFlag(bool issave){ m_WFMsave = issave;}

  static Int_t   AccidentalRunID;
  static Int_t   AccidentalRunID_fillflag;

   
  int ApplyWfmCorrection;
 protected :
  std::string m_detname;

  LcLib::LcDbWfmGenerator *m_dbWfmGenerator; 
  LcLib::LcFTT            *m_fttCalculator;
  LcLib::LcTrigSimCluster *m_onlineClusSimulator;

  Int_t m_CDTNum;

  virtual void SetOnlineClusNominalSample() = 0;
  virtual void SetOnlineClusPeakThreshold() = 0;

  bool m_WFMsave;

};

#endif //__MTCONVERTDETECTOR_H__
