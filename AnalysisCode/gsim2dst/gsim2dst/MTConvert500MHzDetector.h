#ifndef __MTCONVERT500MHZDETECTOR_H__
#define __MTCONVERT500MHZDETECTOR_H__

#include "MTConvertDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvert500MHzDetector : public MTConvertDetector
{
 public:
  MTConvert500MHzDetector( Int_t detID, Int_t userFlag );
  virtual ~MTConvert500MHzDetector();
  
  void         SetBranchAddress( TTree *tree );
  void         SetAccidentalOverlayDisable(){ EnableAccidentalOverlay = false; }
  void         SetAccidentalBranchAddress( TTree *tree );
  void         Branch( TTree *tree );
  virtual void  AddFTTBranch( TTree *tree); 
  virtual void  AddAreaRBranch( TTree* tree);

  void         Convert();
  void         AccidentalOverlay();
  
  void         ZeroSuppress( Float_t threshold = 1. ){;}
  void         GetEtWfm(){;}
  
  virtual void Initialize();

  virtual void ConvertFromDigi();
  virtual void ConvertFromDigiWithResponse();
  virtual void ConvertFromMTimeWithResponse();
  virtual void ConvertFromMTimeWithPulseShape();
  virtual void ConvertTrueInfo();
  //virtual void ConvertFromMTime();
  
  virtual void GetFromDigi(              Int_t& number, Int_t* modID, Short_t* nhit,
					 Float_t (*ene) [MTBP::NMaxHitPerEvent], 
					 Float_t (*time)[MTBP::NMaxHitPerEvent] );
  virtual void GetFromDigiWithResponse(  Int_t& number, Int_t* modID, Short_t* nhit,
					 Float_t (*ene) [MTBP::NMaxHitPerEvent],
					 Float_t (*time)[MTBP::NMaxHitPerEvent] );
  virtual void GetFromMTimeWithResponse( Int_t& number, Int_t* modID, Short_t* nhit,
					 Float_t (*ene) [MTBP::NMaxHitPerEvent],
					 Float_t (*time)[MTBP::NMaxHitPerEvent] );
  virtual void GetTrueInfo(              Int_t& number, Int_t* modID, Short_t* nhit,
					 Float_t (*ene) [MTBP::NMaxHitPerEvent], 
					 Float_t (*time)[MTBP::NMaxHitPerEvent] ) = 0;
  //virtual void GetFromMTime(  Int_t& number, Int_t* modID, Float_t* ene, Float_t* time ) {};
  virtual void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Short_t* nhit,
					   Float_t (*ene) [MTBP::NMaxHitPerEvent],
					   Float_t (*time)[MTBP::NMaxHitPerEvent],
					   Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit,
					   Float_t (*updatedene) [MTBP::NMaxHitPerEvent],
					   Float_t (*updatedtime)[MTBP::NMaxHitPerEvent] );
  
  void SuppressWfm();

  Int_t         GetNDigi();
  TClonesArray *GetDigiArray(){  return DetectorEventData->digi; }
  TClonesArray *GetMTimeArray(){ return DetectorEventData->mtime;}
  
  virtual void  AddAccidental();
  virtual void  AddAccidental( Int_t& number, Int_t* modID, Short_t* nhit,
			       Float_t (*ene) [MTBP::NMaxHitPerEvent], 
			       Float_t (*time)[MTBP::NMaxHitPerEvent] );
  virtual void AddAccidentalWfm();
  virtual void AddAccidentalWfm( Int_t& number, Int_t* modID, Short_t* nhit,
				 Float_t (*ene) [MTBP::NMaxHitPerEvent], 
				 Float_t (*time)[MTBP::NMaxHitPerEvent],
				 Int_t& updatednumber, Int_t* updatedmodID, Short_t* updatednhit,
				 Float_t (*updatedene) [MTBP::NMaxHitPerEvent],
				 Float_t (*updatedtime)[MTBP::NMaxHitPerEvent],
				 Float_t (*wfm)[MTBP::nSample500] );
  
  void          SetAccidentalEntryNumber( Long64_t entry ){ AccidentalEntryID = entry; }//should be static
  void          SetAccidentalTimeShift( Float_t t ); // [ns]
  
  Float_t       GetAccidentalTimeShift() const;
  
  GsimDetectorEventData *GetDetectorEventData(){return DetectorEventData;}
  
  // waveform analysis
  TF1    *WfmFunc;
  void    AddWfmFromFunc( Float_t* wfm, TF1* func );
  Float_t GetIntegralFromFunc( TF1* func );
  
  void    GenerateWfmFromMTimeVec( Int_t iCh, const std::vector<pEnergyTime>& propagatedVec, 
				   Float_t AdditionalTimeShift=0, TF1* fnc=NULL);

  void    GenerateDbWfmFromMTimeVec( Int_t iCh, const std::vector<pEnergyTime>& propagatedVec,
                                   Float_t AdditionalTimeShift = 0 );

 
  void    GetEnergyTimeFromWfm( Float_t* wfm, Short_t& nhit,
				Float_t* ene, Float_t *time,
				Int_t ChIndex, Bool_t ApplyT0Calib = true );

  void    GetEnergyTimeFromAveragedWfm( Float_t* wfm, Short_t& nhit,
					Float_t* ene, Float_t *time,
					Int_t ChIndex, Short_t* peakTime, Bool_t ApplyT0Calib = true );

  void    GetAreaRFromWfm();
  void 	  GetAreaRFromWfm( const Float_t* wfm, Float_t& areaR, const Short_t peakTime, const Int_t calcInterval);

  void EvalFTTChisq( const Int_t ChIndex, const Float_t *wfm, Float_t &FTTChisq, Float_t &FTTTime );

  void SetFileNameE14BasicParam( std::string InputFileName);
  void SetE14BasicParamFromRootFile( std::string InputFileName);
  void SetE14BasicParamFromCode();

 private:
  TTree    *GsimEventTree;
  TTree    *AccidentalTree;
  GsimDetectorEventData *DetectorEventData;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetNominalAverageClusterTime_Data();
  double GetNominalAverageClusterTime_MC();

 protected:
  TTree    *DstTree;
  const Int_t    DetectorID;
  const Char_t  *DetectorName;
  const Int_t    DetectorNCH;
  const Int_t    m_userFlag;

  std::map< Int_t, unsigned int> IDIndexMap; // key : modID, value : dst array index
  Int_t                         *IndexIDArray; // modID array for dst.

  // same format as dst file
  Int_t     DetNumber;
  Int_t    *DetModID;
  Short_t  *DetNHit;
  Float_t (*DetEne)[MTBP::NMaxHitPerEvent];
  Float_t (*DetTime)[MTBP::NMaxHitPerEvent];
  Short_t (*DetPTime)[MTBP::NMaxHitPerEvent];
  Float_t (*DetAreaR)[MTBP::NMaxHitPerEvent];		//area ratio
  Float_t  *DetFTTChisq; 
  Float_t  *DetFTTTime; 
 
  Int_t     DetTrueNumber;
  Int_t    *DetTrueModID;
  Float_t  *DetTrueEne;
  Float_t  *DetTrueTime;
  Short_t  *DetTrueNHit;
  Float_t (*DetTrueEneTmp)[MTBP::NMaxHitPerEvent];
  Float_t (*DetTrueTimeTmp)[MTBP::NMaxHitPerEvent];
  
  // Accidental data
  Int_t      AccidentalDetNumber;
  Int_t     *AccidentalDetModID;
  Short_t   *AccidentalDetNHit;
  Float_t  (*AccidentalDetEne)[MTBP::NMaxHitPerEvent];
  Float_t  (*AccidentalDetTime)[MTBP::NMaxHitPerEvent];
  Float_t   *AccidentalDetPedestal;
  Short_t  (*AccidentalDetWfm)[MTBP::nSample500];  
  Float_t  *AccidentalDetECalibConst;
  Float_t  *AccidentalDetTCalibConst;

  // for new peak finding method
  Int_t     DetUpdatedNumber;
  Int_t    *DetUpdatedModID;
  Short_t  *DetUpdatedNHit;
  Float_t (*DetUpdatedEne)[MTBP::NMaxHitPerEvent];
  Float_t (*DetUpdatedTime)[MTBP::NMaxHitPerEvent];

  // waveform analysis
  Float_t (*DetWfm)[MTBP::nSample500];
  Float_t (*DetSuppWfm)[MTBP::nSample500];
  int     DetSuppWfmNumber;
  int     *DetSuppWfmModID;
  
  pEnergyTime energyTime;
  Int_t ClusteringMtime( std::vector< pEnergyTime >& inputEtVec, 
			 std::vector< pEnergyTime >& outputEtVec, double pitch = 0.1 );//pitch [ns]

  Bool_t   EnableAccidentalOverlay;
  Long64_t AccidentalEntryID;//should be static
  Float_t  AccidentalTimeShift;

  /// not used for 500MHz
  virtual void SetOnlineClusNominalSample(){ ; }
  virtual void SetOnlineClusPeakThreshold(){ ; }

  std::string E14BPConfFile;
  Double_t NominalAverageClusterTime_Data;
  Double_t NominalAverageClusterTime_MC;
  Double_t DetZPosition;
  Int_t    NDeadChannel;
  std::vector<Int_t> DeadChannelIDVec;
  Int_t    NDeadModule;
  std::vector<Int_t> DeadModuleIDVec;
  Double_t DetSmearingSigma;

  Float_t DetChByChNominalTime[4096];
  Float_t DetChByChMinPeakHeight[4096];

};

#endif //__MTCONVERT500MHzDETECTOR_H__
