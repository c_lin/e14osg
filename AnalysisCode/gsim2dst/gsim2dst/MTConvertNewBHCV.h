#ifndef __MTCONVERTNewBHCV_H__
#define __MTCONVERTNewBHCV_H__

#include <iostream>

#include <TFile.h>
#include <TH1F.h>
#include <TROOT.h>
#include <TRandom3.h>
#include <TMath.h>

#include "MTConvert500MHzDetector.h"


class MTConvertNewBHCV : public MTConvert500MHzDetector
{
public:
  MTConvertNewBHCV( Int_t userFlag );
  ~MTConvertNewBHCV();
  
  void ConvertFromDigiWithResponse();
  void GetTrueInfo(             Int_t& number, Int_t* modID, Short_t* nhit,
				Float_t (*ene) [MTBP::NMaxHitPerEvent], 
				Float_t (*time)[MTBP::NMaxHitPerEvent] );
  void GetFromDigiWithResponse( Int_t& number, Int_t* modID, Short_t* nhit,
				Float_t (*ene) [MTBP::NMaxHitPerEvent], 
				Float_t (*time)[MTBP::NMaxHitPerEvent] );
  
private:
  TFile *f_Jitter;
  TH1F  *h_TimeJitter[3];

  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERTNewBHCV_H__
