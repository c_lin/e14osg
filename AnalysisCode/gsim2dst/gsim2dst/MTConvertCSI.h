#ifndef __MTCONVERTCSI_H__
#define __MTCONVERTCSI_H__

#include <fstream>
#include <iostream>
#include <unistd.h>
#include <cmath>
#include <cstdlib>
#include <TRandom3.h>
#include <TFile.h>
#include "MTConvert125MHzDetector.h"
#include "MTAnalysisLibrary/MTFunction.h"
#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include "MTAnalysisLibrary/MTPositionHandler.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTConvertCSI : public MTConvert125MHzDetector
{
public:
  MTConvertCSI( Int_t userFlag, Int_t BremsstrahlungFlag );
  ~MTConvertCSI();
  void Initialize();
  void GetFromMTime(               Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithResponse(   Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time,
				   Float_t* PTime, Float_t* FallTime );
  void GetTrueInfo(                Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  void AddAccidental(); // old method
  void AddAccidental(              Int_t& number, Int_t* modID, Float_t* ene, Float_t* time ); // old method
  void ApplyUTCAmpEffect();
  
private:

  //-------------------------------------------------
  // MPPC instances
  int DetectorNCHout;
  static const int Nsum = 256;
  static const int Ncry = 2716;
  int sumIndex_as_modID[Ncry];
  double MPPCtoffset[Nsum*4];
  double MPPCtADCoffset[Ncry];
  double sumtoffset[Nsum];

  static const int Nparzt = 5;
  double *par_ztMPPC[Ncry];
  double *par_ztPMT[Ncry];
  double *par_LYMPPC[Ncry];
 
  // This factor represents typical integrated ADC for the cosmic ray for 25mm crystals.
  // factor_LY_ADCtypical --> unit is [ADC counts]/[28MeV]
  constexpr static const double factor_LYADCtypical = 1360;
  
  // ratio b.t.w sumADC and peak
  double sumADCtopeakratio;

  // Due to the distortion of waveform at the amplifier,
  // the MPPC time shifts in negative side as the peak height becomes large.
  // Tshift(ns) = ampoffset_grad(ns/ADC)*peakvalue(ADC)
  double ampoffset_grad;
  
  static const int NparWfmMPPC = 7;
  static const int NbinzWfm = 6;
  double *par_WfmMPPC[Ncry];
  double fVop[Ncry];

  //-------------------------------------------------


  MTPulseGenerator* pulseGenerator;
  MTPositionHandler* pHandler;
  double  *groundNoise; // [MeV]
  double  *temporalGroundNoise;
  double  *misCalibrationFactor;
  double (*lightYield)[20];
  double  *meanLightYield;

  bool   OnlineTrigger( int runNo=49 );
  double GetVisibleTime( double z, double t, bool sl ); // small:true, large:false
  double GetVisibleTimeMPPC( double z, double t, int modID );
  double GetRelativeLYMPPC( int modID, double z);

  double GetTfromZ( double z, double* par );
  TF1* MPPCFunc;

  double offset_Wfm[Ncry][NbinzWfm];


  int solv_peak(double x0, double x1, double& xp, double& yp, TF1* MPPCFunc);
  int solv_CFT(double x0, double x1, double yhalf, double& xp, TF1* MPPCFunc);
  


  ///// LoadGroundNoise() :
  ///// groundNoise loaded here is not used in GetFromMTimeWithPulseShape.
  ///// this is for old analysis( GetFromMTimeWithResponse ).
  bool   LoadGroundNoise();

  ///// LoadMisCalibrationFactor() :
  ///// misCalibrationFactor loaded here is used in GetFromMTimeWithPulseShape and GetFromMTimeWithResponse.
  bool   LoadMisCalibrationFactor( Int_t BremsFlag );

  ///// LoadLightYield() :
  ///// meanLightYield loaded here is used in GetFromMTimeWithPulseShape and GetFromMTimeWithResponse.
  bool   LoadLightYield();

  float  GetSmearedTime( float e, float t ) const; // e [MeV], t [ns]
  float  GetTQCorrectedTime( float e, float t ) const; // e [MeV], t [ns]

  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};
#endif //__MTCONVERTCSI_H__
