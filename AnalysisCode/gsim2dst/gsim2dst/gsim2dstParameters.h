#ifndef __GSIM2DSTPARAMETERS_H__
#define __GSIM2DSTPARAMETERS_H__
#include <iostream>
#include <string>
#include <TMath.h>

#include "MTAnalysisLibrary/MTBasicParameters.h"

namespace G2DP{

  Float_t AccTimeShift(Int_t DetID)
  {
    switch( DetID ){
    case MTBP::UCV  : return -66;
    case MTBP::UCVLG: return 0;
    case MTBP::CC03 : return 40.487;
    case MTBP::CC04 : return 15.40;
      
    case MTBP::CC05 : return -13.1;
    case MTBP::CC06 : return -10.6;
    case MTBP::CBAR : return 51.34;
    case MTBP::FBAR : return 49.09;
      
    case MTBP::CSI  : return 30.6098;
    case MTBP::BHPV : return -67.3;
    case MTBP::CV   : return 65.46;
    case MTBP::BCV  : return 50.89;
      
    case MTBP::BHCV : return -82;
    case MTBP::NCC  : return 34.059;
    case MTBP::OEV  : return 27.02;
    case MTBP::LCV  : return 24.91;

    case MTBP::BPCV       : return 25.44;
    case MTBP::newBHCV    : return -81.2;
    case MTBP::BHGC       : return -64.7;

    case MTBP::IB     : return -25.13;
    case MTBP::MBCV   : return 29.99;
    case MTBP::IBCV   : return 20.29;
    case MTBP::DCV   : return 18.68+18.0-2.2;

    case MTBP::COSMIC     : return 0;
    case MTBP::LASER      : return 0;
    case MTBP::ETC        : return 0;
    case MTBP::TRIGGERTAG : return 0;
    default               : return 0;
    }
  }

}
#endif  //__GSIM2DSTPARAMETERS_H__
