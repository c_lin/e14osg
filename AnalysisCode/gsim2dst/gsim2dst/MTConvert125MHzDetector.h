#ifndef __MTCONVERT125MHZDETECTOR_H__
#define __MTCONVERT125MHZDETECTOR_H__

#include "MTConvertDetector.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

struct EnergyTimePos{
	float energy;
	float time;
	float pos[3];
};


class MTConvert125MHzDetector : public MTConvertDetector
{
 public:
  MTConvert125MHzDetector( Int_t detID, Int_t userFlag );
  ~MTConvert125MHzDetector();

  void         SetBranchAddress( TTree* tree );
  void         SetAccidentalOverlayDisable(){ EnableAccidentalOverlay = false; }
  void         SetAccidentalBranchAddress( TTree* tree );
  void         Branch( TTree* tree );
  virtual void  AddFTTBranch( TTree* tree); 
  virtual void  AddAreaRBranch( TTree* tree); 
 
  void         Convert(){ ConvertFromMTimeWithPulseShape(); }
  void         AccidentalOverlay(){ AddAccidentalWfm(); }
  virtual void Initialize();
  
  Int_t        GetNDigi();
  
  virtual void ConvertFromDigi();
  virtual void ConvertFromDigiWithResponse();
  virtual void ConvertFromMTime();
  virtual void ConvertFromMTimeWithPulseShape();
  virtual void ConvertFromMTimeWithResponse();
  virtual void ConvertTrueInfo();

  virtual void GetFromDigi(                Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  virtual void GetFromDigiWithResponse(    Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  virtual void GetFromMTime(               Int_t& number, Int_t* modID, Float_t* ene, Float_t* time ) {};
  virtual void GetFromMTimeWithPulseShape( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, 
					   Float_t* PTime, Float_t* FallTime ) {};
  virtual void GetFromMTimeWithResponse(   Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  virtual void GetTrueInfo(                Int_t& number, Int_t* modID, Float_t* ene, Float_t* time ) = 0;
  TClonesArray* GetDigiArray();
  TClonesArray* GetMTimeArray();

  
  virtual void AddAccidental();
  virtual void AddAccidental(    Int_t& number, Int_t* modID, Float_t* ene, Float_t* time );
  virtual void AddAccidentalWfm();
  virtual void AddAccidentalWfm( Int_t& number, Int_t* modID, Float_t* ene, Float_t* time, 
				 Float_t* PTime, Float_t* FallTime, 
				 Float_t (*wfm)[MTBP::nSample125] );
  void         SetAccidentalEntryNumber( Long64_t entry );
  void         SetAccidentalTimeShift( Float_t t ); // [ns]
  float        GetAccidentalTimeShift() const;
  virtual void ZeroSuppress( Float_t threshold = 1. );//do not record hits with energy within +/-1MeV
  virtual void ZeroSuppress( Int_t& number, Int_t* modID, Float_t* ene, Float_t*time, 
			     Float_t* PTime, Float_t* FallTime, Float_t (*wfm)[MTBP::nSample125], 
			     Float_t threshold = 1. );//do not record hits with energy within +/-1MeV
  
  
  
  //for waveform analysis 
  Double_t WfmTimeShift;
  Double_t WfmSigma0;
  Double_t WfmAsymPar;
  Double_t DetPeakHeightThre;//name changed from "EneThreParabolaTime" 
  Double_t DetNominalPeakTime;//name changed from "NominalPeakTime"

  Int_t    nSampleAverage;//need to be odd number added 
  Int_t    nSampleOneSide;

  TF1    *WfmFunc;
  void    AddWfmFromFunc( Float_t* wfm, TF1* func );
  Float_t GetIntegralFromFunc( TF1* func );

  void    GenerateWfmFromMTimeVec( Int_t iCh, const std::vector<pEnergyTime>& propagatedVec, 
				   Float_t AdditionalTimeShift = 0 , TF1* fnc=NULL);
  
  void    GenerateDbWfmFromMTimeVec( Int_t iCh, const std::vector<pEnergyTime>& propagatedVec,
                                     Float_t AdditionalTimeShift = 0 );


  void    GetEnergyTimeFromWfm( Float_t* wfm, Float_t& energy,
				Float_t& time, Float_t& PTime, Float_t& FallTime,
				Int_t ChIndex, Bool_t ApplyT0Calib = true );

  void    GetAreaRFromWfm();
  void    GetAreaRFromWfm( const Float_t* wfm, Float_t& areaR );

  void SetDetectorEventData(GsimDetectorEventData *DED);
  
  //updated at 19th Jul, 2014 (threshold and nominal time is taken iside the function)
  
  void SuppressWfm();


  virtual void GetEtWfm();
  void EvalFTTChisq( const Int_t ChIndex, const Float_t *wfm, Float_t &FTTChisq, Float_t &FTTTime);  
 
  Int_t GetOnlineClusterNumber() const;

  void SetFileNameE14BasicParam( std::string InputFileName);
  void SetE14BasicParamFromRootFile( std::string InputFileName);
  void SetE14BasicParamFromCode();

  static short CorrectCorruptedWfm_type1(std::vector<short> &data);
  static short CorrectCorruptedWfm(std::vector<short> &data, int runID, std::string detname, int chID );
  static short CorrectCorruptedWfm_type2(std::vector<short> &data, int maxbitloss);

 protected:
  const Int_t   DetectorID;
  const Char_t *DetectorName;
  Int_t   DetectorNCH;
  const Int_t   m_userFlag;

  TTree *GsimEventTree;
  TTree *DstTree;
  TTree *AccidentalTree;
  GsimDetectorEventData *DetectorEventData;


  std::map< Int_t, unsigned int> IDIndexMap;   // key : modID, value : dst array index
  Int_t                         *IndexIDArray; // modID array for dst.

  // same format as dst file
  Int_t     DetNumber;
  Int_t    *DetModID;
  Float_t  *DetEne;
  Float_t  *DetTime;    //forward CFTime (conventional one, no limit for maximum point)
  Float_t  *DetPTime;   //parabola time
  Float_t  *DetFallTime;//backward CFTime
  Float_t  *DetAreaR;		//area ratio
  Float_t  *DetSmoothness;//
  short    *DetWfmCorrectNumber;//
  Float_t (*DetWfm)[MTBP::nSample125];
  Long64_t  DetEtSumWfm[MTBP::nSample125];

  Float_t (*DetSuppWfm)[MTBP::nSample125];
  int     DetSuppWfmNumber;
  int     *DetSuppWfmModID;

  // MC true information 
  /* 
     For single readout detectors : same as MC.
     For dual readout detectors   : oposite channel has 0.
     For OEV                      : sum of 2 modules output which are read by 1 PMT.
     For CC04                     : not decided yet.
  */
  Int_t     DetTrueNumber;
  Int_t    *DetTrueModID;
  Float_t  *DetTrueEne;
  Float_t  *DetTrueTime;
  Float_t  *DetFTTChisq;
  Float_t  *DetFTTTime;

  // Accidental data
  Int_t     AccidentalDetNumber;
  Int_t    *AccidentalDetModID;
  Float_t  *AccidentalDetEne;
  Float_t  *AccidentalDetTime;
  Float_t  *AccidentalPedestal;
  Short_t (*AccidentalWfm)[MTBP::nSample125];
  Float_t  *AccidentalDetECalibConst;
  Float_t  *AccidentalDetTCalibConst;


  Bool_t    EnableAccidentalOverlay;
  Long64_t  AccidentalEntryID;//should be static
  Float_t   AccidentalTimeShift;


  Int_t  PeakSearch( Float_t* wfm, std::vector<MYParabolaTimeContainer>& ParabolaTimeVec,
		     Float_t EneThre, Float_t NominalTime );
  void   GetParabolaParam( Float_t* wfm, Double_t* param );
  void   GetEneThreAndNominalTime( Int_t ChIndex, Float_t& EneThre, Float_t& NominalTime );

  pEnergyTime energyTime;
  Int_t ClusteringMtime( std::vector< pEnergyTime >& inputEtVec, 
			 std::vector< pEnergyTime >& outputEtVec, double pitch = 0.1 );//pitch [ns]
  
  Int_t ClusteringMtime( std::vector< EnergyTimePos >& inputEtVec, 
			 std::vector< EnergyTimePos >& outputEtVec, double pitch = 0.1 );//pitch [ns]
  
  Double_t EtSumOnlineTrig;

  void FillClusterBit( const Int_t ChIndex, const Float_t *wfm );

  virtual void SetOnlineClusNominalSample();
  virtual void SetOnlineClusPeakThreshold();

  std::vector<Double_t> m_clusPeakThresholdVec;

  std::string E14BPConfFile;
  Double_t NominalAverageClusterTime_Data;
  Double_t NominalAverageClusterTime_MC;
  Double_t DetZPosition;
  Int_t    NDeadChannel;
  std::vector<Int_t> DeadChannelIDVec;
  Int_t    NDeadModule;
  std::vector<Int_t> DeadModuleIDVec;
  Double_t DetSmearingSigma;

  Float_t DetChByChNominalTime[4096];
  Float_t DetChByChMinPeakHeight[4096];


 private:
  E14BasicParamManager::E14BasicParamManager *m_E14BP;

};

#endif //__MTCONVERT125MHZDETECTOR_H__
