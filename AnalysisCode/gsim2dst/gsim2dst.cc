#include "gsim2dst.h"

Int_t gsim2dst( const Char_t* InputFileName, const Char_t* OutputFileName, 
		const Char_t *AccidentalFileName, const Int_t NAccidentalFiles, const Int_t Seed, 
                const std::string DbWfmFileDir, const std::string FTTFileDir,
		const std::string E14BasicParamConfFile,
		const Int_t userFlag, const Int_t WFMsaveflag, const int ApplyWfmCorrection,
		int argc, char** argv );

Int_t main(Int_t argc,char** argv)
{
  if( argc != 7 && argc!=8 && argc!=10 && argc!=11 && argc!=12){
    std::cout << "Argument error !" << std::endl;
    std::cout << "usage1 : " << std::endl;
    std::cout << "        " << argv[0] << " InputFileName OutputFileName AccidentalFileName AccidentalFileID Seed (Seed<0 disables accidental overlay) userFlag(default: 20151101) WFMsaveflag" << std::endl;
    std::cout<< " Usage2 : " <<std::endl;
    std::cout<< "         " << argv[0] << " InputFileName OutputFileName AccidentalFileName AccidentalFileID Seed (Seed<0 disables accidental overlay) userFlag DbWfmFileDir (set -DISABLE to disable it) FTTFileDir (set -DISABLE to disable it) WFMsaveflag" << std::endl;
    std::cout<< " Usage3 : " <<std::endl;
    std::cout<< "         " << argv[0] << " InputFileName OutputFileName AccidentalFileName AccidentalFileID Seed (Seed<0 disables accidental overlay) userFlag DbWfmFileDir (set -DISABLE to disable it) FTTFileDir (set -DISABLE to disable it) E14BasicParamConfFile WFMsaveflag" << std::endl;
    return -1;
  }


  std::cout << "gsim2dstwWFM starts." << std::endl;
  
  Char_t *InputFileName     = argv[1];
  Char_t *OutputFileName    = argv[2];
  Char_t *AccidentalFileName = argv[3];
  Int_t   AccidentalFileID  = atoi(argv[4]);
  Int_t   Seed              = atoi(argv[5]);
  Int_t   userFlag=20151101;
  int WFMsaveflag = 0;

  if( argc>=7 ) userFlag     = atoi(argv[6]);
  if( argc==8 ) WFMsaveflag  = atoi(argv[7]);

  std::string DbWfmFileDir      = "-DISABLE"; 
  std::string FTTFileDir        = "-DISABLE";
  if( argc >= 9 ){
    DbWfmFileDir      = argv[7];
    FTTFileDir        = argv[8];
  }
  if( argc==10 ) WFMsaveflag  = atoi(argv[9]);
  
  std::string E14BasicParamConfFile = "-DISABLE";
  if( argc == 11 ){
    E14BasicParamConfFile = argv[9];
    WFMsaveflag = atoi(argv[10]);
  }  
  const int ApplyWfmCorrection = 1;

  std::cout << "InputFile  : "        << InputFileName     << std::endl;
  std::cout << "OutputFile : "        << OutputFileName    << std::endl;
  std::cout << "AccidentalFileName : " << AccidentalFileName << std::endl;
  std::cout << "AccidentalFileID  : " << AccidentalFileID  << std::endl;
  std::cout << "Seed : "              << Seed              << std::endl;
  std::cout << "DbWfmFileDir      : " << DbWfmFileDir      << std::endl;
  std::cout << "FTTFileDir        : " << FTTFileDir        << std::endl;
  std::cout << "E14BasicParamConfFile : " << E14BasicParamConfFile << std::endl;
  std::cout << "userFlag : "          << userFlag          << std::endl;
  std::cout << "WFMsaveFlag : "       << WFMsaveflag       << std::endl;
  std::cout << "ApplyWfmCorrection : "       << ApplyWfmCorrection  << std::endl;
  if( userFlag<20150101 || 21120903<userFlag ){
    std::cout<<"Unexpected userFlag is used! The userFlag format is yyyymmdd. Used userFlag: "<<userFlag<<std::endl;
    return -1;
  }

  gsim2dst( InputFileName, OutputFileName, AccidentalFileName, AccidentalFileID, Seed, 
		  DbWfmFileDir, FTTFileDir, E14BasicParamConfFile, userFlag, WFMsaveflag, ApplyWfmCorrection, argc, argv );
  
  std::cout << "finish!" << std::endl;
  std::cout << std::endl;  
  return 0;
}

//////////////////////////////////////////////////
//////////////////////////////////////////////////
Int_t gsim2dst( const Char_t* InputFileName, const Char_t* OutputFileName,
                const Char_t *AccidentalFileName, const Int_t AccidentalFileID, const Int_t Seed,
                const std::string DbWfmFileDir, const std::string FTTFileDir,
				const std::string E14BasicParamConfFile,
				const Int_t userFlag, const Int_t WFMsaveflag, const int ApplyWfmCorrection,
                int argc, char** argv )
{
  ///// Input preparation
  TChain* itree = new TChain( "eventTree00" );
  itree->Add( InputFileName );
  const Long64_t nentry = itree->GetEntries();
  const Long64_t in     = nentry / 100;
  if( nentry==0 ){
    std::cout << "This file has no events." << std::endl;
    return -1;
  }

  ///// Accidental preparation
  const Bool_t EnableAccidentalOverlay = !( Seed < 0 );
  if( EnableAccidentalOverlay ){
    gRandom->SetSeed( Seed );
    std::cout << "Enable Accidental Overlay." <<std::endl;
  }
  else{
    gRandom->SetSeed( -Seed );
    std::cout << "Disable Accidental Overlay."<< std::endl;
  }  
  
  ///// Output preparation
  TFile* ofile = new TFile( OutputFileName, "RECREATE" );

  // add version information of E14 Analysis Suite
  //E14ProdLibrary::E14ProdInfoWriter::GetInstance()->Write(itree->GetFile(), ofile, argc, argv);

  //// Get shower library information
  TChain *CommandTree = new TChain("commandTree");
  CommandTree->Add( InputFileName );
  Int_t  tmpBremsstrahlungFlag = 0;
  Char_t command[256];
  CommandTree->SetBranchAddress("command",command);
  const Int_t nCommand = CommandTree->GetEntries();
  for(int iCommand=0;iCommand<nCommand;iCommand++){
    CommandTree->GetEntry(iCommand);
    if( strstr(command,"/GsimPhysicsListFactory/GsimQGSP_BERT/setBremsModel") ){
      if( strstr(command,"SBM") ){ tmpBremsstrahlungFlag = 0; } //Seltzer-Berger brems. model (default)
      else if( strstr(command,"eBrems") ){ tmpBremsstrahlungFlag = 1; } // eBrems model
      else{ tmpBremsstrahlungFlag = 0; } // default
    }
  }
  const Int_t BremsstrahlungFlag = tmpBremsstrahlungFlag;
  std::cout<<"BremsstrahlungFlag: "<<BremsstrahlungFlag<<std::endl;
  
  //// Set enable E14BasicParamConfFile flag
  Bool_t EnableSetE14BasicParamFromRootFile = false;
  if( E14BasicParamConfFile!="-DISABLE" ) EnableSetE14BasicParamFromRootFile = true;

  TTree* otree = new TTree( "T", "output from gsim2dst" );
  
  ///// Set Timing Offset
  TTree*  tmontr = new TTree("OffsetTr","t0 offset for tmon data");
  Float_t TimeOffset[MTBP::nDetectors];
  tmontr->Branch("TimeOffset",TimeOffset,Form("TimeOffset[%d]/F",MTBP::nDetectors));
  for( Int_t iDet = 0 ; iDet < MTBP::nDetectors ; ++iDet )
    //    TimeOffset[iDet] = MTBP::AccTimeShift[iDet];
    TimeOffset[iDet] = G2DP::AccTimeShift(iDet);
  tmontr->Fill();
  
  Int_t    GsimGenEventID     = 0;
  Int_t    GsimEntryID        = 0;
  Int_t    MCEventID          = 0;///// obsolete. this is replaced to "GsimEntryID"
  //std::string *b_AccidentalFileName = new std::string;
  Int_t        b_AccidentalFileID   = AccidentalFileID;
  Long64_t     AccidentalEntryID  = -1;
  Int_t        AccidentalRunID    = -1;
  Bool_t       AccidentalOverlayFlag = EnableAccidentalOverlay;
  otree->Branch( "GsimGenEventID",        &GsimGenEventID,        "GsimGenEventID/I" );
  otree->Branch( "GsimEntryID",           &GsimEntryID,           "GsimEntryID/I" );
  otree->Branch( "MCEventID",             &MCEventID,             "MCEventID/I" );
  //otree->Branch( "AccidentalFileName",     AccidentalFileName,    "AccidentalFileName/C");
  otree->Branch( "AccidentalFileID",      &b_AccidentalFileID,    "AccidentalFileID/I" );
  otree->Branch( "AccidentalEntryID",     &AccidentalEntryID,     "AccidentalEntryID/L" );
  otree->Branch( "AccidentalRunID",       &AccidentalRunID,       "AccidentalRunID/I" );
  otree->Branch( "AccidentalOverlayFlag", &AccidentalOverlayFlag, "AccidentalOverlayFlag/O" );
  
  ///// EventData for GsimGenEventID
  GsimEventData *EventData = new GsimEventData();
  itree->SetBranchAddress("Event.",&EventData);

  ///// Set GenParticle 
  MTConvertGenParticle *genParticleConverter = new MTConvertGenParticle( userFlag );
  genParticleConverter->SetBranchAddress( itree );
  genParticleConverter->Branch( otree );

  ///// record userFlag
  Int_t RecUserFlag=userFlag;
  otree->Branch( "userFlag", &RecUserFlag, "userFlag/I" );
 
  ///// Set Accidental File
//  const Int_t NAccidentalFilesInChain = 3;
  TChain *AccidentalTree = new TChain("T");
//  Int_t   AccidentalFileIDMap[NAccidentalFilesInChain];
//  for( Int_t i = 0 ; i < NAccidentalFilesInChain ; ++i ){
//    AccidentalFileIDMap[i] = (Int_t)gRandom->Uniform( NAccidentalFiles );
//    AccidentalTree->Add(Form("%s/Waveform_%d.root",AccidentalFileName,AccidentalFileIDMap[i]));
//  }

  AccidentalTree->Add( AccidentalFileName );

  AccidentalTree->SetBranchAddress("RunID",&AccidentalRunID);
  Long64_t nAccidentalEntries       = AccidentalTree->GetEntries();
  Long64_t AccidentalEntryIDOfChain = (Int_t)( nAccidentalEntries - gRandom->Uniform(nAccidentalEntries) );

  ///// Detector preparation
  Bool_t         DetectorExistFlag[MTBP::nDetectors];
  MTConvertDetector*   detConverter[MTBP::nDetectors];
  detConverter[MTBP::FBAR]    = new MTConvertFBAR(userFlag);
  detConverter[MTBP::NCC]     = new MTConvertNCC( userFlag);
  detConverter[MTBP::CBAR]    = new MTConvertCBAR(userFlag);
  detConverter[MTBP::BCV]     = new MTConvertBCV( userFlag);
  MTConvertCSI* CsIConverter  = new MTConvertCSI( userFlag, BremsstrahlungFlag);
  detConverter[MTBP::CSI]     = CsIConverter;
  detConverter[MTBP::CV]      = new MTConvertCV(  userFlag);
  detConverter[MTBP::LCV]     = new MTConvertLCV( userFlag);
  detConverter[MTBP::OEV]     = new MTConvertOEV( userFlag);
  detConverter[MTBP::CC03]    = new MTConvertCC03(userFlag);
  detConverter[MTBP::CC04]    = new MTConvertCC04(userFlag);
  detConverter[MTBP::CC05]    = new MTConvertCC05(userFlag);
  detConverter[MTBP::CC06]    = new MTConvertCC06(userFlag);
  detConverter[MTBP::BPCV]    = new MTConvertBPCV(userFlag);
  detConverter[MTBP::BHCV]    = new MTConvertBHCV(userFlag);
  detConverter[MTBP::BHPV]    = new MTConvertBHPV(userFlag);
  detConverter[MTBP::BHGC]    = new MTConvertBHGC(userFlag);
  detConverter[MTBP::newBHCV] = new MTConvertNewBHCV(userFlag);
  detConverter[MTBP::IB]      = new MTConvertIBAR(userFlag);
  detConverter[MTBP::IBCV]    = new MTConvertIBCV(userFlag);
  detConverter[MTBP::MBCV]    = new MTConvertMBCV(userFlag);
  detConverter[MTBP::DCV]     = new MTConvertDCV(userFlag);
  detConverter[MTBP::UCV]     = new MTConvertUCV(userFlag);
  detConverter[MTBP::UCVLG]   = new MTConvertUCVLG(userFlag);
  std::cout << "Finish creating detector converter..." <<std::endl;

  for( Int_t idet = 0 ; idet < MTBP::nDetectors ; ++idet ){    
    ///// Check the existence of the detector.
    ///// If it doesn't exist, DetectorHandler of the detector is not generated.
    
    std::string detname = MTBP::GsimE14DetName[idet].c_str();
    if(detname=="UCVLG") detname = "UCV";

    if( itree->GetBranch(Form("%s.", detname.c_str())) ){
      DetectorExistFlag[idet] = true;
      std::cout << "Veto Detector : " << MTBP::GsimE14DetName[idet].c_str() << " exists." << std::endl;
    
      if((WFMsaveflag>>idet)&0x1){
	std::cout << "Waveform of " << MTBP::GsimE14DetName[idet].c_str() << " is saved\n"; 
	detConverter[idet]->SetWFMSaveFlag(true);
      }else detConverter[idet]->SetWFMSaveFlag(false);
	
    }else{
      DetectorExistFlag[idet] = false;
      std::cout<< "Veto Detector : " << MTBP::GsimE14DetName[idet].c_str() <<" doesn't exist." << std::endl;
      continue;
    }

    switch( idet ){
    case MTBP::CC03:
    case MTBP::CC04:
    case MTBP::CC05:
    case MTBP::CC06:
    case MTBP::CBAR:
    case MTBP::FBAR:
    case MTBP::CSI:
    case MTBP::CV:
    case MTBP::BCV:
    case MTBP::NCC:
    case MTBP::OEV:
    case MTBP::DCV:
    case MTBP::LCV:
    case MTBP::BPCV:
    case MTBP::BHCV:
    case MTBP::BHPV:
    case MTBP::BHGC:
    case MTBP::newBHCV:
    case MTBP::IB:
    case MTBP::IBCV:
    case MTBP::MBCV:
    case MTBP::UCV:
    case MTBP::UCVLG:
      if( !EnableAccidentalOverlay )
		  detConverter[idet]->SetAccidentalOverlayDisable();
      detConverter[idet]->SetAccidentalBranchAddress( AccidentalTree );
      if(ApplyWfmCorrection) detConverter[idet]->ApplyWfmCorrection = 1; 
      detConverter[idet]->SetBranchAddress( itree );
      detConverter[idet]->Branch( otree );
      if( EnableSetE14BasicParamFromRootFile ) 
	detConverter[idet]->SetE14BasicParamFromRootFile( E14BasicParamConfFile );
      else
	detConverter[idet]->SetE14BasicParamFromCode();
      break;
    default:
      break;
    }
  }
 
  // To share geant information between UCVLG and UCV.
  ((MTConvert125MHzDetector*)detConverter[MTBP::UCVLG])->
	  SetDetectorEventData(((MTConvert500MHzDetector*)detConverter[MTBP::UCV])->GetDetectorEventData());
 
  /// enable data base waveform simulation ///
  if( DbWfmFileDir!="-DISABLE" ){
     std::cout<<" Set Data-Base waveform simulation. " << std::endl;
     detConverter[MTBP::FBAR]->SetDbWfmDir( DbWfmFileDir );
     detConverter[MTBP::CV  ]->SetDbWfmDir( DbWfmFileDir );
     detConverter[MTBP::CC06]->SetDbWfmDir( DbWfmFileDir );
     detConverter[MTBP::CBAR]->SetDbWfmDir( DbWfmFileDir );
     detConverter[MTBP::NCC ]->SetDbWfmDir( DbWfmFileDir );
     detConverter[MTBP::IB  ]->SetDbWfmDir( DbWfmFileDir );
     /// Do not use for 2016-2018 analysis, C. Lin (2019.05.15)
     // detConverter[MTBP::CSI ]->SetDbWfmDir( DbWfmFileDir ); //
  }else{
     std::cout<<" Data-base waveform simulation is disabled. " << std::endl;
  }

  /// enable FTT chisq evaluation ///
  if( FTTFileDir!="-DISABLE"){
     std::cout<<" Set Fourier Transform template (FTT) algorithm. " << std::endl;
     detConverter[MTBP::FBAR]->SetFTTDir( FTTFileDir );
     detConverter[MTBP::CV  ]->SetFTTDir( FTTFileDir );
     detConverter[MTBP::CC06]->SetFTTDir( FTTFileDir );
     detConverter[MTBP::CBAR]->SetFTTDir( FTTFileDir );
     detConverter[MTBP::NCC ]->SetFTTDir( FTTFileDir );
     detConverter[MTBP::IB  ]->SetFTTDir( FTTFileDir );

     detConverter[MTBP::FBAR]->AddFTTBranch( otree );
     detConverter[MTBP::CV  ]->AddFTTBranch( otree );
     detConverter[MTBP::CC06]->AddFTTBranch( otree );
     detConverter[MTBP::CBAR]->AddFTTBranch( otree );
     detConverter[MTBP::NCC ]->AddFTTBranch( otree );
     detConverter[MTBP::IB  ]->AddFTTBranch( otree );
  }else{
     std::cout<<" Fourier Transform Template (FTT) is disabled. " << std::endl;
  }
  
  /// Stores AreaR ////
  detConverter[MTBP::CC06]->AddAreaRBranch( otree );
  detConverter[MTBP::CBAR]->AddAreaRBranch( otree );
  detConverter[MTBP::FBAR]->AddAreaRBranch( otree );
  detConverter[MTBP::CV]->AddAreaRBranch( otree );
  detConverter[MTBP::IB]->AddAreaRBranch( otree );



  /// online clustering simulation ///
  detConverter[MTBP::CSI]->EnableOnlineClusteringSimulation(userFlag);
  detConverter[MTBP::CSI]->AddOnlineClusterBranch(otree);
  Short_t CDTBit[38*38] = {0};
  otree->Branch( "CDTBit", CDTBit, "CDTBit[38][38]/S");
  
  ///
  std::cout << "# of entries in gsim file       : " << nentry << std::endl;
  std::cout << "# of entries in accidental file : " << nAccidentalEntries << std::endl;
//  std::cout << "     Accidental File ID : { ";
//  for( Int_t i = 0 ; i < NAccidentalFilesInChain ; ++i )
//    std::cout << AccidentalFileIDMap[i] << " ";
//  std::cout << "}"  << std::endl;
  
  ////////////////////////////////
  ////////// Loop Start //////////
  ////////////////////////////////
  std::cout<<"Start Loop Analysis"<<std::endl;
  for( Long64_t ientry = 0 ; ientry < nentry ; ++ientry ){
    if( nentry>100 )
      if( (ientry%in==0) && (ientry!=0)){
	if( ientry / in < 10 )
	  std::cout << " " << (ientry / in) << "%   "  << std::flush;
	else if( ientry % (in * 10) == 0 )
	  std::cout        << (ientry / in) << "% processed.  "  << std::endl;
	else
	  std::cout        << (ientry / in) << "%   " << std::flush;
      }
    itree->GetEntry( ientry );
    
    ////// If there are no hits in CSI, this event is ignored.
    if( detConverter[MTBP::CSI]->GetNDigi() == 0 ) continue;
    
    AccidentalTree->GetEntry( AccidentalEntryIDOfChain );

    ///// Set Accidental Information
    TFile *CurrentAccidentalFile = AccidentalTree->GetFile();
    //*AccidentalFileName = CurrentAccidentalFile->GetName();
    //AccidentalFileID    = AccidentalFileIDMap[ AccidentalTree->GetTreeNumber() ];
    //AccidentalFileID    = -1;
    AccidentalEntryID   = AccidentalEntryIDOfChain;
    
    ///// GenParticle Information
    genParticleConverter->Convert();

    ///// Detector Information
    for( Int_t idet = 0 ; idet < MTBP::nDetectors ; ++idet ){
      if( !DetectorExistFlag[idet] )
	continue;

      switch( idet ){

      case MTBP::CC03:
      case MTBP::CC04:
      case MTBP::CC05:
      case MTBP::CC06:
      case MTBP::CBAR:
      case MTBP::FBAR:
      case MTBP::CSI:
      case MTBP::CV:
      case MTBP::BCV:
      case MTBP::NCC:
      case MTBP::OEV:
      case MTBP::LCV:
      case MTBP::BPCV:
      case MTBP::BHCV:
      case MTBP::BHPV:
      case MTBP::BHGC:
      case MTBP::newBHCV:
      case MTBP::IB:
      case MTBP::IBCV:
      case MTBP::MBCV:
      case MTBP::DCV:
      case MTBP::UCVLG:
      case MTBP::UCV:
	detConverter[idet]->Initialize();
	detConverter[idet]->ConvertTrueInfo();
	detConverter[idet]->SetAccidentalTimeShift( TimeOffset[idet] );
	detConverter[idet]->Convert();
	if( EnableAccidentalOverlay )
	  detConverter[idet]->AccidentalOverlay();
	
	if(idet==MTBP::CSI) CsIConverter->ApplyUTCAmpEffect();
	detConverter[idet]->GetEtWfm();
	
	detConverter[idet]->SuppressWfm();
	
	if(idet!=MTBP::NCC && idet!=MTBP::newBHCV)
	detConverter[idet]->GetAreaRFromWfm();
	break;
      default:
	break;
      } 
    }

    ///// Get CDT Bit Map
    detConverter[MTBP::CSI]->SetCDTBitMap(CDTBit);

    ///// CSI Zero Suppression
    ///// discard hits within +/-1MeV
    detConverter[MTBP::CSI]->ZeroSuppress( 1. );
    
    GsimGenEventID = (Int_t)EventData->event_number;
    GsimEntryID    = ientry;
    MCEventID      = ientry;
    ofile->cd();
    otree->Fill();

    ///// Entry ID of accidental file is incremented.
    ///// if it reaches the end of the accidental tchain, it goes to the first entry of the chain. 
    ++AccidentalEntryIDOfChain;
    AccidentalEntryIDOfChain = ( AccidentalEntryIDOfChain % nAccidentalEntries ); 
  }
  
  ofile->cd();
  otree->Write();
  tmontr->Write();
  ofile->Close();
  
  return 0;
}
