/* -*- C++ -*- */
#ifndef E14ANA_E14ANAVERSIONDATA_E14ANAVERSION_H
#define E14ANA_E14ANAVERSIONDATA_E14ANAVERSION_H

#include <Rtypes.h>

namespace E14AnaVersion
{
  
  class E14AnaVersionData
  {
  private:
    E14AnaVersionData();
    ~E14AnaVersionData();
    E14AnaVersionData(const E14AnaVersionData&);
    E14AnaVersionData& operator=(const E14AnaVersionData&);

  public:
    static E14AnaVersionData* GetInstance();

  public:
    std::string VersionID()   const { return m_VersionID;   }
    Int_t       CompileDate() const { return m_CompileDate; }
    std::string CommitID()    const { return m_CommitID;    }
    Int_t       CommitDate()  const { return m_CommitDate;  }

  private:
    std::string m_VersionID;
    Int_t       m_CompileDate;
    std::string m_CommitID;
    Int_t       m_CommitDate;
    Bool_t      m_IsSet;

    ClassDef(E14AnaVersionData, 1);
  };
  
}

#endif
