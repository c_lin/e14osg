#include "E14AnaVersion/E14AnaVersionData.h"

ClassImp(E14AnaVersion::E14AnaVersionData)

namespace
{
  // E14AS_* are preprocessor macros set in Makefile
  const std::string e14ana_version_ID   = E14AS_VERSION_ID;
  const Int_t       e14ana_compile_date = E14AS_COMPILE_DATE;
  const std::string e14ana_commit_ID    = E14AS_COMMIT_ID;
  const Int_t       e14ana_commit_date  = E14AS_COMMIT_DATE;
}

namespace E14AnaVersion
{
  
  E14AnaVersionData::E14AnaVersionData()
    : m_VersionID(""), m_CompileDate(0), m_CommitID(""), m_CommitDate(0), m_IsSet(kFALSE)
  {
  }
  
  E14AnaVersionData::~E14AnaVersionData()
  {
  }
  
  E14AnaVersionData* E14AnaVersionData::GetInstance()
  {
    static E14AnaVersionData instance;

    if (!instance.m_IsSet) {
      instance.m_VersionID   = e14ana_version_ID;
      instance.m_CompileDate = e14ana_compile_date;
      instance.m_CommitID    = e14ana_commit_ID;
      instance.m_CommitDate  = e14ana_commit_date;
      
      instance.m_IsSet = kTRUE;
    }
    
    return &instance;
  }

}
