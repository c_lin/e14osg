my:
	@ cd E14AnaVersion; make my
	@ cd AnalysisLibrary; make my
	@ cd AnalysisCode; make my

install-headers-my:
	@ cd E14AnaVersion; make install-headers-my
	@ cd AnalysisLibrary; make install-headers-my

distclean:
	@ cd E14AnaVersion; make distclean
	@ cd AnalysisLibrary; make distclean
	@ cd AnalysisCode; make distclean
	@ rm -fr lib
	@ rm -fr include
	@ rm -fr bin
	@ rm -fr share
	@ rm -fr DoxygenOut
