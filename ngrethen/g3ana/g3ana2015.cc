#include <numeric>
#include "TFile.h"
#include "TChain.h"
#include "TMath.h"
#include "gnana/E14GNAnaFunction.h"
#include "gnana/E14GNAnaDataContainer.h"
#include "gamma/GammaFinder.h"
#include "rec2g/Rec2g.h"
#include "CLHEP/Vector/ThreeVector.h"

#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "MTAnalysisLibrary/MTPositionHandler.h"
#include "MTAnalysisLibrary/MTFunction.h"
#include "MTAnalysisLibrary/MTVetoFBAR.h"
#include "MTAnalysisLibrary/MTVetoNCC.h"
#include "MTAnalysisLibrary/MTVetoCBAR.h"
#include "MTAnalysisLibrary/MTVetoBCV.h"
#include "MTAnalysisLibrary/MTVetoCV.h"
#include "MTAnalysisLibrary/MTVetoOEV.h"
#include "MTAnalysisLibrary/MTVetoCC03.h"
#include "MTAnalysisLibrary/MTVetoCSI.h"
#include "MTAnalysisLibrary/MTVetoLCV.h"
#include "MTAnalysisLibrary/MTVetoCC04.h"
#include "MTAnalysisLibrary/MTVetoCC05.h"
#include "MTAnalysisLibrary/MTVetoCC06.h"
#include "MTAnalysisLibrary/MTVetoBHCV.h"
#include "MTAnalysisLibrary/MTVetoBHPV.h"
#include "MTAnalysisLibrary/MTVetoBPCV.h"
#include "MTAnalysisLibrary/MTVetoBHGC.h"
#include "MTAnalysisLibrary/MTVetonewBHCV.h"
#include "MTAnalysisLibrary/MTVetoIB.h"
#include "MTAnalysisLibrary/MTVetoIBCV.h"
#include "MTAnalysisLibrary/MTVetoMBCV.h"
#include "MTAnalysisLibrary/MTVetoDetector.h"
#include "MTAnalysisLibrary/MTGenParticle.h"

#include "SshinoSuppConfInfoManager/SuppConfInfoManager.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "E14ProdInfo/E14ProdInfoWriter.h"

bool user_rec(std::list<Gamma> const &glist, std::vector<Klong> &klVec);
void user_cut(E14GNAnaDataContainer &data, std::vector<Klong> const &klVec);

/*
// my cut set (2013.6.6)
enum{ DELTA_VERTEX_TIME=0, DELTA_KL_MASS,   MIN_HALFET,    KL_PT,
      KL_CHISQZ,           DELTA_PI0_MASS,  DELTA_PI0_Z,   MIN_GAMMAE,
      MAX_GAMMAE,          MIN_FIDUCIAL_XY, MAX_FIDUCIALR, MIN_CLUSTER_DISTANCE,
      KL_Z,                KL_BEAMEXIT_XY,  MAX_SHAPECHISQ };
*/
// my cyt set (2015/8/20)
enum{ DELTA_VERTEX_TIME=0, DELTA_KL_MASS,   CsIEt,         KL_PT,
      KL_CHISQZ,           DELTA_PI0_MASS,  DELTA_PI0_Z,   MIN_GAMMAE,
      MAX_GAMMAE,          MIN_FIDUCIAL_XY, MAX_FIDUCIALR, MIN_CLUSTER_DISTANCE, 
      KL_Z,                KL_BEAMEXIT_XY,  MAX_SHAPECHISQ };

double GetEventStartTime( std::vector<Klong> &klVec, double* vertexTime, const double CSIZPosition )
{
  double gammaHitTime[4] = {0.0};
  double gammaHitX[4]    = {0.0};
  double gammaHitY[4]    = {0.0};
  double gammaE[4]       = {0.0};
  double gammaTWeight[4]  = {0.0};

  Gamma const * pgamma[3] = { &klVec[0].pi0().at(0).g1(), &klVec[0].pi0().at(0).g2(), &(*klVec[0].gamma().begin())}; 
  
  for( int i=0; i<3; i++ ){
      Gamma const &gamma = *pgamma[i]; 
      gammaHitTime[i] = gamma.t();
      gammaHitX[i]    = gamma.pos().x();
      gammaHitY[i]    = gamma.pos().y();
      gammaE[i]       = gamma.e();
      gammaTWeight[i]  = pow( MTFUNC::TResolutionOfCluster( gamma.e() ), -2 );
  }

  double vertexZ = klVec[0].v().z();

  for( int igamma=0; igamma<3; igamma++ ){
    vertexTime[igamma] = gammaHitTime[igamma] - sqrt( pow( MTBP::CSIZPosition-vertexZ, 2) 
			+ pow( gammaHitX[igamma], 2) + pow( gammaHitY[igamma], 2) ) / (TMath::C()/1E6);
  }

  return MTFUNC::WeightedAverage( 3, vertexTime, gammaTWeight );

}

int g3ana( char* inputFile, char* outputFile, int userFlag, int argc, char** argv );

int main( int argc, char** argv)
{

  std::cout << "g3ana starts." << std::endl;

  // read argument  
  if( argc != 3 &&argc!=4 ){
    std::cout << "Argument Error ! :" << std::endl
	      << "\t usage : " << argv[0] << " InputFileName OutputFileName userFlag" << std::endl
	      << "\t input file should be the output of clusteringFromDst."  << std::endl;
    std::cout << "If there is no userFlag, default userFlag (20151101) will be set." << std::endl;
    return -1;
  }
  int UserFlag=20151101;
  if( argc==4 )UserFlag = atoi(argv[3]);
  std::cout<<"userFlag: "<<UserFlag<<std::endl;
  if( UserFlag<20150101 || 21120903<UserFlag ){
    std::cout<<"Unexpected UserFlag is used! The UserFlag format is yyyymmdd. Used userFlag: "<<UserFlag<<std::endl;
    return -1;
  }

  int result = g3ana( argv[1], argv[2], UserFlag, argc, argv );
  std::cout << "g3ana finished." << std::endl;
  std::cout << std::endl;
  return result;
}

int g3ana( char* inputFile, char* outputFile, int userFlag, int argc, char** argv )
{

  bool isRealData = true; // Run:true, Sim:false

  // set input file
  TChain *inputTree = new TChain( "clusteringTree" );
  inputTree->Add( inputFile );
  const long nEntries = inputTree->GetEntries();
  if( nEntries==0 ){
    std::cout << "This file has no events." << std::endl;
    return -1;
  }

  // E14BasicParam
  E14BasicParamManager::E14BasicParamManager *m_E14BP
    = new E14BasicParamManager::E14BasicParamManager();
  m_E14BP->SetDetBasicParam(userFlag,"CSI");
  const double CSIZPosition    = m_E14BP->GetDetZPosition();

  //CSIEt
  Double_t CSIEt      = 0.0;
  Double_t CSIHalfEtR = 0.0;
  Double_t CSIHalfEtL = 0.0;
  inputTree->SetBranchAddress( "CSIEt",      &CSIEt );
  inputTree->SetBranchAddress( "CSIHalfEtR", &CSIHalfEtR );
  inputTree->SetBranchAddress( "CSIHalfEtL", &CSIHalfEtL );

  // Run information
  Int_t RunID = 0;
  Int_t NodeID = 0;
  Int_t FileID = 0;
  Int_t DstEntryID = 0;
  Int_t ClusteringEntryID = 0;// additional branch

  Short_t SpillID = 0;
  Int_t   EventID = 0;
  Int_t   TimeStamp;
  UInt_t  L2TimeStamp;
  Short_t Error;
  Short_t TrigTagMismatch;
  Short_t L2AR;
  Short_t COE_Et_overflow;
  Short_t COE_L2_override;
  Int_t   COE_Esum;
  Int_t   COE_Ex;
  Int_t   COE_Ey;
  Bool_t  isGoodRun;
  Bool_t  isGoodSpill;
  UInt_t  DetectorBit,ScaledTrigBit,RawTrigBit;
  Int_t   CDTNum;
  Short_t CDTData[3][64];
  Short_t CDTBit[38][38];
  Short_t CDTVetoData[4][64];
  Short_t CDTVetoBitSum[13];


  inputTree->SetBranchAddress( "RunID",     &RunID );
  inputTree->SetBranchAddress( "NodeID",    &NodeID );
  inputTree->SetBranchAddress( "FileID",    &FileID );
  inputTree->SetBranchAddress( "DstEntryID",&DstEntryID );

  if(inputTree->GetBranch("DetectorBit")){
    inputTree->SetBranchAddress("SpillID",        &SpillID );
    inputTree->SetBranchAddress("EventID",        &EventID );
    inputTree->SetBranchAddress("TimeStamp",      &TimeStamp);
    inputTree->SetBranchAddress("L2TimeStamp",    &L2TimeStamp);
    inputTree->SetBranchAddress("Error",          &Error);
    inputTree->SetBranchAddress("TrigTagMismatch",&TrigTagMismatch);
    inputTree->SetBranchAddress("L2AR",           &L2AR);
    inputTree->SetBranchAddress("COE_Et_overflow",&COE_Et_overflow);
    inputTree->SetBranchAddress("COE_L2_override",&COE_L2_override);
    inputTree->SetBranchAddress("COE_Esum",       &COE_Esum);
    inputTree->SetBranchAddress("COE_Ex",         &COE_Ex);
    inputTree->SetBranchAddress("COE_Ey",         &COE_Ey);
    inputTree->SetBranchAddress("DetectorBit",    &DetectorBit);
    inputTree->SetBranchAddress("ScaledTrigBit",  &ScaledTrigBit);
    inputTree->SetBranchAddress("RawTrigBit",     &RawTrigBit);
    inputTree->SetBranchAddress("isGoodRun",      &isGoodRun);
    inputTree->SetBranchAddress("isGoodSpill",    &isGoodSpill);
    if(inputTree->GetBranch("CDTNum")){
      inputTree->SetBranchAddress("CDTNum",       &CDTNum);    
      inputTree->SetBranchAddress("CDTData",      CDTData);    
      inputTree->SetBranchAddress("CDTBit",       CDTBit);    
    }
    if(inputTree->GetBranch("CDTVetoData")){
      inputTree->SetBranchAddress("CDTVetoData",  CDTVetoData);    
      inputTree->SetBranchAddress("CDTVetoBitSum",CDTVetoBitSum);    
    }
  }

  // check sim or data
  Int_t    TruePID = 0; // mother particle ID
  Int_t    TrueDecayMode = 0;
  Int_t    GsimGenEventID = 0;
  Int_t    GsimEntryID = 0;
  Int_t    MCEventID = 0;//obsolete
  Double_t TrueVertexZ;
  Double_t TrueVertexTime;
  Double_t TrueMom[3];
  Double_t TruePos[3];
  Bool_t   IBWfmFlag=false;
  if( inputTree->GetBranch("IBWfm") ) IBWfmFlag = true;
  Float_t  IBWfm[64][256];
  MTGenParticle* genP = new MTGenParticle();

  std::string *AccidentalFileName = new std::string;
  Int_t        AccidentalFileID;
  Long64_t     AccidentalEntryID;
  Bool_t       AccidentalOverlayFlag;
  if( inputTree->GetBranch( "TruePID" ) ){
    isRealData = false;
    inputTree->SetBranchAddress("TruePID",       &TruePID );
    inputTree->SetBranchAddress("TrueDecayMode", &TrueDecayMode );
    inputTree->SetBranchAddress("TrueVertexZ",   &TrueVertexZ );
    inputTree->SetBranchAddress("TrueVertexTime",&TrueVertexTime );
    inputTree->SetBranchAddress("TrueMom",        TrueMom );
    inputTree->SetBranchAddress("TruePos",        TruePos );
    inputTree->SetBranchAddress("GsimGenEventID",    &GsimGenEventID );
    inputTree->SetBranchAddress("GsimEntryID",       &GsimEntryID );
    inputTree->SetBranchAddress("MCEventID",         &MCEventID );
    inputTree->SetBranchAddress("AccidentalFileName",   &AccidentalFileName );
    inputTree->SetBranchAddress("AccidentalFileID",     &AccidentalFileID );
    inputTree->SetBranchAddress("AccidentalEntryID",    &AccidentalEntryID );
    inputTree->SetBranchAddress("AccidentalOverlayFlag",&AccidentalOverlayFlag );
    genP->SetBranchAddress( inputTree );
    if( IBWfmFlag ) inputTree->SetBranchAddress("IBWfm",IBWfm);
  }

  E14GNAnaDataContainer data;
  data.setBranchAddress( inputTree );

  SshinoLib::SuppConfInfoManager *SuppConfInfoMan = NULL; 
  SuppConfInfoMan = new SshinoLib::SuppConfInfoManager( inputFile );
  
  // set output file
  TFile *otf = new TFile( outputFile, "RECREATE" );
  
  // add version information of E14 Analysis Suite
  E14ProdLibrary::E14ProdInfoWriter::GetInstance()->Write(inputTree->GetFile(), otf, argc, argv);
  
  // add peakfind and zero-suppression information
  SuppConfInfoMan -> WriteSuppConfInfo( otf );

  TTree *otr = new TTree( "RecTree", "output from g4ana");  
  otr->Branch( "RunID",             &RunID,             "RunID/I" );
  otr->Branch( "NodeID",            &NodeID,            "NodeID/I" );
  otr->Branch( "FileID",            &FileID,            "FileID/I" );
  otr->Branch( "DstEntryID",        &DstEntryID,        "DstEntryID/I" );
  otr->Branch( "ClusteringEntryID", &ClusteringEntryID, "ClusteringEntryID/I" );
  otr->Branch( "userFlag",          &userFlag,          "userFlag/I");

  if(isRealData){
    otr->Branch("SpillID",        &SpillID,        "SpillID/S" );
    otr->Branch("EventID",        &EventID,        "EventID/I" );
    otr->Branch("DetectorBit",    &DetectorBit,    "DetectorBit/i");
    otr->Branch("ScaledTrigBit",  &ScaledTrigBit,  "ScaledTrigBit/i");  
    otr->Branch("RawTrigBit",     &RawTrigBit,     "RawTrigBit/i");
    otr->Branch("TimeStamp",      &TimeStamp,      "TimeStamp/I");
    otr->Branch("L2TimeStamp",    &L2TimeStamp,    "L2TimeStamp/i");
    otr->Branch("Error",          &Error,          "Error/S");
    otr->Branch("TrigTagMismatch",&TrigTagMismatch,"TrigTagMismatch/S");
    otr->Branch("L2AR",           &L2AR,           "L2AR/S");
    otr->Branch("COE_Et_overflow",&COE_Et_overflow,"COE_Et_overflow/S");
    otr->Branch("COE_L2_override",&COE_L2_override,"COE_L2_override/S");
    otr->Branch("COE_Esum",       &COE_Esum,       "COE_Esum/I");
    otr->Branch("COE_Ex",         &COE_Ex,         "COE_Ex/I");
    otr->Branch("COE_Ey",         &COE_Ey,         "COE_Ey/I");
    otr->Branch("isGoodRun",      &isGoodRun,      "isGoodRun/O");
    otr->Branch("isGoodSpill",    &isGoodSpill,    "isGoodSpill/O");
    if(inputTree->GetBranch("CDTNum")){
      otr->Branch("CDTNum",       &CDTNum,         "CDTNum/I");
      otr->Branch("CDTData",      CDTData,         "CDTData[3][64]/S");
      otr->Branch("CDTBit",       CDTBit,          "CDTBit[38][38]/S");
    }
    if(inputTree->GetBranch("CDTVetoData")){
      otr->Branch("CDTVetoData",  CDTVetoData,     "CDTVetoData[4][64]/S");
      otr->Branch("CDTVetoBitSum",CDTVetoBitSum,   "CDTVetoBitSum[13]/S");
    }
  }


  data.branchOfKlong( otr );
  Double_t EventStartTime = 0.0;
  Double_t EventStartZ = 0.0;
  Double_t VertexTime[3] = {0.0};
  // Original cluster information
  Int_t OriginalClusterNumber = 0;
  Double_t* OriginalClusterE = new Double_t [256]();
  Double_t* OriginalClusterT = new Double_t [256]();
  Double_t (*OriginalClusterPos)[3] = new Double_t [256][3]();
  Double_t* OriginalClusterVertexT = new Double_t [256]();
  // My cut set and veto set
  Int_t MyCutCondition = 0;
  Int_t MyVetoCondition = 0;
  // Cut parameters
  Double_t DeltaVertexTime = 0;
  Double_t MaxDeltaPi0Mass = 0;
  Double_t TotalEt = 0;
  Double_t MinHalfEt = 0;
  Double_t MinGammaE = 0;
  Double_t MaxGammaE = 0;
  Double_t MaxFiducialR = 0;
  Double_t MinClusterDistance = 0;
  Double_t MinFiducialXY = 0;
  Double_t KLDeltaChisqZ = 0;
  Double_t MaxShapeChisq = 0;
  Double_t ExtraClusterDeltaVertexTime = 0;
  Double_t ExtraClusterEne = 0;
  Double_t KLBeamExitX = 0;
  Double_t KLBeamExitY = 0;
  Double_t AverageClusterTime = 0;
  otr->Branch( "CSIEt", &CSIEt, "CSIEt/D" );
  otr->Branch( "CSIHalfEtR", &CSIHalfEtR, "CSIHalfEtR/D" );
  otr->Branch( "CSIHalfEtL", &CSIHalfEtL, "CSIHalfEtL/D" );
  otr->Branch( "EventStartTime", &EventStartTime, "EventStartTime/D");
  otr->Branch( "EventStartZ", &EventStartZ, "EventStartZ/D");
  otr->Branch( "VertexTime", VertexTime, "VertexTime[3]/D");
  otr->Branch( "OriginalClusterNumber", &OriginalClusterNumber, "OriginalClusterNumber/I" );
  otr->Branch( "OriginalClusterE", OriginalClusterE, "OriginalClusterE[OriginalClusterNumber]/D" );
  otr->Branch( "OriginalClusterT", OriginalClusterT, "OriginalClusterT[OriginalClusterNumber]/D" );
  otr->Branch( "OriginalClusterVertexT", OriginalClusterVertexT, "OriginalClusterVertexT[OriginalClusterNumber]/D" );
  otr->Branch( "MyCutCondition", &MyCutCondition, "MyCutCondition/I" );
  otr->Branch( "MyVetoCondition", &MyVetoCondition, "MyVetoCondition/I" );
  otr->Branch( "DeltaVertexTime", &DeltaVertexTime, "DeltaVertexTime/D" );
  otr->Branch( "MaxDeltaPi0Mass", &MaxDeltaPi0Mass, "MaxDeltaPi0Mass/D" );
  otr->Branch( "MinHalfEt", &MinHalfEt, "MinHalfEt/D" );
  otr->Branch( "TotalEt", &TotalEt, "TotalEt/D");
  otr->Branch( "MinGammaE", &MinGammaE, "MinGammaE/D");
  otr->Branch( "MaxGammaE", &MaxGammaE, "MaxGammaE/D");
  otr->Branch( "MaxFiducialR", &MaxFiducialR, "MaxFiducialR/D");
  otr->Branch( "MinFiducialXY", &MinFiducialXY, "MinFiducialXY/D");
  otr->Branch( "MinClusterDistance", &MinClusterDistance, "MinClusterDistance/D");
  otr->Branch( "KLDeltaChisqZ", &KLDeltaChisqZ, "KLDeltaChisqZ/D" );
  otr->Branch( "MaxShapeChisq", &MaxShapeChisq, "MaxShapeChisq/D" );
  otr->Branch( "ExtraClusterDeltaVertexTime", &ExtraClusterDeltaVertexTime, "ExtraClusterDeltaVertexTime/D" );
  otr->Branch( "ExtraClusterEne", &ExtraClusterEne, "ExtraClusterEne/D" );
  otr->Branch( "KLBeamExitX", &KLBeamExitX, "KLBeamExitX/D" );
  otr->Branch( "KLBeamExitY", &KLBeamExitY, "KLBeamExitY/D" );
  otr->Branch( "AverageClusterTime", &AverageClusterTime, "AverageClusterTime/D" );
  if( !isRealData ){
    otr->Branch( "TruePID",          &TruePID,           "TruePID/I" );
    otr->Branch( "TrueDecayMode",    &TrueDecayMode,     "TrueDecayMode/I" );
    otr->Branch( "TrueVertexZ",      &TrueVertexZ,       "TrueVertexZ/D" );
    otr->Branch( "TrueVertexTime",   &TrueVertexTime,    "TrueVertexTime/D" );
    otr->Branch( "TrueMom",           TrueMom,           "TrueMom[3]/D" );
    otr->Branch( "TruePos",           TruePos,           "TruePos[3]/D" );
    otr->Branch( "GsimGenEventID",   &GsimGenEventID,    "GsimGenEventID/I" );
    otr->Branch( "GsimEntryID",      &GsimEntryID,       "GsimEntryID/I" );
    otr->Branch( "MCEventID",        &MCEventID,         "MCEventID/I" );
    otr->Branch( "AccidentalFileName",   "std::string",          &AccidentalFileName );
    otr->Branch( "AccidentalFileID",     &AccidentalFileID,      "AccidentalFileID/I" );
    otr->Branch( "AccidentalEntryID",    &AccidentalEntryID,     "AccidentalEntryID/L" );
    otr->Branch( "AccidentalOverlayFlag",&AccidentalOverlayFlag, "AccidentalOverlayFlag/O" );
    genP->Branch( otr );
    if( IBWfmFlag ) otr->Branch("IBWfm",IBWfm,"IBWfm[64][256]/F");
  }

  GammaFinder gFinder;

  // Veto detectors
  Bool_t  DetectorExistFlag[MTBP::nDetectors];
  MTVeto* vetoHandler[MTBP::nDetectors];
  vetoHandler[MTBP::FBAR] = new MTVetoFBAR( userFlag );
  vetoHandler[MTBP::NCC]  = new MTVetoNCC( userFlag );
  vetoHandler[MTBP::CBAR] = new MTVetoCBAR( userFlag );
  vetoHandler[MTBP::BCV]  = new MTVetoBCV( userFlag );
  vetoHandler[MTBP::CV]   = new MTVetoCV( userFlag );
  vetoHandler[MTBP::LCV]  = new MTVetoLCV( userFlag );
  vetoHandler[MTBP::CC03] = new MTVetoCC03( userFlag );
  vetoHandler[MTBP::OEV]  = new MTVetoOEV( userFlag );
  vetoHandler[MTBP::CSI]  = new MTVetoCSI( userFlag );
  vetoHandler[MTBP::CC04] = new MTVetoCC04( userFlag );
  vetoHandler[MTBP::CC05] = new MTVetoCC05( userFlag );
  vetoHandler[MTBP::CC06] = new MTVetoCC06( userFlag );
  vetoHandler[MTBP::BPCV] = new MTVetoBPCV( userFlag );
  vetoHandler[MTBP::IBCV] = new MTVetoIBCV( userFlag );
  vetoHandler[MTBP::MBCV] = new MTVetoMBCV( userFlag );
  
  MTBHVeto* bhvetoHandler[5];
  bhvetoHandler[0] = new MTVetoBHCV( userFlag );
  bhvetoHandler[1] = new MTVetoBHPV( userFlag );
  bhvetoHandler[2] = new MTVetonewBHCV( userFlag );
  bhvetoHandler[3] = new MTVetoBHGC( userFlag );
  bhvetoHandler[4] = new MTVetoIB( userFlag );
  
  for( int idet=0; idet<MTBP::nDetectors; idet++ ){
    ///// Check the existence of the detector.
    ///// If it doesn't exist, DetectorHandler of the detector is not generated.
    if( inputTree->GetBranch(Form("%sNumber",MTBP::detectorName[idet].c_str())) 
	|| inputTree->GetBranch(Form("%sModuleNumber",MTBP::detectorName[idet].c_str())) ){
      DetectorExistFlag[idet] = true;
      std::cout << "Veto Detector : " << MTBP::detectorName[idet].c_str() << " exists." << std::endl;
    }else{
      DetectorExistFlag[idet] = false;
      std::cout<< "Veto Detector : " << MTBP::detectorName[idet].c_str() <<" doesn't exist." << std::endl;
      continue;
    }

    switch( idet ){
    case MTBP::FBAR:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      if( 20180101<=userFlag && userFlag<20180301 ) vetoHandler[idet]->BranchOriginalData( otr );              
      break;
    case MTBP::NCC:
    case MTBP::BCV:
    case MTBP::LCV:
    case MTBP::CSI:
    case MTBP::OEV:
    case MTBP::CC03:      
    case MTBP::CC04:
    case MTBP::CC05:
    case MTBP::CC06:
    case MTBP::BPCV:
    case MTBP::IBCV:
    case MTBP::MBCV:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      break;
    case MTBP::CV:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      vetoHandler[idet]->BranchOriginalData( otr );                  
      break;
    case MTBP::CBAR:
      vetoHandler[idet]->SetBranchAddress( inputTree );
      vetoHandler[idet]->Branch( otr );
      vetoHandler[idet]->BranchOriginalData( otr );
      break;
    case MTBP::BHCV:
      bhvetoHandler[0]->SetBranchAddress( inputTree );
      bhvetoHandler[0]->Branch( otr );
      break;        
    case MTBP::BHPV:
      bhvetoHandler[1]->SetBranchAddress( inputTree );
      bhvetoHandler[1]->Branch( otr );
      break;
    case MTBP::newBHCV:
      bhvetoHandler[2]->SetBranchAddress( inputTree );
      bhvetoHandler[2]->Branch( otr );
      break;                      
    case MTBP::BHGC:
      bhvetoHandler[3]->SetBranchAddress( inputTree );
      bhvetoHandler[3]->Branch( otr );
      break;                            
    case MTBP::IB:
      bhvetoHandler[4]->SetBranchAddress( inputTree );
      bhvetoHandler[4]->Branch( otr );
      if( IBWfmFlag ) bhvetoHandler[4]->UpdatedBranch( otr );
      break;                      
    default:
      break;
    }
  }

  vetoHandler[MTBP::CSI]->SetDataContainer( &data );

  for( Int_t iDet=0 ; iDet<MTBP::nDetectors ; iDet++ ){
    if( !DetectorExistFlag[iDet] )
      continue;

    if( iDet!=MTBP::BHCV && iDet!=MTBP::BHGC && iDet!=MTBP::newBHCV && iDet!=MTBP::BHPV && iDet!=MTBP::IB && MTBP::VetoWidth[iDet]>0 ){
      vetoHandler[iDet]->SetVetoThreshold( MTBP::VetoEneThre[iDet] );
      vetoHandler[iDet]->SetVetoWindow( MTBP::VetoWidth[iDet], MTBP::VetoTiming[iDet]  );
    }
  }
  bhvetoHandler[0]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::BHCV] );
  bhvetoHandler[0]->SetVetoWindow( MTBP::VetoWidth[MTBP::BHCV], MTBP::VetoTiming[MTBP::BHCV] );
  bhvetoHandler[1]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::BHPV] );
  bhvetoHandler[1]->SetVetoWindow( MTBP::VetoWidth[MTBP::BHPV], MTBP::VetoTiming[MTBP::BHPV] );

  bhvetoHandler[2]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::newBHCV] );
  bhvetoHandler[2]->SetVetoWindow( MTBP::VetoWidth[MTBP::newBHCV], MTBP::VetoTiming[MTBP::newBHCV] );

  bhvetoHandler[3]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::BHGC] );
  bhvetoHandler[3]->SetVetoWindow( MTBP::VetoWidth[MTBP::BHGC], MTBP::VetoTiming[MTBP::BHGC] );

  bhvetoHandler[4]->SetVetoThreshold( MTBP::VetoEneThre[MTBP::IB] );
  bhvetoHandler[4]->SetVetoWindow( MTBP::VetoWidth[MTBP::IB], MTBP::VetoTiming[MTBP::IB] );

  // loop analysis
  for( int entry=0; entry<nEntries; entry++ ){
    if( nEntries>100 )
      if( entry%(nEntries/10)==0 )
	std::cout << entry/(nEntries/100) << "%" << std::endl;
    

    // Initialize
    MyCutCondition = 0;
    MyVetoCondition = 0;
    OriginalClusterNumber = 0;
    DeltaVertexTime = 0;
    MaxDeltaPi0Mass = 0;
    MinHalfEt = 0;
    MinGammaE = 9999;
    MaxGammaE = 0;
    MaxFiducialR = 0;
    MinFiducialXY = 9999;
    MinClusterDistance = 1E15;
    KLDeltaChisqZ = 0;
    MaxShapeChisq = 0;
    ExtraClusterDeltaVertexTime = 9999;
    ExtraClusterEne = 0;
    KLBeamExitX = 0;
    KLBeamExitY = 0;
    AverageClusterTime = -9999.;
    TotalEt=0;
    
    // read data
    inputTree->GetEntry( entry );

    ClusteringEntryID = entry;
    
    // Online trigger
    MinHalfEt = std::min( CSIHalfEtR, CSIHalfEtL );
    if( MinHalfEt < 250. ){
      // continue;
    }


    std::list<Cluster> clist;
    data.getData(clist);

    // Store original clusters information
    for( std::list<Cluster>::iterator it=clist.begin(); it!=clist.end(); it++ ){
      OriginalClusterE[OriginalClusterNumber] = it->e();
      OriginalClusterT[OriginalClusterNumber] = it->t();
      OriginalClusterVertexT[OriginalClusterNumber] = it->t();
      OriginalClusterPos[OriginalClusterNumber][0] = it->x();
      OriginalClusterPos[OriginalClusterNumber][1] = it->y();
      OriginalClusterPos[OriginalClusterNumber][2] = it->z();
      
      OriginalClusterNumber++;
    }
    bool* clusterUsageFlag = new bool[OriginalClusterNumber]();
    for( int icluster=0; icluster<OriginalClusterNumber; icluster++ ){
      clusterUsageFlag[icluster] = false;
    }

    
    // gamma finding
    std::list<Gamma> glist;
    gFinder.findGamma( clist, glist); // gamma difinition ( > 20 MeV)


    // check number of gammas
    if( glist.size() != 3 ){
      data.KlongNumber   = 0;
      data.Pi0Number     = 0;
      data.GammaNumber   = 0;
      data.GamClusNumber = 0;
      //otr->Fill();
      data.eventID++;
      continue; 
    }


    // If there are more than 4 clusters, I select most near time 4 clusters
    std::vector< double > clusterTimeVec;
    for( std::list<Gamma>::iterator it=glist.begin(); it!=glist.end(); it++ ){
      clusterTimeVec.push_back( it->t() );
    }


    std::vector<Klong> klVec;
    if( !user_rec( glist, klVec) ){
      data.KlongNumber   = 0;
      data.Pi0Number     = 0;
      data.GammaNumber   = 0;
      data.GamClusNumber = 0;
      //otr->Fill();
      data.eventID++;
      continue;
    }

    // Vertex timing reconstruction
    EventStartTime = GetEventStartTime( klVec, VertexTime, CSIZPosition );
    EventStartZ = klVec[0].vz();


    for( int ipi0=0; ipi0<1; ipi0++ ){
      Pi0 const& pi0 = klVec[0].pi0().at(ipi0);
      for( int igamma=0; igamma<2; igamma++ ){
	Gamma const &g = (igamma==0) ? pi0.g1() : pi0.g2();
	clusterUsageFlag[g.id()] = true;
      }
    }

    for( int igamma=0; igamma<1; igamma++ ){
		Gamma const &g = *(klVec[0].gamma().begin());
		clusterUsageFlag[g.id()] = true;
  	}
    

    //cuts
    user_cut(data,klVec);





    // my cut condition
    double clusterPos[3][2];
    AverageClusterTime = 0;    

    for( int igamma=0; igamma<3; igamma++ ){
      if( DeltaVertexTime < fabs( EventStartTime-VertexTime[igamma] ) ){
	DeltaVertexTime = fabs( EventStartTime-VertexTime[igamma] );
      }
    }

    KLBeamExitX = klVec[0].vx() / ( klVec[0].vz()-MTBP::T1TargetZ) * ( MTBP::BeamExitZ-MTBP::T1TargetZ);
    KLBeamExitY = klVec[0].vy() / ( klVec[0].vz()-MTBP::T1TargetZ) * ( MTBP::BeamExitZ-MTBP::T1TargetZ);

    if( klVec.size() == 1 ){
      KLDeltaChisqZ = 0;
    }else{
      KLDeltaChisqZ = klVec[1].chisqZ() - klVec[0].chisqZ();
    }

    for( int ipi0=0; ipi0<1; ipi0++ ){
      Pi0 const& pi0 = klVec[0].pi0().at(ipi0);
      if( MaxDeltaPi0Mass < fabs( pi0.m() - MTBP::Pi0_MASS ) ){
	MaxDeltaPi0Mass = fabs( pi0.m() - MTBP::Pi0_MASS );
      }

      for( int igamma=0; igamma<2; igamma++ ){
	Gamma const &g = (igamma==0) ? pi0.g1() : pi0.g2();
	if( MinGammaE > g.e() ) MinGammaE = g.e();
	if( MaxGammaE < g.e() ) MaxGammaE = g.e();
	if( MaxFiducialR < g.pos().perp() )           MaxFiducialR = g.pos().perp();
	if( MinFiducialXY > std::max( fabs(g.x()), fabs(g.y())) ) MinFiducialXY = std::max( fabs(g.x()), fabs(g.y()) );
	if( MaxShapeChisq < g.chisq() ) MaxShapeChisq = g.chisq();
	clusterPos[ipi0*2+igamma][0] = g.x();
	clusterPos[ipi0*2+igamma][1] = g.y();
	AverageClusterTime += g.t();
	clusterUsageFlag[g.id()] = true;

	TotalEt+=g.e();
      }
    }

	for( int igamma=0; igamma<1; igamma++ ){
		Gamma const &g = (*klVec[0].gamma().begin());
		if( MinGammaE > g.e() ) MinGammaE = g.e();
		if( MaxGammaE < g.e() ) MaxGammaE = g.e();
		if( MaxFiducialR < g.pos().perp() )           MaxFiducialR = g.pos().perp();
		if( MinFiducialXY > std::max( fabs(g.x()), fabs(g.y())) ) MinFiducialXY = std::max( fabs(g.x()), fabs(g.y()) );
		if( MaxShapeChisq < g.chisq() ) MaxShapeChisq = g.chisq();
		clusterPos[2][0] = g.x();
		clusterPos[2][1] = g.y();
		AverageClusterTime += g.t();
		clusterUsageFlag[g.id()] = true;
		TotalEt+=g.e();
  	}

    for( int igamma=0; igamma<3; igamma++ ){
      for( int jgamma=igamma+1; jgamma<3; jgamma++ ){
	double tempDistance = pow( clusterPos[igamma][0]-clusterPos[jgamma][0], 2) + pow( clusterPos[igamma][1]-clusterPos[jgamma][1], 2);
	if( MinClusterDistance > tempDistance ){
	  MinClusterDistance = tempDistance;
	}
      }
    }
    MinClusterDistance = sqrt( MinClusterDistance );

    AverageClusterTime /= 3;


/*    if( true ){
      if( DeltaVertexTime > 3. ){
	MyCutCondition |= (1<<DELTA_VERTEX_TIME);
      }
      if( fabs( klVec[0].m() - MTBP::KL_MASS ) > 15. ){
	MyCutCondition |= (1<<DELTA_KL_MASS);
      }
      if( TotalEt < 650. ){
	MyCutCondition |= (1<<CsIEt);
      }
      if( klVec[0].p3().perp() > 50. ){
	MyCutCondition |= (1<<KL_PT);
      }
      if( klVec[0].chisqZ() > 20. ){
	MyCutCondition |= (1<<KL_CHISQZ);
      }
      if( MaxDeltaPi0Mass > 6. ){
	MyCutCondition |= (1<<DELTA_PI0_MASS);
      }
      if( klVec[0].deltaZ() > 400. ){	
	MyCutCondition |= (1<<DELTA_PI0_Z);
      }
      if( MinGammaE < 50. ){
	MyCutCondition |= (1<<MIN_GAMMAE);
      }
      if( MinFiducialXY < 150. ){
	MyCutCondition |= (1<<MIN_FIDUCIAL_XY);
      }
      if( 850. < MaxFiducialR ){
	MyCutCondition |= (1<<MAX_FIDUCIALR);
      }
      if( MinClusterDistance < 150. ){
	MyCutCondition |= (1<<MIN_CLUSTER_DISTANCE);
      }
      if( EventStartZ<3000 || 5000<EventStartZ ){
      	MyCutCondition |= (1<<KL_Z);
      }
      if( fabs(KLBeamExitX)>50 || fabs(KLBeamExitY)>50 ){
	MyCutCondition |= (1<<KL_BEAMEXIT_XY);
      }
      if( MaxShapeChisq > 7. ){
	MyCutCondition |= (1<<MAX_SHAPECHISQ);
      }
    }
*/
    // my veto condition
    if( OriginalClusterNumber > 3 ){
      for( int icluster=0; icluster<OriginalClusterNumber; icluster++ ){
	if( clusterUsageFlag[icluster] ) continue; // This cluster is included in KL
	if( fabs( ExtraClusterDeltaVertexTime ) > fabs( OriginalClusterVertexT[icluster] - EventStartTime ) ){
	  ExtraClusterDeltaVertexTime = OriginalClusterVertexT[icluster] - EventStartTime;
	  ExtraClusterEne = OriginalClusterE[icluster];
	}
      }
    }

    // veto detector check
    vetoHandler[MTBP::CSI]->SetKlInfo( &(klVec[0]) );




    for( int idet=0; idet<MTBP::nDetectors; idet++ ){
      if( !DetectorExistFlag[idet] )
	continue;
      switch( idet ){
      case MTBP::FBAR:
      case MTBP::OEV:
      case MTBP::CBAR:
      case MTBP::CC03:
      case MTBP::CC04:
      case MTBP::CC05:
      case MTBP::CC06:
      case MTBP::BCV:
      case MTBP::LCV:
      case MTBP::NCC:
      case MTBP::CV:
	vetoHandler[idet]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( vetoHandler[idet]->Process( 2 ) ){
	  //if( vetoHandler[idet]->Process( 2 ) ){	  
	  MyVetoCondition |= (1<<idet);
	}
	break;
      case MTBP::IBCV:
      case MTBP::MBCV:
      case MTBP::BPCV:
	vetoHandler[idet]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( vetoHandler[idet]->Process( 2 ) ){
	  //if( vetoHandler[idet]->Process( 2 ) ){	  
	  //MyVetoCondition |= (1<<idet);
	}
	break;
      case MTBP::CSI:
	vetoHandler[idet]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( vetoHandler[idet]->Process( 3 ) ){
	  MyVetoCondition |= (1<<idet);
	}	
	break;	
      case MTBP::BHCV:
	bhvetoHandler[0]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[0]->Process( 2 ) ){
	  if(userFlag<20160101)MyVetoCondition |= (1<<idet);
	}
	break;	
      case MTBP::BHPV:
	bhvetoHandler[1]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[1]->Process( 2 ) ){
	  MyVetoCondition |= (1<<idet);
	}
	break;	
      case MTBP::newBHCV:
	bhvetoHandler[2]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[2]->Process( 2 ) ){
	  //MyVetoCondition |= (1<<idet);
	}
	break;	
      case MTBP::BHGC:
	bhvetoHandler[3]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[3]->Process( 2 ) ){
	  //MyVetoCondition |= (1<<idet);
	}
	break;

      case MTBP::IB:
	bhvetoHandler[4]->SetEventStartInfo( EventStartZ, EventStartTime );
	if( bhvetoHandler[4]->Process( 2 ) ){
	  //MyVetoCondition |= (1<<idet);
	}
	if( IBWfmFlag )
	  if( bhvetoHandler[4]->Process( 2 ) )
	    //MyVetoCondition |= (1<<idet);
	break;
	
      default:
	break;
      }
    }

    if( fabs(ExtraClusterDeltaVertexTime) < 10. ){
      MyVetoCondition |= (1<<(MTBP::nDetectors));
    }
    
    //// for reducing IB wfm data size ////
    if( IBWfmFlag && 
	!( MyCutCondition==0 && (MyVetoCondition|0xc0)==0xc0 ) ){
      for(int iCh=0;iCh<64;iCh++){
	for( Int_t iSample = 0 ; iSample < MTBP::nSample500 ; iSample++ ){
	  IBWfm[iCh][iSample] = 0;
	}
      }
    }

    data.setData( klVec );

    otr->Fill();
    data.eventID++;

    delete[] clusterUsageFlag;
  }

  otf->cd();
  otr->Write();
  otf->Close();

  return 0;
}

