#include <iostream>
#include <list>
#include "klong/RecKlong.h"
#include "gnana/E14GNAnaFunction.h"
#include "ShapeChi2/ShapeChi2New.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "pi0/Pi0.h"
#include "rec2g/Rec2g.h"
#include "klong/RecKlong.h"
#include "CLHEP/Vector/LorentzVector.h"

std::vector<Klong> recKpi0gamma(const std::list<Gamma>& glist, int vtxflag=VERTEX_COE_SCALE, double pi0sig2cut=40000.);

int recVtxBisection(double Zmax, double Zmin, std::list<Gamma> glist);
Double_t recVtxMFunction(std::list<Gamma> glist, Double_t VtxZ);

double solv_z_KL_from3g(const Gamma* gams);

bool sortKL(const Klong& left, const Klong& right){
	return fabs(left.m()-MTBP::KL_MASS) < fabs(right.m()-MTBP::KL_MASS);
}

bool user_rec(std::list<Gamma> const &glist, std::vector<Klong> &klVec){
  ///// reconstruction
  klVec = recKpi0gamma(glist) ;  
  if(klVec.size()==0)
    return false;
  
  ///// gamma position & energy correction for angle dependency
  E14GNAnaFunction::getFunction()->correctPosition(klVec[0]); 
  E14GNAnaFunction::getFunction()->correctEnergyWithAngle(klVec[0]);  

  for(std::list<Gamma>::iterator it = klVec[0].gamma().begin(); 
		  it != klVec[0].gamma().end(); it++){
	  E14GNAnaFunction::getFunction()->correctPosition(*it); 
	  E14GNAnaFunction::getFunction()->correctEnergyWithAngle(*it);  
  }
  

  ///// re-reconstruction with corrected gamma
  std::list<Gamma> glist2;
  for( std::vector<Pi0>::iterator it=klVec[0].pi0().begin();
       it!=klVec[0].pi0().end(); it++){
    glist2.push_back(it->g1());
    glist2.push_back(it->g2());
  }
  glist2.push_back(*klVec[0].gamma().begin());
  
  klVec = recKpi0gamma(glist2) ;
  if(klVec.size()==0)
    return false;
  
  ///// shape chi2 evaluation
  //E14GNAnaFunction::getFunction()->shapeChi2(klVec[0]);
  static ShapeChi2New chi2Newcalc;

  for(int i=0;i<klVec.size();i++){

	  for( Int_t iPi0 = 0 ; iPi0 < 1 ; ++iPi0 ){
	    for( Int_t iGamma = 0 ; iGamma < 2 ; ++iGamma ){
	      Gamma& gam = ( (iGamma==0) ? 
			     klVec[i].pi0()[iPi0].g1() :
			     klVec[i].pi0()[iPi0].g2() );
	      chi2Newcalc.shapeChi2(gam);
	    }
	  }
	  
	  Gamma& gam2 = *klVec[i].gamma().begin();  
	  chi2Newcalc.shapeChi2(gam2);
  }
  return true;
}



std::vector<Klong> recKpi0gamma(const std::list<Gamma>& glist, int vtxflag, double pi0sig2cut){


	double mp0 = MTBP::Pi0_MASS;	

	std::vector<Klong> kpi0gam;

	if(glist.size()!=3) return kpi0gam;

	Gamma gams[3];
	int ng = 0;
	for( std::list<Gamma>::const_iterator g =glist.begin(); g!=glist.end(); g++){
		gams[ng++] = *g;
	}

	double posz_3g = solv_z_KL_from3g(gams);

	if(posz_3g<0) return kpi0gam;


	CLHEP::HepLorentzVector Vtx_3g(0,0, posz_3g);

	CLHEP::Hep3Vector Posg[3];
	Posg[0].set(gams[0].x(), gams[0].y(), 6168);
	Posg[1].set(gams[1].x(), gams[1].y(), 6168);
	Posg[2].set(gams[2].x(), gams[2].y(), 6168);

	double Eg[3] = {gams[0].e(), gams[1].e(), gams[2].e()};

	CLHEP::HepLorentzVector Pgams_3grec[3];
	Pgams_3grec[0] =  CLHEP::HepLorentzVector((Posg[0]-Vtx_3g).unit()*Eg[0], Eg[0]);
	Pgams_3grec[1] =  CLHEP::HepLorentzVector((Posg[1]-Vtx_3g).unit()*Eg[1], Eg[1]);
	Pgams_3grec[2] =  CLHEP::HepLorentzVector((Posg[2]-Vtx_3g).unit()*Eg[2], Eg[2]);
	
	double m01 = (Pgams_3grec[0]+Pgams_3grec[1]).m();
	double m12 = (Pgams_3grec[1]+Pgams_3grec[2]).m();
	double m20 = (Pgams_3grec[2]+Pgams_3grec[0]).m();

	int ind_cand;
	if(fabs(m01-mp0)<=fabs(m12-mp0) && fabs(m12-mp0)<fabs(m20-mp0))      ind_cand = 0;
	else if(fabs(m01-mp0)<=fabs(m20-mp0) && fabs(m20-mp0)<fabs(m12-mp0)) ind_cand = 1;
	else if(fabs(m12-mp0)<=fabs(m01-mp0) && fabs(m01-mp0)<fabs(m20-mp0)) ind_cand = 2;
	else if(fabs(m12-mp0)<=fabs(m20-mp0) && fabs(m20-mp0)<fabs(m01-mp0)) ind_cand = 3;
	else if(fabs(m20-mp0)<=fabs(m01-mp0) && fabs(m01-mp0)<fabs(m12-mp0)) ind_cand = 4;
	else if(fabs(m20-mp0)<=fabs(m12-mp0) && fabs(m12-mp0)<fabs(m01-mp0)) ind_cand = 5;
	else{
		ind_cand = -1;
		std::cout << "strange! we skip processing this event!\n";
		return kpi0gam;

	}
	int indspair1[2][6]; 
	int indspair2[2][6];
	int indspair_oth[2][6];

	indspair1[0][0]=0; indspair2[0][0]=1; indspair_oth[0][0]=2;
	indspair1[0][1]=0; indspair2[0][1]=1; indspair_oth[0][1]=2;
	indspair1[0][2]=1; indspair2[0][2]=2; indspair_oth[0][2]=0;
	indspair1[0][3]=1; indspair2[0][3]=2; indspair_oth[0][3]=0;
	indspair1[0][4]=2; indspair2[0][4]=0; indspair_oth[0][4]=1;
	indspair1[0][5]=2; indspair2[0][5]=0; indspair_oth[0][5]=1;

	indspair1[1][0]=1; indspair2[1][0]=2; indspair_oth[1][0]=0;
	indspair1[1][1]=2; indspair2[1][1]=0; indspair_oth[1][1]=1;
	indspair1[1][2]=0; indspair2[1][2]=1; indspair_oth[1][2]=2;
	indspair1[1][3]=2; indspair2[1][3]=0; indspair_oth[1][3]=1;
	indspair1[1][4]=0; indspair2[1][4]=1; indspair_oth[1][4]=2;
	indspair1[1][5]=1; indspair2[1][5]=2; indspair_oth[1][5]=0;

	//std::cout << "1st cand mgg = " << (Pgams_3grec[indspair1[0][ind_cand]]+Pgams_3grec[indspair2[0][ind_cand]]).m() << "\n"; 
	//std::cout << "2nd cand mgg = " << (Pgams_3grec[indspair1[1][ind_cand]]+Pgams_3grec[indspair2[1][ind_cand]]).m() << "\n"; 


	Rec2g rec2g; // pi0 reconstruction 
	for(int i=0;i<2;i++){
		std::list<Gamma> glist_sub;
		glist_sub.push_back(gams[indspair1[i][ind_cand]]);
		glist_sub.push_back(gams[indspair2[i][ind_cand]]);
	
		std::list<Pi0> pi0list = rec2g.recPi0withConstM( glist_sub );

		if(pi0list.size()==0)continue;

		Pi0 pi0 = *pi0list.begin();
		//if(pi0.recZsig2()>pi0sig2cut) continue;
	
//		double recz_KL = pi0.recZ();
		double recz_KL = posz_3g;	

		Pi0 pi1p = pi0;
		pi1p.setRecZ(posz_3g);
		pi1p.setVtx(0,0,posz_3g);
		pi1p.updateVars();
	
		Klong klong;
		klong.setVertexFlag(vtxflag);
		klong.setVtx(CLHEP::Hep3Vector(0,0,posz_3g));
		klong.addPi0(pi1p);
	
		Gamma gam = gams[indspair_oth[i][ind_cand]];
		CLHEP::Hep3Vector gp = CLHEP::Hep3Vector(gam.x(), gam.y(), gam.z()-recz_KL);
		gp.setMag(gam.e());			
		gam.setP3(CLHEP::Hep3Vector(gp.x(),gp.y(), gp.z()));
		klong.addGamma(gam);

	
		
		CLHEP::HepLorentzVector Pgam(gp,gp.mag());
		CLHEP::HepLorentzVector Ppi1(pi1p.p3(), pi1p.e());
		double mklrec = (Ppi1+Pgam).m();
		double Eklrec = (Ppi1+Pgam).e();
	
		klong.setChisqZ(-1);
		klong.setMass(mklrec);
		klong.setDeltaZ(-1);
		
//added by noah, Zvertex of K->3 gamma
//		klong.setVtx(CLHEP::Hep3Vector(0,0,posz_3g));	// this is using Newton's method
/*		klong.setVtx(CLHEP::Hep3Vector(0,0,0));	
		klong.setMass(sqrt(recVtxMFunction(glist, posz_3g)));

		double recVertex = recVtxBisection(5500, 2000,glist); 
		klong.setVtx(CLHEP::Hep3Vector(0,0,recVertex)); // this is using Bisection method
		klong.setMass(sqrt(recVtxMFunction(glist,recVertex) + pow (497, 2)));
		//if( recVtxBisection(5500, 2000,glist) == 1) { // checks to see if there is only one solution
 */              // 	klong.setVtx(CLHEP::Hep3Vector(0,0,VtxZ));
	//	}
//	

		klong.setEnergy(Eklrec);
		klong.setP3((Ppi1+Pgam).v());
		
		kpi0gam.push_back(klong);
	}

	int id = 0;
	for(std::vector<Klong>::iterator i=kpi0gam.begin(); i!=kpi0gam.end(); i++){
		i->setId( id );
		id++;
	}

	return kpi0gam;
}


void calc_F_df_from3g(double* par, double x, double& f, double& dfdx){

	double mkl = MTBP::KL_MASS;	
	
	double R0sq = par[0];
	double R1sq = par[1];
	double R2sq = par[2];
	double Q01  = par[3];
	double Q12  = par[4];
	double Q20  = par[5];
	double E0   = par[6];
	double E1   = par[7];
	double E2   = par[8];

	f =   2*E0*E1*(1-(Q01+x)/sqrt((x+R0sq)*(x+R1sq))) 
		+ 2*E1*E2*(1-(Q12+x)/sqrt((x+R1sq)*(x+R2sq))) 
		+ 2*E2*E0*(1-(Q20+x)/sqrt((x+R2sq)*(x+R0sq))) - mkl*mkl;

	dfdx = -E0*E1*(x*(R0sq+R1sq-2*Q01)+2*R0sq*R1sq-Q01*(R0sq+R1sq))/(x+R0sq)/(x+R1sq)/sqrt((x+R0sq)*(x+R1sq))
		-E1*E2*(x*(R1sq+R2sq-2*Q12)+2*R1sq*R2sq-Q12*(R1sq+R2sq))/(x+R1sq)/(x+R2sq)/sqrt((x+R1sq)*(x+R2sq))
		-E2*E0*(x*(R2sq+R0sq-2*Q20)+2*R2sq*R0sq-Q20*(R2sq+R0sq))/(x+R2sq)/(x+R0sq)/sqrt((x+R2sq)*(x+R0sq));

	return;
}


double solv_z_KL_from3g(const Gamma* gams){


	double R0sq = gams[0].x()*gams[0].x()+gams[0].y()*gams[0].y();
	double R1sq = gams[1].x()*gams[1].x()+gams[1].y()*gams[1].y();
	double R2sq = gams[2].x()*gams[2].x()+gams[2].y()*gams[2].y();
		
	double Q01  = gams[0].x()*gams[1].x()+gams[0].y()*gams[1].y();
	double Q12  = gams[1].x()*gams[2].x()+gams[1].y()*gams[2].y();
	double Q20  = gams[2].x()*gams[0].x()+gams[2].y()*gams[0].y();

	double E[3] = {gams[0].e(), gams[1].e(), gams[2].e()};

	double par[9];
	par[0] = R0sq;
	par[1] = R1sq;
	par[2] = R2sq;
	par[3] = Q01; 
	par[4] = Q12; 
	par[5] = Q20; 
	par[6] = E[0];  
	par[7] = E[1];  
	par[8] = E[2];  

	double t_prev = 0; //0
	double ft_prime;
	double ft;
			
	double t = 300*300;   //300*300
	int c = 0;
	const double tmin = 0;
	const double tmax = 10000*10000;

	while(1){

		calc_F_df_from3g(par, t, ft, ft_prime);
						
		t_prev = t;
		t = t - ft/ft_prime;
		if(t<tmin || t>tmax) t = tmin + (tmax-tmin)*rand()*1.0/RAND_MAX;
		c++;
						
		if(fabs(t_prev-t)<0.1 || c>=1000)break; // fabs(t_prev-t)<1	
	}
			
	if(c==1000){
		return -1; //-1
	}
	double L = sqrt(t);
			
	return 6168.0-L;

}








// ------- below is code (two functions) for solving the KL->3gamma z vertex by the bisection method. Code is from Mr.X
//	   I assume COE ox x and y is zero.
double recVtxMFunction(std::list<Gamma> glist, double VtxZ)
{
        const double MKL = 497.611;
        int id=0;
        double x[3], y[3], z[3];      //cluster Pos on CSI
	double E[3];                  //cluster energy
	double DistCluVtx[3];         //distance between vertex and cluster
	double DistBtwClu[3][3];      //distance between two clusters
	double CosAngle[3][3];        //cos of the opening angle between any two cluster to vertex
	double Function = 0;          //the function we want to find the root which is the invariant mass of Klong = sum of E
	
	for( std::list<Gamma>::iterator g1=glist.begin();
             g1!=glist.end(); g1++ )
        {
                Gamma g = *g1;
                x[id] = g.pos().x() - 0;
                y[id] = g.pos().y() - 0;
                z[id] = 6168;
                E[id] = g.e();
                DistCluVtx[id] = sqrt( pow(z[id]-VtxZ,2) + pow(x[id],2) + pow(y[id],2) );
                id++;
        }



	for(int i=0;i<3;i++)
        {
                for(int j=0;j<3;j++)
                {
                        DistBtwClu[i][j] = sqrt( pow(x[i]-x[j],2) + pow(y[i]-y[j],2) );
                        CosAngle[i][j] = ( pow(DistCluVtx[i],2) + pow(DistCluVtx[j],2) - pow(DistBtwClu[i][j],2) ) / (2*DistCluVtx[i]*DistCluVtx[j]);
                        Function += E[i]*E[j]*(1-CosAngle[i][j]);
                }
        }
        Function -= pow(MKL,2);          //rest mass of Klong
        return Function;
}



// Find solution for the MFunction
int recVtxBisection(double Zmax, double Zmin, std::list<Gamma> glist){
        double Z[1000];  //1000
        int n = 1000;   //1000
        int NumSolution = 0;
        Double_t a, b, c, VtxZ = 0;    // c is mid point of a and b
	
	
	        Double_t ErrZ = 0.1; // original value 0.1
		//Double_t ErrZ = 25;
        for(int i=0; i<n; i++)
        {
                Z[i] = Zmin + (Zmax-Zmin)*i/n;
        }

        for(int i=0; i<n-1; i++)
        {
                if(recVtxMFunction(glist, Z[i])*recVtxMFunction(glist, Z[i+1]) < 0)
                {
                        NumSolution ++;
                        a = Z[i];
                        b = Z[i+1];
                        while(recVtxMFunction(glist, a)*recVtxMFunction(glist, b) < 0 && b-a >= ErrZ)
                        {
                                c = (a+b)/2;
                                if(recVtxMFunction(glist, a)*recVtxMFunction(glist, c) < 0)
                                        b = c;
                                else
                                        a = c;
                                if(recVtxMFunction(glist, c) == 0)
                                        break;
                        }
                        VtxZ = c;
                }
        }
        if(NumSolution == 1){
                //klong.setVtx( 0, 0, VtxZ);
                VtxZ = VtxZ;
	} else {
		 //klong.setVtx( 0, 0, 0.1);
		 VtxZ = NAN;
	}
//        return NumSolution;
        return VtxZ;
}









